import numpy as np
from scipy.optimize import NonlinearConstraint
from scipy.optimize import OptimizeResult

def minimum_norm_optimizer(guess, constraints: dict, tol: float, damping: float,
                           maxiter: int, verbose: bool):
    """Solve a constrained optimization problem using the minimum norm method. Newton-Raphson

    Parameters
    ----------
    guess : np.ndarray
        Initial guess.
    constraints : NonlinearConstraint
        Constraints object.
    tol : float
        Tolerance for the optimization algorithm
    damping : float, optional
        Damping factor
    maxiter : int
        Maximum number of iterations
    verbose : bool
        Print the optimization progress

    Returns
    -------
    opt.OptimizeResult
        Corrected Result object.
    """

    # Initialize the solution.
    solution = guess.copy()
    res_norm_prev = np.inf
    it = 0
    # Iterate until the solution converges.
    while it < maxiter:
        it += 1

        # Check for convergence
        if res_norm_prev < tol:
            break

        # Compute the gradient
        residuals = constraints['fun'](solution)
        gradient = constraints['jac'](solution)
        res_norm_prev = np.linalg.norm(residuals)

        # Compute the step
        step = gradient.T.dot(np.linalg.solve(gradient.dot(gradient.T), residuals))

        print(f"Iteration {it}: Residuals norm {np.linalg.norm(residuals)}") if verbose else None

        # Compute the new solution
        new_solution = solution - step*damping

        # Update the solution
        solution = new_solution.copy()

    # Create the OptimizeResult object.
    result = OptimizeResult()
    result.x = solution
    result.fun = constraints['fun'](solution)
    result.jac = constraints['jac'](solution)
    result.success = True
    result.message = 'Optimization terminated successfully.'

    return result