from typing import Collection

import scipy.optimize as scopt
import numpy as np

import src.init.defaults as dft
from src.solve.constraints import Constraints
from src.solve.optimize.minimum_norm_optimizer import minimum_norm_optimizer

class Optimizer:
    """Minimizer."""
    def __init__(self, equality_constraints: Constraints=None,
                 inequality_constraints: Constraints=None,
                 tol: float=dft.correction_tolerance):
        """Initializes the optimizer.

            Parameters
            ----------
            equality_constraints : Constraints, optional
                Equality constraints.
            inequality_constraints : Constraints, optional
                Inequality constraints.
            tol : float, optional
                Tolerance for the optimization algorithm.
            """

        if equality_constraints is None and inequality_constraints is None:
            raise ValueError('At least one constraint must be provided.')
        
        self.tol = tol  # TODO consider making maxiter also an input parameter
        self._eq_c = equality_constraints
        self._in_c = inequality_constraints

        # Initialise flags
        self.jac_flag = False
        self.maximize_flag = False

        # Initialize attributes
        self.method = None
        self.objective = None
        self.dim_c, self.dim_guess = None, None
        self.dim_c_eq, self.dim_c_in = None, None
        self.scipy_options = None

    @property
    def jac_flag(self):
        return self._jac_flag

    @jac_flag.setter
    def jac_flag(self, value):
        self._jac_flag = value
        # Build the constraints dictionaries
        self.constraints_eq, self.constraints_in, self.constraints_tuple = \
            self._build_constraints_dicts(self._eq_c, self._in_c)

    def _build_constraints_dicts(self, constraints_eq, constraints_ineq) -> Collection[dict]:
        """Builds the constraints in form of dictionary."""
        constraints_eq_dict = None
        constraints_in_dict = None
        constraints_dicts = tuple()

        # Check if there are equality constraints
        if constraints_eq is not None:
            res_eq = self._extract_residuals(constraints_eq.constraints_fstack)
            constraints_eq_dict = {'type': 'eq', 'fun': res_eq}
            if self.jac_flag:
                jac_eq = self._extract_jacobian(constraints_eq.constraints_fstack)
                constraints_eq_dict['jac'] = jac_eq
            constraints_dicts += (constraints_eq_dict,)
        # Check if there are inequality constraints
        if constraints_ineq is not None:
            res_in = self._extract_residuals(constraints_ineq.constraints_fstack)
            constraints_in_dict = {'type': 'ineq', 'fun': res_in}
            if self.jac_flag:
                jac_in = self._extract_jacobian(constraints_ineq.constraints_fstack)
                constraints_in_dict['jac'] = jac_in
            constraints_dicts += (constraints_in_dict,)
        return constraints_eq_dict, constraints_in_dict, constraints_dicts

    def _check_problem_dimensionality(self, guess, verbose=False):
        """Check the dimensionality of the problem, raising error if underdefined."""
        dim_c_eq = 0
        dim_c_in = 0

        if self.constraints_eq is not None:
            dim_c_eq = len(self.constraints_eq['fun'](guess))
        if self.constraints_in is not None:
            dim_c_in = len(self.constraints_in['fun'](guess))

        dim_c = dim_c_eq + dim_c_in
        dim_guess = len(guess)

        msg = f'({dim_guess} variables and {dim_c} constraints)'
        if dim_c == dim_guess:
            print(f'Warning: the problem is square {msg}.') if verbose else None
        elif dim_c > dim_guess:
            print(f'Warning: the problem is over-constrained {msg}.') if verbose else None
        elif dim_c < dim_guess:
            print(f'Warning: the problem is under-constrained {msg}.') if verbose else None
        return dim_c, dim_c_eq, dim_c_in, dim_guess

    @staticmethod
    def _extract_residuals(fun):
        """Constraints residuals extractor."""
        def residuals(x):
            return fun(x, jac_flag=False)
        return residuals

    @staticmethod
    def _extract_jacobian(fun):
        """Constraints jacobian extractorr."""
        def jacobian(x):
            return fun(x, jac_flag=True)[1]
        return jacobian

    def _build_constraints_objects(self) -> Collection[scopt.NonlinearConstraint]:
        """Builds the constraints for the trust region algorithm, divided ."""
        constraints_eq = None
        constraints_in = None
        constraints_tuple = tuple()

        # Check if there are equality constraints
        if self.constraints_eq is not None:
            ub = [0] * self.dim_c_eq
            lb = [0] * self.dim_c_eq
            res_eq = self.constraints_eq['fun']
            jac_eq = self.constraints_eq['jac'] if self.jac_flag else '2-point'
            constraints_eq = scopt.NonlinearConstraint(res_eq, lb, ub, jac_eq)
            constraints_tuple += (constraints_eq,)
        # Check if there are inequality constraints
        if self.constraints_in is not None:
            ub = [0] * self.dim_c_in
            lb = [-np.inf] * self.dim_c_in
            res_in = self.constraints_in['fun']
            jac_in = self.constraints_in['jac'] if self.jac_flag else '2-point'
            constraints_in = scopt.NonlinearConstraint(res_in, lb, ub, jac_in)
            constraints_tuple += (constraints_in,)
        return constraints_eq, constraints_in, constraints_tuple

    def optimize_minimum_norm(self, guess, damping=1e-3, maxiter=100, verbose=False):
        self.method = 'min-norm'

        self.jac_flag = True  # Force the jacobian flag to be True

        # Check the dimensionality of the problem
        self.dim_c, self.dim_c_eq, self.dim_c_in, self.dim_guess =\
            self._check_problem_dimensionality(guess, verbose=verbose)

        # Check if inequality constraints are provided and raise error if method is min-norm
        if self.constraints_in is not None:
            raise ValueError('min-norm method does not support inequality constraints.')

        print(f"Running optimization algorithm '{self.method}'...") if verbose else None
        return minimum_norm_optimizer(guess, self.constraints_eq, tol=self.tol, damping=damping,
                                      maxiter=maxiter, verbose=verbose)

    @staticmethod
    def _decorate_objective_function(obj_fun, max_flag, jac_flag):
        def objective_fun(variables):
            return obj_fun(variables, jac_flag=jac_flag)
        if max_flag:
            def objective_fun(variables):
                if jac_flag:
                    res, jac = obj_fun(variables, jac_flag=jac_flag)
                    return -res, -jac
                return -obj_fun(variables, jac_flag)
        return objective_fun

    def optimize_trust_region(self, guess, objective, jac_flag: bool = False, bounds: tuple = None,
                                  maximize_flag: bool = False, scipy_options: dict = None,
                                  verbose: bool = False):

        if self.jac_flag != jac_flag:
            self.jac_flag = jac_flag

        self.maximize_flag = maximize_flag
        self.scipy_options = scipy_options

        self.method = 'trust-constr'

        # Check the dimensionality of the problem
        self.dim_c, self.dim_c_eq, self.dim_c_in, self.dim_guess =\
            self._check_problem_dimensionality(guess, verbose=verbose)

        _, _, constraints = self._build_constraints_objects()

        self.objective = self._decorate_objective_function(objective, self.maximize_flag, self.jac_flag)

        # TEMPORARY: check if the objective function is the null cost. If yes, set the hessian
        # equal to array of zeros of the same dimension of the guess. The correct check should be
        # if the objective function is linear, which means the jacobian is constant, but this
        # is not implemented yet.
        test = self.objective(guess)
        hessian = None
        if self.jac_flag:
            if test[0] == 0 and np.all(test[1] == 0):
                hessian = lambda x: np.zeros((len(x), len(x)))
                print('Warning: the objective function is the null cost. Setting the hessian to zero.')

        print(f"Running optimization algorithm '{self.method}'...") if verbose else None
        return scopt.minimize(self.objective, guess, jac=self.jac_flag, constraints=constraints,
                              method=self.method, bounds=bounds, tol=self.tol,
                              options=self.scipy_options, hess=hessian)
    
    def optimize_slsqp(self, guess, objective, bounds=None, jac_flag: bool=False,
                       maximize_flag: bool=False, scipy_options: dict=None,
                       verbose: bool=False):

        if self.jac_flag != jac_flag:
            self.jac_flag = jac_flag
        self.maximize_flag = maximize_flag
        self.scipy_options = scipy_options

        self.method = 'SLSQP'

        # Check the dimensionality of the problem
        self.dim_c, self.dim_c_eq, self.dim_c_in, self.dim_guess =\
            self._check_problem_dimensionality(guess, verbose=verbose)

        self.objective = self._decorate_objective_function(objective, self.maximize_flag, self.jac_flag)

        print(f"Running optimization algorithm '{self.method}'...") if verbose else None
        return scopt.minimize(self.objective, guess, jac=self.jac_flag,
                              bounds=bounds, constraints=self.constraints_tuple, 
                              method=self.method, tol=self.tol, options=self.scipy_options)

if __name__ == '__main__':
    from src.solve.constraints import Constraints

    def obj(x, jac_flag=False):
        res = np.linalg.norm(x) ** 2
        if jac_flag:
            jac = 2 * x
            return res, jac
        return res

    # Inequality constraints functions
    def ineq_cstr(x, jac_flag):
        res = np.linalg.norm(x) ** 2 - 2
        if jac_flag:
            jac = 2 * x[np.newaxis, :]
            return res, jac
        return res

    # Equality constraints functions
    def eq_cstr(x, jac_flag):
        res = x[0] - x[1]
        if jac_flag:
            jac = np.zeros((1, len(x)))
            jac[0, :2] = [1, -1]
            return res, jac
        return res

    # Define environment
    x0_guess = np.array([0.78670356, 0.54294056])

    cstr_eq = Constraints([eq_cstr])
    cstr_in = Constraints([ineq_cstr])

    opti = Optimizer(cstr_eq, cstr_in, tol=1e-16)

    sol_maxi = opti.optimize_trust_region(x0_guess, obj, jac_flag=True, maximize_flag=True)
    sol_mini = opti.optimize_trust_region(x0_guess, obj, jac_flag=True)

    print(f'x0 maxi = {sol_maxi.x}')
    print(f'f(x0)  maxi = {sol_maxi.fun}')
    print(f'x0 mini = {sol_mini.x}')
    print(f'f(x0) mini = {sol_mini.fun}')
