from typing import Collection
import numpy as np


def distance_between_first_and_last_point(variables: np.ndarray, d_min: float,
                                          jac_flag: bool):
    """
    Distance between the first and the last point of the trajectory.
    Variables here are the timestates in the form of
    (x0, y0, z0, vx0, vy0, vz0, ..., xN, yN, zN, vxN, vyN, vzN, t0, t1, ..., tN)
    """
    N_nodes = len(variables) // 7
    first_position = variables[:3]
    last_position = variables[6 * (N_nodes - 1):6 * (N_nodes - 1) + 3]

    distance = np.linalg.norm(first_position - last_position)

    if jac_flag:
        jac = np.zeros((len(variables)))
        jac[:3] = (first_position - last_position)/distance
        jac[6 * (N_nodes - 1):6 * (N_nodes - 1) + 3] = -(first_position - last_position)/distance
        return abs(distance - d_min), jac
    return abs(distance - d_min)

def apsis(variables: np.ndarray, environment, index_states: Collection, jac_flag: bool):
    """
    Flight path angle at the desired states.
    Variables here are the timestates in the form of
    (x0, y0, z0, vx0, vy0, vz0, ..., xN, yN, zN, vxN, vyN, vzN, t0, t1, ..., tN)
    """
    N_nodes = len(variables) // 7
    N_states = N_nodes * 6
    all_states = variables[:N_states]
    all_times = variables[-N_nodes:]

    states = [all_states[6*i:6*i + 6] for i in index_states]
    rv_dot = np.zeros(len(states))
    for i, s in enumerate(states):
        rv_dot[i] = np.dot(s[:3], s[3:6])
    if jac_flag:
        jac_rvdot_state = np.zeros(N_states)
        jac_rvdot_time = np.zeros(N_nodes)
        for i, (s, ind) in enumerate(zip(states, index_states)):
            jac_rvdot_state[6*ind:6*ind + 6] = np.array([s[3], s[4], s[5], s[0], s[1], s[2]])

            # Compute derivative of the state w.r.t. time
            state_dot = environment.eom(all_times[ind], s)
            jac_rvdot_time[ind] = np.dot(s[3:6], s[3:6]) + np.dot(state_dot[3:6], s[:3])
        # Stack jacobians
        jac = np.concatenate((jac_rvdot_state, jac_rvdot_time))
        return np.sum(abs(rv_dot)), jac
    return np.sum(abs(rv_dot))

def null_cost(variables: np.ndarray, jac_flag: bool):
    """
    Null cost function.
    """

    if jac_flag:
        return 0, np.zeros((len(variables)))
    return 0
