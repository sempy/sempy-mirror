from typing import Any, Collection

import src.solve.optimize.objective_fun as cfun

class ObjectiveLib:
    """Cost Function library class."""

    @staticmethod
    def _add_args(fun, *args: Any):
        """Set the cost function with user-defined arguments.

        Parameters
        ----------
        fun : Callable
            Function to be called.
        *args : Any
            Arguments to be passed to the function.
        """

        # Decorate the function with the user-defined arguments
        def cost_function(variables, jac_flag=False):
            # Return the decorated function
            return fun(variables, *args, jac_flag=jac_flag)

        # Rename the function with the cost function's name
        cost_function.__name__ = fun.__name__

        # Return the decorated function
        return cost_function

class EphemerisCorrectionLib(ObjectiveLib):
    """
    Cost function for the time states.
    """
    @classmethod
    def distance_between_first_and_last_point(cls, d_min: float=0.0):
        """
        Cost function to compute the distance between the first and the last point of the trajectory.
        """
        return cls._add_args(cfun.distance_between_first_and_last_point, d_min)

    @classmethod
    def apsis(cls, environment, index_state: Collection):
        """
        Cost function to compute the flight path angle at the desired states.
        """
        return cls._add_args(cfun.apsis, environment, index_state)

    @classmethod
    def null_cost(cls):
        """
        Cost function that returns zero.
        """
        return cls._add_args(cfun.null_cost)