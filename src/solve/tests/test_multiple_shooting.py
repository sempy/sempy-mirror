"""
Tests on MultipleShooting routines.

"""

import unittest
import os
import numpy as np
import json

import src.init.defaults as dft
from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.init.ephemeris import Ephemeris
from src.propagation.cr3bp_propagator import Cr3bpSynodicPropagator
from src.propagation.ephemeris_propagator import EphemerisPropagator
from src.solve.multiple_shooting import MultipleShooting

dirname = os.path.dirname(__file__)


class TestMultipleShooting(unittest.TestCase):

    def test_cr3bp_multiple_shooting_initialization(self):
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
        multi_shoot = MultipleShooting(cr3bp)

        self.assertEqual(multi_shoot.mu_cr3bp, cr3bp.mu)
        self.assertIsInstance(multi_shoot.prop, Cr3bpSynodicPropagator)
        self.assertEqual(multi_shoot.epoch_constr, False)
        self.assertEqual(multi_shoot.precision, dft.multiple_shooting_precision)
        self.assertEqual(multi_shoot.maxiter, dft.multiple_shooting_maxiter)

    def test_ephemeris_multiple_shooting_initialization(self):
        ephemeris = Ephemeris((Primary.EARTH, Primary.MOON, Primary.SUN))
        multi_shoot = MultipleShooting(ephemeris)
        multi_shoot2 = MultipleShooting(ephemeris, epoch_constr=True)

        self.assertEqual(multi_shoot.mu_cr3bp, None)
        self.assertIsInstance(multi_shoot.prop, EphemerisPropagator)
        self.assertEqual(multi_shoot.epoch_constr, False)
        self.assertEqual(multi_shoot.precision, dft.multiple_shooting_precision)
        self.assertEqual(multi_shoot.maxiter, dft.multiple_shooting_maxiter)
        self.assertEqual(multi_shoot2.epoch_constr, True)

    def test_get_jacobian(self):
        pth = os.path.join(dirname, 'data', 'init_jacobian_mat.json')
        with open(pth, 'r') as fid:
            mat_dict = json.load(fid)

        nb_pts = 10  # number of patch points
        nb_add_constr = 3  # number of additional constraints

        # random fixed states components and times masks
        state_mask = np.zeros(6 * nb_pts, dtype=np.int)
        state_mask[np.random.randint(0, state_mask.size, 15)] = 1
        t_mask = np.zeros(nb_pts, dtype=np.int)
        t_mask[np.random.randint(0, t_mask.size, 5)] = 1

        # fixed time case
        _, xf_idx_ft, fx_idx_ft, _, dfx_idx_ft = \
            MultipleShooting.get_indexes(nb_pts, 0, False, False, state_mask=state_mask)
        dfx_ft, dfx_ft_red = MultipleShooting.get_jacobian(nb_pts, 0, False, False, xf_idx_ft,
                                                           fx_idx_ft, dfx_idx_ft)
        _, xf_idx_ft_aug, fx_idx_ft_aug, _, dfx_idx_ft_aug = \
            MultipleShooting.get_indexes(nb_pts, nb_add_constr, False, False,
                                         state_mask=state_mask)
        dfx_ft_aug, dfx_ft_red_aug = \
            MultipleShooting.get_jacobian(nb_pts, nb_add_constr, False, False, xf_idx_ft_aug,
                                          fx_idx_ft_aug, dfx_idx_ft_aug)

        dfx_ft_test = np.asarray(mat_dict['fix_time'])
        dfx_ft_aug_test = np.asarray(mat_dict['fix_time_aug'])
        np.testing.assert_array_equal(dfx_ft, dfx_ft_test)
        np.testing.assert_array_equal(dfx_ft_red.ravel(), dfx_ft_test.take(dfx_idx_ft))
        np.testing.assert_array_equal(dfx_ft_aug, dfx_ft_aug_test)
        np.testing.assert_array_equal(dfx_ft_red_aug.ravel(), dfx_ft_aug_test.take(dfx_idx_ft_aug))

        # variable time case
        _, xf_idx_vt, fx_idx_vt, _, dfx_idx_vt = \
            MultipleShooting.get_indexes(nb_pts, 0, True, False,
                                         state_mask=state_mask, t_mask=t_mask)
        dfx_vt, dfx_vt_red = MultipleShooting.get_jacobian(nb_pts, 0, True, False, xf_idx_vt,
                                                           fx_idx_vt, dfx_idx_vt)
        _, xf_idx_vt_aug, fx_idx_vt_aug, _, dfx_idx_vt_aug = \
            MultipleShooting.get_indexes(nb_pts, nb_add_constr, True, False,
                                         state_mask=state_mask, t_mask=t_mask)
        dfx_vt_aug, dfx_vt_red_aug = \
            MultipleShooting.get_jacobian(nb_pts, nb_add_constr, True, False, xf_idx_vt_aug,
                                          fx_idx_vt_aug, dfx_idx_vt_aug)

        dfx_vt_test = np.asarray(mat_dict['var_time'])
        dfx_vt_aug_test = np.asarray(mat_dict['var_time_aug'])
        np.testing.assert_array_equal(dfx_vt, dfx_vt_test)
        np.testing.assert_array_equal(dfx_vt_red.ravel(), dfx_vt_test.take(dfx_idx_vt))
        np.testing.assert_array_equal(dfx_vt_aug, dfx_vt_aug_test)
        np.testing.assert_array_equal(dfx_vt_red_aug.ravel(), dfx_vt_aug_test.take(dfx_idx_vt_aug))

        # variable time with epoch constraint case
        _, xf_idx_ec, fx_idx_ec, _, dfx_idx_ec = \
            MultipleShooting.get_indexes(nb_pts, 0, True, True,
                                         state_mask=state_mask, t_mask=t_mask)
        dfx_ec, dfx_ec_red = MultipleShooting.get_jacobian(nb_pts, 0, True, True, xf_idx_ec,
                                                           fx_idx_ec, dfx_idx_ec)
        _, xf_idx_ec_aug, fx_idx_ec_aug, _, dfx_idx_ec_aug = \
            MultipleShooting.get_indexes(nb_pts, nb_add_constr, True, True,
                                         state_mask=state_mask, t_mask=t_mask)
        dfx_ec_aug, dfx_ec_red_aug = \
            MultipleShooting.get_jacobian(nb_pts, nb_add_constr, True, True, xf_idx_ec_aug,
                                          fx_idx_ec_aug, dfx_idx_ec_aug)

        dfx_ec_test = np.asarray(mat_dict['eph_constr'])
        dfx_ec_aug_test = np.asarray(mat_dict['eph_constr_aug'])
        np.testing.assert_array_equal(dfx_ec, dfx_ec_test)
        np.testing.assert_array_equal(dfx_ec_red.ravel(), dfx_ec_test.take(dfx_idx_ec))
        np.testing.assert_array_equal(dfx_ec_aug, dfx_ec_aug_test)
        np.testing.assert_array_equal(dfx_ec_red_aug.ravel(), dfx_ec_aug_test.take(dfx_idx_ec_aug))

    def test_warnings(self):
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
        ephemeris = Ephemeris((Primary.EARTH, Primary.MOON, Primary.SUN))
        multi_shoot_eph = MultipleShooting(ephemeris, epoch_constr=True)
        multi_shoot_cr3bp = MultipleShooting(cr3bp)

        # init
        with self.assertRaisesRegex(Exception, 'epoch constraint supported only for '
                                               'ephemeris model'):
            _ = MultipleShooting(cr3bp, epoch_constr=True)
        with self.assertRaisesRegex(Exception, 'model must be a Cr3bp or Ephemeris object'):
            _ = MultipleShooting(Primary.EARTH)

        # correct
        with self.assertRaisesRegex(Exception, 'var_time must be True for a multiple shooter '
                                               'with epoch constraint'):
            multi_shoot_eph.correct(np.zeros(10), np.zeros((10, 6)), var_time=False)

        # grid computation
        with self.assertRaisesRegex(Exception, 'all components of the free variables vector '
                                               'have been fixed'):
            multi_shoot_eph.get_indexes(10, 0, True, False, state_mask=np.ones(60, dtype=np.bool),
                                        t_mask=np.ones(10, dtype=np.bool))
        with self.assertRaisesRegex(Exception, 'state mask and state keys are mutually exclusive '
                                               'keyword arguments'):
            multi_shoot_eph.get_indexes(10, 0, True, True, state_mask=np.ones(10), pos_fix=(0, -1))
        with self.assertRaisesRegex(Exception, 'the provided mask size does not match the target '
                                               'array size'):
            multi_shoot_eph.get_indexes(10, 0, True, False, state_mask=np.ones(70))
        with self.assertRaisesRegex(Exception, 'time mask or time keys can be set only if var_time'
                                               ' is True'):
            multi_shoot_eph.get_indexes(10, 0, False, False, t_mask=np.ones(10))
        with self.assertRaisesRegex(Exception, 'time mask and time keys are mutually exclusive '
                                               'keyword arguments'):
            multi_shoot_eph.get_indexes(10, 0, True, False, t_mask=np.ones(10), t_fix=(0, -1))

        # Jacobi constraint
        with self.assertRaisesRegex(NotImplementedError,
                                    'Jacobi constant constraint supported only for CR3BP model'):
            multi_shoot_eph.get_jacobi_constr(np.zeros((10, 6)), True, multi_shoot_eph.mu_cr3bp)
        self.assertRaises(Exception, multi_shoot_cr3bp.get_jacobi_constr,
                          np.zeros((10, 6)), (0, 3.2, 3.2), multi_shoot_cr3bp.mu_cr3bp)


if __name__ == '__main__':
    unittest.main()
