"""
Unit tests for differential correctors in src.diffcorr.diff_corr_3D.DiffCorr3D

"""

import unittest
from scipy.io import loadmat
import numpy as np
import os.path
from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.solve.diff_corr_3d import DiffCorr3D

dirname = os.path.dirname(__file__)

class TestDiffCorr3D(unittest.TestCase):

    def test_diff_corr_3d_bb(self):
        """Test the differential corrector DiffCorr3D.diff_corr_3d_bb. """

        # test data obtained in SEMAT for an EML1 northern Halo orbit with Az=15000km
        # the initial state is obtained via Richardson approximation
        filename = os.path.join(dirname, 'data', 'test_diff_corr.mat')
        data = loadmat(filename, squeeze_me=True, struct_as_record=False)

        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)  # init CR3BP for EM system
        diff_corr = DiffCorr3D(cr3bp.mu)

        # test x0 fixed
        xi0 = tuple(data['bb_x0'].xi0 - 1)  # indexes in MATLAB start from 1
        xif = tuple(data['bb_x0'].xif - 1)  # indexes in MATLAB start from 1
        state0, t12, t = diff_corr.diff_corr_3d_bb(data['bb_x0'].y0, xi0, xif)

        for i in range(6):
            self.assertAlmostEqual(state0[i], data['bb_x0'].yf[i], places=6)
        self.assertAlmostEqual(t, data['bb_x0'].T, places=6)
        self.assertAlmostEqual(t12, data['bb_x0'].T12, places=6)

        # test z0 fixed
        xi0 = tuple(data['bb_z0'].xi0 - 1)  # indexes in MATLAB start from 1
        xif = tuple(data['bb_z0'].xif - 1)  # indexes in MATLAB start from 1
        state0, t12, t = diff_corr.diff_corr_3d_bb(data['bb_z0'].y0, xi0, xif)

        for i in range(6):
            self.assertAlmostEqual(state0[i], data['bb_z0'].yf[i], places=6)
        self.assertAlmostEqual(t, data['bb_z0'].T, places=6)
        self.assertAlmostEqual(t12, data['bb_z0'].T12, places=6)

    def test_diff_corr_3d_full(self):
        """Test the differential corrector DiffCorr3D.diff_corr_3d_full. """

        # test data obtained in SEMAT for an EML1 northern Halo orbit with Az=15000km
        # the initial state is obtained via Richardson approximation
        filename = os.path.join(dirname, 'data', 'test_diff_corr.mat')
        data = loadmat(filename, squeeze_me=True, struct_as_record=False)

        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)  # init CR3BP for EM system
        diff_corr = DiffCorr3D(cr3bp.mu)

        state0, t12, t, g_vec, null_vec = diff_corr.diff_corr_3d_full(data['full'].y0)

        for i in range(6):
            self.assertAlmostEqual(state0[i], data['full'].yf[i], places=6)
        self.assertAlmostEqual(t, data['full'].T, places=6)
        self.assertAlmostEqual(t12, data['full'].T12, places=6)
        for i in range(4):
            self.assertAlmostEqual(g_vec[i, 0], data['full'].gv[i], places=6)
            self.assertAlmostEqual(null_vec[i, 0], data['full'].nv[i], places=6)

    def test_diff_corr_3d_cont(self):
        """Test the differential corrector DiffCorr3D.diff_corr_3d_cont. """

        # test data obtained in SEMAT for an EML1 northern Halo orbit with Az=15000km
        # the initial state is obtained via Richardson approximation
        filename = os.path.join(dirname, 'data', 'test_diff_corr.mat')
        data = loadmat(filename, squeeze_me=True, struct_as_record=False)
        delta_s = 0.05  # pseudo-arclength step size

        # initial free-variables and null vectors are taken from 'full' case
        g_vec0 = np.reshape(data['cont'].gv0, (4, 1))
        n_vec0 = np.reshape(data['cont'].nv0, (4, 1))

        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)  # init CR3BP for EM system
        diff_corr = DiffCorr3D(cr3bp.mu)

        state0, t12, t, g_vec, null_vec = \
            diff_corr.diff_corr_3d_cont(data['cont'].y0, data['cont'].T120, delta_s,
                                        g_vec0, n_vec0)

        for i in range(6):
            self.assertAlmostEqual(state0[i], data['cont'].yf[i], places=6)
        self.assertAlmostEqual(t, data['cont'].T, places=6)
        self.assertAlmostEqual(t12, data['cont'].T12, places=6)
        for i in range(4):
            self.assertAlmostEqual(g_vec[i, 0], data['cont'].gv[i], places=6)
            self.assertAlmostEqual(null_vec[i, 0], data['cont'].nv[i], places=6)


if __name__ == '__main__':
    unittest.main()
