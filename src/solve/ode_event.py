# -*- coding: utf-8 -*-
"""
This module contains the events to perform differential correction.

These events are coded in a format that the scipy numerical ode integrator, solve_ivp, accepts.

"""


def xz_plane_event(_, state):
    """Type of event: reaching the y = 0 plane."""
    return state[1]


xz_plane_event.terminal = True
xz_plane_event.direction = -1
