import inspect
import numpy as np
from typing import Callable, List

class Constraints:
    def __init__(self, constraints_list: List[Callable]):
        """Initialize the constraints."""

        # Check if the jac_flag is present in all the constraints
        for c in constraints_list:
            if 'jac_flag' not in inspect.signature(c).parameters:
                raise ValueError('The constraints must have a parameter called jac_flag.')

        self._constraints_list = constraints_list
        self.names = [c.__name__ for c in constraints_list]
        self.constraints_fstack = self._stack_constraint_functions()

    def _stack_constraint_functions(self):
        def constraints_fstack(variables, jac_flag=False):
            res_stack = np.empty(0)
            if jac_flag:
                jac_stack = np.ndarray((0,len(variables)))
                for c in self._constraints_list:
                    res, jac = c(variables, jac_flag)

                    res_stack = np.append(res_stack, res)
                    jac_stack = np.vstack((jac_stack, jac))

                out_stack = (res_stack, jac_stack)
            else:
                for c in self._constraints_list:
                    res_stack = np.append(res_stack, c(variables, jac_flag))

                out_stack = res_stack
            return out_stack
        return constraints_fstack
