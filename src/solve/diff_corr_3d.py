# pylint: disable=too-many-locals, line-too-long
"""
Differential correction procedure for CR3BP orbits symmetric wrt the xz plane.

"""

import numpy as np
import scipy.linalg as sl

import src.init.defaults as dft
from src.propagation.cr3bp_propagator import Cr3bpSynodicPropagator
from src.solve.ode_event import xz_plane_event

try:
    from src.dynamics.libs.cr3bp_dynamics import eqm_6_synodic
except ImportError:
    try:
        from src.dynamics.cr3bp_dynamics_jit import eqm_6_synodic
    except ImportError:
        print('Numba module not found!')
        from src.dynamics.cr3bp_dynamics import eqm_6_synodic


class DiffCorr3D:
    """DiffCorr3D class implements three differential correction procedures to compute 3D periodic
    orbits in the Circular Restricted Three-Body Problem symmetric with respect to the xz plane.

    Available differential correction methods are the followings:
    1) diff_corr_3D_bb, a 2 degrees of freedom procedure where either the x or the z component of
    the initial state is corrected together with the vy component to target the successive xz plane
    crossing with a velocity vector perpendicular to the plane itself (i.e. vx=0 and vz=0).
    2) diff_corr_3D_full, a 3 DOF procedure where the x, z and vy components of the initial state
    are corrected together with the orbit's half period T12 to target null y, vx and vz components
    of the state itself after a propagation carried out in the time interval [0, T12].
    3) diff_corr_3D_cont, a 3 DOF procedure similar to `diff_corr_3D_full` in which the same free
    variables vector and final constraints are imposed. This implementation is employed within a
    continuation procedure to generate the next periodic orbit of a given family starting from an
    already known member. The goal is achieved enforcing an additional constraint that guarantee
    the resulting orbit being perpendicular to the direction along which the family evolves.

    Parameters
    ----------
    mu_cr3bp : float
        CR3BP mass parameter.
    precision : float, optional
        Precision at which the differential correction procedure is terminated. Default is given by
        `src.defaults.single_shooting_precision`.
    maxiter : int, optional
        Maximum number of iterations performed by the targeting scheme. Default is given by
        `src.defaults.single_shooting_maxiter`.
    method : str, optional
        Integration method. Default is `src.init.defaults.method`.
    rtol : float, optional
        Relative tolerance for integration routines. Default is `src.init.defaults.rtol`.
    atol : float, optional
        Absolute tolerance for integration routines. Default is `src.init.defaults.atol`.
    max_internal_steps : int, optional
        Maximum number of (internally defined) steps allowed for each integration point. This
        option is used only if `events` is ``None``. Default defined in `src.init.defaults`.

    Attributes
    ----------
    mu_cr3bp : float
        CR3BP mass parameter.
    precision : float
        Precision at which the differential correction procedure is terminated.
    maxiter : int
        Maximum number of iterations performed by the targeting scheme.
    propagator : Cr3bpSynodicPropagator
        A `Cr3bpSynodicPropagator` object.
    max_internal_steps : int
        Maximum number of (internally defined) steps allowed for each integration point. This
        option is used only if `events` is ``None``.

    """

    def __init__(self, mu_cr3bp, precision=dft.single_shooting_precision,
                 maxiter=dft.single_shooting_maxiter, method=dft.method, rtol=dft.rtol,
                 atol=dft.atol, max_internal_steps=dft.max_internal_steps):
        """Inits DiffCorr3D class. """

        self.mu_cr3bp = mu_cr3bp
        self.precision = precision
        self.maxiter = maxiter
        self.propagator = Cr3bpSynodicPropagator(mu_cr3bp, with_stm=True, method=method,
                                                 rtol=rtol, atol=atol, time_steps=None,
                                                 max_internal_steps=max_internal_steps)

    @staticmethod
    def find_index(i, j):
        """Map indexes from 6x6 STM matrix to 42-dim state concatenated with flattened STM. """
        return 6 * (i + 1) + j

    def diff_corr_3d_bb(self, state0, xi0, xif):
        """diff_corr_3d_bb method implements a differential correction procedure to compute 3D
        periodic orbits in the CR3BP symmetric with respect to the xz plane.

        It uses an iterative Newton method to progressively correct the initial state until the
        imposed constraints are satisfied within the selected numerical accuracy. The method is
        illustrated below for the Halo orbit case.

        At each step, the free variables vector is X0=[x0 vy0]^T or X0=[z0 vy0]^T, i.e. the x0 or
        z0 component of the initial state is corrected together with the vy0 component of the
        velocity. The choice between x0 or z0 fixed is left to the user and specified with the
        `xi0` input parameter: if xi0=(0, 4) then x0 and vy0 are corrected, if xi0=(2, 4) z0 and
        vy0 are set as free variables instead.

        The constraint vector is F(X)=[y vx vz]^T and the goal is to satisfy F(X)=0 at the xz
        plane crossing so as to obtain an orbit symmetric w.r.t. the former plane.

        Starting from an initial state [x0 y0 z0 vx0 vy0 vz0]^T the equations of motions are
        integrated until the xz plane is reached so that the first constraint (y=0) is
        automatically satisfied.

        The Newton correction dX0 is then applied to satisfy the new constraint FXr=[vx vz]^T=0
        specified with the input parameter xif=(3, 5) where 3, 5 correspond to the indexes of the
        x and z components of the velocity in the 6-dimensional state.

        The first-order correction is subject to FXr = (Af - ppf * Bf) * dX0 where the matrices
        Af, Bf and the vector ppf are assembled with the components of the STM numerically
        integrated along with the state and the first time derivative of the state itself computed
        at t=t_event (i.e. xz plane crossing).

        The vectors X0 and f_xr may differ from the ones written here for
        other types of orbits (e.g. vertical lyapunov orbits).

        For details see Howell 1984, Three-Dimensional Periodic Halo Orbits
        (http://adsabs.harvard.edu/full/1984CeMec..32...53H).

        Parameters
        ----------
        state0 : ndarray
            Orbit's initial state (to be corrected).
        xi0 : tuple
            To select what dimensions are going to be corrected.
        xif : tuple
            To select the desired target.

        Returns
        -------
        state0 : ndarray
            Corrected orbit's initial state.
        t_event : float
            Time at which the xz plane crossing occurs at algorithm convergence, i.e. corrected
            orbit's half-period T12.
        2 * t_event : float
            Corrected orbit's period.

        """

        # initialization
        af_mat = np.zeros((2, 2))  # 2x2 Af matrix
        bf_mat = np.zeros(2)  # 1x2 Bf matrix
        ppf_vec = np.zeros((2, 1))  # 2x1 ppf vector
        t_event = [np.empty(1)]
        target = 1  # y=0 is targeted
        count = 0  # iteration counter

        while True:
            if count > self.maxiter:
                print('Maximum iterations reached in differential correction')
                break

            # integration up to y=0, i.e. until xz_plane_event is reached
            _, _, t_event, state_event = \
                self.propagator.propagate([0, 10], state0, events=xz_plane_event)
            state_event = state_event[0][0]

            # derivative of state_event at y=0 (t=t_event)
            dot_state_event = eqm_6_synodic(0.0, state_event[0:6], self.mu_cr3bp)

            # update the final state FXr = [vx vz]^T.
            fxr_vec = np.array([[state_event[xif[0]]], [state_event[xif[1]]]])

            # compute the first order correction via Newton's method
            for i in range(2):
                for j in range(2):
                    af_mat[i][j] = state_event[self.find_index(xif[i], xi0[j])]
                    bf_mat[j] = state_event[self.find_index(target, xi0[j])]
                    ppf_vec[i, 0] = dot_state_event[xif[i]] / state_event[target + 3]
            af_mat -= ppf_vec * bf_mat  # update Af matrix

            if np.linalg.norm(fxr_vec) < self.precision:  # stops if precision is good enough
                break

            # first order correction computed as dX0 = inv(Af)*FXr
            dx0_vec = np.linalg.solve(af_mat, fxr_vec)

            # update initial state
            for i in range(2):
                state0[xi0[i]] -= dx0_vec[i, 0]
            count += 1

        return state0, t_event[0][0], 2 * t_event[0][0]

    def diff_corr_3d_full(self, state0, t12=None):
        """diff_corr_3d_full method implements a differential correction procedure to compute 3D
        periodic orbits in the CR3BP symmetric with respect to the xz plane.

        The algorithm corrects the initial state via an iterative Newton method until the imposed
        constraints are satisfied within a given numerical accuracy.

        The free variables vector to be corrected is defined as X0=[x0 z0 vy0 T12]^T where the
        first three components are the x, z and vy components of the initial orbit state and T12 is
        the orbit's half-period.

        The final constraint to be met at t=T12 is F(X)=[y vx vz]^T=0 so as to obtain an orbit
        symmetric w.r.t. the xz plane.

        The correction dX0 to be applied to the initial state is subject to F(X)=DF(X)*dX0 with
        DF(X) Jacobian of F(X) w.r.t. X0 defined by DF(X)=dF(X)/dX0. Its components are obtained
        from the STM numerically integrated in [0 T12] along with the state and the first-time
        derivative of the state at t=T12.

        Being X0 4-dimensional and F(X) 3-dimensional, the system is under-determined thus
        admitting more than one solution. In this case a minimum-norm solution is chosen thus
        computing dX0 as follows:

        dX0 = DF(X)^T * (inv(DF(X)) * DF(X)^T) * F(X)

        Once convergence is achieved, the updated initial state and orbit's period are returned
        together with the free variables vector and the null vector of the Jacobian to be used
        for a potential continuation procedure.

        For details, see Pavlak 2013, section 3.3
        (https://engineering.purdue.edu/people/kathleen.howell.1/Publications/Dissertations/2013_Pavlak.pdf).

        Parameters
        ----------
        state0 : ndarray
            Orbit's initial state (to be corrected).
        t12 : float
            Orbit's half-period (to be corrected).

        Returns
        -------
        state0 : ndarray
            Corrected orbit's initial state.
        t12 : float
            Corrected orbit's half-period.
        2 * t12 : float
            Corrected orbit's period.
        g_vec : ndarray
            Final vector of free variables (for potential continuation).
        null_vec : ndarray
            Null vector of the Jacobian (for potential continuation).

        """

        # initialization
        dfx_mat = np.zeros((3, 4))
        if t12 is None:  # compute an estimate of the orbit's half-period if not provided
            _, _, t12, _ = self.propagator.propagate([0, 10], state0, events=xz_plane_event)
            t12 = t12[0][0]
        count = 0  # iteration counter

        while True:
            if count > self.maxiter:
                print('Maximum iterations reached in differential correction')
                break

            # integration for half period (stops at t=t12)
            t_vec, state_vec, _, _ = self.propagator.propagate([0.0, t12], state0)

            # derivatives of state at t12
            dot_state_end = eqm_6_synodic(t_vec[-1], state_vec[-1, 0:6], self.mu_cr3bp)

            # update the final state FX = [y vx vz]^T
            fx_vec = np.array([[state_vec[-1, 1]], [state_vec[-1, 3]], [state_vec[-1, 5]]])

            # compute the first-order correction via Newton method
            for i in range(3):
                for j in range(3):
                    dfx_mat[i, j] = state_vec[-1, self.find_index((2 * i + 1), 2 * j)]
                    dfx_mat[i, 3] = dot_state_end[(2 * i + 1)]  # last column given by [vy ax az]^T

            if np.linalg.norm(fx_vec) < self.precision:  # stops if precision is good enough
                break

            # compute the first-order correction as the minimum norm solution
            dfx_t = dfx_mat.T
            dx0_vec = dfx_t.dot(np.linalg.solve(dfx_mat.dot(dfx_t), fx_vec))

            # update initial state
            for i in range(3):
                state0[2 * i] -= dx0_vec[i, 0]
            t12 -= dx0_vec[3, 0]
            count += 1

        # final vector of free variables and null vector of the Jacobian for potential continuation
        g_vec = np.array([[state0[0]], [state0[2]], [state0[4]], [t12]])
        null_vec = - sl.null_space(dfx_mat)

        return state0, t12, 2 * t12, g_vec, null_vec

    def diff_corr_3d_cont(self, state0, t12, delta_s, g_vec, null_vec):
        """diff_corr_3d_cont method implements a differential correction procedure to compute 3D
        periodic orbits in the CR3BP symmetric with respect to the xz plane. This implementation
        includes a pseudo arclength continuation module.

        The algorithm corrects the initial state via an iterative Newton method until the imposed
        constraints are satisfied within a given numerical accuracy.

        The free variables vector to be corrected is defined as X0=[x0 z0 vy0 T12]^T where the
        first three components are the x, z and vy components of the initial orbit state and T12
        is the orbit's half-period.

        The final constraints to be met at t=T12 are F(X)=[y vx vz]^T=0 and an additional pseudo
        arclength constraints formulated as follows.

        Supposing the solution from a previous iteration being known and denoting with X0i, DX0i
        the corresponding corrected state and null vector of the Jacobian DF(X)=dF(X)/dX0, the
        last constraint is written as (X0 - X0i)^T * DX0i - ds = 0 with ds user-defined arclength
        step size.

        An augmented constraint vector G(X) is thus defined as
        G(X) = [F(X) ((X0 - X0i)^T * DX0i - ds)]^T = 0 while the associated Jacobian becomes
        DG(X) = [DF(X) DX0i^T]^T.

        The correction dX0 to be applied to the initial state is subject to dX0=inv(DG(X))*G(X)
        with DG(X) defined above. Its components are obtained from the STM numerically integrated
        in [0 T12] along with the state and the first-time derivative of the state at t=T12.

        Once convergence is achieved, the updated initial state and orbit's period are returned
        together with the free variables vector and the null vector of the Jacobian to be used
        for a potential continuation procedure.

        The updated null vector is forced to be in the same direction of its previous instance
        (positive inner product) to avoid potential U-turns in the family.

        For details, see Pavlak 2013, section 3.3
        (https://engineering.purdue.edu/people/kathleen.howell.1/Publications/Dissertations/2013_Pavlak.pdf).

        Parameters
        ----------
        state0 : ndarray
            Orbit's initial state (to be corrected).
        t12 : float
            Orbit's half-period (to be corrected).
        delta_s : float
            Pseudo arclength step size.
        g_vec : ndarray
            Vector of free variables (to be corrected).
        null_vec : ndarray
            Null vector of the Jacobian (to be corrected).

        Returns
        -------
        state0 : ndarray
            Corrected orbit's initial state.
        t12 : float
            Corrected orbit's half-period.
        2 * t12 : float
            Corrected orbit's period.
        g_vec : ndarray
            Final vector of free variables (for potential continuation).
        null_vec : ndarray
            Null vector of the Jacobian (for potential continuation).

        """

        # initialization
        dgx_mat = np.zeros((4, 4))
        count = 0  # iteration counter

        while True:
            if count > self.maxiter:
                print('Maximum iterations reached in differential correction')
                break

            # update initial state vector X0 = [x0 z0 vy0 T12]^T
            x0_vec = np.array([[state0[0]], [state0[2]], [state0[4]], [t12]])

            # integration for half period (stops at t=t12)
            t_vec, state_vec, _, _ = self.propagator.propagate([0.0, t12], state0)

            # derivatives of state at t12
            dot_state_end = eqm_6_synodic(t_vec[-1], state_vec[-1, 0:6], self.mu_cr3bp)

            # update the final vector GX = [y vx vz ((X0 - X0i)^T DX0i - ds)]^T where the last
            # component is the pseudo-arclength constraint
            gx_vec = np.array([[state_vec[-1, 1]], [state_vec[-1, 3]], [state_vec[-1, 5]],
                               [(x0_vec - g_vec).T.dot(null_vec)[0, 0] - delta_s]])

            # compute the first-order correction via Newton method
            for i in range(3):
                for j in range(3):
                    dgx_mat[i, j] = state_vec[-1, self.find_index((2*i + 1), 2*j)]
                    dgx_mat[i, 3] = dot_state_end[(2 * i + 1)]  # last column given by [vy ax az]^T
            dgx_mat[-1, :] = np.transpose(null_vec)  # last line as transpose of the null vector

            if np.linalg.norm(gx_vec) < self.precision:  # stops if precision is good enough
                break

            # apply first-order correction
            dx0_vec = np.linalg.solve(dgx_mat, gx_vec)
            for i in range(3):
                state0[2 * i] -= dx0_vec[i, 0]
            t12 -= dx0_vec[3, 0]
            count += 1

        # null vector of Jacobian for next step of continuation
        # it is forced to have the same direction as its previous instance (positive dot product)
        null_vec_old_t = null_vec.T
        null_vec = sl.null_space(dgx_mat[0:3, :])
        null_vec *= np.sign(null_vec_old_t.dot(null_vec))

        # final vector of free variables for next step of continuation
        g_vec = np.array([[state0[0]], [state0[2]], [state0[4]], [t12]])

        return state0, t12, 2 * t12, g_vec, null_vec
