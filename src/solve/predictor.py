# %% Import modules
from abc import ABC, abstractmethod
import numpy as np
from dataclasses import dataclass

# %% Generic Class

class AbstractPredictor(ABC):

    @abstractmethod
    def initialize():
        pass

    @abstractmethod
    def predict():
        pass

    @abstractmethod
    def success_params_update():
        pass

    @abstractmethod
    def fail_params_update():
        pass

    @abstractmethod
    def check_prediction_success():
        pass


# %% Polynomial continuation functions


def _poly_coeff(t: np.ndarray, S: np.ndarray):

    # Compute array of polynomial steps and powers
    steps_mat = t[:, np.newaxis]**list(reversed(range(len(t))))

    # Extract polinomial coefficients
    cf = np.linalg.inv(steps_mat) @ S
    
    return cf


def _poly_predict(t_full: np.ndarray, S_full: np.ndarray, ds: float, n_order: int):

    # TODO: add error check on state array inpit (it must be longer than the degree of the polynomial requested)
    # Trim arrays of curv abs and states according to polynomial order
    t = t_full[-1-n_order:]
    S = S_full[-1-n_order:, :]

    # Compute new polynomial coefficents
    cf = _poly_coeff(t - t[-1], S)

    # Compute predicted new state (nth order)
    order_list = reversed(range(cf.shape[0]))
    
    Sp = np.array([ds**n for n in order_list]) @ cf

    if cf.shape[0] > 2:
        # Compute first order prediciton
        cf_1d = _poly_coeff(t[-2:] - t[-1], S[-2:,:])
        Sp_1d = np.array([ds**n for n in reversed(range(2))]) @ cf_1d  # (ds**[1, 0])*cf_1d

        # Check continuation direction
        Sp_dir = (Sp-S[-1,:])/np.linalg.norm(Sp-S[-1,:])
        Sp_1d_dir = (Sp_1d-S[-1,:])/np.linalg.norm(Sp_1d-S[-1,:])
        check_dir = np.dot(Sp_dir, Sp_1d_dir)

        while check_dir <= 0 and cf.shape[0] > 2:
            
            # Remove oldest state and curviliner abscissa
            t, S = t[1:], S[1:, :]

            # Compute new polynomial coefficents
            cf = _poly_coeff(t - t[-1], S)

            # Compute predicted new state (nth order)
            order_list = reversed(range(cf.shape[0]))
            Sp = np.array([ds**n for n in order_list]) @ cf
            
            
            # Check continuation direction
            Sp_dir = (Sp-S[-1,:])/np.linalg.norm(Sp-S[-1,:])
            Sp_1d_dir = (Sp_1d-S[-1,:])/np.linalg.norm(Sp_1d-S[-1,:])
            check_dir = np.dot(Sp_dir, Sp_1d_dir)

    return Sp

class PolynomialPredictor(AbstractPredictor):

    def __init__(self,
                 pred_tol: float = 1e-6,
                 init_step: float = 1,
                 step_update_ratio: float = 1.3,
                 max_degree: int = 10,
                 min_step: float = 1e-12,
                 max_step: float = 1e3) -> None:

        # Initialize states stack
        self.states_stack = None

        # Initialize polynomial
        self.poly_step = init_step
        self.poly_absc = None
        self.poly_degree = None

        # Setup prediction parameters
        self.step_update_ratio = step_update_ratio
        self.max_degree = max_degree
        self.min_step = min_step
        self.max_step = max_step
        self.pred_tol = pred_tol

        # # Compute new prediction
        self.state_predicted = None


    def initialize(self, new_states: np.ndarray) -> np.ndarray:

        self.poly_degree = new_states.shape[0] - 1
        self.poly_absc = np.arange(0,
                                    new_states.shape[0]*self.poly_step,
                                    self.poly_step)
        self.states_stack = new_states

    def predict(self, poly_step = None, poly_degree = None): # TODO: find a better way to deal with temporary self arguments override

        if poly_step is None:
            poly_step = self.poly_step

        if poly_degree is None:
            poly_degree = self.poly_degree

        self.state_predicted = _poly_predict(self.poly_absc, self.states_stack,
                                             poly_step, poly_degree)

        return self.state_predicted

    def check_prediction_success(self, new_state: np.ndarray):
        
        N_nodes = new_state.size // 6
        # pred_err = max(abs(new_state[:-1] - self.state_predicted[:-1]))
        pred_err = max(abs(new_state[:6*N_nodes+1] - self.state_predicted[:6*N_nodes+1]))

        if pred_err < self.pred_tol:
            return True, pred_err
        return False, pred_err

    def success_params_update(self, new_state: np.ndarray):

        pred_success, pred_err = self.check_prediction_success(new_state)

        if pred_success:

            # Reduce polynomial degree
            new_degree = self._reduce_poly_degree(new_state, pred_err)

            # Update step
            new_step = self._update_step(pred_err)

            # Increase polynomial degree
            if new_degree == self.poly_degree and\
               self.poly_degree < self.max_degree and\
               new_step == self.poly_step:
                new_degree = self._increase_poly_degree(new_state, pred_err) # FIXME: Test on multiple degree increment
            
            # Append previously corrected state
            self.states_stack = np.vstack((self.states_stack, new_state))  # FIXME: do not store all the stack, but dynamically remove oldest rows depending on the new polynomial degree
            self.poly_absc = np.append(self.poly_absc,
                                       self.poly_absc[-1] + self.poly_step)

        else:  # TODO: Now if prediction error is larger than tolerance, the new state is not accepted even if a valid solution -> accept the new solution anyways
            
            # Throw error if step is already at minimum and prediction failed
            if self.poly_step == self.min_step:
                raise ValueError("Predcition failed with minimum allowed step.")

            print('Failed prediction')

            new_step = np.max([self._update_step(pred_err), self.min_step]) # TODO: check if it is worth it putting the np.max() into the previous fucntion or keep it outside
            new_degree = self.poly_degree
        
        #  Update step value and polynomial degree
        self.poly_step = new_step
        self.poly_degree = new_degree


    def fail_params_update(self):
        # Throw error if step is already at minimum and prediction failed
        if self.poly_step == self.min_step:
            raise ValueError("Predcition failed with minimum allowed step.")

        print('Failed correction')
        self.poly_step /= 2

    def _reduce_poly_degree(self, new_state, pred_err):
        
        degree_copy = self.poly_degree

        # Degree reduction
        if degree_copy > 1:
            new_pred_state = self.predict(self.poly_step, degree_copy - 1)
            new_err = np.linalg.norm(new_state - new_pred_state)

            while degree_copy > 1 and new_err < pred_err:
                degree_copy -= 1
                pred_err = new_err

                new_pred_state = self.predict(self.poly_step, degree_copy)
                new_err = np.linalg.norm(new_state - new_pred_state)
        
        return degree_copy

    def _update_step(self, pred_err):
        
        step_copy = self.poly_step

        # Step increase (successful prediction)
        if pred_err <= 0.5*self.pred_tol:
            step_copy *= min([self.step_update_ratio, self.max_step])

        elif pred_err >= self.pred_tol:
            # Compute tolerance excedence ratio
            tol_ratio = self.pred_tol/pred_err

            # Saturate the ratio between acceptable bounds
            saturated_ratio = np.min([0.9, np.max([0.5, tol_ratio])])

            # Update step
            step_copy *= saturated_ratio

        return step_copy

    def _increase_poly_degree(self, new_state, pred_err):
        
        degree_copy = self.poly_degree

        new_pred_state = self.predict(self.poly_step, degree_copy + 1)
        new_err = np.linalg.norm(new_state - new_pred_state)

        if new_err < pred_err:
            degree_copy += 1
        
        return degree_copy

# %% Pseudoarclength continaution

def _pseudoarc_predict():
    pass


class PseudoArcPredictor(AbstractPredictor):

    def __init__(self) -> None:
        super().__init__()

# %% Natural parameter continuation

def _natpar_predict():
    pass


class NatParPredictor(AbstractPredictor):

    def __init__(self) -> None:
        super().__init__()


# %% Predictor Selector

@dataclass
class Predictor:
    polynomial = PolynomialPredictor
    pseudo_arclength = PseudoArcPredictor
    natural_parameter = NatParPredictor