import scipy.optimize as scopt
import numpy as np

import src.init.defaults as dft

from src.solve.constraints import Constraints

class Corrector:
    """Differential corrector.

    This class is a wrapper of Scipy's root finding algorithms. This class is used for problems of
    the form:
        find x such that f(x) = 0

    Attributes
    ----------
    constraints_fstack : Callable
        Depending on the value of jac_flag, return residuals or a tuple of residuals and jacobian.
    method : str
        Root finding method.
    tol : float
        Tolerance.
    scipy_options : dict
        Additional options to be passed to `scipy.optimize.root()`.
    dim_constr : int
        Dimensionality of the constraints.
    dim_guess : int
        Dimensionality of the guess.
    """
    def __init__(self, constraints: Constraints,
                 method: str = dft.correction_method,
                 tol: float = dft.correction_tolerance,
                 scipy_options: dict = None,
                 verbose: bool = False):
        """
        Parameters
        ----------
        constraints :
            Constraints object.
        method : str, optional
            Root finding method.
        tol : float, optional
            Tolerance.
        scipy_options : dict, optional
            Additional options to be passed to `scipy.optimize.root()`.
        """
        self._constraints = constraints
        self.constraints_fstack = None
        self.jac_flag = False
        self.method = method
        self.tol = tol
        self.scipy_options = scipy_options
        self.verbose = verbose

        # Initialize attributes
        self.dim_constr, self.dim_guess = None, None

    @property
    def jac_flag(self):
        return self._jac_flag

    @jac_flag.setter
    def jac_flag(self, value: bool):
        self._jac_flag = value
        self.constraints_fstack = self._decorate_constraints_function_with_jac_flag()

    def _decorate_constraints_function_with_jac_flag(self):
        """Decorates the constraints function with the Jacobian flag."""
        def constraints_fstack(variables):
            return self._constraints.constraints_fstack(variables, self.jac_flag)
        return constraints_fstack

    @staticmethod
    def _check_dimensionality(guess, residuals, verbose):
        """Check the dimensionality of the problem, raising error if underdefined."""
        dim_constr = len(residuals(guess))
        dim_guess = len(guess)

        if dim_constr < dim_guess:
            raise ValueError(f'The problem is under-constrained ({dim_guess} variables and '
                             f'{dim_constr} constraints).')
        elif dim_constr > dim_guess and verbose:
            print(f'Warning: the problem is over-constrained ({dim_guess} variables and '
                  f'{dim_constr} constraints).')
        return dim_constr, dim_guess

    def _correct(self, guess):
        """Correct the guess with the scipy root method.

        Parameters
        ----------
        guess : np.ndarray
            Initial guess.

        Returns
        -------
        OptimizeResult
            Corrected Result object.
        """

        # Run root finding algorithm
        return scopt.root(self.constraints_fstack, guess,
                         method=self.method,
                         jac=self.jac_flag, tol=self.tol,
                         options=self.scipy_options)
        # TODO add error recognition if problem is over-constrained for certain methods

    def __call__(self, guess: np.ndarray, jac_flag: bool=False):

        # Check dimensionality of the problem
        self.dim_constr, self.dim_guess = \
            self._check_dimensionality(guess, self._constraints.constraints_fstack, self.verbose)

        # Update jac_flag if input different from current property value
        if self.jac_flag != jac_flag:
            self.jac_flag = jac_flag

        # Run correction
        sol = self._correct(guess)

        # Check if solution is valid
        # if not sol.success:
        #     raise RuntimeError(f'Root finding algorithm failed: {sol.message}')
        return sol