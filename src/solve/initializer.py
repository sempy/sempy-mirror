from dataclasses import dataclass
from typing import Callable, List, Tuple
import numpy as np
from src.propagation.propagator import Propagator
from src.dynamics.dynamical_environment import DynamicalEnvironment

def _lagrange_point_init(state_lp: np.ndarray, f_matrix_function: Callable,
                         direction: str = 'planar', delta: float = -1e-6,
                         N_nodes: int = 1)\
                         -> Tuple[List]:
    """ Initializer for lagrange points"""

    # Compute  linearized matrix
    F = f_matrix_function(state_lp) 

    # Extract center eigenspace
    eigspace = np.linalg.eig(F)

    # Separate eigenvalues from eigenvectors
    eigvals = eigspace[0]
    eigvecs = eigspace[1]

    # Find ans extract the central eigenspace
    center_index = [i for i in range(len(eigvals))
                    if np.imag(eigvals[i]) != 0]

    eigvals_center = eigvals[center_index]
    eigvecs_center = eigvecs[:, center_index]

    # Set angle for multiple nodes
    if N_nodes < 1:
        raise ValueError("The number of nodes must be a positive integer.")

    if N_nodes == 1:
        theta_vec = [0]
    else:
        theta_vec = np.linspace(0, 2*np.pi, N_nodes+1)[:-1]

    # Generate state continuation direction
    if direction == "planar":  # Planar continuation

        # Extract all direction in the planar eigenspace
        ds_vec = [np.real(np.cos(theta_vec)[:, np.newaxis] *
                      eigvecs_center[:, 2*i]) -
                  np.imag(np.sin(theta_vec)[:, np.newaxis] *
                      eigvecs_center[:, 2*i])
                  for i in range(len(eigvals_center)//2)
                  if abs(eigvecs_center[5, 2*i]) <= 1e-16]

        # Retrieve the corresponding periods
        tau_vec = [2*np.pi/abs(eigvals_center[2*i]) for i
                   in range(len(eigvals_center)//2)
                   if abs(eigvecs_center[5, 2*i]) <= 1e-16]

    elif direction == "vertical":  # Vertical continuation

        # Extract all direction in the planar eigenspace
        ds_vec = [np.real(np.cos(theta_vec)[:, np.newaxis] *
                      eigvecs_center[:, 2*i]) -
                  np.imag(np.sin(theta_vec)[:, np.newaxis] *
                      eigvecs_center[:, 2*i])
                  for i in range(len(eigvals_center)//2)
                  if abs(eigvecs_center[5, 2*i]) > 1e-16]

        # Retrieve the corresponding periods
        tau_vec = [2*np.pi/abs(eigvals_center[2*i]) for i
                   in range(len(eigvals_center)//2)
                   if abs(eigvecs_center[5, 2*i]) > 1e-16]
    else:
        raise KeyError("Incorrect string for 'dir' input.\
                        Please select either  'planar' or  'vertical' ")

    # Normalize direction vectors
    ds_vec = [ds_vec[i]/np.linalg.norm(ds_vec[i])
              for i in range(len(ds_vec))]

    # Create initial arrays for every center manifold couple
    s_guess = []
    for i in range(len(tau_vec)):

        # State LP repeated (for mutiple shooting)
        state_lp_rep = np.tile(state_lp, N_nodes)

        # Extend origin state with guessed period
        s_origin = np.append(state_lp_rep, tau_vec[i])

        # Extend guessed new state with guessed period
        s_perturbed = np.append(state_lp_rep + delta*np.ravel(ds_vec[i]), tau_vec[i])

        # Merge the two extended states in the initial guesses list
        s_guess.append(np.vstack((s_origin, s_perturbed)))


    # # Compute new state guess
    # s_guess = [np.append(state_lp + delta*ds, tau) for ds, tau
    #                in zip(ds_vec, tau_vec)]

    # # Create s_origin
    # s_origin = [np.append(state_lp, tau) for tau in tau_vec]

    return s_guess #, s_origin, ds_vec


def _qp_orbits_init(s_origin: np.ndarray, env: DynamicalEnvironment, prop: Propagator,
                    N_nodes: int, delta: float = 1e-6):
    """Initializer for quasi-periodic orbits"""

    # TODO: add check: the input orbit must be periodic!!

    # Extract state and time
    x0_p = s_origin[0:-1]
    tau_p = s_origin[-1]

    # Propagate one orbit period
    x0_p_ext = np.append(x0_p, np.eye(6).ravel())

    # Propagate the dynamics
    sol_prop = prop(env, [0, tau_p], x0_p_ext, with_stm=True)

    # Extract state transition matrix
    stm = sol_prop.state_vec[-1, 6:].reshape(6, 6)

    # Extract center eigenvalues/eigenvectors
    eigspace = np.linalg.eig(stm)

    eigvals = eigspace[0]
    eigvecs = eigspace[1]

    center_index = [i for i in range(len(eigvals))
                    if not eigvals[i].imag == 0]

    eigvals_center = eigvals[center_index]
    eigvecs_center = eigvecs[:, center_index]

    # Sample the phase angle along the stroboscopic map
    theta_vec = np.arange(0, 2*np.pi, 2*np.pi/N_nodes)

    # Create the states perturbation vector
    ds_vec = [np.real(np.cos(theta_vec)[:, np.newaxis] *
                      eigvecs_center[:, 2*i]) -
              np.imag(np.sin(theta_vec)[:, np.newaxis] *
                      eigvecs_center[:, 2*i])
              for i in range(len(eigvals_center)//2)]

    # Create the winding angle guess
    rho_vec = [np.arctan2(np.imag(eigvals_center[2*i]),
               np.real(eigvals_center[2*i]))
               for i in range(len(eigvals_center)//2)]

    # Replicate the periodic state n times as the number of node, for the
    # array to have the same size as the stroboscopic map
    x0_p_tiled = np.tile(x0_p, N_nodes)

    # Create initial arrays for every center manifold couple
    s_guess = []
    for i in range(len(rho_vec)):

        # Extend origin state with guessed period
        s_origin = np.append(x0_p_tiled, (rho_vec[i], tau_p))

        # Extend guessed new state with guessed period
        s_perturbed = np.append(x0_p_tiled + delta*np.ravel(ds_vec[i]), (rho_vec[i], tau_p))

        # Merge the two extended states in the initial guesses list
        s_guess.append(np.vstack((s_origin, s_perturbed)))

    # # Full origin state with winding angle and frequency
    # s_origin = [np.append(x0_p_tiled, (tau_p, rho)) for rho in rho_vec]

    # # Guess for the stroboscopic map
    # s_guess = [np.append(x0_p_tiled + delta * np.ravel(ds), (tau_p, rho))
    #            for ds, rho in zip(ds_vec, rho_vec)]

    return s_guess #, s_origin, ds_vec


def _custom_init(state: np.ndarray, dir_vec: np.ndarray,
                 delta: float = 1e-6):


    s_origin = state

    s_perturbed = state + delta*dir_vec

    s_guess = np.vstack((s_origin, s_perturbed))

    return s_guess 

# TODO: Add option of initialization from keplerian solution around one of the primaries


@dataclass
class Initializer:

    lagrange_point_orbit = _lagrange_point_init
    quasi_periodic_orbit = _qp_orbits_init
    custom_state_and_direction = _custom_init
