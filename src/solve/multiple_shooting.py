# pylint: disable=too-many-locals, too-many-branches
"""
Multiple Shooting algorithms to correct a trajectory in both the CR3BP and the N-body ephemeris
force models.

"""

import itertools
import numpy as np
from multiprocess.pool import Pool

import src.init.defaults as dft
from src.crtbp.jacobi import jacobi
from src.init.cr3bp import Cr3bp
from src.init.ephemeris import Ephemeris
from src.propagation.cr3bp_propagator import Cr3bpSynodicPropagator
from src.propagation.ephemeris_propagator import EphemerisPropagator, EphemerisSHPropagator
from src.dynamics.ephemeris_dynamics import eqm_6_ephemeris
from src.dynamics.ephemeris_sh_dynamics import eqm_6_sh_ephemeris

try:
    from src.dynamics.libs.cr3bp_dynamics import eqm_6_synodic, u_bar_first_partials
except ImportError:
    from src.dynamics.cr3bp_dynamics_jit import eqm_6_synodic, u_bar_first_partials


class MultipleShooting:
    """Differential correction with Multiple Shooting.

    The class implements a multiple shooting procedure based on a multivariate Newton method
    to correct a series of patch points in a given dynamical model.

    Parameters
    ----------
    model : Cr3bp or Ephemeris
        `Cr3bp` or `Ephemeris` object that defines the environment for the propagation.
    epoch_constr : bool, optional
        Whether additional constraints to enforce epoch continuity between subsequent patch points
        are added to the problem formulation (True) or not (False).
        If model is a `Cr3bp` object and `epoch_constr` is set to ``True`` and exception is raised.
        Default is ``False``.
    ref : str, optional
        Reference frame name as defined by the NAIF's SPICE Toolkit.
        If model is a `Cr3bp` object this parameter is ignored. Default is ``J2000``.
    obs : Primary or None, optional
        Primary body whose position coincides with the origin of the reference frame in which the
        dynamics is propagated.
        If model is a `Cr3bp` object this parameter is ignored.
        Default is ``None`` for which the first Primary object in `model` is selected.
    t_c : float, optional
        Characteristic time for adimensionalization.
        If model is a `Cr3bp` object this parameter is ignored. Default is ``1.0``.
    l_c : float, optional
        Characteristic length for adimensionalization.
        If model is a `Cr3bp` object this parameter is ignored. Default is ``1.0``.
    precision : float, optional
        Precision at which the differential correction procedure is terminated.
        Default is given by `src.defaults.multiple_shooting_precision`.
    maxiter : int, optional
        Maximum number of iterations performed by the targeting scheme.
        Default is given by `src.defaults.multiple_shooting_maxiter`.
    rtol : float, optional
        Relative tolerance for integration routines. Default is `src.init.defaults.rtol`.
    atol : float, optional
        Absolute tolerance for integration routines. Default is `src.init.defaults.atol`.
    max_internal_steps : int, optional
        Maximum number of (internally defined) steps allowed for each integration point.
        Default defined in `src.init.defaults`.

    Attributes
    ----------
    mu_cr3bp : float or None
        CR3BP mass parameter or ``None`` if model is an `Ephemeris` object.
    odes :
        6-dimensional Equations of Motion for the selected dynamical model.
    prop : Cr3bpSynodicPropagator or EphemerisPropagator
        `Cr3bpSynodicPropagator` or `EphemerisPropagator` object.
    epoch_constr : bool
        Whether additional constraints to enforce epoch continuity between subsequent patch points
        are added to the problem formulation (True) or not (False).
        Always ``False`` if model is a `Cr3bp` object.
    precision : float
        Precision at which the differential correction procedure is terminated.
    maxiter : int
        Maximum number of iterations performed by the targeting scheme.

    """

    def __init__(self, model, epoch_constr=False, ref='J2000', obs=None, t_c=1.0, l_c=1.0,
                 precision=dft.multiple_shooting_precision, maxiter=dft.multiple_shooting_maxiter,
                 rtol=dft.rtol, atol=dft.atol, max_internal_steps=dft.max_internal_steps):
        """Inits MultipleShooting class. """

        if isinstance(model, Cr3bp):  # CR3BP dynamics is selected
            if epoch_constr:
                raise Exception('epoch constraint supported only for ephemeris model')
            self.mu_cr3bp = model.mu
            self.odes = lambda t, state: eqm_6_synodic(t, state, model.mu)
            self.prop = Cr3bpSynodicPropagator(model.mu, True, rtol=rtol, atol=atol,
                                               time_steps=None,
                                               max_internal_steps=max_internal_steps)
        elif isinstance(model, Ephemeris):  # Ephemeris dynamics is selected
            self.mu_cr3bp = None
            int_ref, gm_adim = model.get_integration_params(ref, obs, t_c, l_c)
            if model.sh_models is None:  # N-body ephemeris model with point masses only
                self.odes = lambda t, state: eqm_6_ephemeris(t, state, gm_adim, model.naif_ids,
                                                             int_ref, t_c, 1.0 / l_c)
                self.prop = EphemerisPropagator(model, ref, obs, t_c, l_c, True, epoch_constr,
                                                rtol=rtol, atol=atol, time_steps=None,
                                                max_internal_steps=max_internal_steps)
            else:  # N-body ephemeris model with non-uniform gravity field for one or more bodies
                if epoch_constr:
                    raise NotImplementedError('multiple shooting with non-uniform gravity field '
                                              'model and epoch constraint not implemented yet')
                r_adim, bf_ref, z_nm, nm_max = model.get_sh_integration_params(l_c)
                self.odes = lambda t, state: eqm_6_sh_ephemeris(t, state, gm_adim, model.naif_ids,
                                                                int_ref, t_c, 1.0 / l_c, r_adim,
                                                                bf_ref, z_nm, nm_max)
                self.prop = EphemerisSHPropagator(model, ref, obs, t_c, l_c, True, False,
                                                  rtol=rtol, atol=atol, time_steps=None,
                                                  max_internal_steps=max_internal_steps)
        else:
            raise Exception('model must be a Cr3bp or Ephemeris object')

        self.epoch_constr = epoch_constr
        self.precision = precision
        self.maxiter = maxiter

    def correct(self, t_patch, state_patch, var_time=True, cjac_constr=False, procs=dft.procs,
                verbose=True, **kwargs):
        """Perform differential correction on input patch points.

        Parameters
        ----------
        t_patch : ndarray
            Time vector at patch points.
        state_patch : ndarray
            State vector at patch points.
        var_time : bool, optional
            Whether adding the time vector at patch points to the free variables vector (True) or
            treating it as fixed input parameter (False).
            An exception is raised if `var_time` is set to ``False`` and the `MultipleShooting`
            object has been instantiated with `epoch_constr` equal to ``True``.
            Default is ``True``.
        cjac_constr : bool, int or tuple, optional
            If ``False``, no additional constraints on the Jacobi constant along the trajectory
            are enforced.
            If ``True``, an additional constraint is added to the problem formulation to enforce
            the final value of the Jacobi constant on the first patch point. Its value is
            directly computed from the initial state on the same node.
            If an integer, the Jacobi constant constraint is enforced on the patch point whose
            index corresponds to the input parameter. Its value is computed from the initial
            state on the same node.
            If a tuple, it must have the form ``(index, value)`` where ``index`` is the patch
            point index on which the constraint is enforced and ``value`` is the targeted Jacobi
            constant value on the same node.
        procs : int, optional
            Number of spawn worker processes when propagating subsequent trajectory segments
            in parallel or None for serial propagation.
            Default is the number of physical cores on Linux or None on Windows.
        verbose : bool, optional
            Whether printing the error norm at each iteration (True) or not (False).
            Default is `True`.

        Other Parameters
        ----------------
        t_fix : int or iterable
            Patch point index or indexes on which the time has to be treated as fixed parameter.
            If `var_time` is ``False`` an exception is raised.
        pos_fix : int or iterable
            Patch point index or indexes on which the position vector (first three components of
            the state) has to be treated as fixed parameter.
        vel_fix : int or iterable
            Patch point index or indexes on which the velocity vector (last three components of
            the state) has to be treated as fixed parameter.
        state_fix : int or iterable
            Patch point index or indexes on which the six components of the state have to be
            treated as fixed parameters.
        delta_v : int or iterable
            Patch point index or indexes on which an impulsive manoeuvre is performed.
            The manoeuvre is modelled as a discontinuity on the velocity components of the state
            between subsequents trajectory segments patched at the specified nodes.
        t_mask : ndarray
            Boolean mask to identify fixed time and free time nodes among all patch points.
            Zero (or False) indicates a free time node, one (or True) a fixed one. Must have the
            same size as `t_patch` (i.e. number of patch points).
            If `var_time` is ``False`` and exception is raised.
        state_mask : ndarray
            Boolean mask to identify fixed and free state components among all patch points.
            Zero (or False) indicates a free component, one (or True) a fixed one. Must have the
            same size as `state_patch` (i.e. six times the number of patch points).
        constr_mask : ndarray
            Boolean mask to identify state and time continuity constraints that have to be
            enforced between subsequent patch points. Zero (or False) indicates an imposed
            constraint, one (or True) a possible discontinuity on the corresponding state or time
            component between subsequent trajectory arcs (e.g. impulsive manoeuvres).
            If not provided, all continuity constraints are enforced.

        Returns
        -------
        t_patch : ndarray
            Corrected time vector at patch points.
        state_patch : ndarray
            Corrected state vector at patch points.
        sol : dict
            Dictionary containing additional information on the computed solution.
            The following keys are present:

            * | x_vec -- free variables vector at last iteration.
            * | fx_vec -- error vector at last iteration.
            * | dfx_mat -- Jacobian matrix at last iteration.
            * | x_fix_idx -- indexes of the free variables vector corresponding to fixed times and
              | state components not subject to correction or `None`.
            * | x_free_idx -- indexes of the free variables vector corresponding to free times
              | and state components subject to correction or `None`.
            * | fx_fix_idx -- indexes of the error vector corresponding to continuity and
              | additional constraints to be enforced or `None`.
            * | fx_free_idx -- indexes of the error vector corresponding to continuity constraints
              | not to be enforced or `None`.
            * | dfx_mat_idx -- indexes of the flattened Jacobian matrix to be retained while
              | computing the first-order correction or `None`.
              | Rows and columns of the submatrix obtained from these indexes correspond to the
              | enforced continuity and additional constraints and to the components of the free
              | variables vector subject to correction.

        """

        if self.epoch_constr and not var_time:
            raise Exception('var_time must be True for a multiple shooter with epoch constraint')

        nb_pts = t_patch.size  # total number of patch points

        # target value and column indexes in the Jacobian matrix for the Jacobi constant constraint
        cjac_constr, nb_add_constr = \
            self.get_jacobi_constr(state_patch, cjac_constr, self.mu_cr3bp)

        # indexes of the free variables vector corresponding to fixed and free times and state
        # components, indexes of the error vector corresponding to enforced and released
        # continuity and additional constraints, indexes of the Jacobian matrix to be considered
        # while computing the first-order correction
        x_fix_idx, x_free_idx, fx_fix_idx, fx_free_idx, dfx_idx = \
            self.get_indexes(nb_pts, nb_add_constr, var_time, self.epoch_constr, **kwargs)

        # init free variables vector, Jacobian matrix and constraints vector
        x_vec = self.get_free_vector(t_patch, state_patch, var_time, self.epoch_constr)
        dfx_mat, dfx_c = self.get_jacobian(nb_pts, nb_add_constr, var_time, self.epoch_constr,
                                           x_free_idx, fx_fix_idx, dfx_idx)
        fx_vec, err = np.empty(dfx_mat.shape[0]), 0.0

        # init workers and input parameters for propagation step
        workers_input, workers = self.get_workers(t_patch, x_vec, nb_pts, var_time)
        count = 0  # init iteration counter

        while count < self.maxiter:  # iterative solution

            if procs is not None:  # parallel propagation of trajectory segments
                with Pool(processes=procs) as pool:
                    out = pool.starmap(workers, workers_input)
            else:  # serial propagation of trajectory segments
                out = list(itertools.starmap(workers, workers_input))

            # all cases: update error vector (state continuity) and Jacobian matrix
            # (partials of the error vector w.r.t. free states)
            for i in range(nb_pts - 1):
                fx_vec[6 * i:6 * (i + 1)] = out[i][0:6] - x_vec[6 * (i + 1):6 * (i + 2)]
                dfx_mat[6 * i:6 * (i + 1), 6 * i:6 * (i + 1)] = out[i][6:42].reshape(6, 6)

            # variable time with epoch constraint:
            # update error vector (epoch continuity) and Jacobian matrix
            # (partials of the error vector w.r.t. propagation duration and epoch time)
            if self.epoch_constr:
                fx_vec[6 * (nb_pts - 1):7 * (nb_pts - 1)] = x_vec[7 * nb_pts:8 * nb_pts - 1] - \
                                                            x_vec[7 * nb_pts - 1:8 * nb_pts - 2] - \
                                                            x_vec[6 * nb_pts:7 * nb_pts - 1]
                for i in range(nb_pts - 1):
                    dfx_mat[6 * i:6 * (i + 1), 6 * nb_pts + i] = out[i][48:54]
                    dfx_mat[6 * i:6 * (i + 1), 7 * nb_pts - 1 + i] = out[i][42:48]

            # variable time without epoch constraint:
            # update Jacobian matrix (partials of the error vector w.r.t. propagation time spans)
            elif var_time:
                for i in range(nb_pts - 1):
                    dfx_mat[6 * i:6 * (i + 1), 6 * nb_pts + i] = out[i][42:48]
                    dfx_mat[6 * i:6 * (i + 1), 6 * nb_pts + i + 1] = out[i][48:54]

            # CR3BP model, optional: update error vector (Jacobi constant constraint) and
            # Jacobian matrix (partials of the Jacobi constant constraint w.r.t. free states)
            if cjac_constr:
                fx_vec[-1] = jacobi(x_vec[cjac_constr[0]], self.mu_cr3bp) - cjac_constr[1]
                dfx_mat[-1, cjac_constr[0][0:3]] = \
                    - 2.0 * u_bar_first_partials(x_vec[cjac_constr[0][0:3]], self.mu_cr3bp)
                dfx_mat[-1, cjac_constr[0][3:6]] = - 2.0 * x_vec[cjac_constr[0][3:6]]

            # fixed state components, fixed times at specific patch points or state components
            # or times discontinuities between subsequent patch points
            if dfx_c is not None:
                err = np.linalg.norm(fx_vec[fx_fix_idx])  # euclidean norm of the error vector
                if verbose:  # print current error norm
                    print(f"Iteration: {count + 1}\t\tError norm: {err:.5e}")
                if err < self.precision:  # break if precision is good enough
                    break

                # compute first order correction for the free variables subvector from
                # the corresponding error subvector and Jacobian submatrix
                dfx_mat.take(dfx_idx, out=dfx_c.ravel(), mode='wrap')  # extract submatrix
                x_vec[x_free_idx] -= dfx_c.T.dot(np.linalg.solve(dfx_c.dot(dfx_c.T),
                                                                 fx_vec[fx_fix_idx]))

            # no fixed state components, possibly fixed time at all patch points,
            # all state and time continuity constraints are enforced
            else:
                err = np.linalg.norm(fx_vec)  # compute the euclidean norm of the error vector
                if verbose:  # print current error norm
                    print(f"Iteration: {count + 1}\t\tError norm: {err:.5e}")
                if err < self.precision:  # break if precision is good enough
                    break

                # compute first order correction for the full free variables vector from the
                # corresponding error vector and Jacobian matrix
                x_vec -= dfx_mat.T.dot(np.linalg.solve(dfx_mat.dot(dfx_mat.T), fx_vec))

            count += 1  # update iteration counter

        # return corrected patch points times and states and solution dictionary containing
        # additional output information (free variables vector, error vector, error norm,
        # Jacobian matrix, indexes of the free variables vector corresponding to free and fixed
        # times and state components, indexes of the error vector corresponding to enforced and
        # released constraints and indexes of the flattened Jacobian retained while computing the
        # first-oder correction)
        sol = {'x_vec': x_vec, 'fx_vec': fx_vec, 'err_norm': err, 'dfx_mat': dfx_mat,
               'x_fix_idx': x_fix_idx, 'x_free_idx': x_free_idx, 'fx_fix_idx': fx_fix_idx,
               'fx_free_idx': fx_free_idx, 'dfx_mat_idx': dfx_idx}
        if var_time:
            return x_vec[-nb_pts:], x_vec[0:6 * nb_pts].reshape(nb_pts, 6), sol
        return t_patch, x_vec[0:6 * nb_pts].reshape(nb_pts, 6), sol

    @staticmethod
    def get_indexes(nb_pts, nb_add_constr, var_time, epoch_constr, **kwargs):
        """Returns the indexes of the free variables vector corresponding to free times and state
        components, the indexes of the error vector corresponding to enforced continuity and
        additional constraints and the corresponding indexes in the flattened Jacobian matrix.

        The returned indexes are required to extract the relevant components from the free
        variables vector, error vector and Jacobian matrix that define the corresponding
        subvectors and submatrix used to compute and apply the first-order correction.

        Parameters
        ----------
        nb_pts : int
            Number of patch points.
        nb_add_constr : int
            Number of additional constraints other than state and time continuity between
            subsequent patch points.
        var_time : bool
            Whether the time at patch points is treated as a free variable (True) or not (False).
        epoch_constr : bool
            Whether the epoch continuity constraint is enforced (True) or not (False).

        Other Parameters
        ----------------
        t_fix : int or iterable
            Patch point index or indexes on which the time has to be treated as fixed parameter.
            If `var_time` is ``False`` an exception is raised.
        pos_fix : int or iterable
            Patch point index or indexes on which the position vector (first three components of
            the state) has to be treated as fixed parameter.
        vel_fix : int or iterable
            Patch point index or indexes on which the velocity vector (last three components of
            the state) has to be treated as fixed parameter.
        state_fix : int or iterable
            Patch point index or indexes on which the six components of the state have to be
            treated as fixed parameters.
        delta_v : int or iterable
            Patch point index or indexes on which an impulsive manoeuvre is performed.
            The manoeuvre is modelled as a discontinuity on the velocity components of the state
            between subsequents trajectory segments patched at the specified nodes.
        t_mask : ndarray
            Boolean mask to identify fixed time and free time nodes among all patch points.
            Zero (or False) indicates a free time node, one (or True) a fixed one. Must have the
            same size as `t_patch` (i.e. number of patch points).
            If `var_time` is ``False`` and exception is raised.
        state_mask : ndarray
            Boolean mask to identify fixed and free state components among all patch points.
            Zero (or False) indicates a free component, one (or True) a fixed one. Must have the
            same size as `state_patch` (i.e. six times the number of patch points).
        constr_mask : ndarray
            Boolean mask to identify state and time continuity constraints that have to be
            enforced between subsequent patch points. Zero (or False) indicates an imposed
            constraint, one (or True) a possible discontinuity on the corresponding state or time
            component between subsequent trajectory arcs (e.g. impulsive manoeuvres).
            If not provided, all continuity constraints are enforced.

        Returns
        -------
        x_fix : ndarray or None
            Indexes of the free variables vector corresponding to fixed times and state components
            not subject to correction. If all components of the free variables vector are left free
            and all continuity constraints are enforced `None` is returned instead.
        x_free : ndarray or None
            Indexes of the free variables vector corresponding to free times and state components
            subject to correction. If all components of the free variables vector are left free
            and all continuity constraints are enforced `None` is returned instead.
        fx_fix : ndarray
            Indexes of the error vector corresponding to continuity constraints and optional
            additional constraints to be enforced. If all components of the free variables vector
            are left free and all continuity constraints are enforced `None` is returned instead.
        fx_free : ndarray
            Indexes of the error vector corresponding to continuity constraints not to be enforced.
            If all components of the free variables vector are left free and all continuity
            constraints are enforced `None` is returned instead.
        dfx_idx : ndarray
            Indexes of the flattened Jacobian matrix to be retained while computing the
            first-order correction. Rows and columns of the submatrix obtained from these indexes
            correspond to the continuity and additional constraints to be enforced and to the
            components of the free variables vector subject to correction.
            If all components of the free variables vector are left free and all continuity
            constraints are enforced `None` is returned instead.

        """

        # no fixed states components or times, all continuity constraints enforced
        if len(kwargs) == 0:
            return None, None, None, None, None

        # constants definition
        state_keys = ('pos_fix', 'vel_fix', 'state_fix')  # supported fixed state components keys
        state_comps = ((0, 1, 2), (3, 4, 5), (0, 1, 2, 3, 4, 5))  # corresponding relative indexes

        # indexes of the free variables vector corresponding to fixed state components
        state_fix = MultipleShooting.get_state_comps_idx(nb_pts, 'state_mask', state_keys,
                                                         state_comps, **kwargs)

        # indexes of the free variables vector corresponding to fixed times, total number of free
        # variables prior to constraints definition and total number of continuity constraints
        # prior to discontinuities definition
        t_fix, nb_free_vars, nb_constr = \
            MultipleShooting.get_time_comps_idx(nb_pts, var_time, epoch_constr,
                                                't_mask', 't_fix', **kwargs)

        # indexes of the free variables vector corresponding to fixed state components and times
        x_fix = np.unique(np.concatenate((state_fix, t_fix)))
        if x_fix.size == nb_free_vars or (epoch_constr and x_fix.size == 7 * nb_pts):
            raise Exception('all components of the free variables vector have been fixed')

        # indexes of the error vector corresponding to continuity constraints not to be enforced
        kwargs = MultipleShooting.adjust_dv_idx(nb_pts, **kwargs)  # adjust dV indexes
        fx_free = MultipleShooting.get_state_comps_idx(nb_pts - 1, 'constr_mask', ('delta_v',),
                                                       (state_comps[1],), **kwargs)

        # no fixed states components or times, all continuity constraints enforced
        if x_fix.size == 0 and fx_free.size == 0:
            return None, None, None, None, None

        # indexes of the free variables vector corresponding to state components and times
        # subject to correction
        x_free = np.arange(nb_free_vars)[np.isin(np.arange(nb_free_vars), x_fix, invert=True)]

        # indexes of the error vector corresponding to continuity and additional constraints
        # to be enforced
        fx_fix = np.concatenate((np.arange(nb_constr)[np.isin(np.arange(nb_constr),
                                                              fx_free, invert=True)],
                                 np.arange(nb_constr, nb_constr + nb_add_constr)))

        # indexes of the flattened Jacobian that define the submatrix used to compute
        # the first-order correction
        dfx_idx = np.tile(x_free, fx_fix.size) + nb_free_vars * np.repeat(fx_fix, x_free.size)

        return x_fix, x_free, fx_fix, fx_free, dfx_idx

    @staticmethod
    def get_state_comps_idx(nb_pts, mask_key, states_keys, rel_idx, **kwargs):
        """Converts keyword arguments for state components into the corresponding indexes. """
        if mask_key in kwargs and any([k in kwargs for k in states_keys]):
            raise Exception('state mask and state keys are mutually exclusive keyword arguments')
        if mask_key in kwargs:
            return MultipleShooting.mask_to_idx(6 * nb_pts, kwargs[mask_key])
        return MultipleShooting.patch_points_to_comps_idx(nb_pts, states_keys, rel_idx, **kwargs)

    @staticmethod
    def mask_to_idx(nb_pts, mask):
        """Converts a boolean mask into the corresponding indexes. """
        if mask.size != nb_pts:  # check on dimensions
            raise Exception('the provided mask size does not match the target array size')
        return mask.nonzero()[0]

    @staticmethod
    def patch_points_to_comps_idx(nb_pts, states_keys, rel_idx, **kwargs):
        """Converts patch points indexes into the corresponding state components indexes. """
        comps_idx = np.empty(0, dtype=np.int)  # collect indexes for all components
        for i, k in enumerate(states_keys):  # loop over all possible keywords
            if k in kwargs:  # convert patch point indexes to absolute components indexes
                pp_idx = np.asarray([kwargs[k]], dtype=np.int).ravel()  # patch points indexes
                abs_idx = np.repeat(6 * np.where(pp_idx < 0, pp_idx + nb_pts, pp_idx),
                                    len(rel_idx[i])) + np.tile(rel_idx[i], pp_idx.size)
                if (abs_idx < 0).any() or (abs_idx > 6 * nb_pts).any():  # check on values
                    raise Exception(f"states components indexes must lie in the close interval "
                                    f"[0, {6 * (nb_pts - 1)}]")
                comps_idx = np.concatenate((comps_idx, abs_idx))
        return np.unique(comps_idx)

    @staticmethod
    def adjust_dv_idx(nb_pts, **kwargs):
        """Adjusts indexes provided via delta_v keyword to match the correct patch points. """
        if 'delta_v' in kwargs:
            dv_idx = np.asarray(kwargs['delta_v'], dtype=np.int).ravel()
            if (dv_idx < 1 - nb_pts).any() or (dv_idx > nb_pts - 1).any() or (dv_idx == 0).any():
                raise Exception(f"delta_v indexes must lie in one of the two closed intervals "
                                f"[{1 - nb_pts}, -1] or [1, {nb_pts - 1}]")
            dv_idx = np.where(dv_idx < 0, dv_idx, dv_idx - 1)
            kwargs['delta_v'] = dv_idx
        return kwargs

    @staticmethod
    def get_time_comps_idx(nb_pts, var_time, epoch_constr, mask_key, time_key, **kwargs):
        """Converts keyword arguments for time components into the corresponding indexes. """
        if not var_time and any([k in kwargs for k in (mask_key, time_key)]):
            raise Exception('time mask or time keys can be set only if var_time is True')
        if var_time:  # variable time formulation
            if all([k in kwargs for k in (mask_key, time_key)]):
                raise Exception('time mask and time keys are mutually exclusive keyword arguments')
            if mask_key in kwargs:  # fixed times as boolean mask
                t_idx = MultipleShooting.mask_to_idx(nb_pts, kwargs[mask_key])
            elif time_key in kwargs:  # fixed times as patch points indexes
                t_idx = np.asarray([kwargs[time_key]], dtype=np.int).ravel()
                t_idx = np.unique(np.where(t_idx < 0, t_idx + nb_pts, t_idx))  # positive indexes
                if (t_idx < 0).any() or (t_idx > nb_pts - 1).any():  # check on values
                    raise Exception(f"times indexes must lie in the close interval "
                                    f"[0, {nb_pts - 1}]")
            else:  # no fixed times
                t_idx = np.empty(0, dtype=np.int)
            return (t_idx + 7 * nb_pts - 1, 8 * nb_pts - 1, 7 * (nb_pts - 1)) if epoch_constr \
                else (t_idx + 6 * nb_pts, 7 * nb_pts, 6 * (nb_pts - 1))
        return np.empty(0, dtype=np.int), 6 * nb_pts, 6 * (nb_pts - 1)

    @staticmethod
    def get_free_vector(t_patch, state_patch, var_time, epoch_constr):
        """Returns the initial free variables vector `X`.

        Parameters
        ----------
        t_patch : ndarray
            Time vector at patch points.
        state_patch : ndarray
            State vector at patch points.
        var_time : bool
            Whether the time at patch points is treated as a free variable (True) or not (False).
        epoch_constr : bool
            Whether the epoch continuity constraint is enforced (True) or not (False).

        Returns
        -------
        x_vec : ndarray
            Free variables vector.

        """

        if epoch_constr:
            return np.concatenate((state_patch.ravel(), t_patch[1:] - t_patch[:-1],
                                   t_patch.ravel()))
        if var_time:
            return np.concatenate((state_patch.ravel(), t_patch.ravel()))
        return state_patch.flatten()

    @staticmethod
    def get_jacobian(nb_pts, nb_add_constr, var_time, epoch_constr, x_free, fx_fix, dfx_idx):
        """Returns the initial Jacobian matrix `DF(X)`.

        Parameters
        ----------
        nb_pts : int
            Number of patch points.
        nb_add_constr : int
            Number of additional constraints.
        var_time : bool
            Whether the time at patch points is treated as a free variable (True) or not (False).
        epoch_constr : bool
            Whether the epoch continuity constraint is enforced (True) or not (False).
        x_free : ndarray or None
            Indexes of the free variables vector corresponding to free times and state components
            subject to correction. If all components of the free variables vector are left free
            and all continuity constraints are enforced `None` is returned instead.
        fx_fix : ndarray or None
            Indexes of the error vector corresponding to continuity constraints and optional
            additional constraints to be enforced. If all components of the free variables vector
            are left free and all continuity constraints are enforced `None` is returned instead.
        dfx_idx : ndarray or None
            Indexes of the flattened Jacobian that define the submatrix used to compute the
            first-order correction. If the entire matrix is used, and empty array must be passed.

        Returns
        -------
        dfx : ndarray
            Initial Jacobian matrix.
        dfx_c : ndarray or None
            Initial Jacobian submatrix or `None` if all components of the free variables vector
            are left free and all continuity constraints are enforced.

        """

        if epoch_constr:  # N-body ephemeris model with epoch constraint
            dfx = np.zeros((7 * (nb_pts - 1) + nb_add_constr, 8 * nb_pts - 1))
            dfx.ravel()[6:7 * (nb_pts - 1) * (8 * nb_pts - 1) - nb_pts:8 * nb_pts] = - 1.0
            dfx.ravel()[6 * (nb_pts - 1) * (8 * nb_pts - 1) + 7 * nb_pts - 1:
                        7 * (nb_pts - 1) * (8 * nb_pts - 1) - 1:8 * nb_pts] = - 1.0
            dfx.ravel()[6 * (nb_pts - 1) * (8 * nb_pts - 1) + 7 * nb_pts:
                        7 * (nb_pts - 1) * (8 * nb_pts - 1):8 * nb_pts] = 1.0
        elif var_time:  # CR3BP or N-body ephemeris model, no epoch constraint, variable time
            dfx = np.zeros((6 * (nb_pts - 1) + nb_add_constr, 7 * nb_pts))
            dfx.ravel()[6:6 * 7 * nb_pts * (nb_pts - 1) - nb_pts:7 * nb_pts + 1] = - 1.0
        else:  # CR3BP or N-body ephemeris model, no epoch constraint, fixed time
            dfx = np.zeros((6 * (nb_pts - 1) + nb_add_constr, 6 * nb_pts))
            dfx.ravel()[6:6 * 6 * nb_pts * (nb_pts - 1):6 * nb_pts + 1] = - 1.0

        # prune columns corresponding to fixed states components or times and rows corresponding
        # to continuity constraints not to be enforced (i.e. locations of impulsive manoeuvres)
        if dfx_idx is not None:
            return dfx, dfx.take(dfx_idx).reshape(fx_fix.size, x_free.size)
        return dfx, None

    @staticmethod
    def get_jacobi_constr(state_patch, cjac_constr, mu_cr3bp):
        """Returns indexes and values for the imposed Jacobi constant constraint.

        Parameters
        ----------
        state_patch : ndarray
            State vector at patch points.
        cjac_constr : bool, int or tuple, optional
            If ``False``, no additional constraints on the Jacobi constant along the trajectory
            are enforced.
            If ``True``, an additional constraint is added to the problem formulation to enforce
            the final value of the Jacobi constant on the first patch point. Its value is
            directly computed from the initial state on the same node.
            If an integer, the Jacobi constant constraint is enforced on the patch point whose
            index corresponds to the input parameter. Its value is computed from the initial
            state on the same node.
            If a tuple, it must have the form ``(index, value)`` where ``index`` is the patch
            point index on which the constraint is enforced and ``value`` is the targeted Jacobi
            constant value on the same node.
        mu_cr3bp : float
            CR3BP mass parameter.

        Returns
        -------
        cjac_constr : tuple or bool
            Jacobi constant constraint in the form ``(indexes, value)`` where ``indexes`` are the
            column indexes in the Jacobian matrix corresponding to the patch point in which the
            constraint is enforced and ``value`` the constraint value. If no constraint is imposed,
            ``False`` is returned instead.
        nb_add_constr : int
            Number of additional constraints, equal to one or zero depending if the constraint is
            enforced or not.

        """

        if cjac_constr and (mu_cr3bp is None):  # constraint supported only for CR3BP model
            raise NotImplementedError('Jacobi constant constraint supported only for CR3BP model')

        # constraint on first patch point from initial value on the same node (if True) or
        # no constraint imposed (if False)
        if isinstance(cjac_constr, bool):
            return ((np.arange(6), jacobi(state_patch[0, 0:6], mu_cr3bp)), 1) \
                if cjac_constr else (False, 0)

        # constraint at specified patch point from initial value on the same node
        if isinstance(cjac_constr, int):
            return ((6 * cjac_constr + np.arange(6),
                    jacobi(state_patch[cjac_constr, 0:6], mu_cr3bp)), 1)

        # constraint at specified patch point and value
        if isinstance(cjac_constr, (tuple, list)) and len(cjac_constr) == 2:
            return (6 * cjac_constr[0] + np.arange(6), cjac_constr[1]), 1
        raise Exception('cjac_constr must be either True, False, a patch point index or a two '
                        'elements tuple in the form (index, value)')

    def get_workers(self, t_patch, x_vec, nb_pts, var_time):
        """Returns a worker object and corresponding input parameters for the propagation step. """
        if self.epoch_constr:  # variable time formulation with epoch constraint
            t_span = x_vec[6 * nb_pts:8 * nb_pts - 2].reshape(2, nb_pts - 1).T
            return [(t_span[i], x_vec[6 * i:6 * (i + 1)]) for i in range(nb_pts - 1)], \
                self.epoch_constr_worker
        if var_time:  # variable time formulation without epoch constraint
            return [(x_vec[6 * nb_pts + i:6 * nb_pts + 2 + i], x_vec[6 * i:6 * (i + 1)])
                    for i in range(nb_pts - 1)], self.variable_time_worker
        t_span = t_patch.repeat(2)[1:-1].reshape(nb_pts - 1, 2)  # fixed time formulation
        return [(t_span[i], x_vec[6 * i:6 * (i + 1)]) for i in range(nb_pts - 1)], \
            self.fixed_time_worker

    def fixed_time_worker(self, t_span, state0):
        """Worker for propagation step with fixed time. """
        _, state_vec, _, _ = self.prop.propagate(t_span, state0)
        return state_vec[-1]

    def variable_time_worker(self, t_span, state0):
        """Worker for propagation step with variable time. """
        _, state_vec, _, _ = self.prop.propagate(t_span, state0)
        return np.concatenate((state_vec[-1], - state_vec[-1, 6:42].reshape(6, 6).
                               dot(self.odes(t_span[0], state0)),
                               self.odes(t_span[-1], state_vec[-1, 0:6])))

    def epoch_constr_worker(self, t_span, state0):
        """Worker for propagation step with variable time and epoch constraint. """
        _, state_vec, _, _ = self.prop.propagate([t_span[-1], t_span.sum()], state0)
        return np.concatenate((state_vec[-1], self.odes(t_span.sum(), state_vec[-1, 0:6])))
