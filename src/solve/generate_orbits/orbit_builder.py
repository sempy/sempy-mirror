# %% Import modules
from typing import List, Callable, Iterable, Optional, Tuple, Union, Any
from dataclasses import fields, dataclass

import numpy as np
import scipy.interpolate as scitp
import scipy.optimize as scopt
from numpy import ndarray

from src.dynamics.dynamical_environment import DynamicalEnvironment
from src.solve.constraints_libraries.constraints_lib import PoincareLib
from src.solve.predictor import AbstractPredictor
from src.solve.correct.corrector import Corrector
from src.solve.constraints import Constraints
from src.solve.generate_orbits.continuation_lib import BuildStopCriteria, Bifurcations, BifurcationTypes
from src.propagation.propagator import Propagator

import pickle

import plotly.graph_objects as go
import plotly.io as pio


# %% Bifurcation data storage utility classes

@dataclass
class BifurcationData:
    states_and_periods: np.ndarray = None
    directions: np.ndarray = None


@dataclass
class BifurcationsSet:
    tangent: BifurcationData = None
    period_doubling: BifurcationData = None
    period_tripling: BifurcationData = None
    period_quadrupling: BifurcationData = None
    secondary_hopf: BifurcationData = None


# %% Family continuation class
def _build_constraint(corr_fun, pred_state, ref_states):
    def new_constraint(variables, jac_flag):
        return corr_fun(variables, pred_state, ref_states, jac_flag)
    return new_constraint


class BuildOrbit:

    def __init__(self,
                 init: np.ndarray,
                 correction_criteria_list: List[Callable],
                 continuation_criterion: Callable,
                 stop_criteria_list: List[Callable],
                 predictor: AbstractPredictor,
                 env: DynamicalEnvironment,
                 prop: Propagator):

        # Set class attributes
        self.correction_conditions = correction_criteria_list
        self.continuation_conditions = continuation_criterion
        self.states_stack = init # np.vstack((init[1][0], init[0][0]))
        self.N_nodes = len(init[0,:])//6 # FIXME: Only if all states are before times

        self.stop_criteria = BuildStopCriteria(stop_criteria_list)
        self.predictor = predictor

        self.env = env
        self.prop = prop

        # Bifurcation attributes
        self.bi_state = []
        self.bi_direction = []
        self.bi_eigs = []
        self.bi_type = []

        self.bifurcations = BifurcationsSet()

        # Interpolation
        self.interp_states = np.ndarray((0, 7))

    def generate_family(self):
        
        # Correct first initialization guess
        for i in range(1, self.states_stack.shape[0]):

            eval_cont_constr = _build_constraint(self.continuation_conditions,
                                                      self.states_stack[i],
                                                      self.states_stack[i-1:i])
            
            corr_constr = self.correction_conditions + [eval_cont_constr]
            
            constr = Constraints(corr_constr)  # TODO: add possibility to update dynamics in the continuation (e.g. transitioning to elliptic model)

            corr = Corrector(constr, tol=1e-14)
            new_sol = corr(self.states_stack[i], jac_flag = True)

            if new_sol.success is False:
                raise ValueError("Correction of the first orbits failed.")

            self.states_stack[i] = new_sol.x

        # Initialize continuator matrices and parameters
        self.predictor.initialize(self.states_stack)

        # Start Continuation
        while all(self.stop_criteria(self.states_stack) < 0):
            print(f"{self.stop_criteria(self.states_stack)}")

            # ref_state = cont.states_stack[-1,:]
            pred_state = self.predictor.predict()

            eval_cont_constr = _build_constraint(self.continuation_conditions,
                                                     pred_state,
                                                     self.states_stack)

            corr_constr = self.correction_conditions + [eval_cont_constr]

            constr = Constraints(corr_constr)  # TODO: add possibility to update dynamics in the continuation (e.g. transitioning to elliptic model)

            corr = Corrector(constr, tol=1e-14)
            new_sol = corr(pred_state, jac_flag = True)


            prediction_success = self.predictor.check_prediction_success(new_sol.x)[0]
            correction_success = new_sol.success

            if correction_success:
                self.predictor.success_params_update(new_sol.x)

                if prediction_success:
                    self.states_stack = np.vstack((self.states_stack, new_sol.x))

            else:
                self.predictor.fail_params_update()


    '''Detect bifurcations of orbits'''
    def _bifurcations_optim_fun(self, t_itp: float, states_itp: scitp.PPoly, bifurcation_fun: Callable):

        # Get new guess from interpolation
        new_state = states_itp(t_itp).ravel()

        # Build the cosntraint function
        variable_constraint = PoincareLib.fix_elements([0], [new_state[0]])
        constr_list = Constraints(self.correction_conditions + [variable_constraint])
        corr = Corrector(constr_list, verbose=False)
        
        # Compute the new orbit
        sol = corr(new_state, jac_flag = True)

        # Evaluate the bifurcation parameters
        residual = bifurcation_fun(sol.x)

        # print([t_itp, residual**2])
        # Return quadratic residual (to always have positive value)
        return residual**2
    
    def detect_bifurcations(self):

        # extract initial states
        states = self.states_stack

        # Create cubic spline of develped orbits' data
        t_itp = np.arange(states.shape[0])
        states_itp = scitp.CubicSpline(t_itp, states)

        # Setup fixed contraints and corrector for all bifurcations
        constr = Constraints(self.correction_conditions)

        # Iterate optimization for each bifurcation type
        for bi_params in fields(BifurcationTypes):
            print(f"Detecting Type \"{bi_params.name}\" bifurcations...")

            # Extract the broucke's parameters function and name
            bi_params_fun = bi_params.default
            bi_params_name = bi_params.name

            # Build the bifurcation search function
            bifurcation_fun = Bifurcations.find_bifurcation(self.prop, self.env, bi_params_fun)

            # Evaluate residuals for already developed orbits
            # (to find an educated guess)
            res = np.zeros(states.shape[0])

            for t_guess, state in enumerate(states):
                res[t_guess] = bifurcation_fun(state)**2

            # Interpolate bifurcation parameters residual and compute first
            # and second derivatives
            res_itp = scitp.PchipInterpolator(t_itp, res)

            res_der_1 = res_itp.derivative(nu=1)
            res_der_2 = res_itp.derivative(nu=2)

            # Find stationary point (maxima and minima)
            t_stationary = res_der_1.roots(extrapolate=False)

            t_minima = [t for t in t_stationary if res_der_2(t)>0 and t!=0]

            # Start optimization to find the bifurcation points
            bifurcation_states = []
            bifurcation_directions = []
            for i, t_guess in enumerate(t_minima):
                print(f"Bifurcation {i+1:d} out of {len(t_minima):d}")

                # FIXME: use the sempy optimization blocks
                if i==0:
                    lb = t_itp[0]
                else:
                    lb = (t_minima[i]+t_minima[i-1])/2

                if i==len(t_minima)-1:
                    ub = t_itp[-1]
                else:
                    ub = (t_minima[i+1]+t_minima[i])/2

                opt_sol = scopt.minimize(lambda t: self._bifurcations_optim_fun(t, states_itp, bifurcation_fun),
                                         t_guess,
                                         bounds = ((lb, ub),),
                                         method = 'Nelder-Mead',
                                         tol=1e-14)
                
                if opt_sol.success and opt_sol.fun < 1e-14 and opt_sol.x > 1: # FIXME: find a better criteria to accept the solution as a bifurcation point

                    # State interpolation at the optimal point
                    state_guess = states_itp(opt_sol.x).ravel()

                    # Orbit correction
                    variable_constraint =  PoincareLib.fix_elements([0], [state_guess[0]])
                    constr_list = Constraints(self.correction_conditions + [variable_constraint])
                    corr = Corrector(constr_list, verbose=False)

                    corr_sol = corr(state_guess, jac_flag = True)

                    # Adjust period for period multiplaction bifurcations
                    period_multiplier = 1
                    if bi_params_name == 'period_doubling':
                        corr_sol.x[-1] *= 2
                        period_multiplier = 2
                    elif bi_params_name == 'period_tripling':
                        corr_sol.x[-1] *= 3
                        period_multiplier = 3
                    elif bi_params_name == 'period_quadrupling':
                        corr_sol.x[-1] *= 4
                        period_multiplier = 4 # FIXME: best result with 2 apparently

                    
                    prop_sol = self.prop(self.env, np.linspace(0, corr_sol.x[-1], period_multiplier + 1),
                                         corr_sol.x[:6])
                    
                    prop_state = np.append(prop_sol.state_vec[:-1,:].ravel(),
                                           corr_sol.x[-1])
                    
                    # Stack bifurcation state
                    # self.bi_state += [prop_state[np.newaxis, :]]

                    # Extract bifurcation direction
                    jac = constr.constraints_fstack(prop_state, jac_flag = True)[1]

                    Sigma, V = np.linalg.svd(jac)[1:]


                    # self.bi_direction += [V[-2,:]]  # FIXME> Find a better way to identify the eigenvalue associated with the bifurcating family (for example comparing the bifurcating directions with the previous continuation direction, and take the most different one)
                    # self.bi_eigs += [Sigma]
                    # self.bi_type.append(bi_params_name)

                    bifurcation_states.append([prop_state[np.newaxis, :]])
                    bifurcation_directions.append([V[-2,:]])

            # Convert the collected bifurcations list into a numpy array
            bifurcation_states_np = np.squeeze(np.array(bifurcation_states))
            bifurcation_directions_np = np.squeeze(np.array(bifurcation_directions))

            if len(bifurcation_states_np.shape) < 2:
                bifurcation_states_np = bifurcation_states_np[np.newaxis, :]
                bifurcation_directions_np = bifurcation_directions_np[np.newaxis, :]
            
            # Set the state and direction in the Bifurcation object attribute 
            # corresponding to the bifurcation type
            setattr(self.bifurcations, bi_params_name,
                    BifurcationData(bifurcation_states_np,
                                    bifurcation_directions_np))
            

    ''' Interpolation of orbits'''
    def _interp_param_optim_fun(self, t_itp: float, states_itp: scitp.PPoly, interp_param_fun: Callable, target_value: float):
        
        # Get new guess from interpolation
        new_state = states_itp(t_itp).ravel()

        # Build the cosntraint function
        variable_constraint = PoincareLib.fix_elements([0], [new_state[0]])
        constr_list = Constraints(self.correction_conditions + [variable_constraint])
        corr = Corrector(constr_list, verbose=False)

        # Compute the new orbit
        sol = corr(new_state, jac_flag = True)

        # Return residual
        return interp_param_fun(sol.x, target_value)

    def interpolate_family(self, get_interpolation_residual: Callable,
                           interpolation_values: Iterable):


       # extract initial states
        states = self.states_stack

        # Create cubic spline of develped orbits' data
        t_itp = np.arange(states.shape[0])
        states_itp = scitp.CubicSpline(t_itp, states)


        # Initialize interpolated states matrix
        interpolated_states = []

        # Loop to find all of the interpolated solutions
        for i, target_param in enumerate(interpolation_values):
            print(f"Interpolating {i+1:d} out of {len(interpolation_values):d}")

            # Compute squared distance of the sampled orbits' parameters from the target value
            residual = np.array([get_interpolation_residual(state,
                                                            target_param)
                                 for state in self.states_stack])

            # Create interpolating function of the residual
            res_itp = scitp.PchipInterpolator(t_itp, residual)

            # Identify local minima and intersections
            res_first_der = res_itp.derivative(nu=1)
            res_second_der = res_itp.derivative(nu=2)


            # Find stationary point (maxima and minima)
            t_stationary = res_first_der.roots(extrapolate=False)

            t_minima = [t for t in t_stationary if res_second_der(t)>0]


            # Start optimization to find the bifurcation points
            for t_guess in t_minima:

                # FIXME: use the sempy optimization blocks
                if i==0:
                    lb = t_itp[0]
                else:
                    lb = (t_minima[i]+t_minima[i-1])/2

                if i==len(t_minima)-1:
                    ub = t_itp[-1]
                else:
                    ub = (t_minima[i+1]+t_minima[i])/2

                opt_sol = scopt.minimize(lambda t: self._interp_param_optim_fun(t, states_itp, get_interpolation_residual, target_param),
                                         t_guess,
                                         bounds = ((lb, ub),),
                                         method = 'Nelder-Mead',
                                         tol = 1e-14)

                if opt_sol.success and opt_sol.fun < 1e-14: # FIXME: find a better criteria to accept the solution as a bifurcation point

                    # State interpolation at the optimal point
                    state_guess = states_itp(opt_sol.x).ravel()

                    # Orbit correction
                    variable_constraint = PoincareLib.fix_elements([0], [state_guess[0]])
                    constr_list = Constraints(self.correction_conditions + [variable_constraint])
                    corr = Corrector(constr_list, verbose=False)

                    corr_sol = corr(state_guess, jac_flag = True)

                    interpolated_states.append([corr_sol.x])

        # Convert the collected values list into a numpy array
        interpolated_states_np = np.squeeze(np.array(interpolated_states))

        if len(interpolated_states_np.shape) < 2:
            interpolated_states_np = interpolated_states_np[np.newaxis, :]

        self.interp_states = interpolated_states_np

                    # TODO: Decide whether creating a new interpolated states list or just overwrite the one form the continuation

    ''' Export Family Object'''
    def export_family(self, get_interpolation_residual: Optional[Callable] = None,
                      interpolation_values: Optional[Iterable] = None):

        # Build check on itnerpolation inputs
        check_interp_function = get_interpolation_residual is not None
        check_interp_value = interpolation_values is not None
        
        check_both = check_interp_function and check_interp_value
        check_either = check_interp_function or check_interp_value

        # Interpolation requested but wrong imputs provided
        if check_either and not check_both:  
            raise ValueError("If interpolated states are desired, both \"get_interpolation_residual\" and \"interpolation_values\" shall be provided")


        # Create Orbits object
        if check_both: # Interpolation is performed
            self.interpolate_family(get_interpolation_residual, interpolation_values)
            
            if self.interp_states.size == 0:
                raise ValueError("No solution was found (within the domain) with the requested interpolation value.")

            return Orbits(self.interp_states, self.bifurcations, self.env, self.prop)
        
        return Orbits(self.states_stack, self.bifurcations, self.env, self.prop)
        


# TODO: Stop using pickle and find better alternatives
    
class Orbits: 

    def __init__(self, states_and_periods: np.ndarray, bifurcations: BifurcationsSet,
                 environment: DynamicalEnvironment, propagator: Propagator):
        
        # Detect periodic or quasi-periodic orbit
        n_variables = states_and_periods.shape[1]  # Number of variables
        self.N_nodes = n_variables // 6


        # Initial states and periods
        self.states_and_periods = states_and_periods

        if n_variables % 6 == 1:
            self.orbit_type = 'periodic'
            self.states = states_and_periods[:,:-1]
            self.periods = states_and_periods[:,-1]

        elif  n_variables % 6 == 2:
            self.orbit_type = 'quasi-periodic'
            self.states = states_and_periods[:,:-2]
            self.periods = states_and_periods[:,-2:]
        else:
            raise IndexError("Invalid number of variables, check inputs")

        # Bifurcations
        self.bifurcations = bifurcations

        # Environment
        self.environment = environment

        # States propagation
        self.propagator = propagator
    
    def save_as(self, save_path_and_name: str):
        with open(save_path_and_name, 'wb') as f:
            pickle.dump(self, f)

    @classmethod
    def load(cls, load_path_and_name: str):
        with open(load_path_and_name, 'rb') as f:
            data: Orbits = pickle.load(f)
        
        # FIXME: Temporary workaround to load old saved data with no "environment" attribure
        if not hasattr(data, 'environment'):    
            from src.init.primary import Primary
            from src.dynamics.cr3bp_environment import CR3BP

            env = CR3BP(Primary.EARTH, Primary.MOON)
            setattr(data,'environment', env)
            
        return Orbits(data.states_and_periods, data.bifurcations, data.environment, data.propagator)
    
    def propagate(self, index: int = 0, n_eval: int = 1001) -> Tuple[
        Union[ndarray, Tuple[ndarray, Optional[float]]], Union[ndarray, Any]]:

        if self.orbit_type == 'periodic':
            propagated_state = self.states[index, :6]
            propagated_time = np.array([0])

            nodes_time = np.linspace(0, self.periods[index], self.N_nodes + 1)
            for i in range(self.N_nodes):
                prop_sol = self.propagator(self.environment, [nodes_time[i], nodes_time[i+1]],
                                        self.states[index, 6*i:6*(i+1)],
                                        n_eval = np.ceil(n_eval/6).astype(int))
                
                propagated_state = np.vstack((propagated_state, prop_sol.state_vec[1:,:]))
                propagated_time = np.append(propagated_time, prop_sol.t_vec)
        
        elif self.orbit_type == 'quasi-periodic':\

            n_eval = min([n_eval,200])
            
            propagated_state = np.zeros((n_eval, self.states.shape[1]))
            propagated_time = np.linspace(0, self.periods[index, -1], n_eval)
            for i in range(self.N_nodes):
                prop_sol = self.propagator(self.environment, [0, self.periods[index, -1]],
                                           self.states[index, 6*i:6*(i+1)],
                                           n_eval = n_eval) # max 200 points, for speed in make_plot
                
                propagated_state[:, 6*i:6*(i+1)] = prop_sol.state_vec
        else:
            raise ValueError("Invalid orbit type")

        return propagated_time, propagated_state

    @classmethod
    def read_plot(cls, fig_path: str, show: bool = False):
        
        fig = pio.read_json(fig_path + '.json')
        
        if show:
            fig.show()

        return fig

    def make_plot(self, fig: go.FigureWidget = None, title: str =None, orbit_name: str = None,
             save_as: str = None, show: bool = False, idx: int = None) -> go.FigureWidget:

        # If figure doesn't exixt yet, make one
        if fig is None:
            fig = go.FigureWidget()
            
            fig_layout = go.Layout(title_text = title,
                                   title_x = 0.5,
                                   title_xanchor = 'center',
                                   title_y = 0.85)
            fig.update_layout(fig_layout) 
            
            fig_scene = go.layout.Scene(camera_projection_type = "orthographic")
            fig.update_scenes(fig_scene)

        # Plot trajectories
        if self.orbit_type == "periodic":
            for i in range(self.states.shape[0]):

                prop_state: np.ndarray = self.propagate(i)[1]
            
                if orbit_name is None or i > 0:
                    name = None
                    showlegend_flag = False
                else:
                    name = f"{orbit_name}" # ({str(i+1)})"
                    showlegend_flag = True
                
                trace_orbit = go.Scatter3d(x = prop_state[:,0],
                                           y = prop_state[:,1],
                                           z = prop_state[:,2],
                                           showlegend = showlegend_flag,
                                           name = name,
                                           mode = 'lines',
                                           line_width = 4)
                
                fig.add_trace(trace_orbit)

        if self.orbit_type == "quasi-periodic":

            if idx is None:
                for i in range(self.states.shape[0]):
                    prop_state: np.ndarray = self.propagate(i)[1] # self.propagate(-1)[1]
                    
                    if orbit_name is None or i!= self.states.shape[0]-1:
                            name = None
                            showlegend_flag = False
                    else:
                            name = f"{orbit_name}" # ({str(i+1)})"
                            showlegend_flag = True

                    # if i == self.states.shape[0]-1:
                    trace_orbit = go.Mesh3d(x = prop_state[:,0::6].ravel(),
                                            y = prop_state[:,1::6].ravel(),
                                            z = prop_state[:,2::6].ravel(),
                                            showlegend = showlegend_flag,
                                            alphahull = 5,
                                            name = name,
                                            opacity=0.3)  # TODO: make interpolation with even grid and plot the grid itself with scatter3d
                            

                    fig.add_trace(trace_orbit)

                    # trace_init_point = go.Scatter3d(x = [prop_state[0,0]],
                    #                                 y = [prop_state[0,1]],
                    #                                 z = [prop_state[0,2]],
                    #                                 showlegend = showlegend_flag,
                    #                                 name = name,
                    #                                 mode='markers',
                    #                                 marker_size=3)
                                                    
                    # fig.add_trace(trace_init_point)
            else:
                prop_time, prop_state = self.propagate(idx)
                    
                if orbit_name is None:
                        name = None
                        showlegend_flag = False
                else:
                        name = f"{orbit_name}" # ({str(i+1)})"
                        showlegend_flag = True

                # 3D Mesh
                # trace_orbit = go.Mesh3d(x = prop_state[:,0::6].ravel(),
                #                         y = prop_state[:,1::6].ravel(),
                #                         z = prop_state[:,2::6].ravel(),
                #                         showlegend = showlegend_flag,
                #                         alphahull = 5,
                #                         name = name,
                #                         opacity=0.3)  # TODO: make interpolation with even grid and plot the grid itself with scatter3d
                #    
                # fig.add_trace(trace_orbit)

                # Grid mesh
                torus_itp = [scitp.RectBivariateSpline(np.linspace(0, 2*np.pi, prop_time.size),  # [:-1],
                                                       np.linspace(0, 2*np.pi, self.N_nodes+1),  # [:-1],
                                                       np.hstack((prop_state[:, i::6], prop_state[:, [i]])))
                             for i in range(6)]
                
                # Build query angles
                th_0_vec = np.linspace(0,2*np.pi,51)  # [:-1]
                th_1_vec = np.linspace(0,2*np.pi,51)  # [:-1]

                # Build strobomaps
                strobo_maps = np.ndarray((0, 6))
                for th_0 in th_0_vec:
                    strobo_map = [[torus_itp[i](th_0, th_1).squeeze() for i in range(6)] for th_1 in th_1_vec]

                    strobo_maps = np.vstack((strobo_maps, strobo_map))
                    strobo_maps = np.vstack((strobo_maps, np.empty((1,6))*np.nan))
                
                trace_strobo = go.Scatter3d(x = strobo_maps[:,0],
                                            y = strobo_maps[:,1],
                                            z = strobo_maps[:,2],
                                            showlegend = showlegend_flag,
                                            name = name,
                                            mode='lines',
                                            line_dash = 'solid',
                                            line_color = 'black',
                                            line_width = 0.5)
                
                fig.add_trace(trace_strobo)
                
                # Build longitudinal lines
                long_maps = np.ndarray((0, 6))
                for th_1 in th_1_vec:

                    th_1_vec_fixed = th_1 - self.periods[idx,-2]/self.periods[idx,-1]*prop_time
                    th_1_vec_fixed %= 2*np.pi

                    long_map = [[torus_itp[i](th_0, th_1).squeeze() for i in range(6)]
                                for th_0, th_1
                                in zip(np.linspace(0, 2*np.pi, prop_time.size), th_1_vec_fixed)]


                    long_maps = np.vstack((long_maps, long_map))
                    long_maps = np.vstack((long_maps, np.empty((1,6))*np.nan))
                
                trace_long = go.Scatter3d(x = long_maps[:,0],
                                          y = long_maps[:,1],
                                          z = long_maps[:,2],
                                          showlegend = False,
                                          name = name,
                                          mode='lines',
                                          line_dash = 'solid',
                                          line_color = 'black',
                                          line_width = 0.5)

                fig.add_trace(trace_long)
                

                
                



        if save_as is not None:
            # fig.write_html(save_as + '.json')
            fig.write_json(save_as + '.json')
        
        if show:
            fig.show()

        return fig
