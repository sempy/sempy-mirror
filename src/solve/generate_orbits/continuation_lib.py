from dataclasses import dataclass
from typing import Any, List, Callable, Tuple, Union

import numpy as np
import scipy.interpolate as spitp
import scipy.optimize as spopt
from numpy import ndarray

from src.propagation.propagator import Propagator
from src.dynamics.dynamical_environment import DynamicalEnvironment
from src.dynamics.cr3bp_environment import CR3BP


# %% Stopping criteria

def _min_reference_distance(states_stack: np.ndarray, prop: Propagator,
                            env: DynamicalEnvironment,
                            reference_position: np.ndarray,
                            distance_threshold: float) -> float:

    # Propagate previous orbit
    propagation_pre = prop(env, np.linspace(0, states_stack[-2, -1], 100), states_stack[-2, :6])
    prop_state_pre = propagation_pre.state_vec
    prop_time_pre = propagation_pre.t_vec


    # Compute normalized relative position of the previous states w.r.t. reference
    rel_pos_pre = prop_state_pre[:, :3] - reference_position[:3]

    # Interpolate previous orbit
    dir_int_pre = spitp.interp1d(prop_time_pre, rel_pos_pre, kind='cubic', axis=0, fill_value="extrapolate")

    # Propagate initial state
    propagation = prop(env, np.linspace(0, states_stack[-1, -1], 100), states_stack[-1, :6])
    prop_state = propagation.state_vec
    prop_time = propagation.t_vec

    # Find closest point to the attractors and index
    # (initial guess)
    rel_pos = prop_state[:, :3] - reference_position[:3]
    rel_dist = np.linalg.norm(rel_pos, axis=1)
    min_dist_idx = np.where(rel_dist == np.min(rel_dist))[0][0]

    # interpolate directions array
    dir_int = spitp.interp1d(prop_time, rel_pos, kind='cubic', axis=0, fill_value="extrapolate")

    # Find true minimum distance points
    # TODO: After Optimization tools are implemented in sempy, substitute this block of code
    bnds = spopt.Bounds(0, prop_time[-1], keep_feasible=True)

    sol_opt = spopt.minimize(lambda t: np.linalg.norm(dir_int(t), axis=1),
                               prop_time[min_dist_idx],
                               method='Nelder-Mead', bounds=bnds, tol=1e-9,
                               options={'maxiter': 100})

    # Check validity of the solution
    if sol_opt.success is False:
        raise ValueError("Failed Minimization")
    else:       
        # Compute normalized direction of min dist point and previous solution point w.r.t. first primary
        t_opt = sol_opt.x
        t_pre = t_opt * states_stack[-2, -1]/states_stack[-1, -1]
        dir_opt = np.squeeze(dir_int(t_opt))
        dir_pre = np.squeeze(dir_int_pre(t_pre))

        # Compute direction sign of the two minima
        dir_sign = np.sign(np.dot(dir_opt, dir_pre))

        # Extract minimum distance values
        min_dist = sol_opt.fun * dir_sign

    return distance_threshold - min_dist


def _max_reference_distance(states_stack: np.ndarray, prop: Propagator,
                            env: DynamicalEnvironment,
                            reference_position: np.ndarray,
                            distance_threshold: float) -> float:

    # Propagate initial state
    propagation = prop(env, np.linspace(0, states_stack[-1, -1], 100), states_stack[-1, :6])
    prop_state = propagation.state_vec
    prop_time = propagation.t_vec

    # Find closest point to the attractors and index
    # (initial guess)
    rel_pos = prop_state[:, :3] - reference_position[:3]
    rel_dist = np.linalg.norm(rel_pos, axis=1)
    max_dist_idx = np.where(rel_dist == np.max(rel_dist))[0][0]

    # interpolate directions array
    dir_int = spitp.interp1d(prop_time, rel_pos, kind='cubic', axis=0)

    # Find true minimum distance points
    # TODO: After Optimization tools are implemented in sempy, substitute this block of code
    bnds = spopt.Bounds(0, prop_time[-1], keep_feasible=True)

    sol_opt = spopt.minimize(lambda t: -np.linalg.norm(dir_int(t), axis=1),
                               prop_time[max_dist_idx],
                               method='Nelder-Mead', bounds=bnds, tol=1e-9,
                               options={'maxiter': 100})

    # Check validity of the solution
    if sol_opt.success is False:
        raise ValueError("Failed Maximization")
    else:       

        # Extract minimum distance values
        max_dist = -sol_opt.fun

    return max_dist - distance_threshold

def _max_static_distance(states_stack: np.ndarray,
                         reference_position: np.ndarray,
                         distance_threshold: float) -> float:
    
    rel_pos = states_stack[-1, :3] - reference_position[:3]
    rel_dist = np.linalg.norm(rel_pos)

    return rel_dist - distance_threshold

def _min_static_distance(states_stack: np.ndarray,
                         reference_position: np.ndarray,
                         distance_threshold: float) -> float:
    
    rel_pos = states_stack[-1, :3] - reference_position[:3]
    rel_dist = np.linalg.norm(rel_pos)

    return distance_threshold - rel_dist

def _max_orbits_count(states_stack: np.ndarray, max_count: int) -> int:

    return states_stack.shape[0] - max_count


class StopCriteriaList:
    """
    Collector of constraints functions.
    """

    @classmethod
    def min_reference_distance(cls, prop: Propagator, env: DynamicalEnvironment,
                               reference_position: np.ndarray,
                               distance_threshold: float):
        return cls._add_args(_min_reference_distance, prop, env,
                             reference_position, 
                             distance_threshold)
    
    @classmethod
    def max_reference_distance(cls, prop: Propagator, env: DynamicalEnvironment,
                               reference_position: np.ndarray,
                               distance_threshold: float):
        return cls._add_args(_max_reference_distance, prop, env,
                             reference_position, 
                             distance_threshold)
    
    @classmethod
    def min_static_distance(cls, reference_position: np.ndarray,
                            distance_threshold: float):
        return cls._add_args(_min_static_distance,
                             reference_position, 
                             distance_threshold)

    @classmethod
    def max_static_distance(cls, reference_position: np.ndarray,
                            distance_threshold: float):
        return cls._add_args(_max_static_distance,
                             reference_position, 
                             distance_threshold)

    @classmethod
    def max_orbits_count(cls, max_count: int):
        return cls._add_args(_max_orbits_count, max_count)

    @staticmethod
    def _add_args(fun, *args: Any):

        # Decorate the function with the user-defined arguments
        def decorated_fun(states_stack: np.ndarray):
            # Return the decorated function

            return fun(states_stack, *args)

        # Rename the function with the constraint's name
        decorated_fun.__name__ = fun.__name__[1:]

        # Return the decorated function
        return decorated_fun


class BuildStopCriteria:

    def __init__(self, criteria_list: List[Callable]):
        """Initialize the constraints."""

        self._criteria_list = criteria_list
        self.names = [c.__name__ for c in criteria_list]
        self.criteria_fstack = self._stack_criteria()

    def _stack_criteria(self):
        def criteria_fstack(states_stack: np.ndarray):
            res_stack = np.empty(0)
            for c in self._criteria_list:
                res_stack = np.append(res_stack, c(states_stack))

            return res_stack
        return criteria_fstack

    def __call__(self, states_stack):
        return self.criteria_fstack(states_stack)   


# %% Continuation Criteria

def _fix_element(variables: np.ndarray, pred_state: np.ndarray,
                 ref_states: np.ndarray, jac_flag: bool,
                 index_list: List[int]) -> Tuple[Any, List[List[int]]]:

    target_values = pred_state[index_list]
    
    res = variables[index_list] - target_values

    if jac_flag:
        jac = [[1 if j == index_list[i] else 0
                for j in range(len(variables))]
               for i in range(len(index_list))]
        return res, jac
    
    return res


def _fix_distance(variables: np.ndarray,
                  pred_state: np.ndarray,
                  ref_states: np.ndarray,
                  jac_flag: bool,
                  index_list: List[int]) -> Tuple[Union[float, Any], ndarray]:

    # Relative vector
    rel_vec = variables[index_list] - ref_states[-1, index_list]

    # Current distance value
    distance = np.linalg.norm(rel_vec)

    # Check if distance is zero
    if distance == 0:
        raise ValueError("The norm of the selected state's elements is zero.")

    # target distance value
    target_distance = np.linalg.norm(pred_state[index_list] - ref_states[-1, index_list])
    
    # residual
    res = distance - target_distance

    if jac_flag:
        
        jac = np.zeros((1, len(variables)))
        jac[0, index_list] = rel_vec / distance

        return res, jac
    
    return res


class ContinuationCriterion:

    @classmethod
    def fix_element(cls, idx_fix: List[int]):
        return cls._add_args(_fix_element, idx_fix)
    
    @classmethod
    def fix_distance(cls, idx_fix: List[int]):
        return cls._add_args(_fix_distance, idx_fix)

    @staticmethod
    def _add_args(fun, *args):

        # Decorate the function with the user-defined arguments
        def decorated_fun(state, pred_state, ref_states, jac_flag):
            # Return the decorated function

            return fun(state, pred_state, ref_states, jac_flag, *args)

        # Rename the function with the constraint's name
        decorated_fun.__name__ = fun.__name__[1:]

        # Return the decorated function
        return decorated_fun


# %% Bifurcation Criteria

# Bifurcation types
def _tangent_bifurcation(alpha: float, beta: float):

    return 2 + beta + 2*alpha

def _period_doubling_bifurcation(alpha: float, beta: float):

    return 2 + beta - 2*alpha

def _period_tripling_bifurcation(alpha: float, beta: float):

    return beta - alpha - 1

def _period_quadrupling_bifurcation(alpha: float, beta: float):

    return beta - 2

def _secondary_hopf_bifurcation(alpha: float, beta: float):

    return beta - 2 - 0.25*alpha**2

@dataclass
class BifurcationTypes:
    tangent: Callable = _tangent_bifurcation
    period_doubling: Callable = _period_doubling_bifurcation
    period_tripling: Callable = _period_tripling_bifurcation
    period_quadrupling: Callable = _period_quadrupling_bifurcation
    secondary_hopf: Callable = _secondary_hopf_bifurcation


# Bifurcation search class
def _get_bifurcation_params(state, prop: Propagator, environment:DynamicalEnvironment):

    # Extract initial state and period
    x0 = state[:6]
    tau0 = state[-1]

    # Extend initial state with stm
    x0_ext = np.append(x0, np.ravel(np.eye(6)))

    # Propagate for one orbit
    prop_state = prop(environment, [0, tau0], x0_ext, with_stm=True)

    # Extract final stm
    stm = np.reshape(prop_state.state_vec[-1, 6:], (6, 6))
    
    # Compute Broucke parameters
    alpha = 2 - np.trace(stm)
    beta = 0.5*(alpha**2 + 2 - np.trace(stm @ stm))

    return alpha, beta

def _find_bifurcation(state: np.ndarray, prop: Propagator,
                      environment:DynamicalEnvironment,
                      bifurcation_fun: Callable):

    alpha, beta = _get_bifurcation_params(state, prop, environment)

    return bifurcation_fun(alpha, beta)


class Bifurcations:

    @classmethod
    def get_params(cls, state: np.ndarray, prop: Propagator):
        return cls._add_args(_get_bifurcation_params, prop)

    @classmethod
    def find_bifurcation(cls, prop: Propagator, environment:DynamicalEnvironment,
                         bifurcation_fun: Callable):
        return cls._add_args(_find_bifurcation, prop, environment, bifurcation_fun)
    
    @staticmethod
    def _add_args(fun, *args):

        # Decorate the function with the user-defined arguments
        def decorated_fun(state):
            # Return the decorated function

            return fun(state, *args)

        # Rename the function with the constraint's name
        decorated_fun.__name__ = fun.__name__[1:]

        # Return the decorated function
        return decorated_fun


# %% Interpolation Criteria

# Interpolation functions
def _distance(state: np.ndarray, target_value: float, reference_point: np.ndarray): # TODO: Add interpolation function based on the minimum distance between 2 trajectories

    distance = np.linalg.norm(state[:3]-reference_point[:3])

    return (distance - target_value)**2 

def _element(state: np.ndarray, target_value: float, idx: int):

    return (state[idx] - target_value)**2 


def _jacobi(state: np.ndarray, target_value: float, env: CR3BP):

    return (env.get_jacobi(state[:6]) - target_value)**2


class InterpFunctions:

    @classmethod
    def distance(cls, reference_point: np.ndarray):
        return cls._add_args(_distance, reference_point)
    
    @classmethod
    def element(cls, idx: int):
        return cls._add_args(_element, idx)
    
    @classmethod
    def jacobi(cls, env: CR3BP):
        return cls._add_args(_jacobi, env)

    @staticmethod
    def _add_args(fun, *args):

        # Decorate the function with the user-defined arguments
        def decorated_fun(state, target_value):

            # Return the decorated function
            return fun(state, target_value, *args)

        # Rename the function with the constraint's name
        decorated_fun.__name__ = fun.__name__[1:]

        # Return the decorated function
        return decorated_fun
