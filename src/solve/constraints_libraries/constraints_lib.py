from typing import Any, Collection
from src.dynamics.dynamical_environment import DynamicalEnvironment
from src.propagation.propagator import Propagator
import numpy as np
import src.solve.constraints_libraries.constraints_fun as cfun

from multiprocessing import Pool

# %%
class ConstraintsLib:
    """Constraints library class, collects constraints by category."""

    @staticmethod
    def _add_args(fun, *args: Any):
        """Set the constraints functions with user-defined arguments.

        Parameters
        ----------
        fun : Callable
            Function to be called.
        *args : Any
            Arguments to be passed to the function.
        """

        # Decorate the function with the user-defined arguments
        def residuals(variables, jac_flag):
            # Return the decorated function

            return fun(variables, *args, jac_flag=jac_flag)

        # Rename the function with the constraint's name
        residuals.__name__ = fun.__name__

        # Return the decorated function
        return residuals

# %%

# Here, the class that will be calle d by the users is defined.
# The class, named "ConstraintsLib" contains as many methods as the number of
# constraints defined in the previous section
#
# The methods follow the following rules:
#
#   1 - The methods shall be decorated as @classmethod, hence their first
#       input shall be "cls"
#
#   2 - The method's name can be the same as the constraint function's that
#       the method refers to, without the initial underscore.
#       A different name can be used, as long as the user can understand from
#       the name what that specific function does
#
#   3 - The inputs shall be the same as the constraint function, made
#       exception for "variables" and "jac_flag", which shall not be
#       included here
#
#   4 - The method returns a call to the private method "_add_args" (already
#       implemented in this template), that receives as input the name of
#       the external fuction (including the initial underscore) and the
#       inputs of the method
#
#   5 - It is HIGHLY suggested to include the type hins also in the method, in
#       the same way they are written in the corresponding called function

class PeriodicLib(ConstraintsLib):
    """
    Collector of constraints functions.
    """

    @classmethod
    def state_match_mirror(cls, environment: DynamicalEnvironment,
                           propagator: Propagator):
        return cls._add_args(cfun.state_match_mirror, environment,
                             propagator)

    @classmethod
    def state_match(cls, environment: DynamicalEnvironment,
                    propagator: Propagator):
        return cls._add_args(cfun.state_match, environment,
                             propagator)

# %%
class PoincareLib(ConstraintsLib):
    """
    Collector of constraints functions.
    """

    @classmethod
    def fix_elements(cls, index_list: Collection[int],
                     value_list: Collection[float]):
        return cls._add_args(cfun.fix_elements, index_list, value_list)

    @classmethod
    def fix_diff_norm(cls, origin: Collection[float], index_list: Collection[int],
                      trg_val: float):
        return cls._add_args(cfun.fix_diff_norm, origin, index_list, trg_val)

    @classmethod
    def geom_state_match(cls, index_state_1: int, index_state_2: int):
        return cls._add_args(cfun.geom_state_match, index_state_1, index_state_2)

    @classmethod
    def fix_jacobi(cls, trg_val: float, environment):
        return cls._add_args(cfun.fix_jacobi, trg_val, environment)

# %%
class QuasiPeriodicLib(ConstraintsLib):
    """
    Collector of constraints functions.
    """

    @classmethod
    def strobo_map_match(cls, origin: np.ndarray,
                         environment: DynamicalEnvironment,
                         propagator: Propagator,
                         pool: Pool = None):
        return cls._add_args(cfun.strobo_map_match, origin, environment,
                             propagator, pool)
    
# %%
class OptimizeLib(ConstraintsLib):

    @classmethod
    def timestate_node_match(cls, environment: DynamicalEnvironment,
                    propagator: Propagator):

        return cls._add_args(cfun.timestate_node_match, environment,
                             propagator)

    @classmethod
    def timestateepochs_node_match(cls, environment: DynamicalEnvironment,
                                   propagator: Propagator):

        return cls._add_args(cfun.timestateepochs_node_match, environment,
                             propagator)

    @classmethod
    def timestate_node_match_half(cls, propagator: Propagator):
        return cls._add_args(cfun.timestate_half_node_match, propagator)
