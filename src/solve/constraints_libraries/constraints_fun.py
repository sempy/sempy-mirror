from typing import Collection, Tuple, Union, List, Any
import numpy as np
import scipy as sp

from src.dynamics.dynamical_environment import DynamicalEnvironment
from src.dynamics.cr3bp_environment import CR3BP
from src.propagation.propagator import Propagator

from multiprocessing import Pool

# Here, all the functions can be implemented, with the following rules:
#
#  1 - The name shall have an initial underscore, to prevent the direct
#      access of the function from the library
#
#  2 - The first input shall be the set of variables the functions
#      computes the residuals with
#
#  3 - It is HIGHLY recommended tu use the typing package for type hints
#      of the function's inputs
#
#  4 - All implemented functions shall have the boolean input named
#      jac_flag that allows to evaluat the jacobian if true. If no
#      jacobian is provided, set a default value to False



def task(args):
    
    j, s_i, jac_flag, epoch_part, propagator, t_span, env = args

    # Find initial state of node j
    s_i_j = s_i[6 * j:6 * j + 6]



    if jac_flag:
        # Append initial STM to the single state
        s_i_j = np.append(s_i_j, np.eye(6).ravel())

        if epoch_part:
            # Append initial epoch partials to the single state
            s_i_j = np.append(s_i_j, np.zeros(6))

            kwargs = {'with_epoch_partials': True}
        else:
            kwargs = {}

        # Propagate state with STM (0 corresponds to initial epoch of the first state)
        s_f_j = propagator(env, t_span[j], s_i_j,
                           with_stm=True,
                           **kwargs).state_vec[-1, :]

        # Save final state
        s_f = s_f_j[:6]

        # Extract STM
        stm = (s_f_j[6:42].reshape(6, 6),)

        if epoch_part:
            # Extract epoch partials
            e_part = s_f_j[42:48]
        else:
            e_part = np.empty(0)

        # Compute time derivative of the initial and final states
        t_i_j = t_span[j, 0]
        t_f_j = t_span[j, 1]
        s_f_j = np.array(s_f_j)
        s_i_dot_j = env.ode(t_i_j, s_i_j, with_stm=True) if epoch_part else env.ode(t_i_j, s_i_j[:6])
        s_f_dot_j = env.ode(t_f_j, s_f_j, with_stm=True) if epoch_part else env.ode(t_f_j, s_f_j[:6])

        # State derivatives with respect to state variables
        s_i_dot = s_i_dot_j[:6]
        s_f_dot = s_f_dot_j[:6]

    else:
        # Propagate state without STM
        s_f_j = propagator(env, t_span[j], s_i_j).state_vec[-1]

        # Save final state
        s_f = s_f_j

        stm = ()
        e_part = np.empty(0)
        s_i_dot = np.empty(0)
        s_f_dot = np.empty(0)

    return s_f, stm, s_i_dot, s_f_dot, e_part


### GENERIC FUNCTIONS
def _multi_prop(env: DynamicalEnvironment,
                propagator: Propagator,
                s_i: np.ndarray,
                t_span: np.array,
                jac_flag: bool,
                epoch_part: bool=False,
                pool: Pool = None):
    """
    Propagate the given states for the given time spans.

    Parameters
    ----------
    s_i : np.ndarray
        State vector(s) to be propagated (N_nodes * 6 x 1).
    t_span : np.ndarray
        Time span vector (N_nodes x 2).
    propagator : Propagator
        Instance of the Propagator class, containing the propagation setup and
    jac_flag : bool
        Boolean to specify is analytical jacobian matrix shall be provided by the function.
    epoch_part : bool, optional
        Boolean to specify if the 48-dimensional state vector is passed. If True, the
        jac_flag is always set to True. Default is False.

    Returns
    -------
    res : dict
        Keys:
            s_f : np.ndarray
                Final states (6 or (6*(N_nodes - 1)) or (8 * N_nodes -1))
            stm_stack : np.ndarray
                Stack of STM matrices ((6x6) or (6x6x(N_nodes - 1)) or (6x6x(8
            s_i_dot : np.ndarray (6 or (6*(N_nodes - 1)) or (8 * N_nodes -1))
                Time derivative of the initial state
            s_f_dot : np.ndarray (6 or (6*(N_nodes - 1)) or (8 * N_nodes -1))
                Time derivative of the propagated state
            epoch_partials : np.ndarray
                Partial derivatives of the epoch w.r.t. the initial state
    """

    # Extract number of nodes to propagate
    N_nodes_prop = t_span.shape[0]

    # Initialize output
    s_f = np.zeros_like(s_i)
    stm_stack = (tuple() if jac_flag else None)
    s_i_dot = (np.zeros_like(s_i) if jac_flag else None)  # Time derivative of the initial state
    s_f_dot = (np.zeros_like(s_i) if jac_flag else None)  # Time derivative of the propagated state
    e_part = (np.zeros_like(s_i) if epoch_part else None)

    args = [(j, s_i, jac_flag, epoch_part, propagator, t_span, env)
            for j in range(N_nodes_prop)]

    results_stack = []
    if pool is not None:
        for result in pool.map(task, args):
            results_stack.append(result)
    else:
        for result in map(task, args):
            results_stack.append(result)

    for j in range(N_nodes_prop):

        # Extract results from loop
        s_f_j = results_stack[j][0]  # s_f, final state
        stm_j = results_stack[j][1]  # stm, STM matrix
        s_i_dot_j = results_stack[j][2]  # s_i_dot, time derivative of the initial state
        s_f_dot_j = results_stack[j][3]  # s_f_dot, time derivative of the propagated state
        e_part_j = results_stack[j][4]  # e_part, partial derivatives of the epoch w.r.t. the initial state

        # Append results
        s_f[6*j : 6*(j+1)] = s_f_j[:6]
        if jac_flag:
            stm_stack = stm_stack + stm_j
            s_i_dot[6*j : 6*(j+1)] = s_i_dot_j[:6]
            s_f_dot[6*j : 6*(j+1)] = s_f_dot_j[:6]

            if epoch_part:
                e_part[6*j : 6*(j+1)] = e_part_j


    results = {'s_f': s_f}
    if jac_flag:
        results['stm_stack'] = stm_stack
        results['s_f_dot'] = s_f_dot
        results['s_i_dot'] = s_i_dot
        if epoch_part:
            results['epoch_partials'] = e_part
    return results

### POINCARE
def fix_elements(variables: Collection[float], index_list: Collection[int],
                  value_list: Collection[float], jac_flag: bool = False)\
        -> Union[Tuple[list, List[List[int]]], list]:
    """
    Constraint function to fix elements of the state

    Parameters
    ----------
    variables : Collection[float]
        problem's variables to be optimized
    index_list : Collection[int]
        indexes of the variables to be fixed
    value_list : Collection[float]
        target values of the variables
    jac_flag : bool, optional
        boolean to provide also jacobian matrix as an output, together with the residuals,
        by default False

    Returns
    -------
    Tuple[Collection, Collection]
        function residuals value, matrix of residuals derivatives w.r.t. variables
    """

    # Cast positive indexes for negative input indexes
    index_list = [i if i >= 0 else len(variables) + i for i in index_list]

    # Compute residuals for the indexes of fixed variables
    res = [variables[j]-value_list[i] for i, j in enumerate(index_list)]

    if jac_flag:
        # Compute Jacobian matrix
        jac = [[1 if j == index_list[i] else 0
                for j in range(len(variables))]
               for i in range(len(index_list))]

        return res, jac

    return res


def fix_diff_norm(variables: Collection[float], origin: Collection[float],
                   index_list: Collection[int], trg_val: float,
                   jac_flag: bool = False)\
            -> Union[float, Tuple[float, Collection[float]]]:
    """
    Constraint function to fix elements of the state

    Parameters
    ----------
    variables : Collection[float]
        problem's variables to be optimized
    origin : Collection[float]
        origin of the vector to be fixed
    index_list : Collection[int]
        indexes of the variables to be fixed
    trg_val : float
        target value of the norm of the vector
    jac_flag : bool, optional
        boolean to provide also jacobian matrix as an output, together with the residuals,
        by default False

    Returns
    -------
    Tuple[Collection, Collection]
        function residuals value, matrix of residuals derivatives w.r.t. variables
    """

    # Compute norm of the difference between variables and origin,
    # for the selected indexes
    dist = np.sqrt(sum([(variables[j]-origin[i]) ** 2 for i, j
                        in enumerate(index_list)]))

    # Compute the residual w.r.t. the target norm value
    res = dist - trg_val

    if jac_flag:
        # Compute Jacobian matrix
        jac = np.zeros((1, len(variables)))
        jac[0, index_list] = [(variables[j]-origin[i])/dist
                              for i, j
                              in enumerate(index_list)]

        return res, jac
    return res

def fix_jacobi(variables: Collection[float], trg_val: float,
               environment: CR3BP,
               jac_flag: bool = False)\
            -> Union[float, Tuple[float, Collection[float]]]:
    """
    Constraint function to fix jacobi constant

    Parameters
    ----------
    variables : Collection[float]
        problem's variables to be optimized
    trg_val : float
        target value of the norm of the vector
    environment: CR3BP
        dynamical environment data
    jac_flag : bool, optional
        boolean to provide also jacobian matrix as an output, together with the residuals,
        by default False

    Returns
    -------
    Tuple[Collection, Collection]
        function residuals value, matrix of residuals derivatives w.r.t. variables
    """

    if environment.kind != 'CR3BP':
        raise ValueError('The Jacobi constant is defined only for CR3BP')

    # Compute norm of the difference between variables and origin,
    # for the selected indexes
    cjac = environment.get_jacobi(variables[:6])

    # Compute the residual w.r.t. the target norm value
    res = cjac - trg_val

    if jac_flag:
        # Compute Jacobian matrix
        jac = np.zeros((1, len(variables)))
        jac[0, :3] = -2*environment.get_u_bar_first_partials(variables[:6])
        jac[0, 3:6] = -2*environment.ode(0, variables[:6])[3:]

        return res, jac
    return res


### GEOMETRICAL
def geom_state_match(variables: np.ndarray, index_state_1, index_state_2, jac_flag: bool = False):
    """
    """

    # Extract initial (full) state and time
    s_1 = variables[index_state_1:index_state_1 + 6]
    s_2 = variables[index_state_2:index_state_2 + 6]

    # Compute residual (shifting points to match)
    res = s_1 - s_2

    # Return output (residual only or residual + jacobian)
    if jac_flag:

        # Initialize jacobian
        jac = np.zeros((len(s_1), len(variables)))

        # Assemble jacobian matrix
        np.fill_diagonal(jac[:, index_state_1:index_state_1 + 6], 1)
        np.fill_diagonal(jac[:, index_state_2:index_state_2 + 6], -1)
        return res, jac
    return res


### PERIODIC
def state_match_mirror(variables: np.ndarray,
                        environment: DynamicalEnvironment,
                        propagator: Propagator, jac_flag: bool = False) \
        -> Tuple[Any, np.ndarray]:
    """
    Mirror theorem evaluation to ensure orbit periodicity.

    Paramenters
    ----------
    variables : np.ndarray
        Reduced state vector and half period of the orbit
        ([x0, z0, v0, tau/2]).
    environment : DynamicalEnvironment
        Instance of the environment containing the dynamical system's
        characteristics, parameters, and equations of motions
    propagator : Propagator
        Instance of the Propagator class, containing the propagation setup and
        the propagation method.
    jac_flag : bool
        Boolean to specify is analytical jacobian matrix shall be provided by
        the function.

    Returns
    -------
    res : np.ndarray
        Vector of the residuals.
    jac : np.ndarray
        Jacobian matrix of the residuals function w.r.t. the variables
    """

    # Extract initial state and time
    s_i = np.zeros(6)
    s_i[[0, 2, 4]] = variables[0:3]
    tau_h = variables[-1]

    # Add fixed elements of the state (for propagation)
    # s_i = np.insert(s_i, [1, 3, 5], 0)

    # Extend state if jacobian is required
    if jac_flag:
        s_i = np.append(s_i, np.eye(6).ravel())
        with_stm = True
    else:
        with_stm = False

    # State Propagation
    prop_sol = propagator(environment, [0, tau_h], s_i, with_stm=with_stm)

    # Final state
    s_f = prop_sol.state_vec[-1, 0:6]

    # Compute residual
    res = s_f[[1, 3, 5]]

    # Return output (residual only or residual + jacobian)
    if jac_flag:
        # Initialize jacobian
        jac = np.ndarray((3, 4))

        # Extract and reshape STM from propagation
        stm = prop_sol.state_vec[-1, 6:42].reshape(6, 6)

        # Extract STM elements of interest
        # (for mirror theorem)
        stm_red = stm[[1, 3, 5], :][:, [0, 2, 4]]

        # Compute time derivative of the final state
        # and extract elements of interest (for mirror theorem)
        dot_s_f = environment.ode(0, s_f)[[1, 3, 5]]

        # Build jacobian matrix
        jac[:, 0:3] = stm_red
        jac[:, -1] = dot_s_f

        return res, jac
    return res


def state_match(variables: np.ndarray, environment: DynamicalEnvironment,
                 propagator: Propagator, jac_flag: bool = False) \
        -> Tuple[Any, np.ndarray]:
    """
    Final/Initial states match to ensure orbit periodicity.

    Paramenters
    ----------
    variables : np.ndarray
        Full state vector and full period of the orbit
        ([x0, y0, z0, u0, v0, w0, tau]).
    environment : DynamicalEnvironment
        Instance of the environment containing the dynamical system's
        characteristics, parameters, and equations of motions
    propagator : Propagator
        Instance of the Propagator class, containing the propagation setup and
        the propagation method.
    jac_flag : bool
        Boolean to specify is analytical jacobian matrix shall be provided by
        the function.

    Returns
    -------
    res : np.ndarray
        Vector of the residuals.
    jac : np.ndarray
        Jacobian matrix of the residuals function w.r.t. the variables
    """

    # Extract initial (full) state and time
    s_i = variables[:-1]
    tau = variables[-1]

    # Extract number of nodes
    N_nodes = len(s_i) // 6

    # Create the time span array
    t_span = np.tile((0, tau/N_nodes), (N_nodes, 1))

    # State Propagation
    prop_sol = _multi_prop(environment, propagator, s_i, t_span, jac_flag)

    # Final state
    s_f = prop_sol['s_f']

    # Compute residual (shifting points to match)
    res = s_f - np.roll(s_i, shift=-6)

    # Return output (residual only or residual + jacobian)
    if jac_flag:

        # Initialize jacobian
        jac = np.zeros((len(s_i), len(variables)))

        # Build stm stack in the jacobian matrix
        if N_nodes == 1:

            # Extract STM
            stm = prop_sol['stm_stack']

            # Assemble jacobian components related to STM
            jac_stm = stm - np.eye(6)

        else:
            # Extract STM stack
            stm_stack = prop_sol['stm_stack']

            # Assemble jacobian components related to STM
            jac_stm = sp.linalg.block_diag(*stm_stack)

            i, j = np.indices(jac_stm.shape)
            jac_stm[j == i + 6] = -1
            jac_stm[i == j + len(s_i) - 6] = -1

        # Extract time derivative of the propagated state
        s_f_dot = prop_sol['s_f_dot']

        # Assemble jacobian matrix
        jac[:, :-1] = jac_stm
        jac[:, -1] = s_f_dot

        return res, jac
    return res

### QUASI-PERIODIC
def _phase_conditions(variables: np.ndarray, origin: np.ndarray, N_nodes,
                      DFT_mat: np.ndarray, DFT_mat_inv: np.ndarray,
                      k: np.ndarray, environment: DynamicalEnvironment, jac_flag):
    # # Extract initial nodes
    # s_i = variables[0:-2]
    # # tau = variables[-2]
    # # rho = variables[-1]

    # Compute derivatives of reference map wrt theta1 (phase angle)
    ds_ref_dtheta1 = (np.kron(((1j * k.transpose()) * DFT_mat_inv) @ DFT_mat,
                              np.eye(6)) @ origin[:-2]).real

    # Compute derivatives of reference map wrt theta0 (longitude angle)
    omega0_ref = 2 * np.pi / origin[-1]
    omega1_ref = origin[-2] / origin[-1]

    f = np.empty(0)
    for i in range(N_nodes):
        f = np.append(f, environment.ode(0, origin[6 * i:6 * i + 6]))

    ds_ref_dtheta0 = 1 / omega0_ref * (f - omega1_ref * ds_ref_dtheta1)

    # Compute the two phase constraints
    ph_th0 = sum((variables[:-2] - origin[:-2]) * ds_ref_dtheta0) / N_nodes
    ph_th1 = sum(variables[:-2] * ds_ref_dtheta1) / N_nodes

    # Build residual
    res = np.append(ph_th0, ph_th1)

    if jac_flag:
        jac = np.zeros((2, len(variables)))

        jac[0, :-2] = ds_ref_dtheta0 / N_nodes
        jac[1, :-2] = ds_ref_dtheta1 / N_nodes

        return res, jac
    return res,


def strobo_map_match(variables: np.ndarray, origin: np.ndarray,
                      environment: DynamicalEnvironment,
                      propagator: Propagator,
                      pool: Pool = None,
                      jac_flag: bool = False) \
        -> Union[Tuple[np.ndarray, np.ndarray], np.ndarray]:
    """
    Stroboscopic map nodes match for quasi peridoc orbits.
    variables = [x0 y0 z0 u0 v0 w0 x1 y1 ... tau, rho]
    """

    # Extract initial nodes
    s_i = variables[0:-2]
    tau = variables[-1]
    rho = variables[-2]

    # Find number of nodes (specific for state and epoch problem)
    N_nodes = len(variables)//6

    # Create the time span array
    t_span = np.tile((0, tau), (N_nodes, 1))

    # Propagate all nodes
    sol_prop = _multi_prop(environment, propagator, s_i, t_span, jac_flag)

    # Extract final states of all nodes (and the STM if required)
    s_f = sol_prop['s_f']

    # Compute Fourier Transform
    k = (np.array(range(-(N_nodes - 1) // 2, (N_nodes - 1) // 2 + 1))
         if N_nodes % 2 == 1
         else np.array(range(-N_nodes // 2, N_nodes // 2)))[:, np.newaxis]

    DFT_mat = np.exp(-1j * 2 * np.pi * k * np.array(range(0, N_nodes)) / N_nodes)
    DFT_mat_inv = np.linalg.inv(DFT_mat)

    Rho_mat = np.diag(np.exp(-1j * rho * k[:, 0]))

    # Full dense rotation matrix
    Rot_mat_dense = np.real(DFT_mat_inv @ Rho_mat @ DFT_mat)

    # Build the rotation matrix for all nodes and elements
    Rot_mat = np.kron(Rot_mat_dense, np.eye(6))

    # Compute strobo map match
    map_match = Rot_mat.dot(s_f) - s_i

    # Compute phase constraints
    phase_fix = _phase_conditions(variables, origin, N_nodes,
                                  DFT_mat, DFT_mat_inv,
                                  k, environment, jac_flag=jac_flag)

    # Assemble residuals
    res = np.append(map_match, phase_fix[0])

    if jac_flag:
        stm_stack = sol_prop['stm_stack']
        dot_s_f = sol_prop['s_f_dot']

        jac = np.zeros((len(s_i) + 2, len(variables)))

        # Create block diag STM
        jac_stm = sp.linalg.block_diag(*stm_stack)

        # Compute rotation matrix derivative wrt rho
        dRho_mat_drho = np.diag(-1j * k[:, 0]) * Rho_mat

        dRot_mat_drho_dense = np.real(DFT_mat_inv @ dRho_mat_drho @ DFT_mat)
        dRot_mat_drho = np.kron(dRot_mat_drho_dense, np.eye(6))

        # Populate jacobian matrix
        jac[0:len(s_i), 0:len(s_i)] = Rot_mat @ jac_stm - \
                                      np.eye(6 * N_nodes)
        jac[0:len(s_i), -2] = Rot_mat.dot(dot_s_f)
        jac[0:len(s_i), -1] = dRot_mat_drho.dot(s_f)
        jac[len(s_i):, :] = phase_fix[1]

        return res, jac
    return res

### OPTIMIZATION
def timestate_node_match(variables: np.ndarray,
                          environment: DynamicalEnvironment,
                          propagator: Propagator, jac_flag: bool = False):
    """
    Node timestates match.

    Paramenters
    ----------
    variables : np.ndarray
        State vectors and epochs in the form
        [x0, y0, z0, u0, v0, w0, ..., xn, yn, zn, un, vn, wn, t0, ..., tn].
    environment : DynamicalEnvironment
        Instance of the environment containing the dynamical system's
        characteristics, parameters, and equations of motions
    propagator : Propagator
        Instance of the Propagator class, containing the propagation setup and
        the propagation method.
    jac_flag : bool
        Boolean to specify is analytical jacobian matrix shall be provided by
        the function.

    Returns
    -------
    res : np.ndarray
        Vector of the residuals.
    jac : np.ndarray
        Jacobian matrix of the residuals function w.r.t. the variables

    Comments
    --------
    The Jacobian in this case can be written with 2 sub matrices in a 1 x 2 shape.
    J = [[Jss, Jse]]  [6(N_nodes-1) x 7(N_nodes)]
    Where:
        - Jss = dR_s / dX  [6(N_nodes-1) x 6(N_nodes)]  (state-state)
        - Jse = dR_s / dt  [6(N_nodes-1) x (N_nodes)]  (state-epoch)
    """

    # Find numbers (specific for state and epoch problem)
    N_variables = len(variables)
    N_nodes = N_variables//7
    N_states = 6 * N_nodes
    N_epochs = N_nodes
    N_constraints = 6 * (N_nodes- 1)
    N_segments = N_nodes - 1

    # Extract initial (full) state and relative times
    s_i = variables[0:N_states]

    tau_vec = variables[-N_epochs:]

    # Create the time span array
    t_span = np.zeros((N_segments, 2))
    for j in range(t_span.shape[0]):
        t_span[j, :] = np.array([tau_vec[j], tau_vec[j+1]])

    # State Propagation
    prop_sol = _multi_prop(environment, propagator, s_i[:-6], t_span, jac_flag)

    # Final states (6 * (N_nodes - 1))
    s_f = prop_sol['s_f']

    # Compute residuals X1_prop - X2, ..., X_{N-1}_prop - X_N (6 * (N_nodes - 1) in total)
    res = s_f - s_i[6:]

    # Return output (residual only or residual + jacobian)
    if jac_flag:

        # Initialize jacobian
        jac_state_state = np.zeros((N_constraints, N_states))
        jac_state_epoch = np.zeros((N_constraints, N_epochs))

        # Extract STM stack
        stm_stack = prop_sol['stm_stack']

        # Extract time derivative of the initial state in the jacobian
        s_i_dot = prop_sol['s_i_dot']

        # Extract time derivative of the propagated state
        s_f_dot = prop_sol['s_f_dot']

        # Write the diagonal with the STM
        jac_state_state[:, :N_constraints] = sp.linalg.block_diag(*stm_stack)

        for i in range(N_segments):
            # Write the superdiagonal with the identity matrix (with the correct sign)
            jac_state_state[6*i:6*i+6, 6*i+6:6*i+12] = - np.eye(6)

            # Write the diagonal with the time derivative of the initial state multiplied by the STM
            jac_state_epoch[6*i:6*i+6, i] = - stm_stack[i].dot(s_i_dot[6*i:6*i+6])

            # Write the diagonal with the time derivative of the propagated state
            jac_state_epoch[6*i:6*i+6, i+1] = s_f_dot[6*i:6*i+6]

        # Assemble jacobian
        jac = np.hstack((jac_state_state, jac_state_epoch))

        return res, jac

    return res

def timestateepochs_node_match(variables: np.ndarray,
                         environment: DynamicalEnvironment,
                         propagator: Propagator, jac_flag: bool = False):
    """
    Node timestates match with additional constraint on epoch.

    Paramenters
    ----------
    variables : np.ndarray
        State vectors, propagation times and epochs in the form
        [x0, y0, z0, u0, v0, w0, ..., xn, yn, zn, un, vn, wn, T0, ..., Tn-1, t0, ..., tn].
    environment : DynamicalEnvironment
        Instance of the environment containing the dynamical system's
        characteristics, parameters, and equations of motions
    propagator : Propagator
        Instance of the Propagator class, containing the propagation setup and
        the propagation method.
    jac_flag : bool
        Boolean to specify is analytical jacobian matrix shall be provided by
        the function.

    Returns
    -------
    res : np.ndarray
        Vector of the residuals.
    jac : np.ndarray
        Jacobian matrix of the residuals function w.r.t. the variables

    Comments
    --------
    The Jacobian in this case can be written with 6 sub matrices in a 2 x 3 shape.
    J = [[Jss, Jst, Jse], [Jes, Jet, Jee]]  [7(N_nodes-1) x (8(N_nodes) - 1)]
    Where:
        - Jss = dR_s / dX  [6(N_nodes-1) x 6(N_nodes)]  (state-state)
        - Jst = dR_s / dT  [6(N_nodes-1) x (N_nodes - 1)] (state-propagation time)
        - Jse = dR_s / dt  [6(N_nodes-1) x (N_nodes)]    (state-epoch)
        - Jes = dR_t / dX  [(N_nodes - 1) x 6(N_nodes)] (epoch-state)
        - Jet = dR_t / dT  [(N_nodes - 1) x (N_nodes - 1)]  (epoch-propagation time)
        - Jee = dR_t / dt  [(N_nodes - 1) x (N_nodes)]  (epoch-epoch)

        The Jacobian matrix has the following form:

                 [     |     |     ]
                 [ Jss | Jst | Jse ]
        DF(X) =  [     |     |     ]
                 [-----------------]
                 [ Jes | Jet | Jee ]
                 [-----------------]

    """

    # Find numbers (specific for state and epoch problem)
    N_variables = len(variables)
    N_nodes = (N_variables+1) // 8
    N_states = 6 * N_nodes
    N_constraints_state = 6*(N_nodes - 1)
    N_constraints_epoch = N_nodes - 1
    N_delta_t = N_nodes - 1
    N_epochs = N_nodes

    # Extract initial (full) state and relative times
    s_i = variables[:N_states]
    delta_t = variables[-(N_epochs + N_delta_t):-N_epochs]
    tau_vec = variables[-N_epochs:]

    # Create the time span array for integration
    t_span = np.zeros((N_delta_t, 2))
    for j in range(t_span.shape[0]):
        t_span[j, :] = np.array([tau_vec[j], tau_vec[j] + delta_t[j]])

    # State Propagation
    prop_sol = _multi_prop(environment, propagator, s_i[:-6], t_span, jac_flag, epoch_part=True)

    # Final states (6 * (N_nodes - 1))
    s_f = prop_sol['s_f']

    # Compute residuals X1_prop - X2, ..., X_{N-1}_prop - X_N (6 * (N_nodes - 1) in total)
    res_state = s_f - s_i[6:]

    # Compute residuals tau_1 - (tau_0 + delta_t_0), ..., tau_{N-1} - (tau_{N-2} + delta_t_{N-2})
    res_epoch = tau_vec[1:] - tau_vec[:-1] - delta_t

    # Assemble residuals
    res = np.hstack((res_state, res_epoch))

    # Return output (residual only or residual + jacobian)
    if jac_flag:
        # Extract STM stack
        stm_stack = prop_sol['stm_stack']

        # Extract time derivative of the propagated state
        s_f_dot = prop_sol['s_f_dot']

        # Extract epoch partials
        epoch_partials = prop_sol['epoch_partials']

        # Initialize jacobian submatrices
        jac_state_state = np.zeros((N_constraints_state, N_states))
        jac_state_proptime = np.zeros((N_constraints_state, N_delta_t))
        jac_state_epoch = np.zeros((N_constraints_state, N_epochs))
        jac_epoch_epoch = np.zeros((N_constraints_epoch, N_epochs))

        # Write the diagonal with the STM
        jac_state_state[:, :N_constraints_state] = sp.linalg.block_diag(*stm_stack)

        for i in range(N_nodes-1):
            # Write the superdiagonal with the identity matrix (with the correct sign)
            jac_state_state[6*i:6*i+6, 6*i+6:6*i+12] = - np.eye(6)

            # Fill the state_proptime jacobian
            jac_state_proptime[6*i:6*i+6, i] = s_f_dot[6*i:6*i+6]

            # Fill the state_epoch jacobian
            jac_state_epoch[6*i:6*i+6, i] = epoch_partials[6*i:6*i+6]

            # Fill the time_epoch jacobian
            jac_epoch_epoch[i, i] = -1
            jac_epoch_epoch[i, i + 1] = 1

        jac_epoch_states = np.zeros((N_constraints_epoch, N_states))
        jac_epoch_proptime = - np.eye(N_constraints_epoch, N_delta_t)

        # Assemble the jacobian
        jac = np.vstack((np.hstack((jac_state_state, jac_state_proptime, jac_state_epoch)),
                            np.hstack((jac_epoch_states, jac_epoch_proptime, jac_epoch_epoch))))
        return res, jac

    return res

### OPTIMIZATION
def timestate_half_node_match(variables: np.ndarray,
                              environment: DynamicalEnvironment,
                          propagator: Propagator, jac_flag: bool = False):

    # Find numbers (specific for state and epoch problem)
    N_variables = len(variables)
    N_nodes = N_variables//7
    N_states = 6 * N_nodes
    N_epochs = N_nodes
    N_constraints = 6 * (N_nodes- 1)
    N_segments = N_nodes - 1

    # Extract initial (full) state and relative times
    s_i = variables[0:N_states]
    s_i_fwd = s_i[:-6]
    s_i_bwd = s_i[6:]

    tau_vec = variables[-N_epochs:]

    delta_t = tau_vec[1:] - tau_vec[:-1]

    # Create the time span array
    t_span_fwd = np.zeros((N_segments, 2))
    t_span_bwd = np.zeros((N_segments, 2))
    for j in range(N_segments):
        t_span_fwd[j, :] = np.array([tau_vec[j], tau_vec[j] + delta_t[j]/2])
        t_span_bwd[j, :] = np.array([tau_vec[j+1], tau_vec[j+1] - delta_t[j]/2])

    # State Propagation (forward)
    prop_sol_fwd = _multi_prop(environment, propagator, s_i_fwd, t_span_fwd, jac_flag)
    prop_sol_bwd = _multi_prop(environment, propagator, s_i_bwd, t_span_bwd, jac_flag)

    # Final half states (6 * (N_nodes - 1))
    s_h_fwd = prop_sol_fwd['s_f']
    s_h_bwd = prop_sol_bwd['s_f']

    # Compute residuals X1_prop_fwd - X2_prop_bwd, ..., X_{N-1}_prop_fwd - X_N_prop_bwd (6 * (N_nodes - 1) in total)
    res = s_h_fwd - s_h_bwd

    # Return output (residual only or residual + jacobian)
    if jac_flag:

        # Initialize jacobian
        jac_state_state = np.zeros((N_constraints, N_states))
        jac_state_epoch = np.zeros((N_constraints, N_epochs))

        # Extract STM stack
        stm_stack_fwd = prop_sol_fwd['stm_stack']
        stm_stack_bwd = prop_sol_bwd['stm_stack']

        # Extract time derivative of the initial state in the jacobian
        # epoch_part_fwd = prop_sol_fwd['epoch_partials']
        # epoch_part_bwd = prop_sol_bwd['epoch_partials']

        # Extract the accelerations at starting points
        s_i_fwd_dot = prop_sol_fwd['s_i_dot']
        s_i_bwd_dot = prop_sol_bwd['s_i_dot']

        # Write the diagonal with the STM
        jac_state_state[:, :N_constraints] = sp.linalg.block_diag(*stm_stack_fwd)
        jac_state_state[:, 6:] = - sp.linalg.block_diag(*stm_stack_bwd)

        for i in range(N_segments):
            # Write the diagonal with the time derivative of the initial state multiplied by the STM
            jac_state_epoch[6*i:6*i+6, i] = stm_stack_fwd[i] @ s_i_fwd_dot[6*i:6*i+6]

            # Write the diagonal with the time derivative of the propagated state
            jac_state_epoch[6*i:6*i+6, i+1] = - stm_stack_bwd[i] @ s_i_bwd_dot[6*i:6*i+6]

        # Assemble jacobian
        jac = np.hstack((jac_state_state, jac_state_epoch))

        return res, jac

    return res
