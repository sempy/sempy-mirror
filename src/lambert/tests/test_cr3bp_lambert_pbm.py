"""
Test Lambert Solver in the CR3BP force model.

"""

import unittest
import numpy as np

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.orbits.halo import Halo
from src.lambert.cr3bp_lambert_pbm import Cr3bpLambertPbm
from src.init.constants import DAYS2SEC


class TestCr3bpLambertPbm(unittest.TestCase):
    """Unittests for Cr3bpLambertPbm class. """

    def test_cr3bp_lambert_pbm_state(self):
        """Unittest for state1 and state2 kwargs. """

        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)  # dynamical model

        # departure and arrival orbits
        halo1 = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, T=3.148346324314712)
        halo2 = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, T=1.509121761710611)
        halo1.interpolation()
        halo2.interpolation()

        # Cr3bpLambertPbm object
        lambert_pbm = Cr3bpLambertPbm(cr3bp, Primary.MOON, halo1, halo2, precision=1e-11)

        # departure and arrival states
        state1 = np.array([1.073984719398772e+00, 1.319658367932023e-01, -2.955122291976050e-02,
                           9.286027981079438e-02, 4.507033244924556e-02, -2.311022948663541e-01])
        state2 = np.array([9.996924049487665e-01, -4.179990795082401e-02, -8.403308192292476e-02,
                           -7.172096889866261e-02, 1.627477554914851e-02, 3.875385952082813e-01])

        # velocity vectors at the transfer's endpoints after dV1 and prior to dV2
        vel1 = np.array([7.138078287699647e-02, -2.492842323069733e-02, -2.978467684160258e-01])
        vel2 = np.array([-1.205512529948437e-01, 8.758059962094453e-02, 3.833670736485118e-01])

        tof = 1.634636585553805  # time of flight
        dv_tot = 1.855993419711140e-01  # total dV magnitude

        # Lambert Problem solution
        sol = lambert_pbm.solve(tof, verbose=False, state1=state1, state2=state2)
        np.testing.assert_allclose(sol[2][0, 3:6], vel1, rtol=0.0, atol=1e-6)
        np.testing.assert_allclose(sol[2][-1, 3:6], vel2, rtol=0.0, atol=1e-6)
        np.testing.assert_allclose(sol[5][-1], dv_tot, rtol=0.0, atol=1e-6)

    def test_cr3bp_lambert_pbm_theta(self):
        """Unittest for theta1 and theta2 kwargs.

        Reference solution from Davis et al. ‘Locally Optimal Transfers Between Libration Point
        Orbits Using Invariant Manifolds’. Advances in the Astronautical Sciences, vol. 135, 2009.

        """
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)  # dynamical model
        t_c = cr3bp.T / 2.0 / np.pi  # characteristic time for adimensionalization

        # departure and arrival orbits
        halo1 = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, T=7.5 * DAYS2SEC / t_c)
        halo2 = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, T=13.6 * DAYS2SEC / t_c)
        halo1.interpolation()
        halo2.interpolation()

        # departure and arrival positions
        theta1 = 1.07 / 7.5
        theta2 = 4.51 / 13.6

        # Cr3bpLambertPbm object
        lambert_pbm = Cr3bpLambertPbm(cr3bp, orbit1=halo1, orbit2=halo2, precision=1e-11)

        # Lambert Problem solution
        sol = lambert_pbm.solve(12.68 * DAYS2SEC / t_c, nb_revs=1, nb_pts=10, guess='stack',
                                verbose=False, theta1=theta1, theta2=theta2)
        np.testing.assert_allclose(sol[5][0], 48.0 / 1e3 * t_c / cr3bp.L, rtol=0.0, atol=1e-3)
        np.testing.assert_allclose(sol[5][1], 69.01 / 1e3 * t_c / cr3bp.L, rtol=0.0, atol=1e-3)


if __name__ == '__main__':
    unittest.main()
