# pylint: disable=too-many-locals
"""
Lambert Problem in an arbitrary force model.

"""

import numpy as np
from numpy.linalg import LinAlgError
from pykep.core.core import lambert_problem

import src.init.defaults as dft
from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.init.ephemeris import Ephemeris

from src.propagation.patch_points_propagator import PatchPointsPropagator
from src.propagation.r2bp_propagator import R2bpLagrangianPropagator
from src.solve.multiple_shooting import MultipleShooting


class LambertPbm:
    """Lambert Problem in an arbitrary force model.

    The Lambert's Problem in orbital mechanics is stated as follows:

    Given an initial position `P1`, a final position `P2` and a time of flight `TOF` find the
    transfer arc or arcs that connects `P1` to `P2` in the specified `TOF`.

    If the governing dynamics is given, typical outputs are the two velocity vectors `V1` and `V2`
    at `P1` and `P2` that together with the imposed `TOF` fully characterize the transfer arc.

    In this implementation, the initial and final states `(P1, V10)` and `(P2, V20)` are imposed
    so that the required departure and injection maneuvers on the initial and target trajectories
    are also computed as:

        1) `DV1 = V1 - V10`
        1) `DV2 = V20 - V2`

    If the solution of the above-stated problem is well known in the Restricted Two-Body Problem
    (R2BP) framework, no analytical approach exits to determine the transfer trajectory in more
    complex dynamical models such as the Circular Restricted Three-Body Problem (CR3BP) or the
    N-body ephemeris model. For the lasts, a solution is obtained after applying a differential
    correction procedure on approximate patch points states and times extracted from a suitable
    initial guess.

    The required initial guess is computed here from a R2BP approximation and then iteratively
    corrected until a smooth enough transfer arc is obtained in the target force model. Either
    single shooting or multiple shooting procedures are supported to generate continuous transfer
    trajectories with imposed initial position `P1`, final position `P2` and time of flight `TOF`.

    Other methods for initial guess estimation might also be implemented taking advantage of the
    given dynamical model and required transfer characteristics.

    Parameters
    ----------
    model : Cr3bp or Ephemeris
        `Cr3bp` or `Ephemeris` object that defines the dynamical model.
    epoch_constr : bool, optional
        Whether additional constraints to enforce epoch continuity between subsequent patch points
        are added to the problem formulation (True) or not (False). If model is a `Cr3bp` object
        and `epoch_constr` is set to ``True`` and exception is raised. Default is ``False``.
    ref : str, optional
        Reference frame name as defined by the NAIF's SPICE Toolkit.
        If model is a `Cr3bp` object this parameter is ignored. Default is ``J2000``.
    obs : Primary or None, optional
        Primary body whose position coincides with the origin of the reference frame in which the
        dynamics is propagated. If model is a `Cr3bp` object this parameter is ignored.
        Default is ``None`` for which the first Primary object in `model` is selected.
    t_c : float, optional
        Characteristic time for adimensionalization. Default is ``1.0``.
    l_c : float, optional
        Characteristic length for adimensionalization. Default is ``1.0``.
    central_body : Primary or None, optional
        Primary object to be considered as the main attractor for the computation of the required
        initial guess in the R2BP framework. If `None`, alternative methods for the initial guess
        computation must be implemented by the corresponding `LambertPbm` child classes.
        Default is `None`.
    precision : float, optional
        Precision at which the differential correction procedure is terminated.
        Default is given by `src.defaults.multiple_shooting_precision`.
    maxiter : int, optional
        Maximum number of iterations performed by the targeting scheme.
        Default is given by `src.defaults.multiple_shooting_maxiter`.
    rtol : float, optional
        Relative tolerance for integration routines. Default is `src.init.defaults.rtol`.
    atol : float, optional
        Absolute tolerance for integration routines. Default is `src.init.defaults.atol`.
    time_steps : int, optional
        Number of points equally spaced in time in which the solution is returned. Default defined
        in `src.init.defaults`.
    max_internal_steps : int, optional
        Maximum number of (internally defined) steps allowed for each integration segments.
        Default defined in `src.init.defaults`.

    Attributes
    ----------
    model : Cr3bp or Ephemeris
        `Cr3bp` or `Ephemeris` object that defines the dynamical model.
    central_body : Primary or None
        Primary object to be considered as the main attractor for the computation of the required
        initial guess in the R2BP framework. If `None`, alternative methods for the initial guess
        computation must be implemented by the corresponding `LambertPbm` child classes.
    diff_corr : MultipleShooting
        MultipleShooting object to perform a differential correction procedure on approximate
        patch points states and times.
    kep_prop : R2bpLagrangianPropagator
        Keplerian propagator to compute the initial guess patch points states and times in the
        R2BP approximation or `None`.
    pp_prop : PatchPointsPropagator
        Patch point propagator to integrate the resulting transfer arc on a suitable
        discretization grid.

    """

    def __init__(self, model, epoch_constr=False, ref='J2000', obs=None, t_c=1.0, l_c=1.0,
                 central_body=None, precision=dft.multiple_shooting_precision,
                 maxiter=dft.multiple_shooting_maxiter, rtol=dft.rtol, atol=dft.atol,
                 time_steps=dft.time_steps, max_internal_steps=dft.max_internal_steps):
        """Inits LambertPbm class. """

        if not isinstance(model, (Cr3bp, Ephemeris)):
            raise Exception('model must be either a Cr3bp or Ephemeris object')
        if not (central_body is None or isinstance(central_body, Primary)):
            raise Exception('central body must be a Primary object or None')
        self.model, self.central_body = model, central_body
        self.diff_corr = MultipleShooting(model, epoch_constr, ref, obs, t_c, l_c, precision,
                                          maxiter, rtol, atol, max_internal_steps)
        self.kep_prop = None if central_body is None else \
            R2bpLagrangianPropagator(central_body.GM * (l_c ** -3) * (t_c ** 2), None)
        self.pp_prop = \
            PatchPointsPropagator(model, ref, obs, t_c, l_c, rtol=rtol, atol=atol,
                                  time_steps=time_steps, max_internal_steps=max_internal_steps)

    def solve(self, tof, var_tof=False, nb_revs=0, nb_pts=2, guess='r2bp', clockwise=None,
              procs=dft.procs, verbose=True, **kwargs):
        """Solves the Lambert Problem in an arbitrary force model.

        Refer to the `LambertPbm` class docstring for a detailed description of the Lambert
        Problem formulation and solution procedures.

        Departure and arrival positions are specified through keyword arguments `state1` and
        `state2` as described below.

        Parameters
        ----------
        tof : float
            Transfer time.
        var_tof : bool, optional
            Whether the total time of flight `tof` is considered as a constant (False) or a
            free variable (True) by the differential correction procedure. Default is `False`.
        nb_revs : int, optional
            Number of complete revolutions about the main attractor. Default is zero.
        nb_pts : int, optional
            Number of patch points to be considered for the differential correction procedure.
            Default is `2` which corresponds to a single shooting technique where only the
            departure and arrival positions are taken into account.
        guess : str, optional
            Method used to compute an approximate solution to be set as initial guess for the
            differential correction procedure. Default is `r2bp` for R2BP approximation.
        clockwise : bool or None, optional
            Whether to consider prograde motion (False) or retrograde motion (True) for the R2BP
            approximation set as initial guess. Default is None for which the appropriate
            direction is determined based on the initial angular momentum vector.
        procs : int, optional
            Number of spawn worker processes when propagating subsequent trajectory segments
            in parallel or `None` for serial propagation.
            Default is the number of physical cores on Linux or None on Windows.
        verbose : bool, optional
            Whether printing the error norm at each iteration (True) or not (False).
            Default is `True`.

        Other Parameters
        ----------------
        state1 : ndarray
            Departure state before the first impulsive manoeuvre.
        state2 : ndarray
            Arrival state after the second impulsive manoeuvre.

        Returns
        -------
        tof : float or None
            Transfer time or None if the procedure has failed.
        t_corr : ndarray
            Corrected time at patch points.
        state_corr : ndarray
            Corrected states at patch points.
        dv1 : ndarray
            First impulsive manoeuvre.
        dv2 : ndarray
            Second impulsive manoeuvre.
        dv_mag : ndarray
            `dv1`, `dv2` and total `dv` magnitudes.
        state1 : ndarray
            Departure state before the first manoeuvre.
        state2 : ndarray
            Arrival state after the second manoeuvre.

        """
        if guess == 'r2bp':
            try:  # compute R2BP approximation and perform differential correction on patch points
                t_patch, state_patch = self.r2bp_approx(tof, kwargs['state1'], kwargs['state2'],
                                                        clockwise, nb_revs, nb_pts)
                tof, t_corr, state_corr = \
                    self.correct(t_patch, state_patch, var_tof, procs, verbose)
            except (RuntimeError, ValueError) as err:  # catch errors thrown by pyKep methods
                print('PyKep error: ', err)
                tof, t_corr, state_corr = \
                    None, np.full(nb_pts, np.inf), np.full((nb_pts, 6), np.inf)
        elif guess == 'cont':
            t_patch, state_patch = self.cont_approx(tof, kwargs['state1'], kwargs['state2'],
                                                    kwargs['t_patch'], kwargs['state_patch'])
            tof, t_corr, state_corr = self.correct(t_patch, state_patch, var_tof, procs, verbose)
        else:
            raise Exception('guess must be either r2bp or cont')
        dv1, dv2, dv_mag = self.get_dvs(kwargs['state1'], kwargs['state2'], state_corr)
        return tof, t_corr, state_corr, dv1, dv2, dv_mag, kwargs['state1'], kwargs['state2']

    def solve_min_pts(self, tof, var_tof=False, nb_revs=0, nb_pts=(2, 2), guess='r2bp',
                      clockwise=None, procs=dft.procs, verbose=True, **kwargs):
        """Solves the Lambert Problem with the lowest possible number of intermediate patch points.

        Refer to the `LambertPbm` class and its `solve` method docstring for a detailed
        description of the Lambert Problem formulation and solution procedures.

        Departure and arrival positions are specified through keyword arguments `state1` and
        `state2` as described below.

        Parameters
        ----------
        tof : float
            Transfer time.
        var_tof : bool, optional
            Whether the total time of flight `tof` is considered as a constant (False) or a
            free variable (True) by the differential correction procedure. Default is `False`.
        nb_revs : int, optional
            Number of complete revolutions about the main attractor. Default is zero.
        nb_pts : tuple, optional
            Minimum and maximum number of patch points to be considered for the differential
            correction procedure. Default is `(2, 2)` which corresponds to a single shooting
            technique where only the departure and arrival positions are taken into account.
        guess : str, optional
            Method used to compute an approximate solution to be set as initial guess for the
            differential correction procedure. Default is `r2bp` for R2BP approximation.
        clockwise : bool or None, optional
            Whether to consider prograde motion (False) or retrograde motion (True) for the R2BP
            approximation set as initial guess. Default is None for which the appropriate
            direction is determined based on the initial angular momentum vector.
        procs : int, optional
            Number of spawn worker processes when propagating subsequent trajectory segments
            in parallel or `None` for serial propagation.
            Default is the number of physical cores on Linux or None on Windows.
        verbose : bool, optional
            Whether printing the error norm at each iteration (True) or not (False).
            Default is `True`.

        Other Parameters
        ----------------
        state1 : ndarray
            Departure state before the first impulsive manoeuvre.
        state2 : ndarray
            Arrival state after the second impulsive manoeuvre.

        Returns
        -------
        tof : float or None
            Transfer time or None if the procedure has failed.
        t_corr : ndarray
            Corrected time at patch points.
        state_corr : ndarray
            Corrected states at patch points.
        dv1 : ndarray
            First impulsive manoeuvre.
        dv2 : ndarray
            Second impulsive manoeuvre.
        dv_mag : ndarray
            `dv1`, `dv2` and total `dv` magnitudes.
        state1 : ndarray
            Departure state before the first manoeuvre.
        state2 : ndarray
            Arrival state after the second manoeuvre.
        nb_pts : int
            Number of patch points used for the current solution.

        """
        nb_pts_i = nb_pts[0] - 1  # current number of patch points
        while nb_pts_i < nb_pts[1]:  # loop over allowed numbers of patch points
            nb_pts_i += 1  # update current number of patch points
            sol_i = self.solve(tof, var_tof, nb_revs, nb_pts_i, guess, clockwise,
                               procs, verbose, **kwargs)  # solve for the current number of points
            if sol_i[0] is not None:  # solution with current number of patch points has converged
                return sol_i[0], np.asarray([sol_i[1][0], sol_i[1][-1]]), \
                       np.asarray([sol_i[2][0], sol_i[2][-1]]), sol_i[3], sol_i[4], sol_i[5], \
                       sol_i[6], sol_i[7], nb_pts_i
        return None, np.full(2, np.inf), np.full((2, 6), np.inf), np.full(3, np.inf), \
            np.full(3, np.inf), np.full(3, np.inf), np.full(6, np.inf), np.full(6, np.inf), nb_pts_i

    def solve_min_dv(self, tof, var_tof=False, nb_revs=0, nb_pts=(2, 2), guess='r2bp',
                     clockwise=None, procs=dft.procs, verbose=True, **kwargs):
        """Solves the Lambert Problem with the lowest possible cost among all allowed numbers of
        intermediate patch points.

        Refer to the `LambertPbm` class and its `solve` method docstring for a detailed
        description of the Lambert Problem formulation and solution procedures.

        Departure and arrival positions are specified through keyword arguments `state1` and
        `state2` as described below.

        Parameters
        ----------
        tof : float
            Transfer time.
        var_tof : bool, optional
            Whether the total time of flight `tof` is considered as a constant (False) or a
            free variable (True) by the differential correction procedure. Default is `False`.
        nb_revs : int, optional
            Number of complete revolutions about the main attractor. Default is zero.
        nb_pts : tuple, optional
            Minimum and maximum number of patch points to be considered for the differential
            correction procedure. Default is `(2, 2)` which corresponds to a single shooting
            technique where only the departure and arrival positions are taken into account.
        guess : str, optional
            Method used to compute an approximate solution to be set as initial guess for the
            differential correction procedure. Default is `r2bp` for R2BP approximation.
        clockwise : bool or None, optional
            Whether to consider prograde motion (False) or retrograde motion (True) for the R2BP
            approximation set as initial guess. Default is None for which the appropriate
            direction is determined based on the initial angular momentum vector.
        procs : int, optional
            Number of spawn worker processes when propagating subsequent trajectory segments
            in parallel or `None` for serial propagation.
            Default is the number of physical cores on Linux or None on Windows.
        verbose : bool, optional
            Whether printing the error norm at each iteration (True) or not (False).
            Default is `True`.

        Other Parameters
        ----------------
        state1 : ndarray
            Departure state before the first impulsive manoeuvre.
        state2 : ndarray
            Arrival state after the second impulsive manoeuvre.

        Returns
        -------
        tof : float or None
            Transfer time or None if the procedure has failed.
        t_corr : ndarray
            Corrected time at patch points.
        state_corr : ndarray
            Corrected states at patch points.
        dv1 : ndarray
            First impulsive manoeuvre.
        dv2 : ndarray
            Second impulsive manoeuvre.
        dv_mag : ndarray
            `dv1`, `dv2` and total `dv` magnitudes.
        state1 : ndarray
            Departure state before the first manoeuvre.
        state2 : ndarray
            Arrival state after the second manoeuvre.
        nb_pts : int
            Number of patch points used for the current solution.

        """

        sol_i = (None, np.full(2, np.inf), np.full((2, 6), np.inf), np.full(3, np.inf),
                 np.full(3, np.inf), np.full(3, np.inf), np.full(6, np.inf), np.full(6, np.inf),
                 nb_pts[0] - 1)  # current solution
        while sol_i[-1] < nb_pts[1]:  # loop over allowed numbers of patch points
            sol_p = sol_i  # store previous solution
            sol_i = self.solve_min_pts(tof, var_tof, nb_revs, (sol_i[-1] + 1, nb_pts[1]),
                                       guess, clockwise, procs, verbose, **kwargs)
            if sol_i[5][-1] > sol_p[5][-1]:  # previous solution has lower cost
                return sol_p
        return sol_i

    def r2bp_approx(self, tof, state1, state2, clockwise=None, nb_revs=0, nb_pts=2):
        """Computes an initial approximation of the transfer arc in the R2BP force model.

        Initial and final velocities on the transfer trajectory are obtained instantiating a pyKep
        `lambert_problem` object while intermediate patch point states are computed propagating
        the initial state after the first impulsive manoeuvre with a `R2bpLagrangianPropagator`
        class instance.

        See (https://esa.github.io/pykep/documentation/core.html#pykep.lambert_problem) and
        (https://esa.github.io/pykep/documentation/core.html#pykep.propagate_lagrangian) for more
        details on the wrapped pyKep algorithms.

        Parameters
        ----------
        tof : float
            Transfer time.
        state1 : ndarray
            Departure state before the first impulsive manoeuvre.
        state2 : ndarray
            Arrival state after the second impulsive manoeuvre.
        clockwise : bool or None, optional
            Whether to consider prograde motion (False) or retrograde motion (True).
            Default is None for which the appropriate direction is determined based on the initial
            angular momentum vector.
        nb_revs : int, optional
            Number of complete revolutions about the main attractor. Default is zero.
        nb_pts : int, optional
            Number of patch points in which the solution is returned. Default is `2`.

        Returns
        -------
        t_patch : ndarray
            Time at patch points.
        state_patch : ndarray
            States at patch points in the R2BP approximation.

        """
        clockwise = clockwise if clockwise is not None else \
            bool(np.cross(state1[0:3], state1[3:6])[2] < 0.0)
        r2bp_lamb_pbm = lambert_problem(r1=state1[0:3], r2=state2[0:3], tof=tof,
                                        mu=self.kep_prop.mu_r2bp, cw=clockwise, max_revs=nb_revs)
        if nb_pts == 2:  # return initial and final states only
            return np.asarray([0.0, tof]),\
                   np.concatenate((state1[0:3], r2bp_lamb_pbm.get_v1()[-1],
                                   state2[0:3], r2bp_lamb_pbm.get_v2()[-1])).reshape(2, 6)
        t_patch, state_patch = \
            self.kep_prop.propagate(np.linspace(0.0, tof, nb_pts),
                                    np.concatenate((state1[0:3], r2bp_lamb_pbm.get_v1()[-1])))
        return t_patch, np.concatenate((state_patch[:-1].ravel(), state2[0:3],
                                        r2bp_lamb_pbm.get_v2()[-1])).reshape(nb_pts, 6)

    @staticmethod
    def cont_approx(tof, state1, state2, t_patch, state_patch):
        """Computes an initial approximation of the transfer arc adapting given patch points. """
        t_patch = t_patch[0] + (t_patch - t_patch[0]) * tof / (t_patch[-1] - t_patch[0])
        state_patch[0, 0:3], state_patch[-1, 0:3] = state1[0:3], state2[0:3]
        return t_patch, state_patch

    def correct(self, t_patch, state_patch, var_time=False, procs=dft.procs, verbose=True):
        """Performs differential correction on approximated patch points.

        Single shooting or multiple shooting procedure to transition an approximate initial guess
        into a continuous transfer arc. The following constrains are imposed:

            1) continuity constraints on the six components of the state at all patch points.
            2) fix initial position.
            3) fix final position.
            4) fix total time of flight if `var_time` is `False`.

        Parameters
        ----------
        t_patch : ndarray
            Time at patch points.
        state_patch :
            States at patch points.
        var_time : bool, optional
            Whether the total time of flight `tof` is considered as a constant (False) or a
            free variable (True) by the differential correction procedure. Default is `False`.
        procs : int, optional
            Number of spawn worker processes when propagating subsequent trajectory segments
            in parallel or `None` for serial propagation.
            Default is the number of physical cores on Linux or None on Windows.
        verbose : bool, optional
            Whether printing the error norm at each iteration (True) or not (False).
            Default is `True`.

        Returns
        -------
        tof : float or None
            Transfer time or None if the procedure has failed.
        t_corr : ndarray
            Corrected time at patch points.
        state_corr : ndarray
            Corrected states at patch points.

        """

        kwargs = {'pos_fix': (0, -1)} if var_time else {'pos_fix': (0, -1), 't_fix': (0, -1)}
        try:
            t_corr, state_corr, sol = \
                self.diff_corr.correct(t_patch, state_patch, procs=procs, verbose=verbose, **kwargs)
            if sol['err_norm'] < self.diff_corr.precision:  # shooting procedure has converged
                msk = (t_corr >= t_corr[0]) & (t_corr <= t_corr[-1])
                return t_corr[-1] - t_corr[0], t_corr[msk], state_corr[msk]
            return None, np.full(t_patch.shape, np.inf), np.full(state_patch.shape, np.inf)
        except (ValueError, LinAlgError) as err:  # catch differential corrector errors
            print('Differential corrector error: ', err)
            return None, np.full(t_patch.shape, np.inf), np.full(state_patch.shape, np.inf)

    @staticmethod
    def get_dvs(state1, state2, state_corr):
        """Computes the required impulsive maneuvers to perform the transfer.

        Parameters
        ----------
        state1 : ndarray
            Departure state before the first impulsive manoeuvre.
        state2 : ndarray
            Arrival state after the second impulsive manoeuvre.
        state_corr : ndarray
            State vector along the transfer.

        Returns
        -------
        dv1 : ndarray
            First impulsive manoeuvre.
        dv2 : ndarray
            Second impulsive manoeuvre.
        dv_mag : ndarray
            `dv1`, `dv2` and total `dv` magnitudes.

        """

        dv1 = state_corr[0, 3:6] - state1[3:6]  # departure dV
        dv2 = state2[3:6] - state_corr[-1, 3:6]  # injection dV
        dv1_mag, dv2_mag = np.linalg.norm(dv1), np.linalg.norm(dv2)  # dV magnitudes
        return dv1, dv2, np.asarray([dv1_mag, dv2_mag, dv1_mag + dv2_mag])

    def propagate(self, t_corr, state_corr, time_steps=dft.time_steps, procs=dft.procs):
        """Propagates the transfer trajectory patch points.

        Parameters
        ----------
        t_corr : ndarray
            Corrected time at patch points.
        state_corr : ndarray
            Corrected states at patch points.
        time_steps : int, optional
            Number of points equally spaced in time in which the solution is returned between
            subsequent patch points. Default defined in `src.init.defaults`.
        procs : int or None, optional
            Number of spawn worker processes or None for serial implementation.
            Default is the number of physical cores on Linux or None on Windows.

        Returns
        -------
        t_vec : ndarray
            Propagated time vector.
        state_vec : ndarray
            Propagated state vector.

        """

        self.pp_prop.prop.time_steps = time_steps  # update number of inner time steps
        return self.pp_prop.propagate(t_corr, state_corr, procs=procs)
