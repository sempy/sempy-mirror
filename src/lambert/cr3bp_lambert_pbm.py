# pylint: disable=too-many-locals, inconsistent-return-statements
"""
Lambert Problem in the Circular Restricted Three-Body Problem.

"""

import itertools
from multiprocess.pool import Pool
import numpy as np

import src.init.defaults as dft
from src.lambert.lambert_pbm import LambertPbm
from src.orbits.halo import Halo
from src.orbits.nrho import NRHO
from src.propagation.cr3bp_propagator import Cr3bpSynodicPropagator
from src.coc.synodic_inertial import synodic_to_inertial, inertial_to_synodic


class Cr3bpLambertPbm(LambertPbm):
    """Lambert Problem in the Circular Restricted Three-Body Problem.

    The Lambert's Problem in orbital mechanics is stated as follows:

    Given an initial position `P1`, a final position `P2` and a time of flight `TOF` find the
    transfer arc or arcs that connects `P1` to `P2` in the specified `TOF`.

    If the governing dynamics is given, typical outputs are the two velocity vectors `V1` and `V2`
    at `P1` and `P2` that together with the imposed `TOF` fully characterize the transfer arc.

    In this implementation, the initial and final states `(P1, V10)` and `(P2, V20)` are imposed
    so that the required departure and injection maneuvers on the initial and target trajectories
    are also computed as:

        1) `DV1 = V1 - V10`
        1) `DV2 = V20 - V2`

    If the solution of the above-stated problem is well known in the Restricted Two-Body Problem
    (R2BP) framework, no analytical approach exits to determine the transfer trajectory in more
    complex dynamical models such as the Circular Restricted Three-Body Problem (CR3BP).
    For the last, a solution is obtained after applying a differential correction procedure
    on approximate patch points states and times extracted from a suitable initial guess.

    The required initial guess is computed here from a R2BP approximation and then iteratively
    corrected until a smooth enough transfer arc is obtained in the target force model. Either
    single shooting or multiple shooting procedures are supported to generate continuous transfer
    trajectories with imposed initial position `P1`, final position `P2` and time of flight `TOF`.

    Other methods for initial guess estimation might also be implemented taking advantage of the
    given dynamical model and required transfer characteristics.

    Parameters
    ----------
    cr3bp : Cr3bp
        `Cr3bp` object.
    central_body : Primary or None, optional
        Primary object to be considered as the main attractor for the computation of the required
        initial guess in the R2BP framework. If `None`, alternative methods for the initial guess
        computation must be available. Default is `None`.
    orbit1 : CrtbpOrbit or None, optional
        Departure CR3BP orbit or `None`. Default is `None`.
    orbit2 : CrtbpOrbit or None, optional
        Arrival CR3BP orbit. Default is `None`.
    precision : float, optional
        Precision at which the differential correction procedure is terminated.
        Default is given by `src.defaults.multiple_shooting_precision`.
    maxiter : int, optional
        Maximum number of iterations performed by the targeting scheme.
        Default is given by `src.defaults.multiple_shooting_maxiter`.
    rtol : float, optional
        Relative tolerance for integration routines. Default is `src.init.defaults.rtol`.
    atol : float, optional
        Absolute tolerance for integration routines. Default is `src.init.defaults.atol`.
    time_steps : int, optional
        Number of points equally spaced in time in which the solution is returned. Default defined
        in `src.init.defaults`.
    max_internal_steps : int, optional
        Maximum number of (internally defined) steps allowed for each integration segments.
        Default defined in `src.init.defaults`.

    Attributes
    ----------
    orbit1 : CrtbpOrbit
        Departure CR3BP orbit.
    orbit2 : CrtbpOrbit
        Arrival CR3BP orbit.
    cr3bp_prop : Cr3bpSynodicPropagator
        CR3BP propagator to compute the initial guess patch points states and times using the
        trajectory stacking approximation.

    """

    def __init__(self, cr3bp, central_body=None, orbit1=None, orbit2=None,
                 precision=dft.multiple_shooting_precision, maxiter=dft.multiple_shooting_maxiter,
                 rtol=dft.rtol, atol=dft.atol, time_steps=dft.time_steps,
                 max_internal_steps=dft.max_internal_steps):
        """Inits Cr3bpLambertPbm. """

        LambertPbm.__init__(self, cr3bp, l_c=cr3bp.L, t_c=cr3bp.T / 2.0 / np.pi,
                            central_body=central_body, precision=precision, maxiter=maxiter,
                            rtol=rtol, atol=atol, time_steps=time_steps,
                            max_internal_steps=max_internal_steps)

        if not (orbit1 is None or orbit2 is None):
            if not (isinstance(orbit1, (Halo, NRHO)) and isinstance(orbit2, (Halo, NRHO))):
                raise Exception('only Halo and NRHO orbits are currently supported')
            if not (orbit1.li.number == orbit2.li.number and orbit1.family == orbit2.family):
                raise Exception('orbit1 and orbit2 must belong to the same family')
        self.orbit1, self.orbit2 = orbit1, orbit2
        self.cr3bp_prop = Cr3bpSynodicPropagator(cr3bp.mu, rtol=rtol, atol=atol, time_steps=None,
                                                 max_internal_steps=max_internal_steps)

    def solve(self, tof, var_tof=False, nb_revs=0, nb_pts=2, guess='r2bp', clockwise=None,
              procs=dft.procs, verbose=True, **kwargs):
        """Solves the Lambert Problem in the CR3BP force model.

        Refer to the `Cr3bpLambertPbm` class docstring for a detailed description of the
        Lambert Problem formulation and solution procedures.

        Departure and arrival positions are specified through keyword arguments `state1` and
        `state2` or `theta1` and `theta2` as described below.

        Parameters
        ----------
        tof : float
            Transfer time. If zero, an approximate value is computed from the initial and final
            orbits' periods.
        var_tof : bool, optional
            Whether the total time of flight `tof` is considered as a constant (False) or a
            free variable (True) by the differential correction procedure. Default is `False`.
        nb_revs : int, optional
            Number of complete revolutions about the main attractor. Default is zero.
        nb_pts : int, optional
            Number of patch points to be considered for the differential correction procedure.
            Default is `2` which corresponds to a single shooting technique where only the
            departure and arrival positions are taken into account.
        guess : str, optional
            Method used to compute an approximate solution to be set as initial guess for the
            differential correction procedure. Supported values are `r2bp` for R2BP approximation
            and `stack` for trajectory stacking. Default is `r2bp`.
        clockwise : bool or None, optional
            Whether to consider prograde motion (False) or retrograde motion (True) for the R2BP
            approximation set as initial guess. Default is None for which the appropriate
            direction is determined based on the initial angular momentum vector.
        procs : int, optional
            Number of spawn worker processes when propagating subsequent trajectory segments
            in parallel or `None` for serial propagation.
            Default is the number of physical cores on Linux or None on Windows.
        verbose : bool, optional
            Whether printing the error norm at each iteration (True) or not (False).
            Default is `True`.

        Other Parameters
        ----------------
        state1 : ndarray
            Departure state before the first impulsive manoeuvre.
        state2 : ndarray
            Arrival state after the second impulsive manoeuvre.
        theta1 : float
            Departure position on the first orbit specified as a fraction of its period in the
            closed interval ``[0, 1]``.
        theta2 : float
            Arrival position on the target orbit specified as a fraction of its period in the
            closed interval ``[0, 1]``.

        Returns
        -------
        tof : float
            Transfer time.
        t_corr : ndarray
            Corrected time at patch points.
        state_corr : ndarray
            Corrected states at patch points.
        dv1 : ndarray
            First impulsive manoeuvre.
        dv2 : ndarray
            Second impulsive manoeuvre.
        dv_mag : ndarray
            `dv1`, `dv2` and total `dv` magnitudes.
        state1 : ndarray
            Departure state before the first manoeuvre.
        state2 : ndarray
            Arrival state after the second manoeuvre.

        """

        if 'state1' in kwargs and 'state2' in kwargs:  # endpoint states specified
            return LambertPbm.solve(self, tof, var_tof, nb_revs, nb_pts, guess,
                                    clockwise, procs, verbose, **kwargs)
        if 'theta1' in kwargs and 'theta2' in kwargs:  # endpoint positions specified
            if guess == 'stack':  # trajectory stacking approximation as initial guess
                t_patch, state_patch, _ = \
                    self.traj_stack_approx(tof, kwargs['theta1'], kwargs['theta2'],
                                           nb_revs, nb_pts, procs)
                tof, t_corr, state_corr = \
                    self.correct(t_patch, state_patch, var_tof, procs, verbose)
                dv1, dv2, dv_mag = self.get_dvs(state_patch[0], state_patch[-1], state_corr)
                return tof, t_corr, state_corr, dv1, dv2, dv_mag, state_patch[0], state_patch[-1]
            # initial guess from R2BP approximation or given patch points states and times
            t_ends, state_ends, delta_theta = \
                self.traj_stack_approx(tof, kwargs['theta1'], kwargs['theta2'],
                                       nb_revs, 2, None)
            kwargs['state1'], kwargs['state2'] = state_ends[0], state_ends[-1]
            return LambertPbm.solve(self, t_ends[-1], var_tof, nb_revs + int(delta_theta // 1),
                                    nb_pts, guess, clockwise, procs, verbose, **kwargs)

    def r2bp_approx(self, tof, state1, state2, clockwise=None, nb_revs=0, nb_pts=2):
        """Computes an initial approximation of the transfer arc in the R2BP force model.

        Initial and final velocities on the transfer trajectory are obtained instantiating a pyKep
        `lambert_problem` object while intermediate patch point states are computed propagating
        the initial state after the first impulsive manoeuvre with a `R2bpLagrangianPropagator`
        class instance.

        See (https://esa.github.io/pykep/documentation/core.html#pykep.lambert_problem) and
        (https://esa.github.io/pykep/documentation/core.html#pykep.propagate_lagrangian) for more
        details on the wrapped pyKep algorithms.

        Parameters
        ----------
        tof : float
            Transfer time.
        state1 : ndarray
            Departure state before the first impulsive manoeuvre.
        state2 : ndarray
            Arrival state after the second impulsive manoeuvre.
        clockwise : bool or None, optional
            Whether to consider prograde motion (False) or retrograde motion (True) for the R2BP
            approximation set as initial guess. Default is None for which the appropriate
            direction is determined based on the initial angular momentum vector.
        nb_revs : int, optional
            Number of complete revolutions about the main attractor. Default is zero.
        nb_pts : int, optional
            Number of patch points in which the solution is returned. Default is `2`.

        Returns
        -------
        t_patch : ndarray
            Time at patch points.
        state_patch : ndarray
            States at patch points in the R2BP approximation.

        """

        _, state12_in = synodic_to_inertial(np.asarray([0.0, tof]), np.vstack((state1, state2)),
                                            self.model, self.central_body)
        t_in, state_in = LambertPbm.r2bp_approx(self, tof, state12_in[0], state12_in[1],
                                                clockwise, nb_revs, nb_pts)
        return inertial_to_synodic(t_in, state_in, self.model, self.central_body)

    def traj_stack_approx(self, tof, theta1, theta2, nb_revs=0, nb_pts=2, procs=dft.procs):
        """Computes an initial approximation of the transfer stacking together orbits' arcs.

        Given the selected number of patch points, an equal amount of CR3BP orbits belonging to the
        same family of `orbit1` and `orbit2` are instantiated and sampled at different positions
        and times to generate a suitable initial guess.

        If `tof` is `None`, the total time of flight is computed as the average between the
        departure ad arrival orbits' periods weighted by the angle `theta2 - theta1` swept during
        the transfer.

        Parameters
        ----------
        tof : float or None
            Transfer time. If `None`, an approximate value is computed from the initial and final
            orbits' periods.
        theta1 : float
            Departure position on the first orbit specified as a fraction of its period in the
            closed interval `[0, 1]`.
        theta2 : float
            Arrival position on the target orbit specified as a fraction of its period in the
            closed interval `[0, 1]`.
        nb_revs : int, optional
            Number of complete revolutions about the main attractor. Default is zero.
        nb_pts : int, optional
            Number of patch points in which the solution is returned. Default is `2`.
        procs : int, optional
            Number of spawn worker processes when propagating subsequent trajectory segments
            in parallel or `None` for serial propagation.
            Default is the number of physical cores on Linux or None on Windows.

        Returns
        -------
        t_patch : ndarray
            Time at patch points.
        state_patch : ndarray
            States at patch points in the trajectory stacking approximation.
        delta_theta : float
            Difference in position swept during the transfer.

        """
        patch_theta, delta_theta = self.get_patch_positions(theta1, theta2, nb_revs, nb_pts)
        patch_orbs = self.get_patch_orbits(nb_pts, procs)  # patch orbits
        state_patch = self.get_patch_states(patch_orbs, patch_theta, procs)  # patch states
        tof = tof if tof > 0.0 else self.get_tof(theta1, theta2, nb_revs)  # time of flight
        return np.linspace(0.0, tof, nb_pts), state_patch, delta_theta

    def get_tof(self, theta1, theta2, nb_revs=0):
        """Estimation of the required transfer time.

        The time of flight approximation is computed as the departure and arrival orbits'
        average period weighted by the difference in position swept during the transfer.

        theta1 : float
            Departure position on the first orbit specified as a fraction of its period in the
            closed interval `[0, 1]`.
        theta2 : float
            Arrival position on the target orbit specified as a fraction of its period in the
            closed interval `[0, 1]`.
        nb_revs : int, optional
            Number of complete revolutions about the main attractor. Default is zero.

        Returns
        -------
        tof : float
            Time of flight approximation.

        """
        theta2 = theta2 + nb_revs if theta2 > theta1 else theta2 + nb_revs + 1.0
        return 0.5 * (self.orbit1.T + self.orbit2.T) * (theta2 - theta1)

    def get_patch_orbits(self, nb_orbs, procs=dft.procs):
        """Returns a list of orbits from which the patch point states are drawn.

        Subsequent orbits are characterized by a constant increment in the corresponding periods.

        Parameters
        ----------
        nb_orbs : int, optional
            Number of orbits to be considered.
        procs : int, optional
            Number of spawn worker processes when propagating subsequent trajectory segments
            in parallel or `None` for serial propagation.
            Default is the number of physical cores on Linux or None on Windows.

        Returns
        -------
        orbits : iterable
            List of orbits.

        """

        if nb_orbs == 2:  # single shooting procedure, only departure and arrival orbits are taken
            return [self.orbit1, self.orbit2]
        period_vec = np.linspace(self.orbit1.T, self.orbit2.T, nb_orbs)[1:-1]  # orbits' periods
        if procs is not None:  # parallel instantiation of intermediate orbits
            with Pool(processes=procs) as pool:
                out = pool.map(self.get_orbit, period_vec)
        else:  # serial instantiation of intermediate orbits
            out = list(map(self.get_orbit, period_vec))
        return [self.orbit1] + out + [self.orbit2]

    @staticmethod
    def get_patch_positions(theta1, theta2, nb_revs=0, nb_pts=2):
        """Returns the patch orbits positions as fractions of the orbital period.

        Parameters
        ----------
        theta1 : float
            Departure position on the first orbit specified as a fraction of its period in the
            closed interval `[0, 1]`.
        theta2 : float
            Arrival position on the target orbit specified as a fraction of its period in the
            closed interval `[0, 1]`.
        nb_revs : int, optional
            Number of complete revolutions about the main attractor. Default is zero.
        nb_pts : int, optional
            Number of patch points in which the solution is returned. Default is `2`.

        Returns
        -------
        patch_theta : ndarray
            Patch point positions specified as fraction of their respective orbits' periods in the
            closed interval `[0, 1]`.
        delta_theta : float
            Difference in position swept during the transfer.

        """
        theta2 = theta2 + nb_revs if theta2 > theta1 else theta2 + nb_revs + 1.0
        patch_theta = np.remainder(np.linspace(theta1, theta2, nb_pts), 1)
        return patch_theta, theta2 - theta1

    def get_patch_states(self, patch_orbits, patch_theta, procs=dft.procs):
        """Returns the patch point states for the single or multiple shooting procedure.

        Parameters
        ----------
        patch_orbits : iterable
            List of orbits from which the patch point states are drawn.
        patch_theta : ndarray
            Patch points positions specified as fractions of their respective orbits' periods.
        procs : int, optional
            Number of spawn worker processes when propagating subsequent trajectory segments
            in parallel or `None` for serial propagation.
            Default is the number of physical cores on Linux or None on Windows.

        Returns
        -------
        state_patch : ndarray
            States at patch points.

        """
        workers_inputs = [(orbit, patch_theta[i]) for i, orbit in enumerate(patch_orbits)]
        if procs is not None:  # parallel computation of patch points states
            with Pool(processes=procs) as pool:
                out = pool.starmap(self.get_orbit_state, workers_inputs)
        else:  # serial computation of patch points states
            out = list(itertools.starmap(self.get_orbit_state, workers_inputs))
        return np.asarray(out)

    def get_orbit(self, period):
        """Returns an orbit of the same family as `orbit1` and `orbit2` with specified period.

        Parameters
        ----------
        period : float
            Orbit's period.

        Returns
        -------
        orbit : CrtbpOrbit
            CrtbpOrbit object.

        """

        if isinstance(self.orbit1, (Halo, NRHO)):
            orbit = Halo(self.orbit1.cr3bp, self.orbit1.li, self.orbit1.family, T=period)
            orbit.interpolation(fix_dim=Halo.DiffCorrFixDim.period)
            return orbit
        raise NotImplementedError('only Halo and NRHO orbits are currently supported')

    def get_orbit_state(self, orbit, theta):
        """Returns the orbit's state at a given position `theta`.

        Parameters
        ----------
        orbit : CrtbpOrbit
            CrtbpOrbit object.
        theta : float
            Position on the given orbit specified as a fraction of its period in the closed
            interval `[0, 1]`.

        Returns
        -------
        state : ndarray
            Orbit's state at `theta`.

        """

        if theta == 0.0:  # return orbit's initial state
            return orbit.state0
        _, state_vec, _, _ = self.cr3bp_prop.propagate([0.0, orbit.T * theta], orbit.state0)
        return state_vec[-1]
