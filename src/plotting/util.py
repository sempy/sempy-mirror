#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""_______________________________Libraries________________________________"""

import numpy as np

"""Required for sphere, circumference, set_axes_equal"""
from mpl_toolkits.mplot3d import Axes3D

"""Required for overWrite_get_proj"""
import matplotlib.pyplot as plt

"""Required for configure_fig3D and configure_fig2D"""


"""_________________________________functions______________________________"""


def sphere(N, radius, center):
    """ Function that build a sphere, it retreives the cartesian x,y,z coordinates that have
    to be given to the method plot_surface() of the class mpl_toolkits.mplot3d.Axes3D in order
    to generate a sphere"""

    """ input parameters : 
        N = number of points with which the sphere will be discretized, if the input is
            N the sphere will be discretized by NxN points 
        radius = radius of the sphere (constant value)
        center = numpy array containing the center coordinates of the sphere
    """
    u = np.linspace(0, 2 * np.pi, N)
    v = np.linspace(0, np.pi, N)
    x = center[0] + radius * np.outer(np.cos(u), np.sin(v))
    y = center[1] + radius * np.outer(np.sin(u), np.sin(v))
    z = center[2] + radius * np.outer(np.ones(np.size(u)), np.cos(v))

    return x, y, z


def circumference(center, radius, step):

    """Function that retrieves the cartesian cordinates of a circumference"""
    """input parameters: 
        center = numpy array containig the center of the circumference
        redius = constant that indicates the radius of the circumference
        step = constant that determines the number of points with which the circumference 
               will be discretised with step < 1 """
    phi = np.arange(0, 6.28, step)
    x = center[0] + radius * np.cos(phi)
    y = center[1] + radius * np.sin(phi)
    return x, y


def set_axes_equal(ax):
    """Make axes of 3D plot have equal scale so that spheres appear as spheres,
    cubes as cubes, etc..  This is one possible solution to Matplotlib's
    ax.set_aspect('equal') and ax.axis('equal') not working for 3D.

    Input
      ax: a matplotlib axis, e.g., as output from plt.gca().
    """

    x_limits = ax.get_xlim3d()
    y_limits = ax.get_ylim3d()
    z_limits = ax.get_zlim3d()

    x_range = abs(x_limits[1] - x_limits[0])
    x_middle = np.mean(x_limits)
    y_range = abs(y_limits[1] - y_limits[0])
    y_middle = np.mean(y_limits)
    z_range = abs(z_limits[1] - z_limits[0])
    z_middle = np.mean(z_limits)

    # The plot bounding box is a sphere in the sense of the infinity
    # norm, hence I call half the max range the plot radius.
    plot_radius = 0.5 * max([x_range, y_range, z_range])

    ax.set_xlim3d([x_middle - plot_radius, x_middle + plot_radius])
    ax.set_ylim3d([y_middle - plot_radius, y_middle + plot_radius])
    ax.set_zlim3d([z_middle - plot_radius, z_middle + plot_radius])


def set_axes_equal2(ax):
    """It provides the same result as "set_axes_equal" but without the need to use 
    numpy array"""
    max_scale = ax.get_xlim3d()[1]
    min_scale = ax.get_xlim3d()[0]

    max_list = [ax.get_xlim3d()[1], ax.get_ylim3d()[1], ax.get_zlim3d()[1]]
    min_list = [ax.get_xlim3d()[0], ax.get_ylim3d()[0], ax.get_zlim3d()[0]]

    for index in range(len(max_list)):
        if max_list[index] > max_scale:
            max_scale = max_list[index]
        if min_list[index] < min_scale:
            min_scale = min_list[index]

    # centering the figure
    mean = (max_scale - min_scale) / 2

    ax.set_xlim3d(left=min_scale, right=max_scale)
    ax.set_ylim3d(bottom=mean - max_scale, top=max_scale - mean)
    ax.set_zlim3d(bottom=mean - max_scale, top=max_scale - mean)


def overWrite_get_proj(ax, scalex, scaley, scalez):
    """Inputs : the axis class , and 3 constants
    It overwrite the function get_proj of matplotlib by modifing the projection matrix of the plot. 
    It stretch one axis with respect to the others depending on the values of scalex,scaley,scalez. 
    For instance, if scalex,scaley,scalez = 2,1,1 the size of x will be twice the size of y and z"""
    ax.get_proj = lambda: np.dot(
        Axes3D.get_proj(ax), np.diag([scalex, scaley, scalez, 1])
    )


def configure_fig3D(
    figure_tag="3D figure", title="", xLabel="x", yLabel="y", zLabel="z", Labelpad=15
):
    fig = plt.figure(num=figure_tag)
    ax = fig.gca(projection="3d")
    ax.set_title(title)
    ax.set_xlabel(xlabel=xLabel, labelpad=Labelpad)
    ax.set_ylabel(ylabel=yLabel, labelpad=Labelpad)
    ax.set_zlabel(zlabel=zLabel, labelpad=Labelpad)


def configure_fig2D(
    figure_tag="2D figure", title="", xLabel="x", yLabel="y", zLabel="z", Labelpad=15
):
    fig = plt.figure(num=figure_tag)
    ax = fig.gca()
    ax.set_title(title)
    ax.set_xlabel(xlabel=xLabel, labelpad=Labelpad)
    ax.set_ylabel(ylabel=yLabel, labelpad=Labelpad)


"""configure_fig3D(title = 'Frame: Inertial\ 1st Primary: ' + self.planet_obj1.Name +'\ 2nd Primary: ' + self.planet_obj2.Name
                            , xLabel= 'X[KM]', yLabel = 'Y[KM]', zLabel = 'Z[KM]', Labelpad = 15 )"""


def reading_Orbitdata_to_plot(data):
    """function whose aim is to read positions, velocities and time instants from a text file and to convert 
    them into a numpy state_vec  matrix"""
    x_list = []
    y_list = []
    z_list = []
    vx_list = []
    vy_list = []
    vz_list = []
    time_list = []

    file_id = open(data, "r")  # The file.txt has been opened

    for line in file_id:
        splitted_line = line.split()
        time_list.append(float(splitted_line[0]))
        x_list.append(float(splitted_line[1]))
        y_list.append(float(splitted_line[2]))
        z_list.append(float(splitted_line[3]))
        vx_list.append(float(splitted_line[4]))
        vy_list.append(float(splitted_line[5]))
        vz_list.append(float(splitted_line[6]))

    # at the end of this cycle positions, velocities and time instants are converted into float and
    # stored into lists

    # x_list ecc will be list of floats
    file_id.close()  # the file.txt has been closed

    # now each list is converted into array to semplificate later computations

    x_vect = np.array(x_list)
    y_vect = np.array(y_list)
    z_vect = np.array(z_list)
    vx_vect = np.array(vx_list)
    vy_vect = np.array(vy_list)
    vz_vect = np.array(vz_list)
    time_vect = np.array(time_list)

    stateV = np.column_stack(
        (x_vect, y_vect, z_vect, vx_vect, vy_vect, vz_vect, time_vect)
    )

    return stateV


def planet_color(name_first_primary, name_second_primary):
    """Chooses the right color and colormap for the primaries.
    
    Args:
        name_first_primary: A string with the name of the first primary written in capital letters
        name_second_primary: A string with the name of the second primary written in capital letters
        
    Returns:
        A list containing colors and colormaps associated to the two primaries. 
        In details: 
            1st element of the list: first primary colormap used for 3D visualization 
            2nd element of the list: second primary colormap used for 3D visualization
            3rd element of the list: first primary color used for 2D visualization
            4th element of the list: second primary colormap used for 2D visualization
        
        
    """

    colormaps_dic = {
        "MERCURY": "Greys",
        "VENUS": "Oranges",
        "SUN": "hot",
        "EARTH": "GnBu",
        "MOON": "Greys",
        "JUPITER": "copper",
        "SATURN": "bone",
        "URANUS": "winter",
        "NEPTUNE": "cool",
        "MARS": "gist_heat",
        "PLUTO": "pink",
    }
    colormap_m1 = ""
    colormap_m2 = ""
    for planet in colormaps_dic.keys():
        if name_first_primary == planet:
            colormap_m1 = colormaps_dic[planet]
        if name_second_primary == planet:
            colormap_m2 = colormaps_dic[planet]

    color_dic = {
        "MERCURY": "saddlebrown",
        "VENUS": "burlywood",
        "SUN": "orange",
        "EARTH": "deepskyblue",
        "MOON": "darkgrey",
        "JUPITER": "darkgoldenrod",
        "SATURN": "darkkhaki",
        "URANUS": "lightskyblue",
        "NEPTUNE": "mediumblue",
        "MARS": "firebrick",
        "PLUTO": "pink",
    }
    color_m1 = ""
    color_m2 = ""
    for planet in color_dic.keys():
        if name_first_primary == planet:
            color_m1 = color_dic[planet]
        if name_second_primary == planet:
            color_m2 = color_dic[planet]

    return [colormap_m1, colormap_m2, color_m1, color_m2]

