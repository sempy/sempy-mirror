#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Circle
from src.plotting.util import set_axes_equal, sphere, planet_color
from src.init.cr3bp import Cr3bp, Primary
from src.orbits.halo import Halo


class PlottingOptions:
    """PlottingOptions class will initialise all the default plotting options.
    
    An object belonging to that class will store in its attributes all the plotting options necessary to plot.
    All the plotting module classes (inner and outer) will have a PlottingOptions object as input parameter.
    
    Parameters
    ----------
    XY : bool
        2D visualization option. When True the XY is displayed. True by default.
    XZ : bool 
        2D visualization option. When True the XZ is displayed. True by default.
    YZ : bool
        2D visualization option. When True the YZ is displayed. True by default.
    TD : bool
        3D visualization option. When True the 3D plot is shown. True by default.
    disp_first_pr : bool
        First Primary visualization option. When True the first Primary is displayed. True by default.
    disp_second_pr : bool
        Second Primary visualization option. When True the second Primary is displayed. True by default.
    N : int
        Number of points with which the spheres (in 3D plots are discretized). 100 by default.
    scale_factor : list
        The first element of that list is a bool, a global switch for the scale factor.
        The second element is a int. It increases the Primaries size for visualization purposes. 
        The primaries appear scale_factor[1] x bigger than they actually are.
        Default = [True, 3]
    l1 : bool
        L1 visualization option. When True l1 is displayed. True by default.
    l2 : bool
        L2 visualization option. When True l2 is displayed. True by default.
    l3 : bool
        L3 visualization option. When True l3 is displayed. False by default. 
    l4 : bool
        L4 visualization option. When True l4 is displayed. False by default.
    l5 : bool
        L5 visualization option. When True l5 is displayed. False by default.
    names : list
        Names visualization option. It displays Primaries and Libration points names.
        The first element of that list is a bool, a global switch for the names variable.
        The second element is a str. 
        When names[1] = 'normal' only a normal text string is put on the figure.
        When names[1] = 'fancy' text names are linked to the object they are representing with a fancy arrow. 
        Default : names = [False, 'normal']
    legend : bool
        Legend visualization option. When True the legend is diplayed. True by default.
    sci : bool
        Label tiks scientific notation option. 
        When True the tiks label on the figure axes are expressed in scientific notation. False by default. 
        
    Attributes
    ----------
    XY : bool
        2D visualization option. When True the XY is displayed. 
    XZ : bool 
        2D visualization option. When True the XZ is displayed. 
    YZ : bool
        2D visualization option. When True the YZ is displayed.     
    twoDviews : dict of {str : bool}   
        Dictionary containing all the 2D visualization options. Used to loop over the 2D views options.
    TD : bool
        3D visualization option. When True the 3D plot is shown. 
    disp_first_pr : bool
        First Primary visualization option. When True the first Primary is displayed.
    disp_second_pr : bool
        Second Primary visualization option. When True the second Primary is displayed.
    N : int
        Number of points with which the spheres (in 3D plots are discretized). 
    scale_factor : list
        The first element of that list is a bool, a global switch for the scale factor.
        The second element is a int. It increases the Primaries size for visualization purposes. 
        The primaries appear scale_factor[1] x bigger than they actually are.    
    Primaries : dict
        Dictionary containing all the Primaries visualization options. Used to loop over the Primaries options.
    l1 : bool
        L1 visualization option. When True l1 is displayed.
    l2 : bool
        L2 visualization option. When True l2 is displayed.
    l3 : bool
        L3 visualization option. When True l3 is displayed.
    l4 : bool
        L4 visualization option. When True l4 is displayed.
    l5 : bool
        L5 visualization option. When True l5 is displayed.
    L_point_list : list of bool
        List containing all the libration points visualization options. Used to loop over the libration points options.
    names : list
        Names visualization option. It displays Primaries and Libration points names.
        The first element of that list is a bool, a global switch for the names variable.
        The second element is a str. 
        When names[1] = 'normal' only a normal text string is put on the figure.
        When names[1] = 'fancy' text names are linked to the object they are representing with a fancy arrow. 
    legend : bool
        Legend visualization option. When True the legend is diplayed.    
    sci : bool
        Label tiks scientific notation option. When True the tiks label on the figure axes are expressed in scientific notation.    
    orbit_color : str
        Color with which the orbit are drawn. When empty every orbit will be drawn with a different color. 
    
    """

    def __init__(
        self,
        XY=True,
        XZ=True,
        YZ=True,
        TD=True,
        disp_first_pr=True,
        disp_second_pr=True,
        N=100,
        scale_factor=[True, 3],
        l1=True,
        l2=True,
        l3=False,
        l4=False,
        l5=False,
        names=[False, "fancy"],
        legend=True,
        sci=False,
        orbit_color="",
    ):
        """Inits PlottingOptions class."""
        # 2D views
        self.XY = XY
        self.XZ = XZ
        self.YZ = YZ
        # attribute used to loop over the 2D views options
        self.twoDviews = {"XY": self.XY, "XZ": self.XZ, "YZ": self.YZ}
        # 3D view
        self.TD = TD
        # Primaries options
        self.disp_first_pr = disp_first_pr
        self.disp_second_pr = disp_second_pr
        self.N = N
        self.scale_factor = scale_factor
        # attribute used to loop over the primaries options
        self.Primaries = {
            "disp_first_pr": self.disp_first_pr,
            "disp_second_pr": self.disp_second_pr,
            "N": self.N,
            "scale_factor": self.scale_factor,
        }
        # Libration points options
        self.l1 = l1
        self.l2 = l2
        self.l3 = l3
        self.l4 = l4
        self.l5 = l5
        # Attribute used to loop over the libration points options
        self.L_point_list = [self.l1, self.l2, self.l3, self.l4, self.l5]
        # Figure options
        self.names = names
        self.legend = legend
        self.sci = sci
        self.orbit_color = orbit_color

    def print_plotting_options(self):
        """Method used to print all the plotting options"""

        print("2D visualization: " + str(self.twoDviews))
        print("3D visualization: " + str(self.TD))
        print(
            "Primaries: {disp_first_pr : "
            + str(self.disp_first_pr)
            + ", disp_second_pr : "
            + str(self.disp_second_pr)
            + " }"
        )
        print("[l1, l2, l3, l4, l5] = " + str(self.L_point_list))
        print("names = " + str(self.names))
        print("legend = " + str(self.legend))
        print("sci = " + str(self.sci))


class MasterPlotter:
    """Manages to plot Planets, Libration points and trajectories (Orbits and Manifolds) in
    relation to a particular Circular Restricted Three body Problem (CRTBP).
    
    The data to plot are generated by its 3 inner classes:
        1) PlotterPlanets (whose aim is to generate data to plot planets).
        2) PlotterLibrationPoints (whose aim is to generate data to plot the libration points).
        3) PlotterOrbitManifolds (whose aim is to generate data to plot Orbits and Manifolds).
    
    Once the data are generated according to the plotting options, two methods of MasterPlotter
    are respectively used to plot the scene in 2D and in 3D.
    
    Parameters
    ----------
    PlottingOptions : object
        An object of the PlottingOption class.
    CrtbpOrbit : object
        An object of the CrtbpOrbit class.
    
    **kwargs
    **key1 : object
        An object of the CrtbpOrbit class, different from the previous.
    **key2 : object
        An object of the CrtbpOrbit class, different from the previous.
        
    Attributes
    ----------
    CrtbpOrbit : object
        An object of the CrtbpOrbit class.
    PlottingOptions : object
        An object of the PlottingOption class.
    Plot_Plantes : object
        An object of the PlotterPlanets class (inner MasterPlotter class).
    Plot_Lagrange : object 
        An object of the PlotterLibrationPoints class (inner MasterPlotter class).
    Plot_Orbit : object
        An object of the PlotterOrbitManifolds class (inner MasterPlotter class).
    current_figures_label_list : list
        List containing the label of the figures that have already been generated in a script.
    figure2D_labels_to_apply : list
        List containing the labels of the 2D figures that MasterPlotter has to generate.
    figure3D_labels_to_apply : list
        List containing the labels of the 2D figures that MasterPlotter has to generate.

    For keyword **key1
    
    **key1 : object
        An additional (optional) object of PlotterOrbitManifolds class (inner MasterPlotter class).
    
    Notes
    -----
    At least one Orbit (or Manifold) has to be given as input parameter to MasterPlotter
    
    Examples
    --------
    The following examples show the MasterPlotter attributes when additional orbits are given
    as additional input paramters. 
    
    >>> plot1 = MasterPlotter(plot_options, orbit)
    >>> print(plot1.__dict__)
    {'CrtbpOrbit': <src.init.orbit.Halo object at 0x7f6b10fab6d8>, 
    'PlottingOptions': <__main__.PlottingOptions object at 0x7f6b11082390>, 
    'kwargs': {}, 
    'Plot_Plantes': <__main__.MasterPlotter.PlotterPlanets object at 0x7f6b10fab748>, 
    'Plot_Lagrange': <__main__.MasterPlotter.PlotterLibrationPoints object at 0x7f6b10fab7b8>, 
    'Plot_Orbit': <__main__.MasterPlotter.PlotterOrbitManifolds object at 0x7f6b12290b00>}
    >>> plot1 = plot1 = MasterPlotter(plot_options, orbit, orbit2 = orbit2, orbit3 = orbit3) 
    >>> print(plot1.__dict__)
    {'CrtbpOrbit': <src.init.orbit.Halo object at 0x7f6b17db3390>,
    'PlottingOptions': <__main__.PlottingOptions object at 0x7f6b12537cc0>, 
    'kwargs': {'orbit2': <__main__.MasterPlotter.PlotterOrbitManifolds object at 0x7f6b11556828>, 
               'orbit3': <__main__.MasterPlotter.PlotterOrbitManifolds object at 0x7f6b11556860>}, 
    'Plot_Plantes': <__main__.MasterPlotter.PlotterPlanets object at 0x7f6b17db3400>, 
    'Plot_Lagrange': <__main__.MasterPlotter.PlotterLibrationPoints object at 0x7f6b17db3470>, 
    'Plot_Orbit': <__main__.MasterPlotter.PlotterOrbitManifolds object at 0x7f6b123306d8>, 
    'orbit2': <__main__.MasterPlotter.PlotterOrbitManifolds object at 0x7f6b11556828>, 
    'orbit3': <__main__.MasterPlotter.PlotterOrbitManifolds object at 0x7f6b11556860>}
        
    """

    def __init__(self, PlottingOptions, CrtbpOrbit, **kwargs):
        """Inits MasterPlotter class."""
        self.CrtbpOrbit = CrtbpOrbit
        self.PlottingOptions = PlottingOptions
        self.kwargs = kwargs

        # Check that every orbit belongs to the same Cr3bp
        for x, y in kwargs.items():
            if y.cr3bp.name != self.CrtbpOrbit.cr3bp.name:
                raise Exception("All the orbits must belong to the same Cr3bp")

        self.Plot_Plantes = self.PlotterPlanets(PlottingOptions, CrtbpOrbit)
        self.Plot_Lagrange = self.PlotterLibrationPoints(PlottingOptions, CrtbpOrbit)
        self.Plot_Orbit = self.PlotterOrbitManifolds(PlottingOptions, CrtbpOrbit)

        # kwargs dictionary is overwritten
        for x, y in self.kwargs.items():
            self.kwargs[x] = self.PlotterOrbitManifolds(PlottingOptions, y)

        # the elements of kwargs becomes also attributes of the class
        self.__dict__.update(self.kwargs)

        # Figure and legend labels
        (
            self.current_figures_label_list,
            self.figure2D_labels_to_apply,
            self.figure3D_labels_to_apply,
        ) = self.figure_label_generator()

    def scene_twoD(self):
        """Generate the 2D scenes.
        
        According to the plotting options it adds all the data (producted by the inner classes)
        to the scenes and shows the 2D figures, taking care of labels, legends and titles.
        """
        # 2D data list are generated
        self.Plot_Plantes.generate_twoD()
        self.Plot_Lagrange.generate_twoD()
        self.Plot_Orbit.generate_twoD()

        # 2D data lists for additional orbits are genereated
        for x, y in self.kwargs.items():
            self.kwargs[x].generate_twoD()

        k = 0
        for view, option in self.PlottingOptions.twoDviews.items():
            if option is True:
                J = []

                fig = plt.figure(num=self.figure2D_labels_to_apply[k])
                ax = fig.gca()

                # Check if the users wants the scientific notation on the label tiks
                if self.PlottingOptions.sci is True:
                    ax.ticklabel_format(style="scientific", scilimits=(0, 0))

                for letters in view:
                    if letters == "X":
                        J.append(0)
                    if letters == "Y":
                        J.append(1)
                    if letters == "Z":
                        J.append(2)
                # add to the scene all the patches that plotterPlanets has created
                for element in self.Plot_Plantes.local_2D_data[k]:
                    ax.add_patch(element)
                # add to the scene all the patches that plotterLibrationPoints has created
                for element in self.Plot_Lagrange.local_2D_data[k]:
                    ax.add_patch(element)
                # add to the scene the right view of the orbit
                ax.plot(
                    self.Plot_Orbit.local_2D_data[k][0],
                    self.Plot_Orbit.local_2D_data[k][1],
                    self.PlottingOptions.orbit_color,
                )
                # add to the scene eventual additional orbit
                for x, y in self.kwargs.items():
                    ax.plot(
                        self.kwargs[x].local_2D_data[k][0],
                        self.kwargs[x].local_2D_data[k][1],
                        self.PlottingOptions.orbit_color,
                    )

                # figure settings indipendent from the plotting options
                ax.set_aspect("equal", adjustable="datalim")
                ax.set_xlabel(xlabel=view[0] + "[km]", labelpad=10)
                ax.set_ylabel(ylabel=view[1] + "[km]", labelpad=5)
                ax.set_title(
                    "CRTBP Synonical Frame: "
                    + view[0]
                    + view[1]
                    + " view \n Primaries: "
                    + self.CrtbpOrbit.cr3bp.name
                )

                # legend
                if self.PlottingOptions.legend is True:
                    # all the lines (trajectory and orbits) within the scene are collected into a list
                    handles_list = ax.lines
                    # all the labels are generated according to the input Orbit given to Masterplotter
                    labels_list = []
                    # The label corresponding to the the first (mandatory) orbit is collected
                    if "Azdim" in self.CrtbpOrbit.kwargs.keys():
                        labels_list.append(
                            self.CrtbpOrbit.kind
                            + ", "
                            + self.CrtbpOrbit.family.name
                            + ", "
                            + "Az_dim =  "
                            + str(round(self.CrtbpOrbit.Azdim, 3))
                        )
                    elif "Cjac" in self.CrtbpOrbit.kwargs.keys():
                        labels_list.append(
                            self.CrtbpOrbit.kind
                            + ", "
                            + self.CrtbpOrbit.family.name
                            + ", "
                            + "Cjac =  "
                            + str(round(self.CrtbpOrbit.C, 3))
                        )
                    # optional additional orbits are considered
                    for x, y in self.kwargs.items():
                        # remeber that self.kwargs are PlotterOrbitManifold objects
                        if "Azdim" in self.kwargs[x].Cr3bpkwargs.keys():
                            labels_list.append(
                                self.kwargs[x].kind
                                + ", "
                                + self.kwargs[x].family
                                + ", "
                                + "Az_dim =  "
                                + str(round(self.kwargs[x].Azdim, 3))
                            )
                        elif "Cjac" in self.kwargs[x].Cr3bpkwargs.keys():
                            labels_list.append(
                                self.kwargs[x].kind
                                + ", "
                                + self.kwargs[x].family
                                + ", "
                                + "Cjac =  "
                                + str(round(self.kwargs[x].C, 3))
                            )
                    # linking each ax.line with its legend label
                    for i in range(len(handles_list)):
                        handles_list[i].set_label(labels_list[i])

                    ax.legend()
                # ________________________________names 'normal'________________________________________________________

                # add Text names below each planet and libration point if required by the plotting options
                if (
                    self.PlottingOptions.names[0] is True
                    and self.PlottingOptions.names[1] == "normal"
                ):
                    # adding primaries names
                    if view != "YZ":
                        for i in range(len(self.Plot_Plantes.local_2D_data[k])):
                            ax.text(
                                self.Plot_Plantes.local_2D_data[k][i]._center[0],
                                self.Plot_Plantes.local_2D_data[k][i]._center[1]
                                - self.Plot_Plantes.local_2D_data[k][i].width * 2,
                                self.Plot_Plantes.primaries_names[i],
                                transform=ax.transData,
                                horizontalalignment="center",
                            )

                    else:
                        for i in range(len(self.Plot_Plantes.local_2D_data[k])):
                            ax.text(
                                self.Plot_Plantes.local_2D_data[k][i]._center[0],
                                self.Plot_Plantes.local_2D_data[k][i]._center[1]
                                - self.Plot_Plantes.local_2D_data[k][i].width / 2,
                                self.Plot_Plantes.primaries_names[i],
                                transform=ax.transData,
                                horizontalalignment="center",
                            )
                    # adding libration points names
                    if view == "XY" or view == "XZ":
                        for i in range(len(self.Plot_Lagrange.local_2D_data[k])):
                            if self.Plot_Lagrange.point_names[i] != "L4":
                                ax.text(
                                    self.Plot_Lagrange.local_2D_data[k][i]._center[0],
                                    self.Plot_Lagrange.local_2D_data[k][i]._center[1]
                                    + self.Plot_Lagrange.local_2D_data[k][i].width * 3,
                                    self.Plot_Lagrange.point_names[i],
                                    transform=ax.transData,
                                    horizontalalignment="center",
                                )
                            else:
                                ax.text(
                                    self.Plot_Lagrange.local_2D_data[k][i]._center[0],
                                    self.Plot_Lagrange.local_2D_data[k][i]._center[1]
                                    - self.Plot_Lagrange.local_2D_data[k][i].width * 3,
                                    self.Plot_Lagrange.point_names[i],
                                    transform=ax.transData,
                                    horizontalalignment="center",
                                )
                    else:  # when view is YZ
                        for i in range(len(self.Plot_Lagrange.local_2D_data[k])):
                            if (
                                self.Plot_Lagrange.point_names[i] == "L4"
                                or self.Plot_Lagrange.point_names[i] == "L5"
                            ):
                                ax.text(
                                    self.Plot_Lagrange.local_2D_data[k][i]._center[0],
                                    self.Plot_Lagrange.local_2D_data[k][i]._center[1]
                                    + self.Plot_Lagrange.local_2D_data[k][i].width * 3,
                                    self.Plot_Lagrange.point_names[i],
                                    transform=ax.transData,
                                    horizontalalignment="center",
                                )
                            else:  # when L1, L2, L3
                                ax.text(
                                    self.Plot_Lagrange.local_2D_data[k][i]._center[0],
                                    self.Plot_Lagrange.local_2D_data[k][i]._center[1]
                                    - self.Plot_Lagrange.local_2D_data[k][i].width / 2,
                                    "L2",
                                    transform=ax.transData,
                                    horizontalalignment="center",
                                )

                # _________________________________names 'fancy'__________________________________________________

                if (
                    self.PlottingOptions.names[0] is True
                    and self.PlottingOptions.names[1] == "fancy"
                ):
                    # Primaries names
                    if view != "YZ":
                        for i in range(len(self.Plot_Plantes.local_2D_data[k])):
                            ax.annotate(
                                self.Plot_Plantes.primaries_names[i],
                                xy=(
                                    self.Plot_Plantes.local_2D_data[k][i]._center[0],
                                    self.Plot_Plantes.local_2D_data[k][i]._center[1],
                                ),
                                xycoords="data",
                                xytext=(+10, -35),
                                textcoords="offset points",
                                size=15,
                                arrowprops=dict(
                                    arrowstyle="fancy",
                                    fc="0.6",
                                    ec="none",
                                    connectionstyle="angle3,angleA=0,angleB=-90",
                                ),
                            )
                    else:
                        for i in range(len(self.Plot_Plantes.local_2D_data[k])):

                            ax.text(
                                self.Plot_Plantes.local_2D_data[k][i]._center[0],
                                self.Plot_Plantes.local_2D_data[k][i]._center[1]
                                - (self.Plot_Plantes.local_2D_data[k][i].width) / 2,
                                self.Plot_Plantes.primaries_names[i],
                                transform=ax.transData,
                                horizontalalignment="center",
                            )

                    # libration point names
                    if view == "XY" or view == "XZ":

                        for i in range(len(self.Plot_Lagrange.local_2D_data[k])):
                            if self.Plot_Lagrange.point_names[i] != "L4":
                                ax.annotate(
                                    self.Plot_Lagrange.point_names[i],
                                    xy=(
                                        self.Plot_Lagrange.local_2D_data[k][i]._center[
                                            0
                                        ],
                                        self.Plot_Lagrange.local_2D_data[k][i]._center[
                                            1
                                        ],
                                    ),
                                    xycoords="data",
                                    xytext=(+10, +35),
                                    textcoords="offset points",
                                    size=15,
                                    arrowprops=dict(
                                        arrowstyle="fancy",
                                        fc="0.6",
                                        ec="none",
                                        connectionstyle="angle3,angleA=0,angleB=-90",
                                    ),
                                )
                            else:
                                ax.annotate(
                                    self.Plot_Lagrange.point_names[i],
                                    xy=(
                                        self.Plot_Lagrange.local_2D_data[k][i]._center[
                                            0
                                        ],
                                        self.Plot_Lagrange.local_2D_data[k][i]._center[
                                            1
                                        ],
                                    ),
                                    xycoords="data",
                                    xytext=(+10, -35),
                                    textcoords="offset points",
                                    size=15,
                                    arrowprops=dict(
                                        arrowstyle="fancy",
                                        fc="0.6",
                                        ec="none",
                                        connectionstyle="angle3,angleA=0,angleB=-90",
                                    ),
                                )

                    if view == "YZ":
                        for i in range(len(self.Plot_Lagrange.local_2D_data[k])):
                            if (
                                self.Plot_Lagrange.point_names[i] == "L4"
                                or self.Plot_Lagrange.point_names[i] == "L5"
                            ):
                                ax.annotate(
                                    self.Plot_Lagrange.point_names[i],
                                    xy=(
                                        self.Plot_Lagrange.local_2D_data[k][i]._center[
                                            0
                                        ],
                                        self.Plot_Lagrange.local_2D_data[k][i]._center[
                                            1
                                        ],
                                    ),
                                    xycoords="data",
                                    xytext=(+10, +35),
                                    textcoords="offset points",
                                    size=15,
                                    arrowprops=dict(
                                        arrowstyle="fancy",
                                        fc="0.6",
                                        ec="none",
                                        connectionstyle="angle3,angleA=0,angleB=-90",
                                    ),
                                )
                            else:
                                ax.text(
                                    self.Plot_Lagrange.local_2D_data[k][i]._center[0],
                                    self.Plot_Lagrange.local_2D_data[k][i]._center[1]
                                    - (self.Plot_Lagrange.local_2D_data[k][i].width)
                                    / 2,
                                    "L2",
                                    transform=ax.transData,
                                    horizontalalignment="center",
                                )

                ax.plot()
                fig.show()

                k = k + 1

    def scene_TD(self):
        """Generate the 3D scene.
        
        According to the plotting options it adds all the data (producted by the inner classes)
        to the scene and shows the 3D figure, taking care of label, legend and title.
        """
        # 3D data list are generated
        self.Plot_Plantes.generate_TD()
        self.Plot_Lagrange.generate_TD()
        self.Plot_Orbit.generate_TD()

        # 3D data list for eventual additional orbit are generated
        for x, y in self.kwargs.items():
            self.kwargs[x].generate_TD()

        if self.PlottingOptions.TD is True:

            fig = plt.figure(num=self.figure3D_labels_to_apply[0])
            ax = fig.gca(projection="3d")

            # sci check
            if self.PlottingOptions.sci is True:
                ax.ticklabel_format(style="scientific", scilimits=(0, 0))

            # figure settings indipendent from the plotting options
            ax.set_title(
                "CRTBP Synonical Frame: 3D view \n Primaries: "
                + self.CrtbpOrbit.cr3bp.name,
                pad=40,
            )
            ax.set_xlabel(xlabel="X[KM]", labelpad=15)
            ax.set_ylabel(ylabel="Y[KM]", labelpad=15)
            ax.set_zlabel(zlabel="Z[KM]", labelpad=15)

            # add all the planets spheres created by plotterPlanets to the scene
            for i in range(len(self.Plot_Plantes.local_3D_data)):
                ax.plot_surface(
                    self.Plot_Plantes.local_3D_data[i][0],
                    self.Plot_Plantes.local_3D_data[i][1],
                    self.Plot_Plantes.local_3D_data[i][2],
                    cmap=self.Plot_Plantes.planets_color_3D[i],
                )
            # add all the lagrangian points created by plotterLibrationPoints to the scene
            for i in range(len(self.Plot_Lagrange.local_3D_data)):
                ax.plot_surface(
                    self.Plot_Lagrange.local_3D_data[i][0],
                    self.Plot_Lagrange.local_3D_data[i][1],
                    self.Plot_Lagrange.local_3D_data[i][2],
                    color="r",
                )
            # add the orbit to the scene
            if self.PlottingOptions.orbit_color == "":
                ax.plot3D(
                    self.Plot_Orbit.local_3D_data[0],
                    self.Plot_Orbit.local_3D_data[1],
                    self.Plot_Orbit.local_3D_data[2],
                    c="b",
                )

                # add to the scene eventual additional orbit
                for x, y in self.kwargs.items():
                    ax.plot3D(
                        self.kwargs[x].local_3D_data[0],
                        self.kwargs[x].local_3D_data[1],
                        self.kwargs[x].local_3D_data[2],
                    )
            else:
                ax.plot3D(
                    self.Plot_Orbit.local_3D_data[0],
                    self.Plot_Orbit.local_3D_data[1],
                    self.Plot_Orbit.local_3D_data[2],
                    c=self.PlottingOptions.orbit_color,
                )

                for x, y in self.kwargs.items():
                    ax.plot3D(
                        self.kwargs[x].local_3D_data[0],
                        self.kwargs[x].local_3D_data[1],
                        self.kwargs[x].local_3D_data[2],
                        c=self.PlottingOptions.orbit_color,
                    )

            # legend
            if self.PlottingOptions.legend is True:
                # all the lines (trajectory and orbits) within the scene are collected into a list
                handles_list = ax.lines
                # all the labels are generated according to the input Orbits given to Masterplotter
                labels_list = []
                # The label corresponding to the the first (mandatory) orbit is collected
                if "Azdim" in self.CrtbpOrbit.kwargs.keys():
                    labels_list.append(
                        self.CrtbpOrbit.kind
                        + ", "
                        + self.CrtbpOrbit.family.name
                        + ", "
                        + "Az_dim =  "
                        + str(round(self.CrtbpOrbit.Azdim, 3))
                    )
                elif "Cjac" in self.CrtbpOrbit.kwargs.keys():
                    labels_list.append(
                        self.CrtbpOrbit.kind
                        + ", "
                        + self.CrtbpOrbit.family.name
                        + ", "
                        + "Cjac =  "
                        + str(round(self.CrtbpOrbit.C, 3))
                    )
                # optional additional orbits are considered
                for x, y in self.kwargs.items():
                    if "Azdim" in self.kwargs[x].Cr3bpkwargs.keys():
                        labels_list.append(
                            self.kwargs[x].kind
                            + ", "
                            + self.kwargs[x].family
                            + ", "
                            + "Az_dim =  "
                            + str(round(self.kwargs[x].Azdim, 3))
                        )
                    elif "Cjac" in self.kwargs[x].Cr3bpkwargs.keys():
                        labels_list.append(
                            self.kwargs[x].kind
                            + ", "
                            + self.kwargs[x].family
                            + ", "
                            + "Cjac =  "
                            + str(round(self.kwargs[x].C, 3))
                        )
                # linking each ax.line with its legend label
                for i in range(len(handles_list)):
                    handles_list[i].set_label(labels_list[i])

                ax.legend()

            # add Text names below each planet and libration point if required by the plotting options
            if self.PlottingOptions.names[0] is True:
                # primarie s names
                for i in range(2):
                    if list(self.PlottingOptions.Primaries.values())[i] is True:
                        ax.text(
                            self.Plot_Plantes.positions_list[i][0],
                            self.Plot_Plantes.positions_list[i][1],
                            self.Plot_Plantes.positions_list[i][2],
                            self.Plot_Plantes.primaries_names_cr3bp[i],
                        )

                # libration points names
                for i in range(len(self.PlottingOptions.L_point_list)):
                    if self.PlottingOptions.L_point_list[i] is True:
                        index = i + 1
                        ax.text(
                            self.Plot_Lagrange.positions_list[i][0],
                            self.Plot_Lagrange.positions_list[i][1],
                            self.Plot_Lagrange.positions_list[i][2],
                            "L" + str(index),
                        )

            set_axes_equal(ax)
            fig.show()

    def show_all(self):
        """Shows the 2D views and the 3D visualisation in one line."""
        self.scene_twoD()
        self.scene_TD()

    def figure_label_generator(self):
        """Generates the labels for the figures that MasterPlotter has to plot."""

        current_figures_label_list = plt.get_figlabels()
        figure2D_labels_to_apply = []
        figure3D_labels_to_apply = []
        # If MasterPlotter has not been called yet...
        if not current_figures_label_list:
            # 2D visualization
            for view, option in self.PlottingOptions.twoDviews.items():
                if option is True:
                    figure2D_labels_to_apply.append("Master Figure 0 - " + view)
            # 3D visualization
            if self.PlottingOptions.TD is True:
                figure3D_labels_to_apply.append("Master Figure 0 - 3D Visualization")

        else:
            next_index = 0
            for i in range(len(current_figures_label_list)):
                if str(i) in current_figures_label_list[-1].split():
                    next_index = i + 1
            # 2D visualization
            for view, option in self.PlottingOptions.twoDviews.items():
                if option is True:
                    figure2D_labels_to_apply.append(
                        "Master Figure " + str(next_index) + " - " + view
                    )
            if self.PlottingOptions.TD is True:
                figure3D_labels_to_apply.append(
                    "Master Figure " + str(next_index) + " - 3D Visualization"
                )

        return (
            current_figures_label_list,
            figure2D_labels_to_apply,
            figure3D_labels_to_apply,
        )

    def modify_title(self, new_title):
        """
        Modifies the titles of the figures generated by a MasterPlotter according to the plotting options.
        
        Parameters
        ----------
        new_title : str
            New title to give to the figure.
            
        Notes
        -----
        This method is able to modify only figures generated by a MasterPlotter object. 
        
        Examples
        --------
        >>> plot =  MasterPlotter(plot_options, orbit)
        >>> plot.scene_TD()
        >>> plot.scene_twoD()
        >>> plot.modify_title('MODIFIED TITLE')        
        
        """

        # modifying the 2D view titles according to the plotting options
        for element in self.figure2D_labels_to_apply:
            fig = plt.figure(num=element)
            ax = fig.gca()
            ax.set_title(new_title)

        # modifying the 3D view title according to the plotting options
        if self.figure3D_labels_to_apply:
            fig = plt.figure(num=self.figure3D_labels_to_apply[0])
            ax = fig.gca()
            ax.set_title(new_title, pad=50)

    def save_figs(self):
        pass

    class PlotterPlanets:
        """
        Generates data to plot planets in 2D and 3D.
        
        Parameters
        ----------
        CrtbpOrbit : object
            An object of the CrtbpOrbit class.
        PlottingOptions : object
            An object of the PlottingOptions class.
            
        Attributes
        ----------
        planet_obj1 : object 
            First Primary (an object of the class Primary).
        planet_obj2 : object
            Second Primary (an object of the class Primary).
        dim_m1_pos : numpy array
            Center position of the first primary expressed in km.
        dim_m2_pos : numpy array
            Center position of the first primary expressed in km.
        positions_list : list of numpy array 
            List containg the positions of the primaries. Each element of the list is an array.
        adim_m1_pos : numpy array
            Center position of the first primary [dimensionless].
        adim_m2_pos : numpy array
            Center position of the second primary [dimensionless].
        radius_primary1 : numpy.float64
            First primary radius expressed in Km.
        radius_primary2 : numpy.float64
            Second primary radius expressed in Km.
        radius_list : list of numpy.float64
            List containing the radius of the primaries.
        radius_primary1_adim : numpy.float64
            First primary radius [dimensionless].
        radius_primary2_adim : numpy.float64
            Second primary radius [dimensionless].
        color_list : list of str
            List containing the colormap information of the primaries for the 3D visualization 
            and the colors for 2D visualization.
        PlottingOptions : object
            An object of the PlottingOptions class.
        scale_factor : int
            Scale factor used to increase the size of the Primaries within the figure only for visualization purposes.
        local_2D_data : list of matplotlib.patches.Circle 
            List that contains all the 2D data to plot (according to what the plotting options request).
            Each element of the list is a list itself dedicated to a particular 2D view, containg all the patches to plot. 
        local_3D_data : list of numpy array 
            List that contains all the 3D data (parametric coordinates of a sphere) to plot 
            (according to what the plotting options request).
        primaries_names : list of str
            List containing the names of the primaries the user wants to plot.
            If the user wants to hide the first Primary, for instance, this attribute will contain only the name of the second Primary.
        primaries_names_cr3bp : list of str
            List of the Primaries names independent from the user needs.
        planets_color_3D : list of str
            List containing the cmap of the primaries the user wants to plot.
            If the user wants to hide the first Primary, for instance, this attribute will contain only the cmap related to the second Primary.
        """

        def __init__(self, PlottingOptions, CrtbpOrbit):
            """Inits PlotterPlanets class."""
            self.planet_obj1 = CrtbpOrbit.cr3bp.m1
            self.planet_obj2 = CrtbpOrbit.cr3bp.m2
            self.dim_m1_pos = (
                np.asarray(CrtbpOrbit.cr3bp.m1_pos) * CrtbpOrbit.cr3bp.L
            )  # [km]
            self.dim_m2_pos = (
                np.asarray(CrtbpOrbit.cr3bp.m2_pos) * CrtbpOrbit.cr3bp.L
            )  # [km]
            self.positions_list = [self.dim_m1_pos, self.dim_m2_pos]
            self.adim_m1_pos = np.asarray(CrtbpOrbit.cr3bp.m1_pos)  # [adim]
            self.adim_m2_pos = np.asarray(CrtbpOrbit.cr3bp.m2_pos)  # [adim]

            self.radius_primary1 = CrtbpOrbit.cr3bp.R1  # [km]
            self.radius_primary2 = CrtbpOrbit.cr3bp.R2  # [km]

            self.radius_list = [self.radius_primary1, self.radius_primary2]

            self.radius_primary1_adim = (
                self.radius_primary1 / CrtbpOrbit.cr3bp.L
            )  # [adim]
            self.radius_primary2_adim = (
                self.radius_primary2 / CrtbpOrbit.cr3bp.L
            )  # [adim]

            self.PlottingOptions = PlottingOptions

            self.local_2D_data = []
            self.local_3D_data = []

            self.color_list = planet_color(self.planet_obj1.name, self.planet_obj2.name)

            if self.PlottingOptions.Primaries["scale_factor"][0] is True:
                self.scale_factor = self.PlottingOptions.Primaries["scale_factor"][1]
            else:
                self.scale_factor = 1
            self.primaries_names = []

            self.planets_color_3D = []

            if self.PlottingOptions.disp_first_pr is True:
                self.primaries_names.append(self.planet_obj1.name)
                self.planets_color_3D.append(self.color_list[0])
            if self.PlottingOptions.disp_second_pr is True:
                self.primaries_names.append(self.planet_obj2.name)
                self.planets_color_3D.append(self.color_list[1])

            self.primaries_names_cr3bp = [self.planet_obj1.name, self.planet_obj2.name]

        def generate_twoD(self):
            """
            Generates all the data for the 2D visualization according to the plotting options.
            
            Notes
            -----
            Once this method is executed the attribute local_2D_data is updated with the data to plot.
            
            """

            # 2d visualization
            for view, option in self.PlottingOptions.twoDviews.items():
                if option is True:
                    J = []
                    for letters in view:
                        if letters == "X":
                            J.append(0)
                        if letters == "Y":
                            J.append(1)
                        if letters == "Z":
                            J.append(2)
                    data_list = []
                    for i in range(2):
                        if list(self.PlottingOptions.Primaries.values())[i] is True:
                            circle = Circle(
                                (
                                    self.positions_list[i][J[0]],
                                    self.positions_list[i][J[1]],
                                ),
                                radius=self.radius_list[i] * self.scale_factor,
                                fc=self.color_list[i + 2],
                            )

                            data_list.append(circle)

                    self.local_2D_data.append(data_list)

        def generate_TD(self):
            """
            Generates all the data for the 3D visualization according to the plotting options.
            
            Notes
            -----
            Once this method is executed the attribute local_3D_data is updated with the data to plot.
            
            """

            # 3d visualization
            if self.PlottingOptions.TD is True:
                for i in range(2):
                    if list(self.PlottingOptions.Primaries.values())[i] is True:
                        x, y, z = sphere(
                            self.PlottingOptions.Primaries["N"],
                            self.radius_list[i] * self.scale_factor,
                            self.positions_list[i],
                        )
                        self.local_3D_data.append([x, y, z])

    class PlotterLibrationPoints:
        """
        Generates data to plot libration points in 2D and 3D.
        
        According to the considered Circular Restricted Three body Problem (CRTBP) and to the 
        plotting options all libration points data generated and stored into two attributes of that class. 
        
        Parameters
        ----------
        CrtbpOrbit : object
            An object of the CrtbpOrbit class.
        PlottingOptions : object
            An object of the PlottingOptions class.
            
        Attributes
        ----------
        L1_position : numpy array
            L1 position [Km]
        L2_position : numpy array
            L2 position [Km]        
        L3_position : numpy array
            L3 position [Km]
        L4_position : numpy array
            L4 position [Km]
        L5_position : numpy array
            L5 position [Km]
        positions_list : list of numpy array 
            List containing the positions of all the libration points. 
        PlottingOptions : object
            An object of the PlottingOptions class.
        m2_radius : numpy.float64
            Radius in Km of the second Primary, used to plot the librations points. In order to visualize the libration points
            within the scene their radius is set equal to half the radius of the second primary.
        radius : numpy.float64
            Radius of each libration point. Used only for plotting purposes.
        local_2D_data : list of matplotlib.patches.Circle 
            List that contains all the 2D data to plot (according to what the plotting options request).
            Each element of the list is a list itself dedicated to a particular 2D view, containg all the patches to plot. 
        local_3D_data : list of numpy array 
            List that contains all the 3D data (parametric coordinates of a sphere) to plot 
            (according to what the plotting options request).
        point_names : list of str
            List containg the names of the Libration points the user wants to plot. 
        
        """

        def __init__(
            self, PlottingOptions, CrtbpOrbit,
        ):
            """Inits PlotterLibrationPoints class."""
            self.L1_position = (
                np.asarray(CrtbpOrbit.cr3bp.l1.position) * CrtbpOrbit.cr3bp.L
            )
            self.L2_position = (
                np.asarray(CrtbpOrbit.cr3bp.l2.position) * CrtbpOrbit.cr3bp.L
            )
            self.L3_position = (
                np.asarray(CrtbpOrbit.cr3bp.l3.position) * CrtbpOrbit.cr3bp.L
            )
            self.L4_position = (
                np.asarray(CrtbpOrbit.cr3bp.l4.position) * CrtbpOrbit.cr3bp.L
            )
            self.L5_position = (
                np.asarray(CrtbpOrbit.cr3bp.l5.position) * CrtbpOrbit.cr3bp.L
            )

            self.PlottingOptions = PlottingOptions

            self.positions_list = [
                self.L1_position,
                self.L2_position,
                self.L3_position,
                self.L4_position,
                self.L5_position,
            ]
            self.m2_radius = CrtbpOrbit.cr3bp.R2
            self.radius = self.find_right_proportions()

            self.local_2D_data = []
            self.local_3D_data = []
            # libration point names
            self.point_names = []

            for i in range(len(self.PlottingOptions.L_point_list)):
                if self.PlottingOptions.L_point_list[i] is True:
                    index = i + 1
                    self.point_names.append("L" + str(index))

        def find_right_proportions(self):

            """
            Finds the right proportion for the Libration points.
            
            It sets the radius of the libration point patch equals to 1/2 of the second Primary radius.

            Returns
            -------
            numpy.float64
                Half Second primary 's radius.
            
            Notes
            -----
            Libration points are mathematically defined as points in space, but in order to visualize them in plots it is needed
            to treat them as circle patches and to set their radius size to a value comparable with the one of a planet.
            

            """

            if self.PlottingOptions.Primaries["scale_factor"][0] is True:
                scale_factor = self.PlottingOptions.Primaries["scale_factor"][1]
                return (self.m2_radius * scale_factor) / 2
            else:
                return (self.m2_radius) / 2

        def generate_twoD(self):
            """
            Generates all the data for the 2D visualization according to the plotting options.
            
            Notes
            -----
            Once this method is executed the attribute local_2D_data is updated with the data to plot.
            
            """

            for view, option in self.PlottingOptions.twoDviews.items():
                if option is True:
                    J = []
                    for letters in view:
                        if letters == "X":
                            J.append(0)
                        if letters == "Y":
                            J.append(1)
                        if letters == "Z":
                            J.append(2)
                    data_list = []
                    for i in range(len(self.PlottingOptions.L_point_list)):
                        if self.PlottingOptions.L_point_list[i] is True:
                            circle = Circle(
                                (
                                    self.positions_list[i][J[0]],
                                    self.positions_list[i][J[1]],
                                ),
                                radius=self.radius,
                                fc="r",
                            )

                            data_list.append(circle)

                    self.local_2D_data.append(data_list)

        def generate_TD(self):
            """
            Generates all the data for the 3D visualization according to the plotting options.
            
            Notes
            -----
            Once this method is executed the attribute local_3D_data is updated with the data to plot.
            
            """

            if self.PlottingOptions.TD is True:
                for i in range(len(self.PlottingOptions.L_point_list)):
                    if self.PlottingOptions.L_point_list[i] is True:
                        x, y, z = sphere(
                            N=50,
                            radius=self.radius * 1.2,
                            center=self.positions_list[i],
                        )
                        self.local_3D_data.append([x, y, z])

    class PlotterOrbitManifolds:
        """
        Generates data to plot Orbits and Manifolds in 2D and 3D.
        
        Parameters
        ----------
        CrtbpOrbit : object
            An object of the CrtbpOrbit class.
        PlottingOptions : object
            An object of the PlottingOptions class.
            
        Attributes
        ----------
        kind : str
            Kind of Cr3bp Orbit.
        family : str
            Family to which the Cr3bp Orbit belongs.
        Cr3bpkwargs : dict of float
            Attribute that stores additional keyword arguments of the CrtbpOrbit object. These arguments are added to the legend.
        C : float or None
            Jacobi constant value.
        Azdim : float or None
            Orbit vertical extension [km].
        state0 : numpy ndarray
            Orbit's initial state.
        stateV : numpy ndarray
            State vector, array of states results after the orbit's initial state propagation [dimensionless].
        stateV_dim : numpy ndarray
            State vector, array of states results after the orbit's initial state propagation [Km].
        tV : numpy ndarray
            Time vector, array of time results after the orbit's initial state propagation.
        reference_frame : None 
            Reference frame in which lies the orbit.
        local_2D_data : list of numpy ndarray
            List that contains all the 2D trajectories to plot (according to what the plotting options request).
            Each element of the list is a list itself dedicated to a particular 2D view and containg the related couple of coordinates to plot.
        local_3D_data : list of numpy array 
            List that contains the x, y, z coordinates of the trajectory to plot. 
        new_figures_id : list of int 
            List used to keep track of all the already existing figure and eventually plot trajectories in them. 
            
        """

        def __init__(self, PlottingOptions, CrtbpOrbit):
            """Inits PlotterOrbitManifolds class."""

            self.PlottingOptions = PlottingOptions
            # Orbit information
            self.kind = CrtbpOrbit.kind
            self.family = CrtbpOrbit.family.name
            self.Cr3bpkwargs = CrtbpOrbit.kwargs
            self.C = CrtbpOrbit.C
            self.Azdim = CrtbpOrbit.Azdim
            # Orbit states
            self.state0 = CrtbpOrbit.state0
            self.stateV = CrtbpOrbit.state_vec
            self.stateV_dim = self.stateV * CrtbpOrbit.cr3bp.L
            self.tV = CrtbpOrbit.t_vec
            # Frame in which the orbit is represented
            self.reference_frame = CrtbpOrbit.reference_frame
            # data containers
            self.local_2D_data = []
            self.local_3D_data = []

            # list filled with the number of the figures generated by the method plot_new()
            self.new_figures_id = []

        def generate_twoD(self):
            """
            Generates all the data for the 2D visualization according to the plotting options.
            
            Notes
            -----
            Once this method is executed the attribute local_2D_data is updated with the data to plot.
            
            """

            for view, option in self.PlottingOptions.twoDviews.items():
                if option is True:
                    J = []
                    for letters in view:
                        if letters == "X":
                            J.append(0)
                        if letters == "Y":
                            J.append(1)
                        if letters == "Z":
                            J.append(2)
                    data_list = [self.stateV_dim[:, J[0]], self.stateV_dim[:, J[1]]]

                    self.local_2D_data.append(data_list)

        def generate_TD(self):
            """
            Generates all the data for the 3D visualization according to the plotting options.
            
            Notes
            -----
            Once this method is executed the attribute local_3D_data is updated with the data to plot.
            
            """

            if self.PlottingOptions.TD is True:

                self.local_3D_data.append(self.stateV_dim[:, 0])
                self.local_3D_data.append(self.stateV_dim[:, 1])
                self.local_3D_data.append(self.stateV_dim[:, 2])

        def plot_already_existing(self, plot):
            """
            Plots an Orbit in an already existing figure.
            
            Parameters
            ----------
            plot : object
                An object of the MasterPlotter.PlotterOrbitManifolds class. The Orbit will be plotted into the 
                figure associated to this object.
                
            Examples
            --------
            >>> plot1 = MasterPlotter.PlotterOrbitManifolds(orbit4, plot_options)
            >>> plot1.plot_new()
            #It will create new figures according to the plotting options.
            >>> plot2 = MasterPlotter.PlotterOrbitManifolds(orbit3, plot_options)
            >>> plot2.plot_already_existing(plot1)
            #it will add orbit3 to the the figure generated by plot1.plot_new().
            
            """
            # creation of 2D and 3D data
            self.generate_twoD()
            self.generate_TD()

            # 2D visualization
            k = 0
            for view, option in self.PlottingOptions.twoDviews.items():
                if option is True:
                    fig = plt.figure(plot.new_figures_id[k])
                    ax = fig.gca()
                    ax.plot(self.local_2D_data[k][0], self.local_2D_data[k][1])
                    ax.set_aspect("equal", adjustable="datalim")
                    ax.plot()
                    fig.show()
                    k = k + 1

            # 3D visualization
            if self.PlottingOptions.TD is True:
                fig = plt.figure(plot.new_figures_id[k])
                ax = fig.gca(projection="3d")
                ax.plot3D(
                    self.local_3D_data[0], self.local_3D_data[1], self.local_3D_data[2]
                )
                set_axes_equal(ax)
                fig.show()

        def default_figure_coupling(self, master_plotter_object):
            """
            Adds new trajectories to the scenes already created by the methods scene_twoD() and scene_TD() of MasterPlotter.
            
            It has to be used when MasterPlotter has already generated the default figures and an additional trajectory 
            needs to be added  to the scene. 
            
            Notes
            -----
            It is not reccomended to use this method before creating the default figures with MasterPlotter. If it happens
            it will generate figure with the same label of the default ones, and that might create conflicts.
            
            Examples
            --------
            >>> plot1 = MasterPlotter(plot_options, orbit, orbit2 = orbit2, orbit3 = orbit3)
            >>> plot1.scene_TD()
            >>> plot1.scene_twoD()
            >>> plot2 = MasterPlotter.PlotterOrbitManifolds(orbit4, plot_options)
            >>> plot2.default_figure_coupling(plot1)
            It will add orbit4 to the figures generated by plot1 
            
            """
            # creation of 2D and 3D data
            self.generate_twoD()
            self.generate_TD()

            # 2D visualization
            k = 0
            for view, option in self.PlottingOptions.twoDviews.items():
                if option is True:
                    fig = plt.figure(
                        num=master_plotter_object.figure2D_labels_to_apply[k]
                    )
                    ax = fig.gca()
                    ax.plot(self.local_2D_data[k][0], self.local_2D_data[k][1])
                    ax.set_aspect("equal", adjustable="datalim")
                    # updating the legend if PlottingOptions.legend == True
                    if self.PlottingOptions.legend is True:
                        handles_list = ax.lines
                        if "Azdim" in self.Cr3bpkwargs.keys():
                            handles_list[-1].set_label(
                                self.kind
                                + ", "
                                + self.family
                                + ", "
                                + "Az_dim =  "
                                + str(self.Azdim)
                            )
                        elif "Cjac" in self.Cr3bpkwargs.keys():
                            handles_list[-1].set_label(
                                self.kind
                                + ", "
                                + self.family
                                + ", "
                                + "Cjac =  "
                                + str(self.C)
                            )
                        ax.legend()
                    ax.plot()
                    fig.show()
                    k = k + 1
            # 3D visualization
            if self.PlottingOptions.TD is True:
                fig = plt.figure(num=master_plotter_object.figure3D_labels_to_apply[0])
                ax = fig.gca(projection="3d")
                ax.plot3D(
                    self.local_3D_data[0], self.local_3D_data[1], self.local_3D_data[2]
                )
                # Updating the legend if requested
                if self.PlottingOptions.legend is True:
                    handles_list = ax.lines
                    if "Azdim" in self.Cr3bpkwargs.keys():
                        handles_list[-1].set_label(
                            self.kind
                            + ", "
                            + self.family
                            + ", "
                            + "Az_dim =  "
                            + str(self.Azdim)
                        )
                    elif "Cjac" in self.Cr3bpkwargs.keys():
                        handles_list[-1].set_label(
                            self.kind
                            + ", "
                            + self.family
                            + ", "
                            + "Cjac =  "
                            + str(self.C)
                        )

                ax.legend()
                set_axes_equal(ax)

                fig.show()

        def plot_new(self):
            """
            Plots the Orbit in a new figure.
            
            Method used when it necessary to plot an Orbit in a new figure and there is the need to use PlotterOrbitManifolds
            independently from its outer class MasterPlotter. 
            According to the plotting options new figures are created and their labels are stored in the attribute new_figures_id.
            
            Examples
            --------
            >>> plot = MasterPlotter.PlotterOrbitManifolds(plot_options, orbit)
            >>> plot.plot_new()
            It will plot the object "orbit" in new figures (according to the views requested by the plotting options). 
            
            """

            # creation of 2D and 3D data
            self.generate_twoD()
            self.generate_TD()

            # 2D visualization
            k = 0
            for view, option in self.PlottingOptions.twoDviews.items():
                if option is True:
                    fig = plt.figure()
                    ax = fig.gca()
                    if self.PlottingOptions.sci is True:
                        ax.ticklabel_format(style="scientific", scilimits=(0, 0))

                    ax.plot(self.local_2D_data[k][0], self.local_2D_data[k][1])
                    ax.set_title("Orbit Plotter - " + view[0] + view[1] + " view")
                    ax.set_xlabel(xlabel=view[0] + "[km]", labelpad=10)
                    ax.set_ylabel(ylabel=view[1] + "[km]", labelpad=5)

                    ax.set_aspect("equal", adjustable="datalim")
                    if self.PlottingOptions.legend is True:
                        handles_list = ax.lines
                        if "Azdim" in self.Cr3bpkwargs.keys():
                            handles_list[-1].set_label(
                                self.kind
                                + ", "
                                + self.family
                                + ", "
                                + "Az_dim =  "
                                + str(self.Azdim)
                            )
                        elif "Cjac" in self.Cr3bpkwargs.keys():
                            handles_list[-1].set_label(
                                self.kind
                                + ", "
                                + self.family
                                + ", "
                                + "Cjac =  "
                                + str(self.C)
                            )
                        ax.legend()
                    ax.plot()
                    fig.show()
                    # The 'nums' of all the already existing figures are saved in a list and
                    # Only the last one generated is stored into 'new_figures_id'
                    # For every iteration of this loop 'list_already_existing_figures' is overwritten
                    # and updated to the correct number of existing figures.
                    list_already_existing_figures = plt.get_fignums()
                    self.new_figures_id.append(list_already_existing_figures[-1])
                    k = k + 1

            # 3D visualization
            if self.PlottingOptions.TD is True:
                fig = plt.figure()
                ax = fig.gca(projection="3d")

                if self.PlottingOptions.sci is True:
                    ax.ticklabel_format(style="scientific", scilimits=(0, 0))

                ax.plot3D(
                    self.local_3D_data[0], self.local_3D_data[1], self.local_3D_data[2]
                )
                ax.set_title("Orbit Plotter - 3D visualization", pad=40)
                ax.set_xlabel(xlabel="X[KM]", labelpad=15)
                ax.set_ylabel(ylabel="Y[KM]", labelpad=15)
                ax.set_zlabel(zlabel="Z[KM]", labelpad=15)
                if self.PlottingOptions.legend is True:
                    handles_list = ax.lines
                    if "Azdim" in self.Cr3bpkwargs.keys():
                        handles_list[-1].set_label(
                            self.kind
                            + ", "
                            + self.family
                            + ", "
                            + "Az_dim =  "
                            + str(self.Azdim)
                        )
                    elif "Cjac" in self.Cr3bpkwargs.keys():
                        handles_list[-1].set_label(
                            self.kind
                            + ", "
                            + self.family
                            + ", "
                            + "Cjac =  "
                            + str(self.C)
                        )
                    ax.legend()

                set_axes_equal(ax)
                fig.show()
                # if the TD visualization is requested 'list_already_existing_figures'
                # is updated by adding the TD figure
                list_already_existing_figures = plt.get_fignums()
                self.new_figures_id.append(list_already_existing_figures[-1])


# ________________________________part used for testing _____________________________________________


if __name__ == "__main__":

    plt.ioff()
    plt.close("all")
    plot_options = PlottingOptions(disp_second_pr=False)
    plot_options.print_plotting_options()
    CRTBP = Cr3bp(Primary.EARTH, Primary.MOON)

    orbit = Halo(CRTBP, CRTBP.l2, Halo.Family.southern, Azdim=12000)
    orbit.computation()

    orbit1 = Halo(CRTBP, CRTBP.l2, Halo.Family.southern, Azdim=13000)
    orbit1.computation()

    plot2 = MasterPlotter(plot_options, orbit, orbit1=orbit1)
    plot2.show_all()

