"""
Plots three-dimensional trajectories in the phase-space.

"""

import matplotlib.pyplot as plt
from src.plotting.util import set_axes_equal


def plot_3d_trajectory(
    pos_vec, colors, labels, title, units="-", pts_vec=None, pts_col=None, pts_lab=None
):
    """Plots one or more three-dimensional trajectories. """

    fig = plt.figure()
    ax = fig.add_subplot(111, projection="3d")

    for i, l in enumerate(labels):
        if colors is not None:
            ax.plot(
                pos_vec[i, :, 0],
                pos_vec[i, :, 1],
                pos_vec[i, :, 2],
                color=colors[i],
                label=l,
            )
        else:
            ax.plot(pos_vec[i, :, 0], pos_vec[i, :, 1], pos_vec[i, :, 2], label=l)
    if pts_vec is not None:
        for i, l in enumerate(pts_lab):
            ax.scatter(
                pts_vec[i, 0], pts_vec[i, 1], pts_vec[i, 2], color=pts_col[i], label=l
            )

    ax.set_xlabel(f"x [{units}]")
    ax.set_ylabel(f"y [{units}]")
    ax.set_zlabel(f"z [{units}]")
    ax.set_title(title)
    ax.legend(loc=0)
    ax.grid()
    set_axes_equal(ax)
    return fig, ax
