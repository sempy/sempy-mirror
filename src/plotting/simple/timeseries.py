"""
Plots state components time series.

"""

import matplotlib.pyplot as plt


def plot_2d_timeseries(t_vec, state_vec, colors, labels, units, y_labels, title, elapsed=False,
                       state_vec_rhs=None, colors_rhs=None, labels_rhs=None, units_rhs=None,
                       y_labels_rhs=None):
    """Plots two-dimensional time series. """

    fig, ax = plt.subplots(nrows=state_vec.shape[-1], ncols=1, constrained_layout=True)
    fig.suptitle(title)

    x_label = f"elapsed time [{units[0]}]" if elapsed else f"time [{units[0]}]"
    y_labels = [f"{yl} [{units[1]}]" for yl in y_labels]

    for i in range(state_vec.shape[-1]):
        for j, l in enumerate(labels):
            t = t_vec[j, :] - t_vec[j, 0] if elapsed else t_vec[j, :]
            if colors is not None:
                ax[i].plot(t, state_vec[j, :, i], color=colors[j], label=l)
            else:
                ax[i].plot(t, state_vec[j, :, i], label=l)
        ax[i].set_xlabel(x_label)
        ax[i].set_ylabel(y_labels[i])
        ax[i].grid()
        ax[i].legend(loc='best')

    if state_vec_rhs is not None:
        ax_rhs = [a.twinx() for a in ax]
        y_labels_rhs = [f"{yl} [{units_rhs}]" for yl in y_labels_rhs]
        for i in range(state_vec_rhs.shape[-1]):
            for j, l in enumerate(labels_rhs):
                t = t_vec[j, :] - t_vec[j, 0] if elapsed else t_vec[j, :]
                if colors_rhs is not None:
                    ax_rhs[i].plot(t, state_vec_rhs[j, :, i], color=colors_rhs[j], label=l)
                else:
                    ax_rhs[i].plot(t, state_vec_rhs[j, :, i], label=l)
            ax_rhs[i].set_ylabel(y_labels_rhs[i], color=colors_rhs[0])
            ax_rhs[i].tick_params(axis='y', labelcolor=colors_rhs[0])
    else:
        ax_rhs = None

    return fig, ax, ax_rhs
