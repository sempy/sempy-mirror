"""
Simple utility functions for better plots.

"""

from src.plotting.util import set_axes_equal


def decorate_3d_axes(ax, title, units, legend=True):
    """Adds axes labels, title and legend, set an equal aspect ratio, set background color. """
    ax.xaxis.pane.fill = False
    ax.yaxis.pane.fill = False
    ax.zaxis.pane.fill = False
    labs = [fr"${v} \ [{units}]$" for v in "xyz"]  # axes labels
    ax.set_xlabel(labs[0])
    ax.set_ylabel(labs[1])
    ax.set_zlabel(labs[2])
    ax.set_title(title)  # title
    if legend:  # legend
        ax.legend(loc=0)
    ax.grid(True)  # grid lines
    set_axes_equal(ax)  # equal aspect ratio
