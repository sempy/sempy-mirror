#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import tkinter as tk
from tkinter import ttk


class Show_views_frame(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        # definition of tkinter variables
        self.XY = tk.BooleanVar()
        self.XY.set(False)
        self.XZ = tk.BooleanVar()
        self.XZ.set(False)
        self.YZ = tk.BooleanVar()
        self.YZ.set(False)
        self.TD = tk.BooleanVar()
        self.TD.set(True)
        # ----------------------title label-------------------------------------
        title_label = tk.Label(self, text="SHOW VIEWS")
        title_label.grid(row=0, column=0, sticky=tk.W)

        # -----------------1st row ----------------------------------------
        label_XY = tk.Label(self, text="Show results in 'XY' plane")
        Check_flag_XY = tk.Checkbutton(self, variable=self.XY)
        label_XY.grid(row=1, column=0, sticky=tk.W)
        Check_flag_XY.grid(row=1, column=1, sticky=tk.E)
        # -----------------2nd row ----------------------------------------
        label_XZ = tk.Label(self, text="Show results in 'XZ' plane")
        Check_flag_XZ = tk.Checkbutton(self, variable=self.XZ)
        label_XZ.grid(row=2, column=0, sticky=tk.W)
        Check_flag_XZ.grid(row=2, column=1, sticky=tk.E)

        # -----------------3rd row ----------------------------------------
        label_YZ = tk.Label(self, text="Show results in 'YZ' plane")
        Check_flag_YZ = tk.Checkbutton(self, variable=self.YZ)
        label_YZ.grid(row=3, column=0, sticky=tk.W)
        Check_flag_YZ.grid(row=3, column=1, sticky=tk.E)

        # -----------------4th row ----------------------------------------
        label_3D = tk.Label(self, text="Show results in 3D")
        Check_flag_3D = tk.Checkbutton(self, variable=self.TD)
        label_3D.grid(row=4, column=0, sticky=tk.W)
        Check_flag_3D.grid(row=4, column=1, sticky=tk.E)

        # -----------------5th row---------------------------------------
        Save_button1 = tk.Button(self, text="Save", command=self.save_views_settings)
        Save_button1.grid(row=5, column=2, sticky=(tk.W + tk.E))

        self.columnconfigure(1, weight=1)

    def save_views_settings(self):
        views_settings = {
            "XY": self.XY.get(),
            "XZ": self.XZ.get(),
            "YZ": self.YZ.get(),
            "TD": self.TD.get(),
        }
        return views_settings


class Show_entities_frame(tk.Frame):
    def __init__(self, parent, *args, **kwargs):
        super().__init__(parent, *args, **kwargs)
        # definition of tkinter variables
        self.First_Primary = tk.BooleanVar()
        self.First_Primary.set(False)
        self.Second_Primary = tk.BooleanVar()
        self.Second_Primary.set(False)
        self.Libration_Points = tk.StringVar()
        self.Libration_Points.set("None")
        self.Reference_frames = tk.StringVar()
        self.Reference_frames.set("None")
        # ----------------------title label-------------------------------------
        title_label = tk.Label(self, text="SHOW ENTITIES")
        title_label.grid(row=0, column=0, sticky=tk.W)

        # ----------------------1st row ----------------------------------------
        label_First_Primary = tk.Label(self, text="Display first Primary")
        Check_flag_First_Primary = tk.Checkbutton(self, variable=self.First_Primary)
        label_First_Primary.grid(row=1, column=0, sticky=tk.W)
        Check_flag_First_Primary.grid(row=1, column=1, sticky=tk.E)

        # ----------------------2nd row ----------------------------------------
        label_Second_Primary = tk.Label(self, text="Display Second Primary")
        Check_flag_Second_Primary = tk.Checkbutton(self, variable=self.Second_Primary)
        label_Second_Primary.grid(row=2, column=0, sticky=tk.W)
        Check_flag_Second_Primary.grid(row=2, column=1, sticky=tk.E)

        # ----------------------3rd row ----------------------------------------
        label_Libration_Points = tk.Label(self, text="Display Libration Points")
        Option_menu_Libration_Points = ttk.Combobox(
            self,
            textvariable=self.Libration_Points,
            values=["ALL", "L1", "L2", "L3", "L1 + L2", "L1 + L3", "L2 + L3", "None"],
        )
        label_Libration_Points.grid(row=3, column=0, sticky=tk.W)
        Option_menu_Libration_Points.grid(row=3, column=1, sticky=tk.E)

        # ----------------------4th row ----------------------------------------
        label_Reference_frames = tk.Label(self, text="Display Frames")
        Option_menu_Frames = ttk.Combobox(
            self,
            textvariable=self.Reference_frames,
            values=["ALL", "Synonical", "Inertial", "None"],
        )
        label_Reference_frames.grid(row=4, column=0, sticky=tk.W)
        Option_menu_Frames.grid(row=4, column=1, sticky=tk.E)

        # ----------------------5th row ----------------------------------------
        Save_button2 = tk.Button(
            self, text="Save", command=self.save_primaries_settings
        )
        Save_button2.grid(row=5, column=2, sticky=(tk.W + tk.E))

    def save_primaries_settings(self):
        primaries_settings = {
            "First_Primary": self.First_Primary.get(),
            "Second_Primary": self.Second_Primary.get(),
            "Libration_Points": self.Libration_Points.get(),
            "Reference_frames": self.Reference_frames.get(),
        }
        return primaries_settings


class GUI_main_root(tk.Tk):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.title("PLOT SETTINGS")
        self.geometry("400x400")
        self.resizable(width=False, height=False)
        self.columnconfigure(0, weight=1)


if __name__ == "__main__":
    app = GUI_main_root()
    Frame1 = Show_views_frame(app)
    Frame1.grid(sticky=(tk.E + tk.W + tk.N + tk.S))
    Frame2 = Show_entities_frame(app)
    Frame2.grid(sticky=(tk.E + tk.W + tk.N + tk.S))
    app.mainloop()
