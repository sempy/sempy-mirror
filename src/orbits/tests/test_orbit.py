# -*- coding: utf-8 -*-

import unittest

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.orbits.halo import Halo
from src.orbits.plyap import Plyap

class TestOrbit(unittest.TestCase):

    def test_halo_orbit_init(self):

        # The following data was obtained with SEMAT
        m = 1
        dm = -1
        Azdim = 12000
        Az = 0.031217481789802
        Azdim_estimate = 12000
        Az_estimate = 0.031217481789802

        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
        orbit = Halo(cr3bp, cr3bp.l2, Halo.Family.northern, Azdim=12000)

        self.assertEqual(orbit.m_richardson, m)
        self.assertEqual(orbit.dm_richardson, dm)
        self.assertEqual(orbit.Azdim, Azdim) 
        self.assertAlmostEqual(orbit.Az, Az) 
        self.assertEqual(orbit.Azdim_estimate, Azdim_estimate)
        self.assertAlmostEqual(orbit.Az_estimate, Az_estimate) 

    def test_orbit_warns(self):
        """ Test that exceptions are raised."""
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

        with self.assertRaisesRegex(Exception, 'Family must be one between northern and southern'):
            Halo(cr3bp, cr3bp.l2, Halo.Family.planar, Azdim=12000)
        with self.assertRaisesRegex(Exception, 'Family must be planar'):
            Plyap(cr3bp, cr3bp.l2, Plyap.Family.southern, Axdim=12000)
        with self.assertRaises(Exception):
            Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Azdim=12000, Cjac=3.12)
        with self.assertRaises(Exception):
            Plyap(cr3bp, cr3bp.l2, Plyap.Family.planar, Axdim=12000, Cjac=3.12)


if __name__ == '__main__':
    unittest.main()
