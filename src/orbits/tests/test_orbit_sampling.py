"""
Test orbit sampling routines.

"""

import unittest
import os
import numpy as np
from scipy.io import loadmat

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.orbits.halo import Halo


class TestOrbitSampling(unittest.TestCase):

    def test_halo_orbit_sampling(self):

        dtm = loadmat(os.path.join(os.path.dirname(__file__), 'data', 'halo_orbit_patching.mat'),
                      squeeze_me=True, struct_as_record=False)

        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
        halo = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Azdim=30e3)
        halo.interpolation()

        nb_revs = 10
        nb_patch_rev = 50
        t_patch, state_patch = halo.sampling(nb_revs, nb_patch_rev)

        np.testing.assert_allclose(t_patch, dtm['tp_syn'], rtol=0.0, atol=1e-10)
        np.testing.assert_allclose(state_patch, dtm['yp_syn'].T, rtol=0.0, atol=1e-10)


if __name__ == '__main__':
    unittest.main()
