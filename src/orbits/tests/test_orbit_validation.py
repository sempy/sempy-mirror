# -*- coding: utf-8 -*-

import unittest
import scipy.io as sio
import os.path


from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.orbits.halo import Halo

dirname = os.path.dirname(__file__)


class TestOrbit(unittest.TestCase):

    def test_halo_orbit_computation(self):
        """Test Halo orbit crtbp"""

        # The following data was obtained with SEMAT

        filename = os.path.join(dirname, 'data', 'halo_computed_northern_L2_EM_system')
        loaded_mat = sio.loadmat(filename)
        mat_dictionary = sorted(loaded_mat.keys())[-1]
        mat_values = loaded_mat[mat_dictionary]

        # li
        li_struct = mat_values['li'][0][0][0][0]
        li_number = li_struct[0][0][0]
        li_gamma_i = li_struct[1][0][0]
        li_position = li_struct[4][0]
        li_Ci = li_struct[5][0][0]
        li_Ei = li_struct[6][0][0]

        orbit_type = mat_values['type'][0][0][0]
        orbit_family = mat_values['family'][0][0][0]
        orbit_m = mat_values['m'][0][0][0][0]
        orbit_dm = mat_values['dm'][0][0][0][0]
        Azdim = mat_values['Azdim'][0][0][0][0]
        Az = mat_values['Az'][0][0][0][0]
        Azdim_estimate = mat_values['Azdim_estimate'][0][0][0][0]
        Az_estimate = mat_values['Az_estimate'][0][0][0][0]

        stateSTM0 = mat_values['y0'][0][0]

        T12 = mat_values['T12'][0][0][0][0]
        T = mat_values['T'][0][0][0][0]
        C = mat_values['C'][0][0][0][0]
        E = mat_values['E'][0][0][0][0]

        tV = mat_values['tv'][0][0]

        stateV = mat_values['yv'][0][0]

        # the test begins
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

        orbit = Halo(cr3bp, cr3bp.l2, Halo.Family.northern, Azdim=12000)
        orbit.computation()

        # li
        self.assertEqual(orbit.li.number, li_number)
        self.assertAlmostEqual(orbit.li.gamma_i, li_gamma_i)

        # testing a tuple that is almost equal
        self.assertAlmostEqual(orbit.li.position[0], li_position[0])
        self.assertAlmostEqual(orbit.li.position[1], li_position[1])
        self.assertAlmostEqual(orbit.li.position[2], li_position[2])

        self.assertAlmostEqual(orbit.li.Ci, li_Ci)
        self.assertAlmostEqual(orbit.li.Ei, li_Ei)

        self.assertEqual(orbit.kind.lower(), orbit_type.lower())
        self.assertEqual(orbit.family.name.lower(), orbit_family.lower())

        self.assertEqual(orbit.m_richardson, orbit_m)
        self.assertEqual(orbit.dm_richardson, orbit_dm)
        self.assertAlmostEqual(orbit.Azdim, Azdim)
        self.assertAlmostEqual(orbit.Az, Az)
        self.assertEqual(orbit.Azdim_estimate, Azdim_estimate)
        self.assertAlmostEqual(orbit.Az_estimate, Az_estimate)

        for i in range(len(orbit.state0)):
            self.assertAlmostEqual(orbit.state0[i], stateSTM0[i][0])

        self.assertAlmostEqual(orbit.T12, T12, places=5)
        self.assertAlmostEqual(orbit.T, T, places=5)
        self.assertAlmostEqual(orbit.C, C)
        self.assertAlmostEqual(orbit.E, E)

        self.assertAlmostEqual(orbit.t_vec[0], tV[0][0])
        self.assertAlmostEqual(orbit.t_vec[-1], tV[-1][0], places=5)

        for i in range(42):
            self.assertAlmostEqual(orbit.state_vec[0][i], stateV[0][i], places=5)
            self.assertAlmostEqual(orbit.state_vec[-1][i], stateV[-1][i], places=1)

        for i in range(6):
            self.assertAlmostEqual(orbit.state_vec[-1][i], stateV[-1][i], places=5)

    def test_halo_orbit_interpolation(self):

        # The following data was obtained with SEMAT

        filename = os.path.join(dirname, 'data', 'halo_interpolated_northern_L1_EM_system.mat')
        loaded_mat = sio.loadmat(filename)
        mat_dictionary = sorted(loaded_mat.keys())[-1]
        mat_values = loaded_mat[mat_dictionary]

        # li
        li_struct = mat_values['li'][0][0][0][0]
        li_number = li_struct[0][0][0]
        li_gamma_i = li_struct[1][0][0]
        li_position = li_struct[4][0]
        li_Ci = li_struct[5][0][0]
        li_Ei = li_struct[6][0][0]

        orbit_type = mat_values['type'][0][0][0]
        orbit_family = mat_values['family'][0][0][0]
        orbit_m = mat_values['m'][0][0][0][0]
        orbit_dm = mat_values['dm'][0][0][0][0]
        Azdim = mat_values['Azdim'][0][0][0][0]
        Az = mat_values['Az'][0][0][0][0]
        Azdim_estimate = mat_values['Azdim_estimate'][0][0][0][0]
        Az_estimate = mat_values['Az_estimate'][0][0][0][0]

        stateSTM0 = mat_values['y0'][0][0]

        T12 = mat_values['T12'][0][0][0][0]
        T = mat_values['T'][0][0][0][0]
        C = mat_values['C'][0][0][0][0]
        E = mat_values['E'][0][0][0][0]

        tV = mat_values['tv'][0][0]

        stateV = mat_values['yv'][0][0]

        # the test begins
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

        orbit = Halo(cr3bp, cr3bp.l1, Halo.Family.northern, Azdim=12000)
        orbit.interpolation()

        # li
        self.assertEqual(orbit.li.number, li_number)
        self.assertAlmostEqual(orbit.li.gamma_i, li_gamma_i)

        # testing a tuple that is almost equal
        self.assertAlmostEqual(orbit.li.position[0], li_position[0])
        self.assertAlmostEqual(orbit.li.position[1], li_position[1])
        self.assertAlmostEqual(orbit.li.position[2], li_position[2])

        self.assertAlmostEqual(orbit.li.Ci, li_Ci)
        self.assertAlmostEqual(orbit.li.Ei, li_Ei)

        self.assertEqual(orbit.kind.lower(), orbit_type.lower())
        self.assertEqual(orbit.family.name.lower(), orbit_family.lower())

        self.assertEqual(orbit.m_richardson, orbit_m)
        self.assertEqual(orbit.dm_richardson, orbit_dm)
        self.assertAlmostEqual(orbit.Azdim, Azdim, places=5)
        self.assertAlmostEqual(orbit.Az, Az)
        self.assertEqual(orbit.Azdim_estimate, Azdim_estimate)
        self.assertAlmostEqual(orbit.Az_estimate, Az_estimate)

        for i in range(len(orbit.state0)):
            self.assertAlmostEqual(orbit.state0[i], stateSTM0[i][0])

        self.assertAlmostEqual(orbit.T12, T12, places=5)
        self.assertAlmostEqual(orbit.T, T, places=5)
        self.assertAlmostEqual(orbit.C, C)
        self.assertAlmostEqual(orbit.E, E)

        self.assertAlmostEqual(orbit.t_vec[0], tV[0][0])
        self.assertAlmostEqual(orbit.t_vec[-1], tV[-1][0], places=5)

        for i in range(42):
            self.assertAlmostEqual(orbit.state_vec[0][i], stateV[0][i], places=5)
            self.assertAlmostEqual(orbit.state_vec[-1][i], stateV[-1][i], places=1)

        for i in range(6):
            self.assertAlmostEqual(orbit.state_vec[-1][i], stateV[-1][i], places=5)

    def test_orbit_warns(self):
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

        with self.assertRaisesRegex(Exception, 'Provided Azdim out of abacus bounds'):
            orbit = Halo(cr3bp, cr3bp.l2, Halo.Family.northern, Azdim=120000)
            orbit.interpolation()

        with self.assertRaisesRegex(Exception, 'Provided Cjac out of abacus bounds'):
            orbit = Halo(cr3bp, cr3bp.l2, Halo.Family.northern, Cjac=5)
            orbit.interpolation()


if __name__ == '__main__':
    unittest.main()
