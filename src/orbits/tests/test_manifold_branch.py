"""
Test the initialization and crtbp of a manifold branch.

"""

import os.path
import unittest
import numpy as np
from scipy.io import loadmat

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.orbits.nrho import NRHO

dirname = os.path.dirname(__file__)


class TestManifoldBranch(unittest.TestCase):

    def test_manifold_branch_computation(self):

        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

        # results obtained in SEMAT for EM L2 northern NRHO with periselene altitude of 8000 km
        # unstable, exterior manifold branch with initial position at 0.25*T, initial distance to
        # manifold of 50 km and propagation for t=20.0
        filename = os.path.join(dirname, 'data', 'test_manifold_branch.mat')
        branch_mat = loadmat(filename, squeeze_me=True,
                             struct_as_record=False)['manifold_branch']
        eigvec_mat = (branch_mat.yv0 - branch_mat.yorbit[:6]) / cr3bp.d_man

        # init NRHO
        nrho = NRHO(cr3bp, cr3bp.l2, NRHO.Family.northern, Altpdim=8e3)
        nrho.interpolation()

        # init and compute manifold branch
        branch = nrho.ManifoldBranch(nrho, 'unstable', 'exterior')
        branch.computation(0.25, 20.0)

        self.assertEqual(branch.stability, 'unstable')
        self.assertEqual(branch.way, 'exterior')
        self.assertEqual(branch.orbit, nrho)
        self.assertEqual(branch.theta, 0.25)
        self.assertEqual(branch.d_man, cr3bp.d_man)

        np.testing.assert_array_equal(branch.eigvec0, nrho.unstable_dir)
        np.testing.assert_allclose(branch.eigvec, eigvec_mat, rtol=0.0, atol=1e-10)
        np.testing.assert_allclose(branch.state_stm_orbit, branch_mat.yorbit, rtol=0.0, atol=1e-10)
        np.testing.assert_allclose(branch.state_vec[0, :], branch_mat.yv0, rtol=0.0, atol=1e-6)
        np.testing.assert_allclose(branch.state_vec[-1, :], branch_mat.yve, rtol=0.0, atol=1e-6)

    def test_manifold_branch_warns(self):
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
        nro = NRHO(cr3bp, cr3bp.l2, NRHO.Family.northern, Altpdim=8e3)
        nro2 = NRHO(cr3bp, cr3bp.l2, NRHO.Family.northern, Altpdim=15e3)
        nro.interpolation()
        nro2.interpolation()

        with self.assertRaisesRegex(Exception, 'Orbit must be an instance of CrtbpOrbit class.'):
            branch = nro.ManifoldBranch(None, 'stable', 'interior')
            branch.computation(0.5, 10.)
        with self.assertRaisesRegex(Exception,
                                    'The selected orbit does not have stable manifolds.'):
            branch = nro2.ManifoldBranch(nro2, 'stable', 'interior')
            branch.computation(0.5, 10.)
        with self.assertRaisesRegex(Exception,
                                    'The selected orbit does not have unstable manifolds.'):
            branch = nro2.ManifoldBranch(nro2, 'unstable', 'interior')
            branch.computation(0.5, 10.)
        with self.assertRaisesRegex(NotImplementedError, 'Computation of center manifold branches '
                                                         'not currently implemented.'):
            branch = nro.ManifoldBranch(nro, 'center', 'interior')
            branch.computation(0.5, 10.)
        with self.assertRaisesRegex(Exception, 'Manifold branch kind must be one of stable, '
                                               'unstable or center.'):
            branch = nro.ManifoldBranch(nro, '', 'interior')
            branch.computation(0.5, 10.)
        with self.assertRaisesRegex(Exception,
                                    'Manifold branch way must be either interior or exterior.'):
            branch = nro.ManifoldBranch(nro, 'stable', '')
            branch.computation(0.5, 10.)
        with self.assertRaisesRegex(ValueError, 'Position must be expressed as a fraction of the '
                                                'orbit period between 0 and 1.'):
            branch = nro.ManifoldBranch(nro, 'stable', 'interior')
            branch.computation(1.5, 10.)


if __name__ == '__main__':
    unittest.main()
