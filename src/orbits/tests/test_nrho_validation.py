"""
Validation of NRHO initialization, interpolation and postprocessing routines.

"""

import unittest
import os.path
import numpy as np
from scipy.io import loadmat

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.orbits.nrho import NRHO


dirname = os.path.dirname(__file__)


class TestNRHO(unittest.TestCase):

    def test_nrho_interpolation(self):
        """Validation of initialization, interpolation and postprocessing for NROs. """

        # results obtained in SEMAT for L2 southern NRHO with Azdim=75e3 km
        filename = os.path.join(dirname, 'data', 'test_nrho_validation.mat')
        nro_mat = loadmat(filename,
                          squeeze_me=True, struct_as_record=False)['nro']

        # init NRHO
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
        nrho = NRHO(cr3bp, cr3bp.l2, NRHO.Family.southern, Azdim=75e3)
        nrho.interpolation()

        # init parameters
        self.assertEqual(nrho.Azdim_estimate, 75e3)
        self.assertAlmostEqual(nrho.Az_estimate, 75e3 / cr3bp.L)
        self.assertEqual(nrho.Ax, None)
        self.assertEqual(nrho.Axdim, None)
        self.assertEqual(nrho.Ax_estimate, None)
        self.assertEqual(nrho.Axdim_estimate, None)

        # Richardson parameters
        self.assertEqual(nrho.m_richardson, nro_mat.m)
        self.assertEqual(nrho.dm_richardson, nro_mat.dm)

        # orbit's parameters
        self.assertAlmostEqual(nrho.Az, nro_mat.Az, places=6)
        self.assertAlmostEqual(nrho.Azdim, nro_mat.Azdim, places=5)
        self.assertAlmostEqual(nrho.C, nro_mat.C, places=6)
        self.assertAlmostEqual(nrho.E, nro_mat.E, places=6)
        self.assertAlmostEqual(nrho.T, nro_mat.T, places=6)
        self.assertAlmostEqual(nrho.T12, nro_mat.T12, places=6)

        # periselene and aposelene
        self.assertAlmostEqual(nrho.m2_apsis['apoapsis_radius'], nro_mat.apogee.altitude, places=6)
        for i in (0, 2):
            self.assertAlmostEqual(nrho.m2_apsis['apoapsis_position'][i],
                                   nro_mat.apogee.position[i], places=6)
        self.assertAlmostEqual(abs(nrho.m2_apsis['apoapsis_position'][1]),
                               abs(nro_mat.apogee.position[1]), places=6)
        self.assertAlmostEqual(nrho.m2_apsis['periapsis_radius'], nro_mat.perigee.radius,
                               places=6)
        self.assertAlmostEqual(nrho.m2_apsis['periapsis_altitude'], nro_mat.perigee.altitude,
                               places=6)
        for i in range(3):
            self.assertAlmostEqual(nrho.m2_apsis['periapsis_position'][i],
                                   nro_mat.perigee.position[i], places=6)
        self.assertAlmostEqual(nrho.m2_apsis['periapsis_radius'], nro_mat.minDistToM2, places=6)
        self.assertAlmostEqual(nrho.m2_apsis['apoapsis_radius'], nro_mat.maxDistToM2, places=6)
        self.assertAlmostEqual(nrho.m1_apsis['periapsis_radius'], nro_mat.minDistToM1, places=6)
        self.assertAlmostEqual(nrho.m1_apsis['apoapsis_radius'], nro_mat.maxDistToM1, places=6)

        # initial state and state vector
        np.testing.assert_allclose(nrho.t_vec, nro_mat.tv, rtol=0., atol=1e-8)
        np.testing.assert_allclose(nrho.state0, nro_mat.y0[:6], rtol=0., atol=1e-8)
        np.testing.assert_allclose(nrho.state_vec, nro_mat.yv, rtol=0., atol=1e-5)
        np.testing.assert_allclose(nrho.state_vec[-1, 6:].reshape(6, 6), nro_mat.monodromy,
                                   rtol=0., atol=1e-5)


if __name__ == '__main__':
    unittest.main()
