# pylint: disable=too-many-instance-attributes,too-few-public-methods,invalid-name,too-many-lines
# -*- coding: utf-8 -*-


class Orbit:
    """This class contains methods and attributes that will be common for:
    Keplerian Orbits, CRTBP Orbits and Four Body Problem Orbits. In this way,
    those orbits will inherit Orbit class.

    Parameters
    ----------
    state0 : ndarray or None
        Orbit's initial state [-].
    reference_frame : None
        Reference frame in which lies the orbit.

    Attributes
    ----------
    state0 : ndarray or None
        Orbit's initial state. [-]
    state_vec : ndarray or None
        State vector, array of states results after the orbit's initial state propagation [-].
    t_vec : ndarray or None
        Time vector, array of time results after the orbit's initial state propagation [-].
    reference_frame : None
        Reference frame in which lies the orbit.
    kind : str
        Orbit kind.

    """

    def __init__(self, state0=None, reference_frame=None):
        """Inits Orbit class."""
        self.state0 = state0
        self.state_vec = self.t_vec = None
        self.reference_frame = reference_frame
        self.kind = self.__class__.__name__

#
#
# class Qhalo(CrtbpOrbit):
#     """Quasi-Halo orbit. """
#
#     def __init__(self, cr3bp, libp, family, state0=None, **kwargs):
#         """Inits Qhalo class."""
#         CrtbpOrbit.__init__(self, cr3bp, libp, family, state0=state0, **kwargs)
#
#
# class Liss(CrtbpOrbit):
#     """Lissajous orbit. """
#
#     def __init__(self, cr3bp, libp, family, state0=None, **kwargs):
#         """Inits Liss class."""
#         CrtbpOrbit.__init__(self, cr3bp, libp, family, state0=state0, **kwargs)

#
# class DRO(CrtbpOrbit):
#     """Distant Retrograde orbit. """
#
#     def __init__(self, cr3bp, libp, family, state0=None, **kwargs):
#         """Inits DRO class."""
#         CrtbpOrbit.__init__(self, cr3bp, libp, family, state0=state0, **kwargs)
