"""
Vertical Lyapunov Orbit.
"""

from src.orbits.crtbporbit import CrtbpOrbit


class Vlyap(CrtbpOrbit):
    """Vertical Lyapunov orbit. """

    def __init__(self, cr3bp, libp, family, state0=None, **kwargs):
        """Inits Vlyap class."""
        CrtbpOrbit.__init__(self, cr3bp, libp, family, state0=state0, **kwargs)
