# -*- coding: utf-8 -*-

from src.orbits.crtbporbit import CrtbpOrbit
from src.orbits.thirdorder_orbit import ThirdOrderOrbit
from src.orbits.halo import Halo
from src.propagation.cr3bp_propagator import Cr3bpSynodicPropagator
from src.solve.diff_corr_3d import DiffCorr3D


class Dro(CrtbpOrbit):
    """ The DRO class can initialize, compute or interpolate a Direct Retrograde orbit from an abacus.
    The class inherits from CrtbpOrbit.

    This class will initialize and compute a periodic DRO orbit in a given CRTBP system,
    previously created with the Cr3bp class, about the libration point selected
    and with the orbit desired Amplitude (Axdim) or desired Jacobi constant (Cjac).

    If an orbit's initial state is not provided into the Halo class constructor,
    within the crtbp method, a third-order Richardson approximation is used as a first guess
    that will serve as our orbit's initial state. Then, a differential correction process is applied,
    resulting in a corrected state that can be propagated to obtain the corrected orbit.
    For more details, See Koon et al. 2011, chapter 6.

    On the other hand, if the orbit's initial state is passed to the Halo class constructor,
    no third-order guess is used and the crtbp method will apply a
    differential correction procedure to this initial sate in order to obtain the orbit.

    Another option to get the DRO orbit is provided by the interpolation method. It will fetch a state
    stateV_aba, from an abacus according to Axdim or Cjac passed in the initialization. The initial state stat0 is
    the estimated using np.polyfit and np.polyval, and the usual crtbp method is applied to this initial state
    in order to get the interpolated DRO.

    This class offers two options (through keyword arguments) to initialize the orbit:
        1) Via the orbit extension.
        2) Via the Jacobi constant.

    Instances of this class have access to the Orbit and CrtbpOrbit instance attributes.


    Parameters
    ----------
    cr3bp : object
        An object created with the Cr3bp class, e.g. cr3bp.
    libp : object
        A libration point of the object cr3bp, e.g. cr3bp.l2.
    family : object
        The orbit family which is desired to be created, e.g. Halo.Family.southern.
    state0 : ndarray
        Orbit's initial state.

    **kwargs:
    Axdim : float
        Orbit in-plane extension [km].

    Cjac : float
        Jacobi constant.



    Attributes
    ----------
    cr3bp : object
        Attributes of this object can be accessed (check attributes in Cr3bp class).
    li : object
        The libration point which was selected for the orbit design. Attributes of this libration point can be accessed
        (check the lagrange points attributes in Cr3bp class).
    family : object
        The orbit family selected.
    family.name : str
        The kind of family that was selected, string with the family name.
    m : int
        Parameter regarding to the orbit classes proposed by Richardson (see Richardson 1980).
    dm : int
        Delta_m parameter coming from the theory proposed by Richardson (see Richardson 1980).
    kwargs : str
        Will store the keyword used to initialize the orbit (Axdim or Cjac).

    state0 : ndarray
        Orbit's initial state.
    stateV : ndarray
        State vector, array of states results after the orbit's initial state propagation.
    tV : ndarray
        Time vector, array of time results after the orbit's initial state propagation.
    reference_frame : Reference frame in which lies the orbit.
    kind : str
        Orbit kind.

    Attributes
    ----------
    For keyword Axdim.

    Axdim : float
        Orbit in-plane extension [km].
    Ax : float
        Orbit adimensional in-plane extension [-].
    Axdim_estimate : float
        Value equal to Axdim if there is not orbit postprocessing,
        after orbit postprocessing it will take the refined extension.
    Ax_estimate : float
        Value equal to Ax if there is not orbit postprocessing,
        after orbit postprocessing it will take the refined resulting adimensional extension.

    Attributes
    ----------
    For keyword Cjac.

    C : float
        Jacobi constant value.
    E : float
        Orbit energy value.

    """

    def __init__(self, cr3bp, libp, family='PLANAR', state0=None, **kwargs):
        """Inits Halo class."""
        CrtbpOrbit.__init__(self, cr3bp, libp, family='PLANAR', state0=state0, **kwargs)

    def computation(self, select_fixed_dim=None):
        """Constructs the DRO orbit.

        This method will compute a DRO orbit, whether or not an initial state is passed
        into the DRO class constructor.

        For DRO orbits, a  differential correction procedure is used.
        For more details on the differential correction ,  please check  the DiffCorr3D class documentation.


        If initial state is not provided to the Halo class constructor,
        the method uses a third order richardson approximation as first guess,
        then a differential correction procedure is applied to this first guess concatenated with the STM0,
        this differential correction will produce a corrected state together with STM (stateSTM0_c)
        that can be propagated in order to obtain a Halo orbit.

        On the other hand, if an initial state (state0) is passed to the Halo class constructor,
        this method will apply a differential correction procedure to
        this initial sate concatenated with the STM0, this will produce a
        corrected state together with STM (stateSTM0_c) that can be propagated
        in order to obtain a Halo orbit.

        Ultimately, crtbp method will use postprocess method to update some features of the orbit, like Jacobi
        constant, Energy, Az and Azdim

        Parameters
        ----------
        select_fixed_dim:
            Specifies the differential correction procedure, e.g. Halo.DiffCorrFixDim.x0.

        Returns
        -------
        state0:
            which is the Orbit's initial state.
        stateV:
            State vector, array of states results after the orbit's initial state propagation.
        tV:
            Time vector, array of time results after the orbit's initial state propagation.


        """

        # Whether or not an initial state is passed into the Halo class constructor
        if self.state0 is None:
            first_guess = ThirdOrderOrbit(self)
            self.state0 = first_guess.state0
        else:
            self.state0 = self.state0  # this is the state0 that must be passed through the Halo constructur, if a ThirdOrderOrbit(self) is not computed

        # Differential correction procedure starts here
        if select_fixed_dim is None:
            if self.Az_estimate > 0.0520:
                select_fixed_dim = Halo.DiffCorrFixDim.x0
                xi0 = (2, 4)
            else:
                select_fixed_dim = Halo.DiffCorrFixDim.z0  # DiffCorrFixDim.z0 should be used with small Az, DiffCorrFixDim.x0 otherwise.
                xi0 = (0, 4)

        elif select_fixed_dim == Halo.DiffCorrFixDim.z0:
            xi0 = (0, 4)  # index correspond to x and vy. That means that x0 and vy0 are going to be corrected
            if self.Az_estimate > 0.0520:
                print('You are trying to compute a big halo orbit. For these orbits, the differential corrector '
                      'Halo.DiffCorrFixDim.x0 is preferable.')

        elif select_fixed_dim == Halo.DiffCorrFixDim.x0:
            xi0 = (2, 4)  # index correspond to z and vy. That means that z0 and vy0 are going to be corrected

        xif = (3, 5)  # index correspond to vx and vz, (vx, vz) = 0 targeted

        # Performing the selected differential correction procedure
        diff_c = DiffCorr3D(self.cr3bp.mu)
        self.state0, self.T12, self.T = diff_c.diff_corr_3d_bb(self.state0, xi0, xif)

        # Propagation during one orbital period (already done in postprocess...)
        prop = Cr3bpSynodicPropagator(self.cr3bp.mu, with_stm=True)
        self.t_vec, self.state_vec, _, _ = prop.propagate([0., self.T], self.state0)

        self.postprocess()
