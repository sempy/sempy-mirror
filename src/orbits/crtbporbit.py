# pylint: disable=invalid-name, too-many-instance-attributes, too-few-public-methods

from enum import Enum
import itertools
from multiprocessing import Pool
import numpy as np
from scipy.interpolate import CubicSpline

import src.crtbp.jacobi as comp
import src.init.defaults as dft
from src.orbits.orbit import Orbit
from src.init.constants import A_MAT, H1_MAT, H2_MAT
from src.propagation.cr3bp_propagator import Cr3bpSynodicPropagator, Cr3bpArcLengthPropagator


class CrtbpOrbit(Orbit):
    """ CrtbpOrbit class will initialize and provide the common attributes and
    properties for orbits generated in a Circular Restricted Three Body Problem system.
    This class inherits from Orbit.

    CrtbpOrbit class will initialize and provide the common attributes and properties for:
        1) Halo orbits.
        2) Near Rectilinear Halo Orbits (NRHO).
        3) Vertical Lyapunov orbits (Vlyap).
        4) Quasihalo orbits (Qhalo).
        5) Lissajous orbits (Liss).
        6) Planar Lyapunov orbits (Plyap).
        7) Distant Retrograde orbits (DRO).
        8) Third-order orbits.

    This class offers two options (through keyword arguments) to initialize a CRTBP orbit:
        1) Via the orbit extension.
        2) Via the Jacobi constant.

    Instances of this class has access to the parent class, Orbit, instance attributes.

    Parameters
    ----------
    cr3bp: Cr3bp
        An object created with the Cr3bp class, e.g. cr3bp.
    libp: Cr3bp.Libp
        A libration point of the object cr3bp, e.g. cr3bp.l2.
    family: Family
        The orbit family which is desired to be created, e.g. CrtbpOrbit.Family.northern.
    state0: ndarray or None
        Orbit's initial state [-].

    Other Parameters
    -----------------
    Azdim : float
        Orbit vertical extension [km].
    Axdim : float
        Orbit planar extension [km].
    Cjac : float
        Jacobi constant [-].

    Attributes
    ----------
    cr3bp : Cr3bp
        Attributes of this object can be accessed (check attributes in Cr3bp class).
    li : Cr3bp.Libp
        The libration point which was selected for the orbit design. Attributes of this libration
        point can be accessed (check the lagrange points attributes in Cr3bp class).
    family : Family
        The orbit family selected.
    family.name: str
        The kind of family that was selected, string with the family name.
    Az : float or None
        Orbit adimensional vertical extension [-].
    Azdim : float or None
        Orbit vertical extension [km].
    Az_estimate : float or None
        Value equal to Az if there is not orbit postprocessing,
        after orbit postprocessing it will take the refined resulting adimensional vertical
        extension [-].
    Azdim_estimate : float or None
        Value equal to Azdim if there is not orbit postprocessing,
        after orbit postprocessing it will take the refined resulting vertical extension [km].
    Ax : float or None
        Orbit adimensional planar extension [-].
    Axdim : float or None
        Orbit planar extension [km].
    Ax_estimate : float or None
        Value equal to Az if there is not orbit postprocessing,
        after orbit postprocessing it will take the refined resulting adimensional planar
        extension [-].
    Axdim_estimate : float or None
        Value equal to Azdim if there is not orbit postprocessing,
        after orbit postprocessing it will take the refined resulting planar extension [km].
    C : float
        Jacobi constant value [-].
    E : float
        Orbit energy value [-].
    T12 : float
        Orbit's half period [-].
    T : float
        Orbit's period in normalized units [-].
    m1_apsis : dict
        Dictionary with periapsis and apoapsis radius, altitude and position w.r.t. m1.
    m2_apsis : dict
        Dictionary with periapsis and apoapsis radius, altitude and position w.r.t. m2.
    m_richardson: int
        Parameter regarding to the orbit classes proposed by Richardson (see Richardson 1980).
    dm_richardson: int
        Delta_m parameter coming from the theory proposed by Richardson (see Richardson 1980).
    monodromy : ndarray
        Monodromy matrix.
    eigvals : ndarray
        Eigenvalues of the monodromy matrix.
    eigvecs : ndarray
        Eigenvectors corresponding to `eigvals`.
    stable_dir : ndarray or None
        Eigenvector along the stable manifold direction at the orbit's initial state.
    unstable_dir : ndarray or None
        Eigenvector along the unstable manifold direction at the orbit's initial state.
    center_dir : ndarray
        Eigenvectors along the center manifold directions at the orbit's initial state.
    stability_idx : ndarray
        Orbit's stability indexes.
    continuation : dict
        Dictionary with free-variables vector and null vector of the Jacobian for continuation
        procedure.
    manifolds : dict
        Dictionary storing several instances of the inner class `ManifoldBranch` that describe
        the manifold tubes associated to the orbit. Valid keys are 'stable_interior',
        'stable_exterior', 'unstable_interior', 'unstable_exterior' to identify all combinations of
        manifold stability and directions. Each key is mapped to a list of `ManifoldBranch` objects
        corresponding to different branches belonging to the same tube computed for different
        positions along the orbit.
    kwargs : str
        Will store the keyword used to initialize the orbit (Azdim or Axdim or Cjac).

    """

    def __init__(self, cr3bp, libp, family, state0=None, **kwargs):
        """Inits CrtbpOrbit class."""
        Orbit.__init__(self, state0)
        self.cr3bp = cr3bp
        self.li = libp
        self.family = family

        self.Az = self.Azdim = self.Az_estimate = self.Azdim_estimate = None
        self.Ax = self.Axdim = self.Ax_estimate = self.Axdim_estimate = None
        self.C = self.E = self.T12 = self.T = None

        # dict to characterize periapsis and apoapsis w.r.t. the two primaries
        self.m1_apsis = {}
        self.m2_apsis = {}

        # initialization via Jacobi constant or extension (child class specific)
        if len(kwargs) == 1:  # one kwargs in orbit's constructor (Axdim, Azdim or Cjac)
            self.set_init_parameter(kwargs)
        elif len(kwargs) == 0 and self.state0 is not None:  # initial state provided
            print('Orbit initialized via its initial state')
        else:
            raise Exception('Must provide exactly one orbit initialization parameter between '
                            '(Azdim, Cjac) for northern and southern families or (Axdim, Cjac) '
                            'for planar ones')

        # Richardson parameter for third-order approximation
        self.m_richardson = self.family.m_richardson
        self.dm_richardson = self.get_dm_richardson(libp)

        # stability properties
        self.monodromy = None  # monodromy matrix
        self.eigvals = self.eigvecs = None  # eigenvalues and eigenvectors of the monodromy matrix
        self.stable_dir = self.unstable_dir = self.center_dir = None  # stable/unstable/center dir
        self.stability_idx = None  # stability indexes

        # dict to store the free variables vector and null vector of the Jacobian for continuation
        self.continuation = {}

        # dict to store the manifold branches for different manifolds kinds and ways
        self.manifolds = dict(zip(('stable_interior', 'stable_exterior', 'unstable_interior',
                                   'unstable_exterior'), ([], [], [], [])))

        self.kwargs = kwargs

    def set_init_parameter(self, kwargs):
        """Init a Cr3bpOrbit class instance via Jacobi constant or extension. """

        if 'Azdim' in kwargs:
            self.Azdim = self.Azdim_estimate = float(kwargs['Azdim'])
            self.Az = self.Az_estimate = self.Azdim / self.cr3bp.L
        elif 'Axdim' in kwargs:
            self.Axdim = self.Axdim_estimate = float(kwargs['Axdim'])
            self.Ax = self.Ax_estimate = self.Axdim / self.cr3bp.L
        elif 'Cjac' in kwargs:
            self.C = float(kwargs['Cjac'])
            self.E = -0.5 * self.C
        else:
            raise Exception('Must provide Azdim or Cjac parameter value for northern and '
                            'southern families, Axdim or Cjac parameter value for planar ones')

    class Family(Enum):
        """Family is an inner class that will be used to specify the family of an orbit during
        its initialization. This class inherits from Enum.

        Options are the following instances:
            Family.northern
            Family.southern
            Family.planar

        Enum members aka instance (e.g. northern) also have a property that contains just their
        item name, therefore Family.northern.name will return 'northern'.
        The value of Enum members will be passed to the __init__ method converting them
        (the values) into attributes.

        """

        northern = 1
        southern = 3
        planar = 0

        def __init__(self, m_richardson):
            """Inits parameter regarding to the orbit classes proposed by Richardson. """
            self.m_richardson = m_richardson

    def get_dm_richardson(self, libp):
        """Delta_m parameter coming from the theory proposed by Richardson."""

        if libp in (self.cr3bp.l1, self.cr3bp.l3):
            self.dm_richardson = 2 - self.m_richardson
        elif libp == self.cr3bp.l2:
            self.dm_richardson = self.m_richardson - 2
        elif libp in (self.cr3bp.l4, self.cr3bp.l5):
            self.dm_richardson = 0
        else:
            raise Exception('libp must be one between l1, l2, l3, l4, l5')

        return self.dm_richardson

    def get_abacus_name(self, separator='_'):
        """Returns the name of the abacus for the family of the instantiated orbit.

        The returned string adopts the following convention:
        1) first two capital letters corresponding to the initials of the primaries' names
        2) capital `L` followed by an integer denoting the libration point associated to the family
        3) name of the family between `northern` and `southern`
        4) kind of orbit, i.e. `Halo`
        the above substrings are separated by `separator` whose default value is ``_``

        For example, for an L2 northern Halo in the Earth-Moon system it will return the following:
        `EM_L2_northern_Halo`

        Parameters
        ----------
        separator : str, optional
            Separator between substrings that composes the abacus name. Default is ``_``.

        Returns
        -------
        abacus_name : str
            Name of the abacus associated to the orbit's family.

        """

        system = self.cr3bp.m1.name[0] + self.cr3bp.m2.name[0]
        libp = 'L' + str(self.li.number)
        abacus_name = separator.join([system, libp, self.family.name, self.kind])
        return abacus_name

    def interp_eval(self, idp_aba, out_aba, root=0):
        """Returns an estimation of the orbit's parameters computed from interpolated data.

        Parameters
        ----------
        idp_aba : ndarray
            Abacus data for the independent variable chosen as orbit's initialization parameter.
        out_aba : tuple
            Abacus data for the target orbit's parameters to be matched.
        root : int, optional
            Index of the interpolating spline's root selected for the estimation of the orbit's
            parameters. Lower values correspond to orbits closer to their respective libration
            point. Default is zero for which the closest orbit to the corresponding libration
            point is obtained.

        Returns
        -------
        out_val : tuple
            Estimated values for the matched orbit's parameters.

        """
        idx_aba, idp_key = np.arange(idp_aba.size, dtype=np.float64), list(self.kwargs.keys())[0]
        idp_interp = CubicSpline(idx_aba, idp_aba, extrapolate=False)
        out_interp = [CubicSpline(idx_aba, o, extrapolate=False) for o in out_aba]
        idp_roots = idp_interp.roots()
        if idp_roots.size < 1:
            raise Exception(f"Provided {idp_key} out of abacus bounds")
        if idp_roots.size < root + 1:
            raise Exception(f"Only {idp_roots.size} roots exist for the provided {idp_key}")
        return [o(idp_roots[root]) for o in out_interp]

    def postprocess(self, time_steps=dft.time_steps):
        """This method will update Az, Azdim, C, E, state_vec and distances from primaries. """

        self.C = comp.jacobi(self.state0, self.cr3bp.mu)  # Jacobi constant [-]
        self.E = -0.5 * self.C  # orbit's energy [-]

        # propagator with odd number of time steps to include both apsis in the orbit state vector
        time_steps = time_steps if time_steps % 1 == 1 else time_steps + 1  # odd number of steps
        prop = Cr3bpSynodicPropagator(self.cr3bp.mu, with_stm=True, time_steps=time_steps)

        # orbit state vector for one complete revolution
        self.t_vec, self.state_vec, _, _ = prop.propagate([0.0, self.T], self.state0)

        # check that the apsis opposite to state0 is included in the propagated states
        np.testing.assert_allclose(self.t_vec[prop.time_steps // 2], self.T12,
                                   rtol=0.0, atol=np.finfo(np.float64).eps)

        # orbit extension selecting the maximum x or z value among state vector elements
        if self.family in (CrtbpOrbit.Family.northern, CrtbpOrbit.Family.southern):
            self.Az = np.fabs(self.state_vec[:, 2]).max()  # maximum z coordinate [-]
            self.Azdim = self.Az * self.cr3bp.L  # maximum z coordinate [km]
        elif self.family == CrtbpOrbit.Family.planar:
            self.Ax = np.fabs(self.state_vec[:, 0]).max()  # maximum x coordinate [-]
            self.Axdim = self.Ax * self.cr3bp.L  # maximum x coordinate [km]

        # maximum and minimum distances from primaries, apoapsis and periapsis positions
        self.set_apsis(self.m1_apsis, self.cr3bp.m1_pos, self.cr3bp.m1.Rm)
        self.set_apsis(self.m2_apsis, self.cr3bp.m2_pos, self.cr3bp.m2.Rm)

        # monodromy matrix and orbit stability properties
        stm12 = self.state_vec[prop.time_steps // 2, 6:].reshape((6, 6))  # STM after half period
        self.monodromy = np.linalg.multi_dot([A_MAT, H1_MAT, stm12.T, H2_MAT, A_MAT, stm12])
        self.stability_properties()

    def stability_properties(self):
        """Compute orbit's stability characteristics.

        This method computes several metrics to asses the orbit's stability in a linear sense and
        exploit the related manifold structures. The following attributes are computed:

            1) eigvals: eigenvalues of the monodromy matrix
            2) eigvecs: eigenvectors corresponding to eigvals
            3) stable_dir: eigenvector for the stable manifold direction (if exists)
            4) unstable_dir: eigenvector for the unstable manifold direction (if exists)
            5) center_dir: eigenvectors for the center manifold direction (in progress)
            6) stability_idx: stability indexes (nu1, nu2) related to the eigenvalues of the
                monodromy matrix and computed as described below.

        nu1 = 1/2 * (lambda + 1/lambda) with lambda, 1/lambda non-trivial, real eigenvalues
        of the monodromy matrix.

        nu2 = 1/2 * (lambda + lambda_conj) = real(lambda) = real(lambda_conj) with lambda,
        lambda_conj non-trivial, complex-conjugate eigenvalues of the monodromy matrix.

        """

        self.eigvals, self.eigvecs = np.linalg.eig(self.monodromy)  # eigenvalues, eigenvectors

        # indexes corresponding to the two non-trivial pairs
        idx_non_trv = np.argpartition(np.abs(self.eigvals - (1.0 + 0.0j)), 1)[2:]

        # eigenvalues with unique real part and relative multiplicity (all, non-trivial)
        _, idx_unq, mult = np.unique(np.real(self.eigvals), return_index=True, return_counts=True)
        mult_non_trv = mult.compress(np.isin(idx_unq, idx_non_trv, assume_unique=True))

        if np.min(mult_non_trv) == 1:  # two non-trivial, real eigenvalues (lambda, 1/lambda)
            idx_stb = np.argmin(np.abs(self.eigvals))  # index for stable direction
            idx_unstb = np.argmax(np.abs(self.eigvals))  # index for unstable direction
            idx_ctr = idx_non_trv.compress(np.isin(idx_non_trv, [idx_stb, idx_unstb],
                                                   assume_unique=True, invert=True))  # center dir.
            self.stable_dir = np.real(self.eigvecs[:, idx_stb])
            self.unstable_dir = np.real(self.eigvecs[:, idx_unstb])
            self.center_dir = self.eigvecs.take(idx_ctr, axis=1)
            self.stability_idx = \
                0.5 * np.real(np.array([(self.eigvals[idx_ctr[0]] + self.eigvals[idx_ctr[1]]),
                                        (self.eigvals[idx_stb] + self.eigvals[idx_unstb])]))

        else:  # four non-trivial, complex conjugate eigenvalues lying on the unit circle
            self.center_dir = self.eigvecs.take(idx_non_trv, axis=1)
            idx_non_trv_unq = idx_non_trv.compress(np.isin(idx_non_trv, idx_unq,
                                                           assume_unique=True))
            self.stability_idx = \
                np.real([self.eigvals[idx_non_trv_unq[0]], self.eigvals[idx_non_trv_unq[1]]])

        self.stability_idx.sort()  # sort stability indexes in ascending order

    def set_apsis(self, apsis_dict, primary_pos, primary_rm):
        """Set the apsis characteristics.

        This methods computes the periapsis and apoapsis radii, altitudes and position w.r.t.
        a given primary to update the `m1_apsis` and `m2_apsis` attributes.

        Parameters
        ----------
        apsis_dict : dict
            Dictionary to be updated among `m1_apsis` and `m2_apsis`.
        primary_pos : ndarray
            Position of one of the primaries.
        primary_rm : float
            Mean radius of one of the primaries.

        """

        dist, idx = self.cr3bp.distance_to_primary(self.state_vec, primary_pos)
        for i, apsis in enumerate(['periapsis', 'apoapsis']):
            apsis_dict['_'.join([apsis, 'radius'])] = dist[i]
            apsis_dict['_'.join([apsis, 'radius_dim'])] = dist[i] * self.cr3bp.L
            apsis_dict['_'.join([apsis, 'altitude'])] = dist[i] - primary_rm / self.cr3bp.L
            apsis_dict['_'.join([apsis, 'altitude_dim'])] = dist[i] * self.cr3bp.L - primary_rm
            apsis_dict['_'.join([apsis, 'position'])] = self.state_vec[idx[i], 0:3]

    def sampling(self, nb_revs, nb_patch_rev, kind='period'):
        """Returns patch points obtained from a uniform period fraction sampling or a constant
        arclength sampling of the orbit.

        Parameters
        ----------
        nb_revs : int
            Number of revolutions.
        nb_patch_rev : int
            Number of patch points per revolution.
        kind : str, optional
            Kind of sampling among `period` and `arclength`.
            If `period` is selected, a uniform period fraction sampling is performed resulting in
            a constant transfer time between subsequent patch points.
            If `arclength` is selected, a uniform arclength fraction sampling is performed
            resulting in constant arclength trajectory segments between subsequent patch points.
            Default is `period`.

        Returns
        -------
        t_patch : ndarray
            Time at patch points.
        state_patch : ndarray
            Orbit's state at patch points.

        """

        if kind == 'period':
            prop = Cr3bpSynodicPropagator(self.cr3bp.mu)  # propagator instance

            # time and states at patch points for the first revolution
            t_patch_rev = np.linspace(0.0, self.T * (1.0 - 1.0 / nb_patch_rev), nb_patch_rev)
            _, state_patch_rev, _, _ = prop.propagate(t_patch_rev, self.state0)

            # time and states at patch points for all revolutions from orbit periodicity
            t_patch = np.tile(t_patch_rev, nb_revs) + self.T * np.repeat(np.arange(nb_revs),
                                                                         nb_patch_rev)
            state_patch = np.tile(state_patch_rev, (nb_revs, 1))

        elif kind == 'arclength':
            prop = Cr3bpArcLengthPropagator(self.cr3bp.mu, time_steps=None)  # propagator instance

            # orbit arclength over one revolution
            _, state_vec, _, _ = prop.propagate([0.0, self.T], self.state0)

            # arclength samples and events
            arclength_samp = np.linspace(0.0, state_vec[-1, -1] * (1.0 - 1.0 / nb_patch_rev),
                                         nb_patch_rev)
            arclength_evt = [lambda t, state, al=al: state[-1] - al for al in arclength_samp]

            # time and states at patch points for the first revolution
            _, _, t_patch_rev, state_patch_rev = prop.propagate([0.0, self.T], self.state0,
                                                                events=arclength_evt)

            # time and states at patch points for all revolutions from orbit periodicity
            t_patch = np.tile(np.asarray(t_patch_rev).squeeze(), nb_revs) + \
                self.T * np.repeat(np.arange(nb_revs), nb_patch_rev)
            state_patch = np.tile(np.asarray(state_patch_rev).squeeze()[:, 0:6], (nb_revs, 1))

        else:
            raise Exception('kind must be either period or arclength')

        return t_patch, state_patch

    def single_manifold_computation(self, stability, way, theta, t_prop, d_man=None,
                                    time_steps=None, max_internal_steps=dft.max_internal_steps,
                                    events=None):
        """Computes a manifold tube of given stability and way.

        This method computes a manifold tube of given stability (stable, unstable) and way
        (interior/exterior) grouping together a number of manifold branches departing the orbit in
        different positions as specified by `theta`.
        Each branch is propagated forward or backward in time  (for unstable and stable ones
        respectively) for the amount of time specified by `t_prop` or until a terminal event
        is tracked.

        For more information on how events should be specified see:
        (https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.solve_ivp.html)

        Parameters
        ----------
        stability : str
            Stability of the manifold between `stable` and `unstable`.
        way: str
            Way of the manifold between `interior` and `exterior`.
        theta : int or ndarray
            If given as integer, number of equally spaced positions along the orbit for which
            a manifold branch is computed. If given as ndarray, positions along the orbit for
            which a manifold branch is computed expressed as fractions of the orbit's period `T`.
            In the second case all array elements must lie in the interval [0, 1].
        t_prop : float
            Time for which each branch is propagated in normalized units.
        d_man : float or None, optional
            Initial distance to manifold (expressed in km) or None. If None, the default value
            given by the orbit's `cr3bp` attribute will be used.
        time_steps : int, optional
            Number of points equally spaced in time in which the solution is returned or None.
            Default is None for which the value defined in `src.init.defaults` scaled by the
            ratio propagation time over orbital period is used.
        max_internal_steps : int, optional
            Maximum number of (internally defined) steps allowed for each integration point.
            This option is used only if `events` is ``None``.
            Default defined in `src.init.defaults`.
        events : function, iterable or None
            Event or events to track. Default is None.

        Returns
        -------
        CrtbpOrbit.manifolds[name] : list
            List of computed manifold branches.

        """

        if isinstance(theta, int) and theta > 1:
            theta = np.linspace(0.0, 1.0, theta)  # equally spaced positions in [0, 1]
        name = '_'.join([stability, way])  # name of the tube as in CrtbpOrbit.manifolds dict keys

        for pos in theta:  # loop over all positions to compute the corresponding branches
            branch = self.ManifoldBranch(self, stability, way)
            branch.computation(pos, t_prop, d_man=d_man, time_steps=time_steps,
                               max_internal_steps=max_internal_steps, events=events)
            self.manifolds[name].append(branch)
        return self.manifolds[name]

    def all_manifolds_computation(self, theta, t_prop, d_man=None, processes=4, time_steps=None,
                                  max_internal_steps=dft.max_internal_steps, events=None):
        """Computes all manifold tubes for the given orbit.

        This method calls `single_manifold_computation` with all possible combinations of the
        input parameters `stability` and `way` to compute all the manifold tubes associated to a
        given orbit.

        For more information on how events should be specified see:
        (https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.solve_ivp.html)

        Parameters
        ----------
        theta : int or ndarray
            If given as integer, number of equally spaced positions along the orbit for which
            a manifold branch is computed. If given as ndarray, positions along the orbit for
            which a manifold branch is computed expressed as fractions of the orbit's period `T`.
            In the second case all array elements must lie in the interval [0, 1].
        t_prop : float
            Time for which each branch is propagated in normalized units.
        d_man : float or None, optional
            Initial distance to manifold (expressed in km) or None. If None, the default value
            given by the orbit's cr3bp attribute will be used.
        processes : int or None, optional
            Number of processes to spawn while computing different manifolds tubes in parallel.
            Default is `4`.
        time_steps : int, optional
            Number of points equally spaced in time in which the solution is returned or None.
            Default is None for which the value defined in `src.init.defaults` scaled by the
            ratio propagation time over orbital period is used.
        max_internal_steps : int, optional
            Maximum number of (internally defined) steps allowed for each integration point.
            This option is used only if `events` is ``None``.
            Default defined in `src.init.defaults`.
        events : function, iterable or None
            Event or events to track. Default is None.

        """

        # split tube names (stability, way) and parameters as in single_manifold_computation()
        names = [k.split('_') for k in self.manifolds.keys()]
        params = [(name[0], name[1], theta, t_prop, d_man, time_steps, max_internal_steps, events)
                  for name in names]

        if processes is not None:  # compute the four tubes in parallel
            with Pool(processes=processes) as pool:
                out = pool.starmap(self.single_manifold_computation, params)
        else:
            out = itertools.starmap(self.single_manifold_computation, params)
        self.manifolds = dict(zip(self.manifolds.keys(), out))

    class ManifoldBranch:
        """ManifoldBranch class initializes and compute a manifold branch for a given orbit.

        The initial state on the branch is obtained perturbing the corresponding state on the
        orbit along the direction given by the stable/unstable eigenvector and the specified way
        between interior and exterior. The magnitude of the perturbation, or initial distance from
        the manifold, is given by the input parameter `d_man` or set equal to its default value
        specified by the `cr3bp` attribute of the associated `orbit` class instance.

        The stable/unstable eigenvector in the specified position is computed as follows:

        eigvec(t) = STM(t, 0) * eigvec(0)

        with STM(t,0) state transition matrix propagated from the orbit's initial state up to the
        desired position and eigvec(0) stable/unstable eigenvector at the orbit's initial state.
        Since the above matrix product does not preserve the unitary norm of eigvec(0), eigvec(t)
        is then normalized such that norm(eigvec(t)) = 1.0.

        The parameter `way` which specifies the two possible directions for the selected manifold,
        i.e. interior or exterior, and is mapped to a scalar multiplier applied to the initial
        perturbation equal to -1 for `interior` and +1 for `exterior`.

        The initial state on the branch is then computed adding the previous perturbation to the
        local orbit's state. The former is finally propagated for the specified amount of time or
        until a terminal event is tracked to generate the requested trajectory.

        Parameters
        ----------
        orbit : CrtbpOrbit
            CrtbpOrbit object
        stability : str
            Stability of the manifold between `stable` and `unstable`.
        way: str
            Way of the manifold between `interior` and `exterior`.

        Attributes
        ----------
        orbit : CrtbpOrbit
            CrtbpOrbit object
        stability : str
            Stability of the manifold between `stable` and `unstable`.
        way: str
            Way of the manifold between `interior` and `exterior`.
        theta : float
            Position along the orbit specified as fraction of the orbital period in [0, 1] from
            which the branch is started.
        d_man : float
            Distance to manifold or magnitude of the initial perturbation applied to the
            orbit's state.
        eigvec0 : ndarray
            Eigenvector that determines the direction of the specified manifold at the orbit's
            initial state.
        eigvec : ndarray
            Eigenvector that determines the direction of the specified manifold at the orbit's
            state corresponding to `pos`.
        state_stm_orbit : ndarray
            Orbit's state corresponding to `pos` concatenated with the STM propagated from the
            orbit's initial state up to `pos`.
        state_vec : ndarray
            States along the manifold branch.
        t_vec : ndarray
            Time along the manifold branch.
        t_event : list or None
            Time vector(s) corresponding to tracked events or None if no events are specified.
        state_event : list or None
            State vector(s) corresponding to tracked events or None if no events are specified.

        """

        def __init__(self, orbit, stability, way):
            """Inits ManifoldBranch class. """

            if not isinstance(orbit, CrtbpOrbit):
                raise Exception('Orbit must be an instance of CrtbpOrbit class.')
            self.orbit = orbit
            if stability == 'stable':
                if self.orbit.stable_dir is None:
                    raise Exception('The selected orbit does not have stable manifolds.')
                self.stability = stability
                self.eigvec0 = self.orbit.stable_dir
            elif stability == 'unstable':
                if self.orbit.unstable_dir is None:
                    raise Exception('The selected orbit does not have unstable manifolds.')
                self.stability = stability
                self.eigvec0 = self.orbit.unstable_dir
            elif stability == 'center':
                raise NotImplementedError('Computation of center manifold branches not currently '
                                          'implemented.')
            else:
                raise Exception('Manifold branch kind must be one of stable, unstable or center.')
            if way not in ('interior', 'exterior'):
                raise Exception('Manifold branch way must be either interior or exterior.')
            self.way = way

            self.theta = self.d_man = self.eigvec = self.state_stm_orbit = \
                self.state_vec = self.t_vec = self.state_event = self.t_event = None

        def computation(self, theta, t_prop, d_man=None, time_steps=None,
                        max_internal_steps=dft.max_internal_steps, events=None):
            """Compute the initial state on the manifold branch and propagate the last for the
            specified amount of time or until a terminal event is tracked.

            For more information on how events should be specified see:
            (https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.solve_ivp.html)

            Parameters
            ----------
            theta : float
                Position along the orbit specified as fraction of the orbital period in [0, 1]
                from which the branch is started.
            t_prop : float
                Time for which the branch is propagated in normalized units.
            d_man : float or None, optional
                Initial distance to manifold (expressed in km) or None. If None, the default value
                given by the orbit's cr3bp attribute will be used.
            time_steps : int, optional
                Number of points equally spaced in time in which the solution is returned or None.
                Default is None for which the value defined in `src.init.defaults` scaled by the
                ratio propagation time over orbital period is used.
            max_internal_steps : int, optional
                Maximum number of (internally defined) steps allowed for each integration point.
                This option is used only if `events` is None.
                Default defined in `src.init.defaults`.
            events : function, iterable or None
                Event or events to track. Default is None.

            """

            if theta < 0.0 or theta > 1.0:  # position along the orbit as fraction of period
                raise ValueError('Position must be expressed as a fraction of the orbit period '
                                 'between 0 and 1.')
            self.theta = theta
            if d_man is None:  # distance to manifold in adimensional units
                self.d_man = self.orbit.cr3bp.d_man  # default value determined by CR3BP system
            else:
                self.d_man = d_man / self.orbit.cr3bp.L  # user-defined value (input in km)

            # compute the manifold branch direction for the given position along the orbit
            prop = Cr3bpSynodicPropagator(self.orbit.cr3bp.mu, with_stm=True, time_steps=None,
                                          max_internal_steps=max_internal_steps)
            _, state_vec, _, _ = prop.propagate([0.0, theta * self.orbit.T], self.orbit.state0)
            self.state_stm_orbit = state_vec[-1, :]  # state on the orbit concatenated with STM
            self.eigvec = self.state_stm_orbit[6:].reshape((6, 6)).dot(self.eigvec0)
            self.eigvec /= np.linalg.norm(self.eigvec)  # eigenvector for branch direction

            # compute the initial state on the branch
            scaler = 1 if self.way == 'exterior' else -1
            state0_branch = self.state_stm_orbit[:6] + self.d_man * scaler * self.eigvec

            # propagate the manifold branch (forward in time for unstable, backward for stable)
            t_span = [0.0, t_prop] if self.stability == 'unstable' else [0.0, -t_prop]
            t_steps = time_steps if time_steps is not None else \
                dft.time_steps * np.max([1, int(t_prop // self.orbit.T)])
            prop = Cr3bpSynodicPropagator(self.orbit.cr3bp.mu, time_steps=t_steps,
                                          max_internal_steps=max_internal_steps)
            self.t_vec, self.state_vec, self.t_event, self.state_event = \
                prop.propagate(t_span, state0_branch, events=events)
