
from src.orbits.crtbporbit import CrtbpOrbit


class Plyap(CrtbpOrbit):
    """Planar Lyapunov orbit. """

    def __init__(self, cr3bp, libp, family, state0=None, **kwargs):
        """Inits Plyap class."""
        if family != CrtbpOrbit.Family.planar:
            raise Exception('Family must be planar')
        CrtbpOrbit.__init__(self, cr3bp, libp, family, state0=state0, **kwargs)
