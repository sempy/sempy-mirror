# pylint: disable=too-many-branches
from enum import Enum
import numpy as np

from src.init.defaults import single_shooting_precision
from src.solve.diff_corr_3d import DiffCorr3D
from src.solve.multiple_shooting import MultipleShooting
from src.propagation.cr3bp_propagator import Cr3bpSynodicPropagator
from src.utils.json_encoder import save_json, load_json
from src.orbits.crtbporbit import CrtbpOrbit
from src.orbits.thirdorder_orbit import ThirdOrderOrbit


class Halo(CrtbpOrbit):
    """ Halo class will initialize and compute or initialize and interpolate a Halo orbit.
    This class inherits from CrtbpOrbit.

    This class will initialize and compute a periodic Halo orbit in a given CRTBP system,
    previously created with Cr3bp class, about the libration point selected
    and with the orbit desired size (Azdim) or desired Jacobi constant (Cjac).
    As well, the family of the halo orbit can be selected, between southern or northern.

    Here, if an orbit's initial state is not provided into the Halo class constructor,
    in the crtbp method, it is used a third-order Richardson approximation as first guess
    that will serve as our orbit's initial state. Then, a differential correction process is
    applied, to the first guess together with STM0, resulting in a corrected state that can be
    propagated in order to obtain a halo periodic orbit.
    For more details, See Koon et al. 2011, chapter 6.

    On the other hand, if the orbit's initial state is passed to the Halo class constructor,
    no third-order guess is used and the crtbp method will apply a
    differential correction procedure to this initial sate passed, concatenated with the STM0,
    this will produce a corrected state together with STM that can be propagated
    in order to obtain a Halo orbit.

    Another option to get a Halo orbit is provided by Halo class, interpolation.
    Initialization is done in the same way as before, then interpolation method can be used.
    Interpolation method, will fetch stateV_aba, from an abacus according to Azdim or Cjac passed
    in the initialization, with this data the state0 is estimated using np.polyfit and
    np.polyval, then crtbp method is applied to this initial state in order to get the
    interpolated Halo orbit.

    This class offers two options (through keyword arguments) to initialize a Halo orbit:
        1) Via the orbit extension.
        2) Via the Jacobi constant.

    Instances of this class has access to the Orbit and CrtbpOrbit instance attributes.

    Parameters
    ----------
    cr3bp : Cr3bp
        An object created with the Cr3bp class, e.g. cr3bp.
    libp : Cr3bp.Libp
        A libration point of the object cr3bp, e.g. cr3bp.l2.
    family : Family
        The orbit family which is desired to be created, e.g. Halo.Family.southern.
    state0 : ndarray
        Orbit's initial state [-].

    Other Parameters
    ----------------
    Azdim : float
        Orbit vertical extension [km].
    Cjac : float
        Jacobi constant [-].
    T : float
        Orbit's period [-].

    Attributes
    ----------
    cr3bp : Cr3bp
        Attributes of this object can be accessed (check attributes in Cr3bp class).
    li : Cr3bp.Libp
        The libration point which was selected for the orbit design. Attributes of this libration
        point can be accessed (check the lagrange points attributes in Cr3bp class).
    family : Family
        The orbit family selected.
    family.name: str
        The kind of family that was selected, string with the family name.
    Az : float or None
        Orbit adimensional vertical extension [-].
    Azdim : float or None
        Orbit vertical extension [km].
    Az_estimate : float or None
        Value equal to Az if there is not orbit postprocessing,
        after orbit postprocessing it will take the refined resulting adimensional vertical
        extension [-].
    Azdim_estimate : float or None
        Value equal to Azdim if there is not orbit postprocessing,
        after orbit postprocessing it will take the refined resulting vertical extension [km].
    C : float
        Jacobi constant value [-].
    E : float
        Orbit energy value [-].
    T12 : float
        Orbit's half period [-].
    T : float
        Orbit's period in normalized units [-].
    m1_apsis : dict
        Dictionary with periapsis and apoapsis radius, altitude and position w.r.t. m1.
    m2_apsis : dict
        Dictionary with periapsis and apoapsis radius, altitude and position w.r.t. m2.
    m_richardson: int
        Parameter regarding to the orbit classes proposed by Richardson (see Richardson 1980).
    dm_richardson: int
        Delta_m parameter coming from the theory proposed by Richardson (see Richardson 1980).
    monodromy : ndarray
        Monodromy matrix.
    eigvals : ndarray
        Eigenvalues of the monodromy matrix.
    eigvecs : ndarray
        Eigenvectors corresponding to `eigvals`.
    stable_dir : ndarray or None
        Eigenvector along the stable manifold direction at the orbit's initial state.
    unstable_dir : ndarray or None
        Eigenvector along the unstable manifold direction at the orbit's initial state.
    center_dir : ndarray
        Eigenvectors along the center manifold directions at the orbit's initial state.
    stability_idx : ndarray
        Orbit's stability indexes.
    continuation : dict
        Dictionary with free-variables vector and null vector of the Jacobian for continuation
        procedure.
    manifolds : dict
        Dictionary storing several instances of the inner class `ManifoldBranch` that describe
        the manifold tubes associated to the orbit. Valid keys are 'stable_interior',
        'stable_exterior', 'unstable_interior', 'unstable_exterior' to identify all combinations of
        manifold stability and directions. Each key is mapped to a list of `ManifoldBranch` objects
        corresponding to different branches belonging to the same tube computed for different
        positions along the orbit.
    kwargs : str
        Will store the keyword used to initialize the orbit (Azdim or Axdim or Cjac).

    """

    def __init__(self, cr3bp, libp, family, state0=None, **kwargs):
        """Inits Halo class."""
        if family not in (CrtbpOrbit.Family.northern, CrtbpOrbit.Family.southern):
            raise Exception('Family must be one between northern and southern')

        CrtbpOrbit.__init__(self, cr3bp, libp, family, state0=state0, **kwargs)

    def set_init_parameter(self, kwargs):
        """Init a Halo class instance via Jacobi constant, extension or period. """

        if 'T' in kwargs:
            self.T = float(kwargs['T'])
            self.T12 = self.T / 2.0
        else:
            CrtbpOrbit.set_init_parameter(self, kwargs)

    class DiffCorrFixDim(Enum):
        """DiffCorrFixDim is an inner class that will be used to select a given differential
        correction. This class inherits from Enum.

        Options are the following instances:
            Halo.DiffCorrFixDim.x0
            Halo.DiffCorrFixDim.z0
            Halo.DiffCorrFixDim.min_norm
            Halo.DiffCorrFixDim.period
            Halo.DiffCorrFixDim.cjac

        """

        x0 = 'x0_fixed'
        z0 = 'z0_fixed'
        min_norm = 'minimum_norm'
        period = 'period'
        cjac = 'cjac'

        def __init__(self, fixed):
            """Inits DiffCorrFixDim class."""
            self.fixed = fixed

    def computation(self, fix_dim=None):
        """Computes a Halo orbit.

        This method computes an Halo orbit applying a differential correction procedure to correct
        an approximate initial state and satisfy the periodicity condition for the resulting
        trajectory within the desired numerical accuracy.

        Once a refined initial condition is obtained, several orbit characteristics such as
        vertical extension, Jacobi constants, apsides location, radii, altitudes and stability
        properties are computed via the `postprocess` class method.

        An initial guess for the orbit initial state might be obtained with the following methods:

            1. User defined value passed as input parameter to the `Halo` class constructor.
            2. Interpolated value obtained from abacus data via the `interpolate` class method.
            3. Approximate value obtained from a third-order Richardson expansion.

        Five correction algorithms are available to refine the provided initial guess. Underlying
        hypothesis, free variables, constraints and domains of application are described below:

            1. Fixed orbit's period (Halo.DiffCorrFixDim.period).
            2. Fixed Jacobi constant (Halo.DiffCorrFixDim.cjac).
            3. Minimum-norm solution (Halo.DiffCorrFixDim.min_norm).
            4. Fixed z-component of the initial state (Halo.DiffCorrFixDim.z0).
            5. Fixed x-component of the initial state (Halo.DiffCorrFixDim.x0).

        1. Fixed orbit period:

        Single shooting procedure derived from the multiple shooting implementation using only
        two patch points at the trajectory's endpoints. Those patch points are selected as the
        orbit's initial state for two subsequent revolutions, i.e. for t=(0, T) with T desired
        orbit's period. [y0 vx0 vz0]^T and [yf vxf vzf]^T are fixed to zero while [x0 z0 vy0]^T
        and [xf zf vyf]^T are treated as free variables subject to the correction procedure.
        Time at patch points is fixed in order to match the target orbit's period T.

        2. Fixed Jacobi constant:

        Single shooting procedure derived from the multiple shooting implementation using only
        two patch points at the trajectory's endpoints. Those patch points are selected as the
        orbit's initial state for two subsequent revolutions, i.e. for t=(0, T) with T orbit's
        period. [y0 vx0 vz0]^T and [yf vxf vzf]^T are fixed to zero while [x0 z0 vy0]^T and
        [xf zf vyf]^T are treated as free variables subject to the correction procedure. Time at
        patch points is allowed to vary in order to adjust the orbit's period while an additional
        constraint is imposed on the first patch point to match the desired Jacobi constant.

        3. Minimum-norm solution:

        Single shooting procedure specifically implemented for the computation of three-dimensional
        periodic orbits in the CR3BP that exhibit a symmetry with respect to the `xz` plane.
        [x0 z0 vy0 T12]^T where T12 is the orbit's half-period are treated as free variables
        subject to the correction procedure while [y vx vz]^T are constrained to be zero at t=T12
        so as to obtain a periodic orbit symmetric w.r.t. the `xz` plane.

        4. Fixed z-component of the initial state:

        Single shooting procedure specifically implemented for the computation of three-dimensional
        periodic orbits in the CR3BP that exhibit a symmetry with respect to the `xz` plane.
        [x0 vy0]^T are treated as free variables subject to the correction procedure while
        [y vx vz]^T are constrained to be zero at the first `xz` plane crossing so as to obtain a
        periodic orbit symmetric w.r.t. the same plane. The propagation interval T12 is allowed to
        vary in order to satisfy the periodicity and symmetry constraints.
        If the input parameter `fix_dim` is None, this method is the default choice for "small"
        Halo orbits, where "small orbits" are defined based on the results obtained for the
        Earth-Moon system as Halos with vertical extension less than 0.052 in normalized units.

        5. Fixed x-component of the initial state:

        Single shooting procedure specifically implemented for the computation of three-dimensional
        periodic orbits in the CR3BP that exhibit a symmetry with respect to the `xz` plane.
        [z0 vy0]^T are treated as free variables subject to the correction procedure while
        [y vx vz]^T are constrained to be zero at the first `xz` plane crossing so as to obtain a
        periodic orbit symmetric w.r.t. the same plane. The propagation interval T12 is allowed to
        vary in order to satisfy the periodicity and symmetry constraints.
        If the input parameter `fix_dim` is None, this method is the default choice for "big"
        Halo orbits, where "big orbits" are defined based on the results obtained for the
        Earth-Moon system as Halos with vertical extension greater than 0.052 in normalized units.

        Parameters
        ----------
        fix_dim : Halo.DiffCorrFixDim.fix_dim or None, optional
            Differential correction procedure to be employed (e.g. Halo.DiffCorrFixDim.x0) or None.
            Default is `None` for which a suitable procedure is selected based on the estimated
            orbit's vertical extension parameter.

        """
        if self.state0 is None:  # approximate initial state from 3rd-order Richardson expansion
            self.state0 = ThirdOrderOrbit(self).state0
        if fix_dim in (Halo.DiffCorrFixDim.cjac, Halo.DiffCorrFixDim.period):
            if self.T12 is None:
                raise Exception('No estimate for the orbit\'s period has been provided')
            diff_corr = MultipleShooting(self.cr3bp, precision=single_shooting_precision)
            prop = Cr3bpSynodicPropagator(self.cr3bp.mu, time_steps=None)
            t_patch, state_patch, _, _ = prop.propagate([0.0, self.T12], self.state0)
            state_mask = np.zeros(12, dtype=np.bool)
            state_patch[:, 1::2], state_mask[1::2] = 0.0, True
            if fix_dim == Halo.DiffCorrFixDim.cjac:  # single shooting with Jacobi cst. constraint
                if self.C is None:
                    raise Exception('No estimate for the Jacobi constant has been provided')
                t_corr, state_corr, sol = diff_corr.correct(t_patch, state_patch, True, (0, self.C),
                                                            None, False, state_mask=state_mask)
            else:  # single shooting with fixed time at patch points
                t_corr, state_corr, sol = diff_corr.correct(t_patch, state_patch, False, False,
                                                            None, False, state_mask=state_mask)
            if sol['err_norm'] > single_shooting_precision:  # check convergence
                raise Exception('Differential correction procedure has failed to converge')
            self.state0, self.T12, self.T = state_corr[0], t_corr[-1], t_corr[-1] * 2.0
        else:  # specialized single shooting algorithms for CR3BP periodic orbits
            diff_corr = DiffCorr3D(self.cr3bp.mu)
            if fix_dim == Halo.DiffCorrFixDim.min_norm:  # minimum-norm solution
                self.state0, self.T12, self.T, g_vec, null_vec = \
                    diff_corr.diff_corr_3d_full(self.state0, self.T12)
                self.continuation['g_vec'], self.continuation['null_vec'] = g_vec, null_vec
            else:  # single shooting with either x0 or z0 fixed
                if fix_dim == Halo.DiffCorrFixDim.z0 and (self.Az is not None and self.Az > 0.052):
                    print('You are trying to compute a big halo orbit. For these orbits, '
                          'the differential corrector Halo.DiffCorrFixDim.x0 is preferable.')
                if fix_dim is None and self.Az is None:
                    print('No Az estimate has been provided, the default corrector '
                          'Halo.DiffCorrFixDim.x0 will be used')
                if fix_dim == Halo.DiffCorrFixDim.z0 or \
                        (fix_dim is None and self.Az is not None and self.Az < 0.052):
                    self.state0, self.T12, self.T = \
                        diff_corr.diff_corr_3d_bb(self.state0, (0, 4), (3, 5))
                else:
                    self.state0, self.T12, self.T = \
                        diff_corr.diff_corr_3d_bb(self.state0, (2, 4), (3, 5))
            if np.isclose(self.T12, 0.0, rtol=0.0, atol=1e-6):
                raise ValueError('An orbit period equal to zero has been found')
        self.postprocess()

    def save_abacus(self, state0_aba, az_aba, rp_aba, cjac_aba, period_aba):
        """Save the abacus for the orbit's family on a JSON file. """
        save_json(self.get_abacus_name(), State0=state0_aba, Az=az_aba, Rp=rp_aba,
                  Cjac=cjac_aba, T=period_aba)

    def load_abacus(self):
        """Load the abacus for the orbit's family from a JSON file. """
        abacus_dict = load_json(self.get_abacus_name())
        return [np.asarray(abacus_dict[k]) for k in ('State0', 'Az', 'Rp', 'Cjac', 'T')]

    def interpolation(self, root=0, fix_dim=None):
        """Generates an Halo orbit via interpolation of abacus data.

        Firstly, the correct abacus is loaded and an approximate initial state is interpolated
        from the available abacus data.
        Secondly, a differential correction procedure is applied to correct the approximate
        state and all orbit parameters are updated via the `postprocess` class method.

        Parameters
        ----------
        root : int, optional
            Index of the interpolating spline's root selected for the estimation of the orbit's
            initial state. Lower values correspond to orbits closer to their respective libration
            point. Default is zero for which the closest orbit to the corresponding libration
            point is obtained.
        fix_dim : Halo.DiffCorrFixDim.fix_dim, optional
            Specifies the differential correction procedure, e.g. Halo.DiffCorrFixDim.x0.

        """
        state0_aba, az_aba, _, cjac_aba, per_aba = self.load_abacus()
        if 'Azdim' in self.kwargs:
            self.state0 = self.interp_eval(az_aba - self.kwargs['Azdim'] / self.cr3bp.L,
                                           (state0_aba,), root)[0]
        elif 'Cjac' in self.kwargs:
            self.state0, self.Az, self.T = self.interp_eval(cjac_aba - self.kwargs['Cjac'],
                                                            (state0_aba, az_aba, per_aba), root)
            self.T12 = self.T / 2.0
            fix_dim = Halo.DiffCorrFixDim.cjac if fix_dim is None else fix_dim
        elif 'T' in self.kwargs:
            self.state0 = self.interp_eval(per_aba - self.kwargs['T'], (state0_aba,), root)[0]
            fix_dim = Halo.DiffCorrFixDim.period if fix_dim is None else fix_dim
        else:
            raise Exception('Either Azdim, Cjac or T must be provided')
        self.computation(fix_dim=fix_dim)
