"""
Utility classes to encode and decode an abacus into or from a JSON file

"""

import json
import numpy as np

from src.orbits.abacuses import DIRNAME


class NumpyEncoder(json.JSONEncoder):
    """JSON encoder for Numpy's types. """
    def default(self, o):
        """Return a serializable data type. """
        if isinstance(o, np.int64):
            return int(o)
        if isinstance(o, np.float64):
            return float(o)
        if isinstance(o, np.ndarray):
            return o.tolist()
        return json.JSONEncoder.default(self, o)


def save_json(name, **kwargs):
    """Serialize an abacus in a JSON file. """
    abacus_dict = {}
    for k in kwargs:
        abacus_dict[k] = kwargs[k]
    pth = ''.join([DIRNAME, name, '.json'])
    with open(pth, 'w') as fid:
        json.dump(abacus_dict, fid, cls=NumpyEncoder)


def load_json(name):
    """Load an abacus from a JSON file. """
    pth = ''.join([DIRNAME, name, '.json'])
    with open(pth) as fid:
        abacus_dict = json.load(fid)
    return abacus_dict
