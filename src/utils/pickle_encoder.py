"""
Simple encoder to serialize and deserialize a Python object using Pickle.
Warning: Pickle is dangerous, to be used for testing purposes only!

"""

import pickle


def save_pickle(obj, filename):
    """Save an object. """
    with open(filename, 'wb+') as fid:
        pickle.dump(obj, fid)


def load_pickle(filename):
    """Load an object. """
    with open(filename, 'rb') as fid:
        obj = pickle.load(fid)
        return obj
