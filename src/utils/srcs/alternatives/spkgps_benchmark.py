# pylint: disable = invalid-name, missing-function-docstring, unused-variable, fixme
"""
Benchmark different options to call the CSPICE routine spkgps_c from within a Python function
compiled Just-in-Time with Numba.

"""

from sysconfig import get_config_var
from platform import system
from os import path
from ctypes import c_void_p, c_int8, c_int32, c_double, POINTER, CDLL, CFUNCTYPE
import timeit
from cffi import FFI
from numba import jit, objmode
from numba.core.typing.cffi_utils import register_module
from numba.extending import get_cython_function_address
import numpy as np
import spiceypy as sp

from src.init.load_kernels import load_kernels, KERNELS_TO_LOAD, KERNELS_ROOT

# import CFFI dynamic wrapper (currently used in sempy)
# TODO: see `src.utils.srcs.cspice_wrapper.py` for compilation instructions.
try:
    from src.utils.libs.cspice_wrapper import lib as lib_dyna
    from src.utils.libs import cspice_wrapper
    register_module(cspice_wrapper)
    spkgps_cffi_dyna = lib_dyna.spkgps_c
except ImportError:
    spkgps_cffi_dyna = None
    lib_dyna = cspice_wrapper = None
    print('CFFI dynamic wrapper not found!')

# import CFFI static wrapper
# TODO: see `cffi_wrapper_static.py` in this folder for compilation instructions.
try:
    from cffi_wrapper_static import lib as lib_st
    import cffi_wrapper_static
    register_module(cffi_wrapper_static)
    spkgps_cffi_st = lib_st.spkgps_c
except ImportError:
    spkgps_cffi_st = None
    lib_st = cffi_wrapper_static = None
    print('CFFI static wrapper not found!')

# define additional ctypes for C pointers to arrays of 8-bit integers, 32-bit integers and doubles
c_int8_p = POINTER(c_int8)
c_int32_p = POINTER(c_int32)
c_double_p = POINTER(c_double)

# import Cython wrapper
# TODO: Install additional dependency cython with `conda install cython` and follow instructions
#  in `setup.py` in this folder.
try:
    ftype = CFUNCTYPE(c_void_p, c_int32, c_double, c_int8_p, c_int32, c_double_p, c_double_p)
    spkgps_cython = ftype(get_cython_function_address('cython_wrapper', 'spkgps'))
except ModuleNotFoundError:
    spkgps_cython = None
    print('Cython wrapper not found!')

# load the CSPICE shared object (libcspice.so or cspice.dll) and assign I/O types to spkgps_c
# Note: this is the approach used by spiceypy to load and dynamically link the CSPICE shared
# objectstatic ab
# and lspice defined below could be obtained with:
# `from spiceypy.utils.libspicehelper import libspice as lspice`
# However, here we need to define different I/O ctypes for the spkgps_c routine (c_int8_p rather
# than c_char_p) to pass strings through numba as numpy array of 8-bit integers (see how REF is
# encoded for details). If we override the argtypes attribute of spkgps_c loaded by spiceypy,
# we will have conflict when trying to call the spiceypy function spkgps.
# TODO: modify the value of `CSPICE_LIB_DIR` to point to the folder where `cspice.dll` or
#  `libcspice.so` is located.
try:
    host_os = system()
    if host_os == 'Windows':
        CSPICE_LIB = 'cspice.dll'
        CSPICE_LIB_DIR = path.join(get_config_var('srcdir'), 'Library', 'bin')
    else:
        CSPICE_LIB = 'libcspice.so'
        CSPICE_LIB_DIR = get_config_var('LIBDIR')
    lspice = CDLL(path.join(CSPICE_LIB_DIR, CSPICE_LIB))
    spkgps_dlopen = lspice.spkgps_c
    spkgps_dlopen.argtypes = [c_int32, c_double, c_int8_p, c_int32, c_double_p, c_double_p]
except OSError:
    spkgps_dlopen = None

# FFI object to pass pointers to numpy arrays from within numba jitted functions
ffi = FFI()


# define jitted functions
if spkgps_cffi_dyna is not None:
    @jit('float64[::1](int32, float64, int8[::1], int32)', nopython=True, nogil=True)
    def call_cffi_dyna(tgt, et, ref, obs):
        pos = np.empty(3)
        lt = np.empty(1)
        spkgps_cffi_dyna(tgt, et, ffi.from_buffer(ref), obs, ffi.from_buffer(pos),
                         ffi.from_buffer(lt))
        return pos
else:
    call_cffi_dyna = None


if spkgps_cffi_st is not None:
    @jit('float64[::1](int32, float64, int8[::1], int32)', nopython=True, nogil=True)
    def call_cffi_st(tgt, et, ref, obs):
        pos = np.empty(3)
        lt = np.empty(1)
        spkgps_cffi_st(tgt, et, ffi.from_buffer(ref), obs, ffi.from_buffer(pos),
                       ffi.from_buffer(lt))
        return pos
else:
    call_cffi_st = None


if spkgps_cython is not None:
    @jit('float64[::1](int32, float64, int8[::1], int32)', nopython=True, nogil=True)
    def call_cython(tgt, et, ref, obs):
        pos = np.empty(3)
        lt = np.empty(1)
        spkgps_cython(tgt, et, ffi.from_buffer(ref), obs, ffi.from_buffer(pos),
                      ffi.from_buffer(lt))
        return pos
else:
    call_cython = None


if spkgps_dlopen is not None:
    @jit('float64[::1](int32, float64, int8[::1], int32)', nopython=True, nogil=True)
    def call_dlopen(tgt, et, ref, obs):
        pos = np.empty(3)
        lt = np.empty(1)
        spkgps_dlopen(tgt, et, ffi.from_buffer(ref), obs, ffi.from_buffer(pos),
                      ffi.from_buffer(lt))
        return pos
else:
    call_dlopen = None


# this function uses the objmode context manager to fall back to the Python interpreter from
# within a numba jitted function. This approach is not recommended since it considerably degrades
# preformance
@jit(nopython=True, nogil=True)
def call_objmode(tgt, et, ref, obs):
    with objmode(pos='float64[::1]'):
        pos, lt = sp.spkgps(tgt, et, ref, obs)
    return pos


# load the kernels with the spiceypy furnsh routine. These kernels will be shared among all
# functions except spkgps_cffi_st
load_kernels()
# in order to use spkgps_c exposed by the statically linked CFFI wrapper, the kernels must be
# loaded again with the furnsh_c routine exposed by the same wrapper. This is a C function, so the
# string representing the absolute path to the kernels must be encoded as a stream of bytes before
# passing it to the function. This approach won't work from within jitted functions, see how REF is
# encoded for more details
if lib_st is not None:
    for k in KERNELS_TO_LOAD:
        kernel = path.join(KERNELS_ROOT, k).encode('utf-8')
        lib_st.furnsh_c(kernel)

# list of CSPICE wrappers we want to test
OPTS = ('cffi dynamic', 'cffi static', 'cython dynamic', 'dlopen', 'objmode')

# inputs
TGT_ID = 301  # NAIF ID for target body (Moon)
OBS_ID = 399  # NAIF ID for observer (Earth)
REF_STR = 'J2000'  # reference frame
ET0 = sp.str2et('01 January 2020 00:00:00')  # first epoch as TDB seconds past J2000
ETF = sp.str2et('01 January 2021 00:00:00')  # last epoch as TDB seconds past J2000
NB_ET = 100  # number of instants in time between ET0 and ETF for which the position is retrieved
ET_VEC = np.linspace(ET0, ETF, NB_ET)  # epochs for which the position is retrieved

# in order to pass it to jitted function, the name of the reference frame must be parsed to a numpy
# array of 8-bit integers. Its elements correspond to the ASCII codes of the characters that
# compose the frame name plus the terminator '\0'
REF = np.array([ord(c) for c in REF_STR + '\0']).astype(np.int8)

# test outputs against direct call to spiceypy.spkgps
POS_OUT = np.zeros((len(OPTS) + 1, 3))
if spkgps_cffi_dyna is not None:
    POS_OUT[0, :] = call_cffi_dyna(TGT_ID, ET0, REF, OBS_ID)
if spkgps_cffi_st is not None:
    POS_OUT[1, :] = call_cffi_st(TGT_ID, ET0, REF, OBS_ID)
if spkgps_cython is not None:
    POS_OUT[2, :] = call_cython(TGT_ID, ET0, REF, OBS_ID)
if spkgps_dlopen is not None:
    POS_OUT[3, :] = call_dlopen(TGT_ID, ET0, REF, OBS_ID)
POS_OUT[4, :] = call_objmode(TGT_ID, ET0, REF_STR, OBS_ID)
POS_OUT[5, :], _ = sp.spkgps(TGT_ID, ET0, REF_STR, OBS_ID)

print('\nAccuracy benchmark - spkgps function call:')
for i, s in enumerate(OPTS):
    if any(POS_OUT[i, :] != np.zeros(3)):
        np.testing.assert_array_equal(POS_OUT[i, :], POS_OUT[-1, :])
        print(f"{s:20s}: OK")
    else:
        print(f"{s:20s}: skipped")


# define benchmarking functions
if spkgps_cffi_dyna is not None:
    def benchmark_cffi_dyna():
        for j in ET_VEC:
            pos = call_cffi_dyna(TGT_ID, j, REF, OBS_ID)
        return pos
else:
    benchmark_cffi_dyna = None


if spkgps_cffi_st is not None:
    def benchmark_cffi_st():
        for j in ET_VEC:
            pos = call_cffi_st(TGT_ID, j, REF, OBS_ID)
        return pos
else:
    benchmark_cffi_st = None


if spkgps_cython is not None:
    def benchmark_cython():
        for j in ET_VEC:
            pos = call_cython(TGT_ID, j, REF, OBS_ID)
        return pos
else:
    benchmark_cython = None


if spkgps_dlopen is not None:
    def benchmark_dlopen():
        for j in ET_VEC:
            pos = call_dlopen(TGT_ID, j, REF, OBS_ID)
        return pos
else:
    benchmark_dlopen = None


def benchmark_objmode():
    for j in ET_VEC:
        pos = call_objmode(TGT_ID, j, REF_STR, OBS_ID)
    return pos


# benchmark
NB_RUNS = 1000  # number of runs
T_RUN = np.zeros(len(OPTS))  # store run times

if benchmark_cffi_dyna is not None:
    benchmark_cffi_dyna()
    T_RUN[0] = timeit.Timer(benchmark_cffi_dyna).timeit(number=NB_RUNS) / NB_RUNS
if benchmark_cffi_st is not None:
    benchmark_cffi_st()
    T_RUN[1] = timeit.Timer(benchmark_cffi_st).timeit(number=NB_RUNS) / NB_RUNS
if benchmark_cython is not None:
    benchmark_cython()
    T_RUN[2] = timeit.Timer(benchmark_cython).timeit(number=NB_RUNS) / NB_RUNS
if benchmark_dlopen is not None:
    benchmark_dlopen()
    T_RUN[3] = timeit.Timer(benchmark_dlopen).timeit(number=NB_RUNS) / NB_RUNS
T_RUN[4] = timeit.Timer(benchmark_objmode).timeit(number=NB_RUNS) / NB_RUNS
T_RUN *= 1e3  # run time in milliseconds

print(f"\nPerformance benchmark - spkgps function call\n"
      f"average of {NB_RUNS} runs with {NB_ET} functions call each.")
for i, s in enumerate(OPTS):
    if T_RUN[i] != 0.0:
        print(f"{s:20s}: {T_RUN[i]:20.10f} ms")
    else:
        print(f"{s:20s}: {'skipped':>20s}")
