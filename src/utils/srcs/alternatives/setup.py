# pylint: disable = fixme
"""
Build Cython wrapper to CSPICE Toolkit.
The wrapper is dynamically linked with libcspice.so or equivalent installed by spiceypy.

"""

import sysconfig
import os
from platform import system
from setuptools import Extension, setup
from Cython.Build import cythonize

# TODO: Adjust LIB_DIR and INC_DIR to point to the folders in your Conda environment where the
#  CSPICE shared object (libcspice.so or cspice.dll) and the header file SpiceUsr.h are located.
#  To compile the wrapper, open a terminal window and run `python setup.py build_ext -i` from the
#  folder where this file is located.

host_os = system()

if host_os == 'Windows':
    LIB_DIR = os.path.join(sysconfig.get_config_var('srcdir'), 'Library', 'bin')
    INC_DIR = os.path.join(sysconfig.get_config_var('srcdir'), 'Library', 'include', 'cspice')
else:
    LIB_DIR = sysconfig.get_config_var('LIBDIR')
    INC_DIR = os.path.join(sysconfig.get_config_var('INCLUDEDIR'), 'cspice')

setup(
    ext_modules=cythonize([Extension("cython_wrapper",
                                     ["cython_wrapper_dynamic.pyx"],
                                     libraries=['cspice', 'm'],
                                     library_dirs=[LIB_DIR],
                                     include_dirs=[INC_DIR],
                                     extra_link_args=['-Wl,--rpath=' + LIB_DIR])],
                          compiler_directives={'language_level': "3"})
)
