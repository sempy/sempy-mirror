"""
Cython wrapper to CSPICE Toolkit.
Additional dependencies: cython.
"""

# copy functions signatures from C header files
cdef extern from "SpiceUsr.h":
    void spkpos_c(char* targ, double et, char* ref, char* abcorr, char* obs, double ptarg[3], double* lt)

cdef extern from "SpiceUsr.h":
    void spkgps_c(int targ, double et, char* ref, int obs, double pos[3], double* lt)

# expose the C routines to Python
cdef api void spkpos(char* targ, double et, char* ref, char* abcorr, char* obs, double ptarg[3], double* lt):
    spkpos_c (targ, et, ref, abcorr, obs, ptarg, lt)

cdef api void spkgps(int targ, double et, char* ref, int obs, double pos[3], double* lt):
    spkgps_c(targ, et, ref, obs, pos, lt)
