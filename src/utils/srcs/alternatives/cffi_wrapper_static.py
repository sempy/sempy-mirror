# pylint: disable = fixme
"""
This file contains the sources to build a Python wrapper to the JPL's CSPICE Toolkit.
The CFFI (C Foreign Function Interface for Python) package is used to compile a shared object that
exposes the C routines implemented in CSPICE to Python.
The compiled wrapper is statically linked to the CSPICE library (cspice.a or cspice.lib) shipped
by JPL's NAIF.

"""

from os import path
from platform import system
from cffi import FFI

host_os = system()

# TODO : Download the CSPICE toolkit from the NAIF website
#  (https://naif.jpl.nasa.gov/naif/toolkit_C.html), unzip the archive and modify the variable
#  `CSPICE_ROOT` such that it points to the root folder of the downloaded archive.
#  Run this script to compile the wrapper.
CSPICE_ROOT = "/home/r.feynman/Programs/cspice"

# CSPICE_LIB_DIR must point to the folder in CSPICE_ROOT where cspice.lib or cspice.a is located
# CSPICE_INC_DIR must point to the folder in CSPICE_ROOT where SpiceUsr.h is located
CSPICE_LIB_DIR = path.join(CSPICE_ROOT, "lib")
CSPICE_INC_DIR = path.join(CSPICE_ROOT, "include")
CSPICE_LIB = "cspice.lib" if host_os == "Windows" else "cspice.a"

ffibuilder = FFI()

ffibuilder.cdef(
    """
    void spkez_c (int targ, double et, const char * ref, const char * abcorr, int obs,
    double starg[6], double * lt);
    void spkezp_c (int targ, double et, const char * ref, const char * abcorr, int obs,
    double ptarg[3], double * lt);
    void spkezr_c (const char * targ, double et, const char * ref, const char * abcorr,
    const char * obs, double starg[6], double * lt);
    void spkgeo_c (int targ, double et, const char * ref, int obs, double state[6], double * lt);
    void spkgps_c (int targ, double et, const char * ref, int obs, double pos[3], double * lt);
    void spkpos_c (const char * targ, double et, const char * ref, const char * abcorr,
    const char * obs, double ptarg[3], double * lt);
    void furnsh_c (const char * file);
"""
)

ffibuilder.set_source(
    "cffi_wrapper_static",
    """
    #include <stdio.h>
    #include "SpiceUsr.h"
""",
    libraries=["m"],
    extra_objects=[path.join(CSPICE_LIB_DIR, CSPICE_LIB)],
    library_dirs=[CSPICE_LIB_DIR],
    include_dirs=[CSPICE_INC_DIR],
)

if __name__ == "__main__":
    ffibuilder.compile(verbose=True)
