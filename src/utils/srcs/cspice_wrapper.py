# pylint: disable = fixme
"""
This file contains the sources to build a Python wrapper to the JPL's CSPICE Toolkit.
The CFFI (C Foreign Function Interface for Python) package is used to compile a shared object that
exposes the C routines implemented in CSPICE to Python.

"""

# TODO: make it work on Windows. To compile the wrapper, simply run the script.
# TODO: issues with pxform_c and Numba, nested array???

import sysconfig
import os
from cffi import FFI

# the implemented wrapper is dynamically linked to the libcspice.so object automatically installed
# together with spiceypy. Here we specify the folder in which the shared object is placed as well
# as the directory containing the C header files.
# On Anaconda Python, those object are found in the following folders:
# libcspice.so: anaconda3/envs/mdo/lib/libcspice.so
# C header files: anaconda3/envs/mdo/include/cspice

if os.name == 'nt':
    LIB_DIR = os.path.join(sysconfig.get_config_var('srcdir'), 'Library', 'bin')
    INC_DIR = os.path.join(sysconfig.get_config_var('srcdir'), 'Library', 'include')
else:
    LIB_DIR = sysconfig.get_config_var('LIBDIR')
    INC_DIR = sysconfig.get_config_var('INCLUDEDIR')

ffibuilder = FFI()

# The compiled Python wrapper is not exhaustive and exposes only a very limited number of CSPICE
# functions to Python. Here we include the signature of all CSPICE functions we are interested in.
# Additional routines can be added simply copy-pasting their signature from the CSPICE
# documentation available at (https://naif.jpl.nasa.gov/pub/naif/toolkit_docs/C/index.html).
# Note: the custom C types declared in the documentation must be replaced with the corresponding
# default types defined in the C programming language. For example:
# SpiceInt --> int
# SpiceDouble --> double
# SpiceDouble * --> double *
# ConstSpiceChar * --> const char *
ffibuilder.cdef("""
    void spkez_c (int targ, double et, const char * ref, const char * abcorr, int obs,
    double starg[6], double * lt);

    void spkezp_c (int targ, double et, const char * ref, const char * abcorr, int obs,
    double ptarg[3], double * lt);

    void spkezr_c (const char * targ, double et, const char * ref, const char * abcorr,
    const char * obs, double starg[6], double * lt);

    void spkgeo_c (int targ, double et, const char * ref, int obs, double state[6], double * lt);

    void spkgps_c (int targ, double et, const char * ref, int obs, double pos[3], double * lt);

    void spkpos_c (const char * targ, double et, const char * ref, const char * abcorr,
    const char * obs, double ptarg[3], double * lt);

    void pxform_c (const char * from, const char * to, double et, double rotate[3][3]);

""")

# Here we define the name of our CSPICE wrapper. By default, the compiler will then append
# .cpython-37m-x86_64-linux-gnu.so (on Linux) to the former.
# We then add a small snippet of C code to tell the compiler which header files must be included.
# In this case we need the standard C libraries <stdio.h> and the CSPICE top-level header files
# "SpiceUsr.h". Finally, we tell CFFI to build the wrapper linking the libcspice.so shared object
# and the C math library libm required by CSPICE.
ffibuilder.set_source("cspice_wrapper",
                      """
    #include <stdio.h>
    #include "SpiceUsr.h"
""",
                      libraries=['cspice', 'm'],
                      extra_link_args=['-Wl,--rpath=' + LIB_DIR],
                      library_dirs=[LIB_DIR],
                      include_dirs=[INC_DIR + '/cspice'],
                      )

if __name__ == "__main__":
    ffibuilder.compile(verbose=True, tmpdir='../libs')
