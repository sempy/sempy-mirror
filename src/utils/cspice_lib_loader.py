"""
Utilities for loading and exposing to Python the CSPICE dynamic link library.

The module is based on the Python built-in library `ctypes` that provides C compatible data types
and allows calling functions in DLLs or shared libraries.

"""

from os import path
from platform import system
from sysconfig import get_config_var
from ctypes import CDLL, POINTER, c_int8, c_int32, c_double
import numpy as np

host_os = system()  # operating system of the host machine
if host_os == 'Windows':
    CSPICE_LIB_NAME = 'cspice.dll'
    CSPICE_LIB_DIR = path.join(get_config_var('srcdir'), 'Library', 'bin')
else:  # Unix-based systems
    CSPICE_LIB_NAME = 'libcspice.so'
    CSPICE_LIB_DIR = get_config_var('LIBDIR')


def frame_encoder(frame):
    """Encodes one or multiple reference frame names using the ASCII standard.

    In order to be passed to the CSPICE routines through Just-in-Time (JIT) compiled functions,
    `frame` (a string or an iterable of strings) must be encoded as a numpy array of 8-bit integers
    containing the ASCII codes for all chars in `frame` plus the terminator character ``\0`` as
    defined in the C programming language.

    Parameters
    ----------
    frame : str or iterable
        Reference frame name or names to be encoded. Must be valid SPICE names.

    Returns
    -------
    enc_frame : ndarray
        Encoded frame name or names.

    """

    if isinstance(frame, str):  # single frame name to encode
        return np.array([ord(c) for c in frame + '\0'], dtype=np.int8)
    if isinstance(frame, (tuple, list)):  # multiple frame names to encode
        nb_chars = [len(f) for f in frame]  # number of characters for each frame name
        nb_cols = max(nb_chars) + 1  # number of columns in the output array
        padded_frame = [f + '\0' * (nb_cols - nb_chars[i]) for i, f in enumerate(frame)]
        encoded_frame = np.asarray([ord(c) for c in ''.join(padded_frame)], dtype=np.int8)
        return encoded_frame.reshape(len(frame), max(nb_chars) + 1)
    raise Exception('frame must be a string or a tuple of strings representing valid frame names')


libcspice = CDLL(path.join(CSPICE_LIB_DIR, CSPICE_LIB_NAME))  # CSPICE DLL
c_double_p = POINTER(c_double)  # data type for C pointers to arrays of doubles

spkgps = libcspice.spkgps_c  # spkgps_c function provided by CSPICE
spkgps.argtypes = [c_int32, c_double, POINTER(c_int8), c_int32, c_double_p, c_double_p]

spkgeo = libcspice.spkgeo_c  # spkgeo_c function provided by CSPICE
spkgeo.argtypes = [c_int32, c_double, POINTER(c_int8), c_int32, c_double_p, c_double_p]

pxform = libcspice.pxform_c  # pxform_c function provided by CSPICE
pxform.argtypes = [POINTER(c_int8), POINTER(c_int8), c_double, c_double_p]
