"""
Created on Wed Aug 19 10:50:19 2019

This module contains functions to load abacuses which are .mat files with data to perform orbit
interpolation.

"""
import os.path
import scipy.io as sio

# Local pylint disabled commands
# pylint: disable=invalid-name

# dirname_root = os.path.abspath('..')
dirname_root = os.path.dirname(__file__)


def abacus_mat(abacus_name):
    """This function can load Halo orbits abacuses in .mat format."""

    abacuses = '/abacuses/'
    dirname = dirname_root + abacuses
    filename = os.path.join(dirname, abacus_name)
    loaded_abacus = sio.loadmat(filename)
    abacus_dictionary = sorted(loaded_abacus.keys())[-1]
    abacus = loaded_abacus[abacus_dictionary]
    matrix = abacus['matrix'][0][0]
    A_z_limit = abacus['Azlimit'][0][0][0][0]
    C_jac_limit = abacus['Cjaclimit'][0][0][0]
    # print(filename)
    # The result is a dictionary, one key/value pair for each variable:
    # print(sorted(loaded_abacus.keys()))
    # By default SciPy reads MATLAB structs as structured NumPy arrays where the dtype fields
    # are of type object and the names correspond to the MATLAB struct field names.
    # print(abacus.dtype)
    # print(matrix.shape)
    A_z = matrix[:, 6]
    C_jac = matrix[:, 7]
    state_V_aba = matrix[:, 0:6]

    return A_z_limit, C_jac_limit, A_z, C_jac, state_V_aba
