# -*- coding: utf-8 -*-
"""
Created on Thu Jun 20 10:50:13 2019

"""


def decomment(csvfile):
    """ Function to ignore comments within a csv file"""
    for row in csvfile:
        raw = row.split('#')[0].strip()
        if raw:
            yield raw
