# pylint: disable = import-error, no-member
"""
Utility functions to convert states, times and epochs from numpy arrays and Python strings into
Orekit objects.

"""

import os
import numpy as np
import spiceypy as sp
import orekit

from orekit.pyhelpers import setup_orekit_curdir
from org.orekit.time import AbsoluteDate
from org.orekit.time import TimeScalesFactory
from org.orekit.propagation import SpacecraftState
from org.hipparchus.geometry.euclidean.threed import Vector3D
from org.orekit.utils import AbsolutePVCoordinates, PVCoordinates

from src.init.load_kernels import load_kernels

load_kernels()

vm = orekit.initVM()
DATA_DIR = os.path.join(os.path.split(os.path.split(os.path.dirname(__file__))[0])[0],
                        'examples', 'validation', 'data')
setup_orekit_curdir(os.path.join(DATA_DIR, 'orekit-data.zip'))
MONTHS = ("JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC")


def str2abs_date(str_date):
    """AbsoluteDate object from UTC date in calendar format. """
    year = int(str_date[0:4])
    month = MONTHS.index(str_date[5:8]) + 1
    day = int(str_date[9:11])
    hours = int(str_date[12:14])
    minutes = int(str_date[15:17])
    seconds = float(str_date[18:])
    date = AbsoluteDate(year, month, day, hours, minutes, seconds, TimeScalesFactory.getUTC())
    return date


def et2abs_date(eph_time):
    """AbsoluteDate object from ephemeris time. """
    return str2abs_date(sp.et2utc(eph_time, 'C', 16))


def abs_date2str(date):
    """UTC date in calendar format from AbsoluteDate object. """
    str_date = date.toString()
    month = MONTHS[int(str_date[5:7]) - 1]
    return ' '.join([str_date[0:4], month, str_date[8:10], str_date[11:]])


def abs_date2et(date):
    """Ephemeris time from AbsoluteDate object. """
    return sp.str2et(abs_date2str(date))


def ndarray2pv_coords(state):
    """PVCoordinates from 6-dim vector `state`. """
    state_c = state.copy() * 1e3
    pos = Vector3D(float(state_c[0]), float(state_c[1]), float(state_c[2]))
    vel = Vector3D(float(state_c[3]), float(state_c[4]), float(state_c[5]))
    return PVCoordinates(pos, vel)


def pv_coords2ndarray(pvc):
    """6-dim vector from PVCoordinates. """
    pos = pvc.getPosition()
    vel = pvc.getVelocity()
    return np.array([pos.getX(), pos.getY(), pos.getZ(),
                     vel.getX(), vel.getY(), vel.getZ()]) * 1e-3


def ndarray_str2sc_state(state, str_date, ref):
    """SpacecraftState from 6-dim vector `state` and UTC date in calendar format `str_date`. """
    return SpacecraftState(AbsolutePVCoordinates(ref, str2abs_date(str_date),
                                                 ndarray2pv_coords(state)))


def ndarray_et2sc_state(state, eph_time, ref):
    """SpacecraftState from 6-dim vector `state` and ephemeris time `eph_time`. """
    return SpacecraftState(AbsolutePVCoordinates(ref, et2abs_date(eph_time),
                                                 ndarray2pv_coords(state)))


def sc_state2ndarray_str(scs, ref):
    """6-dim vector and UTC date in calendar format from SpacecraftState. """
    state = pv_coords2ndarray(scs.getPVCoordinates(ref))
    return state, abs_date2str(scs.getDate())


def sc_state2ndarray_et(scs, ref):
    """6-dim vector and ephemeris time from SpacecraftState. """
    state = pv_coords2ndarray(scs.getPVCoordinates(ref))
    return state, abs_date2et(scs.getDate())
