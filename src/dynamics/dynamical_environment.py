from typing import Iterable, Union

from src.coc.frames import Frame
from src.init.primary import Primary

from src.dynamics.perturbations import Perturbation, BodyFixedPerturbation

class DynamicalEnvironment:
    """Class for a dynamical environment."""
    kind = None

    def __init__(self, bodies: Iterable[Primary],
                 frame: Frame,
                 perturbations: Iterable[Union[Perturbation, BodyFixedPerturbation]] = None,
                 adim_t=1., adim_l=1.):

        """Initialize the environment."""

        # Check that all bodies are Primary
        for body in bodies:
            if not isinstance(body, Primary):
                raise ValueError(f'Body {body} must be a Primary, not a {type(body)}.')

        self._bodies = self._sort_bodies(tuple(bodies))

        body_names = [body.name for body in self.bodies]
        if len(body_names) != len(set(body_names)):
            raise ValueError('Bodies list must have unique items!')

        # Check if both the barycenters and the bodies are in the list of bodies
        if Primary.EARTH_AND_MOON in self.bodies:
            if Primary.EARTH in self.bodies or Primary.MOON in self.bodies:
                raise ValueError('EARTH_AND_MOON cannot be used with EARTH or MOON.')

        self._frame = frame
        self._adim_t = adim_t
        self._adim_l = adim_l
        self._ode = None

        """I need to check that the bodies in the perturbations are in the list of bodies"""
        if perturbations is not None:
            for perturbation in perturbations:
                if perturbation.name == 'SRP' and 'SUN' not in [body.name for body in self.bodies]:
                    raise ValueError('SRP perturbation requires Sun in the list of bodies.')
                elif perturbation.kind == 'body_fixed' and perturbation.body not in self.bodies:
                    raise ValueError('Body fixed perturbation requires the body to be in the list of bodies.')
                else:
                    raise ValueError('Perturbation not recognised.')

        self._perturbations = perturbations

    @property
    def frame(self):
        return self._frame

    @property
    def adim_t(self):
        return self._adim_t

    @property
    def adim_l(self):
        return self._adim_l

    @property
    def adim_v(self):
        return self.adim_l / self.adim_t

    @property
    def bodies(self):
        return self._bodies

    @property
    def name(self):
        return '+'.join([body.name for body in self._bodies])

    @staticmethod
    def _sort_bodies(bodies, key='M'):
        """Sort bodies by mass."""
        return tuple(sorted(bodies, key=lambda body: getattr(body, key), reverse=True))

    def ode(self, t, s, **kwargs):
        """Equations of motion."""
        pass

    def dimensionalize_state(self, state):
        """Dimensionalize state."""
        if len(state) > 6:
            raise ValueError('State must have 6 elements.')
        state = state.copy()
        state[0:3] *= self.adim_l
        state[3:6] *= self.adim_v
        return state

    def adimensionalize_state(self, state):
        """Adimensionalize state."""
        if len(state) > 6:
            raise ValueError('State must have 6 elements.')
        state = state.copy()
        state[0:3] /= self.adim_l
        state[3:6] /= self.adim_v
        return state

    # @staticmethod
    # def _generate_perturbed_eom(eom, f_pert):
    #     """Add a perturbation to the equations of motion."""
    #     def perturbed_eom(t, s, *args, **kwargs):
    #         f_p_state = np.concatenate([np.array([0, 0, 0]),  f_pert(t, s, *args, **kwargs)])
    #         return eom(t, s, *args, **kwargs) + f_p_state
    #
    #     return perturbed_eom
    #
    # @staticmethod
    # def _generate_perturbed_variational_matrix(variational_matrix, A_pert):
    #     """Add a perturbation to the variational matrix."""
    #     A_p_unperturbed = variational_matrix
    #     def perturbed_variational_matrix(t, s, *args, **kwargs):
    #         return A_p_unperturbed(t, s, *args, **kwargs) + A_pert(t, s, *args, **kwargs)
    #     return perturbed_variational_matrix

