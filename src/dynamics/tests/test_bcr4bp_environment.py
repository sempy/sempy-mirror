# -*- coding: utf-8 -*-

import unittest
import numpy as np
import os.path
import spiceypy as sp
from scipy.io import loadmat

from src.init.primary import Primary
from src.dynamics.cr3bp_environment import CR3BP
from src.dynamics.bcr4bp_environment import BCR4BP
from src.init.load_kernels import load_kernels

dirname = os.path.dirname(__file__)


class TestBCR4BP(unittest.TestCase):

    def test_initialisation(self):
        """This will test the results of the cr3bp module for the Sun-Earth system."""

        cr3bp = CR3BP(Primary.EARTH, Primary.MOON)
        bcr4bp = BCR4BP(Primary.EARTH, Primary.MOON, np.pi/2)

        assert bcr4bp.cr3bp.name == cr3bp.name
        assert bcr4bp.start_sun_phase == np.pi/2

        bcr4bp_2 = BCR4BP(Primary.EARTH, Primary.MOON, np.pi/2, sun_inc=np.pi/4, sun_raan=np.pi/4)
        assert bcr4bp_2.start_sun_phase == np.pi/2
        assert bcr4bp_2.sun_info.inc == np.pi/4
        assert bcr4bp_2.sun_info.raan == np.pi/4

    def test_set_to_date(self):
        bcr4bp = BCR4BP(Primary.EARTH, Primary.MOON, np.pi/2)
        bcr4bp.set_sun_to_date('2036 AUG 07 02:52:00')  # Total lunar eclipse, phase should be 180 deg
        np.testing.assert_almost_equal(bcr4bp.start_sun_phase, np.pi, decimal=3)

    def test_error(self):
        msg = "The Sun's inclination must be non-zero if its RAAN is non-zero."
        with self.assertRaisesRegex(ValueError, msg):
            BCR4BP(Primary.EARTH, Primary.MOON, np.pi/2, sun_raan=np.pi/4)

    def test_continuation_parameter(self):
        """When the continuation parameter is 0, the BCR4BP should be the same as the CR3BP."""
        bcr_0 = BCR4BP(Primary.EARTH, Primary.MOON, continuation_param=0.)
        x0 = np.array([1., 0., 0., 0., 1., 0.])
        u_bar_crt = bcr_0.cr3bp.get_u_bar(x0)
        u_bar_bcr_0 = bcr_0.get_u_bar(0, x0)
        u_bar_dot_crt = bcr_0.cr3bp.get_u_bar_first_partials(x0)
        u_bar_dot_bcr_0 = bcr_0.get_u_bar_first_partials(0, x0)
        u_bar_dot_dot_crt = bcr_0.cr3bp.get_u_bar_second_partials(x0)
        u_bar_dot_dot_bcr_0 = bcr_0.get_u_bar_second_partials(0, x0)
        jac_crt = bcr_0.cr3bp.get_jacobi(x0)
        ham_bcr_0 = bcr_0.get_hamiltonian(0, x0)

        np.testing.assert_almost_equal(u_bar_crt, u_bar_bcr_0, decimal=8)
        np.testing.assert_almost_equal(u_bar_dot_crt, u_bar_dot_bcr_0, decimal=8)
        np.testing.assert_almost_equal(u_bar_dot_dot_crt, u_bar_dot_dot_bcr_0, decimal=8)
        np.testing.assert_almost_equal(jac_crt, ham_bcr_0, decimal=8)

        # Change the continuation_parameter and check that the BCR4BP is different
        bcr_0.continuation_parameter = 1.
        u_bar_bcr_1 = bcr_0.get_u_bar(0, x0)
        u_bar_dot_bcr_1 = bcr_0.get_u_bar_first_partials(0, x0)
        u_bar_dot_dot_bcr_1 = bcr_0.get_u_bar_second_partials(0, x0)
        ham_bcr_1 = bcr_0.get_hamiltonian(0, x0)
        assert not np.array_equal(u_bar_bcr_0, u_bar_bcr_1)
        assert not np.array_equal(u_bar_dot_bcr_0, u_bar_dot_bcr_1)
        assert not np.array_equal(u_bar_dot_dot_bcr_0, u_bar_dot_dot_bcr_1)
        assert not np.array_equal(ham_bcr_0, ham_bcr_1)

    def test_change_start_phase(self):
        bc = BCR4BP(Primary.EARTH, Primary.MOON, np.pi/2)
        x0 = np.array([1., 0., 0., 0., 1., 0.])
        u_bar_0 = bc.get_u_bar(0, x0)
        bc.start_sun_phase = np.pi/4
        assert bc.start_sun_phase == np.pi/4
        u_bar_1 = bc.get_u_bar(0, x0)
        assert not np.array_equal(u_bar_0, u_bar_1)

    def test_dynamics(self):
        # The following data was generated using SEMAT
        filename = os.path.join(dirname, 'data', f'bcr4bp_derivatives.mat')
        loaded_mat = loadmat(filename)
        sun_phase0 = loaded_mat['sun_phase0'][0][0]
        m_s = loaded_mat['ms'][0][0]
        a_s = loaded_mat['as'][0][0]
        omega_sun = -loaded_mat['omegaS'][0][0]
        bc = BCR4BP(Primary.EARTH, Primary.MOON, sun_phase0)

        # Overwrite the default values (Needed as SEMat uses different values)
        bc._sun_info = bc._sun_info._replace(mass=m_s)
        bc._sun_info = bc._sun_info._replace(radius=a_s)
        bc._sun_info = bc._sun_info._replace(ang_vel=omega_sun)

        # Retrieve initial time and state
        t0 = loaded_mat['t0'][0][0]
        x0 = np.array(loaded_mat['x0'][0])
        x0_stm = np.array(loaded_mat['x0_stm'][0])

        # Transform in floating point
        t0 = t0.astype(float)
        x0 = x0.astype(float)
        x0_stm = x0_stm.astype(float)

        # Retrieve the derivatives
        x_dot = loaded_mat['xdot'][:, 0]
        x_dot_stm = loaded_mat['xdot_stm'][:, 0]

        # Compute the derivatives using the BCR4BP
        x_dot_bcr = bc.ode(t0, x0)

        # Compute the derivatives using the BCR4BP with STM
        x_dot_stm_bcr = bc.ode(t0, x0_stm, with_stm=True)

        # Compare the results
        np.testing.assert_almost_equal(x_dot, x_dot_bcr, decimal=8)
        np.testing.assert_almost_equal(x_dot_stm, x_dot_stm_bcr, decimal=8)

if __name__ == '__main__':
    load_kernels()
    unittest.main()
    sp.kclear()
