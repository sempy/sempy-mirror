"""
Test dynamics in the ephemeris model.

"""

import unittest
import numpy as np
import os
from scipy.io import loadmat

from src.init.load_kernels import load_kernels
from src.init.ephemeris import Ephemeris
from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.dynamics.ephemeris_dynamics import eqm_6_ephemeris, eqm_42_ephemeris, eqm_48_ephemeris
from src.init.constants import MOON_OMEGA_MEAN, MOON_SMA

load_kernels()  # load kernel pool

# load data obtained in SEMAT
DIRNAME = os.path.dirname(__file__)
DATA_EQM_6_42 = loadmat(os.path.join(DIRNAME, 'data', 'test_ephemeris_dynamics.mat'),
                        squeeze_me=True, struct_as_record=False)

# define the ephemeris model, reference frame and adimensionalization quantities
T_C = 1.0 / MOON_OMEGA_MEAN
L_CI = 1.0 / MOON_SMA
EPHEMERIS = Ephemeris((Primary.EARTH, Primary.MOON, Primary.SUN))
GM_ADIM = EPHEMERIS.gm_dim * T_C ** 2 * MOON_SMA ** -3
REF = np.array([ord(c) for c in 'J2000\0']).astype(np.int8)


class TestEphemerisDynamics(unittest.TestCase):

    def test_eqm_6_ephemeris(self):
        ydot6_adim = eqm_6_ephemeris(DATA_EQM_6_42['t0_adim'], DATA_EQM_6_42['y0_adim'], GM_ADIM,
                                     EPHEMERIS.naif_ids, REF, T_C, L_CI)
        ydot6_dim = eqm_6_ephemeris(DATA_EQM_6_42['t0_dim'], DATA_EQM_6_42['y0_dim'],
                                    EPHEMERIS.gm_dim, EPHEMERIS.naif_ids, REF, 1.0, 1.0)
        np.testing.assert_allclose(ydot6_adim, DATA_EQM_6_42['ydot6_adim'], rtol=0.0, atol=1e-10)
        np.testing.assert_allclose(ydot6_dim, DATA_EQM_6_42['ydot6_dim'], rtol=0.0, atol=1e-10)

    def test_eqm_42_ephemeris(self):
        y0_42_adim = np.concatenate((DATA_EQM_6_42['y0_adim'], np.eye(6).ravel()))
        y0_42_dim = np.concatenate((DATA_EQM_6_42['y0_dim'], np.eye(6).ravel()))
        ydot42_adim = eqm_42_ephemeris(DATA_EQM_6_42['t0_adim'], y0_42_adim, GM_ADIM,
                                       EPHEMERIS.naif_ids, REF, T_C, L_CI)
        ydot42_dim = eqm_42_ephemeris(DATA_EQM_6_42['t0_dim'], y0_42_dim, EPHEMERIS.gm_dim,
                                      EPHEMERIS.naif_ids, REF, 1.0, 1.0)
        np.testing.assert_allclose(ydot42_adim, DATA_EQM_6_42['ydot42_adim'], rtol=0.0, atol=1e-10)
        np.testing.assert_allclose(ydot42_dim, DATA_EQM_6_42['ydot42_dim'], rtol=0.0, atol=1e-10)

    def test_eqm_48_ephemeris(self):

        data = loadmat(os.path.join(DIRNAME, 'data', 'test_eqm48.mat'), struct_as_record=False,
                       squeeze_me=True)
        data_adim = data['eqm48']
        data_dim = data['eqm48_dim']

        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
        t_c = cr3bp.T / 2.0 / np.pi
        l_ci = 1.0 / cr3bp.L
        gm_adim = EPHEMERIS.gm_dim * t_c ** 2 * cr3bp.L ** -3

        for i, t in enumerate(data_adim.t):
            state_dot = eqm_48_ephemeris(t, np.ascontiguousarray(data_adim.y[i, :]), gm_adim,
                                         EPHEMERIS.naif_ids, REF, t_c, l_ci)
            np.testing.assert_allclose(state_dot, data_adim.ydot[i, :], rtol=0.0, atol=1e-12)
        for i, t in enumerate(data_dim.t):
            state_dot = eqm_48_ephemeris(t, np.ascontiguousarray(data_dim.y[i, :]),
                                         EPHEMERIS.gm_dim, EPHEMERIS.naif_ids, REF, 1.0, 1.0)
            np.testing.assert_allclose(state_dot, data_dim.ydot[i, :], rtol=0.0, atol=1e-12)


if __name__ == '__main__':
    unittest.main()
