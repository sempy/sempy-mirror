# -*- coding: utf-8 -*-

import unittest
import numpy as np
import os.path
import spiceypy as sp

from src.init.primary import Primary
from src.init.load_kernels import load_kernels
from src.dynamics.r2bp_environment import R2BP
from src.dynamics.nbp_environment import NBP

dirname = os.path.dirname(__file__)

class TestR2BP(unittest.TestCase):

    def test_r2bp_results(self):
        """This will test the results of the cr3bp module for the Sun-Earth system."""

        r2bp = R2BP(Primary.EARTH)

        # m1
        self.assertEqual(r2bp.bodies[0].name, Primary.EARTH.name)

        # cr3bp params
        self.assertAlmostEqual(r2bp.mu, Primary.EARTH.GM)

    def test_ode(self):
        nbp = NBP([Primary.EARTH,])
        r2bp = R2BP(Primary.EARTH)

        x0 = np.array([10000., 0., 0., 0., 8., 0.])
        x0_stm = np.concatenate((x0, np.eye(6).flatten()))

        xdot = r2bp.ode(0, x0)
        xstmdot = r2bp.ode(0, x0_stm, with_stm=True)

        xdot_nbp = nbp.ode(0, x0)
        xstmdot_nbp = nbp.ode(0, x0_stm, with_stm=True)

        self.assertTrue(np.allclose(xdot, xdot_nbp))
        self.assertTrue(np.allclose(xstmdot, xstmdot_nbp))


if __name__ == '__main__':
    load_kernels()
    unittest.main()
    sp.kclear()