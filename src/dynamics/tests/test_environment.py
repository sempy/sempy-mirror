import unittest
from src.dynamics.nbp_environment import NBP
from src.init.primary import Primary


class TestEnvironment(unittest.TestCase):
	def test_warnings(self):
		bodies_wrong = [Primary.SUN, Primary.MOON, Primary.EARTH, Primary.JUPITER, Primary.SUN]
		with self.assertRaisesRegex(ValueError, 'Bodies list must have unique items!'):
			NBP(bodies_wrong)

		bodies_wrong_2 = [Primary.SUN, Primary.MOON, Primary.EARTH, Primary.JUPITER, Primary.EARTH_AND_MOON]
		with self.assertRaisesRegex(ValueError, 'EARTH_AND_MOON cannot be used with EARTH or MOON.'):
			NBP(bodies_wrong_2)

		bodies_wrong_3 = [Primary.SUN, 'Moon', Primary.EARTH]
		with self.assertRaisesRegex(ValueError, "Body Moon must be a Primary, not a <class 'str'>."):
			NBP(bodies_wrong_3)
