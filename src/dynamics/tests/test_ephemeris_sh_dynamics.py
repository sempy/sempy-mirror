"""
Test dynamics in the ephemeris model with non-uniform gravity field model.

"""

import unittest
import numpy as np
import os
from scipy.io import loadmat
from spiceypy import furnsh

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.init.ephemeris import Ephemeris
from src.init.load_kernels import load_kernels, KERNELS_ROOT
from src.dynamics.ephemeris_sh_dynamics import eqm_6_sh_ephemeris, eqm_42_sh_ephemeris

dirname = os.path.dirname(__file__)

load_kernels()
furnsh(os.path.join(KERNELS_ROOT, 'pck/moon_pa_de421_1900-2050.bpc'))
furnsh(os.path.join(KERNELS_ROOT, 'fk/moon_080317.tf'))

data = loadmat(os.path.join(dirname, 'data', 'test_sh_dyna_prop.mat'),
               squeeze_me=True, struct_as_record=False)['out']
cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
eph = Ephemeris(Primary.MOON, moon=('GRGM0050', 50, 50))
l_c = cr3bp.L
t_c = cr3bp.T / 2.0 / np.pi


class TestEphemerisSHDynamics(unittest.TestCase):

    def test_eqm_6_sh_dynamics(self):

        ref, gm_adim = eph.get_integration_params(t_c=t_c, l_c=l_c)
        r_adim, bf_ref, z_nm, nm_max = eph.get_sh_integration_params(l_c=l_c)
        state = np.ascontiguousarray(data.y0[:, 0:6])
        state_dot = np.zeros(state.shape)

        for i in range(data.t0.size):
            state_dot[i, :] = eqm_6_sh_ephemeris(data.t0[i], state[i], gm_adim, eph.naif_ids, ref,
                                                 t_c, 1.0 / l_c, r_adim, bf_ref, z_nm, nm_max)

        np.testing.assert_allclose(state_dot, data.y0dot[:, 0:6], rtol=0.0, atol=1e-13)

    def test_eqm_42_sh_dynamics(self):

        ref, gm_adim = eph.get_integration_params(t_c=t_c, l_c=l_c)
        r_adim, bf_ref, z_nm, nm_max = eph.get_sh_integration_params(l_c=l_c)
        state = np.ascontiguousarray(data.y0)
        state_dot = np.zeros(state.shape)

        for i in range(data.t0.size):
            state_dot[i, :] = eqm_42_sh_ephemeris(data.t0[i], state[i], gm_adim, eph.naif_ids, ref,
                                                  t_c, 1.0 / l_c, r_adim, bf_ref, z_nm, nm_max)

        np.testing.assert_allclose(state_dot, data.y0dot, rtol=0.0, atol=1e-11)


if __name__ == '__main__':
    unittest.main()
