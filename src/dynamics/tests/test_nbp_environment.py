import unittest
import numpy as np
import os
from scipy.io import loadmat
import spiceypy as sp
import datetime

from src.init.load_kernels import load_kernels
from src.dynamics.nbp_environment import NBP
from src.dynamics.cr3bp_environment import CR3BP
from src.init.primary import Primary
from src.init.constants import MOON_OMEGA_MEAN, MOON_SMA

# load data obtained in SEMAT
DIRNAME = os.path.dirname(__file__)
DATA_EQM_6_42 = loadmat(os.path.join(DIRNAME, 'data', 'test_ephemeris_dynamics.mat'),
                        squeeze_me=True, struct_as_record=False)

# define the ephemeris model, reference frame and adimensionalization quantities
T_C = 1.0 / MOON_OMEGA_MEAN
L_C = MOON_SMA

BODIES = [Primary.EARTH, Primary.MOON, Primary.SUN]
START_ET = 0
START_EPOCH = sp.et2datetime(START_ET)

class TestNBP(unittest.TestCase):

    def test_attributes(self):
        nbp = NBP(BODIES)
        self.assertEqual(len(nbp.bodies), 3)
        for body in BODIES:
            self.assertTrue(body in nbp.bodies)

        self.assertEqual(nbp.integration_center, 'EARTH')  # integration center is the first body in the list
        self.assertEqual(nbp.start_date, START_EPOCH)
        self.assertAlmostEqual(nbp.start_et, START_ET, places=6)

    def test_integration_center(self):
        nbp = NBP(BODIES, center='SUN')
        self.assertEqual(nbp.integration_center, 'SUN')
        nbp.integration_center = 'MOON'
        self.assertEqual(nbp.integration_center, 'MOON')

    def test_initial_epoch(self):
        nbp = NBP(BODIES, start_date=START_EPOCH)
        self.assertEqual(nbp.start_date, START_EPOCH)
        nbp.start_date = START_EPOCH + datetime.timedelta(days=1)
        self.assertEqual(nbp.start_date, START_EPOCH + datetime.timedelta(days=1))
        nbp = NBP(BODIES, start_date='2000-01-01 12:00:00 TDB')
        self.assertAlmostEqual(nbp.start_et, START_ET, places=6)
        nbp.start_et = START_ET + 3600
        self.assertAlmostEqual(nbp.start_et, START_ET + 3600, places=6)

    def test_dynamics6(self):
        nbp_dim = NBP(BODIES)
        nbp_adim = NBP(BODIES, adim_t=T_C, adim_l=L_C)
        ydot6_adim = nbp_adim.ode(DATA_EQM_6_42['t0_adim'], DATA_EQM_6_42['y0_adim'])
        ydot6_dim = nbp_dim.ode(DATA_EQM_6_42['t0_dim'], DATA_EQM_6_42['y0_dim'])
        np.testing.assert_allclose(ydot6_adim, DATA_EQM_6_42['ydot6_adim'], rtol=0.0, atol=1e-10)
        np.testing.assert_allclose(ydot6_dim, DATA_EQM_6_42['ydot6_dim'], rtol=0.0, atol=1e-10)

    def test_eqm_42_ephemeris(self):
        nbp_dim = NBP(BODIES)
        nbp_adim = NBP(BODIES, adim_t=T_C, adim_l=L_C)
        y0_42_adim = np.concatenate((DATA_EQM_6_42['y0_adim'], np.eye(6).ravel()))
        y0_42_dim = np.concatenate((DATA_EQM_6_42['y0_dim'], np.eye(6).ravel()))
        ydot42_adim = nbp_adim.ode(DATA_EQM_6_42['t0_adim'], y0_42_adim, with_stm=True)
        ydot42_dim = nbp_dim.ode(DATA_EQM_6_42['t0_dim'], y0_42_dim, with_stm=True)
        np.testing.assert_allclose(ydot42_adim, DATA_EQM_6_42['ydot42_adim'], rtol=0.0, atol=1e-10)
        np.testing.assert_allclose(ydot42_dim, DATA_EQM_6_42['ydot42_dim'], rtol=0.0, atol=1e-10)

    def test_eqm_48_ephemeris(self):
        data = loadmat(os.path.join(DIRNAME, 'data', 'test_eqm48.mat'), struct_as_record=False,
                       squeeze_me=True)
        data_adim = data['eqm48']
        data_dim = data['eqm48_dim']
        t_adim = data_adim.t - data_adim.t[0]
        t_dim = data_dim.t - data_dim.t[0]

        crt = CR3BP(Primary.EARTH, Primary.MOON)
        crt._adim_t = 375699.7937499362  # Override the adimensionalization time
        et_0 = data_adim.t[0] * crt.adim_t
        date0 = sp.et2datetime(et_0)

        nbp_adim = NBP(BODIES, adim_t=crt.adim_t, adim_l=crt.adim_l, start_date=date0)
        nbp_dim = NBP(BODIES, start_date=date0)

        for i, t in enumerate(t_adim):
            state_dot = nbp_adim.ode(t, np.ascontiguousarray(data_adim.y[i, :]), with_stm=True,
                                     with_epoch_partials=True)
            np.testing.assert_allclose(state_dot, data_adim.ydot[i, :], rtol=0.0, atol=1e-9)
        for i, t in enumerate(t_dim):
            state_dot = nbp_dim.ode(t, np.ascontiguousarray(data_dim.y[i, :]), with_stm=True,
                                    with_epoch_partials=True)
            np.testing.assert_allclose(state_dot, data_dim.ydot[i, :], rtol=0.0, atol=1e-12)

if __name__ == '__main__':
    load_kernels()  # load kernel pool
    unittest.main()
    sp.kclear()