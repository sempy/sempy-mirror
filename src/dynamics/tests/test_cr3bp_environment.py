# -*- coding: utf-8 -*-

import unittest
import numpy as np
import scipy.io as sio
import os.path
from src.init.primary import Primary
from src.dynamics.cr3bp_environment import CR3BP

dirname = os.path.dirname(__file__)


class TestCR3BP(unittest.TestCase):

    def test_EM_cr3bp_results(self):
        self._test_cr3bp_results(system='EM')

    def test_SE_cr3bp_results(self):
        self._test_cr3bp_results(system='SE')

    def _test_cr3bp_results(self, system='EM'):
        """This will test the results of the cr3bp module for the Sun-Earth system."""

        # Initialize CR3BP
        if system == 'EM':
            cr3bp = CR3BP(Primary.EARTH, Primary.MOON)
        elif system == 'SE':
            cr3bp = CR3BP(Primary.SUN, Primary.EARTH)
        else:
            raise ValueError('Invalid system')

        # The following data was generated using SEMAT
        filename = os.path.join(dirname, 'data', f'cr3bp_{system}.mat')
        loaded_mat = sio.loadmat(filename)
        mat_dictionary = sorted(loaded_mat.keys())[-1]
        mat_values = loaded_mat[mat_dictionary]

        # m1
        m1_struct = mat_values['m1'][0][0][0][0]
        m1_name = m1_struct[0][0]
        m1_pos = m1_struct[7][0]

        # m2
        m2_struct = mat_values['m2'][0][0][0][0]
        m2_name = m2_struct[0][0]
        m2_pos = m2_struct[7][0]

        # cr3bp params
        mu = mat_values['mu'][0][0][0][0]
        L = mat_values['L'][0][0][0][0]
        T = mat_values['T'][0][0][0][0]
        name = mat_values['name'][0][0][0]

        # l1
        l1_struct = mat_values['l1'][0][0][0][0]
        l1_position = l1_struct[4][0]

        # l2
        l2_struct = mat_values['l2'][0][0][0][0]
        l2_position = l2_struct[4][0]

        # l3
        l3_struct = mat_values['l3'][0][0][0][0]
        l3_position = l3_struct[4][0]

        # l4
        l4_struct = mat_values['l4'][0][0][0][0]
        l4_position = l4_struct[2][0]

        # l5
        l5_struct = mat_values['l5'][0][0][0][0]
        l5_position = l5_struct[2][0]

        # m1
        self.assertEqual(cr3bp.bodies[0].name, m1_name)
        self.assertEqual(cr3bp.bodies[0].name, cr3bp.m1.name)
        self.assertAlmostEqual(cr3bp.m1_pos[0], m1_pos[0])
        self.assertAlmostEqual(cr3bp.m1_pos[1], m1_pos[1])
        self.assertAlmostEqual(cr3bp.m1_pos[2], m1_pos[2])

        # m2
        self.assertEqual(cr3bp.bodies[1].name, m2_name)
        self.assertEqual(cr3bp.bodies[1].name, cr3bp.m2.name)
        self.assertAlmostEqual(cr3bp.m2_pos[0], m2_pos[0])
        self.assertAlmostEqual(cr3bp.m2_pos[1], m2_pos[1])
        self.assertAlmostEqual(cr3bp.m2_pos[2], m2_pos[2])

        # cr3bp params
        self.assertAlmostEqual(cr3bp.mu, mu)
        self.assertAlmostEqual(cr3bp.adim_l, L)
        self.assertAlmostEqual(cr3bp.adim_t*2*np.pi, T)
        self.assertEqual(cr3bp.name, name)

        # l1
        self.assertAlmostEqual(cr3bp.libration_points.L1[0], l1_position[0])
        self.assertAlmostEqual(cr3bp.libration_points.L1[1], l1_position[1])
        self.assertAlmostEqual(cr3bp.libration_points.L1[2], l1_position[2])

        # l2
        self.assertAlmostEqual(cr3bp.libration_points.L2[0], l2_position[0])
        self.assertAlmostEqual(cr3bp.libration_points.L2[1], l2_position[1])
        self.assertAlmostEqual(cr3bp.libration_points.L2[2], l2_position[2])

        # l3
        self.assertAlmostEqual(cr3bp.libration_points.L3[0], l3_position[0])
        self.assertAlmostEqual(cr3bp.libration_points.L3[1], l3_position[1])
        self.assertAlmostEqual(cr3bp.libration_points.L3[2], l3_position[2])

        # l4
        self.assertAlmostEqual(cr3bp.libration_points.L4[0], l4_position[0])
        self.assertAlmostEqual(cr3bp.libration_points.L4[1], l4_position[1])
        self.assertAlmostEqual(cr3bp.libration_points.L4[2], l4_position[2])

        # l5
        self.assertAlmostEqual(cr3bp.libration_points.L5[0], l5_position[0])
        self.assertAlmostEqual(cr3bp.libration_points.L5[1], l5_position[1])
        self.assertAlmostEqual(cr3bp.libration_points.L5[2], l5_position[2])

    def test_dynamics6(self):
        # The following data for the system Earth-Moon, cr3bp_derivatives_6,
        # was generated using SEMAT
        cr3bp_derivatives_6 = (0, 0.186527612353890, 0, 0.015363614500942, 0, -0.135929695436756)

        # the test begins for the system Earth-Moon
        cr3bp = CR3BP(Primary.EARTH, Primary.MOON)

        x, y, z = 1.116756261235440, 0, 0.0222299747777274
        vx, vy, vz = 0, 0.186527612353890, 0
        init_cond = np.array([x, y, z, vx, vy, vz])
        dot_state = cr3bp.ode(0.0, init_cond)

        for i in range(5):
            self.assertAlmostEqual(dot_state[i], cr3bp_derivatives_6[i], places=6)

    def test_dynamics42(self):

        # The following data for the system Earth-Moon, cr3bp_derivatives_42,
        # was generated using the previous version of SEMpy (already validated)
        filename_in = os.path.join(dirname, 'data', 'cr3bp_state0_42.csv')
        filename_out = os.path.join(dirname, 'data', 'cr3bp_derivatives_42.csv')
        cr3bp_derivatives_42 = np.genfromtxt(filename_out, delimiter=',')
        cr3bp_state0_42 = np.genfromtxt(filename_in, delimiter=',')

        # the test begins
        cr3bp = CR3BP(Primary.EARTH, Primary.MOON)
        dot_state_stm = cr3bp.ode(0.0, cr3bp_state0_42, with_stm=True)

        # testing a tuple that is almost equal
        for i in range(42):
            self.assertAlmostEqual(dot_state_stm[i], cr3bp_derivatives_42[i], places=6)

if __name__ == '__main__':
    unittest.main()
