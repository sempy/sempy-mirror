# pylint: disable=duplicate-code
"""
Circular Restricted Three-Body problem environment.
"""

import numpy as np
from numba import jit
from dataclasses import dataclass
from typing import Iterable
import spiceypy as sp
from collections import namedtuple

from src.dynamics.perturbations import Perturbation

from src.dynamics.dynamical_environment import DynamicalEnvironment
from src.dynamics.cr3bp_environment import CR3BP
from src.coc.frames import SelectFrame
from src.init.primary import Primary

@jit('float64[::1](float64, float64, float64, float64, float64, float64)',
        nopython=True, cache=True, nogil=True)
def _sun_position(t, start_sun_phase, sun_radius, sun_inc, sun_ang_vel, sun_raan):
    sun_phase = -sun_ang_vel * t + start_sun_phase
    x_s = sun_radius * (np.cos(sun_phase - sun_raan) * np.cos(sun_raan) -
                        np.sin(sun_phase - sun_raan) * np.sin(sun_raan) * np.cos(sun_inc))
    y_s = sun_radius * (np.cos(sun_phase - sun_raan) * np.sin(sun_raan) +
                        np.sin(sun_phase - sun_raan) * np.cos(sun_raan) * np.cos(sun_inc))
    z_s = sun_radius * np.sin(sun_phase - sun_raan) * np.sin(sun_inc)
    return np.array([x_s, y_s, z_s])

@jit('float64(float64, float64[::1], float64, float64, float64, float64, float64, float64, float64, float64)',
        nopython=True, cache=True, nogil=True)
def _u_bar(t, state, mu, start_sun_phase, sun_mass, sun_radius, sun_inc, sun_ang_vel, sun_raan, cont_param):
    x, y, z = state[:3]
    p_S = _sun_position(t, start_sun_phase, sun_radius, sun_inc, sun_ang_vel, sun_raan)
    r_13_2 = (x + mu)**2 + y**2 + z**2  # r13**2
    r_23_2 = (x - 1.0 + mu)**2 + y**2 + z**2  # r23**2
    r_S3_2 = (x - p_S[0])**2 + (y - p_S[1])**2 + (z - p_S[2])**2  # rS3**2

    u_bar = 0.5 * (x**2 + y**2) + (1.0 - mu) / np.sqrt(r_13_2) + mu / np.sqrt(r_23_2) + \
            + 0.5 * mu * (1.0 - mu) +\
            cont_param * (sun_mass/np.sqrt(r_S3_2) -
                          sun_mass / sun_radius**3 * (x * p_S[0] + y * p_S[1] + z * p_S[2]))
    return -u_bar

@jit('float64[::1](float64, float64[::1], float64, float64, float64, float64, float64, float64,'
     'float64, float64)',
     nopython=True, cache=True, nogil=True)
def _u_bar_first_partials(t, state, mu, start_sun_phase, sun_mass, sun_radius, sun_inc, sun_ang_vel,
                          sun_raan, cont_param):
    """First partial derivatives of the augmented potential U_bar.

    This implementation of the cr4bp is taken from the Koon Lo Marsden Ross,
    The Sun in considered as a perturbation of the CR3BP
    The validation of this code was done thanks to the comparison with SEMat

    Parameters
    ----------
    t: float
        Integration time
    state : iterable
        Orbit's state.
    mu : float
        CR3BP mass parameter.
    start_sun_phase : float
        Initial angular displacement
    sun_mass : float
        Normalized Sun's mass
    sun_radius : float
        Normalized Sun's orbital radius
    sun_ang_vel: float
        Normalized Sun's angular velocity

    Returns
    -------
    u_bar1 : ndarray
        First partial derivatives of the augmented potential wrt (x, y, z).

    """
    x, y, z = state[:3]

    p_S = _sun_position(t, start_sun_phase, sun_radius, sun_inc, sun_ang_vel, sun_raan)
    
    r_13_2 = (x + mu)**2 + y**2 + z**2  # r13**2
    r_23_2 = (x - 1.0 + mu)**2 + y**2 + z**2 # r23**2
    r_S3_2 = (x - p_S[0])**2 + (y - p_S[1])**2 + (z - p_S[2])**2  # rS3**2
    
    r_1_i32 = r_13_2**(-1.5)
    r_2_i32 = r_23_2**(-1.5)
    r_S_i32 = r_S3_2**(-1.5)

    u_bar1 = np.empty(3)  # preallocate array to store the partials

    u_bar1[0] = mu * (x - 1.0 + mu) * r_2_i32 + \
                (1.0 - mu) * (x + mu) * r_1_i32 - x +\
                cont_param * (sun_mass * (x - p_S[0]) * r_S_i32 + sun_mass * p_S[0]/(sun_radius**3))
    u_bar1[1] = mu * y * r_2_i32 +\
                (1.0 - mu) * y * r_1_i32 - y+\
                cont_param * (sun_mass * (y - p_S[1]) * r_S_i32 + sun_mass * p_S[1]/(sun_radius**3))
    u_bar1[2] = mu * z * r_2_i32 +\
                (1.0 - mu) * z * r_1_i32 +\
                cont_param * (sun_mass * (z - p_S[2]) * r_S_i32 + sun_mass * p_S[2]/(sun_radius**3))

    return u_bar1

@jit('float64[:, ::1](float64, float64[::1], float64, float64, float64, float64, float64, float64,'
     'float64, float64)',
     nopython=True, cache=True, nogil=True)
def _u_bar_second_partials(t, state, mu, start_sun_phase, sun_mass, sun_radius, sun_inc,
                           sun_ang_vel, sun_raan, cont_param):
    """Second partial derivatives of the augmented potential U_bar.

    Parameters
    ----------
    t: float
        Time, required for integrators.
    state : iterable
        Orbit's state.
    mu : float
        CR3BP mass parameter.
    start_sun_phase : float
        Initial angular displacement
    sun_mass : float
        Normalized Sun mass
    sun_radius : float
        Normalized Sun orbital radius
    sun_ang_vel: float
        Normalized Sun angular velocity

    Returns
    -------
    u_bar2 : ndarray
        Second partial derivatives of the augmented potential as 3x3 Numpy array with
        ``u_bar2[i, j] = d_u_bar[i]/d_state[j]``.

    """

    x, y, z = state[:3]

    p_S = _sun_position(t, start_sun_phase, sun_radius, sun_inc, sun_ang_vel, sun_raan)

    r_13_2 = (x + mu)**2 + y**2 + z**2  # r13**2
    r_23_2 = (x - 1.0 + mu)**2 + y**2 + z**2  # r23**2
    r_S3_2 = (x - p_S[0])**2 + (y - p_S[1])**2 + (
                z - p_S[2])**2  # rS3**2

    r_1_i32 = r_13_2**(-1.5)
    r_2_i32 = r_23_2**(-1.5)
    r_S_i32 = r_S3_2**(-1.5)
    r_1_i52 = r_13_2**(-2.5)
    r_2_i52 = r_23_2**(-2.5)
    r_S_i52 = r_S3_2**(-2.5)

    u_bar2 = np.empty((3, 3))
    
    u_bar2[0, 0] = + mu * r_2_i32\
                   + (1.0 - mu) * r_1_i32\
                   - 3.0 * (1.0 - mu) * (x + mu)**2 * r_1_i52\
                   - 3.0 * mu * (x - 1.0 + mu)**2 * r_2_i52 \
                   - 1.0 \
                   + cont_param*(sun_mass * r_S_i32 - 3.0 * sun_mass * (x - p_S[0])**2 * r_S_i52)\

    u_bar2[0, 1] = - 3.0 * y * (mu + x) * (1.0 - mu) * r_1_i52\
                   - 3.0 * mu * y * (x - 1.0 + mu) * r_2_i52\
                   + cont_param*(- 3.0 * sun_mass * (x - p_S[0]) * (y - p_S[1]) * r_S_i52)  # OK SEMat

    u_bar2[0, 2] = - 3.0 * z * (mu + x) * (1.0 - mu) * r_1_i52\
                   - 3.0 * mu * z * (x - 1.0 + mu) * r_2_i52\
                   + cont_param*(- 3.0 * sun_mass * z * (x - p_S[0]) * r_2_i52)

    u_bar2[1, 0] = u_bar2[0, 1]

    u_bar2[1, 1] = + mu * r_2_i32\
                   + (1.0 - mu) * r_1_i32\
                   - 3.0 * (1.0 - mu) * y**2 * r_1_i52\
                   - 3.0 * mu * y**2 * r_2_i52 \
                   - 1.0 \
                   + cont_param*(sun_mass * r_S_i32 - 3.0 * sun_mass * (y - p_S[1])**2 * r_S_i52)

    u_bar2[1, 2] = - 3.0 * (1.0 - mu) * y * z * r_1_i52\
                   - 3.0 * mu * y * z * r_2_i52\
                   + cont_param*(- 3.0 * sun_mass * z * (y - p_S[1]) * r_S_i52)

    u_bar2[2, 0] = u_bar2[0, 2]
    u_bar2[2, 1] = u_bar2[1, 2]

    u_bar2[2, 2] = + mu * r_2_i32\
                   + (1.0 - mu) * r_1_i32\
                   - 3.0 * (1.0 - mu) * z**2  * r_1_i52\
                   - 3.0 * mu * z**2 * r_2_i52 \
                   + cont_param*(sun_mass * r_S_i32 - 3.0 * sun_mass * (z - p_S[2])**2 * r_S_i52)
    return u_bar2


@jit('float64[::1](float64, float64[::1], float64, float64, float64, float64, float64, float64,'
     'float64, float64)',
     nopython=True, cache=True, nogil=True)
def _acceleration_grav(t, state, mu, start_sun_phase, sun_mass, sun_radius, sun_inc, sun_ang_vel,
                       sun_raan, cont_param):
    """Equations of motion in synodic frame for the 6-dimensional state.

    Parameters
    ----------
    t: float
        Time, required for integrators.
    state : iterable
        Orbit's state.
    mu : float
        CR3BP mass parameter.
    start_sun_phase : float
        Initial angular displacement
    sun_mass : float
        Normalized Sun's mass
    sun_radius : float
        Normalized Sun's orbital radius
    sun_ang_vel: float
        Normalized Sun's angular velocity

    Returns
    -------
    acc_grav : ndarray, shape (3,)
        Acceleration vector in synodic frame.

    """

    u_bar1 = _u_bar_first_partials(t,state, mu, start_sun_phase, sun_mass, sun_radius, sun_inc,
                                   sun_ang_vel, sun_raan, cont_param)
    acc_grav = np.zeros(3)
    acc_grav[0] = 2.0 * state[4] - u_bar1[0]
    acc_grav[1] = - 2.0 * state[3] - u_bar1[1]
    acc_grav[2] = - u_bar1[2]
    return acc_grav


@jit('float64[:, ::1](float64, float64[::1], float64, float64, float64, float64, float64, float64,'
     'float64, float64)',
     nopython=True, cache=True, nogil=True)
def _variational_matrix_grav(t, state, mu, start_sun_phase, sun_mass, sun_radius, sun_inc,
                             sun_ang_vel, sun_raan, cont_param):
    """Equations of motion in synodic frame for the 6-dimensional orbit state and
    the 36-dimensional STM.

    Parameters
    ----------
    t : float
        Time, required for integrators.
    state : iterable
        Orbit's state (6-dim) concatenated with 36-dim State Transition Matrix (STM).
    mu : float
        CR3BP mass parameter.
    start_sun_phase : float
        Initial angular displacement
    sun_mass : float
        Normalized Sun's mass
    sun_radius : float
        Normalized Sun's orbital radius
    sun_ang_vel: float
        Normalized Sun's angular velocity

    Returns
    -------
    dot_state_stm : ndarray
        Set of 42 first-order ODEs of the 6-dim state and 36-dim STM.

    """

    # first-order ODEs for STM
    A_stm = np.zeros((6, 6))
    A_stm[0:3, 3:6] = np.eye(3)
    A_stm[3:6, 0:3] = -_u_bar_second_partials(t, state, mu, start_sun_phase, sun_mass, sun_radius,
                                              sun_inc, sun_ang_vel, sun_raan, cont_param)
    A_stm[3, 4] = 2.0
    A_stm[4, 3] = - 2.0

    return A_stm


@jit('float64[::1](float64, float64[::1], float64, float64, float64, float64, float64, float64,'
     'float64, float64, boolean)',
     nopython=True, cache=True, nogil=True)
def _ode_grav(t, state, mu, start_sun_phase, sun_mass, sun_radius, sun_inc, sun_ang_vel, sun_raan,
              cont_param, with_stm):
    state_dot = np.empty(6)
    acc_grav = _acceleration_grav(t, state, mu, start_sun_phase, sun_mass, sun_radius, sun_inc,
                                  sun_ang_vel, sun_raan, cont_param)
    state_dot[:3] = state[3:6]
    state_dot[3:6] = acc_grav
    if with_stm:
        state_dot = np.hstack((state_dot, np.empty(36)))
        A_stm = _variational_matrix_grav(t, state, mu, start_sun_phase, sun_mass, sun_radius,
                                         sun_inc, sun_ang_vel, sun_raan, cont_param)
        state_dot[6:42] = A_stm.dot(state[6:42].reshape(6, 6)).ravel()
    return state_dot


class BCR4BP(DynamicalEnvironment):
    """Bi-Circular Restricted 4 Body Problem (BCR4BP) class.

    This class computes the parameters and implements the equations of motion of a CR3BP system.Its
    attributes must have the same names as the ODE functions input parameters.

    Attributes (TEMPORARY, NEEDS UPDATE)
    ----------
    bodies : Iterable
        List of bodies in the system.
    frame_ode : Frame
        Reference frame of the environment's ODEs.
    mu : float
        CR3BP mass parameter.
    eom : function
        Decorated equations of motion.

    Methods
    -------
    get_ubar(state)
        Computes the value of the potential at a given state.
    get_ubar_first_partials(state)
        Computes the first partial derivatives of the potential at a given state.
    get_ubar_second_partials(state)
        Computes the second partial derivatives of the potential at a given state.
    get_jacobi(state)
        Computes the jacobi constant at a given state.
    """
    kind = 'BCR4BP'
    hamiltonian_scaling_factor = 1690  # Hamiltonian scaling term, from Boudad Master Thesis (2018)

    def __init__(self, primary_1: Primary, primary_2: Primary, start_sun_phase:float=0.,
                 sun_inc: float=0., sun_raan:float=0., continuation_param: float=1.0,
                 perturbations: Iterable[Perturbation]=None):
        """
        Parameters
        ----------
        primary_1 : Primary
            First primary of the system.
        primary_2 : Primary
            Second primary of the system.
        start_sun_phase : float, optional
            Azimuth of the starting point in the synodic frame (in radians, default is 0).
        sun_inc : float, optional
            Inclination of the Sun (in radians, default is 0).
        sun_raan : float, optional
            RAAN of the Sun (in radians, default is 0).
        continuation_param : float, optional
            Continuation parameter (default is 1.0), allows to transition from CR3BP to BCR4BP.
        perturbations : iterable of Perturbation, optional
            List of perturbations to be applied to the system (default is None).
        """
        # Check that neither of the primaries is the Sun
        if primary_1 == Primary.SUN or primary_2 == Primary.SUN:
            raise ValueError("The Sun cannot be a primary in the BCR4BP system.")

        # Raise an error if raan is not 0 and inc is 0
        if sun_raan != 0. and sun_inc == 0.:
            raise ValueError("The Sun's inclination must be non-zero if its RAAN is non-zero.")

        # Initialize the CR3BP environment
        cr3bp = CR3BP(primary_1, primary_2)

        # Initialize the DynamicalEnvironment
        super().__init__(list(cr3bp.bodies) + [Primary.SUN], self.AvailableFrames.SYNODIC,
                         perturbations,
                         adim_t=cr3bp.adim_t,
                         adim_l=cr3bp.adim_l)

        # Create namedtuples for the Sun's information
        sun_info = namedtuple('SunInfo', ['mass', 'radius', 'inc', 'ang_vel', 'raan'])
        sun_mass = Primary.SUN.M / (self.bodies[1].M + self.bodies[2].M)
        sun_radius = self.bodies[1].a / self.adim_l
        sun_ang_vel = 1 - np.sqrt((1 + sun_mass) / sun_radius**3)
        self._sun_info = sun_info(sun_mass, sun_radius, sun_inc, sun_ang_vel, sun_raan)
        self._start_sun_phase = start_sun_phase

        # Continuation parameter
        self._cont_param = continuation_param

        # Store the CR3BP environment
        self._cr3bp = cr3bp


    @property
    def m1(self):
        return self._cr3bp.m1

    @property
    def m2(self):
        return self._cr3bp.m2

    @property
    def libration_points(self):
        return self._cr3bp.libration_points

    @property
    def m1_pos(self):
        return self._cr3bp.m1_pos

    @property
    def m2_pos(self):
        return self._cr3bp.m2_pos

    @property
    def sun_info(self):
        return self._sun_info

    @property
    def cr3bp(self):
        return self._cr3bp

    @property
    def start_sun_phase(self):
        return self._start_sun_phase

    @start_sun_phase.setter
    def start_sun_phase(self, value):
        self._start_sun_phase = value

    def set_sun_to_date(self, date):
        self.start_sun_phase = self._get_sun_phase_from_date(date)

    @property
    def continuation_parameter(self):
        return self._cont_param

    @continuation_parameter.setter
    def continuation_parameter(self, value):
        self._cont_param = value

    def get_sun_position(self, t):
        """Returns the Sun's position at a given time.

        Parameters
        ----------
        t : float
            Time, required for integrators.
        """
        return _sun_position(t, self.start_sun_phase, self.sun_info.radius, self.sun_info.inc,
                             self.sun_info.ang_vel, self.sun_info.raan)

    def get_u_bar(self, t, state):
        """Returns the value of the potential function U_bar."""
        return _u_bar(t, state, self.cr3bp.mu, self.start_sun_phase, *self.sun_info,
                      self._cont_param)

    def get_u_bar_first_partials(self, t, state):
        """Returns the first partial derivatives of the potential with respect to the state."""
        return _u_bar_first_partials(t, state, self.cr3bp.mu, self.start_sun_phase, *self.sun_info,
                                     self._cont_param)

    def get_u_bar_second_partials(self, t, state):
        """Returns the second partial derivatives of the potential function U_bar."""
        return _u_bar_second_partials(t, state, self.cr3bp.mu, self.start_sun_phase, *self.sun_info,
                                      self._cont_param)

    def get_hamiltonian(self, t, state):
        """Returns the value of the Hamiltonian."""
        u_bar = self.get_u_bar(t, state)
        xdot, ydot, zdot = state[3:6]
        return -2*u_bar - (xdot**2 + ydot**2 + zdot**2) - \
            self.hamiltonian_scaling_factor*self.continuation_parameter

    def _get_sun_phase_from_date(self, date):
        """

        Returns
        -------
        angle_sun: float
            this is the angular position of the Sun in radiant, computed under the BR4BP hypothesis
        """

        et0 = sp.str2et(date)

        state_21, _ = sp.spkgeo(self.bodies[2].naif_id, et0, 'J2000', self.bodies[1].naif_id)
        pos_S1, _ = sp.spkpos(self.bodies[0].name, et0, 'J2000', 'NONE', self.bodies[1].name)

        pos_B1 = state_21[:3] * self.cr3bp.mu  # position of barycenter from P1
        pos_SB = pos_S1 - pos_B1  # Position of Sun from the P1-P2 baricenter

        # Compute the normal to the P1-P2 plane
        n = np.cross(state_21[:3], state_21[3:6])
        n /= np.linalg.norm(n)

        # Compute the projection of the Sun position on the P1-P2 plane
        pos_SB_proj = pos_SB - np.dot(pos_SB, n) * n

        # Compute the angle between the projection of the Sun position on the P1-P2 plane and the P1-P2 axis
        phase = np.arccos(np.dot(pos_SB_proj, state_21[:3]) /
                              (np.linalg.norm(pos_SB_proj) * np.linalg.norm(state_21[:3])))
        return phase

    def ode(self, t, state, with_stm=False):
        # TODO Here sum the perturbations
        state_dot_tot = _ode_grav(t, state, self.cr3bp.mu, self.start_sun_phase, *self.sun_info,
                         self._cont_param, with_stm=with_stm)
        return state_dot_tot

    @dataclass
    class AvailableFrames:
        """Class that collects the available frames of motion for the environment."""
        SYNODIC = SelectFrame.SYNODIC
