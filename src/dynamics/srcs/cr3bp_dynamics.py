# pylint: disable=duplicate-code
"""
Equations of motion (EOMs) in the Circular Restricted Three-Body problem written as a set of
first-order Ordinary Differential Equations (ODEs).

Source code for precompiled libraries built with Numba.

"""

import numpy as np
from numba.pycc import CC
from numba import njit


# object to generate compiled extension with Numba
NUMBA_COMPILER = CC('cr3bp_dynamics')
NUMBA_COMPILER.output_dir = '../libs'
NUMBA_COMPILER.target_cpu = 'host'
NUMBA_COMPILER.verbose = True


@njit
@NUMBA_COMPILER.export('u_bar_first_partials', 'float64[::1](float64[::1], float64)')
def u_bar_first_partials(state, mu_cr3bp):
    """First partial derivatives of the augmented potential U_bar.

    Parameters
    ----------
    state : iterable
        Orbit's state.
    mu_cr3bp : float
        CR3BP mass parameter.

    Returns
    -------
    u_bar1 : ndarray
        First partial derivatives of the augmented potential wrt (x, y, z).

    """

    r_13 = ((state[0] + mu_cr3bp) * (state[0] + mu_cr3bp) +
            state[1] * state[1] + state[2] * state[2]) ** (-1.5)  # 1/r1^3
    r_23 = (((state[0] - 1.0 + mu_cr3bp) * (state[0] - 1.0 + mu_cr3bp)) +
            state[1] * state[1] + state[2] * state[2]) ** (-1.5)  # 1/r2^3

    u_bar1 = np.empty(3)  # preallocate array to store the partials

    u_bar1[0] = mu_cr3bp * (state[0] - 1.0 + mu_cr3bp) * r_23 + \
        (1.0 - mu_cr3bp) * (state[0] + mu_cr3bp) * r_13 - state[0]
    u_bar1[1] = mu_cr3bp * state[1] * r_23 + (1.0 - mu_cr3bp) * state[1] * r_13 - state[1]
    u_bar1[2] = mu_cr3bp * state[2] * r_23 + (1.0 - mu_cr3bp) * state[2] * r_13

    return u_bar1


@njit
@NUMBA_COMPILER.export('u_bar_second_partials', 'float64[:, ::1](float64[::1], float64)')
def u_bar_second_partials(state, mu_cr3bp):
    """Second partial derivatives of the augmented potential U_bar.

    Parameters
    ----------
    state : iterable
        Orbit's state.
    mu_cr3bp : float
        CR3BP mass parameter.

    Returns
    -------
    u_bar2 : ndarray
        Second partial derivatives of the augmented potential as 3x3 Numpy array with
        ``u_bar2[i, j] = d_u_bar[i]/d_state[j]``.

    """

    r_12 = (state[0] + mu_cr3bp) * (state[0] + mu_cr3bp) + state[1] * state[1] + \
        state[2] * state[2]  # r1^2
    r_22 = ((state[0] - 1.0 + mu_cr3bp) * (state[0] - 1.0 + mu_cr3bp)) + state[1] * state[1] + \
        state[2] * state[2]  # r2^2

    r_13 = r_12 ** (-1.5)  # 1/r1^3
    r_23 = r_22 ** (-1.5)  # 1/r2^3
    r_15 = r_12 ** (-2.5)  # 1/r1^5
    r_25 = r_22 ** (-2.5)  # 1/r2^5

    u_bar2 = np.empty((3, 3))

    u_bar2[0, 0] = mu_cr3bp * r_23 + (1.0 - mu_cr3bp) * r_13 - \
        3.0 * mu_cr3bp * (state[0] - 1.0 + mu_cr3bp) * (state[0] - 1.0 + mu_cr3bp) * r_25 - \
        3.0 * (state[0] + mu_cr3bp) * (state[0] + mu_cr3bp) * (1.0 - mu_cr3bp) * r_15 - 1.0

    u_bar2[0, 1] = 3.0 * state[1] * (mu_cr3bp + state[0]) * (mu_cr3bp - 1.0) * r_15 - \
        3.0 * mu_cr3bp * state[1] * (state[0] - 1.0 + mu_cr3bp) * r_25

    u_bar2[0, 2] = 3.0 * state[2] * (mu_cr3bp + state[0]) * (mu_cr3bp - 1.0) * r_15 - \
        3.0 * mu_cr3bp * state[2] * (state[0] - 1.0 + mu_cr3bp) * r_25

    u_bar2[1, 0] = u_bar2[0, 1]

    u_bar2[1, 1] = mu_cr3bp * r_23 - (mu_cr3bp - 1.0) * r_13 + \
        3.0 * state[1] * state[1] * (mu_cr3bp - 1.0) * r_15 - \
        3.0 * mu_cr3bp * state[1] ** 2 * r_25 - 1.0

    u_bar2[1, 2] = 3.0 * state[1] * state[2] * (mu_cr3bp - 1.0) * r_15 - \
        3.0 * mu_cr3bp * state[1] * state[2] * r_25

    u_bar2[2, 0] = u_bar2[0, 2]
    u_bar2[2, 1] = u_bar2[1, 2]

    u_bar2[2, 2] = mu_cr3bp * r_23 - (mu_cr3bp - 1.0) * r_13 + \
        3.0 * state[2] ** 2 * (mu_cr3bp - 1.0) * r_15 - 3.0 * mu_cr3bp * state[2] ** 2 * r_25

    return u_bar2


@njit
@NUMBA_COMPILER.export('eqm_6_synodic', 'float64[::1](float64, float64[::1], float64)')
def eqm_6_synodic(_, state, mu_cr3bp):
    """Equations of motion in synodic frame for the 6-dimensional orbit state.

    Parameters
    ----------
    _ : float
        Time, required for integrators.
    state : iterable
        Orbit's state.
    mu_cr3bp : float
        CR3BP mass parameter.

    Returns
    -------
    dot_state : ndarray
        Set of 6 first-order ODEs of the 6-dim state.

    """

    u_bar = u_bar_first_partials(state, mu_cr3bp)

    dot_state = np.empty(6)  # preallocate array for state derivatives
    dot_state[0] = state[3]
    dot_state[1] = state[4]
    dot_state[2] = state[5]
    dot_state[3] = 2.0 * state[4] - u_bar[0]
    dot_state[4] = - 2.0 * state[3] - u_bar[1]
    dot_state[5] = - u_bar[2]

    return dot_state


@njit
@NUMBA_COMPILER.export('eqm_42_synodic', 'float64[::1](float64, float64[::1], float64)')
def eqm_42_synodic(_, state_stm, mu_cr3bp):
    """Equations of motion in synodic frame for the 6-dimensional orbit state and
    the 36-dimensional STM.

    Parameters
    ----------
    _ : float
        Time, required for integrators.
    state_stm : iterable
        Orbit's state (6-dim) concatenated with 36-dim State Transition Matrix (STM).
    mu_cr3bp : float
        CR3BP mass parameter.

    Returns
    -------
    dot_state_stm : ndarray
        Set of 42 first-order ODEs of the 6-dim state and 36-dim STM.

    """

    # first-order ODEs for state
    dot_state = eqm_6_synodic(_, state_stm[0:6], mu_cr3bp)

    # first-order ODEs for STM
    a_stm = np.zeros((6, 6))
    a_stm[0:3, 3:6] = np.eye(3)
    a_stm[3:6, 0:3] = - u_bar_second_partials(state_stm[0:6], mu_cr3bp)
    a_stm[3, 4] = 2.0
    a_stm[4, 3] = - 2.0
    a_stm = a_stm.dot(state_stm[6:].reshape((6, 6)))
    dot_state_stm = np.concatenate((dot_state, a_stm.reshape(36)))

    return dot_state_stm


@njit
@NUMBA_COMPILER.export('eqm_6_synodic_arclength', 'float64[::1](float64, float64[::1], float64)')
def eqm_6_synodic_arclength(_, state, mu_cr3bp):
    """Equations of motion in synodic frame for the 6-dimensional orbit state and the
    orbit arc length. """

    dot_state = np.empty(7)  # preallocate memory
    dot_state[0:6] = eqm_6_synodic(_, state[0:6], mu_cr3bp)
    dot_state[6] = (state[3] * state[3] + state[4] * state[4] + state[5] * state[5]) ** 0.5
    return dot_state


@njit
@NUMBA_COMPILER.export('eqm_42_synodic_arclength', 'float64[::1](float64, float64[::1], float64)')
def eqm_42_synodic_arclength(_, state_stm, mu_cr3bp):
    """Equations of motion in synodic frame for the 6-dimensional orbit state,
    the 36-dimensional STM and the orbit arc length. """

    dot_state_stm = np.empty(37)
    dot_state_stm[0:36] = eqm_42_synodic(_, state_stm[0:36], mu_cr3bp)
    dot_state_stm[36] = (state_stm[3] * state_stm[3] + state_stm[4] * state_stm[4] +
                         state_stm[5] * state_stm[5]) ** 0.5
    return dot_state_stm


if __name__ == '__main__':
    NUMBA_COMPILER.compile()
