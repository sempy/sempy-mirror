# pylint: disable = not-an-iterable, too-many-locals
"""
Dynamics in the ephemeris model with the optional inclusion of perturbative terms due to
the non-uniform gravity field of one or more celestial bodies.

@author: Alberto FOSSA'
"""

import numpy as np
from cffi import FFI
from numba import jit, prange
# from numba.core.typing.cffi_utils import register_module

from src.dynamics.ephemeris_dynamics import gravity_gradient, relative_position
from src.gravity.cunningham_method import sh_elementary_potentials, sh_gravity_acc, \
    sh_gravity_gradient
from src.utils.cspice_lib_loader import pxform  # TODO: solve issue with CFFI compiled wrapper

# try:  # import CSPICE functions from CFFI wrapper
#    from src.utils.libs.cspice_wrapper import lib
#    from src.utils.libs import cspice_wrapper
#    register_module(cspice_wrapper)
#    pxform = lib.pxform_c
# except ImportError:  # CFFI wrapper not found, import CSPICE functions from loaded DLL
#    cspice_wrapper = lib = None
#    from src.utils.cspice_lib_loader import pxform

ffi = FFI()


@jit('float64[::1](float64, float64[::1], float64, float64, int8[::1], int8[::1], float64[:, ::1], '
     'complex128[:, ::1], int64, int64)', nopython=True, nogil=True, fastmath=True)
def inertial_sh_acceleration(e_t, r_b, gm_b, r_ref, int_ref, bf_ref, rot_mat, z_nm, n_max, m_max):
    """Computes the perturbative acceleration due to the non-uniform gravity field
    of the selected celestial body.

    Input and output vector are expressed in the integration frame `int_ref`.

    Parameters
    ----------
    e_t : float
        Epoch time expressed in ephemeris seconds.
    r_b : ndarray
        Position of the spacecraft w.r.t. the body expressed in the integration frame.
    gm_b : float
        Body standard gravitational parameter.
    r_ref : float
        Reference radius.
    int_ref : ndarray
        Integration frame name encoded as a Numpy array of 8-bit ints.
    bf_ref : ndarray
        Body-fixed frame name encoded as a Numpy array of 8-bit ints.
    rot_mat : ndarray
        `3x3` ndarray to store the rotation matrix from `int_ref` to `bf_ref`.
    z_nm : ndarray
        Complex harmonics coefficients `Z(n,m) = C(n,m) + jS(n,m)` up to `n_max`, `m_max`.
    n_max : int
        Maximum degree for the spherical harmonics.
    m_max : int
        Maximum order for the spherical harmonics.

    Returns
    -------
    acc : ndarray
        Perturbative acceleration expressed in the integration frame.

    """

    pxform(ffi.from_buffer(int_ref), ffi.from_buffer(bf_ref), e_t, ffi.from_buffer(rot_mat))
    r_bf = rot_mat.dot(r_b)  # spacecraft position in body-fix frame
    v_nm = sh_elementary_potentials(r_bf / r_ref, n_max + 1, m_max + 1)  # elementary potentials
    a_bf = sh_gravity_acc(gm_b, r_ref, z_nm, v_nm, n_max, m_max)  # acceleration in body-fix frame
    return rot_mat.T.dot(a_bf)


@jit('(Tuple((float64[::1], float64[:, ::1])))(float64, float64[::1], float64, float64, '
     'int8[::1], int8[::1], float64[:, ::1], complex128[:, ::1], int64, int64)',
     nopython=True, nogil=True, fastmath=True)
def inertial_sh_acc_grad(e_t, r_b, gm_b, r_ref, int_ref, bf_ref, rot_mat, z_nm, n_max, m_max):
    """Computes the perturbative acceleration and gradient due to the non-uniform gravity
    field of the selected celestial body.

    Input and output vector are expressed in the integration frame `int_ref`.

    Parameters
    ----------
    e_t : float
        Epoch time expressed in ephemeris seconds.
    r_b : ndarray
        Position of the spacecraft w.r.t. the body expressed in the integration frame.
    gm_b : float
        Body standard gravitational parameter.
    r_ref : float
        Reference radius.
    int_ref : ndarray
        Integration frame name encoded as a Numpy array of 8-bit ints.
    bf_ref : ndarray
        Body-fixed frame name encoded as a Numpy array of 8-bit ints.
    rot_mat : ndarray
        `3x3` ndarray to store the rotation matrix from `int_ref` to `bf_ref`.
    z_nm : ndarray
        Complex harmonics coefficients `Z(n,m) = C(n,m) + jS(n,m)` up to `n_max`, `m_max`.
    n_max : int
        Maximum degree for the spherical harmonics.
    m_max : int
        Maximum order for the spherical harmonics.

    Returns
    -------
    acc : ndarray
        Perturbative acceleration expressed in the integration frame.
    grav_grad : ndarray
        Perturbative gradient expressed in the integration frame.

    """

    pxform(ffi.from_buffer(int_ref), ffi.from_buffer(bf_ref), e_t, ffi.from_buffer(rot_mat))
    r_bf = rot_mat.dot(r_b)  # spacecraft position in body-fix frame
    v_nm = sh_elementary_potentials(r_bf / r_ref, n_max + 2, m_max + 2)  # elementary potentials
    a_bf = sh_gravity_acc(gm_b, r_ref, z_nm, v_nm, n_max, m_max)  # acceleration in body-fix frame
    grav_grad_bf = sh_gravity_gradient(gm_b, r_ref, z_nm, v_nm, n_max, m_max)  # gradient
    return rot_mat.T.dot(a_bf), rot_mat.T.dot(grav_grad_bf).dot(rot_mat)

# TODO: same jit args type problem as in the ephemeris_dynamics.py module
# TODO: Test modified function "_eqm_6_sh_ephemeris" with "BuildDynamics"
# @jit('float64[::1](float64, float64[::1], float64[::1], int32[::1], int8[::1], float64, float64, '
    #  'float64[::1], int8[:, ::1], complex128[:, :, ::1], int64[:, ::1])',
@jit(nopython=True, nogil=True, fastmath=True)
def _eqm_6_sh_ephemeris(t_adim, state, gm_adim, body_ids, ref, t_c, l_ci,
                       r_adim, bf_ref, z_nm, nm_max, stm=False):
    """Equations of motion in the ephemeris force model written as set of first-order Ordinary
    Differential Equations.

    The implemented ODEs describe the motion of a particle under the gravitational attraction of
    ``n`` celestial bodies modelled as point masses. Perturbative terms due to the non-uniform
    gravity field of one or more celestial bodies are modelled with spherical harmonics series
    truncated at degree and order `(n,m)` and optionally included in the force model.

    The dynamics is expressed in an inertial reference frame, i.e. J2000, whose origin coincides
    by convention with the first celestial body in `body_ids`.
    Positions of the other ``n-1`` bodies w.r.t. the frame's origin are obtained from ephemeris
    data extrapolated using the SPICE routine `spkgps`.

    The following notation is adopted:
        1) subscript `sc` denotes the particle of interest, i.e. the spacecraft.
        2) subscript `o` denotes the first celestial body at the origin of the inertial frame.
        3) subscript `b` denotes all other ``n-1`` bodies in turn.

    Parameters
    ----------
    t_adim : float
        Current time in non-dimensional units.
    state : ndarray
        Current state in non-dimensional units.
    gm_adim : ndarray
        Standard gravitational parameters for the ``n`` considered bodies.
    body_ids : ndarray
        NAIF IDs for the ``n`` considered bodies.
    ref : ndarray
        Name of the inertial reference frame, i.e. J2000, encoded as Numpy array of 8-bit integers.
    t_c : float
        Characteristic time for dimensionalization.
    l_ci : float
        Reciprocal of the characteristic length for adimensionalization.
    r_adim : ndarray
        Reference radii for non-uniform gravity potential models.
        Must be in the same units as `state`.
    bf_ref : ndarray
        Body-fixed reference frames for non-uniform gravity potential models encoded as Numpy
        arrays of 8-bit integers. Appropriate SPICE kernels must be loaded beforehand to compute
        the instantaneous rotation matrix from `ref` to each instance of `bf_ref` using the
        CSPICE function `pxform`.
    z_nm : ndarray
        Complex harmonics coefficients `Z(n,m) = C(n,m) + jS(n,m)`.
    nm_max : ndarray
        Maximum degrees and orders for the non-uniform gravity potential model of each body in
        the ephemeris. If the degree is equal to `0` or `-1` no SH model is considered for the
        corresponding body.

    Returns
    -------
    state_dot : ndarray
        First time derivative of `state`.

    """
    if stm is False:
        state_dot = np.zeros((6,))  # first time derivative of the state
        state_dot[0:3] = state[3:6]  # velocity as derivative of the position
        v_dot = state_dot[3:6]  # acceleration as derivative of the velocity
        r_osc = state[0:3]  # position of the spacecraft w.r.t. the origin

        rot_mat = np.empty((3, 3))  # rotation matrix from integration to body-fixed frame
        r_ob = np.empty((3,))  # position of body b w.r.t. the origin
        r_bsc = np.empty((3,))  # position of the spacecraft w.r.t. body b
        lt_ob = np.empty((1,))  # light-time from body b to the origin (required by spkgps)
        t_dim = t_adim * t_c  # dimensional time (for cspice function calls)

        for j in prange(1, gm_adim.size):  # loop over all bodies except the one at the origin
            relative_position(body_ids[j], t_dim, ref, body_ids[0], r_ob, r_osc, r_bsc, lt_ob, l_ci)
            v_dot += - gm_adim[j] * (r_bsc * r_bsc.dot(r_bsc) ** -1.5 + r_ob * r_ob.dot(r_ob) ** -1.5)
            if nm_max[j, 1] > 0:  # non-uniform gravity field model provided for body b
                i = nm_max[j, 0]  # index of body b in r_ref_adim, bf_ref, and z_nm arrays
                v_dot += inertial_sh_acceleration(t_dim, r_bsc, gm_adim[j], r_adim[i], ref, bf_ref[i],
                                                rot_mat, z_nm[i], nm_max[j, 1], nm_max[j, 2])
        v_dot += - gm_adim[0] * r_osc * r_osc.dot(r_osc) ** -1.5  # update acceleration (origin)
        if nm_max[0, 1] > 0:  # non-uniform gravity field model provided for the origin
            i = nm_max[0, 0]  # index of the origin in r_ref_adim, bf_ref, and z_nm arrays
            v_dot += inertial_sh_acceleration(t_dim, r_osc, gm_adim[0], r_adim[i], ref, bf_ref[i],
                                            rot_mat, z_nm[i], nm_max[0, 1], nm_max[0, 2])
    else:
        state_dot = np.zeros((42,))  # first time derivative of the state
        state_dot[0:3] = state[3:6]  # velocity as derivative of the position
        v_dot = state_dot[3:6]  # acceleration as derivative of the velocity
        r_osc = state[0:3]  # position of the spacecraft w.r.t. the origin

        rot_mat = np.empty((3, 3))  # rotation matrix from integration to body-fixed frame
        r_ob = np.empty((3,))  # position of body b w.r.t. the origin
        r_bsc = np.empty((3,))  # position of the spacecraft w.r.t. body b
        lt_ob = np.empty((1,))  # light-time from body b to the origin (required by spkgps)
        grav_grad_bsc = np.empty((3, 3))  # gravity gradient of the spacecraft due to body b
        a_stm = np.zeros((6, 6))  # matrix A of the partials of the ODEs w.r.t. the state
        a_stm[0, 3] = a_stm[1, 4] = a_stm[2, 5] = 1.0  # NE 3x3 sub-matrix of A equal to identity 3x3
        t_dim = t_adim * t_c  # dimensional time (for cspice function calls)

        for j in prange(1, gm_adim.size):  # loop over all bodies except the one at the origin
            relative_position(body_ids[j], t_dim, ref, body_ids[0], r_ob, r_osc, r_bsc, lt_ob, l_ci)
            r_bsc2 = gravity_gradient(r_bsc, gm_adim[j], grav_grad_bsc)  # gravity gradient (body b)
            v_dot += - gm_adim[j] * (r_ob * r_ob.dot(r_ob) ** -1.5 + r_bsc * r_bsc2 ** -1.5)
            a_stm[3:6, 0:3] += grav_grad_bsc  # update matrix A (body b)
            if nm_max[j, 1] > 0:  # non-uniform gravity field model provided for body b
                i = nm_max[j, 0]  # index of body b in r_ref_adim, bf_ref, and z_nm arrays
                sh_acc, sh_grad = inertial_sh_acc_grad(t_dim, r_bsc, gm_adim[j], r_adim[i], ref,
                                                    bf_ref[i], rot_mat, z_nm[i],
                                                    nm_max[j, 1], nm_max[j, 2])
                v_dot += sh_acc  # update acceleration (body b, perturbative terms)
                a_stm[3:6, 0:3] += sh_grad  # update matrix A (body b, perturbative terms)

            r_osc2 = gravity_gradient(r_osc, gm_adim[0], grav_grad_bsc)  # gravity gradient (origin)
            v_dot += -gm_adim[0] * r_osc * r_osc2 ** -1.5  # update acceleration (origin)
            a_stm[3:6, 0:3] += grav_grad_bsc  # update matrix A (origin)
            if nm_max[0, 1] > 0:  # non-uniform gravity field model provided for the origin
                i = nm_max[0, 0]  # index of the origin in r_ref_adim, bf_ref, and z_nm arrays
                sh_acc, sh_grad = inertial_sh_acc_grad(t_dim, r_osc, gm_adim[0], r_adim[i], ref, bf_ref[i],
                                                    rot_mat, z_nm[i], nm_max[0, 1], nm_max[0, 2])
                v_dot += sh_acc  # update acceleration (origin, perturbative terms)
                a_stm[3:6, 0:3] += sh_grad  # update matrix A (origin, perturbative terms)

            state_dot[6:42] = a_stm.dot(state[6:42].reshape((6, 6))).ravel()  # derivatives of STM

    return state_dot


@jit('float64[::1](float64, float64[::1], float64[::1], int32[::1], int8[::1], float64, float64, '
     'float64[::1], int8[:, ::1], complex128[:, :, ::1], int64[:, ::1])',
     nopython=True, nogil=True, fastmath=True)
def _eqm_42_sh_ephemeris(t_adim, state42, gm_adim, body_ids, ref, t_c, l_ci,
                        r_adim, bf_ref, z_nm, nm_max):
    """Equations of motion in the ephemeris force model written as set of first-order Ordinary
    Differential Equations.

    The implemented ODEs describe the motion of a particle under the gravitational attraction of
    ``n`` celestial bodies modelled as point masses and are propagated together with the
    corresponding 6x6 State Transition Matrix (STM). Perturbative terms due to the non-uniform
    gravity field of one or more celestial bodies are modelled with spherical harmonics series
    truncated at degree and order `(n,m)` and optionally included in the force model.

    The dynamics is expressed in an inertial reference frame, i.e. J2000, whose origin coincides
    by convention with the first celestial body in `body_ids`.
    Positions of the other ``n-1`` bodies w.r.t. the frame's origin are obtained from ephemeris
    data extrapolated using the SPICE routine `spkgps`.

    The following notation is adopted:
        1) subscript `sc` denotes the particle of interest, i.e. the spacecraft.
        2) subscript `o` denotes the first celestial body at the origin of the inertial frame.
        3) subscript `b` denotes all other ``n-1`` bodies in turn.

    Parameters
    ----------
    t_adim : float
        Current time in non-dimensional units.
    state42 : ndarray
        Current state concatenated with STM in non-dimensional units.
    gm_adim : ndarray
        Standard gravitational parameters for the ``n`` considered bodies.
    body_ids : ndarray
        NAIF IDs for the ``n`` considered bodies.
    ref : ndarray
        Name of the inertial reference frame, i.e. J2000, encoded as Numpy array of 8-bit integers.
    t_c : float
        Characteristic time for dimensionalization.
    l_ci : float
        Reciprocal of the characteristic length for adimensionalization.
    r_adim : ndarray
        Reference radii for non-uniform gravity potential models.
        Must be in the same units as `state42`.
    bf_ref : ndarray
        Body-fixed reference frames for non-uniform gravity potential models encoded as Numpy
        arrays of 8-bit integers. Appropriate SPICE kernels must be loaded beforehand to compute
        the instantaneous rotation matrix from `ref` to each instance of `bf_ref` using the
        CSPICE function `pxform`.
    z_nm : ndarray
        Complex harmonics coefficients `Z(n,m) = C(n,m) + jS(n,m)`.
    nm_max : ndarray
        Maximum degrees and orders for the non-uniform gravity potential model of each body in
        the ephemeris. If the degree is equal to `0` or `-1` no SH model is considered for the
        corresponding body.

    Returns
    -------
    state42_dot : ndarray
        First time derivative of `state42`.

    """

    state42_dot = np.zeros((42,))  # first time derivative of the state
    state42_dot[0:3] = state42[3:6]  # velocity as derivative of the position
    v_dot = state42_dot[3:6]  # acceleration as derivative of the velocity
    r_osc = state42[0:3]  # position of the spacecraft w.r.t. the origin

    rot_mat = np.empty((3, 3))  # rotation matrix from integration to body-fixed frame
    r_ob = np.empty((3,))  # position of body b w.r.t. the origin
    r_bsc = np.empty((3,))  # position of the spacecraft w.r.t. body b
    lt_ob = np.empty((1,))  # light-time from body b to the origin (required by spkgps)
    grav_grad_bsc = np.empty((3, 3))  # gravity gradient of the spacecraft due to body b
    a_stm = np.zeros((6, 6))  # matrix A of the partials of the ODEs w.r.t. the state
    a_stm[0, 3] = a_stm[1, 4] = a_stm[2, 5] = 1.0  # NE 3x3 sub-matrix of A equal to identity 3x3
    t_dim = t_adim * t_c  # dimensional time (for cspice function calls)

    for j in prange(1, gm_adim.size):  # loop over all bodies except the one at the origin
        relative_position(body_ids[j], t_dim, ref, body_ids[0], r_ob, r_osc, r_bsc, lt_ob, l_ci)
        r_bsc2 = gravity_gradient(r_bsc, gm_adim[j], grav_grad_bsc)  # gravity gradient (body b)
        v_dot += - gm_adim[j] * (r_ob * r_ob.dot(r_ob) ** -1.5 + r_bsc * r_bsc2 ** -1.5)
        a_stm[3:6, 0:3] += grav_grad_bsc  # update matrix A (body b)
        if nm_max[j, 1] > 0:  # non-uniform gravity field model provided for body b
            i = nm_max[j, 0]  # index of body b in r_ref_adim, bf_ref, and z_nm arrays
            sh_acc, sh_grad = inertial_sh_acc_grad(t_dim, r_bsc, gm_adim[j], r_adim[i], ref,
                                                   bf_ref[i], rot_mat, z_nm[i],
                                                   nm_max[j, 1], nm_max[j, 2])
            v_dot += sh_acc  # update acceleration (body b, perturbative terms)
            a_stm[3:6, 0:3] += sh_grad  # update matrix A (body b, perturbative terms)

    r_osc2 = gravity_gradient(r_osc, gm_adim[0], grav_grad_bsc)  # gravity gradient (origin)
    v_dot += -gm_adim[0] * r_osc * r_osc2 ** -1.5  # update acceleration (origin)
    a_stm[3:6, 0:3] += grav_grad_bsc  # update matrix A (origin)
    if nm_max[0, 1] > 0:  # non-uniform gravity field model provided for the origin
        i = nm_max[0, 0]  # index of the origin in r_ref_adim, bf_ref, and z_nm arrays
        sh_acc, sh_grad = inertial_sh_acc_grad(t_dim, r_osc, gm_adim[0], r_adim[i], ref, bf_ref[i],
                                               rot_mat, z_nm[i], nm_max[0, 1], nm_max[0, 2])
        v_dot += sh_acc  # update acceleration (origin, perturbative terms)
        a_stm[3:6, 0:3] += sh_grad  # update matrix A (origin, perturbative terms)

    state42_dot[6:42] = a_stm.dot(state42[6:42].reshape((6, 6))).ravel()  # derivatives of STM

    return state42_dot


ode_types = {"state": _eqm_6_sh_ephemeris,
             "state_stm": _eqm_42_sh_ephemeris}