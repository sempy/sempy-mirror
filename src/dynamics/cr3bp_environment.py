# pylint: disable=duplicate-code
"""
Circular Restricted Three-Body problem environment.
"""

import numpy as np
from numba import jit
from dataclasses import dataclass
from typing import Iterable
from src.dynamics.perturbations import Perturbation

from src.dynamics.dynamical_environment import DynamicalEnvironment
from src.coc.frames import SelectFrame
from src.init.primary import Primary


@jit('float64(float64[::1], float64)', nopython=True, cache=True, nogil=True)
def _u_bar(state, mu):
    """Augmented potential (from Koon et al. 2011, chapter 2).

    Parameters
    ----------
    state : np.ndarray
        A 6-dimensional state vector.
    mu : float
        CR3BP mass parameter.
    """
    mu1 = 1 - mu
    mu2 = mu
    # where r1 and r2 are expressed in rotating coordinates
    r1_var = ((state[0]+mu2)**2 + state[1]**2 + state[2]**2)**(1/2)
    r2_var = ((state[0]-mu1)**2 + state[1]**2 + state[2]**2)**(1/2)
    aug_pot = -1/2*(state[0]**2+state[1]**2) - mu1/r1_var - mu2/r2_var - 1/2*mu1*mu2
    return aug_pot


@jit('float64[::1](float64[::1], float64)', nopython=True, cache=True, nogil=True)
def _u_bar_first_partials(state, mu):
    """First partial derivatives of the augmented potential U_bar.

    Parameters
    ----------
    state : np.ndarray
        A 6-dimensional state vector.
    mu : float
        CR3BP mass parameter.

    Returns
    -------
    _u_bar : np.ndarray
        First partial derivatives of the augmented potential wrt (x, y, z).

    """

    r_13 = ((state[0] + mu) * (state[0] + mu) +
            state[1] * state[1] + state[2] * state[2]) ** (-1.5)  # 1/r1^3
    r_23 = (((state[0] - 1.0 + mu) * (state[0] - 1.0 + mu)) +
            state[1] * state[1] + state[2] * state[2]) ** (-1.5)  # 1/r2^3

    u_bar1 = np.empty(3)  # preallocate array to store the partials

    u_bar1[0] = mu * (state[0] - 1.0 + mu) * r_23 + \
        (1.0 - mu) * (state[0] + mu) * r_13 - state[0]
    u_bar1[1] = mu * state[1] * r_23 + (1.0 - mu) * state[1] * r_13 - state[1]
    u_bar1[2] = mu * state[2] * r_23 + (1.0 - mu) * state[2] * r_13
    return u_bar1


@jit('float64[:, ::1](float64[::1], float64)', nopython=True, cache=True, nogil=True)
def _u_bar_second_partials(state, mu):
    """Second partial derivatives of the augmented potential U_bar.

    Parameters
    ----------
    state : np.ndarray
        A 6-dimensional state vector.
    mu : float
        CR3BP mass parameter.

    Returns
    -------
    u_bar2 : np.ndarray
        Second partial derivatives of the augmented potential as 3x3 Numpy array with
        ``u_bar2[i, j] = d_u_bar[i]/d_state[j]``.

    """

    r_12 = (state[0] + mu) * (state[0] + mu) + state[1] * state[1] + \
        state[2] * state[2]  # r1^2
    r_22 = ((state[0] - 1.0 + mu) * (state[0] - 1.0 + mu)) + state[1] * state[1] + \
        state[2] * state[2]  # r2^2

    r_13 = r_12 ** (-1.5)  # 1/r1^3
    r_23 = r_22 ** (-1.5)  # 1/r2^3
    r_15 = r_12 ** (-2.5)  # 1/r1^5
    r_25 = r_22 ** (-2.5)  # 1/r2^5

    u_bar2 = np.empty((3, 3))

    u_bar2[0, 0] = mu * r_23 + (1.0 - mu) * r_13 - \
        3.0 * mu * (state[0] - 1.0 + mu) * (state[0] - 1.0 + mu) * r_25 - \
        3.0 * (state[0] + mu) * (state[0] + mu) * (1.0 - mu) * r_15 - 1.0

    u_bar2[0, 1] = 3.0 * state[1] * (mu + state[0]) * (mu - 1.0) * r_15 - \
        3.0 * mu * state[1] * (state[0] - 1.0 + mu) * r_25

    u_bar2[0, 2] = 3.0 * state[2] * (mu + state[0]) * (mu - 1.0) * r_15 - \
        3.0 * mu * state[2] * (state[0] - 1.0 + mu) * r_25

    u_bar2[1, 0] = u_bar2[0, 1]

    u_bar2[1, 1] = mu * r_23 - (mu - 1.0) * r_13 + \
        3.0 * state[1] * state[1] * (mu - 1.0) * r_15 - \
        3.0 * mu * state[1] ** 2 * r_25 - 1.0

    u_bar2[1, 2] = 3.0 * state[1] * state[2] * (mu - 1.0) * r_15 - \
        3.0 * mu * state[1] * state[2] * r_25

    u_bar2[2, 0] = u_bar2[0, 2]
    u_bar2[2, 1] = u_bar2[1, 2]

    u_bar2[2, 2] = mu * r_23 - (mu - 1.0) * r_13 + \
        3.0 * state[2] ** 2 * (mu - 1.0) * r_15 - 3.0 * mu * state[2] ** 2 * r_25

    return u_bar2


@jit('float64(float64[::1], float64)', nopython=True, cache=True, nogil=True)
def _jacobi(state, mu):
    """Computes the jacobi constant at a given state in the CRTBP system with the mass ratio mu.
    See equation 2.3.14 of Koon et al. 2011.

    Parameters
    ----------
    state : np.ndarray
        A 6-dimensional state vector.
    mu : float
        CR3BP mass parameter.
        """
    return -2*_u_bar(state, mu) - (state[3]**2 + state[4]**2 + state[5]**2)


@jit('float64[::1](float64[::1], float64)', nopython=True, cache=True, nogil=True)
def _acceleration_grav(state, mu):
    """Equations of motion in synodic frame for the 6-dimensional state.

    Parameters
    ----------
    state : np.ndarray
        A 6-dimensional or 42-dimensional state vector.
    mu : float
        CR3BP mass parameter.

    Returns
    -------
    dot_state : np.ndarray
        Set of 6 first-order ODEs of the 6-dim state.

    """

    u_bar1 = _u_bar_first_partials(state, mu)
    state_dot = np.zeros(3)
    state_dot[0] = 2.0 * state[4] - u_bar1[0]
    state_dot[1] = - 2.0 * state[3] - u_bar1[1]
    state_dot[2] = - u_bar1[2]
    return state_dot


@jit('float64[:, ::1](float64[::1], float64)', nopython=True, cache=True, nogil=True)
def _variational_matrix_grav(state, mu):
    """Variational matrix."""
    A_stm = np.zeros((6, 6))
    A_stm[:3, 3:] = np.eye(3)
    A_stm[3:, :3] = - _u_bar_second_partials(state, mu)
    A_stm[3:, 3:] = np.array([[0, 2, 0],
                              [-2, 0, 0],
                              [0, 0, 0]])
    return A_stm


@jit('float64[::1](float64[::1], float64, boolean)', nopython=True, cache=True, nogil=True)
def _ode_grav(state, mu, with_stm):
    state_dot = np.zeros(6)
    acc_grav = _acceleration_grav(state, mu)
    state_dot[:3] = state[3:6]
    state_dot[3:6] = acc_grav
    if with_stm:
        state_dot = np.hstack((state_dot, np.zeros(36)))
        A_stm = _variational_matrix_grav(state, mu)
        state_dot[6:42] = A_stm.dot(state[6:42].reshape(6, 6)).ravel()
    return state_dot


@jit('float64[:, ::1](float64)',nopython=True, cache=True, nogil=True)
def _libration_points(mu):

    # Non dimensional position of the second primary
    xp_2 = 1 - mu

    # Define the polynomials to find the collinear points
    poly_L1 = np.array([1., 2*(mu-xp_2), xp_2**2-4*xp_2*mu+mu**2,
                        2*mu*xp_2*(xp_2-mu)+(mu-xp_2), mu**2*xp_2**2+2*(xp_2**2+mu**2),
                        mu**3-xp_2**3], dtype=np.complex128)
    poly_L2 = np.array([1., 2*(mu-xp_2), xp_2**2-4*xp_2*mu+mu**2,
                        2*mu*xp_2*(xp_2-mu)-(mu+xp_2), mu**2*xp_2**2+2*(xp_2**2-mu**2),
                        -(mu**3+xp_2**3)], dtype=np.complex128)
    poly_L3 = np.array([1., 2*(mu-xp_2), xp_2**2-4*xp_2*mu+mu**2,
                        2*mu*xp_2*(xp_2-mu)+(mu+xp_2), mu**2*xp_2**2+2*(mu**2-xp_2**2),
                        xp_2**3+mu**3], dtype=np.complex128)

    # Find the roots of polynomials
    L1roots = np.roots(poly_L1)
    L2roots = np.roots(poly_L2)
    L3roots = np.roots(poly_L3)

    # Extract the real root
    L1realroot = np.real(L1roots[np.argmin(np.abs(L1roots.imag))])
    L2realroot = np.real(L2roots[np.argmin(np.abs(L2roots.imag))])
    L3realroot = np.real(L3roots[np.argmin(np.abs(L3roots.imag))])

    # Build the matrix of all the lagrangian points
    libration_points_states = np.zeros((5, 6), dtype=np.float64)

    # Populate the elements with the roots (x and y terms)
    libration_points_states[:, 0] = [L1realroot,
                            L2realroot,
                            L3realroot,
                            -mu + 0.5,
                            -mu + 0.5]
    
    libration_points_states[3:, 1] = [np.sqrt(3)/2, -np.sqrt(3)/2]

    # return matrix
    return libration_points_states


@jit('float64[:, ::1](float64[::1], float64)', nopython=True, cache=True, nogil=True)
def _f_matrix(state, mu):

    F = np.zeros((6,6))

    F[:3, 3:]  = np.eye(3)    
    F[3:, :3]  = - _u_bar_second_partials(state, mu)
    F[3:, 3:]  = np.array([[0, 2, 0],
                           [-2, 0, 0],
                           [0, 0, 0]])

    return F


class CR3BP(DynamicalEnvironment):
    """Circular Restricted 3 Body Problem (CR3BP) class.

    This class computes the parameters and implements the equations of motion of a CR3BP system.Its
    attributes must have the same names as the ODE functions input parameters.

    Attributes (TEMPORARY, NEEDS UPDATE)
    ----------
    bodies : Iterable
        List of bodies in the system.
    frame_ode : Frame
        Reference frame of the environment's ODEs.
    mu : float
        CR3BP mass parameter.
    eom : function
        Decorated equations of motion.

    Methods
    -------
    get_ubar(state)
        Computes the value of the potential at a given state.
    get_ubar_first_partials(state)
        Computes the first partial derivatives of the potential at a given state.
    get_ubar_second_partials(state)
        Computes the second partial derivatives of the potential at a given state.
    get_jacobi(state)
        Computes the jacobi constant at a given state.
    """
    kind = 'CR3BP'

    def __init__(self, primary_1: Primary, primary_2: Primary, perturbations: Iterable[Perturbation]=None):
        """
        Parameters
        ----------
        primary_1 : Primary
            Larger body.
        primary_2 : Primary
            Smaller body.
        perturbations : iterable of Perturbation, optional
        """
        # For the time constant, the smallest semi-major axis is used

        super().__init__((primary_1, primary_2), self.AvailableFrames.SYNODIC,
                         perturbations,
                         adim_t=1.,
                         adim_l=1.)

        # Overwrite the time and length constants
        self._adim_t = np.sqrt(self.m2.a ** 3 / (primary_1.GM + primary_2.GM))
        self._adim_l = self.m2.a

        # Mass parameter (argument of EOMs)
        self._mu = self.bodies[1].M / (self.bodies[0].M + self.bodies[1].M)

        # Set lagrangian points state
        self._libration_points = self.LibrationPoints(self.mu)

    @property
    def mu(self):
        return self._mu

    @property
    def m1_pos(self):
        return -self.mu, 0, 0

    @property
    def m2_pos(self):
        return 1 - self.mu, 0, 0

    @property
    def libration_points(self):
        return self._libration_points

    @property
    def m1(self):
        return self.bodies[0]

    @property
    def m2(self):
        return self.bodies[1]

    def get_u_bar(self, state):
        """Returns the potential at a given state.

        Parameters
        ----------
        state : np.ndarray
            A 6-dimensional state vector.
        """
        return _u_bar(state, self.mu)

    def get_u_bar_first_partials(self, state):
        """Returns the first partial derivatives of the potential with respect to the state.

        Parameters
        ----------
        state : np.ndarray
            A 6-dimensional state vector.
        """
        return _u_bar_first_partials(state, self.mu)

    def get_u_bar_second_partials(self, state):
        """Returns the second partial derivatives of the potential function U_bar.

        Parameters
        ----------
        state : np.ndarray
            A 6-dimensional state vector.
        """
        return _u_bar_second_partials(state, self.mu)

    def get_jacobi(self, state):
        """Get the Jacobi constant at a given state.

        Parameters
        ----------
        state : np.ndarray
            A 6-dimensional state vector.
        """
        return _jacobi(state, self.mu)

    def get_f_matrix(self, state):
        """Get the linearized dynamics matrix.

        Parameters
        ----------
        state : np.ndarray
            A 6-dimensional state vector.
        """
        return _f_matrix(state, self.mu)

    def  ode(self, _, state, with_stm=False):
        # TODO Here sum the perturbations
        state_dot_tot = _ode_grav(state, self.mu, with_stm)
        return  state_dot_tot

    class LibrationPoints:
        def __init__(self, mu):
            LP = _libration_points(mu)

            self.L1 = LP[0, :]
            self.L2 = LP[1, :]
            self.L3 = LP[2, :]
            self.L4 = LP[3, :]
            self.L5 = LP[4, :]

    @dataclass
    class AvailableFrames:
        """Class that collects the available frames of motion for the environment."""
        SYNODIC = SelectFrame.SYNODIC

