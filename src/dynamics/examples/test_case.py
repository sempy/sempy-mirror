'''Example of orbit correction in cr3bp'''
x0 = [1,2,3,4,4,5]

cr3bp = Cr3bp()
cr3bp.set_primaries(Primaries)
cr3bp.set_ode.state

prop = Propagator(options)
prop.set_dynamics(cr3bp)


constr = Constraints(prop)
constr.add_constr.periodic() # -> xf - x0 = 0, xf = prop(x0, tau) 
# constr.add_constr.quasi_periodic()
constr.add_constr.poincare.fix_vars(indx, val)
constr.merge_constr()

corrector = Corrector(option)
corrector.set_constraints(constr)
corrector.set_init_cond([x0, tau])
sol = corrector.solve()


''' Update in er3bp '''

x0_new = sol[0:6]
tau_new = sol[6]
### change of coordinates needed

er3bp = Er3bp()
er3bp.set_primaries(Primaries)
er3bp.set_true_anomaly(val)
er3bp.set_ode.state

prop.set_dynamics(er3bp)

constr.purge_constraints()
constr.add_constr.periodic()
constr.add_constr.poincare.fix_vars(idx_tau, val_tau)
constr.merge_constr()

corrector.set_constraints(constr)                                                                    
corrector.set_init_cond([x0_new, tau_new])

sol_er3bp = corrector.solve()


''' Multiple shooting in ephemeris '''
nbp = NBP()
nbp.set_primaries(Primaries)
nbp.set_date(date, format='ET')
nbp.set_frame('eclipj2000')
nbp.set_origin(Primaries[2].naif_id)


# Sample nodes
x0_new2 = sol_er3bp[0:6]
tau_new2 = sol_er3bp[6]
t_nodes,x_nodes = prop.propagate(np.linspace(0,tau_new2*5,100),x0_new2)

### change of coordinates needed

constr.purge_constr()
constr.add_constr.poincare.fix_vars()

