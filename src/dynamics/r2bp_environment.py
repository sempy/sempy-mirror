import numpy as np
from numba import jit
from dataclasses import dataclass

from src.dynamics.dynamical_environment import DynamicalEnvironment
from src.coc.frames import SelectFrame, Frame
from src.init.primary import Primary

@jit('float64[::1](float64[::1], float64)', nopython=True, cache=True, nogil=True)
def _acceleration_grav(state, mu):
    """Equations of motion in inertial frame for the 6-dimensional state.

    Parameters
    ----------
    state : np.ndarray
        A 6-dimensional or 42-dimensional state vector.
    mu : float
        R2BP gravitational parameter.

    Returns
    -------
    dot_state : np.ndarray
        Set of 6 first-order ODEs of the 6-dim state.

    """
    pos_norm_3 = np.linalg.norm(state[:3])**3
    acc_grav = np.empty(3)  # preallocate array for state derivatives
    acc_grav[0] = - mu * state[0] / pos_norm_3
    acc_grav[1] = - mu * state[1] / pos_norm_3
    acc_grav[2] = - mu * state[2] / pos_norm_3

    return acc_grav

@jit('float64[:, ::1](float64[::1], float64)', nopython=True, cache=True, nogil=True)
def _variational_matrix_grav(state, mu):
    """First partial derivative of the 2-body accelerations.

    [dax/dx, day/dx, daz/dx,
    dax/dy, day/dy, daz/dy,
    dax/dz, day/dz, daz/dz]

    Parameters
    ----------
    state : np.ndarray
        State vector.
    mu : float
        Gravitational parameter.
    """

    r_norm2 = (state[0]**2 + state[1]**2 + state[2]**2)

    r_i3 = r_norm2 ** (-1.5)  # 1/r_norm^3
    r_i5 = r_norm2 ** (-2.5)  # 1/r_norm^5

    A_stm = np.zeros((6, 6))
    A_stm[:3, 3:] = np.eye(3)

    acc_dot = np.empty((3, 3))

    acc_dot[0, 0] = mu * (3.0 * state[0] * state[0] * r_i5 - 1 * r_i3)
    acc_dot[0, 1] = mu * 3.0 * state[0] * state[1] * r_i5
    acc_dot[0, 2] = mu * 3.0 * state[0] * state[2] * r_i5

    acc_dot[1, 0] = acc_dot[0, 1]
    acc_dot[1, 1] = mu * (3.0 * state[1] * state[1] * r_i5 - 1 * r_i3)
    acc_dot[1, 2] = mu * 3.0 * state[1] * state[2] * r_i5

    acc_dot[2, 0] = acc_dot[0, 2]
    acc_dot[2, 1] = acc_dot[1, 2]
    acc_dot[2, 2] = mu * (3.0 * state[2] * state[2] * r_i5 - 1 * r_i3)
    A_stm[3:, :3] = acc_dot

    return A_stm


@jit('float64[::1](float64[::1], float64, boolean)', nopython=True, cache=True, nogil=True)
def _ode_grav(state, mu, with_stm):
    """Equations of motion in inertial frame for the 6-dimensional state.

    Parameters
    ----------
    state : np.ndarray
        A 6-dimensional or 42-dimensional state vector.
    mu : float
        R2BP gravitational parameter.

    Returns
    -------
    dot_state : np.ndarray
        Set of 6 first-order ODEs of the 6-dim state.

    """
    state_dot = np.empty(6)  # preallocate array for state derivatives
    state_dot[0:3] = state[3:6]
    state_dot[3:6] = _acceleration_grav(state, mu)
    if with_stm:
        state_dot = np.hstack((state_dot, np.zeros(36)))
        A_stm = _variational_matrix_grav(state, mu)
        state_dot[6:42] = A_stm.dot(state[6:42].reshape(6, 6)).flatten()
    return state_dot


class R2BP(DynamicalEnvironment):
    """Restricted 2 Body Problem (R2BP) class.

    This class computes the parameters and implements the equations of motion of a R2BP system. Its
    attributes must have the same names as the ODE functions input parameters.
    """

    def __init__(self, attractor: Primary, perturbations=None,
                 frame: Frame=None, adim_t=1.0, adim_l=1.0):
        """
        Parameters
        ----------
        attractor : Primary
            Main attractor of the system.
        perturbations : iterable of SHModel, optional (placeholder)
            List of spherical harmonic models to be used for the perturbations.
        frame : Frame, optional
            Reference frame of the system, by default Inertial.
        adim_t : float, optional
            Characteristic time for adimensionalization, by default 1.0
        adim_l : float, optional
            Characteristic length for adimensionalization, by default 1.0
        """
        if frame is None:
            frame = self.AvailableFrames.INERTIAL

        super().__init__((attractor, ), frame, perturbations, adim_t, adim_l)

        self._mu = self.bodies[0].GM / self.adim_t**2 * self.adim_l

    @property
    def mu(self):
        """Gravitational parameter of the attractor."""
        return self._mu

    def ode(self, _, s, with_stm=False):
        # TODO Here sum the perturbations
        state_dot_tot = _ode_grav(s, self.mu, with_stm)
        return state_dot_tot

    @dataclass
    class AvailableFrames:
        """Class that collects the possible reference AvailableFrames for the environment. """
        INERTIAL = SelectFrame.INERTIAL
        # Add here other possible reference AvailableFrames
