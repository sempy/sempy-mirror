import datetime

import numpy as np
from dataclasses import dataclass
from cffi import FFI
from numba import jit, prange
from numba.core.typing.cffi_utils import register_module
from typing import Union, Iterable
import spiceypy as sp

try:  # import CSPICE functions from CFFI wrapper
    from src.utils.libs.cspice_wrapper import lib
    from src.utils.libs import cspice_wrapper
    register_module(cspice_wrapper)
    spkgps = lib.spkgps_c
    spkgeo = lib.spkgeo_c
except ImportError:  # CFFI wrapper not found, import CSPICE functions from loaded DLL
    cspice_wrapper = lib = None
    from src.utils.cspice_lib_loader import spkgps, spkgeo

ffi = FFI()  # FFI object to pass pointers to numpy array from within JIT functions

from src.dynamics.dynamical_environment import DynamicalEnvironment
from src.coc.frames import Frame, SelectFrame
from src.init.primary import Primary

@jit('float64[:, ::1](float64[::1], float64)', nopython=True, nogil=True, cache=True, fastmath=True)
def _gravity_gradient(r_b, gm_b):
    """Computes the partials of the gravitational acceleration due to a given body `b` storing
    the results in the input matrix `grav_grad`.

    Parameters
    ----------
    r_b : np.ndarray
        Relative position w.r.t. the body `b`.
    gm_b : float
        Standard gravitational parameter of body `b`.

    Returns
    -------
    grav_grad : np.ndarray
        Gravity gradient components as a 3x3 matrix.
    """

    r_b2 = r_b.dot(r_b)
    fac = 3.0 * gm_b * r_b2 ** -2.5

    grav_grad = np.empty((3, 3))
    grav_grad.ravel()[0:9:4] = gm_b * r_b2 ** -1.5 * (3.0 * r_b * r_b / r_b2 - 1.0)
    grav_grad[0, 1] = grav_grad[1, 0] = fac * r_b[0] * r_b[1]  # d2U/dxdy, d2U/dydx
    grav_grad[0, 2] = grav_grad[2, 0] = fac * r_b[0] * r_b[2]  # d2U/dxdz, d2U/dzdx
    grav_grad[1, 2] = grav_grad[2, 1] = fac * r_b[1] * r_b[2]  # d2U/dydz, d2U/dydz

    return grav_grad


@jit('float64[::1](float64, float64[::1], float64, float64[::1], int32[::1], float64[:, ::1], int8[::1], float64, float64)',
     nopython=True, nogil=True, fastmath=True)
def _acceleration_grav(t, state, et0, gms, body_ids, body_states, frame_enc, adim_t, adim_l_inv):
    """Equations of motion in the ephemeris force model written as set of first-order Ordinary
    Differential Equations. The implemented ODEs describe the motion of a particle under the gravitational attraction of
    ``n`` celestial bodies modelled as point masses. The dynamics is expressed in an inertial reference Frame, i.e. J2000, whose origin coincides
    by convention with the first celestial body in `body_ids`.
    Positions of the other ``n-1`` bodies w.r.t. the Frame's origin are obtained from ephemeris
    data extrapolated using the SPICE routine `spkgps`.

    The following notation is adopted:
        1) subscript `sc` denotes the particle of interest, i.e. the spacecraft.
        2) subscript `o` denotes the first celestial body at the origin of the inertial Frame.
        3) subscript `b` denotes all other ``n-1`` bodies in turn.

    Parameters
    ----------
    t : float
        Current time with respect to et0 (s or adim).
    state : np.ndarray
        Current state (km, km/s or adim).
    et0 : float
        Absolute epoch time (s).
    gms : np.ndarray
        Standard gravitational parameters for the ``n`` considered bodies (km3/s2).
    body_ids : np.ndarray
        NAIF IDs for the ``n`` considered bodies.
    frame_enc : np.ndarray
        Name of the inertial reference Frame, i.e. J2000, encoded as Numpy array of 8-bit integers.
    adim_t : float
        Characteristic time for adimensionalization.
    adim_l_inv : float
        Reciprocal of the characteristic length for adimensionalization.

    Returns
    -------
    state_dot : np.ndarray
        First time derivative of `state` (km/s, km/s2).

    """

    et = et0 + t * adim_t  # current time (s)
    r_osc = state[0:3]  # position of the spacecraft w.r.t. the origin

    # acceleration due to the origin
    acc_grav = - gms[0] * (r_osc * r_osc.dot(r_osc) ** -1.5)

    # Initialise arrays for relative state
    lt_ob_init = np.empty((1,))  # light time from body b to the origin

    # loop over all bodies except the one at the origin
    for j in prange(1, gms.size):
        # Retrieve relative position of body b w.r.t. the origin and of spacecraft w.r.t. body b
        spkgeo(body_ids[j], et, ffi.from_buffer(frame_enc), body_ids[0],
               ffi.from_buffer(body_states[j]), ffi.from_buffer(lt_ob_init))
        # state of body b w.r.t. the origin [km]
        r_ob = body_states[j, :3] * adim_l_inv  # relative position of body b w.r.t. the origin
        r_bsc = r_osc - r_ob  # relative position of spacecraft w.r.t. body b

        # Acceleration
        acc_grav += - gms[j] * (r_bsc * r_bsc.dot(r_bsc) ** -1.5 +
                                      r_ob * r_ob.dot(r_ob) ** -1.5)
    return acc_grav

@jit('float64[:, ::1](float64[::1], float64[::1], float64[:, ::1], float64)',
     nopython=True, nogil=True, fastmath=True)
def _variational_matrix_grav(state, gms, body_states, adim_l_inv):
    """Equations of motion in the ephemeris force model including the STM variational equations.
    """

    r_osc = state[0:3]  # position of the spacecraft w.r.t. the origin

    A_stm = np.zeros((6, 6))  # matrix A of the partials of the ODEs w.r.t. the state
    A_stm[0, 3] = A_stm[1, 4] = A_stm[
        2, 5] = 1.0  # NE 3x3 sub-matrix of A equal to identity 3x3

    # loop over all bodies
    for j in prange(0, gms.size):
        # Update relative position of body b w.r.t. the origin and of spacecraft w.r.t. body b
        r_bsc = r_osc - body_states[j, :3]*adim_l_inv  # relative position of spacecraft w.r.t. body b

        # Gravity gradient
        grav_grad_bsc = _gravity_gradient(r_bsc, gms[j])  # gravity gradient (body b)
        A_stm[3:6, 0:3] += grav_grad_bsc  # update matrix A (body b)

    return A_stm

@jit('float64[::1](float64[::1], float64[:, ::1], float64[::1], float64[:, ::1], float64, float64)',
     nopython=True, nogil=True, fastmath=True)
def _epoch_partials_grav(state, A_stm, gms, body_states, adim_t, adim_l_inv):
    """Equations of motion in the ephemeris force model including the STM variational equations and
    the epoch partials.
    Epoch partials are described in [Pavlak, 2013] as:

    d/dt (dx/dtau) = A * (dx/dtau) + sum_i (df/dr_1i) * v_1i with i = 2, ..., n-1

    T. A. Pavlak, ‘Trajectory Design and Orbit Maintenance Strategies in Multi-Body Dynamical
    Regimes’, PhD Thesis, School of Aeronautics and Astronautics, Purdue University, West Lafayette,
    Indiana, 2013, equation (3.41), page 40.
    """

    r_osc = state[0:3]  # position of the spacecraft w.r.t. the origin

    epoch_partials = np.zeros((6,))  # epoch partials
    df_dr = np.zeros((6, 3))  # partials of the ODEs w.r.t. the position of body b

    # loop over all bodies except the one at the origin
    for j in prange(1, gms.size):
        # Update relative position of body b w.r.t. the origin and of spacecraft w.r.t. body b
        r_ob = body_states[j, 0:3] * adim_l_inv
        v_ob = body_states[j, 3:6] * adim_l_inv * adim_t
        r_bsc = r_osc - r_ob

        # Gravity gradient
        grav_grad_bsc = _gravity_gradient(r_bsc, gms[j])  # gravity gradient (body b)
        grav_grad_ob = _gravity_gradient(r_ob, gms[j])  # gravity gradient (body b)

        df_dr[3:6, :] = grav_grad_ob - grav_grad_bsc
        epoch_partials += df_dr.dot(v_ob)

    # Epoch partials
    epoch_partials += A_stm.dot(state[42:48])

    return epoch_partials

@jit('float64[::1](float64, float64[::1], float64, float64[::1], int32[::1], float64[:, ::1], int8[::1], float64, float64, boolean, boolean)',
     nopython=True, nogil=True, fastmath=True)
def _ode_grav(t, state, start_et, gms, body_ids, body_states, frame_enc, adim_t, adim_l_inv, with_stm, with_epoch_partials):
    # Mind that epoch partials cannot be True if STM is False
    if with_epoch_partials and not with_stm:
        raise ValueError('Epoch partials cannot be computed if STM is not computed.')
    state_dot = np.zeros(6)  # state derivative
    acc_grav = _acceleration_grav(t, state, start_et, gms, body_ids,
                                  body_states,
                                  frame_enc,
                                  adim_t, adim_l_inv)
    # This call updates the body_states attribute with the current state of the bodies.

    state_dot[:3] = state[3:6]  # velocity
    state_dot[3:] = acc_grav  # acceleration

    if with_stm:
        # Increase the size of the state vector to include the STM
        state_dot = np.hstack((state_dot, np.zeros((36,))))
        A_stm = _variational_matrix_grav(state, gms, body_states,
                                         adim_l_inv)

        state_dot[6:42] = A_stm.dot(state[6:42].reshape(6, 6)).ravel()  # STM
        if with_epoch_partials:
            # Increase the size of the state vector to include the epoch partials
            state_dot = np.hstack((state_dot, np.zeros((6,))))
            state_dot[42:] = _epoch_partials_grav(state, A_stm, gms,
                                                  body_states, adim_t,
                                                  adim_l_inv)
    return state_dot


class NBP(DynamicalEnvironment):
    """N-body problem (NBP) class.
    This class computes the parameters and implements the equations of motion of a CR3BP system. Its
    attributes must have the same names as the ODE functions input parameters.
    """
    kind = 'NBP'

    def __init__(self, bodies: Iterable[Primary],
                 center: Union[int, str, Primary] = None,
                 perturbations = None,
                 start_date: Union[str, datetime.datetime]='2000 JAN 01 12:00:00.000 TDB',
                 frame: Frame = None,
                 adim_t=1.0,
                 adim_l=1.0):
        """
        Parameters
        ----------
        center : int or str
            NAIF ID or name of the center body.
        bodies : list or tuple
            Collection of Primary objects
        perturbations : list or tuple, optional
            List of perturbations to be considered, by default None
        start_date : str, datetime, optional
            Start epoch of the simulation, by default the SPICE J2000 epoch (0 seconds past J2000 midday)
        frame : Frame, optional
            Reference frame of the ODEs, by default J2000
        adim_t : float, optional
            Characteristic time for adimensionalization, by default 1.0
        adim_l : float, optional
            Characteristic length for adimensionalization, by default 1.0
        """

        if frame is None:
            frame = self.AvailableFrames.J2000
        if center is None:
            center = bodies[0]

        super().__init__(bodies, frame, perturbations, adim_t, adim_l)

        # Set the integration center
        self.integration_center = center

        # Set the start epoch
        self.start_date = start_date

        # Initialise the SPICE states of the bodies
        self._body_states = np.zeros((len(self.bodies), 6))

    @property
    def _gms(self):
        return np.array([b.GM for b in self.bodies]) * (self.adim_t ** 2) * (self.adim_l ** -3)

    @property
    def _body_ids(self):
        return np.array([b.naif_id for b in self.bodies]).astype(np.int32)

    @property
    def _adim_l_inv(self):
        return 1 / self.adim_l

    @property
    def start_date(self):
        """Start epoch of the simulation, as a datetime object."""
        return self._start_date

    @start_date.setter
    def start_date(self, date: Union[str, datetime.datetime]):
        # Verify the accepted input types (string or datetime)
        if type(date) is datetime.datetime:
            et = sp.datetime2et(date)
        elif type(date) is str:
            et = sp.str2et(date)
            date = sp.et2datetime(et)
        else:
            raise TypeError('The start date must be a string or a datetime object.')
        self._start_date, self._start_et = date, et

    @property
    def start_et(self):
        """Start epoch of the simulation, as ephemeris seconds past J2000 at 12:00."""
        return self._start_et

    @start_et.setter
    def start_et(self, et: float):
        # ephemeris seconds past J2000 at 12:00
        self._start_et = et
        self._start_date = sp.et2datetime(et)

    @property
    def integration_center(self):
        """NAIF ID of the integration center."""
        return self._integration_center

    @integration_center.setter
    def integration_center(self, center: Union[int, Primary, str]):
        # Check the type of the center body
        if isinstance(center, int):  # NAIF ID
            center_name = sp.bodc2n(center)
        elif isinstance(center, str):  # Name
            center_name = center.upper()
        elif isinstance(center, Primary):  # Primary object
            center_name = center.name
        else:
            raise TypeError('Center body must be a NAIF ID, a name or a Primary object')

        body_names = [body.name for body in self.bodies]
        if center_name not in body_names:  # See if center is in the list of bodies
            raise ValueError('Center body must be in the list of bodies')
        self._integration_center = center_name
        center_idx = body_names.index(center_name)
        # Resort bodies to have the integration center as the first element
        self._bodies = (self.bodies[center_idx],) + \
                      tuple(self.bodies[:center_idx]) + tuple(self.bodies[center_idx + 1:])

    def ode(self, t, s, with_stm=False, with_epoch_partials=False):
        # TODO Here sum the perturbations
        state_dot_tot = _ode_grav(t, s, self.start_et, self._gms, self._body_ids, self._body_states,
                         self.frame.name_enc, self.adim_t, self._adim_l_inv, with_stm,
                            with_epoch_partials)
        return state_dot_tot

    @dataclass
    class AvailableFrames:
        """Class that collects the possible reference frames for the environment. """
        EJ2000 = SelectFrame.EJ2000
        J2000 = SelectFrame.J2000
