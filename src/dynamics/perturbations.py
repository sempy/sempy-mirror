import numpy as np

class Perturbation:
	kind = None
	acc = None
	A_mat = None
	epoch_partials = None
	def __init__(self, name):
		self.name = name

class BodyFixedPerturbation(Perturbation):
	kind = 'body_fixed'

	def __init__(self, name, body, body_data):
		super().__init__(name)
		self.body = body
		self.body_data = body_data


class SRP(Perturbation):
	def __init__(self,):
		super().__init__('SRP')

	def acc(self, t, state, primary_states, sc_data):
		"""Compute the acceleration due to solar radiation pressure."""
		return np.zeros(3)

class Drag(BodyFixedPerturbation):
	def __init__(self, body, body_data):
		super().__init__(f'AtmosphericDrag', body, body_data)

	def acc(self, t, state, primary_states, sc_data):
		"""Compute the acceleration due to drag."""
		return np.zeros(3)


class SphericalHarmonics(BodyFixedPerturbation):
	def __init__(self, body, body_data):
		super().__init__('SphericalHarmonics', body, body_data)

	def acc(self, t, state, primary_states):
		"""Compute the acceleration due to spherical harmonics."""
		return np.zeros(3)


