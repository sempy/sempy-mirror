# pylint: disable=too-few-public-methods
"""
Propagator class and sub-classes to integrate the equations of motion in different force models
and reference frames.

"""

import numpy as np
import scipy.integrate as sci

from typing import Iterable, Union, Callable, Sized
from dataclasses import dataclass

import src.init.defaults as dft
from src.dynamics.dynamical_environment import DynamicalEnvironment


def _append_equation_to_ode(ode, f_add_list):
    """Add variables to the equations of motion."""
    def augmented_ode(t, s):
        return np.append(ode(t, s), [f(t, s) for f in f_add_list])
    return augmented_ode


def _get_fun_and_state0_with_parameters(path_param: Union[dict, Iterable[dict]], fun: callable,
                                        state0: np.ndarray):
    if isinstance(path_param, dict):
        path_param = [path_param]
    param_fun_list = []
    # If given, add the initial value of the parameter to the initial state
    for p in path_param:
        if 'initial_value' in p:
            state0 = np.append(state0, p['initial_value'])
        else:
            state0 = np.append(state0, 0.0)
        param_fun_list.append(p['function'])
    fun = _append_equation_to_ode(fun, param_fun_list)
    return fun, state0, path_param

class Propagator:
    """Propagator class.

    It manages settings such as integration method, relative and absolute tolerances etc.

    `Propagator` class is a wrapper of Scipy `solve_ivp()` to numerically propagate a given set of
    Ordinary Differential Equations (ODEs) from user-selected initial conditions and time span.
    If events are passed to `solve_ivp()`, it provides root finding algorithms to accurately
    determine the time instant and corresponding state at which the event occurs.

    Attributes
    ----------



    """

    def __init__(self,
                 method: str = dft.method,
                 rel_tol: float = dft.rtol, abs_tol: float = dft.atol,
                 max_internal_steps: int = dft.max_internal_steps,
                 **scipy_options):
        """
        Parameters
        ----------
        method : str, optional
            Integration method.
        rel_tol : float, optional
            Relative tolerance.
        abs_tol : float, optional
            Absolute tolerance.
        max_internal_steps : int, optional
            Maximum number of internal steps.
        **scipy_options
            Additional options to be passed to `scipy.integrate.solve_ivp()`.
        """

        # Integrator settings
        self._method = method
        self._rtol = rel_tol
        self._atol = abs_tol
        self._max_internal_steps = max_internal_steps
        self._scipy_options = scipy_options

    @property
    def method(self):
        return self._method

    @property
    def rtol(self):
        return self._rtol

    @property
    def atol(self):
        return self._atol

    @property
    def max_internal_steps(self):
        return self._max_internal_steps

    @property
    def scipy_options(self):
        return self._scipy_options

    @property
    def ode(self):
        return self._ode

    def _integrate(self, ode, t_span, y0, events, n_eval):
        # Propagate using solve_ivp
        if len(t_span) > 2:  # solution on user-provided time grid
            sol = sci.solve_ivp(fun=ode,
                                t_span=[t_span[0], t_span[-1]],
                                y0=y0,
                                t_eval=t_span,
                                events=events,
                                method=self.method, rtol=self.rtol, atol=self.atol,
                                **self.scipy_options)
        elif n_eval is not None:  # solution on uniformly spaced time grid
            t_eval = np.linspace(t_span[0], t_span[1], n_eval)
            sol = sci.solve_ivp(fun=ode,
                                t_span=t_span,
                                y0=y0,
                                t_eval=t_eval,
                                events=events,
                                method=self.method, rtol=self.rtol, atol=self.atol,
                                **self.scipy_options)

        else:  # solution only at time interval boundaries
            sol = sci.solve_ivp(fun=ode,
                                t_span=t_span,
                                y0=y0,
                                t_eval=t_span,
                                events=events,
                                method=self.method, rtol=self.rtol, atol=self.atol,
                                **self.scipy_options)
        return sol

    def _integrate_with_centre_switch(self):
        """This method is used to integrate the equations of motion in the NBP, where the integration
        centre can be switched automatically depending on the position of the spacecraft with respect
        to the current centre and its sphere of Influence. An event is triggered at its passage, and
        the next body is selected as integration centre. This method might be heavy on performance.
        """
        return self

    def __call__(self,
                 environment: DynamicalEnvironment,
                 t_span: Union[np.ndarray, Iterable],
                 state0: np.ndarray,
                 events: Union[Iterable[Callable], Callable] = None,
                 n_eval: int = None,
                 switch_centre: bool = False,
                 path_param: Union[dict, Iterable[dict]] = None,
                 with_stm = False,
                 **ode_kwargs):
        """Integrate the chosen set of ODEs.

        Parameters
        ----------
        environment : DynamicalEnvironment
            Instance of the `DynamicalEnvironment` class.
        t_span : Sized
            Time interval over which the integration is performed.
                - If `n_eval` is ``None`` and `t_span` is a two elements iterable, i.e.
                  ``[t0, tf]``, only the initial and final states will be returned.
                - If `n_eval` is a positive integer and `t_span` is a two elements iterable,
                  i.e. ``[t0, tf]``, the solution will be returned in a uniformly spaced time grid
                  computed as ``numpy.linspace(t0, tf, n_eval)``.
                - If `t_span` has more than two elements, the solution will be returned on the time
                  grid defined by `t_span` itself. In case n_eval is not None, it will be
                  ignored.

        state0 : np.ndarray
            Initial state.
        events : Callable or Iterable[Callable], optional
            Events to be tracked. They can be either methods of Propagator.Events or user-defined
            functions with interface ``f(t, s) -> float``.
        n_eval : int, optional
            Number of points equally spaced in time in which the solution is returned.
        path_fun : Callable or Iterable of Callables, optional
            Function (or iterable of functions) to be addded to the equations of motion (available
            defaults are collected in the Propagator.AdditionalParameter class). If user-defined
            functions are passed, they must have the following interface: ``f(t, s) -> float``.
            These are parameters that do not influence the dynamics of the system, but are useful to
            compute quantities such as energies, angular momenta, etc. Equations describe
            dot(lambda), where lambda is the parameter, t the time and s the state vector.

        Returns
        -------
        solution : Solution
            Solution object containing the results of the integration.

        """

        if events is not None and not isinstance(events, Iterable):
            events = [events]

        correct_state_size = self._get_correct_state_size(with_stm = with_stm, **ode_kwargs)
        
        # Assign the equations of motion (gravitation only for now)
        def ode(t, s):
            return environment.ode(t, s[:correct_state_size], with_stm, **ode_kwargs)

        # If given, add the parameter at the end of the complete state vector
        if path_param is not None:
            ode, state0, path_param = _get_fun_and_state0_with_parameters(path_param, ode, state0)
            path_ndim = len(path_param)
        else:
            path_ndim = 0
        state_size = state0.shape[0]

        # Check that the given initial state has the correct dimension
        if state_size != correct_state_size + path_ndim:
            raise ValueError(f"Initial state has wrong dimension."
                             f"Expected {correct_state_size + path_ndim}, got {state_size}.")

        self._ode = ode

        # Integrate
        if switch_centre:
            sol = self._integrate_with_centre_switch()
        else:
            sol = self._integrate(ode, t_span, state0, events, n_eval)

        if events is None:
            t_events = None
            y_events = None
        else:
            t_events = sol.t_events
            y_events = sol.y_events

        return Solution(sol.t, sol.y.T, events, t_events, y_events, path_param)

    @staticmethod
    def _get_correct_state_size(**kwargs):
        """Return the size of the state vector."""
        if kwargs.get('with_stm', False):
            if kwargs.get('with_epoch_partials', False):
                return 48
            else:
                return 42
        else:
            return 6


@dataclass
class PathParameters:
    """Additional parameters to be added to the equations of motion. These are parameters that
    do not influence the dynamics of the system, but are useful to compute quantities such as
    energies, angular momenta, etc. Equations must correspond to dot(lambda) = f(t, s), where
    lambda is the parameter, t the time and s the state vector. If the parameter requires the state
    to have 42 components (the STM is included), an error must be raised if the state has 6
    components."""

    @staticmethod
    def arclength(_, s):
        return np.linalg.norm(s[3:6])

    @staticmethod
    def momentum_integral(_, s):
        return np.dot(s[0:3], s[3:6])

    # Add here other useful parameters


@dataclass
class Events:
    """ Propagation events. This is an experimental class, to aid the user defining events"""

    @staticmethod
    def collision(name: str, pos=(0, 0, 0), rad=0.0):
        """Collision event."""
        # TODO: this is simplified, but if the impact is not with the origin, this will not
        #  work for the NBP case (positions are dependent on the time)
        def collision(_, s):
            return np.linalg.norm(s[:3] - pos) - rad
        collision.terminal = True
        collision.__name__ = name
        return collision


class Solution:
    """Solution class.

    It contains the results of the integration of the ODEs.

    Attributes
    ----------
    t_vec : np.ndarray
        Time vector for all states in `state_vec`.
    state_vec : np.ndarray
        Propagated state vector. Subsequent rows correspond to subsequent times in `t_vec`
        while different columns correspond to different components of the initial state
        (i.e. ``state_vec[:, 0]`` is the x component of the state for all times in `t_vec`
        while ``state_vec[-1, :]`` is the final state at ``t_vec[-1]``).
    t_events : iterable or None
        List of times corresponding to tracked events or None if no events have been tracked.
        If one or more events have been tracked but not found in the provided time span,
        a list containing an empty array is returned.
    state_events : iterable or None
        List of states corresponding to tracked events or None if no events have been tracked.
        If one or more events have been tracked but not found in the provided time span,
        a list containing an empty array is returned.
    tracked_events : iterable or None
        List of events tracked or None if no events have been tracked.
    """

    def __init__(self, t_vec: np.ndarray, state_vec: np.ndarray,
                 events: Iterable = None,
                 t_events: Iterable = None,
                 y_events: Iterable = None,
                 path_param: Iterable = None):

        """Inits Solution class. """

        self.t_vec = t_vec
        self.state_vec = state_vec
        self.t_events = t_events
        self.state_events = y_events

        if events is not None:
            self.tracked_events = [e.__name__ for e in events]
        else:
            self.tracked_events = None

        if path_param is not None:
        #     # Separate the integrated path parameters from the state vector
            self.path_param_vec = state_vec[:, -len(path_param):]
            self.state_vec = state_vec[:, :-len(path_param)]

            if events is not None:
                # Separate the integrated path parameters from the state events
                self.path_param_events = []
                for i, s_e in enumerate(self.state_events):
                    if len(s_e) != 0:
                        self.state_events[i] = s_e[:, :-len(path_param)]
                        self.path_param_events.append(s_e[:, -len(path_param):])
