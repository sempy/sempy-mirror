# pylint: disable = too-few-public-methods
"""
Keplerian Propagator to integrate the spacecraft state in the Restricted Two-Body Problem
framework using Lagrange coefficients and universal variables.

The Propagator class is a convenient wrapper to the pyKep method `propagate_lagrangian`
(http://esa.github.io/pykep/documentation/core.html#pykep.propagate_lagrangian)

"""

import numpy as np
from pykep.core.core import propagate_lagrangian

import src.init.defaults as dft


class R2bpLagrangianPropagator:
    """Propagates the spacecraft state in the Restricted Two-Body Problem using Lagrange
    coefficients and universal variables.

    The class constitutes a convenient wrapper to the pyKep method `propagate_lagrangian`,
    refer to (http://esa.github.io/pykep/documentation/core.html#pykep.propagate_lagrangian) for
    the implementation details.

    Parameters
    ----------
    mu_r2bp : float
        Central body standard gravitational parameter.
    time_steps : int or None, optional
        Number of points equally spaced in time in which the solution is returned. Default defined
        in `src.init.defaults`.

    Attributes
    ----------
    mu_r2bp : float
        Central body standard gravitational parameter.
    time_steps : int or None
        Number of points equally spaced in time in which the solution is returned.

    """

    def __init__(self, mu_r2bp, time_steps=dft.time_steps):
        """Inits R2bpLagrangianPropagator. """

        self.mu_r2bp = mu_r2bp
        self.time_steps = time_steps

    def propagate(self, t_span, state0):
        """Propagates the initial spacecraft state.

        t_span : iterable
            Time interval over which the integration is performed.
            If `time_steps` is ``None`` and `t_span` is a two elements iterable,
            i.e. ``[t0, tf]``, only the initial and final states will be returned.
            If `time_steps` is a positive integer and `t_span` is a two elements iterable,
            i.e. ``[t0, tf]``, the solution will be returned in a uniformly spaced time
            grid computed as ``numpy.linspace(t0, tf, time_steps)``.
            If `t_span` has more than two elements, the solution will be returned on the time grid
            defined by `t_span` itself.
        state0 : ndarray
            Initial state.

        Returns
        -------
        t_vec : ndarray
            Time vector for all states in `state_vec`.
        state_vec : ndarray
            Propagated state vector. Subsequent rows correspond to subsequent times in `t_vec`
            while different columns correspond to different components of the initial state
            (i.e. ``state_vec[:, 0]`` is the x component of the state for all times in `t_vec`
            while ``state_vec[-1, :]`` is the final state at ``t_vec[-1]``).

        """

        if len(t_span) < 2:
            raise Exception('t_span must be at least a two-elements iterable')
        if len(t_span) == 2 and self.time_steps is not None:  # uniformly spaced time grid
            t_span = np.linspace(t_span[0], t_span[1], self.time_steps)

        state_vec = np.empty((t_span.size, 6))  # preallocate state vector
        state_vec[0] = state0  # first state is the initial one
        for i in range(1, len(t_span)):
            r_i, v_i = propagate_lagrangian(r0=state_vec[i - 1, 0:3], v0=state_vec[i - 1, 3:6],
                                            tof=t_span[i] - t_span[i - 1], mu=self.mu_r2bp)
            state_vec[i, 0:3], state_vec[i, 3:6] = r_i, v_i
        return t_span, state_vec
