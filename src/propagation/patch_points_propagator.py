# pylint: disable=too-few-public-methods
"""
PatchPointPropagator class to obtain a continuous trajectory from a series of
differentially corrected patch points.

"""

import itertools
import numpy as np
from multiprocess.pool import Pool

import src.init.defaults as dft
from src.init.cr3bp import Cr3bp
from src.init.ephemeris import Ephemeris
from src.propagation.cr3bp_propagator import Cr3bpSynodicPropagator
from src.propagation.ephemeris_propagator import EphemerisPropagator, EphemerisSHPropagator


class PatchPointsPropagator:
    """PatchPointsPropagator class propagates a series of differentially corrected patch points
    to obtain a continuous trajectory.

    The class takes advantage of the `multiprocess` module to dispatch a series of workers that
    integrate subsequent trajectory segments concurrently.
    Their respective outputs are finally gathered together and concatenated in unique time and
    state vectors describing a continuous trajectory.

    Parameters
    ----------
    model : Cr3bp or Ephemeris
        `Cr3bp` or `Ephemeris` object that defines the environment for the propagation.
    ref : str, optional
        Reference frame name as defined by the NAIF's SPICE Toolkit. If model is a `Cr3bp` object
        this parameter is ignored. Default is ``J2000``.
    obs : Primary or None, optional
        Primary body whose position coincides with the origin of the reference frame in which the
        dynamics is propagated. If model is a `Cr3bp` object this parameter is ignored.
        Default is ``None`` for which the first Primary object in `model` is selected.
    t_c : float, optional
        Characteristic time for adimensionalization.
        If model is a `Cr3bp` object this parameter is ignored.Default is ``1.0``.
    l_c : float, optional
        Characteristic length for adimensionalization.
        If model is a `Cr3bp` object this parameter is ignored. Default is ``1.0``.
    with_stm : bool, optional
        Whether to propagate the 6-dimensional state (False) or the 6-dimensional state
        concatenated with the 42-dimensional State Transition Matrix (True). Default is ``False``.
    rtol : float, optional
        Relative tolerance. Default defined in `src.init.defaults`.
    atol : float, optional
        Absolute tolerance. Default defined in `src.init.defaults`.
    time_steps : int, optional
        Number of points equally spaced in time in which the solution is returned. Default defined
        in `src.init.defaults`.
    max_internal_steps : int, optional
        Maximum number of (internally defined) steps allowed for each integration point. This
        option is used only if `events` is ``None``. Default defined in `src.init.defaults`.

    Attributes
    ----------
    prop : Propagator
        `Propagator` object.
    time_steps : int
        Number of points equally spaced in time in which the solution is returned.

    """

    def __init__(self, model, ref='J2000', obs=None, t_c=1.0, l_c=1.0, with_stm=False,
                 rtol=dft.rtol, atol=dft.atol, time_steps=dft.time_steps,
                 max_internal_steps=dft.max_internal_steps):
        """Inits PatchPointsPropagator class. """

        if isinstance(model, Cr3bp):  # CR3BP dynamics is selected
            self.prop = Cr3bpSynodicPropagator(model.mu, with_stm=with_stm, rtol=rtol, atol=atol,
                                               max_internal_steps=max_internal_steps)
        elif isinstance(model, Ephemeris):  # Ephemeris dynamics is selected
            if model.sh_models is None:  # N-body ephemeris model with point masses only
                self.prop = EphemerisPropagator(model, ref=ref, obs=obs, t_c=t_c, l_c=l_c,
                                                with_stm=with_stm, rtol=rtol, atol=atol,
                                                max_internal_steps=max_internal_steps)
            else:  # N-body ephemeris model with non-uniform gravity field for one or more bodies
                self.prop = EphemerisSHPropagator(model, ref=ref, obs=obs, t_c=t_c, l_c=l_c,
                                                  with_stm=with_stm, rtol=rtol, atol=atol,
                                                  max_internal_steps=max_internal_steps)
        else:
            raise Exception('model must be a Cr3bp or Ephemeris object')
        self.time_steps = time_steps

    def propagate(self, t_patch, state_patch, prune_ends=True, prune_first=0, prune_last=0,
                  procs=dft.procs):
        """Propagates the corrected patch points optionally pruning a given number of initial and
        final nodes.

        Parameters
        ----------
        t_patch : ndarray
            Time vector at patch points.
        state_patch : ndarray
            State vector at patch points.
        prune_ends : bool, optional
            Whether to prune the last propagated point on each segment or not. Default is `True`.
        prune_first : int, optional
            Number of initial patch points to be pruned. Default is zero.
        prune_last : int, optional
            Number of final patch points to be pruned. Default is zero.
        procs : int or None, optional
            Number of spawn worker processes or None for serial implementation.
            Default is the number of physical cores on Linux or None on Windows.

        Returns
        -------
        t_vec : ndarray
            Propagated time vector.
        state_vec : ndarray
            Propagated state vector.

        """

        self.prop.time_steps = self.time_steps + 1 if prune_ends else self.time_steps
        nb_seg = t_patch.size - (prune_first + prune_last + 1)  # number of segments to propagate
        worker_inputs = [(t_patch[prune_first + i:prune_first + 2 + i],
                          state_patch[prune_first + i, :]) for i in range(nb_seg)]

        if procs is not None:  # parallel propagation of trajectory segments
            with Pool(processes=procs) as pool:
                out = pool.starmap(self.prop.propagate, worker_inputs)
        else:  # serial propagation
            out = list(itertools.starmap(self.prop.propagate, worker_inputs))

        # retrieve the time and state vectors for the whole trajectory from worker's outputs
        t_vec = np.concatenate([out[i][0][:self.time_steps] for i in range(nb_seg)])
        state_vec = np.concatenate([out[i][1][:self.time_steps] for i in range(nb_seg)])
        state_end = np.concatenate((state_patch[- prune_last - 1], self.prop.stm0)).reshape(1, 42) \
            if (self.prop.with_stm and state_patch[- prune_last - 1].size == 6) \
            else state_patch[- prune_last - 1].reshape(1, state_patch[- prune_last - 1].size)
        return np.concatenate((t_vec, [t_patch[-(prune_last + 1)]])), \
            np.concatenate((state_vec, state_end))
