# pylint: disable=too-few-public-methods, too-many-locals
"""
Propagator classes to integrate the equations of motion in the N-body ephemeris force model.

"""

import numpy as np

import src.init.defaults as dft
from src.init.constants import STM0
from src.propagation.propagator import Propagator
from src.dynamics.ephemeris_dynamics import eqm_6_ephemeris, eqm_42_ephemeris, eqm_48_ephemeris
from src.dynamics.ephemeris_sh_dynamics import eqm_6_sh_ephemeris, eqm_42_sh_ephemeris


class EphemerisPropagator(Propagator):
    """Propagates the equations of motion in the user-defined ephemeris model and reference frame.

    `EphemerisPropagator` implements methods to easily interface with the Scipy `solve_ivp()`
    and `odeint()` integration routines and it allows to propagate 6-dimensional, 42-dimensional
    and 48-dimensional equations of motions in the given ephemeris model written as a set of
    first-order Ordinary Differential Equations expressed in a user-defined frame.
    If 6-dimensional equations are selected, only the state composed by the 3 components of the
    position and the 3 components of the velocity vector is propagated over the given time span.
    If 42-dimensional equations are selected, the state concatenated with the 36 components of the
    State Transition Matrix (STM) are propagated.
    If 48-dimensional equations are selected, the state concatenated with the 36 components of the
    State Transition Matrix (STM) and the 6 components of the partials of the states w.r.t. epoch
    time are propagated.

    Parameters
    ----------
    eph : Ephemeris
        `Ephemeris` object.
    ref : str, optional
        Reference frame name as defined by the NAIF's SPICE Toolkit. Default is ``J2000``.
    obs : Primary or None, optional
        Primary body whose position coincides with the origin of the reference frame in which the
        dynamics is propagated. Default is ``None`` for which the first Primary object in `eph` is
        selected.
    t_c : float, optional
        Characteristic time for adimensionalization. Default is ``1.0``.
    l_c : float, optional
        Characteristic length for adimensionalization. Default is ``1.0``.
    with_stm : bool, optional
        ``False`` to propagate the 6-dimensional state only, ``True`` to propagate the
        6-dimensional state concatenated with the 36-dimensional STM.
        This parameter is ignored if `with_epoch_partials` is set to ``True``.
        Default is ``False``.
    with_epoch_partials : bool, optional
        ``False`` to propagate the 6-dimensional or 42-dimensional equations, ``True`` to
        propagate the 6-dimensional state concatenated with the 36-dimensional STM and the
        6-dimensional partials of the state w.r.t. epoch time.
        This parameter takes precedence over `with_stm`. Default is ``False``.
    method : str, optional
        Integration method to use. Default defined in `src.init.defaults`.
    rtol : float, optional
        Relative tolerance. Default defined in `src.init.defaults`.
    atol : float, optional
        Absolute tolerance. Default defined in `src.init.defaults`.
    time_steps : int, optional
        Number of points equally spaced in time in which the solution is returned. Default defined
        in `src.init.defaults`.
    max_internal_steps : int, optional
        Maximum number of (internally defined) steps allowed for each integration point. This
        option is used only if `events` is ``None``. Default defined in `src.init.defaults`.

    Attributes
    ----------
    eph : Ephemeris
        `Ephemeris` object.
    ref : str
        Reference frame name as defined by the NAIF's SPICE Toolkit.
    stm0 : ndarray or None
        Flattened STM0.
    stm0_dxdtau0 : ndarray or None
        Flattened STM0 concatenated with the initial conditions for the partials of the state
        w.r.t. epoch time, i.e. ``numpy.zeros(6)``.
    odes : function
        Equations of motions to be integrated, 6-dim, 42-dim (state and STM) or 48-dim.
    with_stm : bool
        ``False`` to propagate the 6-dimensional or 48-dimensional equations,
        ``True`` to propagate the 42-dimensional ones.
    with_epoch_partials : bool
        ``False`` to propagate the 6-dimensional or 42-dimensional equations,
        ``True`` to propagate the 48-dimensional ones.
    method : str
        Integration method to use.
    rtol : float
        Relative tolerance.
    atol : float, optional
        Absolute tolerance.
    time_steps : int
        Number of points equally spaced in time in which the solution is returned.
    max_internal_steps : int
        Maximum number of (internally defined) steps allowed for each integration point. This
        option is used only if `events` is ``None``.

    Warnings
    --------
    If `t_c` and `l_c` are both equal to ``1.0``, the equations of motion are integrated in
    dimensional units of seconds and kilometers for time and length respectively. The integration
    time span `t_span` and the initial state `state0` taken as input by
    `EphemerisPropagator.propagate()` are assumed to be in consistent units,
    i.e `s` for `t_span` and `km`, `km/s` for `state0`.
    If at least one parameter between `t_c` and `l_c` differs from ``1.0``, the equations of
    motion are scaled by the corresponding input values and integrated in non-dimensional units.
    In this case `t_span` and `state0` are assumed to be non-dimensional and consistent with the
    adimensionalization parameters previously defined.
    The outputs vectors `state_vec`, `t_vec`, `state_event`, `t_event` are always in the same units
    as `state0` and `t_span`.

    """

    def __init__(self, eph, ref='J2000', obs=None, t_c=1.0, l_c=1.0, with_stm=False,
                 with_epoch_partials=False, method=dft.method, rtol=dft.rtol, atol=dft.atol,
                 time_steps=dft.time_steps, max_internal_steps=dft.max_internal_steps):
        """Inits EphemerisPropagator. """

        Propagator.__init__(self, method=method, rtol=rtol, atol=atol, time_steps=time_steps,
                            max_internal_steps=max_internal_steps)

        # encoded reference frame and normalized standard gravitational parameters for all bodies
        ref_encoded, gm_adim = eph.get_integration_params(ref, obs, t_c, l_c)

        self.eph = eph   # Ephemeris object, defines the celestial bodies included in the model
        self.ref = ref  # string representing the selected integration frame (e.g. J2000)

        if with_epoch_partials:
            self.odes = lambda t, state: eqm_48_ephemeris(t, state, gm_adim, self.eph.naif_ids,
                                                          ref_encoded, t_c, 1.0 / l_c)
            self.stm0_dxdtau0 = np.concatenate((STM0.ravel(), np.zeros(6)))
            self.stm0 = None
            with_stm = False
        elif with_stm:
            self.odes = lambda t, state: eqm_42_ephemeris(t, state, gm_adim, self.eph.naif_ids,
                                                          ref_encoded, t_c, 1.0 / l_c)
            self.stm0 = STM0.ravel()
            self.stm0_dxdtau0 = None
        else:
            self.odes = lambda t, state: eqm_6_ephemeris(t, state, gm_adim, self.eph.naif_ids,
                                                         ref_encoded, t_c, 1.0 / l_c)
            self.stm0 = self.stm0_dxdtau0 = None
        self.with_stm = with_stm
        self.with_epoch_partials = with_epoch_partials

    def propagate(self, t_span, state0, events=None):
        """Integrate the chosen set of ODEs.

        Parameters
        ----------
        t_span : iterable
            Time interval over which the integration is performed.
            If `EphemerisPropagator.time_steps` is ``None`` and `t_span` is a two elements
            iterable, i.e. ``[t0, tf]``, only the initial and final states will be returned.
            If `EphemerisPropagator.time_steps` is a positive integer and `t_span` is a two
            elements iterable, i.e. ``[t0, tf]``, the solution will be returned in a uniformly
            spaced time grid computed as ``numpy.linspace(t0, tf, time_steps)``.
            If `t_span` has more than two elements, the solution will be returned on the time grid
            defined by `t_span` itself.
        state0 : ndarray
            Initial state.
            If 6-dimensional equations are selected, `state0` must be a 6-dimensional array of
            initial conditions.
            If 42-dimensional equations are selected and `state0` is 6-dimensional, the last is
            concatenated with the default initial STM (6x6 identity matrix) before propagation.
            If 42-dimensional equations are selected and `state0` is 42-dimensional, an
            initial state already concatenated with the initial STM is assumed and set as initial
            condition for the propagation algorithm.
            If 48-dimensional equations are selected and `state0` is 6-dimensional, the last is
            concatenated with the default initial STM (6x6 identity matrix) and the initial values
            for the epoch partials before propagation.
            If 48-dimensional equations are selected and `state0` is 48-dimensional, an
            initial state already concatenated with the initial STM and the initial values for the
            epoch partials is assumed and set as initial condition for the propagation algorithm.
        events : function or iterable
            Event or events to track.

        Returns
        -------
        t_vec : ndarray
            Time vector for all states in `state_vec`.
        state_vec : ndarray
            Propagated state vector. Subsequent rows correspond to subsequent times in `t_vec`
            while different columns correspond to different components of the initial state
            (i.e. ``state_vec[:, 0]`` is the x component of the state for all times in `t_vec`
            while ``state_vec[-1, :]`` is the final state at ``t_vec[-1]``).
        t_event : iterable or None
            List of times corresponding to tracked events or None if no events have been tracked.
            If one or more events have been tracked but not found in the provided time span,
            a list containing an empty array is returned.
        state_event : iterable or None
            List of states corresponding to tracked events or None if no events have been tracked.
            If one or more events have been tracked but not found in the provided time span,
            a list containing an empty array is returned.

        Warnings
        --------
        `state0` and `t_span` must be in units consistent with the adimensionalization parameters
        `t_c` and `l_c` defined during the initialization of the `EphemerisPropagator` object.
        No further scaling will be performed by this routine.

        """

        if self.with_stm and state0.size == 6:  # 42-dim equations, no STM
            state0 = np.concatenate((state0, self.stm0))
        if self.with_epoch_partials and state0.size == 6:  # 48-dim equations, no STM and dx_dtau
            state0 = np.concatenate((state0, self.stm0_dxdtau0))
        return Propagator.propagate(self, t_span, state0, events)


class EphemerisSHPropagator(Propagator):
    """Propagates the equations of motion in the user-defined ephemeris model and reference frame.

    The dynamical model takes into account the gravitational attraction of a given number of
    celestial bodies modelled as point masses and perturbative terms due to the non-uniform gravity
    field of one or more bodies computed with spherical harmonics series truncated at a predefined
    degree and order.

    `EphemerisSHPropagator` implements methods to easily interface with the Scipy `solve_ivp()`
    and `odeint()` integration routines and it allows to propagate 6-dimensional, 42-dimensional
    and 48-dimensional equations of motions in the given ephemeris model written as a set of
    first-order Ordinary Differential Equations expressed in a user-defined frame.
    If 6-dimensional equations are selected, only the state composed by the 3 components of the
    position and the 3 components of the velocity vector is propagated over the given time span.
    If 42-dimensional equations are selected, the state concatenated with the 36 components of the
    State Transition Matrix (STM) are propagated.
    If 48-dimensional equations are selected, the state concatenated with the 36 components of the
    State Transition Matrix (STM) and the 6 components of the partials of the states w.r.t. epoch
    time are propagated.

    Parameters
    ----------
    eph : Ephemeris
        `Ephemeris` object.
    ref : str, optional
        Reference frame name as defined by the NAIF's SPICE Toolkit. Default is ``J2000``.
    obs : Primary or None, optional
        Primary body whose position coincides with the origin of the reference frame in which the
        dynamics is propagated. Default is ``None`` for which the first Primary object in `eph` is
        selected.
    t_c : float, optional
        Characteristic time for adimensionalization. Default is ``1.0``.
    l_c : float, optional
        Characteristic length for adimensionalization. Default is ``1.0``.
    with_stm : bool, optional
        ``False`` to propagate the 6-dimensional state only, ``True`` to propagate the
        6-dimensional state concatenated with the 36-dimensional STM.
        This parameter is ignored if `with_epoch_partials` is set to ``True``.
        Default is ``False``.
    with_epoch_partials : bool, optional
        ``False`` to propagate the 6-dimensional or 42-dimensional equations, ``True`` to
        propagate the 6-dimensional state concatenated with the 36-dimensional STM and the
        6-dimensional partials of the state w.r.t. epoch time.
        This parameter takes precedence over `with_stm`. Default is ``False``.
    method : str, optional
        Integration method to use. Default defined in `src.init.defaults`.
    rtol : float, optional
        Relative tolerance. Default defined in `src.init.defaults`.
    atol : float, optional
        Absolute tolerance. Default defined in `src.init.defaults`.
    time_steps : int, optional
        Number of points equally spaced in time in which the solution is returned. Default defined
        in `src.init.defaults`.
    max_internal_steps : int, optional
        Maximum number of (internally defined) steps allowed for each integration point. This
        option is used only if `events` is ``None``. Default defined in `src.init.defaults`.

    Attributes
    ----------
    eph : Ephemeris
        `Ephemeris` object.
    ref : str
        Reference frame name as defined by the NAIF's SPICE Toolkit.
    stm0 : ndarray or None
        Flattened STM0.
    stm0_dxdtau0 : ndarray or None
        Flattened STM0 concatenated with the initial conditions for the partials of the state
        w.r.t. epoch time, i.e. ``numpy.zeros(6)``.
    odes : function
        Equations of motions to be integrated, 6-dim, 42-dim (state and STM) or 48-dim.
    with_stm : bool
        ``False`` to propagate the 6-dimensional or 48-dimensional equations,
        ``True`` to propagate the 42-dimensional ones.
    with_epoch_partials : bool
        ``False`` to propagate the 6-dimensional or 42-dimensional equations,
        ``True`` to propagate the 48-dimensional ones.
    method : str
        Integration method to use.
    rtol : float
        Relative tolerance.
    atol : float, optional
        Absolute tolerance.
    time_steps : int
        Number of points equally spaced in time in which the solution is returned.
    max_internal_steps : int
        Maximum number of (internally defined) steps allowed for each integration point. This
        option is used only if `events` is ``None``.

    Warnings
    --------
    If `t_c` and `l_c` are both equal to ``1.0``, the equations of motion are integrated in
    dimensional units of seconds and kilometers for time and length respectively. The integration
    time span `t_span` and the initial state `state0` taken as input by
    `EphemerisPropagator.propagate()` are assumed to be in consistent units,
    i.e `s` for `t_span` and `km`, `km/s` for `state0`.
    If at least one parameter between `t_c` and `l_c` differs from ``1.0``, the equations of
    motion are scaled by the corresponding input values and integrated in non-dimensional units.
    In this case `t_span` and `state0` are assumed to be non-dimensional and consistent with the
    adimensionalization parameters previously defined.
    The outputs vectors `state_vec`, `t_vec`, `state_event`, `t_event` are always in the same units
    as `state0` and `t_span`.

    """

    def __init__(self, eph, ref='J2000', obs=None, t_c=1.0, l_c=1.0, with_stm=False,
                 with_epoch_partials=False, method=dft.method, rtol=dft.rtol, atol=dft.atol,
                 time_steps=dft.time_steps, max_internal_steps=dft.max_internal_steps):
        """Inits EphemerisSHPropagator. """

        Propagator.__init__(self, method=method, rtol=rtol, atol=atol, time_steps=time_steps,
                            max_internal_steps=max_internal_steps)

        # encoded integration frame and normalized standard gravitational parameters for all bodies
        int_ref, gm_adim = eph.get_integration_params(ref, obs, t_c, l_c)

        # normalized reference radii, spherical harmonics coefficients and encoded body-fixed
        # frames for the bodies with an associated non-uniform gravity field model
        r_adim, bf_ref, z_nm, nm_max = eph.get_sh_integration_params(l_c)

        self.eph = eph   # Ephemeris object, defines the celestial bodies included in the model
        self.ref = ref  # string representing the selected integration frame (e.g. J2000)

        if with_epoch_partials:
            raise NotImplementedError('ephemeris propagator with non-uniform gravity field model'
                                      'and 48-dimensional equations not implemented yet')
        if with_stm:  # change back to elif once 48-dim equations are implemented
            self.odes = lambda t, state: eqm_42_sh_ephemeris(t, state, gm_adim, self.eph.naif_ids,
                                                             int_ref, t_c, 1.0 / l_c, r_adim,
                                                             bf_ref, z_nm, nm_max)
            self.stm0 = STM0.ravel()
            self.stm0_dxdtau0 = None
        else:
            self.odes = lambda t, state: eqm_6_sh_ephemeris(t, state, gm_adim, self.eph.naif_ids,
                                                            int_ref, t_c, 1.0 / l_c, r_adim,
                                                            bf_ref, z_nm, nm_max)
            self.stm0 = self.stm0_dxdtau0 = None
        self.with_stm = with_stm
        self.with_epoch_partials = with_epoch_partials

    def propagate(self, t_span, state0, events=None):
        """Integrate the chosen set of ODEs.

        Parameters
        ----------
        t_span : iterable
            Time interval over which the integration is performed.
            If `EphemerisPropagator.time_steps` is ``None`` and `t_span` is a two elements
            iterable, i.e. ``[t0, tf]``, only the initial and final states will be returned.
            If `EphemerisPropagator.time_steps` is a positive integer and `t_span` is a two
            elements iterable, i.e. ``[t0, tf]``, the solution will be returned in a uniformly
            spaced time grid computed as ``numpy.linspace(t0, tf, time_steps)``.
            If `t_span` has more than two elements, the solution will be returned on the time grid
            defined by `t_span` itself.
        state0 : ndarray
            Initial state.
            If 6-dimensional equations are selected, `state0` must be a 6-dimensional array of
            initial conditions.
            If 42-dimensional equations are selected and `state0` is 6-dimensional, the last is
            concatenated with the default initial STM (6x6 identity matrix) before propagation.
            If 42-dimensional equations are selected and `state0` is 42-dimensional, an
            initial state already concatenated with the initial STM is assumed and set as initial
            condition for the propagation algorithm.
            If 48-dimensional equations are selected and `state0` is 6-dimensional, the last is
            concatenated with the default initial STM (6x6 identity matrix) and the initial values
            for the epoch partials before propagation.
            If 48-dimensional equations are selected and `state0` is 48-dimensional, an
            initial state already concatenated with the initial STM and the initial values for the
            epoch partials is assumed and set as initial condition for the propagation algorithm.
        events : function or iterable
            Event or events to track.

        Returns
        -------
        t_vec : ndarray
            Time vector for all states in `state_vec`.
        state_vec : ndarray
            Propagated state vector. Subsequent rows correspond to subsequent times in `t_vec`
            while different columns correspond to different components of the initial state
            (i.e. ``state_vec[:, 0]`` is the x component of the state for all times in `t_vec`
            while ``state_vec[-1, :]`` is the final state at ``t_vec[-1]``).
        t_event : iterable or None
            List of times corresponding to tracked events or None if no events have been tracked.
            If one or more events have been tracked but not found in the provided time span,
            a list containing an empty array is returned.
        state_event : iterable or None
            List of states corresponding to tracked events or None if no events have been tracked.
            If one or more events have been tracked but not found in the provided time span,
            a list containing an empty array is returned.

        Warnings
        --------
        `state0` and `t_span` must be in units consistent with the adimensionalization parameters
        `t_c` and `l_c` defined during the initialization of the `EphemerisPropagator` object.
        No further scaling will be performed by this routine.

        """

        if self.with_stm and state0.size == 6:  # 42-dim equations, no STM
            state0 = np.concatenate((state0, self.stm0))
        # if self.with_epoch_partials and state0.size == 6:  # 48-dim equations, no STM and dx_dtau
        #    state0 = np.concatenate((state0, self.stm0_dxdtau0))
        return Propagator.propagate(self, t_span, state0, events)
