import unittest
import os.path
import numpy as np
import spiceypy as sp

from src.init.load_kernels import load_kernels
from src.init.primary import Primary

from src.dynamics.nbp_environment import NBP
from src.dynamics.r2bp_environment import R2BP
from src.propagation.propagator import Propagator

DIRNAME = os.path.dirname(__file__)

class TestR2BPPropagation(unittest.TestCase):

    def test_r2bp_propagation(self):
        nbp = NBP([Primary.EARTH, ])
        r2bp = R2BP(Primary.EARTH)

        x0 = np.array([10000., 0., 0., 0., 8., 0.])
        x0_stm = np.concatenate((x0, np.eye(6).flatten()))
        t = np.linspace(0, 1000, 1000)

        # Propagate
        propagator = Propagator()
        sol_nbp = propagator(nbp, t, x0)
        sol_r2bp = propagator(r2bp, t, x0)

        # compare final states
        self.assertTrue(np.allclose(sol_nbp.state_vec[-1, :6], sol_r2bp.state_vec[-1, :6]))

        # Propagate with STM
        sol_nbp_stm = propagator(nbp, t, x0_stm, with_stm=True)
        sol_r2bp_stm = propagator(r2bp, t, x0_stm, with_stm=True)

        # compare final states
        self.assertTrue(np.allclose(sol_nbp_stm.state_vec[-1, :], sol_r2bp_stm.state_vec[-1, :]))

if __name__ == '__main__':
    load_kernels()
    unittest.main()
    sp.kclear()
