# -*- coding: utf-8 -*-

import unittest
import os.path
from scipy.io import loadmat
import numpy as np

from src.init.primary import Primary
from src.dynamics.bcr4bp_environment import BCR4BP
from src.propagation.propagator import Propagator

DIRNAME = os.path.dirname(__file__)


class TestBCR4BPPropagation(unittest.TestCase):

    def test_propagation(self):
        # The following data was generated using SEMAT
        filename = os.path.join(DIRNAME, 'data', f'bcr4bp_propagation.mat')
        loaded_mat = loadmat(filename)
        sun_phase0 = loaded_mat['sun_phase0'][0][0]
        m_s = loaded_mat['ms'][0][0]
        a_s = loaded_mat['as'][0][0]
        omega_sun = -loaded_mat['omegaS'][0][0]
        bc = BCR4BP(Primary.EARTH, Primary.MOON, sun_phase0)

        # Overwrite the default values (Needed as SEMat uses different values)
        bc._sun_info = bc._sun_info._replace(mass=m_s)
        bc._sun_info = bc._sun_info._replace(radius=a_s)
        bc._sun_info = bc._sun_info._replace(ang_vel=omega_sun)

        # Retrieve initial time and state
        t_prop = np.array(loaded_mat['t_prop'].T[0], dtype=np.float64)
        x0 = np.array(loaded_mat['x0'][0], dtype=np.float64)
        x0_stm = np.array(loaded_mat['x0_stm'][0], dtype=np.float64)
        x_prop = np.array(loaded_mat['yarc_bcp'], dtype=np.float64)
        x_prop_stm = np.array(loaded_mat['yarc_bcp_stm'], dtype=np.float64)

        # Propagate
        prop = Propagator()

        # Propagate
        sol = prop(bc, t_prop, x0)
        sol_stm = prop(bc, t_prop, x0_stm, with_stm=True)

        # Compare
        np.testing.assert_allclose(sol.state_vec[1], x_prop[1], rtol=1e-10, atol=1e-10)
        np.testing.assert_allclose(sol_stm.state_vec[1], x_prop_stm[1], rtol=1e-10, atol=1e-10)

if __name__ == '__main__':
    unittest.main()
