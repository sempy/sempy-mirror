# Check that the wrapped propagator e solve ivp give the same results

# %%
import unittest
import numpy as np

from scipy.integrate import solve_ivp
from src.propagation.propagator import Propagator, Solution, Events, PathParameters

from src.dynamics.cr3bp_environment import CR3BP
from src.dynamics.nbp_environment import NBP
from src.init.primary import Primary
import src.init.defaults as dft

""" Tests to see if the propagator wrapper will obtain the same results as solve_ivp"""
class TestPropagator(unittest.TestCase):
    def test_propagator_initialisation(self):
        # Create propagator
        prop = Propagator()

        # Check that the propagator is correctly initialised
        self.assertEqual(prop.method, dft.method)
        self.assertEqual(prop.rtol, dft.rtol)
        self.assertEqual(prop.atol, dft.atol)

    def test_propagator_execution_cr3bp(self):
        # Create a CR3BP environment
        cr3bp = CR3BP(Primary.EARTH, Primary.MOON)

        # Create propagator
        prop = Propagator()

        # Define time span
        tspan = np.linspace(0, 1, 100)

        # Define initial state
        x0_state_stm = np.concatenate([np.array([1, 0, 0, 0, 1, 0]), np.eye(6).ravel()])

        # Propagate with SEMpy integrator
        sol_sempy = prop(cr3bp, tspan, x0_state_stm, with_stm=True)
        state_vec = sol_sempy.state_vec[:, :6]
        stm = sol_sempy.state_vec[:, 6:].reshape(-1, 6, 6)

        # Propagate with solve_ivp
        def ode(t, y):
            return cr3bp.ode(t, y, with_stm=True)
        sol_ivp = solve_ivp(ode, [tspan[0], tspan[-1]], x0_state_stm, t_eval=tspan,
                            method=prop.method, rtol=prop.rtol, atol=prop.atol)

        # Check that the error is zero
        self.assertTrue(np.allclose(sol_ivp.y.T[:, :6], state_vec, atol=1e-10))
        self.assertTrue(np.allclose(sol_ivp.y.T[:, 6:].reshape(-1, 6, 6), stm, atol=1e-10))

    def test_propagator_execution_nbp(self):
        # Create a NBP environment
        nbp = NBP(center='EARTH', bodies=(Primary.EARTH, Primary.MOON, Primary.SUN))

        # Create propagator
        prop = Propagator()

        # Define time span
        tspan = np.linspace(0, 30*24*60*60, 1000)

        # Define initial state
        x0_state = np.array([2.25105609e+05, 3.06095218e+05, 1.17675826e+05, -1.78774164e+00,
                             1.08181665e+00, 6.51598209e-01])
        x0_state_stm = np.concatenate([x0_state, np.eye(6).ravel()])

        # Propagate with SEMpy integrator
        sol_sempy = prop(nbp, tspan, x0_state_stm, with_stm=True)
        state_vec = sol_sempy.state_vec[:, :6]
        stm = sol_sempy.state_vec[:, 6:].reshape(-1, 6, 6)

        # Propagate with solve_ivp
        def ode_stm(t, x):
            return nbp.ode(t, x, with_stm=True)
        sol_ivp = solve_ivp(ode_stm, [tspan[0], tspan[-1]], x0_state_stm,
                            t_eval=tspan,
                            method=prop.method, rtol=prop.rtol, atol=prop.atol)

        # Check that the error is zero
        self.assertTrue(np.allclose(sol_ivp.y.T[:, :6], state_vec, atol=1e-10))
        self.assertTrue(np.allclose(sol_ivp.y.T[:, 6:].reshape(-1, 6, 6), stm, atol=1e-10))

    def test_propagator_solution(self):
        # Create a CR3BP environment
        cr3bp = CR3BP(Primary.EARTH, Primary.MOON)

        # Create propagator
        prop = Propagator()

        # Define time span
        tspan = np.linspace(0, 1, 100)

        x0 = np.array([1, 0, 0, 0, 1, 0])
        x0_stm = np.concatenate([x0, np.eye(6).ravel()])

        # Propagate with SEMpy integrator
        sol_sempy = prop(cr3bp, tspan, x0_stm, with_stm=True)

        # Check that the solution is correct
        self.assertIsInstance(sol_sempy, Solution)
        self.assertTrue(np.equal(sol_sempy.t_vec, tspan).all())
        self.assertTrue(np.equal(sol_sempy.state_vec[0, :6], x0).all())
        self.assertTrue(np.equal(sol_sempy.state_vec[0, 6:].reshape(-1, 6, 6), np.eye(6)).all())
        self.assertEqual(sol_sempy.state_vec.shape, (len(tspan), 42))
        self.assertEqual(sol_sempy.t_vec.shape, (len(tspan),))
        self.assertEqual(sol_sempy.t_vec[0], tspan[0])
        self.assertEqual(sol_sempy.t_vec[-1], tspan[-1])

    def test_propagator_execution_nbp_adim(self):
        adim_t = 1e5
        adim_l = 1e6

        # Create a NBP environment
        nbp = NBP(center='EARTH',
                  bodies=(Primary.EARTH, Primary.MOON, Primary.SUN),)

        nbp_adim = NBP(center='EARTH',
                  bodies=(Primary.EARTH, Primary.MOON, Primary.SUN),
                  adim_t=adim_t, adim_l=adim_l)

        # Create propagator
        prop = Propagator()

        # Define time span
        tspan = np.linspace(0, 30 * 24 * 60 * 60, 1000)
        tspan_adim = tspan / adim_t

        # Define initial state
        x0_state = np.array([2.25105609e+05, 3.06095218e+05, 1.17675826e+05, -1.78774164e+00,
                             1.08181665e+00, 6.51598209e-01])
        x0_state_stm = np.concatenate([x0_state, np.eye(6).ravel()])
        x0_state_adim = x0_state / adim_l
        x0_state_adim[3:] = x0_state_adim[3:] * adim_t
        x0_state_stm_adim = np.concatenate([x0_state_adim, np.eye(6).ravel()])

        # Propagate with SEMpy integrator
        sol_sempy = prop(nbp, tspan, x0_state_stm, with_stm=True)
        state_vec = sol_sempy.state_vec[:, :6]
        stm = sol_sempy.state_vec[:, 6:].reshape(-1, 6, 6)

        # Propagate with SEMpy integrator (adimensional)
        sol_sempy_adim = prop(nbp_adim, tspan_adim, x0_state_stm_adim, with_stm=True)

        # Re-dimensionalise solution
        state_vec_redim = sol_sempy_adim.state_vec[:, :6] * adim_l
        state_vec_redim[:, 3:] = state_vec_redim[:, 3:] / adim_t
        stm_redim = sol_sempy_adim.state_vec[:, 6:].reshape(-1, 6, 6)
        stm_redim[:, 3:6, 0:3] /= adim_t
        stm_redim[:, 0:3, 3:6] *= adim_t

        # Check that the error is close to zero
        self.assertTrue(np.allclose(state_vec_redim, state_vec, atol=1e-5))
        self.assertTrue(np.allclose(stm_redim, stm, atol=1e-5))

    def test_propagation_with_parameters(self):
        # Create a CR3BP environment
        cr3bp = CR3BP(Primary.EARTH, Primary.MOON)

        # Create propagator
        prop = Propagator()

        # Define time span
        tspan = np.linspace(0, 1, 100)

        # Define initial state
        x0 = np.array([1, 0, 0, 0, 0.1, 0])

        # Propagate with SEMpy integrator
        momentum_integral = {'function': PathParameters.momentum_integral, 'initial_value': 145}
        sol_sempy = prop(cr3bp, tspan, x0, path_param=momentum_integral)

        # Check that the initial momentum is 145
        self.assertEqual(sol_sempy.path_param_vec[0], 145)

    def test_propagation_event(self):
        # Create a CR3BP environment
        cr3bp = CR3BP(Primary.EARTH, Primary.MOON)

        # Create propagator
        prop = Propagator()

        # Define time span
        tspan = np.linspace(0, 1, 100)

        # Define initial state
        x0 = np.array([1, 0, 0, 0, 0.1, 0])

        # Initialize the impact event
        impact_event = Events.collision('lunar_impact', cr3bp.m2_pos, Primary.MOON.Rm/cr3bp.adim_l)

        # Propagate with SEMpy integrator
        sol_sempy = prop(cr3bp, tspan, x0, events=impact_event)

        # Check that the impact event is at a distance from the Moon's surface equal to the radius
        dist_impact = np.linalg.norm(sol_sempy.state_events[0][0, :3] - cr3bp.m2_pos) - \
                      Primary.MOON.Rm/cr3bp.adim_l
        self.assertTrue(np.isclose(dist_impact, 0))

if __name__ == '__main__':
    from src.init.load_kernels import load_kernels
    import spiceypy as sp

    # Load kernel
    load_kernels()

    unittest.main()

    # Unload kernels
    sp.kclear()