import unittest
import os.path
import numpy as np
import spiceypy as sp
from scipy.io import loadmat

from src.init.load_kernels import load_kernels, KERNELS_ROOT
from src.init.constants import MOON_SMA, MOON_OMEGA_MEAN
from src.init.primary import Primary

from src.dynamics.nbp_environment import NBP
from src.propagation.propagator import Propagator

DIRNAME = os.path.dirname(__file__)
sp.furnsh(os.path.join(KERNELS_ROOT, 'pck/moon_pa_de421_1900-2050.bpc'))
sp.furnsh(os.path.join(KERNELS_ROOT, 'fk/moon_080317.tf'))


class TestNBPPropagation(unittest.TestCase):

    def test_nbp_propagation(self):
        data_mat = loadmat(os.path.join(DIRNAME, 'data', 'test_ephemeris_propagator.mat'),
                           squeeze_me=True, struct_as_record=False)['data']

        bodies = (Primary.EARTH, Primary.MOON, Primary.SUN)
        nbp_adim = NBP(bodies, adim_l=MOON_SMA, adim_t=1/MOON_OMEGA_MEAN)
        nbp = NBP(bodies)
        max_steps = 4_000_000
        propagator = Propagator(max_internal_steps=max_steps)

        # # 6-dim equations, non-dimensional
        sol6 = propagator(nbp_adim, data_mat.t_span[0, :], data_mat.y0[0, :])
        np.testing.assert_allclose(sol6.state_vec[-1, :], data_mat.yf6[0, :], rtol=1e-10, atol=1e-10)
        
        # 42-dim equations, non-dimensional
        x0_42 = np.concatenate((data_mat.y0[0, :], np.eye(6).ravel()))
        sol42 = propagator(nbp_adim, data_mat.t_span[0, :], x0_42, with_stm=True)
        tf42_adim, yf42_adim = sol42.t_vec, sol42.state_vec
        np.testing.assert_allclose(yf42_adim[-1, :], data_mat.yf42[0, :], rtol=1e-10, atol=1e-10)
        #
        # 48-dim equations, non-dimensional
        x0_48 = np.concatenate((data_mat.y0[0, :], np.eye(6).ravel(), np.zeros((6,))))
        sol48 = propagator(nbp_adim, data_mat.t_span[0, :], x0_48, with_stm=True,
                           with_epoch_partials=True)
        tf48_adim, yf48_adim = sol48.t_vec, sol48.state_vec
        np.testing.assert_allclose(yf48_adim[-1, :], data_mat.yf48[0, :], rtol=1e-10, atol=1e-10)


        # 6-dim equations, dimensional [km, km/s]
        sol6_dim = propagator(nbp, data_mat.t_span[1, :], data_mat.y0[1, :])
        tf6_dim, yf6_dim = sol6_dim.t_vec, sol6_dim.state_vec
        np.testing.assert_allclose(yf6_dim[-1, :], data_mat.yf6[1, :], rtol=1e-10, atol=1e-10)

        # 42-dim equations, dimensional [km, km/s]
        x0_42_dim = np.concatenate((data_mat.y0[1, :], np.eye(6).ravel()))
        sol42_dim = propagator(nbp, data_mat.t_span[1, :], x0_42_dim, with_stm=True)
        tf42_dim, yf42_dim = sol42_dim.t_vec, sol42_dim.state_vec
        np.testing.assert_allclose(yf42_dim[-1, :], data_mat.yf42[1, :], rtol=1e-10, atol=1e-10)

        # 48-dim equations, dimensional [km, km/s]
        x0_48_dim = np.concatenate((data_mat.y0[1, :], np.eye(6).ravel(), np.zeros((6,))))
        sol48_dim = propagator(nbp, data_mat.t_span[1, :], x0_48_dim, with_stm=True,
                                 with_epoch_partials=True)
        tf48_dim, yf48_dim = sol48_dim.t_vec, sol48_dim.state_vec
        np.testing.assert_allclose(yf48_dim[-1, :], data_mat.yf48[1, :], rtol=1e-10, atol=1e-10)

        # test with GMAT data, integration is performed in dimensional units of km and s
        y0_gmat = np.array([-203261.3784021969, -338246.0409067131, -129905.6591475186,
                            1.025839533673108, -0.5513935055210328, -0.4809128853776429])
        yf_gmat = np.array([75876.59104060699, -387548.667964136, -205966.1069506892,
                            1.055937995015543, 0.139545498853069, -0.09606399564054512])
        epoch0_utc = '2020 JUN 04 10:12:38.390 UTC'
        nbp.start_date = epoch0_utc

        sol = propagator(nbp, [0, 259200.0], y0_gmat)
        tv, yv = sol.t_vec, sol.state_vec
        np.testing.assert_allclose(yv[-1, :], yf_gmat, rtol=1e-4, atol=1e-8)

    def test_ephemeris_sh_propagator(self):
        # Not implemented yet
        pass

if __name__ == '__main__':
    load_kernels()
    unittest.main()
    sp.kclear()
