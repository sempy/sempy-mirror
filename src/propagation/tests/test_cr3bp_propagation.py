# -*- coding: utf-8 -*-

import unittest
import os.path
from scipy.io import loadmat

from src.init.primary import Primary
from src.dynamics.cr3bp_environment import CR3BP
from src.propagation.propagator import Propagator

DIRNAME = os.path.dirname(__file__)


class TestCr3bpPropagation(unittest.TestCase):

    def test_cr3bp_propagation(self):
        # data obtained in SEMAT for an L1 Halo orbit with Az=15000km
        filename = os.path.join(DIRNAME, 'data', 'cr3bp_propagator.mat')
        d = loadmat(filename)
        state0 = d['y0'].flatten()
        state_end = d['yf'].flatten()
        tf = d['T'][0][0]

        cr3bp = CR3BP(Primary.EARTH, Primary.MOON)

        # propagation of state only (6-dim)
        prop = Propagator()

        # Test propagation for 6-dim state
        sol = prop(cr3bp, [0.0, tf], state0[0:6])
        t_vec, state_vec, = sol.t_vec, sol.state_vec

        for i in range(5):
            self.assertAlmostEqual(state_vec[-1, i], state_end[i], places=6)

        # Test propagation for 6-dim state and STM
        sol_stm = prop(cr3bp, [0.0, tf], state0, with_stm=True)
        t_vec, state_vec = sol_stm.t_vec, sol_stm.state_vec,
        for i in range(41):
            self.assertAlmostEqual(state_vec[-1, i], state_end[i], places=6)


if __name__ == '__main__':
    unittest.main()
