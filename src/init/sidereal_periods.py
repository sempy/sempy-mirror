# -*- coding: utf-8 -*-

import os.path
import csv
import numpy as np

# import src.init.constants as cst
import src.utils.csvdecomment as csvdc

import re
import spiceypy

dirname = os.path.dirname(__file__)


class SiderealPeriods:
    """"Will provide the celestial bodies sidereal periods.

    From a .csv file this class will provide the NASA celestial bodies sidereal periods.

    In order to do that, it only will be necessary to use the name of a body as the class' argument.

    Data contained in the .csv file comes from:
    https://nssdc.gsfc.nasa.gov/planetary/factsheet/
    https://ssd.jpl.nasa.gov/?planet_phys_par#B

    Args:
        body: A body name, e.g 'Mars'

    Attributes:
        Trot: Body's sidereal rotation period, is the time for one rotation of the body on its axis
            relative to the fixed stars.  A minus sign indicates retrograde rotation. [s].
        Torb: Body's sidereal orbit period, for its orbit around the Sun, relative to the fixed
            stars (for the MOON it is for its orbit about the Earth) [s].

    """
    # Local pylint disabled commands
    # pylint: disable=invalid-name, too-few-public-methods

    def __init__(self, body='Moon'):
        """Inits SiderealPeriods."""
        self.Trot, self.Torb = SiderealPeriods.get_sidereal_periods(body)

    @staticmethod
    def get_sidereal_periods(body='Moon'):
        """Method to get the sidereal periods of bodies."""
        filename = os.path.join(dirname, 'siderealPeriods.csv')
        with open(filename, newline='') as csvfile:
            # Create list from csv file
            bodies_sidereal_periods = list(csv.reader(csvdc.decomment(csvfile), delimiter=';'))
            
            
            # bodies_sidereal_p = np.array(bodies_sidereal_periods)
            # bodies_name = bodies_sidereal_p[0:, 0]

         # Make list flat and delete empty entries
        flat_bodies_sidereal_periods = [item for row in bodies_sidereal_periods for item in row if item != '']

        # Identify indexes of the flat list where the name of the planets appear
        row_start = [index for index, value in enumerate(flat_bodies_sidereal_periods) if bool(re.match(r'^[a-zA-z\s]+$', value))]

        # Assign a list of final index to delimit one row for each planet name
        row_end = row_start[1:]
        row_end.append(len(flat_bodies_sidereal_periods))  # this adds a final index after the last planet name 

        # Create the list of planet's names
        bodies_name = [flat_bodies_sidereal_periods[idx] for idx in row_start]

        # Create the array of all the keplerian elements
        bodies_sidereal_p = np.array([flat_bodies_sidereal_periods[r_start+1:r_end]
                                      for r_start, r_end
                                      in zip(row_start, row_end)],
                                     dtype=np.float64)
        
        # Identify the row corresponding to the planet query
        i = bodies_name.index(body)

        # if body != '' and body in bodies_name:
        #     # body != '' is there to avoid using the second line (which name string is blank in the
        #     # csv file) corresponding to the planets keplerian elements rates
        #     i = np.where(bodies_name == body)
        #     # will return an array (i[0][0]) with the corresponding index (i) where the body is
        #     # located within the csv file
        #     sidereal_rotation_p = np.array(bodies_sidereal_p[i, 1],
        #                                    dtype=np.float) * cst.DAYS2SEC  # [s]
        #     sidereal_orbital_p = np.array(bodies_sidereal_p[i, 2],
        #                                   dtype=np.float) * cst.DAYS2SEC  # [s]

        # else:
        #     raise Exception

        sidereal_rotation_p = bodies_sidereal_p[i, 0] * spiceypy.convrt( 1, 'days', 'seconds' )  # [s] cst.DAYS2SEC
        sidereal_orbital_p = bodies_sidereal_p[i, 1] * spiceypy.convrt( 1, 'days', 'seconds' )  # [s] cst.DAYS2SEC
        

        # return sidereal_rotation_p[0][0], sidereal_orbital_p[0][0]
        return sidereal_rotation_p, sidereal_orbital_p
