"""
Load the SPICE kernels defined in the meta-kernel `kernel.tm`.

"""

import os
from spiceypy import furnsh, ktotal

DIRNAME = os.path.dirname(__file__)
KERNELS_ROOT = os.path.join(DIRNAME, 'kernels')
KERNELS_TO_LOAD = (
    'lsk/naif0012.tls',
    'pck/pck00010.tpc',
    'pck/gm_de431.tpc',
    'spk/de430.bsp',
)  # kernels for N-body ephemeris model with point masses
KERNELS_TO_LOAD_MOON_POT = (
    'fk/moon_080317.tf',
    'lsk/naif0012.tls',
    'pck/pck00010.tpc',
    'pck/moon_pa_de421_1900-2050.bpc',
    'pck/gm_de431.tpc',
    'spk/de421.bsp',
)  # kernels for N-body ephemeris model with point masses and Moon gravitational potential


def load_kernels():
    """Load SPICE kernels. """
    if ktotal('all') == 0:
        for k in KERNELS_TO_LOAD:
            furnsh(os.path.join(KERNELS_ROOT, k))


def load_kernels_moon_pot():
    """Load SPICE kernels. """
    if ktotal('all') == 0:
        for k in KERNELS_TO_LOAD_MOON_POT:
            furnsh(os.path.join(KERNELS_ROOT, k))
