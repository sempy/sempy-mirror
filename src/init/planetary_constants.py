# -*- coding: utf-8 -*-


import spiceypy  # Import the CSPICE-Python interface.
import src.init.constants as cst


class PlanetaryConstants:
    """Will provide planetary constants from a given SPICE kernel.

    Using the python package spiceypy, this class will load NASA SPICE Planetary Constants Kernels
    (PCK) in order to obtain the needed planetary constants, among them the radii and standard
    gravitational parameters.

    Note that for bodies other than the Sun, the Earth and the Moon the NAIF IDs,
    standard gravitational parameters and masses correspond to the ones of the entire planetary
    system, e.g. the planet with all its moons.

    New PCK can be downloaded from:
    https://naif.jpl.nasa.gov/pub/naif/generic_kernels/pck/

    In order to do that, it will only be necessary to use the name of a planet as the class'
    argument.

    Parameters
    ----------
    planet : str
        A planet name, e.g 'Mars'

    Attributes
    ----------
    Req : float or None
        Planet's equatorial radius [km].
    Rm : float or None
        Planet's mean radius [km].
    GM : float
        Planet or planetary system's standard gravitational parameter [km^3.s^-2].
    M : float
        Planet or planetary system's mass [kg].
    naif_id : int
        Planet or planetary system's NAIF ID.

    """

    # Local pylint disabled commands
    # pylint: disable=invalid-name

    # Using some kernels like de-403-masses can raise errors because of pluto GM (BODY999_GM),
    # given that pluto sometimes is considered as a Dwarf planet

    def __init__(self, planet='Earth'):
        """Inits PlanetaryConstants class."""
        if planet == 'Earth and Moon':
            self.Req, self.Rm = None, None
        else:
            self.Req, self.Rm = PlanetaryConstants.get_radii(planet)
        self.naif_id = PlanetaryConstants.get_naif_id(planet)
        self.GM = PlanetaryConstants.get_gravitational_param(self.naif_id)
        self.M = self.get_mass()

    @staticmethod
    def get_naif_id(planet):
        """Returns the NAIF ID of the body `planet` or its planetary system. """
        if planet == 'Earth and Moon':
            planet = 'Earth barycenter'
        elif planet not in ('Sun', 'Earth', 'Moon'):
            planet = planet + ' barycenter'
        return spiceypy.bodn2c(planet)

    @staticmethod
    def get_radii(planet):
        """Returns the planet's equatorial and mean radii.

        Access the kernel pool data for the tri-axial radii of the planet,
        `radii[0]` holds the equatorial radius, `radii[2]` the polar radius.

        """

        radii = spiceypy.bodvrd(planet, 'RADII', 3)[1]
        return radii[0], radii.mean()

    @staticmethod
    def get_gravitational_param(naif_id):
        """Return the planet's standard gravitational parameter.

        For bodies other than the Sun, the Earth and the Moon the value for the planetary system
        (planet plus all its moons) is returned instead.

        """

        return spiceypy.bodvcd(naif_id, 'GM', 1)[1][0]

    def get_mass(self):
        """Returns the planet or planetary system's mass obtained from the corresponding
        standard gravitational parameter.

        """

        return self.GM / cst.G
