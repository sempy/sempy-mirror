# -*- coding: utf-8 -*-


from enum import Enum

import src.init.constants as cst


class Primary(Enum):
    """Initializes celestial bodies (primaries). This class inherits from Enum.

    This class will contain physical parameters (Req; Rm; M; GM) and orbital parameters (a; Tkep,
    Tsid_rot, Tsid_orb) of: MERCURY; VENUS; EARTH; MOON; MARS; JUPITER; SATURN; URANUS; NEPTUNE;
    PLUTO; SUN; EARTH_AND_MOON. These celestial bodies parameters are meant to be used in the
    initialization of a Circular Restricted Three body Problem.

    Attributes
    ----------
    Req : float
        Primary's equatorial radius [km].
    Rm : float
        Primary's mean radius [km].
    M : float
        Primary's mass [kg].
    GM : float
        Primary's standard gravitational parameter [km^3.s^-2].
    a : float
        Primary's semi-major axis for its orbit around the Sun (for the MOON it is for its orbit
        about the Earth) [km].
    Tkep : float
        Primary's orbital period for its orbit around the Sun (for the MOON it is for its orbit
        about the Earth) [s].
    Tsid_rot : float
        Primary's sidereal rotation period, is the time for one rotation of the body on its axis
        relative to the fixed stars.  A minus sign indicates retrograde rotation. [s].
    Tsid_orb : float
        Primary's sidereal orbit period, for its orbit around the Sun, relative to the fixed stars
        (for the MOON it is for its orbit about the Earth) [s].
    name : str
        Primary's name.

    Examples
    --------
    >>> primary1 = Primary.SUN
    >>> primary2 = Primary.EARTH
    """

    # Local pylint disabled commands
    # pylint: disable=invalid-name

    # Importing planets information
    MERCURY = (cst.MERCURY.Req, cst.MERCURY.Rm, cst.MERCURY.M, cst.MERCURY.GM,
               cst.MERCURY_KEP_ELEM.a, cst.MERCURY_KEP_ELEM.T, cst.MERCURY_SID_P.Trot,
               cst.MERCURY_SID_P.Torb, cst.MERCURY.naif_id)

    VENUS = (cst.VENUS.Req, cst.VENUS.Rm, cst.VENUS.M, cst.VENUS.GM, cst.VENUS_KEP_ELEM.a,
             cst.VENUS_KEP_ELEM.T, cst.VENUS_SID_P.Trot, cst.VENUS_SID_P.Torb, cst.VENUS.naif_id)

    EARTH = (cst.EARTH.Req, cst.EARTH.Rm, cst.EARTH.M, cst.EARTH.GM, cst.EARTH_KEP_ELEM.a,
             cst.EARTH_KEP_ELEM.T, cst.EARTH_SID_P.Trot, cst.EARTH_SID_P.Torb, cst.EARTH.naif_id)

    MARS = (cst.MARS.Req, cst.MARS.Rm, cst.MARS.M, cst.MARS.GM, cst.MARS_KEP_ELEM.a,
            cst.MARS_KEP_ELEM.T, cst.MARS_SID_P.Trot, cst.MARS_SID_P.Torb, cst.MARS.naif_id)

    JUPITER = (cst.JUPITER.Req, cst.JUPITER.Rm, cst.JUPITER.M, cst.JUPITER.GM,
               cst.JUPITER_KEP_ELEM.a, cst.JUPITER_KEP_ELEM.T, cst.JUPITER_SID_P.Trot,
               cst.JUPITER_SID_P.Torb, cst.JUPITER.naif_id)

    SATURN = (cst.SATURN.Req, cst.SATURN.Rm, cst.SATURN.M, cst.SATURN.GM, cst.SATURN_KEP_ELEM.a,
              cst.SATURN_KEP_ELEM.T, cst.SATURN_SID_P.Trot, cst.SATURN_SID_P.Torb,
              cst.SATURN.naif_id)

    URANUS = (cst.URANUS.Req, cst.URANUS.Rm, cst.URANUS.M, cst.URANUS.GM, cst.URANUS_KEP_ELEM.a,
              cst.URANUS_KEP_ELEM.T, cst.URANUS_SID_P.Trot, cst.URANUS_SID_P.Torb,
              cst.URANUS.naif_id)

    NEPTUNE = (cst.NEPTUNE.Req, cst.NEPTUNE.Rm, cst.NEPTUNE.M, cst.NEPTUNE.GM,
               cst.NEPTUNE_KEP_ELEM.a, cst.NEPTUNE_KEP_ELEM.T, cst.NEPTUNE_SID_P.Trot,
               cst.NEPTUNE_SID_P.Torb, cst.NEPTUNE.naif_id)

    PLUTO = (cst.PLUTO.Req, cst.PLUTO.Rm, cst.PLUTO.M, cst.PLUTO.GM, cst.PLUTO_KEP_ELEM.a,
             cst.PLUTO_KEP_ELEM.T, cst.PLUTO_SID_P.Trot, cst.PLUTO_SID_P.Torb,
             cst.PLUTO.naif_id)

    SUN = (cst.SUN.Req, cst.SUN.Rm, cst.SUN.M, cst.SUN.GM, 0.0, 0.0, 0.0, 0.0, cst.SUN.naif_id)

    MOON = (cst.MOON.Req, cst.MOON.Rm, cst.MOON.M, cst.MOON.GM, cst.MOON_SMA, cst.MOON_PERIOD,
            cst.MOON_SID_P.Trot, cst.MOON_SID_P.Torb, cst.MOON.naif_id)

    EARTH_AND_MOON = (cst.EARTH.Req, cst.EARTH.Rm, cst.EARTH_AND_MOON.M,
                      cst.EARTH_AND_MOON.GM, cst.EARTH_KEP_ELEM.a, cst.EARTH_KEP_ELEM.T,
                      cst.EARTH_SID_P.Trot, cst.EARTH_SID_P.Torb, cst.EARTH_AND_MOON.naif_id)

    def __init__(self, Req, Rm, M, GM, a, Tkep, Trot, Torb, naif_id):
        """Inits Primary class."""
        self.Req = Req  # [km]
        self.Rm = Rm  # [km]
        self.M = M  # [kg]
        self.GM = GM  # [km^3.s^-2]
        self.a = a  # [km]
        self.Tkep = Tkep  # [s]
        self.Tsid_rot = Trot  # [s]
        self.Tsid_orb = Torb  # [s]
        self.naif_id = naif_id

        # Enum members aka instance (e.g. SUN) also have a property that contains just their item
        # name, therefore Primary.SUN.name will return 'SUN'. The value of Enum members will be
        # passed to the __init__ method converting them (the values) into attributes.

    @property
    def g(self):
        """Primary's surface gravity [m/s^2]. """
        return cst.G * 1000 * self.M / (self.Req * self.Req)  # [m/s^2]
