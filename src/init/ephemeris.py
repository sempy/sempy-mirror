"""
Ephemeris class to define the celestial bodies to be included in the ephemeris force model.

"""

import numpy as np

from src.init.primary import Primary
from src.utils.cspice_lib_loader import frame_encoder
from src.gravity.sh_coefficients import sh_provider


class Ephemeris:
    """Ephemeris class is a collection of Primary objects, their physical properties and their IDs
    as defined in the JPL's SPICE Toolkit.

    It is meant to ease the definition of the force model used by `EphemerisPropagator` while
    propagating a particle's state in a higher fidelity model.

    Parameters
    ----------
    bodies : Primary or tuple
        `Primary` object or tuple of `Primary` objects to be included in the `Ephemeris` class
        instance. For a multi-element tuple, objects must be unique.

    Attributes
    ----------
    bodies : tuple
        Tuple of `Primary` objects.
    naif_ids : ndarray
        Array of integers corresponding to the bodies' NAIF IDs.
    gm_dim : ndarray
        Array containing the bodies' standard gravitational parameters in dimensional units
        of km^3*s^-2.
    sh_models : dict or None
        Data for the non-uniform gravity field models of one or more bodies or `None`.
        See the `get_sh_models` class method for details on how `sh_models` is built.

    Warnings
    --------
    When a `Primary` object different from `EARTH`, `MOON` and `SUN` is passed as input, its name
    its parsed to `BODY BARYCENTER` and both NAIF ID and standard gravitational parameter
    correspond to the barycenter of the primary plus all its moons.
    For example, `Primary.JUPITER` will result in ``JUPITER BARYCENTER`` and NAIF ID ``5``
    rather than ``JUPITER`` and ``599``.
    For consistency, the same approximation is made for the inner planets Mercury and Venus which
    have no moons and the standard gravitational parameter of the body coincides with the one of
    the system's barycenter.
    The above procedure is needed since the generic spk kernels (i.e. DE430) used to retrieve the
    position of the celestial bodies included in the model do not contain information for the outer
    planets and Mars, but only for their respective barycenters when considered as systems with all
    their moons.

    """

    def __init__(self, bodies, **kwargs):
        """Inits Ephemeris class. """

        # check consistency of the user-provided input, i.e. no duplicates
        if isinstance(bodies, Primary):
            self.bodies = (bodies,)
        elif isinstance(bodies, (tuple, list)) and len(bodies) > 0 \
                and len(bodies) == len(set(bodies)) \
                and all([isinstance(b, Primary) for b in bodies]):
            self.bodies = bodies
        else:
            raise Exception('body must be either a Primary object or a non-empty tuple of unique '
                            'Primary objects')

        # retrieve the bodies (or body barycenters) NAIF IDs and standard gravitational parameters
        self.naif_ids = np.asarray([b.naif_id for b in self.bodies], dtype=np.intc)
        self.gm_dim = np.asarray([b.GM for b in self.bodies])

        # retrieve gravity potential models if any
        self.sh_models = self.get_sh_models(**kwargs) if len(kwargs) > 0 else None

    def roll(self, body):
        """Reorder all `Ephemeris` class attributes to have the `Primary` in input indexed at 0.

        Parameters
        ----------
        body : Primary
            Primary object.

        """

        idx = self.get_index(body)
        self.bodies = tuple(np.roll(self.bodies, -idx))
        self.naif_ids, self.gm_dim = [np.roll(k, -idx) for k in (self.naif_ids, self.gm_dim)]
        if self.sh_models is not None:
            self.sh_models['nm_max'] = np.roll(self.sh_models['nm_max'], -idx, axis=0)

    def get_index(self, body):
        """Returns the index corresponding to `body`. """
        return self.bodies.index(body)

    def get_naif_id(self, body):
        """Returns the NAIF ID corresponding to `body`. """
        return self.naif_ids[self.get_index(body)]

    def get_gm_dim(self, body):
        """Returns the standard gravitational parameter corresponding to `body`. """
        return self.gm_dim[self.get_index(body)]

    def get_sh_models(self, **kwargs):
        """Processes data for non-uniform gravity field models of one or more bodies
        in `Ephemeris` to be stored in the `sh_models` attribute.

        Keyword-only arguments must point to a body included in `Ephemeris` with at most one kwargs
        specified for each body.
        They must be in the form `body_name=('GRAVITY_MODEL', n_max, m_max, update_gm)` with:

            1) `body_name`: name of the body to which the SH model refers to (e.g. `moon`).
            2) `GRAVITY_MODEL`: name of the JSON file where the SH coefficients are stored.
            3) `n_max`: maximum degree to consider for the spherical harmonics.
            4) `m_max`: maximum order to consider for the spherical harmonics.
            5) `update_gm`: whether to update the body standard gravitational parameter or not.

        Moreover, `GRAVITY_MODEL` must be recognized by the `sh_provider` function implemented in
        the `gravity` sub-package.

        The output dictionary `sh_models` contains the following keys:

            1) `r_ref`: 1-dim array of reference radii (in km) for each body in `kwargs`.
            2) `body_ref`: 2-dim array of (encoded) body-fixed frames for each body in `kwargs`.
            3) `z_nm`: 3-dim array of complex harmonics for each body in `kwargs`.
            4) `nm_max`: 2-dim array of maximum degrees and orders for all bodies in `Ephemeris`.

        Array `nm_max` is a `(total number of bodies) x 3` array of ints with columns:

            1) Index in keys 1-3 with data for the current body or `-1` if no SH model is given.
            2) Maximum SH degree for the current body or `-1` if no SH model is given.
            3) Maximum SH order for the current body or `-1` if no SH model is given.

        Other Parameters
        ----------------
        body_name : tuple
            Name of the JSON file where the SH coefficients for the body are stored,
            maximum degree and maximum order for the SH model. See above for details.

        Returns
        -------
        sh_model : dict
            Data for the non-uniform gravity potential models of one or more bodies.
            See above for details.

        """

        eph_body_names = tuple([b.name.lower() for b in self.bodies])  # bodies in the ephemeris
        sh_body_names = tuple(kwargs.keys())  # bodies with associated SH models

        if not all([n in eph_body_names for n in sh_body_names]):  # check consistency
            raise Exception('kwargs must point to a primary object included in the model')

        # maximum degrees and orders to be considered for the spherical harmonics coefficients
        nm_max = np.asarray([[kwargs[k][1], kwargs[k][2]] for k in sh_body_names], dtype=np.int64)
        if (np.diff(nm_max) > 0).any():
            raise Exception('SH order cannot be grater than the corresponding SH degree')

        body_idx = np.asarray([eph_body_names.index(k) for k in sh_body_names], dtype=np.int64)
        nm_max_idx = - np.ones((len(eph_body_names), 3), dtype=np.int64)  # SH max degrees/orders
        r_ref = np.empty(len(sh_body_names))  # bodies' reference radii [km]
        z_nm = np.empty((len(sh_body_names), np.max(nm_max) + 1, np.max(nm_max) + 1),
                        dtype=np.complex128)  # matrix of SH coefficients
        body_ref = []  # encoded body-fixed reference frames names

        for i, k in enumerate(sh_body_names):  # loop over bodies with associated SH model
            r_ref[i], gm_i, z_nm_i, body_ref_i = \
                sh_provider(self.bodies[body_idx[i]], kwargs[k][0], nm_max=nm_max[i, 0])
            z_nm[i, 0:z_nm_i.shape[0], 0:z_nm_i.shape[1]] = z_nm_i  # SH coefficients
            nm_max_idx[body_idx[i], 0] = i  # index of body's data in r_ref, z_nm, body_ref
            nm_max_idx[body_idx[i], 1:3] = \
                [min(z_nm_i.shape[0] - 1, nm_max[i, 0]), min(z_nm_i.shape[1] - 1, nm_max[i, 1])]
            body_ref.append(body_ref_i)  # body-fixed reference frame name as string
            if len(kwargs[k]) > 3 and kwargs[k][3]:  # update body GM with value in SH model
                self.gm_dim[body_idx[i]] = gm_i

        return {'r_ref': r_ref, 'body_ref': frame_encoder(body_ref), 'z_nm': z_nm,
                'nm_max': nm_max_idx}

    def get_integration_params(self, ref='J2000', obs=None, t_c=1.0, l_c=1.0):
        """Returns input parameters for `Propagator` and `MultipleShooting` objects.

        These parameters correspond to a N-body ephemeris force model with point masses only.

        Parameters
        ----------
        ref : str, optional
            Reference frame name as defined by the NAIF's SPICE Toolkit. Default is ``J2000``.
        obs : Primary or None, optional
            Primary body whose position coincides with the origin of the reference frame
            in which the dynamics is propagated. Default is ``None`` for which the first Primary
            object in `self` is selected.
        t_c : float, optional
            Characteristic time for adimensionalization. Default is ``1.0``.
        l_c : float, optional
            Characteristic length for adimensionalization. Default is ``1.0``.

        Returns
        -------
        ref_encoded : ndarray
            Reference frame name encoded as Numpy array of 8-bit integers using ASCII standard.
        gm_adim : ndarray
            Array containing the bodies' standard gravitational parameters in normalized units.

        """

        # by convention, the selected reference frame is centered on the celestial body at index 0
        # in the bodies attribute of the Ephemeris object. If the user passes a different value
        # for obs, the elements in bodies and in the other iterable-like attributes of Ephemeris
        # are reordered to match the convention.
        if obs is not None:
            if obs not in self.bodies:
                raise Exception('obs must be a Primary object included in the Ephemeris model')
            self.roll(obs)

        return frame_encoder(ref), self.gm_dim * (t_c ** 2) * (l_c ** -3)

    def get_sh_integration_params(self, l_c=1.0):
        """Returns input parameters for `Propagator` and `MultipleShooting` objects.

        These parameters account for the inclusion of one or more complex gravity field model in
        the N-body ephemeris.

        Parameters
        ----------
        l_c : float, optional
            Characteristic length for adimensionalization. Default is ``1.0``.

        Returns
        -------
        r_ref_adim : ndarray
            Normalized reference radii.
        bf_ref : ndarray
            Encoded body-fixed reference frames.
        z_nm : ndarray
            Complex harmonics `Z(n,m) = C(n,m) + jS(n,m)`.
        nm_max : ndarray
            Maximum SH degrees and orders.

        Warnings
        --------
        This method should always be called after `get_integration_params` since depending on the
        input parameter `obs` the last might swap the rows of `nm_max`.

        """

        if self.sh_models is None:
            raise Exception('No SH models included for bodies in Ephemeris')
        r_ref_adim, bf_ref, z_nm, nm_max = \
            self.sh_models['r_ref'] / l_c, self.sh_models['body_ref'], \
            self.sh_models['z_nm'], self.sh_models['nm_max']
        return r_ref_adim, bf_ref, z_nm, nm_max
