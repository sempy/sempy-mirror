# -*- coding: utf-8 -*-
"""
This module will get the default values from the default.ini file, then,
from here those default values will be used in the package's modules.

To add new defaults values in this module (using ConfigParser()), it is necessary to add them first
in the the module config_file_create.py, which will build the default.ini file updated, and then,
the user can modify the default values in there (default.ini file).


"""

import os.path
from configparser import ConfigParser
from psutil import cpu_count
from platform import system

# To get default parameters from the default.ini file
dirname = os.path.dirname(__file__)
filename = os.path.join(dirname, 'default.ini')

parser = ConfigParser()
parser.read(filename)

libp_precision = parser.getfloat('Libration_points_default_params', 'libp_precision')

# propagator
method = parser.get('ODE_solver_default_params', 'default_method')
rtol = parser.getfloat('ODE_solver_default_params', 'default_rtol')
atol = parser.getfloat('ODE_solver_default_params', 'default_atol')
time_steps = parser.getint('ODE_solver_default_params', 'default_time_steps')
max_internal_steps = parser.getint('ODE_solver_default_params', 'default_max_internal_steps')

# differential correctors
single_shooting_precision = parser.getfloat('Differential_correction_default_params',
                                            'single_shooting_precision')
multiple_shooting_precision = parser.getfloat('Differential_correction_default_params',
                                              'multiple_shooting_precision')
single_shooting_maxiter = parser.getint('Differential_correction_default_params',
                                        'single_shooting_maxiter')
multiple_shooting_maxiter = parser.getint('Differential_correction_default_params',
                                          'multiple_shooting_maxiter')
correction_method = parser.get('Differential_correction_default_params', 'method')
correction_tolerance = parser.getfloat('Differential_correction_default_params', 'tolerance')

# serial or parallel propagation in multiple shooting and patch points propagator
procs = None if system() == 'Windows' else cpu_count(False)

# _______________default_plotting_options______________________
XY = parser.getboolean('Default_plotting_options', 'xy')
XZ = parser.getboolean('Default_plotting_options', 'xz')
YZ = parser.getboolean('Default_plotting_options', 'yz')
TD = parser.getboolean('Default_plotting_options', 'td')
disp_first_pr = parser.getboolean('Default_plotting_options', 'disp_first_pr')
disp_second_pr = parser.getboolean('Default_plotting_options', 'disp_second_pr')
N = parser.getint('Default_plotting_options', 'n')
