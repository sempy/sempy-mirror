# -*- coding: utf-8 -*-


import os.path
import csv
import math
import numpy as np

import spiceypy
import src.utils.csvdecomment as cvsdc
# import src.init.constants as cst

import re

from src.init.load_kernels import load_kernels



dirname = os.path.dirname(__file__)


class PlanetKeplerianElements:
    """Will provide the planet orbital elements.

    From a .csv file this class will provide the JPL Keplerian Elements for Approximate Positions of
    the Major Planets.

    In order to do that, it only will be necessary to use the name of a planet as the class'
    argument.

    Given that the JPL keplerian elements provided for the Earth are actually the keplerian elements
    of the Earth-Moon barycenter, the name that will be provided to the class' argument will be
    'EM Bary' (this name is explicitly used by JPL in the csv file).

    Data contained in the .csv file comes from:
    https://ssd.jpl.nasa.gov/?planet_pos
    https://ssd.jpl.nasa.gov/txt/p_elem_t1.txt
    https://ssd.jpl.nasa.gov/txt/aprx_pos_planets.pdf

    The orbital period is derived from Kepler's Third Law.

    Parameters
    ----------
        planet : str
            A planet name, e.g 'Mars'

    Attributes
    ----------
        a : float
            Semi-major axis [km]
        e : float
            Eccentricity [radians]
        I : float
            Inclination [degrees]
        L : float
            Mean longitude [degrees]
        w : float
            Longitude of perihelion [degrees]
        W : float
            Longitude of the ascending node [degrees]
        T : float
            Orbital Period [s]

    """

    # Local pylint disabled commands
    # pylint: disable=invalid-name

    def __init__(self, planet='EM Bary'):
        
        load_kernels()
        
        """Inits PlanetKeplerianElements class."""
        self.a,\
        self.e,\
        self.I,\
        self.L,\
        self.w,\
        self.W = PlanetKeplerianElements.get_planet_kep_elements(planet)
        self.T = self.planet_period()

    @staticmethod
    def get_planet_kep_elements(planet='EM Bary'):
        """
        Method to get the jpl Keplerian Elements for Approximate Positions of the Major Planets.
        """
        filename = os.path.join(dirname, 'planetKepElem.csv')
        with open(filename, newline='') as file:
            # Create list from csv file
            planet_kep_elements = list(csv.reader(cvsdc.decomment(file), delimiter=';'))

        # Make list flat and delete empty entries
        flat_planet_kep_elements = [item for row in planet_kep_elements for item in row if item != '']

        # Identify indexes of the flat list where the name of the planets appear
        row_start = [index for index, value in enumerate(flat_planet_kep_elements) if bool(re.match(r'^[a-zA-z\s]+$', value))]

        # Assign a list of final index to delimit one row for each planet name
        row_end = row_start[1:]
        row_end.append(len(flat_planet_kep_elements))  # this adds a final index after the last planet name 

        # Create the list of planet's names
        planets_name = [flat_planet_kep_elements[idx] for idx in row_start]

        # Create the array of all the keplerian elements
        planet_kep_elem = np.array([flat_planet_kep_elements[r_start+1:r_end]
                                    for r_start, r_end
                                    in zip(row_start, row_end)],
                                   dtype=np.float64)
        
        # Identify the row corresponding to the planet query
        i = planets_name.index(planet)
        
        # Extract the information for the selected planet
        semi_major_axis = planet_kep_elem[i, 0] * spiceypy.convrt( 1, 'AU', 'km' )  # [km] cst.AU 
        eccentricity = planet_kep_elem[i, 1]  # [radians]
        inclination = planet_kep_elem[i, 2]  # [degrees]
        mean_longitude = planet_kep_elem[i, 3]  # [degrees]
        longitude_of_perihelion = planet_kep_elem[i, 4]  # [degrees]
        longitude_of_ascending_node = planet_kep_elem[i, 5]  # [degrees]

        # return semi_major_axis[0][0], eccentricity[0][0], inclination[0][0], mean_longitude[0][0], \
        #        longitude_of_perihelion[0][0], longitude_of_ascending_node[0][0]

        return semi_major_axis, eccentricity, inclination, mean_longitude, \
               longitude_of_perihelion, longitude_of_ascending_node

    def planet_period(self):
        """Method that uses Kepler's Third Law to get the planet orbital period. """
        T = 2 * math.pi * ((self.a ** 3) / spiceypy.bodvcd(10, 'GM', 1)[1][0]) ** (1 / 2)  # cst.SUN.GM

        return T  # [s]
