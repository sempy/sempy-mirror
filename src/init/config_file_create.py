# -*- coding: utf-8 -*-
"""This module will build a .ini file, which contains the default parameters that will be,
read it in the default module and then, used in the different modules that need those parameters.

Here, you will be able to add new default parameters with ConfigParser(),
and then create a new .ini file.

"""

from configparser import ConfigParser

CONFIG = ConfigParser()

CONFIG['Libration_points_default_params'] = {'libp_precision': 1e-12}

CONFIG['Differential_correction_default_params'] = \
    {'single_shooting_precision': 1e-11, 'multiple_shooting_precision': 1e-8,
     'single_shooting_maxiter': 50, 'multiple_shooting_maxiter': 20}

CONFIG['ODE_solver_default_params'] = \
    {'default_method': 'LSODA', 'default_rtol': 3e-14, 'default_atol': 1e-14,
     'default_time_steps': 1000, 'default_max_internal_steps': 1_000_000}

CONFIG['Default_plotting_options'] = {
    'XY': True, 'XZ': True, 'YZ': True, 'TD': True,
    'disp_first_pr': True, 'disp_second_pr': True,
    'N': 100, 'scale_factor': [True, 3],
    'l1': True, 'l2': True, 'l3': False, 'l4': False, 'l5': False,
    'names': [False, 'fancy'], 'legend': True, 'sci': False, 'orbit_color': ""
}

with open('./default.ini', 'w') as f:
    CONFIG.write(f)
