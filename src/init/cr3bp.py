# -*- coding: utf-8 -*-
"""
Created on Wed May 22 17:15:28 2019

"""

import numpy as np

import src.crtbp.jacobi as comp
from src.init.primary import Primary
import src.init.defaults as dft


class Cr3bp:
    """Initializes the Circular Restricted Three body Problem (CRTBP).

    Initializes a CRTBP whose primaries are defined by objects of the class Primary, m1 being the
    biggest primary and m2 the smallest one (e.g. m1 = Primary.EARTH, m2 = Primary.MOON).

    In order to do this, it is only necessary to use the class Cr3bp, for example:

    Examples
    --------
    >>> cr3bp = Cr3bp(Primary.SUN, Primary.EARTH)

    >>> cr3bp_nro = Cr3bp(Primary.EARTH, Primary.MOON)

    Parameters
    ----------
    m1 : Primary
        An object of the Primary class.
    m2 : Primary
        Another object of the Primary class, different than the previous one.

    Attributes
    ----------
    CRTBP characteristics:
    mu : float
        CRTBP mass parameter, µ [-].
    L : float
        Distance parameter, semi-major axis of m2 [km].
    T : float
        Time parameter, orbital period of m2 [s].
    R1 : float
        m1 equatorial radius [km] .
    R2 : float
        m2 equatorial radius [km].
    rh : float
        Hill's radius (adimensional formula) [-].
    l1.gamma_i : float
        L1 distance to the closest primary.
    l1.position : float
        Libration point position.
    l1.Ci : float
        L1 Jacobi constant.
    l1.Ei : float
        L1 Energy.
    l1.number : int
        Libration point number.

    l2.gamma_i : float
        L2 distance to the closest primary.
    l2.position : float
        Libration point position.
    l2.Ci : float
        L2 Jacobi constant.
    l2.Ei : float
        L2 Energy.
    l2.number : int
        Libration point number.

    l3.gamma_i : float
        L3 distance to the closest primary.
    l3.position : float
        Libration point position.
    l3.Ci : float
        L3 Jacobi constant.
    l3.Ei : float
        L3 Energy.
    l3.number : int
        Libration point number.

    l4.gamma_i : float
        L4 distance to the closest primary.
    l4.position : float
        Libration point position.
    l4.Ci : float
        L4 Jacobi constant.
    l4.Ei : float
        L4 Energy.
    l4.number : int
        Libration point number.

    l5.gamma_i : float
        L5 distance to the closest primary.
    l5.position : float
        Libration point position.
    l5.Ci : float
        L5 Jacobi constant.
    l5.Ei : float
        L5 Energy.
    l5.number : int
        Libration point number.

    m1_pos : float
        First primary's position.
    m2_pos : float
        Second primary's position.
    d_man : float
        Distance to manifold.
    name : str
        CRTBP system's name.

    Primaries features:
    m1 : object
        Primary 1.
    m2 : object
        Primary 2.
    m1.Req : float
        First primary's equatorial radius [km].
    m1.Rm : float
        First primary's mean radius [km].
    m1.M : float
        First primary's mass [kg].
    m1.GM : float
        First primary's standard gravitational parameter [km^3.s^-2].
    m1.a : float
        First primary's semi-major axis for orbit around the Sun (for the MOON it is for its orbit
        about the Earth) [km].
    Tkep : float
        First primary's orbital period for its orbit around the Sun (for the MOON it is for its
        orbit about the Earth) [s].
    Tsid_rot : float
        First primary's sidereal rotation period, is the time for one rotation of the body on its
        axis relative to the fixed stars.  A minus sign indicates retrograde rotation. [s].
    Tsid_orb : float
        First primary's sidereal orbit period, for its orbit around the Sun, relative to the fixed
        stars (for the MOON it is for its orbit about the Earth) [s].
    m1.Name : str
        First primary's name.
    m1.g : float
        First primary's surface gravity [m/s^2].
    m2.Req : float
        Second primary's equatorial radius [km].
    m2.Rm : float
        Second primary's mean radius [km].
    m2.M : float
        Second primary's mass [kg].
    m2.GM : float
        Second primary's standard gravitational parameter [km^3.s^-2].
    m2.a : float
        Second primary's semi-major axis for orbit around the Sun (for the MOON it is for its orbit
        about the Earth) [km].
    Tkep : float
        Second primary's orbital period for its orbit around the Sun (for the MOON it is for its
        orbit about the Earth) [s].
    Tsid_rot : float
        Second primary's sidereal rotation period, is the time for one rotation of the body on its
        axis relative to the fixed stars.  A minus sign indicates retrograde rotation. [s].
    Tsid_orb : float
        Second primary's sidereal orbit period, for its orbit around the Sun, relative to the fixed
        stars (for the MOON it is for its orbit about the Earth) [s].
    m2.Name : str
        Second primary's name.
    m2.g : str
        Second primary's surface gravity [m/s^2].


    """

    # Local pylint disabled commands
    # pylint: disable=too-many-instance-attributes, invalid-name

    def __init__(self, m1, m2):
        """Inits Cr3bp class."""

        # CR3PB initialization with its respective inner parameters
        self.m1, \
        self.m2, \
        self.mu, \
        self.L, \
        self.T, \
        self.R1, \
        self.R2, \
        self.rh = Cr3bp.crtbp_param(m1, m2)
        # unpacking the crtbp resulting tuple into those instance variables

        # Li initialization using Libp inner class
        self.l1 = self.Libp(1, dft.libp_precision, self.rh, self.mu)
        self.l2 = self.Libp(2, dft.libp_precision, self.rh, self.mu)
        self.l3 = self.Libp(3, dft.libp_precision, self.rh, self.mu)
        self.l4 = self.Libp(4, dft.libp_precision, self.rh, self.mu)
        self.l5 = self.Libp(5, dft.libp_precision, self.rh, self.mu)

        # Position of the primaries
        self.m1_pos, self.m2_pos = self.primary_pos()

        # Distance to the manifold
        self.d_man = self.d_manifold(m1, m2, self.L)

        # CR3PB system name
        strings = [m1.name, m2.name]
        self.name = '+'.join(strings)  # m1, '+', m2

    @staticmethod
    def crtbp_param(m1, m2):
        """Initializes a given CRTBP system. """

        # Error raise
        if m1.M < m2.M:
            raise Exception('Primary m2 can not be bigger than m1')
        if m1 == m2:
            raise Exception('Both primaries cannot be the same')

        mu = m2.GM / (m1.GM + m2.GM)  # adim
        L = m2.a  # [km] Distance parameter = semi major axis of m2
        R1 = m1.Req  # [km] m1 radius
        R2 = m2.Req  # [km] m2 radius
        rh = (mu / 3.0) ** (1.0 / 3.0)  # [-]  Hill's radius (adim formula)

        if m2 == Primary.MOON:
            T = m2.Tsid_orb  # [s]  Time parameter = sidereal orbital period
        else:
            T = m2.Tkep  # [s]  Time parameter = orbit period of m2

        return m1, m2, mu, L, T, R1, R2, rh

    def primary_pos(self):
        """Position of primaries. """
        self.m1_pos = (-self.mu, 0, 0)
        self.m2_pos = (1 - self.mu, 0, 0)

        return self.m1_pos, self.m2_pos

    @staticmethod
    def d_manifold(m1, m2, L):
        """Distance to manifold. """

        if m1 == Primary.EARTH and m2 == Primary.MOON:
            d_man = 50 / L  # 50km for the Earth-Moon system
        else:
            d_man = 100 / L  # 100km otherwise

        return d_man

    class Libp:
        """Only for Cr3bp class internal use.

        Inner class of Cr3bp class to initialize Libration points for the CRTBP.

        Parameters
        ----------
        libp_precision : float
                 Precision for the Newton-Raphson method.
        number : int
                Libration point number.

        Attributes
        ----------
        gamma_i : float
            Distance to the closest primary.
        position : float
            Libration point position.
        Ci : float
            Jacobi constant.
        Ei : float
            Energy.
        number : int
            Libration point number.

        """

        def __init__(self, number, libp_precision, rh, mu):
            """Inits Libp class."""

            self.gamma_i, \
            self.position, \
            self.Ci, \
            self.Ei, \
            self.number = Cr3bp.Libp.init_libp(number, libp_precision, rh, mu)

        @staticmethod
        def init_libp(number, libration_point_precision, rh, mu):
            """Initializes a libration point."""

            if number == 1:
                gamma_i = rh - 1 / 3.0 * rh ** 2 - 1 / 9 * rh ** 3  # initial guess
                gamma_i = Cr3bp.Libp.rtnewton(gamma_i, libration_point_precision, mu, number)
                # libp_gamma_i = gamma_i

                position = (1 - mu - gamma_i, 0, 0)

                # Energy & Jacobi constant
                Ci = comp.jacobi(position, mu)
                Ei = -0.5 * Ci

                # number = number

                return gamma_i, position, Ci, Ei, number

            if number == 2:
                gamma_i = rh + 1 / 3.0 * rh ** 2 - 1 / 9 * rh ** 3
                gamma_i = Cr3bp.Libp.rtnewton(gamma_i, libration_point_precision, mu, number)
                libp_gamma_i = gamma_i

                position = (1 - mu + gamma_i, 0, 0)

                # Energy & Jacobi constant
                Ci = comp.jacobi(position, mu)
                Ei = -0.5 * Ci

                # number = number

                return gamma_i, position, Ci, Ei, number

            if number == 3:
                gamma_i = 7 / 12.0 * mu + 237 ** 2 / 12 ** 4 * mu ** 3
                gamma_i = Cr3bp.Libp.rtnewton(gamma_i, libration_point_precision, mu, number)
                libp_gamma_i = 1 - gamma_i  # BEWARE  for L3, gamma3 = L3-M1 distance != L3-M2

                position = (-1 - mu + gamma_i, 0, 0)

                # Energy & Jacobi constant
                Ci = comp.jacobi(position, mu)
                Ei = -0.5 * Ci

                # number = number

                return libp_gamma_i, position, Ci, Ei, number

            if number == 4:
                # gamma_i: distance to the closest primary
                libp_gamma_i = 1

                position = (-mu + 0.5, 3 ** (1 / 2) / 2.0, 0)

                # Energy & Jacobi constant
                Ci = comp.jacobi(position, mu)
                Ei = -0.5 * Ci

                # number = number

                return libp_gamma_i, position, Ci, Ei, number

            if number == 5:

                # gamma_i: distance to the closest primary
                libp_gamma_i = 1

                position = (-mu + 0.5, -3 ** (1 / 2) / 2.0, 0)

                # Energy & Jacobi constant
                Ci = comp.jacobi(position, mu)
                Ei = -0.5 * Ci

                # number = number

                return libp_gamma_i, position, Ci, Ei, number

            return 0

        @staticmethod
        def polynomial_li(mu_p, number, y):
            """Provides the function value and its first derivative for the Newton-Raphson method.

            f corresponds to the equation satisfied by the
            Li-m2 distance for the L1/L2 cases and by 1-(Li-m1 distance) for the L3 case.

            """

            if number == 1:
                f = y ** 5 - ((3.0 - mu_p) * y ** 4) + ((3 - 2 * mu_p) * y ** 3) - \
                    (mu_p * y ** 2) + (2 * mu_p * y - mu_p)
                df = (5 * y ** 4) - (4 * (3.0 - mu_p) * y ** 3) + \
                     (3 * (3 - 2 * mu_p) * (y ** 2)) - (2 * mu_p * y) + (2 * mu_p)
            elif number == 2:
                f = y ** 5 + (3.0 - mu_p) * y ** 4 + (3 - 2 * mu_p) * y ** 3 - mu_p * y ** 2 - \
                    2 * mu_p * y - mu_p
                df = 5 * y ** 4 + 4 * (3.0 - mu_p) * y ** 3 + 3 * (3 - 2 * mu_p) * y ** 2 - \
                     2 * mu_p * y - 2 * mu_p
            elif number == 3:
                f = y ** 5 + (7 + mu_p) * y ** 4 + (19 + 6 * mu_p) * y ** 3 - \
                    (24 + 13 * mu_p) * y ** 2 + (12 + 14 * mu_p) * y - 7 * mu_p
                df = 5 * y ** 4 + 4 * (7 + mu_p) * y ** 3 + 3 * (19 + 6 * mu_p) * y ** 2 - \
                     2 * (24 + 13 * mu_p) * y + (12 + 14 * mu_p)
            return f, df

        @staticmethod
        def rtnewton(y, precision_gg, mu_p, number):
            """Using the Newton-Raphson method, find the root of a function known to lie
            close to y (in our case is gamma_i). The root rtnewt will be refined until its accuracy
            is known within ± precision_gg. polynomial_li is a function that returns both the
            function value and the first derivative of the function at the point gg_var."""

            gg_var = y
            while True:
                f00, df0 = Cr3bp.Libp.polynomial_li(mu_p, number, gg_var)

                if abs(f00) < precision_gg:
                    break

                gg_var = gg_var - f00 / df0
            return gg_var

    @staticmethod
    def distance_to_primary(state_vec, primary_pos):
        """Compute the distance from an orbit's state to a given primary.

        If `state_vec` is a single state (i.e. a (6,) vector), its distance from the primary
        is returned.
        If `state_vec` is a series of states (i.e. a (6,n) vector) corresponding for example
        to a propagated orbit, the maximum and minimum distances and corresponding indexes
        are returned.

        Parameters
        ----------
        state_vec : ndarray
            Single state or series of states.
        primary_pos : ndarray
            Position of the primary w.r.t. which the distance is computed.

        Returns
        -------
        dist : float or tuple
            Distance from the primary if `state_vec` is a single state,
            tuple of (minimum distance, maximum distance) if `state_vec` is a series of states.
        idx: None or tuple
            None if `state_vec` is a single state, tuple of (index minimum distance, index of
            maximum distance) if `state_vec` is a series of states.

        """

        if len(state_vec.shape) == 1:
            rel_pos = state_vec[0:3] - primary_pos
            dist = np.linalg.norm(rel_pos)
            idx = None
        else:
            rel_pos_vec = state_vec[:, 0:3] - np.asarray(primary_pos).reshape((1, 3))
            dist_vec = np.linalg.norm(rel_pos_vec, axis=1)
            dist = (np.amin(dist_vec), np.amax(dist_vec))
            idx = (dist_vec.argmin(), dist_vec.argmax())
        return dist, idx
