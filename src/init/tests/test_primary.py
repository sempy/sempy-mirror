"""
Test Primary class.

"""

import unittest
import spiceypy as sp

from src.init.load_kernels import load_kernels
from src.init.primary import Primary

load_kernels()


class TestPrimary(unittest.TestCase):

    def test_naif_id(self):
        for p in Primary:
            if p.name in ('SUN', 'EARTH', 'MOON'):
                self.assertEqual(p.naif_id, sp.bodn2c(p.name))
            elif p.name == 'EARTH_AND_MOON':
                self.assertEqual(p.naif_id, sp.bodn2c('EARTH BARYCENTER'))
            else:
                self.assertEqual(p.naif_id, sp.bodn2c(p.name + ' BARYCENTER'))

    def test_gravitational_param(self):
        for p in Primary:
            if p.name in ('SUN', 'EARTH', 'MOON'):
                self.assertEqual(p.GM, sp.bodvrd(p.name, 'GM', 1)[1][0])
            elif p.name == 'EARTH_AND_MOON':
                self.assertEqual(p.GM, sp.bodvrd('EARTH BARYCENTER', 'GM', 1)[1][0])
            else:
                self.assertEqual(p.GM, sp.bodvrd(p.name + ' BARYCENTER', 'GM', 1)[1][0])


if __name__ == '__main__':
    unittest.main()
