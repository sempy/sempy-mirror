"""
Test the function src.crtbp.dist2primary.dist2primary and the crtbp of the Halo
orbit aposelene and periselene.

"""

import unittest
import os.path
dirname = os.path.dirname(__file__)
from scipy.io import loadmat

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.orbits.halo import Halo


class TestDistToPrimaries(unittest.TestCase):  # to be moved with orbit's tests

    def test_dist2primary(self):

        # data obtained in SEMAT for an L2 southern Halo orbit with Az=12000km
        filename = os.path.join(dirname, 'data', 'test_d2p.mat')
        data = loadmat(filename, squeeze_me=True, struct_as_record=False)['d']

        # L2 southern Halo orbit with Az=12000km
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
        halo = Halo(cr3bp, cr3bp.l1, Halo.Family.northern, Azdim=15000)
        halo.computation()

        # compute minimum and maximum distances from the Moon
        dist, _ = cr3bp.distance_to_primary(halo.state_vec, cr3bp.m2_pos)

        # check minimum and maximum distances from the Moon
        self.assertAlmostEqual(dist[0], data.dmin, places=8)
        self.assertAlmostEqual(dist[1], data.dmax, places=8)

        # check periselene and aposelene radii and altitudes
        self.assertAlmostEqual(halo.m2_apsis['periapsis_radius'], data.periselene.radius,
                               places=8)
        self.assertAlmostEqual(halo.m2_apsis['periapsis_altitude'], data.periselene.altitude,
                               places=8)
        self.assertAlmostEqual(halo.m2_apsis['apoapsis_radius'],
                               data.aposelene.altitude, places=8)  # error in SEMAT, alt is radius

        # check periselene and aposelene positions
        for i in range(3):
            self.assertAlmostEqual(halo.m2_apsis['periapsis_position'][i],
                                   data.periselene.position[i], places=8)
            self.assertAlmostEqual(halo.m2_apsis['apoapsis_position'][i],
                                   data.aposelene.position[i], places=8)


if __name__ == '__main__':
    unittest.main()
