# -*- coding: utf-8 -*-


import unittest

import src.init.constants as cst


class TestInitconstants(unittest.TestCase):
    
    
    def test_Cst(self):
        """Test for the hardcoded constants."""
        
        self.assertEqual(cst.MIN2SEC, 60)
        self.assertEqual(cst.DAYS2SEC, 86400)
        self.assertEqual(cst.G, 6.67408e-20)
        self.assertEqual(cst.AU, 149597870.700)
        self.assertEqual(cst.MOON_SMA, 384400)
        
if __name__ == '__main__':
    unittest.main()
    #import xmlrunner
    #unittest.main(testRunner=xmlrunner.XMLTestRunner(output='test-reports'))
