# -*- coding: utf-8 -*-

import unittest
import scipy.io as sio
import os.path

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
import src.init.defaults as dft


dirname = os.path.dirname(__file__)


class TestInitCr3bp(unittest.TestCase):


    def test_EM_cr3bp_results(self):
        """This will test the results of the cr3bp module for the Earth-Moon system."""

        # The following data was generated using SEMAT

        filename = os.path.join(dirname, 'data', 'cr3bp_EM.mat')
        loaded_mat = sio.loadmat(filename)
        mat_dictionary = sorted(loaded_mat.keys())[-1]
        mat_values = loaded_mat[mat_dictionary]

        # m1
        m1_struct = mat_values['m1'][0][0][0][0]
        m1_name = m1_struct[0][0]
        m1_Req = m1_struct[1][0][0]
        m1_Rm = m1_struct[2][0][0]
        m1_GM = m1_struct[3][0][0]
        m1_M = m1_struct[4][0][0]
        m1_a = m1_struct[5][0][0]
        m1_T = m1_struct[6][0][0]
        m1_pos = m1_struct[7][0]

        # m2
        m2_struct = mat_values['m2'][0][0][0][0]
        m2_name = m2_struct[0][0]
        m2_Req = m2_struct[1][0][0]
        m2_Rm = m2_struct[2][0][0]
        m2_GM = m2_struct[3][0][0]
        m2_M = m2_struct[4][0][0]
        m2_a = m2_struct[5][0][0]
        m2_T = m2_struct[6][0][0]
        m2_pos = m2_struct[7][0]

        # cr3bp params
        mu = mat_values['mu'][0][0][0][0]
        L = mat_values['L'][0][0][0][0]
        T = mat_values['T'][0][0][0][0]
        R1 = mat_values['R1'][0][0][0][0]
        R2 = mat_values['R2'][0][0][0][0]
        rh = 0.159401340395938  # SEMAT value was wrong, corrected on 03/10/2020
        d_man = mat_values['d_man'][0][0][0][0]
        name = mat_values['name'][0][0][0]
        libp_p = mat_values['libp_precision'][0][0][0][0]

        # l1
        l1_struct = mat_values['l1'][0][0][0][0]
        l1_number = l1_struct[0][0][0]
        l1_gamma_i = l1_struct[1][0][0]
        l1_position = l1_struct[4][0]
        l1_Ci = l1_struct[5][0][0]
        l1_Ei = l1_struct[6][0][0]

        # l2
        l2_struct = mat_values['l2'][0][0][0][0]
        l2_number = l2_struct[0][0][0]
        l2_gamma_i = l2_struct[1][0][0]
        l2_position = l2_struct[4][0]
        l2_Ci = l2_struct[5][0][0]
        l2_Ei = l2_struct[6][0][0]

        # l3
        l3_struct = mat_values['l3'][0][0][0][0]
        l3_number = l3_struct[0][0][0]
        l3_gamma_i = l3_struct[1][0][0]
        l3_position = l3_struct[4][0]
        l3_Ci = l3_struct[5][0][0]
        l3_Ei = l3_struct[6][0][0]

        # l4
        l4_struct = mat_values['l4'][0][0][0][0]
        l4_number = l4_struct[0][0][0]
        l4_gamma_i = l4_struct[1][0][0]
        l4_position = l4_struct[2][0]
        l4_Ci = l4_struct[3][0][0]
        l4_Ei = l4_struct[4][0][0]

        # l5
        l5_struct = mat_values['l5'][0][0][0][0]
        l5_number = l5_struct[0][0][0]
        l5_gamma_i = l5_struct[1][0][0]
        l5_position = l5_struct[2][0]
        l5_Ci = l5_struct[3][0][0]
        l5_Ei = l5_struct[4][0][0]

        # the test begins
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

        # m1
        self.assertEqual(cr3bp.m1.name, m1_name)
        self.assertAlmostEqual(cr3bp.m1.Req, m1_Req)
        self.assertAlmostEqual(cr3bp.m1.Rm, m1_Rm)
        self.assertAlmostEqual(cr3bp.m1.M, m1_M)
        self.assertAlmostEqual(cr3bp.m1.GM, m1_GM)
        self.assertAlmostEqual(cr3bp.m1.a, m1_a)
        self.assertAlmostEqual(cr3bp.m1.Tkep, m1_T)
        self.assertAlmostEqual(cr3bp.m1_pos[0], m1_pos[0])
        self.assertAlmostEqual(cr3bp.m1_pos[1], m1_pos[1])
        self.assertAlmostEqual(cr3bp.m1_pos[2], m1_pos[2])

        # m2
        self.assertEqual(cr3bp.m2.name, m2_name)
        self.assertAlmostEqual(cr3bp.m2.Req, m2_Req)
        self.assertAlmostEqual(cr3bp.m2.Rm, m2_Rm)
        self.assertAlmostEqual(cr3bp.m2.M, m2_M)
        self.assertAlmostEqual(cr3bp.m2.GM, m2_GM)
        self.assertAlmostEqual(cr3bp.m2.a, m2_a)
        self.assertAlmostEqual(cr3bp.m2.Tsid_orb, m2_T)
        self.assertAlmostEqual(cr3bp.m2_pos[0], m2_pos[0])
        self.assertAlmostEqual(cr3bp.m2_pos[1], m2_pos[1])
        self.assertAlmostEqual(cr3bp.m2_pos[2], m2_pos[2])

        # cr3bp params
        self.assertAlmostEqual(cr3bp.mu, mu)
        self.assertAlmostEqual(cr3bp.L, L)
        self.assertAlmostEqual(cr3bp.T, T)
        self.assertAlmostEqual(cr3bp.R1, R1)
        self.assertAlmostEqual(cr3bp.R2, R2)
        self.assertAlmostEqual(cr3bp.rh, rh)
        self.assertAlmostEqual(cr3bp.d_man, d_man)
        self.assertEqual(cr3bp.name, name)
        self.assertEqual(dft.libp_precision, libp_p)

        # l1
        self.assertEqual(cr3bp.l1.number, l1_number)
        self.assertAlmostEqual(cr3bp.l1.gamma_i, l1_gamma_i)

        # testing a tuple that is almost equal
        self.assertAlmostEqual(cr3bp.l1.position[0], l1_position[0])
        self.assertAlmostEqual(cr3bp.l1.position[1], l1_position[1])
        self.assertAlmostEqual(cr3bp.l1.position[2], l1_position[2])

        self.assertAlmostEqual(cr3bp.l1.Ci, l1_Ci)
        self.assertAlmostEqual(cr3bp.l1.Ei, l1_Ei)

        # l2
        self.assertEqual(cr3bp.l2.number, l2_number)
        self.assertAlmostEqual(cr3bp.l2.gamma_i, l2_gamma_i)

        # testing a tuple that is almost equal
        self.assertAlmostEqual(cr3bp.l2.position[0], l2_position[0])
        self.assertAlmostEqual(cr3bp.l2.position[1], l2_position[1])
        self.assertAlmostEqual(cr3bp.l2.position[2], l2_position[2])

        self.assertAlmostEqual(cr3bp.l2.Ci, l2_Ci)
        self.assertAlmostEqual(cr3bp.l2.Ei, l2_Ei)

        # l3
        self.assertEqual(cr3bp.l3.number, l3_number)
        self.assertAlmostEqual(cr3bp.l3.gamma_i, l3_gamma_i)

        # testing a tuple that is almost equal
        self.assertAlmostEqual(cr3bp.l3.position[0], l3_position[0])
        self.assertAlmostEqual(cr3bp.l3.position[1], l3_position[1])
        self.assertAlmostEqual(cr3bp.l3.position[2], l3_position[2])

        self.assertAlmostEqual(cr3bp.l3.Ci, l3_Ci)
        self.assertAlmostEqual(cr3bp.l3.Ei, l3_Ei)

        # l4
        self.assertEqual(cr3bp.l4.number, l4_number)
        self.assertAlmostEqual(cr3bp.l4.gamma_i, l4_gamma_i)

        # testing a tuple that is almost equal
        self.assertAlmostEqual(cr3bp.l4.position[0], l4_position[0])
        self.assertAlmostEqual(cr3bp.l4.position[1], l4_position[1])
        self.assertAlmostEqual(cr3bp.l4.position[2], l4_position[2])

        self.assertAlmostEqual(cr3bp.l4.Ci, l4_Ci)
        self.assertAlmostEqual(cr3bp.l4.Ei, l4_Ei)

        # l5
        self.assertEqual(cr3bp.l5.number, l5_number)
        self.assertAlmostEqual(cr3bp.l5.gamma_i, l5_gamma_i)

        # testing a tuple that is almost equal
        self.assertAlmostEqual(cr3bp.l5.position[0], l5_position[0])
        self.assertAlmostEqual(cr3bp.l5.position[1], l5_position[1])
        self.assertAlmostEqual(cr3bp.l5.position[2], l5_position[2])

        self.assertAlmostEqual(cr3bp.l5.Ci, l5_Ci)
        self.assertAlmostEqual(cr3bp.l5.Ei, l5_Ei)

    def test_SE_cr3bp_results(self):
        """This will test the results of the cr3bp module for the Sun-Earth system."""

        # The following data was generated using SEMAT

        filename = os.path.join(dirname, 'data', 'cr3bp_SE.mat')
        loaded_mat = sio.loadmat(filename)
        mat_dictionary = sorted(loaded_mat.keys())[-1]
        mat_values = loaded_mat[mat_dictionary]

        # m1
        m1_struct = mat_values['m1'][0][0][0][0]
        m1_name = m1_struct[0][0]
        m1_Req = m1_struct[1][0][0]
        m1_Rm = m1_struct[2][0][0]
        m1_GM = m1_struct[3][0][0]
        m1_M = m1_struct[4][0][0]
        m1_a = m1_struct[5][0][0]
        m1_T = m1_struct[6][0][0]
        m1_pos = m1_struct[7][0]

        # m2
        m2_struct = mat_values['m2'][0][0][0][0]
        m2_name = m2_struct[0][0]
        m2_Req = m2_struct[1][0][0]
        m2_Rm = m2_struct[2][0][0]
        m2_GM = m2_struct[3][0][0]
        m2_M = m2_struct[4][0][0]
        m2_a = m2_struct[5][0][0]
        m2_T = m2_struct[6][0][0]
        m2_pos = m2_struct[7][0]

        # cr3bp params
        mu = mat_values['mu'][0][0][0][0]
        L = mat_values['L'][0][0][0][0]
        T = mat_values['T'][0][0][0][0]
        R1 = mat_values['R1'][0][0][0][0]
        R2 = mat_values['R2'][0][0][0][0]
        rh = 0.0100038658320006  # SEMAT value was wrong, corrected on 03/10/2020
        d_man = mat_values['d_man'][0][0][0][0]
        name = mat_values['name'][0][0][0]
        libp_p = mat_values['libp_precision'][0][0][0][0]

        # l1
        l1_struct = mat_values['l1'][0][0][0][0]
        l1_number = l1_struct[0][0][0]
        l1_gamma_i = l1_struct[1][0][0]
        l1_position = l1_struct[4][0]
        l1_Ci = l1_struct[5][0][0]
        l1_Ei = l1_struct[6][0][0]

        # l2
        l2_struct = mat_values['l2'][0][0][0][0]
        l2_number = l2_struct[0][0][0]
        l2_gamma_i = l2_struct[1][0][0]
        l2_position = l2_struct[4][0]
        l2_Ci = l2_struct[5][0][0]
        l2_Ei = l2_struct[6][0][0]

        # l3
        l3_struct = mat_values['l3'][0][0][0][0]
        l3_number = l3_struct[0][0][0]
        l3_gamma_i = l3_struct[1][0][0]
        l3_position = l3_struct[4][0]
        l3_Ci = l3_struct[5][0][0]
        l3_Ei = l3_struct[6][0][0]

        # l4
        l4_struct = mat_values['l4'][0][0][0][0]
        l4_number = l4_struct[0][0][0]
        l4_gamma_i = l4_struct[1][0][0]
        l4_position = l4_struct[2][0]
        l4_Ci = l4_struct[3][0][0]
        l4_Ei = l4_struct[4][0][0]

        # l5
        l5_struct = mat_values['l5'][0][0][0][0]
        l5_number = l5_struct[0][0][0]
        l5_gamma_i = l5_struct[1][0][0]
        l5_position = l5_struct[2][0]
        l5_Ci = l5_struct[3][0][0]
        l5_Ei = l5_struct[4][0][0]

        # the test begins
        cr3bp = Cr3bp(Primary.SUN, Primary.EARTH)

        # m1
        self.assertEqual(cr3bp.m1.name, m1_name)
        self.assertAlmostEqual(cr3bp.m1.Req, m1_Req)
        self.assertAlmostEqual(cr3bp.m1.Rm, m1_Rm)
        self.assertAlmostEqual(cr3bp.m1.M, m1_M)
        self.assertAlmostEqual(cr3bp.m1.GM, m1_GM)
        self.assertAlmostEqual(cr3bp.m1.a, m1_a)
        self.assertAlmostEqual(cr3bp.m1.Tkep, m1_T)
        self.assertAlmostEqual(cr3bp.m1_pos[0], m1_pos[0])
        self.assertAlmostEqual(cr3bp.m1_pos[1], m1_pos[1])
        self.assertAlmostEqual(cr3bp.m1_pos[2], m1_pos[2])

        # m2
        self.assertEqual(cr3bp.m2.name, m2_name)
        self.assertAlmostEqual(cr3bp.m2.Req, m2_Req)
        self.assertAlmostEqual(cr3bp.m2.Rm, m2_Rm)
        self.assertAlmostEqual(cr3bp.m2.M, m2_M)
        self.assertAlmostEqual(cr3bp.m2.GM, m2_GM)
        self.assertAlmostEqual(cr3bp.m2.a, m2_a)
        self.assertAlmostEqual(cr3bp.m2.Tkep, m2_T)
        self.assertAlmostEqual(cr3bp.m2_pos[0], m2_pos[0])
        self.assertAlmostEqual(cr3bp.m2_pos[1], m2_pos[1])
        self.assertAlmostEqual(cr3bp.m2_pos[2], m2_pos[2])

        # cr3bp params
        self.assertAlmostEqual(cr3bp.mu, mu)
        self.assertAlmostEqual(cr3bp.L, L)
        self.assertAlmostEqual(cr3bp.T, T)
        self.assertAlmostEqual(cr3bp.R1, R1)
        self.assertAlmostEqual(cr3bp.R2, R2)
        self.assertAlmostEqual(cr3bp.rh, rh)
        self.assertAlmostEqual(cr3bp.d_man, d_man)
        self.assertEqual(cr3bp.name, name)
        self.assertEqual(dft.libp_precision, libp_p)

        # l1
        self.assertEqual(cr3bp.l1.number, l1_number)
        self.assertAlmostEqual(cr3bp.l1.gamma_i, l1_gamma_i)

        # testing a tuple that is almost equal
        self.assertAlmostEqual(cr3bp.l1.position[0], l1_position[0])
        self.assertAlmostEqual(cr3bp.l1.position[1], l1_position[1])
        self.assertAlmostEqual(cr3bp.l1.position[2], l1_position[2])

        self.assertAlmostEqual(cr3bp.l1.Ci, l1_Ci)
        self.assertAlmostEqual(cr3bp.l1.Ei, l1_Ei)

        # l2
        self.assertEqual(cr3bp.l2.number, l2_number)
        self.assertAlmostEqual(cr3bp.l2.gamma_i, l2_gamma_i)

        # testing a tuple that is almost equal
        self.assertAlmostEqual(cr3bp.l2.position[0], l2_position[0])
        self.assertAlmostEqual(cr3bp.l2.position[1], l2_position[1])
        self.assertAlmostEqual(cr3bp.l2.position[2], l2_position[2])

        self.assertAlmostEqual(cr3bp.l2.Ci, l2_Ci)
        self.assertAlmostEqual(cr3bp.l2.Ei, l2_Ei)

        # l3
        self.assertEqual(cr3bp.l3.number, l3_number)
        self.assertAlmostEqual(cr3bp.l3.gamma_i, l3_gamma_i)

        # testing a tuple that is almost equal
        self.assertAlmostEqual(cr3bp.l3.position[0], l3_position[0])
        self.assertAlmostEqual(cr3bp.l3.position[1], l3_position[1])
        self.assertAlmostEqual(cr3bp.l3.position[2], l3_position[2])

        self.assertAlmostEqual(cr3bp.l3.Ci, l3_Ci)
        self.assertAlmostEqual(cr3bp.l3.Ei, l3_Ei)

        # l4
        self.assertEqual(cr3bp.l4.number, l4_number)
        self.assertAlmostEqual(cr3bp.l4.gamma_i, l4_gamma_i)

        # testing a tuple that is almost equal
        self.assertAlmostEqual(cr3bp.l4.position[0], l4_position[0])
        self.assertAlmostEqual(cr3bp.l4.position[1], l4_position[1])
        self.assertAlmostEqual(cr3bp.l4.position[2], l4_position[2])

        self.assertAlmostEqual(cr3bp.l4.Ci, l4_Ci)
        self.assertAlmostEqual(cr3bp.l4.Ei, l4_Ei)

        # l5
        self.assertEqual(cr3bp.l5.number, l5_number)
        self.assertAlmostEqual(cr3bp.l5.gamma_i, l5_gamma_i)

        # testing a tuple that is almost equal
        self.assertAlmostEqual(cr3bp.l5.position[0], l5_position[0])
        self.assertAlmostEqual(cr3bp.l5.position[1], l5_position[1])
        self.assertAlmostEqual(cr3bp.l5.position[2], l5_position[2])

        self.assertAlmostEqual(cr3bp.l5.Ci, l5_Ci)
        self.assertAlmostEqual(cr3bp.l5.Ei, l5_Ei)

    def test_cr3bp_warns(self):
        """ Test that exceptions are raised."""
        with self.assertRaisesRegex(Exception, 'Primary m2 can not be bigger than m1'):
            Cr3bp(Primary.EARTH, Primary.SUN)

        with self.assertRaisesRegex(Exception, 'Both primaries cannot be the same'):
            Cr3bp(Primary.EARTH, Primary.EARTH)


if __name__ == '__main__':
    unittest.main()
