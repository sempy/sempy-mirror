# -*- coding: utf-8 -*-


import unittest

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp


class TestInitCr3bp(unittest.TestCase):

    def test_EM_cr3bp_results(self):
        """This will test the results of the cr3bp module for the Earth-Moon system."""

        # The following data was generated using SEMAT

        # m1
        m1_name = 'EARTH'
        m1_Req = 6.378140000000000e+03
        m1_Rm = 6371
        m1_M = 5.972190000000000e+24
        m1_GM = 3.986004400000000e+05
        m1_a = 149600000 
        m1_T = 3.155814950400000e+07
        m1_pos = (-0.012150581623434, 0, 0)

        # m2
        m2_name = 'MOON'
        m2_Req = 1.737500000000000e+03
        m2_Rm = 1.737500000000000e+03
        m2_M = 7.345814120628661e+22
        m2_GM = 4.902801000000000e+03
        m2_a = 384400 
        m2_T = 2.360584684800000e+06
        m2_pos = (0.987849418376566, 0, 0)

        # cr3bp params
        mu = 0.012150581623434
        L = 384400
        T = 2.360584684800000e+06
        R1 = 6.378140000000000e+03
        R2 = 1.737500000000000e+03
        rh = 0.159401340395938  # SEMAT value was wrong, corrected on 03/10/2020
        d_man = 1.300728407908429e-04
        name = 'EARTH+MOON'
        # l1
        l1_number = 1
        l1_gamma_i = 0.150934272990064
        l1_position = (0.836915145386502, 0, 0)
        l1_Ci = 3.200344025980471
        l1_Ei = -1.600172012990235
        # l2 
        l2_number = 2
        l2_gamma_i = 0.167832731737070
        l2_position = (1.155682150113637, 0, 0)
        l2_Ci = 3.184163374496417
        l2_Ei = -1.592081687248209
        # l3
        l3_number = 3
        l3_gamma_i = 0.992912065517993
        l3_position = (-1.005062647141427, 0, 0)
        l3_Ci = 3.024150091686332
        l3_Ei = -1.512075045843166
        # l4
        l4_number = 4
        l4_gamma_i = 1
        l4_position = (0.487849418376566, 0.866025403784439, 0)
        l4_Ci = 3
        l4_Ei = -1.500000000000000        
        # l5
        l5_number = 5
        l5_gamma_i = 1
        l5_position = (0.487849418376566, -0.866025403784439, 0)
        l5_Ci = 3
        l5_Ei = -1.500000000000000

        # the test begins
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

        # m1
        self.assertEqual(cr3bp.m1.name, m1_name)
        self.assertAlmostEqual(cr3bp.m1.Req, m1_Req, places=2)
        self.assertAlmostEqual(cr3bp.m1.Rm, m1_Rm, places=1)

        # it is within the error of: +- 0.0006e24 kg (https://en.wikipedia.org/wiki/Earth_mass)
        self.assertAlmostEqual(cr3bp.m1.M, m1_M, delta=1.7526137079345657e+20)
        self.assertAlmostEqual(cr3bp.m1.GM, m1_GM, places=2)
        self.assertAlmostEqual(cr3bp.m1.a, m1_a, delta=1738.8495574891567)
        self.assertAlmostEqual(cr3bp.m1.Tkep, m1_T, delta=170.0618127770722)
        self.assertAlmostEqual(cr3bp.m1_pos[0], m1_pos[0])
        self.assertAlmostEqual(cr3bp.m1_pos[1], m1_pos[1])
        self.assertAlmostEqual(cr3bp.m1_pos[2], m1_pos[2])

        # m2
        self.assertEqual(cr3bp.m2.name, m2_name)
        self.assertAlmostEqual(cr3bp.m2.Req, m2_Req, delta=0.09999999999990905)
        self.assertAlmostEqual(cr3bp.m2.Rm, m2_Rm, delta=0.09999999999968168)
        self.assertAlmostEqual(cr3bp.m2.M, m2_M, delta=2.1719179761465426e+18) 
        self.assertAlmostEqual(cr3bp.m2.GM, m2_GM, places=2)
        self.assertAlmostEqual(cr3bp.m2.a, m2_a, delta=0)
        self.assertAlmostEqual(cr3bp.m2.Tsid_rot, m2_T, delta=36.11519999988377)
        self.assertAlmostEqual(cr3bp.m2_pos[0], m2_pos[0]) 
        self.assertAlmostEqual(cr3bp.m2_pos[1], m2_pos[1])
        self.assertAlmostEqual(cr3bp.m2_pos[2], m2_pos[2])

        # cr3bp params
        self.assertAlmostEqual(cr3bp.mu, mu)
        self.assertAlmostEqual(cr3bp.L, L)
        self.assertAlmostEqual(cr3bp.T, T, delta=6.7392000001855195)
        self.assertAlmostEqual(cr3bp.R1, R1, places=2)
        self.assertAlmostEqual(cr3bp.R2, R2, delta=0.09999999999990905)
        self.assertAlmostEqual(cr3bp.rh, rh)
        self.assertAlmostEqual(cr3bp.d_man, d_man)
        self.assertEqual(cr3bp.name, name)

        # l1
        self.assertEqual(cr3bp.l1.number, l1_number)
        self.assertAlmostEqual(cr3bp.l1.gamma_i, l1_gamma_i)
        self.assertAlmostEqual(cr3bp.l1.position[0], l1_position[0]) 
        self.assertAlmostEqual(cr3bp.l1.position[1], l1_position[1])
        self.assertAlmostEqual(cr3bp.l1.position[2], l1_position[2])
        self.assertAlmostEqual(cr3bp.l1.Ci, l1_Ci)
        self.assertAlmostEqual(cr3bp.l1.Ei, l1_Ei)

        # l2
        self.assertEqual(cr3bp.l2.number, l2_number)
        self.assertAlmostEqual(cr3bp.l2.gamma_i, l2_gamma_i)
        self.assertAlmostEqual(cr3bp.l2.position[0], l2_position[0]) 
        self.assertAlmostEqual(cr3bp.l2.position[1], l2_position[1])
        self.assertAlmostEqual(cr3bp.l2.position[2], l2_position[2])
        self.assertAlmostEqual(cr3bp.l2.Ci, l2_Ci)
        self.assertAlmostEqual(cr3bp.l2.Ei, l2_Ei)

        # l3
        self.assertEqual(cr3bp.l3.number, l3_number)
        self.assertAlmostEqual(cr3bp.l3.gamma_i, l3_gamma_i)
        self.assertAlmostEqual(cr3bp.l3.position[0], l3_position[0]) 
        self.assertAlmostEqual(cr3bp.l3.position[1], l3_position[1])
        self.assertAlmostEqual(cr3bp.l3.position[2], l3_position[2])
        self.assertAlmostEqual(cr3bp.l3.Ci, l3_Ci)
        self.assertAlmostEqual(cr3bp.l3.Ei, l3_Ei)       

        # l4
        self.assertEqual(cr3bp.l4.number, l4_number)
        self.assertAlmostEqual(cr3bp.l4.gamma_i, l4_gamma_i)
        self.assertAlmostEqual(cr3bp.l4.position[0], l4_position[0]) 
        self.assertAlmostEqual(cr3bp.l4.position[1], l4_position[1])
        self.assertAlmostEqual(cr3bp.l4.position[2], l4_position[2])
        self.assertAlmostEqual(cr3bp.l4.Ci, l4_Ci)
        self.assertAlmostEqual(cr3bp.l4.Ei, l4_Ei) 

        # l5
        self.assertEqual(cr3bp.l5.number, l5_number)
        self.assertAlmostEqual(cr3bp.l5.gamma_i, l5_gamma_i)
        self.assertAlmostEqual(cr3bp.l5.position[0], l5_position[0]) 
        self.assertAlmostEqual(cr3bp.l5.position[1], l5_position[1])
        self.assertAlmostEqual(cr3bp.l5.position[2], l5_position[2])
        self.assertAlmostEqual(cr3bp.l5.Ci, l5_Ci)
        self.assertAlmostEqual(cr3bp.l5.Ei, l5_Ei) 

    def test_SE_cr3bp_results(self):
        """This will test the results of the cr3bp module for the Sun-Earth system."""

        # The following data was generated using SEMAT

        # m1
        m1_name = 'SUN'
        m1_Req = 696342
        m1_Rm = 696342
        m1_M = 1.988544000000000e+30
        m1_GM = 1.327124400419394e+11
        m1_a = 0 
        m1_T = 0
        m1_pos = (-3.003288870544600e-06, 0, 0)
        
        # m2
        m2_name = 'EARTH'
        m2_Req = 6.378140000000000e+03
        m2_Rm = 6371
        m2_M = 5.972190000000000e+24
        m2_GM = 3.986004400000000e+05
        m2_a = 149600000 
        m2_T = 3.155814950400000e+07
        m2_pos = (0.999996996711129, 0, 0)
        
        # cr3bp params       
        mu = 3.003288870544600e-06
        L = 149600000
        T = 3.155814950400000e+07
        R1 = 696342
        R2 = 6.378140000000000e+03
        rh = 0.0100038658320006
        d_man = 6.684491978609626e-07
        name = 'SUN+EARTH'
        # l1
        l1_number = 1
        l1_gamma_i = 0.009970191208209
        l1_position = (0.990026805502921, 0, 0)
        l1_Ci = 3.000893659413626
        l1_Ei = -1.500446829706813
        # l2  
        l2_number = 2
        l2_gamma_i = 0.010036905625124
        l2_position = (1.010033902336253, 0, 0)
        l2_Ci = 3.000889654987951
        l2_Ei = -1.500444827493975
        # l3
        l3_number = 3
        l3_gamma_i = 0.999998248081492
        l3_position = (-1.000001251370363, 0, 0)
        l3_Ci = 3.000006006568533
        l3_Ei = -1.500003003284266
        # l4
        l4_number = 4
        l4_gamma_i = 1
        l4_position = (0.499996996711129, 0.866025403784439, 0)
        l4_Ci = 3
        l4_Ei = -1.500000000000000        
        # l5
        l5_number = 5
        l5_gamma_i = 1
        l5_position = (0.499996996711129, -0.866025403784439, 0)
        l5_Ci = 3
        l5_Ei = -1.500000000000000

        # the test begins
        cr3bp = Cr3bp(Primary.SUN, Primary.EARTH)
        
        # m1
        self.assertEqual(cr3bp.m1.name, m1_name)
        self.assertAlmostEqual(cr3bp.m1.Req, m1_Req, delta=342.0)
        self.assertAlmostEqual(cr3bp.m1.Rm, m1_Rm, delta=342.0)

        # it is within the error of: +- 2x10e26 kg (https://fr.wikipedia.org/wiki/Masse_solaire)
        self.assertAlmostEqual(cr3bp.m1.M, m1_M, delta=6.858403346544563e+25)
        self.assertAlmostEqual(cr3bp.m1.GM, m1_GM, places=3)
        self.assertEqual(cr3bp.m1.a, m1_a)
        self.assertEqual(cr3bp.m1.Tkep, m1_T)
        self.assertAlmostEqual(cr3bp.m1_pos[0], m1_pos[0], places=4) 
        self.assertAlmostEqual(cr3bp.m1_pos[1], m1_pos[1])
        self.assertAlmostEqual(cr3bp.m1_pos[2], m1_pos[2])

        # m2
        self.assertEqual(cr3bp.m2.name, m2_name)
        self.assertAlmostEqual(cr3bp.m2.Req, m2_Req, places=2)
        self.assertAlmostEqual(cr3bp.m2.Rm, m2_Rm, places=1)

        # it is within the error of: +- 0.0006e24 kg (https://en.wikipedia.org/wiki/Earth_mass)
        self.assertAlmostEqual(cr3bp.m2.M, m2_M, delta=1.7526137079345657e+20)
        self.assertAlmostEqual(cr3bp.m2.GM, m2_GM, places=2)
        self.assertAlmostEqual(cr3bp.m2.a, m2_a, delta=1738.8495574891567)
        self.assertAlmostEqual(cr3bp.m2.Tkep, m2_T, delta=170.0618127770722)
        self.assertAlmostEqual(cr3bp.m2_pos[0], m2_pos[0]) 
        self.assertAlmostEqual(cr3bp.m2_pos[1], m2_pos[1])
        self.assertAlmostEqual(cr3bp.m2_pos[2], m2_pos[2])

        # cr3bp params
        self.assertAlmostEqual(cr3bp.mu, mu)
        self.assertAlmostEqual(cr3bp.L, L, delta=1738.8495574891567)
        self.assertAlmostEqual(cr3bp.T, T, delta=170.0618127770722)
        self.assertAlmostEqual(cr3bp.R1, R1, delta=342.0)
        self.assertAlmostEqual(cr3bp.R2, R2, delta=0.09999999999990905)
        self.assertAlmostEqual(cr3bp.rh, rh)
        self.assertAlmostEqual(cr3bp.d_man, d_man)
        self.assertEqual(cr3bp.name, name)

        # l1
        self.assertEqual(cr3bp.l1.number, l1_number)
        self.assertAlmostEqual(cr3bp.l1.gamma_i, l1_gamma_i, places=6)
        self.assertAlmostEqual(cr3bp.l1.position[0], l1_position[0], places=6) 
        self.assertAlmostEqual(cr3bp.l1.position[1], l1_position[1])
        self.assertAlmostEqual(cr3bp.l1.position[2], l1_position[2])
        self.assertAlmostEqual(cr3bp.l1.Ci, l1_Ci)
        self.assertAlmostEqual(cr3bp.l1.Ei, l1_Ei)

        # l2
        self.assertEqual(cr3bp.l2.number, l2_number)
        self.assertAlmostEqual(cr3bp.l2.gamma_i, l2_gamma_i, places=6)
        self.assertAlmostEqual(cr3bp.l2.position[0], l2_position[0], places=6) 
        self.assertAlmostEqual(cr3bp.l2.position[1], l2_position[1])
        self.assertAlmostEqual(cr3bp.l2.position[2], l2_position[2])
        self.assertAlmostEqual(cr3bp.l2.Ci, l2_Ci)
        self.assertAlmostEqual(cr3bp.l2.Ei, l2_Ei)

        # l3
        self.assertEqual(cr3bp.l3.number, l3_number)
        self.assertAlmostEqual(cr3bp.l3.gamma_i, l3_gamma_i)
        self.assertAlmostEqual(cr3bp.l3.position[0], l3_position[0]) 
        self.assertAlmostEqual(cr3bp.l3.position[1], l3_position[1])
        self.assertAlmostEqual(cr3bp.l3.position[2], l3_position[2])
        self.assertAlmostEqual(cr3bp.l3.Ci, l3_Ci)
        self.assertAlmostEqual(cr3bp.l3.Ei, l3_Ei)       

        # l4
        self.assertEqual(cr3bp.l4.number, l4_number)
        self.assertAlmostEqual(cr3bp.l4.gamma_i, l4_gamma_i)
        self.assertAlmostEqual(cr3bp.l4.position[0], l4_position[0]) 
        self.assertAlmostEqual(cr3bp.l4.position[1], l4_position[1])
        self.assertAlmostEqual(cr3bp.l4.position[2], l4_position[2])
        self.assertAlmostEqual(cr3bp.l4.Ci, l4_Ci)
        self.assertAlmostEqual(cr3bp.l4.Ei, l4_Ei) 

        # l5
        self.assertEqual(cr3bp.l5.number, l5_number)
        self.assertAlmostEqual(cr3bp.l5.gamma_i, l5_gamma_i)
        self.assertAlmostEqual(cr3bp.l5.position[0], l5_position[0]) 
        self.assertAlmostEqual(cr3bp.l5.position[1], l5_position[1])
        self.assertAlmostEqual(cr3bp.l5.position[2], l5_position[2])
        self.assertAlmostEqual(cr3bp.l5.Ci, l5_Ci)
        self.assertAlmostEqual(cr3bp.l5.Ei, l5_Ei) 

    def test_cr3bp_warns(self):
        """ Test that exceptions are raised."""
        with self.assertRaisesRegex(Exception, 'Primary m2 can not be bigger than m1'):
            Cr3bp(Primary.EARTH, Primary.SUN)
            
        with self.assertRaisesRegex(Exception, 'Both primaries cannot be the same'):
            Cr3bp(Primary.EARTH, Primary.EARTH)


if __name__ == '__main__':
    unittest.main()
