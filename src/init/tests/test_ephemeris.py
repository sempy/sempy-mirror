"""
Unit-tests for Ephemeris class.

"""

import unittest
import spiceypy as sp

from src.init.primary import Primary
from src.init.ephemeris import Ephemeris


class TestEphemeris(unittest.TestCase):

    def test_ephemeris(self):

        # init Ephemeris for Earth, Moon, Sun, Jupiter
        primaries = (Primary.EARTH, Primary.MOON, Primary.SUN, Primary.JUPITER)
        eph = Ephemeris(primaries)

        # test instance attributes
        for i, p in enumerate(primaries):
            self.assertEqual(eph.bodies[i], p)
            self.assertEqual(eph.naif_ids[i], p.naif_id)
            self.assertEqual(eph.gm_dim[i], p.GM)

        # test getters
        self.assertEqual(eph.get_index(Primary.MOON), 1)
        self.assertEqual(eph.get_naif_id(Primary.MOON), 301)
        self.assertEqual(eph.get_gm_dim(Primary.MOON), Primary.MOON.GM)

        # test rolling
        eph.roll(Primary.SUN)
        primaries_rolled = (Primary.SUN, Primary.JUPITER, Primary.EARTH, Primary.MOON)
        for i, p in enumerate(primaries_rolled):
            self.assertEqual(eph.bodies[i], p)
            self.assertEqual(eph.naif_ids[i], p.naif_id)
            self.assertEqual(eph.gm_dim[i], p.GM)

    def test_ephemeris_warns(self):

        with self.assertRaisesRegex(Exception, 'body must be either a Primary object or a '
                                               'non-empty tuple of unique Primary objects'):
            eph = Ephemeris((Primary.EARTH, Primary.EARTH))
        with self.assertRaisesRegex(Exception, 'body must be either a Primary object or a '
                                               'non-empty tuple of unique Primary objects'):
            eph = Ephemeris((Primary.EARTH, 'MOON'))
        with self.assertRaisesRegex(Exception, 'body must be either a Primary object or a '
                                               'non-empty tuple of unique Primary objects'):
            eph = Ephemeris(())


if __name__ == '__main__':
    unittest.main()
