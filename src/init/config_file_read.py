# -*- coding: utf-8 -*-
"""
This is just one example on how to get the default values from the .ini file

"""

from configparser import ConfigParser

PARSER = ConfigParser()

PARSER.read('default.ini')


print(PARSER.sections())

print(PARSER.get('Libration_points_default_params', 'libp_precision'),
      type(PARSER.get('Libration_points_default_params', 'libp_precision')))

print(PARSER.getfloat('Libration_points_default_params', 'libp_precision'),
      type(PARSER.getfloat('Libration_points_default_params', 'libp_precision')))

X = PARSER.getfloat('Libration_points_default_params', 'libp_precision')
