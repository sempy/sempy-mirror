# -*- coding: utf-8 -*-
"""
This module will centralize any kind of constants, and then, from here they will be imported and
used in other modules.

Planetary Constants, from NASA SPICE, JPL Keplerian Elements for Approximate Positions of the
Major Planets, as well as bodies' sidereal periods are unpacked here.

As well, in this module you will define new constants required to be used in the whole package.

"""

import math
import numpy as np
from spiceypy import kclear

import src.init.planet_keplerian_elements as pke
import src.init.planetary_constants as pc
import src.init.sidereal_periods as sp
from src.init.load_kernels import load_kernels


DAYS2SEC = 86400  # [s]
MIN2SEC = 60  # [s]

# Newtonian constant of gravitation (from CODATA 2014), also used by:
# https://ssd.jpl.nasa.gov/?planet_phys_par
G = 6.67408e-20  # [km^3 kg−1 s−2]

# AU value comes from: The Planetary and Lunar Ephemeris DE430 and DE431
# https://ipnpr.jpl.nasa.gov/progress_report/42-196/196C.pdf
# Mars high resolution gravity fields from MRO, Mars seasonal gravity, and other dynamical
# parameters
# http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.454.8581&rep=rep1&type=pdf
AU = 149597870.700  # [km]

# load kernel pool
load_kernels()

# Planetary constants form spice kernel
MERCURY = pc.PlanetaryConstants('Mercury')
VENUS = pc.PlanetaryConstants('Venus')

EARTH = pc.PlanetaryConstants('Earth')
R_EARTH_EQ = EARTH.Req  # [km]
R_EARTH_M = EARTH.Rm  # [km]
GM_EARTH = EARTH.GM  # [km^3*s^-2]
M_EARTH = EARTH.M  # [kg]

MARS = pc.PlanetaryConstants('Mars')
JUPITER = pc.PlanetaryConstants('Jupiter')
SATURN = pc.PlanetaryConstants('Saturn')
URANUS = pc.PlanetaryConstants('Uranus')
NEPTUNE = pc.PlanetaryConstants('Neptune')
PLUTO = pc.PlanetaryConstants('Pluto')
SUN = pc.PlanetaryConstants('Sun')
MOON = pc.PlanetaryConstants('Moon')
EARTH_AND_MOON = pc.PlanetaryConstants('Earth and Moon')

# unload kernels
kclear()

# Moon semi-major axis and its period (for orbit about the Earth)
# https://nssdc.gsfc.nasa.gov/planetary/factsheet/moonfact.html
# Lunar Constants and Models Document
# de430_moon
MOON_SMA = 384400  # [km]
MOON_PERIOD = 2 * math.pi * ((MOON_SMA ** 3) / EARTH.GM) ** (1 / 2)  # [s] Using Kepler's Third Law

# Moon mean motion from JLP HORIZON System (https://ssd.jpl.nasa.gov/?horizons)
MOON_OMEGA_MEAN = 2.6616995e-6  # [rad*s^-1]
MOON_SYN_P = 29.530588853 * DAYS2SEC  # Moon synodic period [s]

# JPL Planets Keplerian Elements
MERCURY_KEP_ELEM = pke.PlanetKeplerianElements('Mercury')
VENUS_KEP_ELEM = pke.PlanetKeplerianElements('Venus')
EARTH_KEP_ELEM = pke.PlanetKeplerianElements('EM Bary')  # Earth-Moon Barycenter
MARS_KEP_ELEM = pke.PlanetKeplerianElements('Mars')
JUPITER_KEP_ELEM = pke.PlanetKeplerianElements('Jupiter')
SATURN_KEP_ELEM = pke.PlanetKeplerianElements('Saturn')
URANUS_KEP_ELEM = pke.PlanetKeplerianElements('Uranus')
NEPTUNE_KEP_ELEM = pke.PlanetKeplerianElements('Neptune')
PLUTO_KEP_ELEM = pke.PlanetKeplerianElements('Pluto')

# Bodies' sidereal periods
MERCURY_SID_P = sp.SiderealPeriods('Mercury')
VENUS_SID_P = sp.SiderealPeriods('Venus')
EARTH_SID_P = sp.SiderealPeriods('Earth')
MARS_SID_P = sp.SiderealPeriods('Mars')
JUPITER_SID_P = sp.SiderealPeriods('Jupiter')
SATURN_SID_P = sp.SiderealPeriods('Saturn')
URANUS_SID_P = sp.SiderealPeriods('Uranus')
NEPTUNE_SID_P = sp.SiderealPeriods('Neptune')
PLUTO_SID_P = sp.SiderealPeriods('Pluto')
MOON_SID_P = sp.SiderealPeriods('Moon')

# Initialisation of the STM as the identity matrix
STM0 = np.eye(6)

# Transformation matrices from STM(T12,0) to STM(T,0) or monodromy matrix. See Howell 1984,
# Three-Dimensional Periodic Halo Orbits (http://adsabs.harvard.edu/full/1984CeMec..32...53H)
A_MAT = np.eye(6)
A_MAT[1, 1] = A_MAT[3, 3] = A_MAT[5, 5] = -1.0
H1_MAT, H2_MAT = np.zeros((6, 6)), np.zeros((6, 6))
H1_MAT[0:3, 3:6] = H2_MAT[3:6, 0:3] = - np.eye(3)
H1_MAT[3:6, 0:3] = H2_MAT[0:3, 3:6] = np.eye(3)
H1_MAT[3, 4] = H2_MAT[0, 1] = -2.0
H1_MAT[4, 3] = H2_MAT[1, 0] = 2.0

# GMAT Modified Julian offset
GMAT_MJD_OFFSET = 2430000.0  # [days]
