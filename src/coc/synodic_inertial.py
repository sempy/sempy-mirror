# pylint: disable = not-an-iterable
"""
Change of coordinates from synodic to inertial reference frame and vice-versa.

The inertial reference frame is defined such that its x, y, and z axes are aligned with
the x, y, and z axes of the synodic frame for `t=0` where `t` is the time expressed in
normalized units. As a consequence, the z direction of the two frames coincides for
every `t` while the x and y directions of the synodic frame rotate w.r.t. the inertial ones
at a constant rate `n=1`.

The inertial frame origin is selected to be either the first or second primary associated
to the input Cr3bp structure.
"""

import numpy as np
from numba import jit, prange


def synodic_to_inertial(t_syn, state_syn, cr3bp, obs):
    """Change of coordinates from synodic to inertial frame.

    Parameters
    ----------
    t_syn : ndarray
        Time vector in normalized units.
    state_syn : ndarray
        State vector in synodic frame and normalized units.
    cr3bp : Cr3bp
        Cr3bp object.
    obs : Primary
        Primary object at the origin of the inertial frame.
        Must be either `m1` or `m2` of the above-defined Cr3bp structure.

    Returns
    -------
    t_in : ndarray
        Time vector in inertial frame and normalized units.
    state_in : ndarray
        State vector in inertial frame and normalized units.

    """
    state_obs = get_observer_state(cr3bp, obs)
    rot_mat = rot_mat_synodic_to_inertial(t_syn)
    return t_syn.copy(), (rot_mat @ (state_syn[:, 0:6] - state_obs).reshape(t_syn.size, 6, 1))\
        .reshape(t_syn.size, 6)


def inertial_to_synodic(t_in, state_in, cr3bp, obs):
    """Change of coordinates from inertial to synodic frame.

    Parameters
    ----------
    t_in : ndarray
        Time vector in normalized units.
    state_in : ndarray
        State vector in inertial frame and normalized units.
    cr3bp : Cr3bp
        Cr3bp object.
    obs : Primary
        Primary object at the origin of the inertial frame.
        Must be either `m1` or `m2` of the above-defined Cr3bp structure.

    Returns
    -------
    t_syn : ndarray
        Time vector in synodic frame and normalized units.
    state_syn : ndarray
        State vector in synodic frame and normalized units.

    """
    state_obs = get_observer_state(cr3bp, obs)
    state_syn = np.empty((t_in.size, 6))
    rot_mat = rot_mat_synodic_to_inertial(t_in)
    _inertial_to_synodic(state_in, state_syn, rot_mat)
    return t_in.copy(), state_syn + state_obs


@jit('void(float64[:, ::1], float64[:, ::1], float64[:, :, ::1])',
     nopython=True, nogil=True, cache=True, fastmath=True)
def _inertial_to_synodic(state_in, state_syn, rot_mat):
    """Change of coordinates from inertial to synodic frame. """
    for i in prange(state_in.shape[0]):
        state_syn[i, :] = np.linalg.solve(rot_mat[i], state_in[i, 0:6])


def rot_mat_synodic_to_inertial(t_syn):
    """Rotation matrix from synodic to inertial frame. """
    cos_t = np.cos(t_syn)
    sin_t = np.sin(t_syn)
    rot_mat = np.zeros((t_syn.size, 6, 6))
    rot_mat[:, 0, 0] = rot_mat[:, 1, 1] = rot_mat[:, 3, 3] = \
        rot_mat[:, 4, 4] = rot_mat[:, 4, 0] = cos_t
    rot_mat[:, 3, 1] = - cos_t
    rot_mat[:, 1, 0] = rot_mat[:, 4, 3] = sin_t
    rot_mat[:, 0, 1] = rot_mat[:, 3, 4] = rot_mat[:, 3, 0] = rot_mat[:, 4, 1] = - sin_t
    rot_mat[:, 2, 2] = rot_mat[:, 5, 5] = 1.0
    return rot_mat


def get_observer_state(cr3bp, obs):
    """Returns the observer state in synodic frame and normalized units. """
    if obs not in (cr3bp.m1, cr3bp.m2):
        raise Exception('obs must be either m1 or m2 associated to the input Cr3bp structure')
    pos_obs = cr3bp.m1_pos if obs == cr3bp.m1 else cr3bp.m2_pos  # observer's position vector
    return np.concatenate((pos_obs, np.zeros(3)))
