from dataclasses import dataclass
from src.utils.cspice_lib_loader import frame_encoder


class Frame:
    """Class to create a frame object.

    Attributes:
    ----------
    name : str
        Name of the frame.
    naif : bool
        If the frame name is a NAIF frame.
    kind : str
        Kind of the frame.
    name_enc : int
        Encoded name of the frame.
    """
    def __init__(self, kind, name, naif=True):
        """
        Parameters:
        ----------
        kind : str
            Kind of the frame.
        name : str
            Name of the frame.
        naif : bool
            If the frame name is a NAIF frame.
        """
        self.name = name
        self.kind = kind
        self.naif = naif
        if self.naif:
            self.name_enc = frame_encoder(self.name)

@dataclass
class SelectFrame:
    """Class to store the reference frame of the environment."""

    SYNODIC = Frame('ROT','Synodic', False)
    INERTIAL = Frame('FIX', 'Inertial', naif=False)
    J2000 = Frame('FIX', 'J2000',)
    EJ2000 = Frame('FIX', 'ECLIPJ2000')
    GALACTIC = Frame('FIX', 'GALACTIC')
    MARSIAU = Frame('FIX', 'MARSIAU')
