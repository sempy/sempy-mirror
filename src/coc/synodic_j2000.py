# pylint: disable=not-an-iterable,too-many-locals
"""
This module implements change of coordinates from the synodic reference frame defined for a CR3BP
system to the instantaneous J2000 inertial frame centered at the center of the first primary.
Instantaneous positions and velocities for both primaries and the system's barycenter as
well as their relatives distances used to build the instantaneous rotation matrices are obtained
from ephemeris data.
"""

import numpy as np
from cffi import FFI
from numba import jit, prange
from numba.core.typing.cffi_utils import register_module

from src.init.constants import EARTH_AND_MOON
from src.utils.cspice_lib_loader import frame_encoder

try:  # import CSPICE functions from CFFI wrapper
    from src.utils.libs.cspice_wrapper import lib
    from src.utils.libs import cspice_wrapper

    register_module(cspice_wrapper)
    spkgeo = lib.spkgeo_c
except ImportError:  # CFFI wrapper not found, import CSPICE functions from loaded DLL
    cspice_wrapper = lib = None
    from src.utils.cspice_lib_loader import spkgeo

ffi = FFI()  # FFI object created to pass pointers to numpy array from within JIT functions
ref_encoded = frame_encoder('J2000')  # frame name encoded as a Numpy array of 8-bit ints


def synodic_to_j2000(t_syn, state_syn, et0, cr3bp, obs=None, adim=False, bary_from_spice=False):
    """Change of coordinates from CR3BP synodic frame to J2000 inertial frame.

    Parameters
    ----------
    t_syn : ndarray
        Time vector in CR3BP normalized units.
    state_syn : ndarray
        State vector in CR3BP synodic frame and normalized units.
    et0 : float
        Initial epoch in ephemeris time (TDB seconds past J2000) [s].
    cr3bp : Cr3bp
        `Cr3bp` object.
    obs : Primary or None, optional
        Primary object at the origin of the J2000 inertial frame or `None` to perform only a
        rotation from synodic to inertial coordinates. If not `None`, it must be either `m1` or `m2`
        associated to the input CR3BP structure. Default is `None`.
    adim : bool, optional
        If ``False``, output vectors are in dimensional units of `s` and `km` for time and length
        respectively. If ``True``, output vectors are in normalized units. Default is ``False``.
    bary_from_spice : bool, optional
        If ``False``, the state of the system's barycenter is defined in synodic frame and then
        rotated in J2000 frame using the instantaneous rotation matrix. If ``True``, the state of
        the system barycenter is obtained directly in J2000 frame from SPICE. The second option is
        available for the Earth-Moon system only. Default is ``False``.

    Returns
    -------
    t_j2000 : ndarray
        Time vector in ephemeris time [s] or [-].
    state_j2000 : ndarray
        State vector in J2000 frame [km, km/s] or [-, -].

    """

    gm_sum = cr3bp.m1.GM + cr3bp.m2.GM  # sum of primaries' standard gravitational parameters [km^3/s^2]

    if obs is None:  # pure rotation from synodic to inertial
        t_j2000, state_j2000 = _synodic_to_j2000(t_syn, state_syn, et0, cr3bp.mu, cr3bp.adim_t, gm_sum,
                                                 cr3bp.m1.naif_id, cr3bp.m2.naif_id, 0)
    elif obs in (cr3bp.m1, cr3bp.m2):  # rotation and translation to primary-centered frame
        obs = 1 if obs == cr3bp.m1 else 2  # define new frame's origin
        if not bary_from_spice:  # system's barycenter state given in synodic frame
            t_j2000, state_j2000 = _synodic_to_j2000(t_syn, state_syn, et0, cr3bp.mu, cr3bp.adim_t, gm_sum,
                                                     cr3bp.m1.naif_id, cr3bp.m2.naif_id, obs)
        elif cr3bp.name == 'EARTH+MOON':  # system's barycenter state in J2000 frame from SPICE
            t_j2000, state_j2000 = _synodic_to_j2000_bary(t_syn, state_syn, et0, cr3bp.adim_t, gm_sum,
                                                          cr3bp.m1.naif_id, cr3bp.m2.naif_id,
                                                          EARTH_AND_MOON.naif_id, obs)
        else:
            raise Exception('State of the system\'s barycenter from SPICE available only for '
                            'the Earth-Moon system')
    else:
        raise Exception('obs must be either None or one between m1, m2 associated to the input '
                        'Cr3bp structure')

    if adim:  # normalization of time and state vectors in J2000 through the CR3BP constants
        t_j2000 /= cr3bp.adim_t
        state_j2000 /= cr3bp.adim_l
        state_j2000[:, 3:6] *= cr3bp.adim_t
    return t_j2000, state_j2000


def j2000_to_synodic(t_j2000, state_j2000, et0, cr3bp, obs=None, adim=False, bary_from_spice=False):
    """Change of coordinates from J2000 inertial frame to CR3BP synodic frame.

    Parameters
    ----------
    t_j2000 : ndarray
        Time vector in ephemeris time [s] or [-].
    state_j2000 : ndarray
        State vector in J2000 frame [km, km/s] or [-, -].
    et0 : float
        Initial epoch in ephemeris time (TDB seconds past J2000) [s].
    cr3bp : Cr3bp
        `Cr3bp` object.
    obs : Primary or None, optional
        Primary object at the origin of the J2000 inertial frame or `None` to perform only a
        rotation from inertial to synodic coordinates. If not ``None`, it must be either `m1` or
        `m2` associated to the input CR3BP structure. Default is `None`.
    adim : bool, optional
        If ``False``, input vectors are assumed to be in dimensional units of `s` and `km` for
        time and length respectively. If ``True``, input vectors are assumed to be in normalized
        units. Default is ``False``.
    bary_from_spice : bool, optional
        If ``False``, the state of the system's barycenter is defined in synodic frame and then
        rotated in J2000 frame using the instantaneous rotation matrix. If ``True``, the state of
        the system barycenter is obtained directly in J2000 frame from SPICE. The second option is
        available for the Earth-Moon system only. Default is ``False``.

    Returns
    -------
    t_syn : ndarray
        Time vector in CR3BP normalized units.
    state_syn : ndarray
        State vector in CR3BP synodic frame and normalized units.

    """

    gm_sum = cr3bp.m1.GM + cr3bp.m2.GM  # sum of primaries' standard gravitational parameters [km^3/s^2]

    if adim:  # dimensionalization of the time and state vectors in J2000
        t_j2000 = t_j2000.copy() * cr3bp.adim_t
        state_j2000 = state_j2000.copy() * cr3bp.adim_l
        state_j2000[:, 3:6] /= cr3bp.adim_t

    if obs is None:  # rotation from inertial to synodic only
        t_syn, state_syn = _j2000_to_synodic(t_j2000, state_j2000, et0, cr3bp.mu, cr3bp.adim_t, gm_sum,
                                             cr3bp.m1.naif_id, cr3bp.m2.naif_id, 0)
    elif obs in (cr3bp.m1, cr3bp.m2):  # rotation and translation to primary-centered frame
        obs = 1 if obs == cr3bp.m1 else 2  # define new frame's origin
        if not bary_from_spice:  # system's barycenter state given in synodic frame
            t_syn, state_syn = _j2000_to_synodic(t_j2000, state_j2000, et0, cr3bp.mu, cr3bp.adim_t, gm_sum,
                                                 cr3bp.m1.naif_id, cr3bp.m2.naif_id, obs)
        elif cr3bp.name == 'EARTH+MOON':  # system's barycenter state in J2000 frame from SPICE
            t_syn, state_syn = _j2000_to_synodic_bary(t_j2000, state_j2000, et0, cr3bp.adim_t, gm_sum,
                                                      cr3bp.m1.naif_id, cr3bp.m2.naif_id,
                                                      EARTH_AND_MOON.naif_id, obs)
        else:
            raise Exception('State of the system\'s barycenter from SPICE available only for '
                            'the Earth-Moon system')
    else:
        raise Exception('obs must be either None or one between m1, m2 associated to the input '
                        'Cr3bp structure')
    return t_syn, state_syn


@jit('float64(float64, float64[::1], float64[::1], int32, int32, float64[:, ::1])',
     nopython=True, nogil=True, fastmath=True)
def _rot_mat_synodic_to_j2000(eph_time, state_m2, light_time, m1_id, m2_id, rot_mat):
    """Instantaneous rotation matrix from synodic to J2000 inertial frame.

    Parameters
    ----------
    eph_time : float
        Ephemeris time [s].
    state_m2 : ndarray
        Second primary's state in J2000 frame [km, km/s].
    light_time : ndarray
        Light time (required by spkgps) from `m1` to `m2` [s].
    m1_id : int
        First primary's NAIF ID.
    m2_id : int
        Second primary's NAIF ID.
    rot_mat : ndarray
        Instantaneous rotation matrix.

    Returns
    -------
    l_c : float
        Instantaneous `m1-m2` distance [km].

    """

    # m2 state in J2000 frame centered at m1
    spkgeo(m2_id, eph_time, ffi.from_buffer(ref_encoded), m1_id, ffi.from_buffer(state_m2),
           ffi.from_buffer(light_time))
    l_c = np.linalg.norm(state_m2[0:3])  # m1-m2 distance [km]

    # angular momentum vector and angular velocity for m2 orbit about m1 (2BP approximation)
    h_vec = np.cross(state_m2[0:3], state_m2[3:6])
    omega = np.linalg.norm(h_vec) * l_c ** -2

    # unit vectors of the synodic frame expressed in J2000 frame
    e1syn = state_m2[0:3] / l_c
    e3syn = h_vec / np.linalg.norm(h_vec)
    e2syn = np.cross(e3syn, e1syn)

    # update rotation matrix from synodic to J2000
    rot_mat[0:3, 0] = rot_mat[3:6, 3] = e1syn
    rot_mat[0:3, 1] = rot_mat[3:6, 4] = e2syn
    rot_mat[0:3, 2] = rot_mat[3:6, 5] = e3syn
    rot_mat[3:6, 0] = omega * e2syn
    rot_mat[3:6, 1] = - omega * e1syn

    return l_c


@jit('Tuple((float64[::1], float64[:, ::1]))(float64[::1], float64[:, ::1], float64, float64, '
     'float64, float64, int32, int32, int64)', nopython=True, nogil=True, fastmath=True)
def _synodic_to_j2000(t_syn, state_syn, et0, mu_cr3bp, t_c, gm_sum, m1_id, m2_id, obs):
    """Change of coordinates from J2000 inertial frame to CR3BP synodic frame.

    The state of the system's barycenter is defined in synodic frame and then rotated in J2000
    frame using the instantaneous rotation matrix.

    Input state and time are in normalized units, outputs in dimensional units of `km` and `s` for
    lengths and time respectively.

    Parameters
    ----------
    t_syn : ndarray
        Time vector in normalized units.
    state_syn : ndarray
        State vector in synodic frame and normalized units. Might be either 6-dimensional
        (state only) or 42-dimensional (state concatenated with STM). In the second case, only the
        first 6 columns are considered and a nx6 state is returned.
    et0 : float
        Initial epoch in ephemeris time (TDB seconds past J2000) [s].
    mu_cr3bp : float
        CR3BP mass parameter.
    cr3bp.adim_t : float
        Characteristic time for dimensionalization [s].
    gm_sum : float
        Sum of primaries' gravitational constants [km3/s2].
    m1_id : int
        First primary's NAIF ID.
    m2_id : int
        Second primary's NAIF ID.
    obs : int
        Primary object at the origin of the inertial frame or `0` to perform only a rotation from
        synodic to inertial coordinates. If not `0`, it must be either `1` or `2`.

    Returns
    -------
    t_j2000 : ndarray
        Time vector in ephemeris time [s].
    state_j2000 : ndarray
        State vector in J2000 frame and dimensional units [km, km/s].

    Warnings
    --------
    For performance reasons, input arrays must be C-contiguous. If slicing a numpy array by
    columns, i.e. `state[:, 0:6]`, the obtained memory-view is not C-contiguous and an error will
    be raised. A solution is passing `numpy.ascontiguousarray(state[:, 0:6])`.
    Note that if a 42-dimensional state is available (state concatenated with STM) slicing is not
    necessary and the entire state can be passed as input thus guaranteeing its contiguity.
    If slicing along rows to pass patch points corresponding to a specific interval in time, i.e.
    `state[n:m, :]`, C contiguity of the input array is also guaranteed.

    """

    # initialization
    t_j2000 = et0 + t_syn * t_c  # ephemeris time for all states [s]
    state_j2000 = np.empty((t_syn.size, 6))  # state in J2000 frame [km, km/s]
    state_m2 = np.empty(6)  # m2 state w.r.t. m1 in J2000 frame [km, km/s]
    light_time = np.empty(1)  # m1-m2 light time (needed by spkgeo) [s]
    rot_mat = np.zeros((6, 6))  # instantaneous rotation matrix from synodic to J2000

    for i in prange(t_syn.size):

        # instantaneous characteristic length [km] and rotation matrix computed with the current epoch
        l_c_k = _rot_mat_synodic_to_j2000(t_j2000[i], state_m2, light_time, m1_id, m2_id, rot_mat)

        # instantaneous time constant recomputed as for the CR3BP model [s]
        t_c_k = np.sqrt(l_c_k ** 3 / gm_sum)

        # CR3BP state dimensionalized with instantaneous quantities
        state_syn_dim = state_syn[i, 0:6] * l_c_k
        state_syn_dim[3:6] /= t_c_k

        # State translation depending on frame origin
        if obs == 1:  # inertial frame centered on m1
            state_syn_dim[0] += mu_cr3bp * l_c_k
        elif obs == 2:  # inertial frame centered on m2
            state_syn_dim[0] += (mu_cr3bp - 1.0) * l_c_k

        # Actual rotation on the state to obtain state in J2000
        state_j2000[i, :] = rot_mat.dot(state_syn_dim)

    return t_j2000, state_j2000


@jit('Tuple((float64[::1], float64[:, ::1]))(float64[::1], float64[:, ::1], float64, float64,'
     'float64, int32, int32, int32, int64)', nopython=True, nogil=True, fastmath=True)
def _synodic_to_j2000_bary(t_syn, state_syn, et0, t_c, gm_sum, m1_id, m2_id, bary_id, obs):
    """Change of coordinates from J2000 inertial frame to CR3BP synodic frame.

    The state of the system's barycenter is obtained directly in J2000 frame from SPICE.

    Input state and time are in normalized units, outputs in dimensional units of `km` and `s` for
    lengths and time respectively.

    Parameters
    ----------
    t_syn : ndarray
        Time vector in normalized units.
    state_syn : ndarray
        State vector in synodic frame and normalized units. Might be either 6-dimensional
        (state only) or 42-dimensional (state concatenated with STM). In the second case, only the
        first 6 columns are considered and a nx6 state is returned.
    et0 : float
        Initial epoch in ephemeris time (TDB seconds past J2000) [s].
    cr3bp.adim_t : float
        Characteristic time for dimensionalization [s].
    gm_sum : float
        Sum of primaries' gravitational constants [km3/s2].
    m1_id : int
        First primary's NAIF ID.
    m2_id : int
        Second primary's NAIF ID.
    bary_id : int
        System barycenter NAIF ID.
    obs : int
        Primary object at the origin of the inertial frame, must be either `1` or `2`.

    Returns
    -------
    t_j2000 : ndarray
        Ephemeris time vector [s].
    state_j2000 : ndarray
        State vector in J2000 frame and dimensional units [km, km/s].

    Warnings
    --------
    For performance reasons, input arrays must be C-contiguous. If slicing a numpy array by
    columns, i.e. `state[:, 0:6]`, the obtained memory-view is not C-contiguous and an error will
    be raised. A solution is passing `numpy.ascontiguousarray(state[:, 0:6])`.
    Note that if a 42-dimensional state is available (state concatenated with STM) slicing is not
    necessary and the entire state can be passed as input thus guaranteeing its contiguity.
    If slicing along rows to pass patch points corresponding to a specific interval in time, i.e.
    `state[n:m, :]`, C contiguity of the input array is also guaranteed.

    """

    # Initialisation
    t_j2000 = et0 + t_syn * t_c  # ephemeris time for all states [s]
    state_j2000 = np.empty((t_syn.size, 6))  # state in J2000 frame [km, km/s]
    state_m2 = np.empty(6)  # m2 state w.r.t. m1 in J2000 frame [km, km/s]
    state_bary = np.empty(6)  # m1-m2 barycenter state w.r.t. m1 in J2000 frame [km, km/s]
    light_time = np.empty(1)  # m1-m2 light time (needed by spkgeo) [s]
    rot_mat = np.zeros((6, 6))  # instantaneous rotation matrix from synodic to J2000

    for i in prange(t_j2000.size):

        # recover state of the CR3BP system barycenter
        spkgeo(bary_id, t_j2000[i], ffi.from_buffer(ref_encoded), m1_id,
               ffi.from_buffer(state_bary), ffi.from_buffer(light_time))

        # instantaneous characteristic length [km] and rotation matrix computed with the current epoch
        l_c_k = _rot_mat_synodic_to_j2000(t_j2000[i], state_m2, light_time, m1_id, m2_id, rot_mat)

        # instantaneous time constant recomputed as for the CR3BP model [s]
        t_c_k = np.sqrt(l_c_k ** 3 / gm_sum)

        # CR3BP state dimensionalized with instantaneous quantities
        state_syn_dim = state_syn[i, 0:6] * l_c_k
        state_syn_dim[3:6] /= t_c_k

        # state translation depending on frame origin and rotation
        if obs == 1:  # inertial frame centered on m1
            state_j2000[i, :] = rot_mat.dot(state_syn_dim) + state_bary
        else:  # inertial frame centered on m2
            state_j2000[i, :] = rot_mat.dot(state_syn_dim) + state_bary - state_m2

    return t_j2000, state_j2000


@jit('Tuple((float64[::1], float64[:, ::1]))(float64[::1], float64[:, ::1], float64, float64,'
     'float64, float64, int32, int32, int64)', nopython=True, nogil=True, fastmath=True)
def _j2000_to_synodic(t_j2000, state_j2000, et0, mu_cr3bp, t_c, gm_sum, m1_id, m2_id, obs):
    """Change of coordinates from J2000 inertial frame to CR3BP synodic frame.

    The state of the system's barycenter is defined in synodic frame and then rotated in J2000
    frame using the instantaneous rotation matrix.

    Input state and time are in dimensional units of `km` and `s` for lengths and time
    respectively, output in normalized units.

    Parameters
    ----------
    t_j2000 : ndarray
        Time vector in ephemeris time [s].
    state_j2000 : ndarray
        State vector in J2000 frame and dimensional units [km, km/s]. Might be either
        6-dimensional (state only) or 42-dimensional (state concatenated with STM).
        In the second case, only the first 6 columns are considered and a nx6 state is returned.
    et0 : float
        Initial epoch in ephemeris time (TDB seconds past J2000) [s].
    mu_cr3bp : float
        CR3BP mass parameter.
    cr3bp.adim_t : float
        Characteristic time for dimensionalization [s].
    gm_sum : float
        Sum of primaries' gravitational constants [km3/s2].
    m1_id : int
        First primary's NAIF ID.
    m2_id : int
        Second primary's NAIF ID.
    obs : int
        Primary object at the origin of the inertial frame or `0` to perform only a rotation from
        inertial to synodic. If not `0`, it must be either `1` or `2`.

    Returns
    -------
    t_syn : ndarray
        Time vector in normalized units.
    state_syn : ndarray
        State vector in synodic frame and normalized units.

    Warnings
    --------
    For performance reasons, input arrays must be C-contiguous. If slicing a numpy array by
    columns, i.e. `state[:, 0:6]`, the obtained memory-view is not C-contiguous and an error will
    be raised. A solution is passing `numpy.ascontiguousarray(state[:, 0:6])`.
    Note that if a 42-dimensional state is available (state concatenated with STM) slicing is not
    necessary and the entire state can be passed as input thus guaranteeing its contiguity.
    If slicing along rows to pass patch points corresponding to a specific interval in time, i.e.
    `state[n:m, :]`, C contiguity of the input array is also guaranteed.

    """

    # initialization
    t_syn = (t_j2000 - et0) / t_c  # time in normalized units
    state_syn = np.empty((t_j2000.size, 6))  # state vector in synodic frame
    state_m2 = np.empty(6)  # m2 state w.r.t. m1 in J2000 frame [km, km/s]
    light_time = np.empty(1)  # m1-m2 light time (needed by spkgeo) [s]
    rot_mat = np.zeros((6, 6))  # instantaneous rotation matrix from synodic to J2000

    for i in prange(t_j2000.size):

        # instantaneous characteristic length [km] and rotation matrix computed with the current epoch
        l_c_k = _rot_mat_synodic_to_j2000(t_j2000[i], state_m2, light_time, m1_id, m2_id, rot_mat)

        # instantaneous time constant recomputed as for the CR3BP model [s]
        t_c_k = np.sqrt(l_c_k ** 3 / gm_sum)

        # J2000 state rotated to synodic state
        state_syn[i, :] = np.linalg.solve(rot_mat, state_j2000[i, 0:6])

        # CR3BP state dimensionalized with instantaneous quantities
        state_syn[i, :] /= l_c_k
        state_syn[i, 3:6] *= t_c_k

        # State translation depending on frame origin
        if obs == 1:  # inertial frame centered on m1
            state_syn[i, 0] -= mu_cr3bp
        elif obs == 2:  # inertial frame centered on m2
            state_syn[i, 0] += (1.0 - mu_cr3bp)

    return t_syn, state_syn


@jit('Tuple((float64[::1], float64[:, ::1]))(float64[::1], float64[:, ::1], float64, float64, '
     'float64, int32, int32, int32, int64)', nopython=True, nogil=True, fastmath=True)
def _j2000_to_synodic_bary(t_j2000, state_j2000, et0, t_c, gm_sum, m1_id, m2_id, bary_id, obs):
    """Change of coordinates from J2000 inertial frame to CR3BP synodic frame.

    The state of the system's barycenter is obtained directly in J2000 frame from SPICE.

    Input state and time are in dimensional units of `km` and `s` for lengths and time
    respectively, output in normalized units.

    Parameters
    ----------
    t_j2000 : ndarray
        Time vector in ephemeris time [s].
    state_j2000 : ndarray
        State vector in J2000 frame and dimensional units [km, km/s]. Might be either
        6-dimensional (state only) or 42-dimensional (state concatenated with STM).
        In the second case, only the first 6 columns are considered and a nx6 state is returned.
    et0 : float
        Initial epoch in ephemeris time (TDB seconds past J2000) [s].
    cr3bp.adim_t : float
        Characteristic time for dimensionalization [s].
    gm_sum : float
        Sum of primaries' gravitational constants [km3/s2].
    m1_id : int
        First primary's NAIF ID.
    m2_id : int
        Second primary's NAIF ID.
    bary_id : int
        System barycentre's NAIF ID.
    obs : int
        Primary object at the origin of the inertial frame, must be either `1` or `2`.

    Returns
    -------
    t_syn : ndarray
        Time vector in normalized units.
    state_syn : ndarray
        State vector in synodic frame and normalized units.

    Warnings
    --------
    For performance reasons, input arrays must be C-contiguous. If slicing a numpy array by
    columns, i.e. `state[:, 0:6]`, the obtained memory-view is not C-contiguous and an error will
    be raised. A solution is passing `numpy.ascontiguousarray(state[:, 0:6])`.
    Note that if a 42-dimensional state is available (state concatenated with STM) slicing is not
    necessary and the entire state can be passed as input thus guaranteeing its contiguity.
    If slicing along rows to pass patch points corresponding to a specific interval in time, i.e.
    `state[n:m, :]`, C contiguity of the input array is also guaranteed.

    """

    # initialization
    t_syn = (t_j2000 - et0) / t_c  # time in normalized units
    state_syn = np.empty((t_j2000.size, 6))  # state vector in synodic frame
    state_m2 = np.empty(6)  # m2 state w.r.t. m1 in J2000 frame [km, km/s]
    state_bary = np.empty(6)  # m1-m2 barycenter state w.r.t. m1 in J2000 frame [km, km/s]
    light_time = np.empty(1)  # m1-m2 light time (needed by spkgeo) [s]
    rot_mat = np.zeros((6, 6))  # instantaneous rotation matrix from synodic to J2000

    for i in prange(t_j2000.size):

        # recover state of the CR3BP system barycenter
        spkgeo(bary_id, t_j2000[i], ffi.from_buffer(ref_encoded), m1_id,
               ffi.from_buffer(state_bary), ffi.from_buffer(light_time))

        # instantaneous characteristic length [km] and rotation matrix computed with the current epoch
        l_c_k = _rot_mat_synodic_to_j2000(t_j2000[i], state_m2, light_time, m1_id, m2_id, rot_mat)

        # instantaneous time constant recomputed as for the CR3BP model [s]
        t_c_k = np.sqrt(l_c_k ** 3 / gm_sum)

        # state translation depending on frame origin and rotation
        if obs == 1:  # inertial frame centered on m1
            state_syn[i, :] = np.linalg.solve(rot_mat, (state_j2000[i, 0:6] - state_bary))
        else:  # inertial frame centered on m2
            state_syn[i, :] = \
                np.linalg.solve(rot_mat, (state_j2000[i, 0:6] - state_bary + state_m2))

        # CR3BP state is dimensionalized with instantaneous quantities
        state_syn[i, :] /= l_c_k
        state_syn[i, 3:6] *= t_c_k

    return t_syn, state_syn
