"""
Coordinate frame change from inertial to `qsw` and `tnw` local orbital frames.

"""

import numpy as np
from numba import jit
from src.coc.two_vectors_frame import two_vectors_frame


def inertial_to_qsw(state):
    """Coordinate frame change from inertial to `qsw` local orbital frame.

    The instantaneous `qsw` frame is defined as follows:
        1) x-axis along radial direction
        2) z-axis along the instantaneous angular momentum vector
        3) y-axis completes the right-handed trihedra

    Parameters
    ----------
    state : ndarray
        State vector time series in inertial frame.

    Returns
    -------
    state_qsw : ndarray
        State vector time series in `qsw` frame.
    rot_mat : ndarray
        Rotation matrix time series.

    """

    nb_states = state.shape[0]

    rot_mat = np.zeros((nb_states, 6, 6))
    rot_mat[:, 0:3, 0:3] = rot_mat[:, 3:6, 3:6] = qsw_matrix(state)
    rot_mat[:, 3:6, 0:3] = - rot_mat[:, 0:3, 0:3] @ angular_velocity_tensor(state)
    return (rot_mat @ state.reshape((nb_states, 6, 1))).reshape(nb_states, 6), rot_mat


def inertial_to_tnw(state):
    """Coordinate frame change from inertial to `tnw` local orbital frame.

    The instantaneous `qsw` frame is defined as follows:
        1) x-axis along spacecraft velocity vector.
        2) z-axis along the instantaneous angular momentum vector.
        3) y-axis completes the right-handed trihedra.

    Parameters
    ----------
    state : ndarray
        State vector time series in inertial frame.

    Returns
    -------
    state_qsw : ndarray
        State vector time series in `tnw` frame.
    rot_mat : ndarray
        Rotation matrix time series.

    """

    nb_states = state.shape[0]

    rot_mat = np.zeros((nb_states, 6, 6))
    rot_mat[:, 0:3, 0:3] = rot_mat[:, 3:6, 3:6] = tnw_matrix(state)
    rot_mat[:, 3:6, 0:3] = - rot_mat[:, 0:3, 0:3] @ angular_velocity_tensor(state)
    return (rot_mat @ state.reshape((nb_states, 6, 1))).reshape(nb_states, 6), rot_mat


@jit('f8[:, :, ::1](f8[:, ::1])', nopython=True, nogil=True, fastmath=True, cache=True)
def qsw_matrix(state):
    """Rotation matrix from inertial to `qsw` local orbital frame.

    The instantaneous `qsw` frame is defined as follows:
        1) x-axis along radial direction
        2) z-axis along the instantaneous angular momentum vector
        3) y-axis completes the right-handed trihedra

    Parameters
    ----------
    state : ndarray
        State vector time series in inertial frame.

    Returns
    -------
    qsw_mat : ndarray
        Rotation matrices time series.

    """

    return two_vectors_frame(state[:, 0:3], state[:, 3:6])


@jit('f8[:, :, ::1](f8[:, ::1])', nopython=True, nogil=True, fastmath=True, cache=True)
def tnw_matrix(state):
    """Rotation matrix from inertial to `tnw` local orbital frame.

    The instantaneous `tnw` frame is defined as follows:
        1) x-axis along spacecraft velocity vector
        2) z-axis along the instantaneous angular momentum vector
        3) y-axis completes the right-handed trihedra

    Parameters
    ----------
    state : ndarray
        State vector time series in inertial frame.

    Returns
    -------
    qsw_mat : ndarray
        Rotation matrices time series.

    """

    return two_vectors_frame(state[:, 3:6], - state[:, 0:3])


@jit('f8[:, :, ::1](f8[:, ::1])', nopython=True, nogil=True, fastmath=True, cache=True)
def angular_velocity_tensor(state):
    """Returns the angular velocity tensors corresponding to a states time series.

    For a given state `x(t) = [r(t), v(t)]` the corresponding angular velocity vector `w(t)`
    and tensor `W(t)` are defined as:

    `w(t) = [wx(t), wy(t), wz(t)] = r(t) x v(t) / ||r(t)||^2`

    `W(t) = [[0, -wz(t), wy(t)],
            [wz(t), 0, -wx(t)],
            [-wy(t), wx(t), 0]]`

    Parameters
    ----------
    state : ndarray
        State vector time series.

    Returns
    -------
    w_mat : ndarray
        Angular velocity tensor time series.

    """

    r2_inv = (state[:, 0] * state[:, 0] + state[:, 1] * state[:, 1] +
              state[:, 2] * state[:, 2]) ** -1  # inverse of r squared

    w_mat = np.zeros((state.shape[0], 3, 3))
    w_mat[:, 2, 1] = (state[:, 1] * state[:, 5] - state[:, 2] * state[:, 4]) * r2_inv  # wx(t)
    w_mat[:, 0, 2] = (state[:, 2] * state[:, 3] - state[:, 0] * state[:, 5]) * r2_inv  # wy(t)
    w_mat[:, 1, 0] = (state[:, 0] * state[:, 4] - state[:, 1] * state[:, 3]) * r2_inv  # wz(t)

    # W(t) is skew-symmetric
    w_mat[:, 0, 1] = - w_mat[:, 1, 0]
    w_mat[:, 2, 0] = - w_mat[:, 0, 2]
    w_mat[:, 1, 2] = - w_mat[:, 2, 1]

    return w_mat
