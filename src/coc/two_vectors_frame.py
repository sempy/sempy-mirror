"""
Functions to define a new reference frame from two given vectors and retrieve the corresponding
transformation matrix.

"""


import numpy as np
from numba import jit


@jit('f8[:, ::1](f8[:, :])', nopython=True, nogil=True, fastmath=True, cache=True)
def norm(vec):
    """Computes the 2-norm of a vector time series.

    Parameters
    ----------
    vec : ndarray
        Vector time series as a `n x 3` ndarray with `n` number of instants in time for which
        the norm is computed.

    Returns
    -------
    vec_norm : ndarray
        2-norm of `vec` along axis 1 as a `n x 1` ndarray.

    """

    return (vec[:, 0:1] * vec[:, 0:1] + vec[:, 1:2] * vec[:, 1:2] +
            vec[:, 2:3] * vec[:, 2:3]) ** 0.5


@jit('f8[:, ::1](f8[:, :], f8[:, :])', nopython=True, nogil=True, fastmath=True, cache=True)
def cross(vec1, vec2):
    """Computes the cross product between two vector time series.

    Parameters
    ----------
    vec1 : ndarray
        First vector time series, must be a `n x 3` ndarray with `n` number of instants in time
        for which the product is computed.
    vec2 : ndarray
        Second vector time series, must be a `n x 3` ndarray with `n` number of instants in time
        for which the product is computed.

    Returns
    -------
    cross_prod : ndarray
        Cross product between `vec1` and `vec2` such that
        `cross_prod[i] = vec1[i] x vec2[i]` for every `i`.

    """

    cross_prod = np.empty(vec1.shape)
    cross_prod[:, 0] = vec1[:, 1] * vec2[:, 2] - vec1[:, 2] * vec2[:, 1]
    cross_prod[:, 1] = vec1[:, 2] * vec2[:, 0] - vec1[:, 0] * vec2[:, 2]
    cross_prod[:, 2] = vec1[:, 0] * vec2[:, 1] - vec1[:, 1] * vec2[:, 0]
    return cross_prod


@jit('f8[:, :, ::1](f8[:, :], f8[:, :])', nopython=True, nogil=True, fastmath=True, cache=True)
def two_vectors_frame(vec1, vec2):
    """Returns the rotation matrix from the initial frame in which `vec1` and `vec2` are expressed
    and the new frame defined by the same vectors.

    The two vectors frame is defined as follows:
        1) x-axis along `vec1`.
        2) z-axis along the cross product `vec1 x vec2`.
        3) y-axis completes the right-handed trihedra.

    Parameters
    ----------
    vec1 : ndarray
        First vector series.
    vec2 : ndarray
        Second vector series.

    Returns
    -------
    rot_mat : ndarray
        Rotation matrix series.

    """

    nb_cases = vec1.shape[0]  # number of cases for which the transformation matrix is computed
    cross_prod = cross(vec1, vec2)  # cross product defining the direction of the z-axis

    e_1 = vec1 / norm(vec1)  # unit vector along x-axis
    e_3 = cross_prod / norm(cross_prod)  # unit vector along z-axis
    e_2 = cross(e_3, e_1)  # unit vector along y-axis

    rot_mat = np.empty((nb_cases, 3, 3))
    rot_mat[:, 0] = e_1
    rot_mat[:, 1] = e_2
    rot_mat[:, 2] = e_3
    return rot_mat
