# pylint: disable=not-an-iterable
"""
Change of coordinate frame origin between instantaneous J2000 inertial frames.

"""

import numpy as np
from cffi import FFI
from numba import jit, prange
from numba.core.typing.cffi_utils import register_module

from src.utils.cspice_lib_loader import frame_encoder

try:  # import CSPICE functions from CFFI wrapper
    from src.utils.libs.cspice_wrapper import lib
    from src.utils.libs import cspice_wrapper

    register_module(cspice_wrapper)
    spkgeo = lib.spkgeo_c
except ImportError:  # CFFI wrapper not found, import CSPICE functions from loaded DLL
    cspice_wrapper = lib = None
    from src.utils.cspice_lib_loader import spkgeo

ffi = FFI()  # FFI object to pass pointers to numpy array from within JIT functions
ref_encoded = frame_encoder('J2000')  # frame name encoded as a Numpy array of 8-bit ints


def translation_j2000(t_vec, state_vec, m_1, m_2, t_c=1.0, l_c=1.0):
    """Change of coordinate frame origin between instantaneous J2000 inertial frames centered at
    different primary bodies.

    Parameters
    ----------
    t_vec : ndarray
        Time vector.
    state_vec : ndarray
        State vector in original reference frame centered at `m_1`.
    m_1 : Primary
        Body at the origin of the reference frame in which `state_vec` is given.
    m_2 : Primary
        Body at the origin of the target reference frame in which `state_vec` is returned.
    t_c : float, optional
        Characteristic time for adimensionalization expressed in seconds. Default is `1.0`.
    l_c : float, optional
        Characteristic length for adimensionalization expressed in kilometers. Default is `1.0`.

    Returns
    -------
    t_vec : ndarray
        Time vector.
    state_vec : ndarray
        State vector in target reference frame centered at `m_2`.

    """

    return t_vec.copy(), _translation_j2000(t_vec, state_vec, m_1.naif_id, m_2.naif_id, t_c, l_c)


@jit('float64[:, ::1](float64[::1], float64[:, ::1], int32, int32, float64, float64)',
     nopython=True, nogil=True, fastmath=True)
def _translation_j2000(t_vec, state_vec, m1_id, m2_id, t_c=1.0, l_c=1.0):
    """Change of coordinate frame origin between instantaneous J2000 inertial frames centered at
    different primary bodies.

    Parameters
    ----------
    t_vec : ndarray
        Time vector.
    state_vec : ndarray
        State vector in original reference frame centered at `m_1`.
    m1_id : int
        NAIF ID of the body at the origin of the reference frame in which `state_vec` is given.
    m2_id : int
        NAIF ID of the body at the origin of the target reference frame.
    t_c : float, optional
        Characteristic time for adimensionalization expressed in seconds. Default is `1.0`.
    l_c : float, optional
        Characteristic length for adimensionalization expressed in kilometers. Default is `1.0`.

    Returns
    -------
    state_vec : ndarray
        State vector in target reference frame centered at `m_2`.

    """

    light_time = np.empty(1)  # body 1 to body 2 light time (required by spkgps)
    state21 = np.empty(state_vec.shape)  # state vector of body 1 wrt body 2
    for i in prange(t_vec.size):  # loop over all time instants
        spkgeo(m1_id, t_vec[i] * t_c, ffi.from_buffer(ref_encoded), m2_id,
               ffi.from_buffer(state21[i, :]), ffi.from_buffer(light_time))
    state21 /= l_c  # normalization (position and velocity)
    state21[:, 3:6] *= t_c  # normalization (velocity only)
    return state_vec + state21
