"""
Test synodic to primary-centered coordinate frame change.

"""

import unittest
import os
from scipy.io import loadmat
import numpy as np

from src.init.primary import Primary
from src.dynamics.cr3bp_environment import CR3BP
from src.coc.synodic_inertial import inertial_to_synodic, synodic_to_inertial

dirname = os.path.dirname(__file__)


class TestSynodicInertial(unittest.TestCase):

    def test_synodic_inertial(self):
        """Test synodic to m2 inertial coordinate frame change. """

        data = loadmat(os.path.join(dirname, 'data', 'syn2lci.mat'),
                       squeeze_me=True, struct_as_record=False)['data']
        cr3bp = CR3BP(Primary.EARTH, Primary.MOON)

        t_lci, y_lci = synodic_to_inertial(data.tv, data.ysyn0, cr3bp, Primary.MOON)
        t_syn, y_syn = inertial_to_synodic(data.tv, y_lci, cr3bp, Primary.MOON)

        np.testing.assert_array_equal(t_lci, data.tv)
        np.testing.assert_array_equal(t_syn, data.tv)
        np.testing.assert_allclose(y_lci, data.ylci, rtol=0.0, atol=1e-16)
        np.testing.assert_allclose(y_syn, data.ysyn, rtol=0.0, atol=1e-16)


if __name__ == '__main__':
    unittest.main()
