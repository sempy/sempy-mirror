"""
Test coordinate frame change from l
"""

import os
import unittest
import numpy as np
from scipy.io import loadmat

from src.coc.local_orbital_frames import angular_velocity_tensor, qsw_matrix, tnw_matrix, \
    inertial_to_qsw, inertial_to_tnw

dirname = os.path.dirname(__file__)
data = loadmat(os.path.join(dirname, 'data', 'inertial_local_orbital.mat'),
               squeeze_me=True, struct_as_record=False)['data']


class TestInertialLocalOrbital(unittest.TestCase):

    def test_angular_velocity_tensor(self):
        state = np.random.random((10, 6))
        w_mat = angular_velocity_tensor(state)

        w_vec = np.empty((state.shape[0], 3))
        for i in range(state.shape[0]):
            rcv = np.cross(state[i, 0:3], state[i, 3:6])  # cross product between r and v
            r2i = np.linalg.norm(state[i, 0:3]) ** -2  # inverse of r squared
            w_vec[i, :] = rcv * r2i

        np.testing.assert_allclose(w_mat[:, 2, 1], w_vec[:, 0], rtol=0.0, atol=1e-15)  # wx
        np.testing.assert_allclose(w_mat[:, 1, 2], - w_vec[:, 0], rtol=0.0, atol=1e-15)  # -wx
        np.testing.assert_allclose(w_mat[:, 0, 2], w_vec[:, 1], rtol=0.0, atol=1e-15)  # wy
        np.testing.assert_allclose(w_mat[:, 2, 0], - w_vec[:, 1], rtol=0.0, atol=1e-15)  # -wy
        np.testing.assert_allclose(w_mat[:, 1, 0], w_vec[:, 2], rtol=0.0, atol=1e-15)  # wz
        np.testing.assert_allclose(w_mat[:, 0, 1], - w_vec[:, 2], rtol=0.0, atol=1e-15)  # -wz

    def test_qsw_matrix(self):
        qsw_ref = np.transpose(data.Mqsw, axes=(2, 0, 1))
        qsw_mat = qsw_matrix(np.ascontiguousarray(data.Xi))
        np.testing.assert_allclose(qsw_mat, qsw_ref, rtol=0.0, atol=5e-15)

    def test_tnw_matrix(self):
        tnw_ref = np.transpose(data.Mtnw, axes=(2, 0, 1))
        tnw_mat = tnw_matrix(np.ascontiguousarray(data.Xi))
        np.testing.assert_allclose(tnw_mat, tnw_ref, rtol=0.0, atol=5e-15)

    def test_inertial_qsw(self):
        rot_mat_ref = np.transpose(data.Jqsw, axes=(2, 0, 1))
        state_qsw, rot_mat = inertial_to_qsw(np.ascontiguousarray(data.Xi))
        np.testing.assert_allclose(state_qsw, data.Xqsw, rtol=0.0, atol=5e-15)
        np.testing.assert_allclose(rot_mat, rot_mat_ref, rtol=0.0, atol=5e-15)

    def test_inertial_tnw(self):
        rot_mat_ref = np.transpose(data.Jtnw, axes=(2, 0, 1))
        state_tnw, rot_mat = inertial_to_tnw(np.ascontiguousarray(data.Xi))
        np.testing.assert_allclose(state_tnw, data.Xtnw, rtol=0.0, atol=5e-15)
        np.testing.assert_allclose(rot_mat, rot_mat_ref, rtol=0.0, atol=5e-15)


if __name__ == '__main__':
    unittest.main()
