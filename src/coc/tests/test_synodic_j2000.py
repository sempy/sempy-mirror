"""
Unit-tests for change of coordinates in `synodic_j2000.py`.

"""

import unittest
from os import path
import numpy as np
import spiceypy as sp

from src.coc.synodic_j2000 import synodic_to_j2000, j2000_to_synodic
from src.dynamics.cr3bp_environment import CR3BP
from src.init.load_kernels import load_kernels
from src.init.primary import Primary


class TestSynodicJ2000(unittest.TestCase):

    def test_synodic_j2000_earth(self):
        """Test coordinate transformation from Earth-Moon synodic to Earth-centered inertial frames. """

        # reference data
        dirname = path.dirname(__file__)
        data = np.loadtxt(path.join(dirname, 'data', 'synodic_j2000_earth.txt'))

        t_syn = np.ascontiguousarray(data[:, 0])
        x_syn = np.ascontiguousarray(data[:, 1:7])
        t_j2000 = np.ascontiguousarray(data[:, 7])
        x_j2000 = np.ascontiguousarray(data[:, 8:])

        # CR3BP system
        cr3bp = CR3BP(Primary.EARTH, Primary.MOON)

        # test synodic -> J2000
        t_j2000_2, x_j2000_2 = synodic_to_j2000(t_syn, x_syn, t_j2000[0]*cr3bp.adim_t, cr3bp,
                                                obs=cr3bp.m1, adim=True, bary_from_spice=False)

        np.testing.assert_allclose(t_j2000_2, t_j2000, rtol=0.0, atol=3e-13)
        np.testing.assert_allclose(x_j2000_2, x_j2000, rtol=0.0, atol=1e-15)

        # test J2000 -> synodic
        t_syn_2, x_syn_2 = j2000_to_synodic(t_j2000, x_j2000, t_j2000[0]*cr3bp.adim_t, cr3bp,
                                            obs=cr3bp.m1, adim=True, bary_from_spice=False)
        np.testing.assert_allclose(t_syn_2, t_syn, rtol=0.0, atol=1e-15)
        np.testing.assert_allclose(x_syn_2, x_syn, rtol=0.0, atol=1e-15)

    def test_synodic_j2000_moon(self):
        """Test coordinate transformation from Earth-Moon synodic to Moon-centered inertial frames. """

        # reference data
        dirname = path.dirname(__file__)
        data = np.loadtxt(path.join(dirname, 'data', 'synodic_j2000_moon.txt'))

        t_syn = np.ascontiguousarray(data[:, 0])
        x_syn = np.ascontiguousarray(data[:, 1:7])
        t_j2000 = np.ascontiguousarray(data[:, 7])
        x_j2000 = np.ascontiguousarray(data[:, 8:])

        # CR3BP system
        cr3bp = CR3BP(Primary.EARTH, Primary.MOON)

        # test synodic -> J2000
        t_j2000_2, x_j2000_2 = synodic_to_j2000(t_syn, x_syn, t_j2000[0] * cr3bp.adim_t, cr3bp,
                                                obs=cr3bp.m2, adim=True, bary_from_spice=False)
        np.testing.assert_allclose(t_j2000_2, t_j2000, rtol=0.0, atol=3e-13)
        np.testing.assert_allclose(x_j2000_2, x_j2000, rtol=0.0, atol=1e-15)

        # test J2000 -> synodic
        t_syn_2, x_syn_2 = j2000_to_synodic(t_j2000, x_j2000, t_j2000[0] * cr3bp.adim_t, cr3bp,
                                            obs=cr3bp.m2, adim=True, bary_from_spice=False)
        np.testing.assert_allclose(t_syn_2, t_syn, rtol=0.0, atol=1e-15)
        np.testing.assert_allclose(x_syn_2, x_syn, rtol=0.0, atol=1e-15)

if __name__ == '__main__':
    load_kernels()
    unittest.main()
    sp.kclear()