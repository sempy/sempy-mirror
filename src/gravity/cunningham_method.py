"""
Cunningham's algorithm for the computation of the non-uniform gravitational potential,
acceleration and gravity gradient with respect to the position.

See Cunningham, On the Computation of the Spherical Harmonic terms needed during the Numerical
Integration of the Orbital Motion of an Artificial Satellite, 1970.
(http://adsabs.harvard.edu/full/1970CeMec...2..207C)

"""

import numpy as np
from numba import jit, prange


@jit('complex128[:, ::1](float64[::1], int64, int64)',
     nopython=True, nogil=True, cache=True, fastmath=True)
def sh_elementary_potentials(pos, n_max, m_max):
    """Computes the elementary potentials `V(n,m)` up to degree and order `n_max`, `m_max`.

    Parameters
    ----------
    pos : ndarray
        Position vector in cartesian coordinates and body fixed reference frame.
    n_max : int
        Maximum degree for which the `V(n,m)` are computed.
    m_max : int
        Maximum order for which the `V(n,m)` are computed.

    Returns
    -------
    v_nm : ndarray
        Elementary potentials `V(n,m)`.

    """

    r_sq_inv = (pos[0] * pos[0] + pos[1] * pos[1] + pos[2] * pos[2]) ** -1
    xy_rsq = (pos[0] + pos[1] * 1.0j) * r_sq_inv
    z_rsq = pos[2] * r_sq_inv

    v_nm = np.zeros((n_max + 1, m_max + 1), dtype=np.complex128)
    v_nm[0, 0] = r_sq_inv ** 0.5
    v_nm[1, 0] = z_rsq * v_nm[0, 0]

    for i in prange(2, n_max + 1):  # zonal terms
        v_nm[i, 0] = ((2 * i - 1) * z_rsq * v_nm[i - 1, 0] -
                      (i - 1) * r_sq_inv * v_nm[i - 2, 0]) / i
    for j in prange(1, m_max + 1):  # tesseral and sectorial terms
        v_nm[j, j] = (2 * j - 1) * xy_rsq * v_nm[j - 1, j - 1]
        if j < n_max:
            v_nm[j + 1, j] = (2 * j + 1) * z_rsq * v_nm[j, j]
        for i in prange(j + 2, n_max + 1):
            v_nm[i, j] = ((2 * i - 1) * z_rsq * v_nm[i - 1, j] -
                          (i + j - 1) * r_sq_inv * v_nm[i - 2, j]) / (i - j)

    return v_nm


@jit('float64[::1](float64, float64, complex128[:, ::1], complex128[:, ::1], int64, int64)',
     nopython=True, nogil=True, cache=True, fastmath=True)
def sh_gravity_acc(gm_body, r_ref, z_nm, v_nm, n_max, m_max):
    """Computes the gravitational acceleration `dU(X)/dX` up to degree and order `n_max`, `m_max`.

    Parameters
    ----------
    gm_body : float
        Central body standard gravitational parameter.
    r_ref : float
        Reference radius.
    z_nm : ndarray
        Complex harmonics coefficients `Z(n,m) = C(n,m) + jS(n,m)` up to `n_max`, `m_max`.
    v_nm : ndarray
        Elementary potentials `V(n,m)` up to `n_max + 1`, `m_max + 1`.
    n_max : int
        Maximum degree for the spherical harmonics.
    m_max : int
        Maximum order for the spherical harmonics.

    Returns
    -------
    acc : ndarray
        Gravitational acceleration `dU(X)/dX`.

    """

    acc = np.zeros((3,))

    for j in prange(m_max + 1):  # loop over orders
        for i in prange(j, n_max + 1):  # loop over degrees

            if j == 0:  # zonal harmonics
                c_n0 = z_nm[i, 0].real
                acc[0] -= c_n0 * v_nm[i + 1, 1].real
                acc[1] -= c_n0 * v_nm[i + 1, 1].imag
                acc[2] -= (i + 1) * c_n0 * v_nm[i + 1, 0].real

            else:  # tesseral and sectorial harmonics
                c_nm = z_nm[i, j].real
                s_nm = z_nm[i, j].imag
                fac = (i - j + 1) * (i - j + 2)

                acc[0] += 0.5 * (- c_nm * v_nm[i + 1, j + 1].real -
                                 s_nm * v_nm[i + 1, j + 1].imag +
                                 fac * (c_nm * v_nm[i + 1, j - 1].real +
                                        s_nm * v_nm[i + 1, j - 1].imag))
                acc[1] += 0.5 * (- c_nm * v_nm[i + 1, j + 1].imag +
                                 s_nm * v_nm[i + 1, j + 1].real +
                                 fac * (- c_nm * v_nm[i + 1, j - 1].imag +
                                        s_nm * v_nm[i + 1, j - 1].real))
                acc[2] -= (i - j + 1) * (c_nm * v_nm[i + 1, j].real + s_nm * v_nm[i + 1, j].imag)

    return gm_body * r_ref ** -2 * acc


@jit('float64[:, ::1](float64, float64, complex128[:, ::1], complex128[:, ::1], int64, int64)',
     nopython=True, nogil=True, cache=True, fastmath=True)
def sh_gravity_gradient(gm_body, r_ref, z_nm, v_nm, n_max, m_max):
    """Computes the the gravity gradient with respect to the position `d2U(X)/dX2` up to degree
    and order `n_max`, `m_max`.

    Parameters
    ----------
    gm_body : float
        Central body standard gravitational parameter.
    r_ref : float
        Reference radius.
    z_nm : ndarray
        Complex harmonics coefficients `Z(n,m) = C(n,m) + jS(n,m)` up to `n_max`, `m_max`.
    v_nm : ndarray
        Elementary potentials `V(n,m)` up to `n_max + 2`, `m_max + 2`.
    n_max : int
        Maximum degree for the spherical harmonics.
    m_max : int
        Maximum order for the spherical harmonics.

    Returns
    -------
    grav_grad : ndarray
        Gravity gradient `d2U(X)/dX2`.

    """

    grav_grad = np.zeros((3, 3))

    for j in prange(m_max + 1):  # loop over orders
        for i in prange(j, n_max + 1):  # loop over degrees

            if j == 0:  # zonal harmonics
                c_n0 = z_nm[i, 0].real
                fac1 = (i + 1) * (i + 2)
                grav_grad[0, 0] += 0.5 * c_n0 * (v_nm[i + 2, 2].real - fac1 * v_nm[i + 2, 0].real)
                grav_grad[0, 1] += 0.5 * c_n0 * v_nm[i + 2, 2].imag
                grav_grad[0, 2] += (i + 1) * c_n0 * v_nm[i + 2, 1].real
                grav_grad[1, 1] -= 0.5 * c_n0 * (v_nm[i + 2, 2].real + fac1 * v_nm[i + 2, 0].real)
                grav_grad[1, 2] += (i + 1) * c_n0 * v_nm[i + 2, 1].imag
                grav_grad[2, 2] += fac1 * c_n0 * v_nm[i + 2, 0].real

            elif j == 1:  # tesseral and sectorial harmonics (first-order terms)
                c_n1 = z_nm[i, 1].real
                s_n1 = z_nm[i, 1].imag
                fac0 = i * (i + 1)
                fac1 = (i + 1) * (i + 2)

                grav_grad[0, 0] += 0.25 * (c_n1 * v_nm[i + 2, 3].real +
                                           s_n1 * v_nm[i + 2, 3].imag -
                                           fac0 * (3.0 * c_n1 * v_nm[i + 2, 1].real +
                                                   s_n1 * v_nm[i + 2, 1].imag))
                grav_grad[0, 1] += 0.25 * (c_n1 * v_nm[i + 2, 3].imag -
                                           s_n1 * v_nm[i + 2, 3].real -
                                           fac0 * (c_n1 * v_nm[i + 2, 1].imag +
                                                   s_n1 * v_nm[i + 2, 1].real))
                grav_grad[0, 2] += 0.5 * i * (c_n1 * v_nm[i + 2, 2].real +
                                              s_n1 * v_nm[i + 2, 2].imag -
                                              fac1 * c_n1 * v_nm[i + 2, 0].real)
                grav_grad[1, 1] -= 0.25 * (c_n1 * v_nm[i + 2, 3].real +
                                           s_n1 * v_nm[i + 2, 3].imag +
                                           fac0 * (c_n1 * v_nm[i + 2, 1].real +
                                                   3.0 * s_n1 * v_nm[i + 2, 1].imag))
                grav_grad[1, 2] += 0.5 * i * (c_n1 * v_nm[i + 2, 2].imag -
                                              s_n1 * v_nm[i + 2, 2].real -
                                              fac1 * s_n1 * v_nm[i + 2, 0].real)
                grav_grad[2, 2] += fac0 * (c_n1 * v_nm[i + 2, 1].real + s_n1 * v_nm[i + 2, 1].imag)

            else:  # tesseral and sectorial harmonics (higher-order terms)
                c_nm = z_nm[i, j].real
                s_nm = z_nm[i, j].imag
                fac12 = (i - j + 1) * (i - j + 2)
                fac23 = (i - j + 2) * (i - j + 3)
                fac14 = fac12 * (i - j + 3) * (i - j + 4)

                grav_grad[0, 0] += 0.25 * (c_nm * v_nm[i + 2, j + 2].real +
                                           s_nm * v_nm[i + 2, j + 2].imag -
                                           2.0 * fac12 * (c_nm * v_nm[i + 2, j].real +
                                                          s_nm * v_nm[i + 2, j].imag) +
                                           fac14 * (c_nm * v_nm[i + 2, j - 2].real +
                                                    s_nm * v_nm[i + 2, j - 2].imag))
                grav_grad[0, 1] += 0.25 * (c_nm * v_nm[i + 2, j + 2].imag -
                                           s_nm * v_nm[i + 2, j + 2].real +
                                           fac14 * (- c_nm * v_nm[i + 2, j - 2].imag +
                                                    s_nm * v_nm[i + 2, j - 2].real))
                grav_grad[0, 2] += 0.5 * (i - j + 1) * (c_nm * v_nm[i + 2, j + 1].real +
                                                        s_nm * v_nm[i + 2, j + 1].imag -
                                                        fac23 * (c_nm * v_nm[i + 2, j - 1].real +
                                                                 s_nm * v_nm[i + 2, j - 1].imag))
                grav_grad[1, 1] -= 0.25 * (c_nm * v_nm[i + 2, j + 2].real +
                                           s_nm * v_nm[i + 2, j + 2].imag +
                                           2.0 * fac12 * (c_nm * v_nm[i + 2, j].real +
                                                          s_nm * v_nm[i + 2, j].imag) +
                                           fac14 * (c_nm * v_nm[i + 2, j - 2].real +
                                                    s_nm * v_nm[i + 2, j - 2].imag))
                grav_grad[1, 2] += 0.5 * (i - j + 1) * (c_nm * v_nm[i + 2, j + 1].imag -
                                                        s_nm * v_nm[i + 2, j + 1].real +
                                                        fac23 * (c_nm * v_nm[i + 2, j - 1].imag -
                                                                 s_nm * v_nm[i + 2, j - 1].real))
                grav_grad[2, 2] += fac12 * (c_nm * v_nm[i + 2, j].real +
                                            s_nm * v_nm[i + 2, j].imag)

    grav_grad[1, 0] = grav_grad[0, 1]
    grav_grad[2, 0] = grav_grad[0, 2]
    grav_grad[2, 1] = grav_grad[1, 2]

    return gm_body * r_ref ** -3 * grav_grad


@jit('float64[::1](float64[::1], float64, float64, complex128[:, ::1], int64, int64)',
     nopython=True, nogil=True, cache=True, fastmath=True)
def sh_body_fix_acc(pos, gm_body, r_ref, z_nm, n_max, m_max):
    """Computes the gravitational acceleration due to the non-uniform gravity potential
    of the selected celestial body.

    Parameters
    ----------
    pos : ndarray
        Position vector in cartesian coordinates and body fixed reference frame.
    gm_body : float
        Central body standard gravitational parameter.
    r_ref : float
        Reference radius.
    z_nm : ndarray
        Complex harmonics coefficients `Z(n,m) = C(n,m) + jS(n,m)` up to `n_max`, `m_max`.
    n_max : int
        Maximum degree for the spherical harmonics.
    m_max : int
        Maximum order for the spherical harmonics.

    Returns
    -------
    acc : ndarray
        Gravitational acceleration in cartesian coordinates and body fixed reference frame.

    """

    v_nm = sh_elementary_potentials(pos / r_ref, n_max + 1, m_max + 1)
    return sh_gravity_acc(gm_body, r_ref, z_nm, v_nm, n_max, m_max)


@jit('Tuple((float64[::1], float64[:, ::1]))(float64[::1], float64, float64, complex128[:, ::1], '
     'int64, int64)', nopython=True, nogil=True, cache=True, fastmath=True)
def sh_body_fix_gradient(pos, gm_body, r_ref, z_nm, n_max, m_max):
    """Computes the gravitational acceleration and the gravity gradient due to the non-uniform
    gravitational potential of the selected celestial body.

    Parameters
    ----------
    pos : ndarray
        Position vector in cartesian coordinates and body fixed reference frame.
    gm_body : float
        Central body standard gravitational parameter.
    r_ref : float
        Reference radius.
    z_nm : ndarray
        Complex harmonics coefficients `Z(n,m) = C(n,m) + jS(n,m)` up to `n_max`, `m_max`.
    n_max : int
        Maximum degree for the spherical harmonics.
    m_max : int
        Maximum order for the spherical harmonics.

    Returns
    -------
    acc : ndarray
        Gravitational acceleration in cartesian coordinates and body fixed reference frame.
    grav_grad : ndarray
        Partials of the gravitational acceleration w.r.t the position.

    """

    v_nm = sh_elementary_potentials(pos / r_ref, n_max + 2, m_max + 2)
    acc = sh_gravity_acc(gm_body, r_ref, z_nm, v_nm, n_max, m_max)
    grav_grad = sh_gravity_gradient(gm_body, r_ref, z_nm, v_nm, n_max, m_max)

    return acc, grav_grad
