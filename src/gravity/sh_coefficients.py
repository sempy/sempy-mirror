# pylint: disable=too-many-locals
"""
Parses Spherical Harmonics ASCII Models to JSON files and load parsed data from JSON.

"""

import os
import json
import numpy as np
from scipy.special import factorial

from src.init.primary import Primary
from src.utils.json_encoder import NumpyEncoder

DIRNAME = os.path.dirname(__file__)


def sh_encoder(body, frame, ascii_file, json_file, nm_max=None):
    """Encodes Spherical Harmonics (SH) ASCII Models to JSON files.

    For the Moon, additional data obtained from the GRAIL mission can be downloaded from:
    (https://pds-geosciences.wustl.edu/grail/grail-l-lgrs-5-rdr-v1/grail_1001/)

    Parameters
    ----------
    body : Primary
        Primary object.
    frame : str
        Body fixed reference frame in which the data are provided. Must be a valid SPICE name.
    ascii_file : str
        Name of the ASCII file to be loaded and parsed.
        Must be located in the correct subdirectory `sempy/src/gravity/data`.
    json_file : str
        Name of the JSON file to be written.
    nm_max : int or None, optional
        Maximum SH degree and order for which data are parsed or None.
        If `None`, all data in the corresponding input file will be kept. Default is `None`.

    """

    pth_ascii = os.path.join(DIRNAME, 'data', ascii_file)  # input ASCII file
    pth_json = os.path.join(DIRNAME, 'data', json_file + '.json')  # output JSON file
    out_dict = {'body': body.name, 'frame': frame}  # output dictionary to be dumped on JSON file

    with open(pth_ascii, 'r') as fin:  # read input data

        # header line
        header = fin.readline().replace(' ', '').replace('\n', '').split(',')  # read line
        out_dict['r_ref'] = float(header[0])  # reference body radius [km]
        out_dict['GM'] = float(header[1])  # standard gravitational parameter [km^3*s^-2]
        assert int(header[3]) == int(header[4])  # verify that degree and order are the same
        nm_data = int(header[3])  # extract degree and order
        assert bool(header[5])  # verify that coefficients are normalized
        assert float(header[6]) == 0.  # verify that the reference longitude is zero
        assert float(header[7]) == 0.  # verify that the reference latitude is zero

        # Cnm, Snm coefficients
        nm_max = nm_max if (nm_max is not None and nm_max < nm_data) else nm_data
        out_dict['degree'] = nm_max  # degree of the field (parsed data)
        out_dict['order'] = nm_max  # order of the field (parsed data)
        cs_nm = np.zeros((nm_max + 1, nm_max + 1))  # matrix of Cnm, Snm coefficients
        cs_nm[0, 0] = 1.0  # central attraction
        for _ in range(int((nm_max + 1) * (nm_max + 2) / 2 - 1)):  # loop over all lines
            line = fin.readline().replace(' ', '').replace('\n', '').split(',')  # read line
            n_deg, m_ord = int(line[0]), int(line[1])  # degree and order
            cs_nm[n_deg, m_ord] = float(line[2])  # Cnm coefficient
            if m_ord > 0:
                cs_nm[m_ord - 1, n_deg] = float(line[3])  # Snm coefficient
        out_dict['CSnm'] = cs_nm

    with open(pth_json, 'w+') as fout:  # write output file
        json.dump(out_dict, fout, cls=NumpyEncoder)


def sh_provider(body, json_file, nm_max=None, inc00=False, norm=False):
    """Provides Spherical Harmonics coefficients from JSON files.

    Spherical Harmonics (SH) normalized coefficients are stored in a JSON file as a single real
    matrix `CS(n,m)` that combines `C(n,m)` and `S(n,m)` coefficients in the lower-triangular
    and upper-triangular sub-matrices respectively. These coefficients are retrieved as a single
    complex matrix `Z(n,m) = C(n,m) + jS(n,m)`.

    If `norm` is set to `True` (the default), un-normalized coefficients rather than normalized
    ones are returned. The applied multiplicative factor is defined as:

    `fac = ((n - m)! * (2n + 1) * k / (n + m)!) ** 0.5` with `k=1` for `m=0` and `k=2` otherwise

    Additional data can be produced with the function `sh_parser`.

    Parameters
    ----------
    body : Primary
        Primary object.
    json_file : str
        Name of the JSON file to be loaded.
    nm_max : int or None, optional
        Maximum SH order and degree for which data are parsed or None.
        If `None`, all data in the corresponding JSON file will be kept. Default is `None`.
    inc00 : bool, optional
        Whether to include the central term `Z(0,0)` or not. Default is `False`.
    norm : bool, optional
        Whether return normalized or un-normalized (False) coefficients. Default is `False`.

    Returns
    -------
    r_ref : float
        Body's reference radius [km]
    GM : float
        Body's standard gravitational parameter [km^3*s^-2]
    z_nm : ndarray
        `(nm_max + 1) x (nm_max + 1)` complex matrix of (un-)normalized coefficients `Z(n,m)`.
    frame : str
        Body fixed reference frame in which data are provided. It is a valid SPICE name.

    """

    # load data from JSON file
    pth_json = os.path.join(DIRNAME, 'data', json_file + '.json')
    with open(pth_json, 'r') as fid:
        data_dict = json.load(fid)

    # check that loaded data match input body
    if not body.name == data_dict['body']:
        raise Exception(f"Primary {body.name} does not match {data_dict['body']} to which the "
                        f"input JSON file refers to.")

    nm_max = nm_max if (nm_max is not None and nm_max < data_dict['degree']) \
        else data_dict['degree']  # maximum degree and order

    # complex matrix of normalized coefficients
    z_nm = np.zeros((nm_max + 1, nm_max + 1), dtype=np.complex128)
    z_nm.real = np.tril(np.asarray(data_dict['CSnm'])[0:nm_max + 1, 0:nm_max + 1])
    z_nm.imag[1:, 1:] = np.triu(np.asarray(data_dict['CSnm'])[0:nm_max, 1:nm_max + 1]).T
    z_nm[0, 0] = 1.0 + 0.0j if inc00 else 0.0 + 0.0j  # central term Z00

    if norm:  # return normalized coefficients
        return data_dict['r_ref'], data_dict['GM'], z_nm, data_dict['frame']

    # compute un-normalized coefficients
    n_mat = np.repeat(np.arange(1, nm_max + 1).reshape(nm_max, 1), nm_max, axis=1)
    m_mat = np.repeat(np.arange(1, nm_max + 1).reshape(1, nm_max), nm_max, axis=0)
    fac = np.zeros((nm_max + 1, nm_max + 1), dtype=np.longdouble)  # multiplicative factor
    fac[:, 0] = (2.0 * np.arange(nm_max + 1, dtype=np.longdouble) + 1.0) ** 0.5  # m=0, k=1
    fac[1:, 1:] = (2.0 * (2.0 * n_mat.astype(np.longdouble) + 1.0) *
                   factorial(n_mat - m_mat, True).astype(np.longdouble) /
                   factorial(n_mat + m_mat, True).astype(np.longdouble)) ** 0.5  # m>0, k=2
    z_nm_un = (z_nm * fac).astype(np.complex128)  # un-normalized coefficients
    return data_dict['r_ref'], data_dict['GM'], z_nm_un, data_dict['frame']


if __name__ == '__main__':
    # sh_encoder(Primary.MOON, 'MOON_PA_DE421', 'gggrx_0660pm_sha.tab', 'GRGM0100', nm_max=100)
    R_REF, GM_BODY, Z_NM, REF = sh_provider(Primary.MOON, 'GRGM0100', nm_max=80)
