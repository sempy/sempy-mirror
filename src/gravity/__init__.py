"""
Gravity Potential Subpackage.

Defines methods to compute the gravitational potential and acceleration due to a non-homogeneous,
non-spherical central body using Spherical Harmonics series expansions.

"""