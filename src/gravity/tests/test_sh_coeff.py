"""
Tests the function to load Spherical Harmonics data from JSON files.

"""

import os
import unittest
import numpy as np
from scipy.io import loadmat

from src.init.primary import Primary
from src.gravity.sh_coefficients import sh_provider

dirname = os.path.dirname(__file__)


class TestSHCoefficients(unittest.TestCase):

    def test_sh_provider(self):
        # load data for GRAIL model obtained in CelestLab
        fid = os.path.join(dirname, 'data', 'cunningham_model.mat')
        shd = loadmat(fid, squeeze_me=True, struct_as_record=False)['sh_data']

        # load and parse data from JSON file
        r_ref, gm_moon, z_nm, frame = sh_provider(Primary.MOON, 'GRGM0050',
                                                  nm_max=int(shd.nmmax[0, 0]),
                                                  inc00=True, norm=False)

        self.assertEqual(r_ref, shd.rf)
        self.assertEqual(gm_moon, shd.gm)
        self.assertEqual(frame, 'MOON_PA_DE421')
        np.testing.assert_allclose(z_nm, shd.znm.conj(), rtol=1e-15, atol=1e-20)


if __name__ == '__main__':
    unittest.main()
