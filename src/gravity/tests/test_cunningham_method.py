"""
Tests the Cunningham method module.

"""

import os
import unittest
import numpy as np
from scipy.io import loadmat

from src.init.primary import Primary
from src.gravity.cunningham_method import sh_body_fix_acc, sh_body_fix_gradient
from src.gravity.sh_coefficients import sh_provider

dirname = os.path.dirname(__file__)


class TestCunninghamModel(unittest.TestCase):

    def test_sh_body_fix_acc(self):

        # load data for GRAIL model obtained in CelestLab
        fid = os.path.join(dirname, 'data', 'cunningham_model.mat')
        shd = loadmat(fid, squeeze_me=True, struct_as_record=False)['sh_data']
        pos_vec = np.ascontiguousarray(shd.pos)  # positions vector [km]
        nm_max_vec = np.asarray(shd.nmmax, dtype=np.int64)  # max degrees and orders
        assert np.min(nm_max_vec[0, :] - nm_max_vec[1, :]) == 0
        idx_max = np.argmin(nm_max_vec[0, :] - nm_max_vec[1, :])

        # load SEMPY's GRAIL model from JSON file
        r_ref, gm_moon, z_nm, _ = sh_provider(Primary.MOON, 'GRGM0050',
                                              nm_max=int(shd.nmmax[0, idx_max]), inc00=True)
        _, _, z_nm_pert, _ = sh_provider(Primary.MOON, 'GRGM0050',
                                         nm_max=int(shd.nmmax[0, idx_max]))

        acc = np.empty(shd.accv.shape)  # gravitational acceleration
        acc_pert = np.empty(shd.accp.shape)  # perturbative acceleration

        for i in range(nm_max_vec.shape[1]):  # loop over different cases
            for j in range(pos_vec.shape[0]):  # loop over different positions
                acc[j, :, i] = sh_body_fix_acc(pos_vec[j, :], gm_moon, r_ref, z_nm,
                                               nm_max_vec[0, i], nm_max_vec[1, i])
                acc_pert[j, :, i] = sh_body_fix_acc(pos_vec[j, :], gm_moon, r_ref, z_nm_pert,
                                                    nm_max_vec[0, i], nm_max_vec[1, i])

        np.testing.assert_allclose(acc, shd.accv, rtol=0.0, atol=1e-16)
        np.testing.assert_allclose(acc_pert, shd.accp, rtol=0.0, atol=1e-16)

    def test_sh_body_fix_gradient(self):

        # load data for GRAIL model obtained in CelestLab
        fid = os.path.join(dirname, 'data', 'cunningham_model_partials.mat')
        shd = loadmat(fid, squeeze_me=True, struct_as_record=False)['sh_data']
        pos_vec = np.ascontiguousarray(shd.pos)  # positions vector [km]
        nm_max_vec = np.asarray(shd.nmmax, dtype=np.int64)  # max degrees and orders
        assert np.min(nm_max_vec[0, :] - nm_max_vec[1, :]) == 0
        idx_max = np.argmin(nm_max_vec[0, :] - nm_max_vec[1, :])

        # load SEMPY's GRAIL model from JSON file
        r_ref, gm_moon, z_nm, _ = sh_provider(Primary.MOON, 'GRGM0050',
                                              nm_max=int(shd.nmmax[0, idx_max]), inc00=True)
        _, _, z_nm_pert, _ = sh_provider(Primary.MOON, 'GRGM0050',
                                         nm_max=int(shd.nmmax[0, idx_max]))

        acc = np.empty(shd.accv.shape)  # gravitational acceleration
        acc_pert = np.empty(shd.accp.shape)  # perturbative acceleration
        acc_part = np.empty(shd.accparts2.shape)  # gravitational acceleration
        acc_part_pert = np.empty(shd.accpp2.shape)  # perturbative acceleration

        for i in range(nm_max_vec.shape[1]):  # loop over different cases
            for j in range(pos_vec.shape[0]):
                acc[j, :, i], acc_part[j, :, :, i] = \
                    sh_body_fix_gradient(pos_vec[j, :], gm_moon, r_ref, z_nm,
                                         nm_max_vec[0, i], nm_max_vec[1, i])
                acc_pert[j, :, i], acc_part_pert[j, :, :, i] = \
                    sh_body_fix_gradient(pos_vec[j, :], gm_moon, r_ref, z_nm_pert,
                                         nm_max_vec[0, i], nm_max_vec[1, i])

        np.testing.assert_allclose(acc, shd.accv, rtol=0.0, atol=1e-16)
        np.testing.assert_allclose(acc_pert, shd.accp, rtol=0.0, atol=1e-16)
        np.testing.assert_allclose(acc_part, shd.accparts2, rtol=0.0, atol=1e-16)
        np.testing.assert_allclose(acc_part_pert, shd.accpp2, rtol=0.0, atol=1e-16)


if __name__ == '__main__':
    unittest.main()
