# -*- coding: utf-8 -*-
"""
Created on Mon Jul 15 10:03:58 2019

"""

import unittest
import csv
import numpy as np
import os.path
dirname = os.path.dirname(__file__)

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.crtbp.richardson_coefficients import RichardsonCoefficients




class TestRichardsonCoefficients(unittest.TestCase):
         

    def test_EM_coeff_results(self):
        # The following data was generated using SEMAT
        
        # l1
        l1_c1 = -5.544897979808701
        
        filename = os.path.join(dirname, 'data', 'EM_l1_RC.csv')
        with open(filename , newline='') as file:
            EM_rc_l1_coef = list(csv.reader(file, delimiter=';'))                
            EM_rc_l1 = np.array(EM_rc_l1_coef, dtype = np.float)
            l1_c2, l1_c3, l1_c4, l1_lambda_r, l1_omega_p, l1_omega_v, l1_kappa, l1_Delta, l1_d1, l1_d2, l1_a21, l1_a22, l1_a23, l1_a24, l1_b21, l1_b22, l1_d21, l1_a31, l1_a32, l1_b31, l1_b32, l1_d31, l1_d32, l1_s1, l1_s2, l1_l1, l1_l2 = EM_rc_l1[0, 0:]


        # l2 
        l2_c1 = -6.885916341540265

        filename = os.path.join(dirname, 'data', 'EM_l2_RC.csv')
        with open(filename , newline='') as file:
            EM_rc_l2_coef = list(csv.reader(file, delimiter=';'))                
            EM_rc_l2 = np.array(EM_rc_l2_coef, dtype = np.float)
            l2_c2, l2_c3, l2_c4, l2_lambda_r, l2_omega_p, l2_omega_v, l2_kappa, l2_Delta, l2_d1, l2_d2, l2_a21, l2_a22, l2_a23, l2_a24, l2_b21, l2_b22, l2_d21, l2_a31, l2_a32, l2_b31, l2_b32, l2_d31, l2_d32, l2_s1, l2_s2, l2_l1, l2_l2 = EM_rc_l2[0, 0:]


        # l3
        l3_c1 = 1.012237318938304
        
        filename = os.path.join(dirname, 'data', 'EM_l3_RC.csv')
        with open(filename , newline='') as file:
            EM_rc_l3_coef = list(csv.reader(file, delimiter=';'))                
            EM_rc_l3 = np.array(EM_rc_l3_coef, dtype = np.float)
            l3_c2, l3_c3, l3_c4, l3_lambda_r, l3_omega_p, l3_omega_v, l3_kappa, l3_Delta, l3_d1, l3_d2, l3_a21, l3_a22, l3_a23, l3_a24, l3_b21, l3_b22, l3_d21, l3_a31, l3_a32, l3_b31, l3_b32, l3_d31, l3_d32, l3_s1, l3_s2, l3_l1, l3_l2 = EM_rc_l3[0, 0:]


        
        # the test begins
        cr3bp_nro = Cr3bp(Primary.EARTH, Primary.MOON)
        #l1
        rc_l1 = RichardsonCoefficients(cr3bp_nro, cr3bp_nro.l1)
        #l2
        rc_l2 = RichardsonCoefficients(cr3bp_nro, cr3bp_nro.l2)
        #l3
        rc_l3 = RichardsonCoefficients(cr3bp_nro, cr3bp_nro.l3)
        
        
        #l1
        self.assertAlmostEqual(rc_l1.c1, l1_c1, places=6)
        self.assertAlmostEqual(rc_l1.c2, l1_c2, places=6)
        self.assertAlmostEqual(rc_l1.c3, l1_c3)
        self.assertAlmostEqual(rc_l1.c4, l1_c4, places=6)
        self.assertAlmostEqual(rc_l1.lambda_r, l1_lambda_r)
        self.assertAlmostEqual(rc_l1.omega_p, l1_omega_p)
        self.assertAlmostEqual(rc_l1.omega_v, l1_omega_v)
        self.assertAlmostEqual(rc_l1.kappa, l1_kappa)
        self.assertAlmostEqual(rc_l1.Delta, l1_Delta)
        self.assertAlmostEqual(rc_l1.d1, l1_d1, places=4)
        self.assertAlmostEqual(rc_l1.a21, l1_a21)
        self.assertAlmostEqual(rc_l1.a22, l1_a22)
        self.assertAlmostEqual(rc_l1.a23, l1_a23)
        self.assertAlmostEqual(rc_l1.a24, l1_a24)
        self.assertAlmostEqual(rc_l1.b21, l1_b21)
        self.assertAlmostEqual(rc_l1.b22, l1_b22)
        self.assertAlmostEqual(rc_l1.d21, l1_d21)
        self.assertAlmostEqual(rc_l1.a31, l1_a31)
        self.assertAlmostEqual(rc_l1.a32, l1_a32)
        self.assertAlmostEqual(rc_l1.b31, l1_b31)
        self.assertAlmostEqual(rc_l1.b32, l1_b32)
        self.assertAlmostEqual(rc_l1.d31, l1_d31)
        self.assertAlmostEqual(rc_l1.d32, l1_d32)
        self.assertAlmostEqual(rc_l1.s1, l1_s1, places=6)
        self.assertAlmostEqual(rc_l1.s2, l1_s2)
        self.assertAlmostEqual(rc_l1.l1, l1_l1, places=5) #richardson coefficient l1 of libration point l1
        self.assertAlmostEqual(rc_l1.l2, l1_l2, places=6) #richardson coefficient l2 of libration point l1
        
        
        #l2
        self.assertAlmostEqual(rc_l2.c1, l2_c1, places=6)
        self.assertAlmostEqual(rc_l2.c2, l2_c2, places=6)
        self.assertAlmostEqual(rc_l2.c3, l2_c3)
        self.assertAlmostEqual(rc_l2.c4, l2_c4)
        self.assertAlmostEqual(rc_l2.lambda_r, l2_lambda_r)
        self.assertAlmostEqual(rc_l2.omega_p, l2_omega_p)
        self.assertAlmostEqual(rc_l2.omega_v, l2_omega_v)
        self.assertAlmostEqual(rc_l2.kappa, l2_kappa)
        self.assertAlmostEqual(rc_l2.Delta, l2_Delta)
        self.assertAlmostEqual(rc_l2.d1, l2_d1, places=4)
        self.assertAlmostEqual(rc_l2.a21, l2_a21)
        self.assertAlmostEqual(rc_l2.a22, l2_a22)
        self.assertAlmostEqual(rc_l2.a23, l2_a23)
        self.assertAlmostEqual(rc_l2.a24, l2_a24)
        self.assertAlmostEqual(rc_l2.b21, l2_b21)
        self.assertAlmostEqual(rc_l2.b22, l2_b22)
        self.assertAlmostEqual(rc_l2.d21, l2_d21)
        self.assertAlmostEqual(rc_l2.a31, l2_a31)
        self.assertAlmostEqual(rc_l2.a32, l2_a32)
        self.assertAlmostEqual(rc_l2.b31, l2_b31)
        self.assertAlmostEqual(rc_l2.b32, l2_b32)
        self.assertAlmostEqual(rc_l2.d31, l2_d31)
        self.assertAlmostEqual(rc_l2.d32, l2_d32)
        self.assertAlmostEqual(rc_l2.s1, l2_s1)
        self.assertAlmostEqual(rc_l2.s2, l2_s2)
        self.assertAlmostEqual(rc_l2.l1, l2_l1, places=6) #richardson coefficient l1 of libration point l2
        self.assertAlmostEqual(rc_l2.l2, l2_l2) #richardson coefficient l2 of libration point l2
             
        
        #l3
        self.assertAlmostEqual(rc_l3.c1, l3_c1)
        self.assertAlmostEqual(rc_l3.c2, l3_c2)
        self.assertAlmostEqual(rc_l3.c3, l3_c3)
        self.assertAlmostEqual(rc_l3.c4, l3_c4)
        self.assertAlmostEqual(rc_l3.lambda_r, l3_lambda_r)
        self.assertAlmostEqual(rc_l3.omega_p, l3_omega_p)
        self.assertAlmostEqual(rc_l3.omega_v, l3_omega_v)
        self.assertAlmostEqual(rc_l3.kappa, l3_kappa)
        self.assertAlmostEqual(rc_l3.Delta, l3_Delta)
        self.assertAlmostEqual(rc_l3.d1, l3_d1, places=6)
        self.assertAlmostEqual(rc_l3.a21, l3_a21)
        self.assertAlmostEqual(rc_l3.a22, l3_a22)
        self.assertAlmostEqual(rc_l3.a23, l3_a23)
        self.assertAlmostEqual(rc_l3.a24, l3_a24)
        self.assertAlmostEqual(rc_l3.b21, l3_b21)
        self.assertAlmostEqual(rc_l3.b22, l3_b22)
        self.assertAlmostEqual(rc_l3.d21, l3_d21)
        self.assertAlmostEqual(rc_l3.a31, l3_a31)
        self.assertAlmostEqual(rc_l3.a32, l3_a32)
        self.assertAlmostEqual(rc_l3.b31, l3_b31)
        self.assertAlmostEqual(rc_l3.b32, l3_b32)
        self.assertAlmostEqual(rc_l3.d31, l3_d31)
        self.assertAlmostEqual(rc_l3.d32, l3_d32)
        self.assertAlmostEqual(rc_l3.s1, l3_s1)
        self.assertAlmostEqual(rc_l3.s2, l3_s2)
        self.assertAlmostEqual(rc_l3.l1, l3_l1) #richardson coefficient l1 of libration point l3
        self.assertAlmostEqual(rc_l3.l2, l3_l2) #richardson coefficient l2 of libration point l3


    def test_SE_coeff_results(self):
        # The following data was generated using SEMAT

        # l2 
        l2_c1 = -1.006320015411917e+02

        filename = os.path.join(dirname, 'data', 'SE_l2_RC.csv')
        with open(filename , newline='') as file:
            EM_rc_l2_coef = list(csv.reader(file, delimiter=';'))                
            EM_rc_l2 = np.array(EM_rc_l2_coef, dtype = np.float)
            l2_c2, l2_c3, l2_c4, l2_lambda_r, l2_omega_p, l2_omega_v, l2_kappa, l2_Delta, l2_d1, l2_d2, l2_a21, l2_a22, l2_a23, l2_a24, l2_b21, l2_b22, l2_d21, l2_a31, l2_a32, l2_b31, l2_b32, l2_d31, l2_d32, l2_s1, l2_s2, l2_l1, l2_l2 = EM_rc_l2[0, 0:]
 

        # the test begins
        cr3bp_SE = Cr3bp(Primary.SUN, Primary.EARTH)
        #l1
        rc_l2 = RichardsonCoefficients(cr3bp_SE, cr3bp_SE.l2)
        
        #l2
        self.assertAlmostEqual(rc_l2.c1, l2_c1, places=2)
        self.assertAlmostEqual(rc_l2.c2, l2_c2, places=5)
        self.assertAlmostEqual(rc_l2.c3, l2_c3, places=6)
        self.assertAlmostEqual(rc_l2.c4, l2_c4, places=5)
        self.assertAlmostEqual(rc_l2.lambda_r, l2_lambda_r, places=6)
        self.assertAlmostEqual(rc_l2.omega_p, l2_omega_p, places=6)
        self.assertAlmostEqual(rc_l2.omega_v, l2_omega_v, places=6)
        self.assertAlmostEqual(rc_l2.kappa, l2_kappa, places=6)
        self.assertAlmostEqual(rc_l2.Delta, l2_Delta)
        self.assertAlmostEqual(rc_l2.d1, l2_d1, places=3)
        self.assertAlmostEqual(rc_l2.a21, l2_a21, places=6)
        self.assertAlmostEqual(rc_l2.a22, l2_a22)
        self.assertAlmostEqual(rc_l2.a23, l2_a23, places=6)
        self.assertAlmostEqual(rc_l2.a24, l2_a24)
        self.assertAlmostEqual(rc_l2.b21, l2_b21)
        self.assertAlmostEqual(rc_l2.b22, l2_b22)
        self.assertAlmostEqual(rc_l2.d21, l2_d21, places=6)
        self.assertAlmostEqual(rc_l2.a31, l2_a31, places=6)
        self.assertAlmostEqual(rc_l2.a32, l2_a32)
        self.assertAlmostEqual(rc_l2.b31, l2_b31, places=6)
        self.assertAlmostEqual(rc_l2.b32, l2_b32)
        self.assertAlmostEqual(rc_l2.d31, l2_d31)
        self.assertAlmostEqual(rc_l2.d32, l2_d32)
        self.assertAlmostEqual(rc_l2.s1, l2_s1, places=5)
        self.assertAlmostEqual(rc_l2.s2, l2_s2)
        self.assertAlmostEqual(rc_l2.l1, l2_l1, places=4) #richardson coefficient l1 of libration point l2
        self.assertAlmostEqual(rc_l2.l2, l2_l2, places=5) #richardson coefficient l2 of libration point l2
        
        
        
      
        
    def test_richardson_coeff_warns(self):
        """ Test that exceptions are raised."""
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
        
        with self.assertRaisesRegex(Exception, 'Incorrect Libration point'):
            RichardsonCoefficients(cr3bp, cr3bp.l4)

            
        with self.assertRaisesRegex(Exception, 'Incorrect Libration point'):
            RichardsonCoefficients(cr3bp, cr3bp.l5)
         
            

                   
        
if __name__ == '__main__':
    unittest.main()