# -*- coding: utf-8 -*-
"""
Created on Tue Jul 16 11:30:48 2019

"""

import unittest
import csv
import numpy as np
import os.path
dirname = os.path.dirname(__file__)

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.orbits.halo import Halo
from src.orbits.thirdorder_orbit import ThirdOrderOrbit


class TestRichardsonThirdOrderOrbit(unittest.TestCase):

    def test_EM_results(self):
        # The following data was generated using SEMAT
        
        # l2 
        l2_c1 = -6.885916341540265

        filename = os.path.join(dirname, 'data', 'EM_l2_RC.csv')
        with open(filename , newline='') as file:
            EM_rc_l2_coef = list(csv.reader(file, delimiter=';'))                
            EM_rc_l2 = np.array(EM_rc_l2_coef, dtype = np.float)
            l2_c2, l2_c3, l2_c4, l2_lambda_r, l2_omega_p, l2_omega_v, l2_kappa, l2_Delta, l2_d1, l2_d2, l2_a21, l2_a22, l2_a23, l2_a24, l2_b21, l2_b22, l2_d21, l2_a31, l2_a32, l2_b31, l2_b32, l2_d31, l2_d32, l2_s1, l2_s2, l2_l1, l2_l2 = EM_rc_l2[0, 0:]

        # amplitud_li_frame
        Az_li = 0.186003537371412
        Ax_li = 0.190781504387879
        T_li = 3.399000156868591
        
        
        # The following data was generated using SEMAT to get a firts guess
        x, y, z, vx, vy, vz = 1.116393899500454, 0, -0.027057507769218, 0, 0.189121964194904, 0


        # the test begins
        cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
        
        orbit = Halo(cr3bp, cr3bp.l2, Halo.Family.northern, Azdim = 12000)

        state0_richardson = ThirdOrderOrbit(orbit)
        
        # l2 richardson coefficients
        self.assertAlmostEqual(state0_richardson.rc.c1, l2_c1, places=6)
        self.assertAlmostEqual(state0_richardson.rc.c2, l2_c2, places=6)
        self.assertAlmostEqual(state0_richardson.rc.c3, l2_c3)
        self.assertAlmostEqual(state0_richardson.rc.c4, l2_c4)
        
        # amplitud_li_frame
        self.assertAlmostEqual(state0_richardson.Az_li, Az_li)
        self.assertAlmostEqual(state0_richardson.Ax_li, Ax_li)
        
        # params_li_frame
        self.assertAlmostEqual(state0_richardson.T_li, T_li)
        
        
        # richardson_state0_synodic_frame
        self.assertAlmostEqual(state0_richardson.state0[0], x)
        self.assertAlmostEqual(state0_richardson.state0[1], y)
        self.assertAlmostEqual(state0_richardson.state0[2], z)

        self.assertAlmostEqual(state0_richardson.state0[3], vx)
        self.assertAlmostEqual(state0_richardson.state0[4], vy)
        self.assertAlmostEqual(state0_richardson.state0[5], vz)
            
            
            
            
            
            
            
            
            
if __name__ == '__main__':
    unittest.main()