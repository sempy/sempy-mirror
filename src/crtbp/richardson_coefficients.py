# -*- coding: utf-8 -*-
"""
Created on Sat Jul 13 13:37:00 2019

"""


class RichardsonCoefficients:
    """ This class computes Richardson coefficients for third-order approximation of Halo orbits.

    An instance of the RichardsonCoefficients class will have as attributes all the coefficients obtained by
    Richardson in his work of 1980 in order to get a third-order approximation OF Halo orbits about the Lagrange
    points of a given CRTBP system.

    see Richardson 1980: "Analytic construction of periodic orbits about the collinear points"
    (http://adsabs.harvard.edu/full/1980CeMec..22..241R)


    Parameters
    ----------
    cr3bp : object
        An object created with the Cr3bp class.
    libp : object
        A libration point of the object cr3bp, e.g cr3bp.l2.

    Attributes
    ----------
    Attributes(For lagrange points from l1 to l3).
    c1 : float
        Richardson's first coefficient.
    c2 : float
        Richardson's second coefficient.
    c3 : float
        Richardson's third coefficient.
    c4 : float
        Richardson's fourth coefficient.
    lambda_r : float
        Richardson's coefficient.
    omega_p : float
        Richardson's coefficient.
    omega_v : float
        Richardson's coefficient.
    kappa : float
        Richardson's coefficient.
    Delta : float
        Richardson's coefficient.
    d1 : float
        Richardson's coefficient.
    d2 : float
        Richardson's coefficient.
    a21 : float
        Richardson's coefficient.
    a22 : float
        Richardson's coefficient.
    a23 : float
        Richardson's coefficient.
    a24 : float
        Richardson's coefficient.
    b21 : float
        Richardson's coefficient.
    b22 : float
        Richardson's coefficient.
    d21 : float
        Richardson's coefficient.
    a31 : float
        Richardson's coefficient.
    a32 : float
        Richardson's coefficient.
    b31 : float
        Richardson's coefficient.
    b32 : float
        Richardson's coefficient.
    d31 : float
        Richardson's coefficient.
    d32 : float
        Richardson's coefficient.
    s1 : float
        Richardson's coefficient.
    s2 : float
        Richardson's coefficient.
    l1 : float
        Richardson's coefficient.
    l2 : float\
        Richardson's coefficient.

    """
    def __init__(self, cr3bp, libp):
        """Inits Richardson class."""
        
        self.cr3bp = cr3bp
        self.c1 = self.cn(cr3bp, libp, 1)
        self.c2 = self.cn(cr3bp, libp, 2)
        self.c3 = self.cn(cr3bp, libp, 3)
        self.c4 = self.cn(cr3bp, libp, 4)
        self.lambda_r, self.omega_p, self.omega_v, self.kappa, self.Delta, self.d1, self.d2, self.a21, self.a22, self.a23, self.a24, self.b21, self.b22, self.d21, self.a31, self.a32, self.b31, self.b32, self.d31, self.d32, self.s1, self.s2, self.l1, self.l2 = RichardsonCoefficients.cpm(self.c2, self.c3, self.c4)

        
    def cn(self, cr3bp, libp, n):
        """Cn coefficients for the corresponding li point.
        
        In particular, the convention for case 3 is equivalent to the one of
        Richardson (1980), which may differ from later article (Jorba & Masdemont
        1999, etc). Cases 1 and 2 are always good.
    
        Args:
            cr3bp: An object created with the Cr3bp class. 
            libp: A libration point of the object cr3bp, e.g cr3bp.l2.
            n: Coefficient's number  
        
        Returns:
            cn coefficients.
        
        """
        if libp == self.cr3bp.l1 and n == 1:
            cn = (cr3bp.mu - 1.0 + cr3bp.l1.gamma_i) / cr3bp.l1.gamma_i 
        elif libp == self.cr3bp.l2 and n == 1:
            cn = (cr3bp.mu - 1.0 - cr3bp.l2.gamma_i) / cr3bp.l2.gamma_i             
        elif libp == self.cr3bp.l3 and n == 1:
            cn = (cr3bp.mu + cr3bp.l3.gamma_i) / cr3bp.l3.gamma_i            
        
        elif libp == self.cr3bp.l1:
            cn = cr3bp.l1.gamma_i**(-3) * (cr3bp.mu + (-1)**n*(1-cr3bp.mu)*cr3bp.l1.gamma_i**(n+1)/(1-cr3bp.l1.gamma_i)**(n+1))
        elif libp == self.cr3bp.l2:
            cn = cr3bp.l2.gamma_i**(-3) * ((-1)**n*cr3bp.mu + (-1)**n*(1-cr3bp.mu)*cr3bp.l2.gamma_i**(n+1)/(1+cr3bp.l2.gamma_i)**(n+1))
        elif libp == self.cr3bp.l3:
            cn = cr3bp.l3.gamma_i**(-3) * (1 - cr3bp.mu + cr3bp.mu*cr3bp.l3.gamma_i**(n+1)/(1+cr3bp.l3.gamma_i)**(n+1))
        else:
            raise Exception('Incorrect Libration point')
        return cn
    
    
    @staticmethod
    def cpm(c2, c3, c4):
        """Computation of characteristic constants of the periodic motion."""
        lambda_r = (0.5*(2 - c2 + (9 * c2**2 - 8*c2)**(1/2)))**(1/2)  # in fact the varable name is just lambda without the _r, the name was changed due to lambda is keyword

        omega_p = lambda_r  # omega_p is set equal to lambda_r, see Koon et al. & Gomez et al. for details

        omega_v = c2**(1/2)

        kappa = (lambda_r**2+1+2*c2)/(2.0*lambda_r)

        Delta = lambda_r**2-c2
        

        d1 = (3*lambda_r**2)/kappa*( kappa*(6*lambda_r**2 - 1) - 2*lambda_r)
        
        d2 = (8*lambda_r**2)/kappa*( kappa*(11*lambda_r**2 - 1) - 2*lambda_r)
        
        a21 = (3*c3*(kappa**2 - 2))/(4*(1 + 2*c2))
        
        a22 = (3*c3)/(4*(1 + 2*c2))
        
        a23 = - (3*c3*lambda_r)/(4*kappa*d1)*(3*kappa**3*lambda_r - 6*kappa*(kappa-lambda_r) + 4)
        
        a24 = - (3*c3*lambda_r)/(4*kappa*d1)*(2 + 3*kappa*lambda_r)
        
        b21 = - (3*c3*lambda_r)/(2*d1)*(3*kappa*lambda_r - 4)
        
        b22 = (3*c3*lambda_r)/(d1)
        
        d21 = - c3/(2*lambda_r**2)
        
        a31 = - (9*lambda_r)/(4*d2)*(4*c3*(kappa*a23 - b21) + kappa*c4*(4 + kappa**2)) + (9*lambda_r**2 + 1 - c2)/(2*d2)*(3*c3*(2*a23 - kappa*b21) + c4*(2+3*kappa**2))
        
        a32 = - (9*lambda_r)/(4*d2)*(4*c3*(kappa*a24 - b22) + kappa*c4) - 3/(2*d2)*(9*lambda_r**2 + 1 - c2)*(c3*(kappa*b22 + d21 - 2*a24) - c4)
        
        b31 = 3/(8*d2) * (8*lambda_r*(3*c3*(kappa*b21 - 2*a23) - c4*(2 + 3*kappa**2)) + (9*lambda_r**2 + 1 + 2*c2)*(4*c3*(kappa*a23 - b21) + kappa*c4*(4 + kappa**2)) )
        
        b32 = 1/d2 * (9*lambda_r*(c3*(kappa*b22 + d21 - 2*a24) - c4) + 3/8*(9*lambda_r**2 + 1 + 2*c2)*(4*c3*(kappa*a24 - b22) + kappa*c4))
        
        d31 = 3/(64*lambda_r**2)*(4*c3*a24 + c4)
        
        d32 = 3/(64*lambda_r**2)*(4*c3*(a23 - d21) + c4*(4 + kappa**2))
        
        s1 = (2*lambda_r*(lambda_r*(1 + kappa**2) - 2*kappa))**(-1 ) * (3/2*c3*(2*a21*(kappa**2 - 2) -  a23*(kappa**2 + 2) - 2*kappa*b21)- 3/8*c4*(3*kappa**4 - 8*kappa**2 + 8))
        
        s2 = (2*lambda_r*(lambda_r*(1 + kappa**2) - 2*kappa))**(-1 ) * (3/2*c3*(2*a22*(kappa**2 - 2) + a24*(kappa**2 + 2) + 2*kappa*b22 + 5*d21) + 3/8*c4*(12 - kappa**2))
        
        l1 = - 3/2 * c3*(2*a21 + a23 +5*d21) - 3/8*c4*(12 - kappa**2) + 2*lambda_r**2*s1
        
        l2 = 3/2 * c3*(a24 - 2*a22) + 9/8*c4 + 2*lambda_r**2*s2
        
                    
        return lambda_r, omega_p, omega_v, kappa, Delta, d1, d2, a21, a22, a23, a24, b21, b22, d21, a31, a32, b31, b32, d31, d32, s1, s2, l1, l2
