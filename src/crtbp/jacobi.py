# -*- coding: utf-8 -*-
"""
Created on Tue May 28 11:07:26 2019

"""


def u_bar(state, mu_var):
    """Augmented potential (from Koon et al. 2011, chapter 2). """
    x_var, y_var, z_var = state
    mu1 = 1 - mu_var
    mu2 = mu_var
    # where r1 and r2 are expressed in rotating coordinates
    r1_var = ((x_var+mu2)**2 + y_var**2 + z_var**2)**(1/2)
    r2_var = ((x_var-mu1)**2 + y_var**2 + z_var**2)**(1/2)
    aug_pot = -1/2*(x_var**2+y_var**2) - mu1/r1_var - mu2/r2_var - 1/2*mu1*mu2
    return aug_pot


def jacobi(state, mu_var):
    """Computes the jacobi constant at a given state in the CRTBP system with the mass ratio mu.
    See equation 2.3.14 of Koon et al. 2011. """
    if len(state) == 3:  # if only position is given, velocity is supposed null
        jacobi_cst = -2*u_bar(state, mu_var)
    elif len(state) == 6:
        jacobi_cst = -2*u_bar(state[0:3], mu_var) - (state[3]**2 + state[4]**2 + state[5]**2)
    else:
        raise Exception('State dimension wrong. State dimensions must be 3 or 6')
    return jacobi_cst
