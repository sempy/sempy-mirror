#!/bin/bash

#SEMPY_PATH=~/git/saclab_dev/sempy
SEMPY_PATH=$(pwd)/..

echo "*************"
echo "* Test Unit *"
echo "*************"

cd $SEMPY_PATH/src/tests
python -m unittest

echo "**************************"
echo "* Static code validation *"
echo "**************************"
cd $SEMPY_PATH
pylint -j 0 src

echo "****************************"
echo "* Coverage code estimation *"
echo "****************************"
cd $SEMPY_PATH/src/tests
coverage erase
coverage run --omit test_constants.py test_constants.py
testfiles=$(ls -d test_*)
for f in $testfiles
do
 echo "Processing $f"
 # do something on $f
 coverage run --omit $f $f
done
coverage report




exit(0)

usage:
bash runTests.bash >> testReport.txt

# test units
cd $SEMPY_PATH/src/tests
python -m unittest


#GOAL:
#----------------------------------------------------------------------
#Ran 19 tests in 3.846s
#
#OK

# static tests 
cd $SEMPY_PATH/
pylint src

#GOAL:
#------------------------------------
#Your code has been rated at 10/10

pylint -j 0 src

