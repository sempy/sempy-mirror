﻿# Tutorial for basic functionalities of SEMPY core module


## 1. Dynamical environments
This first tutorial will teach you how to instantiate and use dynamical environment objects.

### 1.1 Importing classes and modules
Before running a script it is necessary to import all modules and classes required for the task at hand.
For this tutorial, import the following modules and classes:

```
import numpy as np
import matplotlib.pyplot as plt

from src.init.primary import Primary
from src.dynamics.cr3bp_environment import CR3BP
```

The Class `Primary` contains all necessary information about celestial bodies used to define dynamical environments. The environment classes allow to compute their main characteristics and Equations of Motion.

### 1.2 Initializing environments

To setup a CR3BP (Circular Restricted 3-Body Problem) environment the user must create a dedicated instance of the `CR3BP` class. As arguments, the user needs to pass the primaries to the CR3BP class constructor, `m1` and `m2`, just by using two instances of the `Primary` class. For instance: `Primary.EARTH` and `Primary.MOON`. The primaries that are available in the package can be found in the Primary module. To initialize an Earth-Moon CR3BP system, use the following command:

```
cr3bp = CR3BP(Primary.EARTH, Primary.MOON)
```
CR3BP environments are, by definition, defined in the rotating frame, centred on the system’s barycenter. 

NBP (N-Body Problem) environments require a ‘center’ (string) and a list of Primary bodies to be included as point masses. Moreover, they allow defining an initial epoch (default is 1/1/2000 UTC) of the system, and a reference frame (default is J2000). For the latter, the AvailableFrames subclass of NBP allows selecting the available frames. NBP’s can be adimensionalised through two user defined constants (for length and times). 
```
nbp = NBP(‘EARTH’, [Primary.SUN, Primary.MOON, Primary.EARTH])
nbp_2022 = NBP(‘EARTH’, [Primary.SUN, Primary.MOON, Primary.EARTH], start_epoch=’1 JAN 2022’)
nbp_ecliptic = NBP(‘EARTH’, [Primary.SUN, Primary.MOON, Primary.EARTH], frame=NBP.AvailableFrames.EJ2000)
nbp_adim = NBP(‘EARTH’, [Primary.SUN, Primary.MOON, Primary.EARTH], adim_l = 384400, adim_t=300000)
```

R2BP (Restricted 2-Body Problem) is the classical Keplerian motion, requires only one Primary object and can as well be adimensionalised.

## 2. Numerical propagation

This tutorial is dedicated to users who wish to use `scipy`'s numerical integrators to propagate an initial state `state0` and store the different parameters of the integration within SEMpy's dedicated structure. In this tutorial, the user wants to propagte an adimensional state in the Earth-Moon synodic frame using the CR3BP dynamical equations for a given time. 

 ### 2.1 Importing classes and modules

```
import numpy as np
import matplotlib.pyplot as plt

from src.init.primary import Primary
from src.propagation.propagator import Propagator

from src.dynamics.cr3bp import CR3BP
```

### 2.2 Initialization of the environment

Initialize the dynamical framework in which you wish to work. For instance, if you wish to propagate dynamics in the CR3BP:

```
cr3bp = CR3BP(Primary.EARTH, Primary.MOON)
```

If it hasn't been done yet, set the initial state to be propagated. For instance:

```
state0 = np.array([1.116756261235440, 0, 0.0222299747777274, 0, 0.186527612353890, 0])
```

The dynamics (i.e. the Equations of Motion (EOM)) are adimensionalised through the system’s characteristic quantities and already contain the system’s parameters needed for the integration process. They are an attribute of the CR3BP class, and can be retrieved as follows:
```
state0_dot = cr3bp.eom(0, state0)
```
To propagate the initial state in time, set the duration of the propagation. For instance:
```
T = 3.407534811635316
t_span = [0, T]
```

Reminder: in this example the user wishes to propagate an initial adimensional state using the adimensional CR3BP equations written in standard form the the EARTH-Moon synodic frame. The time will therefore also be adimensional.

### 2.3 Numerical integration
Propagate the state by instancing the `Propagator` class. Parameters of this class are independent from the dynamical environment, and concern the numerical integration methods and tolerances. If not specified, they are set to their default values. For more information on numerical propagation with SEMpy please refer to the `Propagator` class documentation.

```
propagate = Propagator()
```

In order to propagate, the Propagator class can be directly called, and it requires the EOM, the time span and the initial state. Optionally, it can handle events (defined as per `scipy` documentation), and PathParameters, functions that are appended to the state vector be integrated.
In its simplest implementation, a propagation can be performed as:
```
solution = propagate(cr3bp.eom, t_span, state0)
```
The solution object is an instance of the Solution class, that, similarly to `scipy`, contains the propagated states (`solution.state_vec`) and times (`solution.t_vec`).
As the Propagator class is independent on dynamical environment, it can be used for multiple dynamical environments (so only one needs to be imported).

## 3: Differential correction
A fundamental step for generating and studying trajectories and orbits is the **correction process**, in which a guess on the state is iteratively corrected to fit a set of constraints that describe the desired trajectory/orbit.

As an example, imagine that we need to define a periodic orbit in the Circular Restricted Three Body Problem (**CR3BP**), given an initial guessed state `x0_guess` and period `T0_guess`.

The first step is to initialize the dynamical environment and the propagator. This is already explained in Section 2.2 and Section 2.3, and will give us the two instances “environment” and “propagator”.

The additional required steps are the construction of the constraints, and of the corrector, described in the following sections.

Please, notice that the both the constraint building and the correction process, explained in Section 3.1 and 3.2 respectively, are used in the families generation of Section 4. Nevertheless, the family generation automatically deals with the management of the corrector, and the user is only asked to build the constraints as per Section 3.2.
Furthermore, the “basic user” version of the family generator will also embed
the user is only asked to build the set of constraints (Section 3.1), while the correction process is managed automatically. Finally, it is worth mentioning that the “basic user” version of the family generator will also embed predefined sequences of constraints, relieving the user from this task.  


### 3.1 Constraints
To build the set of constraints, we need to import the libraries containing the conditions (functions) we are interested in, and the class that assembles them in a single constraints object.

In our example, we want to obtain a single periodic orbit, so our objectives are:

1) Ensure continuity between initial state and the propagated state after one orbit period.
2) Extract a specific orbit among the local family, according to some parameter fixed by the user.
3) Obtain a specific initial condition along the orbit.

The first objective belongs to the library of constraints specific for periodic orbits, while the other two can vary according to user’s needs, and belong to the group of the so-called **Poincarè conditions**.
We proceed then to import the aforementioned libraries of constraints, together with the class `Constraints`, which will assemble the various objectives into a single object manageable by the Corrector:
```
from sempy.solve.constraints_libraries.constraints_lib import PeriodicLib, PoincareLib

from sempy.solve.constraints import Constraints
```
In this example, we want to impose periodicity by matching the full propagated state with the initial one, hence we write:
```
constr_periodicity = PeriodicLib.state_match(environment, propagator)
```

To fix the orbit, we may want to specify the **x** component of the initial state.
Also, we fix the position along the orbit by specifying another term of the initial state. A common practice for orbits around collinear libration points is to set $y=0$.

Both conditions make use of a single method of the Poincarè library, `fix_element`, which requires the indexes of the variables vector, and the corresponding desired values:
```
constr_elements= PoincareLib.fix_elements([0, 1], [1.1, 0])
```
Notice that other parameters, such as orbital period, can be alternatively constrained, or other constraints functions within the Poincarè library can be used.

Finally, we can assemble the constraints object:
```
constraints = Constraint([constr_periodicity, constr_elements])
```

### 3.2 Correction
Now we proceed to build the corrector and obtain the state of the final periodic orbit.
First, the `Corrector` class is imported, and it is instantiated providing the constraints:
```
from sempy.solve.correct.corrector import Corrector
corrector = Corrector(constraints)
```
The default corrector possesses a set of predefined parameters that can be overridden. In particular, it leverages (by default) a *Levenberg-Marquardt* algorithm, with a tolerance for convergence of 1e-11. These and other parameters can be modified when creating the instance, as follows:
```
corrector = Corrector(constraints, method="hybr", tol=1e-14)
```

Now we can call the instance and find the solution:
```
x0_guess = np.array([1.13, 0, 0, 0, 0.03, 0])
T0_guess = 2
guess = np.append(x0_guess,, T0)

solution = corrector(guess, jac_flag=True)
```
The optional parameter `jac_flag`, when set to **True**, allows to exploit an analytical expression of the Jacobian matrix, to speed up the correction procedure. Its default value is **False**, and can be changed to **True** only if all of the selected constraint functions embed the corresponding jacobian terms **(this is always the case for currently implemented constraints, and should always be ensured when adding new constraints to the libraries)**.
 
The object `solution` will contain all the attributes that are provided from the method `scipy.optimize.root`. For example, if we want to extract the solution’s new variables, and the residual of the constraints:
```
corrected_variables = solution.x
corrected_residual = solution.fun
```


## 4: Families (not fully implemented)
Sempy allows to collect, store, and use families of orbits through the `Family` object.
In this section, the creation of a new family object is first illustrated (Section 4.1), then it is explained how to use them (Section 4.2).

### 4.1: Family generation (through continuation)
The Sempy library enables two approaches to generate families of orbits. A **Simple** approach (explained in detail in Section 4.1.1), that allows to quickly generate a known orbital family from a predefined set, and an **Expert** approach (explained in Section 4.1.2) that allows skilled users of Sempy to set customized conditions and obtain new orbital families.

#### 4.1.1 Simple approach (TBD)
```
recipe_halo = OrbitRecipe.EML2SH
family = Family.generate(recipe_halo)
```

#### 4.1.2 Expert approach
The expert approach lets the user follow the mathematical steps required for generating families of orbits. In particular, the following ingredients are required:
1) Initialization of a state and direction
2) Setup of the constraints that define the orbit (see Section 3.1)
3) Setup of an additional “continuation” constraint, that allows to move across the family
4) Initialization of a continuation scheme to develop new orbit from the previously corrected ones
5) Setup of stop criteria for the continuation iterative process
6) Setup and run of the orbits builder
7) (facultative) Bifurcation detection
8) Family export

To explain all the steps, let us consider the example of generating a planar Lyapunov family around L2  of the Earth-Moon system, in the Circular Restricted Three Body Problem.
First, we need to import and build the CR3BP environment `env`, and the Propagator `prop`, following the instructions from Section 2.2 and 2.3.
```
from sempy.init.primary import Primary
from sempy.dynamics.cr3bp_environment import CR3BP
from sempy.propagation.propagator import Propagator

env = CR3BP(Primary.EARTH, Primary.MOON)
prop = Propagator()

```

***Initialization***

To initialize a state and a direction (required to continue the family), we make use of the `Initializer` class, which contains various initialization options. In our case, we choose a lagrangian point initialization:
```
from sempy.solve.initializer import Initializer

lagrangian_point = env.lagrange_points.L2
linearized_dynamics_matrix_function = env.get_f_matrix

initial_guess_list = Initializer.lagrange_point_orbit(lagrangian_point , linearized_dynamics_matrix_function , delta=-1e-4, direction='planar')

initial_guess = initial_guess_list[0]
```
Notice that every initialization method has its own inputs.
In this case, we need to specify the lagrangian point, the function to obtain the linearized matrix for the dynamics (both embedded in the environment instance), a `delta` that defines the displacement imposed from the lagrangian point, and `direction` allows the user to search solutions in the binary system’s plane or in the out-of-plane direction.
The function returns a list of initial guesses (there may be multiple continuation directions depending on the inputs). Each element of the list will be a 2-by-N array, in which the first row is the initial (lagrangian) point, while the second is the first periodic orbit guess.

***Setup constraints***

Following the steps from Section 3.1, we import the constraints that ensure each orbit to be periodic, and their initial state to lay on the x-z plane of the binary system (so $y=0$): 
```
from sempy.solve.constraints_libraries.constraints_lib import PeriodicLib, PoincareLib
from sempy.solve.constraints import Constraints

constr_periodicity = PeriodicLib.state_match(env, prop)
constr_xz_plane = PoincareLib.fix_elements([1], [0])

constr_list = [constr_periodicity, constr_xz_plane]
```
Notice that, differently from Section 3.1, the list of constraints is not processed by the `Constraints` class, as this is automatically managed inside the class that generates the family (shown later).

***Setup continuation constraint***

The continuation constraints allows us to fix the final constraint that define a unique orbit within the family. Differently from the other constraints, it does not require a target value to be fixed, but it generates a new target value automatically from the continuation process, in relation with the previously generated orbits of the family.
In our example we ask to fix the distance of the position components of the state between the last corrected orbit, and the new guess provided by the continuator (explained in the next step).
```
from sempy.solve.generate_orbits.continuation_lib import ContinuationCriterion

cont_constr = ContinuationCriterion.fix_distance([0, 1, 2])
```
Here, [0,1,2] are the indexes of the state elements from which the distance is computed. In a different setup we may fix the difference in the velocity ([3,4,5]) or even the full state ([0,1,2,3,4,5]).

***Initialize continuation scheme***

Now we need to specify a method for performing the continuation. In this example, we will use a **polynomial** continuator, that approximates well the behavior of the family and allows fast continuation. Other schemes, such as **natural parameter** continuation, or **pseudo-arclength** continuation are included in the continuation method selection.
```
from sempy.solve.predictor import Predictor

continuator = Predictor.polynomial()
```
Notice that several default options can be modified. For example, if we want a prediction tolerance of 1e-4 and a maximum degree of the polynomial of 4, we may write
```
continuator = Predictor.polynomial(pred_tol=1e-4, max_degree=4)
```

***Setup stop conditions for the continuation***

The last ingredient of the process are the stopping criteria for the continuation process.
These criteria are collected in a class (`StopCriteriaList`) within the same module of `ContinuationCriterion`, i.e. `sempy.solve.generate_orbits.continuation_lib` *(TEMPORARY SETUP, IT MAY BE CHANGED IN THE FUTURE)*.

In our example we ask for four condition to be satisfied while generating the family:
1) Number of iterations below 50
2) Minimum adimensional distance from Earth of 1e-3
3) Minimum adimensional distance from Moon of 1e-3
4) Maximum adimensional distance from the system’s barycenter of 2

The code reads:

```
from sempy.solve.generate_orbits.continuation_lib import StopCriteriaList

max_count = 50
primaries_position = np.array([[-env.mu, 0, 0], [1-env.mu, 0, 0]])
barycenter_position = np.array([0, 0, 0])
min_dist_thresh = 1e-3
max_dist_thresh = 2

stop_criteria_list = [StopCriteriaList.max_orbits_count(max_count),
                      StopCriteriaList.min_reference_distance(prop, env, primaries_pos[0], min_dist_thresh),
                      StopCriteriaList.min_reference_distance(prop, env, primaries_pos[1], min_dist_thresh),
                      StopCriteriaList.max_reference_distance(prop, env, barycenter_position, max_dist_thresh)]
```

***Setup of orbits builder and family generation***

Finally, we can initialize the `BuildOrbit` *(TEMPORARY NAME)* class that generates the family, providing all the previous parameters:

```
from sempy.solve.generate_orbits.orbit_builder import BuildOrbit

orbits_generator = BuildOrbit(initial_guess, corr_constr, cont_constr, stop_criteria_list, continuator, env, prop)

orbits_generator.generate_family() 
```

The method will fill the `states_stack` attribute of the generator with an array whose rows are the orbits, and the columns are the elements of the state for each orbit. Last column is the period of the orbits.

***Detect bifurcations***

The interested user may ask the builder to extract data about bifurcations of the newly generated family. To do so, simply run:
```
orbits_generator.detect_bifurcations()
```
This will populate an attribute of the generator called `bifurcations`, that contains a dataclass of all the tipes of bifurcations, and their state and bifurcation direction within the generated family. Notice that in case a specific bifurcation type is not present in the family, the attribute will be `None`

**Export family (or selected orbits) to independent object**

To export the generated family to an independent object, we can call the method `export_family` of the `orbits_generator` instance.
The general use is the following:
```
family = orb_gen.export_family()
```
This will create the family object with all the orbits (their state and their period) produced by the continuation process.

One may be instead interested in some specific orbits of the family, satisfying a particular condition.
To do so, we will need an interpolation across the orbits produced by the generator.

First, we import the interpolation criteria, and the desired values for the selected criteria. Then, we pass the criterion and the desired value to the export method.

For example, we may want to extract the orbits with a specific period, e.g. the mean period of all the developed family:
```
import numpy as np
from src.solve.generate_orbits.continuation_lib import InterpFunctions

interpolation_function = InterpFunctions.element(-1)

mean_period = [np.mean(orbits_generator.states_stack[:,-1])]

orbits_with_mean_period = orbits_generator.export_family(interpolation_function, mean_period)
```

***Save family (TBD)***

```
family.save_as(“name_of_the_family”)
```
### 4.2: Family loading from pre-computed abacus (INCOMPLETE)

```
cr3bp = CR3BP(Earth, Moon)
family = Family.load(cr3bp, ‘L2’, ‘Halo’, ‘Southern’)
# or
family = Family.load(‘EML2HS’)
```


### 4.2: (INCOMPLETE)

Load the family (or generate new) as in section…

```

```

## 5: Orbits (not implemented yet)

### 5.1 Importing classes and modules
As before, classes and modiul 

```
import numpy as np
import matplotlib.pyplot as plt

from src.init.primary import Primary
from src.propagation.propagator import Propagator

from src.dynamics.cr3bp import CR3BP
```
### 5.2 Loading and interpolating from family object
In this example, we wish to load the family of Halo orbits defined around L2 point of the Earth-Moon system (code ‘L2H’). We also wish to select the southern family.
```
from sempy.orbits.family import Family


halo_EM_L2_family_southern = Family(cr3bp).load.L2HS

halo_T26 = halo_L2_family.select.period(2.6)
halo_J146 = halo_L2_family.select.jacobi(1.46)
halo_Az7 = halo_L2_family.select.Az(7e4)

# Lists of orbits
halo_1_2 = halo_L2_family.select.period([1, 2])
halo_1_to_4 = halo_L2_family.select.period(1., 4.)  # range
halo_all =  halo_L2_family.select.all()  # entire family

```
The `select` method interpolates inside the pre-computed (or generated) initial conditions contained in the Family object, and performs a step of differential correction to cancel any numerical errors from the interpolation process.

Comment: some parameters are not monotone for some families. When loading Halos, for example, it could be that there are more with the same value. In this case, both can be  returned as a list of orbits, or alternatively as an instance of Family but with only the orbits required.

### 5.2 Accessing the orbit’s properties

```
from sempy.orbits.orbit import Orbit

my_orbit = Orbit(cr3bp)
my_orbit
```

## 5. Computing and visualizing several CR3BP orbits and systems simultaneously

If needed, orbits in several different CRTBP systems can be built an and analyzed in the same script.


### 5.1 Imports
```
from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.init.orbit import Halo
import matplotlib.pyplot as plt
from src.plotting.plotting_module import PlottingOptions, MasterPlotter
```
### 5.2 Initialization and Computation

Let's suppose the first CR3BP system of interest is the Earth-Moon one. We start by computing four different orbits within this system about different Lagrangian points.

```
cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
    
orbit_EM_1 = Halo(cr3bp, cr3bp.l2, Halo.Family.northern, Azdim=12000)
orbit_EM_1.computation()

orbit_EM_2 = Halo(cr3bp, cr3bp.l1, Halo.Family.northern, Azdim=12000)
orbit_EM_2.computation()

orbit_EM_3 = Halo(cr3bp, cr3bp.l3, Halo.Family.northern, Azdim=12000)
orbit_EM_3.computation()

orbit_EM_4 = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Azdim=12000)
orbit_EM_4.computation()

```

Let's suppose the second CR3BP system of interest is the Sun-Earth one. Here only one orbit is built:

```
cr3bp_SE = Cr3bp(Primary.SUN, Primary.EARTH)
orbit_SE = Halo(cr3bp_SE, cr3bp_SE.l2, Halo.Family.northern, Azdim=125000)
orbit_SE.computation()
```

Finally, the third CRTBP system is the Sun-Jupiter one, with a Halo orbit about L2:

```
cr3bp_SJ = Cr3bp(Primary.SUN, Primary.JUPITER)
orbit_SJ = Halo(cr3bp_SJ, cr3bp_SJ.l2, Halo.Family.northern, Azdim=15000000)
orbit_SJ.computation()

```

### 5.3 Visualization

Let's start by resetting the plotting windows:
```
plt.ioff()
plt.close('all')
 ```

And finally let's instance a different plot for each system.

Earth-Moon system:

```
plot_options = PlottingOptions(names=[True, 'normal'], sci=True, l3=True, l4=False, l5=False)
plot_EM = MasterPlotter(plot_options, orbit_EM_1, orbit_EM_2, orbit_EM_3, orbit_EM_4)  # scale_factor=[True, 3]
plot_EM.scene_TD()
plot_EM.scene_twoD()
```

Sun-Earth system:

```
plot_options_SE = PlottingOptions(names=[True, 'normal'], scale_factor=[True, 10], sci=True, l3=False, l4=False, l5=False)
plot2 = MasterPlotter(plot_options_SE, orbit_SE)
plot2.scene_twoD()
plot2.scene_TD()

```
Sun-Jupiter system:

```
plot_options_SJ = PlottingOptions(names=[True, 'normal'], scale_factor=[True, 20], sci=True, l3=False, l4=False, l5=False)
plot3 = MasterPlotter(plot_options_SJ, orbit_SJ)
plot3.scene_twoD()
plot3.scene_TD()
```

