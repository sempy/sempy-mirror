# Testing routines


## 1. Unit testing

To manually run unit tests, go to the tests folder of the package, eg:

```
cd C:\Users\r.feynman\SEMpy\src\tests
```

and execute in the anaconda prompt terminal:

```
python -m unittest
```

## 2. Static code analysis with pylint

pylint provides a static analysis of your code.
Messages are explicit, and require correction to comply with [PEP8](https://www.python.org/dev/peps/pep-0008/) standards.

Here's a typical output from a static code analysis performed on faulty code, with pylint:

```bash
pylint computation
#-------------------------------------------------------------------
#Your code has been rated at 10.00/10 (previous run: 8.33/10, +1.67)

pylint frames/
#************* Module src.frames
#frames/__init__.py:12:0: C0304: Final newline missing (missing-final-newline)
#************* Module src.frames.reference_frame
#frames/reference_frame.py:18:0: C0304: Final newline missing (missing-final-newline)
#frames/reference_frame.py:8:0: C0115: Missing class docstring (missing-class-docstring)
#frames/reference_frame.py:8:0: R0903: Too few public methods (0/2) (too-few-public-methods)
#frames/reference_frame.py:11:0: C0115: Missing class docstring (missing-class-docstring)
#frames/reference_frame.py:11:0: R0903: Too few public methods (0/2) (too-few-public-methods)
#frames/reference_frame.py:14:0: C0115: Missing class docstring (missing-class-docstring)
#frames/reference_frame.py:14:0: R0903: Too few public methods (0/2) (too-few-public-methods)
#frames/reference_frame.py:17:0: C0115: Missing class docstring (missing-class-docstring)
#frames/reference_frame.py:17:0: R0903: Too few public methods (0/2) (too-few-public-methods)
#
#------------------------------------
#Your code has been rated at -2.50/10
```

To respect the conventions, two options: 
 - Correcting what pylint is complaining about
 - Updating pylint's configuration file if a specific convention is used (refer to
http://pylint.pycqa.org/en/1.9/user_guide/options.html#cmdoption-variable-rgx). Please discuss with the SEMpy project managers before considering this option.




### 2.1 Installing pylint
In you dedicated environment:
```bash
pip install pylint
```
### 2.2 Running pylint
To run pylint on all source files of the library:
```bash
cd ../../
pylint src
```


To run pylint at module level, move to the location of the selected module and run:
```
pylint <module-name>
```

### 2.3 Ignoring specific patterns

```bash
pylint --argument-rgx='[a-z]'  -j 0 diffcorr/
```
This should only be done as a last resort and after consulting the project managers.

## 3. Continuous integration with Jenkins

### 3.1 Installing Jenkins

```bash
pip install python-jenkins

wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
sudo echo "deb https://pkg.jenkins.io/debian-stable binary/" >> /etc/apt/sources.list
sudo apt-get update
sudo apt-get install jenkins

```

For more information oh how to install Jenkins, please refer to [jenkins](https://wiki.jenkins.io/display/JENKINS/Installing+Jenkins+on+Ubuntu).

### 3.2 Configuring Jenkins

#### Having ADMIN access

In your browser go to  http://localhost:8080/

sudo cat /var/lib/jenkins/secrets/initialAdminPassword


#### Creating a project

create project 'sempy' (New item, freestyle project)

#### Building a test

python setup.py bdist_wheel


#### Incorporating unit testing with Jenkins

pip install -e ./
cd src\tests
python -m unittest
python -m unittest discover  -s src/tests/ -p "test_*.py" -v

### 3.3 Using Jenkins

In http://localhost:8080/job/sempy/configure, execute in Shell:
```
/home/tgateau/anaconda3/bin/python -m unittest discover  -s src/tests/ -p "test_*.py" -v --junitxml results.xml
/home/tgateau/anaconda3/bin/python -m unittest src/tests/test_constants.py -v
/home/tgateau/anaconda3/bin/python -m unittest src/tests/test_*.py -v
````
19 tests should be working


#### Triggering Jenkins on git update

1. Enable Poll SCM in your Jenkins build configuration

2. In .git/hooks add a file named <notifyCommit> and add these line inside: 

```
#!/bin/sh
curl http://localhost:8080/git/notifyCommit?url=https://openforge.isae.fr/git/sempy
```

http://localhost:8080/git/notifyCommit?url=https://openforge.isae.fr/git/sempy

Scheduled polling of sempy
No Git consumers using SCM API plugin for: https://openforge.isae.fr/git/sempy



## TODO ref IEEE testing tools list
