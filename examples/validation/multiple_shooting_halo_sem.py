"""
Correction of an L2 southern Halo orbit in the Sun-Earth-Moon ephemeris model.

"""

import os
import numpy as np
import spiceypy as sp
import matplotlib.pyplot as plt
from time import perf_counter
from scipy.io import loadmat

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.init.ephemeris import Ephemeris
from src.orbits.halo import Halo
from src.coc.synodic_j2000 import synodic_to_j2000, j2000_to_synodic
from src.solve.multiple_shooting import MultipleShooting
from src.init.load_kernels import load_kernels
from src.plotting.util import set_axes_equal
from src.__init__ import DIRNAME

load_kernels()  # load SPICE kernels

# %% load SEMAT data for comparison
fid = os.path.join(os.path.split(DIRNAME)[0], 'examples/validation/data',
                   'halo_multiple_shooting_sem.mat')
dtm = loadmat(fid, squeeze_me=True, struct_as_record=False)['data']

# %% init environment and Halo orbit
cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
eph = Ephemeris((Primary.EARTH, Primary.MOON, Primary.SUN))
et0 = sp.str2et('2020 JUN 18 12:00:00.000')

halo = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Azdim=30e3)
halo.interpolation()

# %% orbit sampling and change of coordinates
nb_revs = 10
nb_patch_rev = 50
t_patch_syn, state_patch_syn = halo.sampling(nb_revs, nb_patch_rev)

t_syn = np.empty((nb_revs * nb_patch_rev, 4))
state_syn = np.empty((nb_revs * nb_patch_rev, 6, 4))
t_j2000 = np.empty((nb_revs * nb_patch_rev, 4))
state_j2000 = np.empty((nb_revs * nb_patch_rev, 6, 4))
t_j2000_dim = np.empty((nb_revs * nb_patch_rev, 4))
state_j2000_dim = np.empty((nb_revs * nb_patch_rev, 6, 4))

t_syn[:, 0], state_syn[:, :, 0] = t_patch_syn, state_patch_syn
t_j2000[:, 0], state_j2000[:, :, 0] = \
    synodic_to_j2000(t_patch_syn, state_patch_syn, et0, cr3bp, cr3bp.m1, adim=True,
                     bary_from_spice=True)
t_j2000_dim[:, 0], state_j2000_dim[:, :, 0] = \
    synodic_to_j2000(t_patch_syn, state_patch_syn, et0, cr3bp, cr3bp.m1, bary_from_spice=True)

# %% tests on patch points
t_patch_mat = (dtm.time.syn.patch, dtm.time.adim.patch, dtm.time.dim.patch)
state_patch_mat = (dtm.state.syn.patch, dtm.state.adim.patch, dtm.state.dim.patch)
abs_tol_patch = (1e-11, 1e-11, 1e-5)

for i, t in enumerate((t_syn, t_j2000, t_j2000_dim)):
    np.testing.assert_allclose(t[:, 0], t_patch_mat[i], rtol=0.0, atol=abs_tol_patch[i])
for i, s in enumerate((state_syn, state_j2000, state_j2000_dim)):
    np.testing.assert_allclose(s[:, :, 0], state_patch_mat[i], rtol=0.0, atol=abs_tol_patch[i])

# %% differential correction in J2000 frame and normalized units
multi_shoot = MultipleShooting(eph, t_c=cr3bp.T / 2.0 / np.pi, l_c=cr3bp.L, precision=1e-8)
multi_shoot_epoch = MultipleShooting(eph, epoch_constr=True, t_c=cr3bp.T / 2.0 / np.pi,
                                     l_c=cr3bp.L, precision=1e-8)

t_exec = np.empty((3, 3))

print('\nMultiple shooting with fixed time\n')
t_exec[0, 0] = perf_counter()
t_j2000[:, 1], state_j2000[:, :, 1], _ = \
    multi_shoot.correct(t_j2000[:, 0], state_j2000[:, :, 0], var_time=False)
t_exec[0, 1] = perf_counter()

print('\nMultiple shooting with variable time\n')
t_exec[1, 0] = perf_counter()
t_j2000[:, 2], state_j2000[:, :, 2], _ = multi_shoot.correct(t_j2000[:, 0], state_j2000[:, :, 0])
t_exec[1, 1] = perf_counter()

print('\nMultiple shooting with variable time and epoch constraint\n')
t_exec[2, 0] = perf_counter()
t_j2000[:, 3], state_j2000[:, :, 3], sol_eph = \
    multi_shoot_epoch.correct(t_j2000[:, 0], state_j2000[:, :, 0])
t_exec[2, 1] = perf_counter()

# execution times (SEMAT: 73.945266 s, 75.582425 s, 89.093744 s)
t_exec[:, 2] = t_exec[:, 1] - t_exec[:, 0]
print('\nExecution times:\n')
for i, c in enumerate(('Fixed time', 'Variable time', 'Epoch constraint')):
    print(f"{c:17s}: {t_exec[i, 2]:10.5f} s")
print('\n')

# %% tests on multiple shooting outputs
t_j2000_mat = (dtm.time.adim.fix_corr, dtm.time.adim.var_corr, dtm.time.adim.eph_corr)
state_j2000_mat = (dtm.state.adim.fix_corr, dtm.state.adim.var_corr, dtm.state.adim.eph_corr)
abs_tol_time = (1e-11, 1e-9, 1e-9)
abs_tol_state = (1e-9, 1e-9, 1e-9)

for i, t in enumerate(t_j2000_mat):
    np.testing.assert_allclose(t_j2000[:, i + 1], t, rtol=0.0, atol=abs_tol_time[i])
for i, s in enumerate(state_j2000_mat):
    np.testing.assert_allclose(state_j2000[:, :, i + 1], s, rtol=0.0, atol=abs_tol_state[i])

np.testing.assert_allclose(sol_eph['x_vec'], dtm.xvec.x_corr, rtol=0.0, atol=1e-9)

# %% dimensionalization and updated initial epochs
t_j2000_dim[:, 1:4] = t_j2000[:, 1:4] * cr3bp.T / 2.0 / np.pi
state_j2000_dim[:, :, 1:4] = state_j2000[:, :, 1:4] * cr3bp.L
state_j2000_dim[:, 3:6, 1:4] /= cr3bp.T / 2.0 / np.pi

et0_corr = t_j2000_dim[0, 1:4]
utc0_corr = sp.et2utc(et0_corr, 'C', 5)

# %% change of coordinates
for i in range(1, 4):
    t_syn[:, i], state_syn[:, :, i] = j2000_to_synodic(np.ascontiguousarray(t_j2000[:, i]),
                                                       np.ascontiguousarray(state_j2000[:, :, i]),
                                                       et0_corr[i - 1], cr3bp, cr3bp.m1,
                                                       adim=True, bary_from_spice=True)

# %% test on coordinate change
t_syn_mat = (dtm.time.syn.fix_corr, dtm.time.syn.var_corr, dtm.time.syn.eph_corr)
state_syn_mat = (dtm.state.syn.fix_corr, dtm.state.syn.var_corr, dtm.state.syn.eph_corr)

for i, t in enumerate(t_syn_mat):
    np.testing.assert_allclose(t_syn[:, i + 1], t, rtol=0.0, atol=abs_tol_time[i])
for i, s in enumerate(state_syn_mat):
    np.testing.assert_allclose(state_syn[:, :, i + 1], s, rtol=0.0, atol=abs_tol_state[i])

# %% plot in synodic frame
labels = ('CR3BP orbit', 'Fix time', 'Variable time', 'Epoch constraint')
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot(halo.state_vec[:, 0], halo.state_vec[:, 1], halo.state_vec[:, 2],
        color='k', label=labels[0])
for i, c in enumerate(('b', 'g', 'r')):
    ax.plot(state_syn[:, 0, i + 1], state_syn[:, 1, i + 1], state_syn[:, 2, i + 1],
            color=c, label=labels[i + 1])
set_axes_equal(ax)
ax.set_title('EM L2 southern Halo orbit corrected in the SEM model')
ax.set_xlabel('x [-]')
ax.set_ylabel('y [-]')
ax.set_zlabel('z [-]')
ax.legend(loc=0)
plt.show()

sp.kclear()
