"""
Validation of the Multiple Shooting routines in the EM CR3BP model.

"""

# %% import modules
import os
import numpy as np
from scipy.io import loadmat
import matplotlib.pyplot as plt

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.orbits.halo import Halo

from src.propagation.patch_points_propagator import PatchPointsPropagator
from src.solve.multiple_shooting import MultipleShooting

from src.plotting.util import set_axes_equal
from src.__init__ import DIRNAME

# %% load data
fid = os.path.join(os.path.split(DIRNAME)[0], 'examples/validation/data',
                   'test_multiple_shooting_cr3bp.mat')
data_dict = loadmat(fid, squeeze_me=True, struct_as_record=False)
t_mat = data_dict['t']
y_mat = data_dict['y']
cjac_mat = data_dict['C0']

# %% init environment and Halo orbit
cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
halo = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Azdim=30e3)
halo.interpolation()

# %% init MultipleShooting object and outputs
multi_shoot = MultipleShooting(cr3bp, precision=1e-12)

t_corr = np.empty((t_mat.patch.size, 4))
state_corr = np.empty((t_mat.patch.size, 6, 4))

# %% constraints definition
pos_fix = 0  # fixed initial position
cjac_constr = (0, cjac_mat)  # fixed Jacobi constant at first patch point

# %% correction with fixed time, no boundary constraints
t_corr[:, 0], state_corr[:, :, 0], _ = \
    multi_shoot.correct(t_mat.patch, y_mat.patch, var_time=False)

# %% correction with fixed time, fixed initial position and Jacobi constant value
t_corr[:, 1], state_corr[:, :, 1], _ = \
    multi_shoot.correct(t_mat.patch, y_mat.patch, var_time=False, pos_fix=pos_fix,
                        cjac_constr=cjac_constr)

# %% correction with variable time, no boundary constraints
t_corr[:, 2], state_corr[:, :, 2], _ = \
    multi_shoot.correct(t_mat.patch, y_mat.patch, var_time=True)

# %% correction with variable time, fixed initial position and Jacobi constant value
t_corr[:, 3], state_corr[:, :, 3], _ = \
    multi_shoot.correct(t_mat.patch, y_mat.patch, var_time=True, pos_fix=pos_fix,
                        cjac_constr=cjac_constr)

# %% checks
for i, t in enumerate((t_mat.fix_nobc, t_mat.fix_bcs, t_mat.var_nobc, t_mat.var_bcs)):
    np.testing.assert_allclose(t_corr[:, i], t, rtol=0.0, atol=1e-11)
for i, y in enumerate((y_mat.fix_nobc, y_mat.fix_bcs, y_mat.var_nobc, y_mat.var_bcs)):
    np.testing.assert_allclose(state_corr[:, :, i], y, rtol=0.0, atol=1e-11)

# %% propagation
pp = PatchPointsPropagator(cr3bp, time_steps=50)

nb_states = (pp.prop.time_steps - 1) * (t_corr.shape[0] - 1) + 1
t_prop = np.empty((nb_states, 4))
state_prop = np.empty((nb_states, 6, 4))

for i in range(4):
    t_prop[:, i], state_prop[:, :, i] = pp.propagate(t_corr[:, i], state_corr[:, :, i])

# %% plot
labels = ('fix time no BC', 'fix time BC', 'var time no BC', 'var time BC')
colors = ('r', 'g', 'b', 'm')
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot(halo.state_vec[:, 0], halo.state_vec[:, 1], halo.state_vec[:, 2],
        label='Halo orbit', color='k')
for i, l in enumerate(labels):
    ax.plot(state_prop[:, 0, i], state_prop[:, 1, i], state_prop[:, 2, i],
            label=l, color=colors[i])
set_axes_equal(ax)
ax.set_title('EM L2 southern Halo orbit')
ax.set_xlabel('x [-]')
ax.set_ylabel('y [-]')
ax.set_zlabel('z [-]')
ax.legend(loc=0)
plt.show()
