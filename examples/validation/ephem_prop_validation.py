"""
Validation of the Ephemeris propagator against SEMAT and GMAT integrators.

Ephemeris model: Earth, Moon and Sun as point masses.
Initial conditions: patch points along an L2 Southern Halo orbit of vertical extension 30000 km
corrected in the ephemeris model with SEMAT's multiple shooting techniques.
Initial epoch: 01 June 2020 at 12:00:00.000 UTC.
Propagation time: 3 days or 259200 seconds.

Planetary constants and ephemerides sources:
1) planetary constants: pck kernel `gm_de431.tpc`
2) ephemerides: spk kernel `de430.bsp`
3) leap seconds: lsk kernel `naif0012.tls`

Data obtained in SEMAT and GMAT are stored in the `data` sub-folder as `SEMAT_results.json`
and `GMAT_results.json` respectively.

"""

from os import path
from timeit import Timer
import json
import numpy as np
from spiceypy import kclear

from src.utils.json_encoder import NumpyEncoder
from src.init.primary import Primary
from src.init.ephemeris import Ephemeris
from src.propagation.ephemeris_propagator import EphemerisPropagator
from src.init.load_kernels import load_kernels

load_kernels()


def compute_error(actual, target):
    """Compute minimum, average and maximum error along rows. """
    delta = np.abs(actual - np.asarray(target)) * 1e3 * 1e2  # in cm, cm/s
    error = np.empty((3, 6))
    error[0, :] = np.min(delta, axis=0)
    error[1, :] = np.mean(delta, axis=0)
    error[2, :] = np.max(delta, axis=0)
    return error


PERF_BENCH = False  # if False prevent performance benchmark and look at accuracy only
SAVE_RES = False  # if True store the obtained results under ephemeris_propagator_SEMPY.json

# load GMAT and SEMAT data
NAMES = ('GMAT', 'SEMAT', 'SEMPY')
DATA_DIR = path.join(path.dirname(__file__), 'data')
with open(path.join(DATA_DIR, ''.join(['ephemeris_propagator_', NAMES[0], '.json']))) as fid:
    GMAT_RES = json.load(fid)
with open(path.join(DATA_DIR, ''.join(['ephemeris_propagator_', NAMES[1], '.json']))) as fid:
    SEMAT_RES = json.load(fid)

# initial conditions were obtained in SEMAT, so we will use the same here
DELTA_T = SEMAT_RES['dt']
T_0 = np.asarray(SEMAT_RES['t0'])
STATE_0 = np.asarray(SEMAT_RES['y0'])

# instantiate an Ephemeris object
EPH = Ephemeris((Primary.EARTH, Primary.MOON, Primary.SUN))

# instantiate a propagator object
# the equations of motion are integrated in dimensional units of km and km/s. Time is in seconds
PROP = EphemerisPropagator(EPH, time_steps=None, max_internal_steps=4_000_000)

# init output vectors
T_F = np.empty(T_0.shape)
STATE_F = np.empty(STATE_0.shape)

# propagate
for i, t in enumerate(T_0):
    T_VI, STATE_VI, _, _ = PROP.propagate([t, t + DELTA_T], STATE_0[i, :])
    STATE_F[i, :] = STATE_VI[-1, :]
    T_F[i] = T_VI[-1]

# assert that the final times match, the position error is below 10 cm and the velocity error
# is below 1 mm/s
for res in (GMAT_RES, SEMAT_RES):
    np.testing.assert_array_equal(T_F, np.asarray(res['tf']))
    np.testing.assert_allclose(STATE_F[:, :3], np.asarray(res['yf'])[:, :3], rtol=0., atol=1e-4)
    np.testing.assert_allclose(STATE_F[:, 3:], np.asarray(res['yf'])[:, 3:], rtol=0., atol=1e-6)

# store results (if needed)
if SAVE_RES:
    SEMPY_RES = {'t0': SEMAT_RES['t0'], 'tf': T_F, 'y0': SEMAT_RES['y0'], 'yf': STATE_F}
    PTH = path.join(DATA_DIR, ''.join(['ephemeris_propagator_', NAMES[-1], '.json']))
    with open(PTH, 'w') as fid:
        json.dump(SEMPY_RES, fid, cls=NumpyEncoder)

# compute the minimum, maximum and average error in position and velocity at the end of the
# propagation
GMAT_ERR = compute_error(STATE_F, np.asarray(GMAT_RES['yf']))
SEMAT_ERR = compute_error(STATE_F, np.asarray(SEMAT_RES['yf']))
COMPS = ('x [cm]', 'y [cm]', 'z [cm]', 'vx [cm/s]', 'vy [cm/s]', 'vz [cm/s]')

print(f"\nMinimum, average and maximum error between {NAMES[-1]} and {NAMES[0]}/{NAMES[1]} after "
      f"propagating an initial state for {DELTA_T/86400} days:\n")
print(f"{'':12s}{COMPS[0]:^20s}{COMPS[1]:^20s}{COMPS[2]:^20s}{COMPS[3]:^20s}{COMPS[4]:^20s}"
      f"{COMPS[5]:^20s}\n")
for i, err in enumerate((GMAT_ERR, SEMAT_ERR)):
    for j, case in enumerate(('min', 'avg', 'max')):
        print(f"{NAMES[i]:6s}{case:4s}: {err[j, 0]:20.16f}{err[j, 1]:20.16f}{err[j, 2]:20.16f}"
              f"{err[j, 3]:20.16f}{err[j, 4]:20.16f}{err[j, 5]:20.16f}")
    print('')

# performance benchmark
# Hardware: laptop Lenovo with Intel Core i7-4720HQ CPU @ 2.60GHz and 8.0 GiB RAM
# Software: Debian GNU/Linux 10 (Buster), Python 3.7.7
# approximate results:
# SEMPY: 0.14 s
# SEMAT: 1.6 s
# GMAT: 1.0 to 9.0 s depending on integrator and error control scheme
NB_RUN = 100
NB_STATES = len(T_0)


def benchmark_ephemeris_propagator():
    """Utility function to be passed to Timer. """
    for k in range(NB_STATES):
        t_vec, state_vec, _, _ = PROP.propagate([T_0[k], T_0[k] + DELTA_T], STATE_0[k, :])
    return state_vec[-1, :], t_vec


if PERF_BENCH:
    benchmark_ephemeris_propagator()
    PROP_TIME = Timer(benchmark_ephemeris_propagator).timeit(NB_RUN) / NB_RUN
    print(f"Execution time, average of {NB_RUN} runs: {PROP_TIME} s\n")

kclear()
