"""
Comparison of the Ephemeris propagator against Orekit integrators.

Ephemeris model: Earth, Moon and Sun as point masses.
Initial conditions: patch points along an L2 Southern Halo orbit of vertical extension 30000 km
corrected in the ephemeris model with SEMAT's multiple shooting techniques.
Initial epoch: 01 June 2020 at 12:00:00.000 UTC.
Propagation time: 3 days or 259200 seconds.

Planetary constants and ephemerides sources for Sempy classes:
1) planetary constants: pck kernel `gm_de431.tpc`
2) ephemerides: spk kernel `de430.bsp`
3) leap seconds: lsk kernel `naif0012.tls`

Planetary constants and ephemerides sources for Orekit propagator:
1, 2) planetary constants and ephemerides: DE430 binaries in `orekit-data.zip`
3) leap seconds: `tai-utc.dat` in `orekit-data.zip`

Note: if using PyCharm IDE on Linux, please modify those settings to run the script from the
integrated Python Console (see https://gitlab.orekit.org/orekit-labs/python-wrapper/-/issues/418):
1) open a new Python Console
2) In the left-most column of icons change `Settings` (the small gearwheel),
    `Variables Loading Policy` to `On Demand`

"""

from os import path
from timeit import Timer
import json

from src.init.primary import Primary
from src.init.ephemeris import Ephemeris
from src.propagation.ephemeris_propagator import EphemerisPropagator
from src.utils.orekit_utils import *

from org.orekit.bodies import CelestialBodyFactory, CelestialBody
from org.orekit.frames import FramesFactory
from org.orekit.propagation.numerical import NumericalPropagator
from org.hipparchus.ode.nonstiff import DormandPrince853Integrator
from org.orekit.forces.gravity import NewtonianAttraction, ThirdBodyAttraction
from org.orekit.forces.inertia import InertialForces


def compute_error(actual, target):
    """Compute minimum, average and maximum error along rows. """
    delta = np.abs(actual - np.asarray(target)) * 1e3 * 1e2  # in cm, cm/s
    error = np.empty((3, 6))
    error[0, :] = np.min(delta, axis=0)
    error[1, :] = np.mean(delta, axis=0)
    error[2, :] = np.max(delta, axis=0)
    return error


PERF_BENCH = True  # if False prevent performance benchmark and look at accuracy only

# constants
NAMES = ('GMAT', 'SEMAT', 'OREKIT', 'SEMPY')
FRAME = FramesFactory.getGCRF()

# load GMAT and SEMAT data
with open(path.join(DATA_DIR, ''.join(['ephemeris_propagator_', NAMES[0], '.json']))) as fid:
    GMAT_RES = json.load(fid)
with open(path.join(DATA_DIR, ''.join(['ephemeris_propagator_', NAMES[1], '.json']))) as fid:
    SEMAT_RES = json.load(fid)

# initial conditions were obtained in SEMAT, so we will use the same here
DELTA_T = float(SEMAT_RES['dt'])
T_0 = np.asarray(SEMAT_RES['t0'])
STATE_0 = np.asarray(SEMAT_RES['y0'])
SC_STATE0 = []  # initial conditions for Orekit propagator
for i, t in enumerate(T_0):
    SC_STATE0.append(ndarray_et2sc_state(STATE_0[i, :], t, FRAME))

# instantiate an Ephemeris object
EPH = Ephemeris((Primary.EARTH, Primary.MOON, Primary.SUN))

# instantiate a propagator object
# the equations of motion are integrated in dimensional units of km and km/s. Time is in seconds
PROP = EphemerisPropagator(EPH, time_steps=None, max_internal_steps=4_000_000)

# instantiate an Orekit propagator object
INTEGRATOR = DormandPrince853Integrator(0.0, 1e20, 1e-14, 3e-14)
PROP_OREKIT = NumericalPropagator(INTEGRATOR)
PROP_OREKIT.setOrbitType(None)
PROP_OREKIT.setIgnoreCentralAttraction(True)
PROP_OREKIT.addForceModel(InertialForces(FRAME))
PROP_OREKIT.addForceModel(NewtonianAttraction(CelestialBodyFactory.getEarth().getGM()))
PROP_OREKIT.addForceModel(ThirdBodyAttraction(CelestialBodyFactory.getMoon()))
PROP_OREKIT.addForceModel(ThirdBodyAttraction(CelestialBodyFactory.getSun()))

# init output vectors
T_F = np.empty(T_0.shape)
STATE_F = np.empty(STATE_0.shape)
SC_STATE_F = []
T_F_OREKIT = np.empty(T_0.shape)
STATE_F_OREKIT = np.empty(STATE_0.shape)

# propagate with Sempy
for i, t in enumerate(T_0):
    T_VI, STATE_VI, _, _ = PROP.propagate([t, t + DELTA_T], STATE_0[i, :])
    STATE_F[i, :] = STATE_VI[-1, :]
    T_F[i] = T_VI[-1]

# assert that the final times match, the position error is below 10 cm and the velocity error
# is below 1 mm/s
for res in (GMAT_RES, SEMAT_RES):
    np.testing.assert_array_equal(T_F, np.asarray(res['tf']))
    np.testing.assert_allclose(STATE_F[:, :3], np.asarray(res['yf'])[:, :3], rtol=0., atol=1e-4)
    np.testing.assert_allclose(STATE_F[:, 3:], np.asarray(res['yf'])[:, 3:], rtol=0., atol=1e-6)

# propagate with Orekit
for i, abs_state in enumerate(SC_STATE0):
    np.testing.assert_equal(abs_date2et(abs_state.getDate()), T_0[i])
    np.testing.assert_allclose(pv_coords2ndarray(abs_state.getPVCoordinates(FRAME)), STATE_0[i],
                               rtol=0.0, atol=1e-10)
    PROP_OREKIT.resetInitialState(abs_state)
    SC_STATE_F.append(PROP_OREKIT.propagate(abs_state.getDate().shiftedBy(DELTA_T)))

# convert back to ndarray
for i, abs_state in enumerate(SC_STATE_F):
    STATE_F_OREKIT[i, :], T_F_OREKIT[i] = sc_state2ndarray_et(abs_state, FRAME)

# assert that the final times match, the position error is below 2 m and the velocity error
# is below 1 mm/s
np.testing.assert_allclose(T_F_OREKIT, T_F, rtol=0.0, atol=1e-4)
np.testing.assert_allclose(STATE_F_OREKIT[:, :3], STATE_F[:, :3], rtol=0.0, atol=2e-3)
np.testing.assert_allclose(STATE_F_OREKIT[:, 3:], STATE_F[:, 3:], rtol=0.0, atol=1e-4)
for res in (GMAT_RES, SEMAT_RES):
    np.testing.assert_allclose(T_F_OREKIT, np.asarray(res['tf']), rtol=0.0, atol=1e-4)
    np.testing.assert_allclose(STATE_F_OREKIT[:, :3], np.asarray(res['yf'])[:, :3],
                               rtol=0., atol=2e-3)
    np.testing.assert_allclose(STATE_F_OREKIT[:, 3:], np.asarray(res['yf'])[:, 3:],
                               rtol=0., atol=1e-6)

# compute the minimum, maximum and average error in position and velocity at the end of the
# propagation
GMAT_ERR = compute_error(STATE_F, np.asarray(GMAT_RES['yf']))
SEMAT_ERR = compute_error(STATE_F, np.asarray(SEMAT_RES['yf']))
OREKIT_ERR = compute_error(STATE_F, STATE_F_OREKIT)
COMPS = ('x [cm]', 'y [cm]', 'z [cm]', 'vx [cm/s]', 'vy [cm/s]', 'vz [cm/s]')

print(f"\nMinimum, average and maximum error between {NAMES[-1]} and {NAMES[0]}, {NAMES[1]}, "
      f"{NAMES[2]} after propagating an initial state for {DELTA_T/86400} days:\n")
print(f"{'':13s}{COMPS[0]:^30s}{COMPS[1]:^30s}{COMPS[2]:^30s}{COMPS[3]:^30s}{COMPS[4]:^30s}"
      f"{COMPS[5]:^30s}\n")
for i, err in enumerate((GMAT_ERR, SEMAT_ERR, OREKIT_ERR)):
    for j, case in enumerate(('min', 'avg', 'max')):
        print(f"{NAMES[i]:7s}{case:4s}: {err[j, 0]:30.16f}{err[j, 1]:30.16f}{err[j, 2]:30.16f}"
              f"{err[j, 3]:30.16f}{err[j, 4]:30.16f}{err[j, 5]:30.16f}")
    print('')

# performance benchmark
# Hardware: laptop Lenovo with Intel Core i7-4720HQ CPU @ 2.60GHz and 8.0 GiB RAM
# Software: Debian GNU/Linux 10 (Buster), Python 3.7.7
# approximate results:
# SEMPY: 0.14 s
# OREKIT: 0.22 s
# SEMAT: 1.6 s
# GMAT: 1.0 to 9.0 s depending on integrator and error control scheme
# (stored data are for highest accuracy and lowest performance)
NB_RUN = 10
NB_STATES = len(T_0)


def benchmark_ephemeris_propagator():
    """Utility function to be passed to Timer. """
    for k in range(NB_STATES):
        t_vec, state_vec, _, _ = PROP.propagate([T_0[k], T_0[k] + DELTA_T], STATE_0[k, :])
    return state_vec[-1, :], t_vec[-1]


def benchmark_orekit_propagator():
    """Utility function to be passed to Timer. """
    for scs in SC_STATE0:
        PROP_OREKIT.resetInitialState(scs)
        scs_final = PROP_OREKIT.propagate(scs.getDate().shiftedBy(DELTA_T))
    return scs_final


if PERF_BENCH:
    benchmark_ephemeris_propagator()
    EXEC_TIME = Timer(benchmark_ephemeris_propagator).timeit(NB_RUN) / NB_RUN
    benchmark_orekit_propagator()
    EXEC_TIME_OREKIT = Timer(benchmark_orekit_propagator).timeit(NB_RUN) / NB_RUN
    print(f"\nExecution time, average of {NB_RUN} runs:")
    print(f"{NAMES[-1]:7s}: {EXEC_TIME} s")
    print(f"{NAMES[-2]:7s}: {EXEC_TIME_OREKIT} s")
    print(f"Speed-up: {EXEC_TIME_OREKIT / EXEC_TIME}")
