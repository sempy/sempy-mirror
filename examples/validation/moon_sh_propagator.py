"""
Validation script for the Ephemeris propagator in the Sun-Earth-Moon full body problem with the
inclusion of the Moon's non-uniform gravity potential.

The irregular shape of the Moon is modelled with spherical harmonics series truncated
at degree and order 8.

Both Earth-centered and Moon-centered J2000 inertial frames are considered for the propagation
of the initial orbit's states.

"""

import os
import numpy as np
import spiceypy as sp
from scipy.io import loadmat

from src.init.primary import Primary
from src.init.ephemeris import Ephemeris
from src.init.load_kernels import load_kernels_moon_pot
from src.init.constants import GMAT_MJD_OFFSET

from src.propagation.ephemeris_propagator import EphemerisSHPropagator

load_kernels_moon_pot()  # load kernels, DE421 are used for consistency with Moon orientation data

# %% GMAT data
dirname = os.path.dirname(__file__)
gmat_data = loadmat(os.path.join(dirname, 'data', 'sh_dyna.mat'),
                    squeeze_me=True, struct_as_record=False)['gmat_data']

# %% environment definition
cases = ('Earth', 'Moon')  # Earth-centered and Moon-centered propagation
bodies = ((Primary.EARTH, Primary.MOON, Primary.SUN), (Primary.MOON, Primary.EARTH, Primary.SUN))
case_data = (gmat_data.eci, gmat_data.lci)

# %% propagation
for i, c in enumerate(cases):
    eph = Ephemeris(bodies[i], moon=('GRGM0050', 8, 8, True))  # SEM model with Moon SH
    prop = EphemerisSHPropagator(eph, time_steps=None)  # propagator instance
    et = np.zeros((case_data[i].utc0_mjd.size, 2))  # initial and final epochs
    et[:, 0] = \
        np.asarray([sp.utc2et(str(mjd + GMAT_MJD_OFFSET) + ' JD') for mjd in case_data[i].utc0_mjd])
    et[:, 1] = \
        np.asarray([sp.utc2et(str(mjd + GMAT_MJD_OFFSET) + ' JD') for mjd in case_data[i].utc_mjd])
    state0 = np.ascontiguousarray(case_data[i].y0)  # initial states
    state_final = np.zeros(state0.shape)  # final states
    for j in range(et.shape[0]):  # propagation
        _, sv, _, _ = prop.propagate(et[j], state0[j, :])
        state_final[j, :] = sv[1]
    err = np.abs(state_final - case_data[i].yf)  # error between SEMPY and GMAT final states
    print(f"\nPropagation in {c}-centered J2000 inertial frame\n")
    print(f"Max position error: {err[:, 0:3].max() * 1e3} m")
    print(f"Max velocity error: {err[:, 3:6].max() * 1e6} mm/s")
