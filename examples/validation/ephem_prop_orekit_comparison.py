"""
Comparison between EphemerisPropagator and Orekit propagators.

Note: if using PyCharm IDE on Linux, please modify those settings to run the script from the
integrated Python Console (see https://gitlab.orekit.org/orekit-labs/python-wrapper/-/issues/418):
1) open a new Python Console
2) In the left-most column of icons change `Settings` (the small gearwheel),
    `Variables Loading Policy` to `On Demand`

"""

import json
from scipy.integrate import odeint
from timeit import Timer

from src.init.ephemeris import Ephemeris
from src.init.primary import Primary
from src.propagation.ephemeris_propagator import EphemerisPropagator
from src.utils.orekit_utils import *

from orekit import JArray_double
from org.orekit.bodies import CelestialBodyFactory, CelestialBody
from org.orekit.frames import FramesFactory
from org.orekit.utils import PVCoordinatesProvider
from org.orekit.propagation.numerical import NumericalPropagator
from org.hipparchus.ode.nonstiff import DormandPrince853Integrator
from org.orekit.forces.gravity import NewtonianAttraction, ThirdBodyAttraction
from org.orekit.forces.inertia import InertialForces


def body_pos(body, et, ref):
    """Position of celestial body `body` at epoch `et` using Orekit ephemerides. """
    pos = PVCoordinatesProvider.cast_(body).getPVCoordinates(et2abs_date(et), ref).getPosition()
    return np.array([pos.getX(), pos.getY(), pos.getZ()]) * 1e-3


def eqm_6_orekit_ephemeris(t, state, gm, bodies, ref):
    """Equations of motion with planets positions from Orekit ephemerides. """
    state_dot = np.zeros(6)
    state_dot[0:3] = state[3:6]
    r_ki = state[0:3]
    acc = state_dot[3:6]
    for j in range(1, gm.size):
        r_kj = body_pos(bodies[j], t, ref)
        r_ji = r_ki - r_kj
        acc += - gm[j] * (r_ji * r_ji.dot(r_ji) ** -1.5 +
                          r_kj * r_kj.dot(r_kj) ** -1.5)
    acc += - gm[0] * r_ki * r_ki.dot(r_ki) ** -1.5
    return state_dot


def eqm_6_orekit_forces(t, state, bodies):
    """Equations of motion with acceleration from Orekit forces. """
    state_dot = np.zeros(6)
    state_dot[0:3] = state[3:6]
    acc = state_dot[3:6]
    scs = ndarray_et2sc_state(state, t, FRAME)
    for b in bodies:
        if b == EARTH:
            force = NewtonianAttraction(b.getGM())
        else:
            force = ThirdBodyAttraction(b)
        vec3d = force.acceleration(scs, JArray_double([b.getGM()]))
        acc += np.array([vec3d.getX(), vec3d.getY(), vec3d.getZ()]) * 1e-3
    return state_dot


def ndarray_pretty_print(x):
    """Print 6-dim ndarray. """
    print(f"P: {x[0]:.16f}, {x[1]:.16f}, {x[2]:.16f} km")
    print(f"V: {x[3]:.16f}, {x[4]:.16f}, {x[5]:.16f} km/s")


# constants from Orekit
FRAME = FramesFactory.getGCRF()
EARTH = CelestialBodyFactory.getEarth()
MOON = CelestialBodyFactory.getMoon()
SUN = CelestialBodyFactory.getSun()
BODIES_OREKIT = (EARTH, MOON, SUN)
GM_OREKIT = np.array([EARTH.getGM(), MOON.getGM(), SUN.getGM()]) * 1e-9  # km^3*s^-2

# init Ephemeris object
EPH = Ephemeris((Primary.EARTH, Primary.MOON, Primary.SUN))

# init EphemerisPropagator object
PROP = EphemerisPropagator(EPH, time_steps=None, max_internal_steps=4_000_000)

# init Orekit propagator
INT_OREKIT = DormandPrince853Integrator(0.0, 1e20, 1e-14, 3e-14)
PROP_OREKIT = NumericalPropagator(INT_OREKIT)
PROP_OREKIT.setOrbitType(None)
PROP_OREKIT.setIgnoreCentralAttraction(True)
PROP_OREKIT.addForceModel(InertialForces(FRAME))
PROP_OREKIT.addForceModel(NewtonianAttraction(EARTH.getGM()))
PROP_OREKIT.addForceModel(ThirdBodyAttraction(MOON))
PROP_OREKIT.addForceModel(ThirdBodyAttraction(SUN))

# load data as dictionary
with open(os.path.join(DATA_DIR, 'ephemeris_propagator_SEMAT.json')) as fid:
    DATA_DICT = json.load(fid)

# parse data to ndarray
T0_VEC = np.asarray(DATA_DICT['t0'])
TF_VEC = np.asarray(DATA_DICT['tf'])
UTC0_VEC = DATA_DICT['utc0']
UTC_F_VEC = DATA_DICT['utcf']
STATE0_VEC = np.asarray(DATA_DICT['y0'])
STATE_F_VEC = np.asarray(DATA_DICT['yf'])
DT = DATA_DICT['dt']

# initial conditions
idx = 58
et0 = T0_VEC[idx]
utc0 = UTC0_VEC[idx]
state0 = STATE0_VEC[idx, :]
sc_state0 = ndarray_et2sc_state(state0, et0, FRAME)
t_span = [T0_VEC[idx], T0_VEC[idx] + DT]

# propagation with EphemerisPropagator
t_vec, state_vec, _, _ = PROP.propagate(t_span, state0)
state_final = state_vec[-1, :]
t_final = t_vec[-1]
np.testing.assert_allclose(state_final, STATE_F_VEC[idx, :], rtol=0.0, atol=1e-5)
np.testing.assert_equal(t_final, TF_VEC[idx])

# propagation with odeint and Orekit ephemerides
state_vec_orekit_eph = \
    odeint(lambda t, state: eqm_6_orekit_ephemeris(t, state, GM_OREKIT, BODIES_OREKIT, FRAME),
           state0, t_span, tfirst=True, rtol=3e-14, atol=1e-14, mxstep=4000000)
state_final_orekit_eph = state_vec_orekit_eph[-1]

# propagation with odeint and Orekit forces
state_vec_orekit_forces = \
    odeint(lambda t, state: eqm_6_orekit_forces(t, state, BODIES_OREKIT),
           state0, t_span, tfirst=True, rtol=3e-14, atol=1e-14, mxstep=4000000)
state_final_orekit_forces = state_vec_orekit_forces[-1]

# propagation with Orekit integrator and force model
date_final = et2abs_date(t_span[-1])
PROP_OREKIT.setInitialState(sc_state0)
sc_state_final = PROP_OREKIT.propagate(date_final)
state_final_orekit, date_final_orekit = sc_state2ndarray_str(sc_state_final, FRAME)

print('\nInitial conditions:')
print('\nSEMPY at epoch ', sp.et2utc(t_span[0], 'C', 3))
ndarray_pretty_print(state0)
print('\nOREKIT at epoch ', sc_state0.getDate())
ndarray_pretty_print(sc_state2ndarray_str(sc_state0, FRAME)[0])
print('\nFinal conditions:')
print('\nSEMPY at epoch ', sp.et2utc(t_span[-1], 'C', 3))
ndarray_pretty_print(state_final)
print('\nOdeint with OREKIT ephemerides at epoch ', sp.et2utc(t_span[-1], 'C', 3))
ndarray_pretty_print(state_final_orekit_eph)
print('\nOdeint with OREKIT forces at epoch ', sp.et2utc(t_span[-1], 'C', 3))
ndarray_pretty_print(state_final_orekit_forces)
print('\nOREKIT at epoch ', sc_state_final.getDate())
ndarray_pretty_print(sc_state2ndarray_str(sc_state_final, FRAME)[0])
print('\n\nError SEMPY vs odeint with OREKIT ephemerides')
ndarray_pretty_print(np.abs(state_final_orekit_eph - state_final))
print('\nError SEMPY vs odeint with OREKIT forces')
ndarray_pretty_print(np.abs(state_final_orekit_forces - state_final))
print('\nError SEMPY vs OREKIT')
ndarray_pretty_print(np.abs(sc_state2ndarray_str(sc_state_final, FRAME)[0] - state_final))


# benchmark performance
def benchmark_sempy():
    return PROP.propagate(t_span, state0)


def benchmark_orekit():
    PROP_OREKIT.resetInitialState(sc_state0)
    scs = PROP_OREKIT.propagate(date_final)
    return scs


nb_runs = 10
benchmark_sempy()
dt_sempy = Timer(benchmark_sempy).timeit(number=nb_runs) / nb_runs
benchmark_orekit()
dt_orekit = Timer(benchmark_orekit).timeit(number=nb_runs) / nb_runs
print(f"\nExecution time, average of {nb_runs} runs:")
print(f"SEMPY:  {dt_sempy:20.16f} s")
print(f"OREKIT: {dt_orekit:20.16f} s")
print(f"Speed-up: {dt_orekit/dt_sempy}")
