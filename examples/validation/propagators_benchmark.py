"""
Cr3bp and Ephemeris propagator benchmark.

"""

# %% imports
import os
from scipy.io import loadmat
import numpy as np
import spiceypy as sp
from timeit import Timer

from src.init.constants import DAYS2SEC
from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.init.ephemeris import Ephemeris
from src.orbits.nrho import NRHO
from src.propagation.cr3bp_propagator import Cr3bpSynodicPropagator
from src.propagation.ephemeris_propagator import EphemerisPropagator
from src.coc.synodic_j2000 import synodic_to_j2000
from src.utils.orekit_utils import DATA_DIR
from src.init.load_kernels import load_kernels

load_kernels()

"""
from org.orekit.bodies import CelestialBodyFactory
from org.orekit.frames import FramesFactory
from org.orekit.propagation.numerical import NumericalPropagator
from org.hipparchus.ode.nonstiff import DormandPrince853Integrator
from org.orekit.forces.gravity import NewtonianAttraction, ThirdBodyAttraction
from org.orekit.forces.inertia import InertialForces
"""

# %% name of the file in which the results are stored or None to prevent from saving them
file_name = 'benchmark_results'

# %% SEMAT data
dm = loadmat(os.path.join(DATA_DIR, 'propagator_benchmark_semat.mat'),
             squeeze_me=True, struct_as_record=False)['data']

# %% cr3bp, ephemeris and NRHO initialization
cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

nro = NRHO(cr3bp, cr3bp.l2, NRHO.Family.northern, Azdim=75e3)
nro.interpolation()

eph = Ephemeris((Primary.EARTH, Primary.MOON, Primary.SUN))

# %% initial conditions and propagation intervals
l_c = cr3bp.L
t_c = cr3bp.T / 2.0 / np.pi
et0 = sp.str2et('2020 JUN 01 12:00:00.000')

state0_syn = nro.state0
t0_j2000, state0_j2000 = synodic_to_j2000(np.asarray([0.0]), np.asarray([state0_syn]),
                                          et0, cr3bp, cr3bp.m1, bary_from_spice=True, adim=True)
t0_j2000_dim, state0_j2000_dim = synodic_to_j2000(np.asarray([0.0]), np.asarray([state0_syn]),
                                                  et0, cr3bp, cr3bp.m1, bary_from_spice=True)

t_span_syn = np.array([0.0, nro.T])
t_span_j2000 = np.array([0.0, DAYS2SEC / t_c]) + t0_j2000
t_span_j2000_dim = np.array([0.0, DAYS2SEC]) + t0_j2000_dim

t_span = np.empty((2, 3))
t_span[:, 0] = t_span_syn
t_span[:, 1] = t_span_j2000
t_span[:, 2] = t_span_j2000_dim

state0 = np.empty((6, 3))
state0[:, 0] = state0_syn
state0[:, 1] = state0_j2000
state0[:, 2] = state0_j2000_dim

# %% assertion on initial conditions
np.testing.assert_allclose(l_c, dm.l_c, rtol=0.0, atol=0.0)
np.testing.assert_allclose(t_c, dm.t_c, rtol=0.0, atol=0.0)
np.testing.assert_allclose(et0, dm.et0, rtol=0.0, atol=0.0)

np.testing.assert_allclose(t_span, dm.t_span, rtol=1e-10, atol=0.0)
np.testing.assert_allclose(state0, dm.y0, rtol=1e-10, atol=0.0)

# %% propagators setup
prop6_syn = Cr3bpSynodicPropagator(cr3bp.mu, time_steps=None)
prop42_syn = Cr3bpSynodicPropagator(cr3bp.mu, with_stm=True, time_steps=None)

prop6_j2000 = EphemerisPropagator(eph, time_steps=None, max_internal_steps=4_000_000,
                                  t_c=t_c, l_c=l_c)
prop42_j2000 = EphemerisPropagator(eph, with_stm=True, time_steps=None,
                                   max_internal_steps=4_000_000, t_c=t_c, l_c=l_c)
prop48_j2000 = EphemerisPropagator(eph, with_epoch_partials=True, time_steps=None,
                                   max_internal_steps=4_000_000, t_c=t_c, l_c=l_c)

prop6_j2000_dim = EphemerisPropagator(eph, time_steps=None, max_internal_steps=4_000_000)
prop42_j2000_dim = EphemerisPropagator(eph, with_stm=True, time_steps=None,
                                       max_internal_steps=4_000_000)
prop48_j2000_dim = EphemerisPropagator(eph, with_epoch_partials=True, time_steps=None,
                                       max_internal_steps=4_000_000)

# %% propagation
tv6_syn, sv6_syn, _, _ = prop6_syn.propagate(t_span[:, 0], state0[:, 0])
tv42_syn, sv42_syn, _, _ = prop42_syn.propagate(t_span[:, 0], state0[:, 0])

tv6_j2000, sv6_j2000, _, _ = prop6_j2000.propagate(t_span[:, 1], state0[:, 1])
tv42_j2000, sv42_j2000, _, _ = prop42_j2000.propagate(t_span[:, 1], state0[:, 1])
tv48_j2000, sv48_j2000, _, _ = prop48_j2000.propagate(t_span[:, 1], state0[:, 1])

tv6_j2000_dim, sv6_j2000_dim, _, _ = prop6_j2000_dim.propagate(t_span[:, 2], state0[:, 2])
tv42_j2000_dim, sv42_j2000_dim, _, _ = prop42_j2000_dim.propagate(t_span[:, 2], state0[:, 2])
tv48_j2000_dim, sv48_j2000_dim, _, _ = prop48_j2000_dim.propagate(t_span[:, 2], state0[:, 2])

# %% assertion on propagation outputs
for i, tv in enumerate((tv6_syn, tv6_j2000, tv6_j2000_dim)):
    np.testing.assert_allclose(tv, dm.t_vec6[:, i], rtol=0.0, atol=1e-10)
for i, tv in enumerate((tv42_syn, tv42_j2000, tv42_j2000_dim)):
    np.testing.assert_allclose(tv, dm.t_vec42[:, i], rtol=0.0, atol=1e-10)
for i, tv in enumerate((tv48_j2000, tv48_j2000_dim)):
    np.testing.assert_allclose(tv, dm.t_vec48[:, i], rtol=0.0, atol=1e-10)

np.testing.assert_allclose(sv6_syn, dm.y_vec6[:, :, 0], rtol=0.0, atol=1e-8)
np.testing.assert_allclose(sv6_j2000, dm.y_vec6[:, :, 1], rtol=0.0, atol=1e-8)
np.testing.assert_allclose(sv6_j2000_dim, dm.y_vec6[:, :, 2], rtol=0.0, atol=1e-4)

np.testing.assert_allclose(sv42_syn, dm.y_vec42[:, :, 0], rtol=0.0, atol=1e-5)
np.testing.assert_allclose(sv42_j2000, dm.y_vec42[:, :, 1], rtol=0.0, atol=1e-5)
np.testing.assert_allclose(sv42_j2000_dim, dm.y_vec42[:, :, 2], rtol=0.0, atol=1e-3)

np.testing.assert_allclose(sv48_j2000, dm.y_vec48[:, :, 0], rtol=0.0, atol=1e-5)
np.testing.assert_allclose(sv48_j2000_dim, dm.y_vec48[:, :, 1], rtol=0.0, atol=1e-3)

# %% Orekit propagator setup

"""
frame = FramesFactory.getGCRF()
earth = CelestialBodyFactory.getEarth()
moon = CelestialBodyFactory.getMoon()
sun = CelestialBodyFactory.getSun()

integrator = DormandPrince853Integrator(0.0, 1e30, 1e-14, 3e-14)
prop_orekit = NumericalPropagator(integrator)
prop_orekit.setOrbitType(None)
prop_orekit.setIgnoreCentralAttraction(True)
prop_orekit.addForceModel(InertialForces(frame))
prop_orekit.addForceModel(NewtonianAttraction(earth.getGM()))
prop_orekit.addForceModel(ThirdBodyAttraction(moon))
prop_orekit.addForceModel(ThirdBodyAttraction(sun))

sc_state0 = ndarray_et2sc_state(state0[:, -1], et0, frame)
date_final = et2abs_date(t_span[1, -1])
prop_orekit.setInitialState(sc_state0)

# %% Orekit propagation
sc_state_final = prop_orekit.propagate(date_final)

# %% Orekit assertions
sv_orekit = np.empty((2, 6))
tv_orekit = np.empty(2)
sv_orekit[0, :], tv_orekit[0] = sc_state2ndarray_et(sc_state0, frame)
sv_orekit[1, :], tv_orekit[1] = sc_state2ndarray_et(sc_state_final, frame)

np.testing.assert_allclose(tv_orekit, dm.t_vec6[:, 2], rtol=1e-10, atol=0.0)
np.testing.assert_allclose(sv_orekit, dm.y_vec6[:, :, 2], rtol=0.0, atol=1e-3)
"""


# %% benchmark functions
def benchmark_prop6_syn():
    return prop6_syn.propagate(t_span[:, 0], state0[:, 0])


def benchmark_prop42_syn():
    return prop42_syn.propagate(t_span[:, 0], state0[:, 0])


def benchmark_prop6_j2000():
    return prop6_j2000.propagate(t_span[:, 1], state0[:, 1])


def benchmark_prop42_j2000():
    return prop42_j2000.propagate(t_span[:, 1], state0[:, 1])


def benchmark_prop48_j2000():
    return prop48_j2000.propagate(t_span[:, 1], state0[:, 1])


def benchmark_prop6_j2000_dim():
    return prop6_j2000_dim.propagate(t_span[:, 2], state0[:, 2])


def benchmark_prop42_j2000_dim():
    return prop42_j2000_dim.propagate(t_span[:, 2], state0[:, 2])


def benchmark_prop48_j2000_dim():
    return prop48_j2000_dim.propagate(t_span[:, 2], state0[:, 2])


# def benchmark_orekit():
#    prop_orekit.resetInitialState(sc_state0)
#    return prop_orekit.propagate(date_final)


# %% benchmark
nb_runs = 50
t_exec_sempy = np.zeros((8, 1))
t_exec_orekit = np.zeros((8, 1))

benchmark_prop6_syn()
t_exec_sempy[0, 0] = Timer(benchmark_prop6_syn).timeit(number=nb_runs) / nb_runs

benchmark_prop42_syn()
t_exec_sempy[1, 0] = Timer(benchmark_prop42_syn).timeit(number=nb_runs) / nb_runs

benchmark_prop6_j2000()
t_exec_sempy[2, 0] = Timer(benchmark_prop6_j2000).timeit(number=nb_runs) / nb_runs

benchmark_prop42_j2000()
t_exec_sempy[3, 0] = Timer(benchmark_prop42_j2000).timeit(number=nb_runs) / nb_runs

benchmark_prop48_j2000()
t_exec_sempy[4, 0] = Timer(benchmark_prop48_j2000).timeit(number=nb_runs) / nb_runs

benchmark_prop6_j2000_dim()
t_exec_sempy[5, 0] = Timer(benchmark_prop6_j2000_dim).timeit(number=nb_runs) / nb_runs

benchmark_prop42_j2000_dim()
t_exec_sempy[6, 0] = Timer(benchmark_prop42_j2000_dim).timeit(number=nb_runs) / nb_runs

benchmark_prop48_j2000_dim()
t_exec_sempy[7, 0] = Timer(benchmark_prop48_j2000_dim).timeit(number=nb_runs) / nb_runs

# benchmark_orekit()
# t_exec_orekit[5, 0] = Timer(benchmark_orekit).timeit(number=nb_runs) / nb_runs

# %% compare performance and store results (optional)
t_exec_sempy *= 1e3
t_exec_orekit *= 1e3
t_exec_mat = dm.t_exec.reshape((8, 1)) * 1e3
t_exec_mex = dm.t_exec_mex.reshape((8, 1)) * 1e3

benchmark_results = np.hstack((t_exec_sempy, t_exec_mat, t_exec_mat / t_exec_sempy, t_exec_mex,
                               t_exec_mex / t_exec_sempy, t_exec_orekit,
                               t_exec_orekit / t_exec_sempy))

if file_name is not None:
    with open(os.path.join(DATA_DIR, file_name + '.npy'), 'wb') as fid:
        np.save(fid, benchmark_results, allow_pickle=False)
    with open(os.path.join(DATA_DIR, file_name + '.csv'), 'w+') as fid:
        np.savetxt(fid, benchmark_results, fmt='%20.3f', delimiter=',',
                   header='Execution times (ms) and speed-up of SEMPY vs MATLAB ode113, '
                          'MEX ode78, Orekit')
