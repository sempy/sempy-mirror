"""
Comparison between single and multiple shooting procedures in the CR3BP.

The test comprises two scenarios in which a periodic Earth-Moon Halo orbit is obtained via
differential correction techniques starting from an approximate initial state computed with a
third-order Richardson approximation.

In the first case, an L2 southern Halo orbit with vertical extension of 15000 km is initialized
and corrected with both x0-fixed and minimum norm solution single shooting techniques.
The hypotheses at the root of the aforementioned procedures are then converted in the corresponding
states and times constraints to obtain an equivalent formulation of the correction algorithms
using the multiple shooting implementation with only two patch points.
It is then demonstrated that the results obtained with single and multiple shooting techniques are
equivalent within the selected tolerance for the differential correction procedures.

In the second case, an L1 southern Halo orbit with vertical extension of 30000 km is initialized
and corrected with both z0-fixed and minimum norm solution single shooting techniques.
The same procedure described above is then repeated to verify the equivalence between single
shooting and multiple shooting problem formulation.

"""

# %% import modules
import numpy as np
import matplotlib.pyplot as plt

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp

from src.orbits.halo import Halo
from src.orbits.thirdorder_orbit import ThirdOrderOrbit

from src.propagation.cr3bp_propagator import Cr3bpSynodicPropagator
from src.solve.ode_event import xz_plane_event
from src.solve.diff_corr_3d import DiffCorr3D
from src.solve.multiple_shooting import MultipleShooting
from src.plotting.util import set_axes_equal

# %% init CR3BP structure for the Earth-Moon system
cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

# small L2 southern Halo with vertical extension of 15000 km for correction with z0 fixed
halo_small = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Azdim=15e3)
halo_small_3rd = ThirdOrderOrbit(halo_small)  # compute Richardson approximation
halo_small.interpolation()
state0_small_3rd = halo_small_3rd.state0.copy()  # initial guess for correction procedure

# big L1 southern Halo with vertical extension of 30000 km for correction with x0 fixed
halo_big = Halo(cr3bp, cr3bp.l1, Halo.Family.southern, Azdim=30e3)
halo_big_3rd = ThirdOrderOrbit(halo_big)  # compute Richardson approximation
halo_big.interpolation()
state0_big_3rd = halo_big_3rd.state0.copy()  # initial guess for correction procedure

# propagation of the approximate initial conditions for half orbit period and sampling
nb_pts = 2  # number of patch points indexes
prop = Cr3bpSynodicPropagator(cr3bp.mu, time_steps=nb_pts)

# small L2 Halo
_, _, t_xz_cross, _ = prop.propagate([0.0, 3.0], state0_small_3rd.copy(),
                                     events=xz_plane_event)
t_patch_small, state_patch_small, _, _ = \
    prop.propagate([0.0, t_xz_cross[0][0]], state0_small_3rd.copy())
state_patch_small[-1, -5::2] = 0.0  # correct numerical inaccuracy

# big L1 Halo
_, _, t_xz_cross, _ = prop.propagate([0.0, 5.0], state0_big_3rd.copy(), events=xz_plane_event)
t_patch_big, state_patch_big, _, _ = \
    prop.propagate([0.0, t_xz_cross[0][0]], state0_big_3rd.copy())
state_patch_big[-1, -5::2] = 0.0  # correct numerical inaccuracy

# %% init single shooting and multiple shooting routines with equivalent tolerances
single_shoot = DiffCorr3D(cr3bp.mu, maxiter=20)
multi_shoot = MultipleShooting(cr3bp, precision=1e-11, maxiter=20)

# %% constraint definition for multiple shooting implementation

# fixed time at first patch point
t_mask = np.zeros(nb_pts, dtype=np.bool)
t_mask[0] = True

# constraints on state components for correction with z0 fixed
# (xi0 = (0, 4) for single shooting):
# [y0, z0, vx0, vz0]^T = [0.0, z0, 0.0, 0.0]^T fixed,
# [yf, vxf, vzf]^T = [0.0, 0.0, 0.0]^T targeted
state_mask_z0 = np.zeros(6 * nb_pts, dtype=np.bool)
state_mask_z0[1:6:2] = state_mask_z0[2] = True
state_mask_z0[-5::2] = True

# constraints on state components for correction with x0 fixed
# (xi0 = (2, 4) for single shooting):
# [x0, y0, vx0, vz0]^T = [x0, 0.0, 0.0, 0.0]^T fixed,
# [yf, vxf, vzf]^T = [0.0, 0.0, 0.0]^T targeted
state_mask_x0 = np.zeros(6 * nb_pts, dtype=np.bool)
state_mask_x0[0] = state_mask_x0[1:6:2] = True
state_mask_x0[-5::2] = True

# constraint on state components for correction with minimum norm solution:
# [y0, vx0, vz0]^T = [0.0, 0.0, 0.0]^T fixed, [yf vxf vzf]^T = [0.0, 0.0, 0.0]^T targeted
state_mask_mn = np.zeros(6 * nb_pts, dtype=np.bool)
state_mask_mn[1:6:2] = True
state_mask_mn[-5::2] = True
constr_mask = np.zeros(6 * (nb_pts - 1), dtype=np.bool)
constr_mask[-6::2] = True

# %% correction of the small L2 Halo with z0 fixed
state0_corr_small_z0, t12_corr_small_z0, _ = \
    single_shoot.diff_corr_3d_bb(state0_small_3rd.copy(), (0, 4), (3, 5))
t_corr_small_z0, state_corr_small_z0, _ = \
    multi_shoot.correct(t_patch_small.copy(), state_patch_small.copy(),
                        state_mask=state_mask_z0, t_mask=t_mask)

# check
np.testing.assert_allclose(t12_corr_small_z0, t_corr_small_z0[-1], rtol=0.0, atol=1e-12)
np.testing.assert_allclose(state0_corr_small_z0, state_corr_small_z0[0, :],
                           rtol=0.0, atol=1e-12)

# %% correction of the small L2 Halo with minimum norm solution
state0_corr_small_mn, t12_corr_small_mn, _, _, _ = \
    single_shoot.diff_corr_3d_full(state0_small_3rd.copy())
t_corr_small_mn, state_corr_small_mn, _ = \
    multi_shoot.correct(t_patch_small.copy(), state_patch_small.copy(),
                        state_mask=state_mask_mn, t_mask=t_mask, constr_mask=constr_mask)

# check
np.testing.assert_allclose(t12_corr_small_mn, t_corr_small_mn[-1], rtol=0.0, atol=1e-12)
np.testing.assert_allclose(state0_corr_small_mn, state_corr_small_mn[0, :],
                           rtol=0.0, atol=1e-12)

# %% correction of the big L1 Halo with x0 fixed
state0_corr_big_x0, t12_corr_big_x0, _ = \
    single_shoot.diff_corr_3d_bb(state0_big_3rd.copy(), (2, 4), (3, 5))
t_corr_big_x0, state_corr_big_x0, _ = \
    multi_shoot.correct(t_patch_big.copy(), state_patch_big.copy(),
                        state_mask=state_mask_x0, t_mask=t_mask)

# check
np.testing.assert_allclose(t12_corr_big_x0, t_corr_big_x0[-1], rtol=0.0, atol=1e-12)
np.testing.assert_allclose(state0_corr_big_x0, state_corr_big_x0[0, :], rtol=0.0, atol=1e-12)

# %% correction of the big L1 Halo with minimum norm solution
state0_corr_big_mn, t12_corr_big_mn, _, _, _ = \
    single_shoot.diff_corr_3d_full(state0_big_3rd.copy())
t_corr_big_mn, state_corr_big_mn, _ = \
    multi_shoot.correct(t_patch_big.copy(), state_patch_big.copy(), state_mask=state_mask_mn,
                        t_mask=t_mask, constr_mask=constr_mask)

# check
np.testing.assert_allclose(t12_corr_big_mn, t_corr_big_mn[-1], rtol=0.0, atol=1e-12)
np.testing.assert_allclose(state0_corr_big_mn, state_corr_big_mn[0, :], rtol=0.0, atol=1e-12)

# %% propagation of corrected initial conditions
prop2 = Cr3bpSynodicPropagator(cr3bp.mu, time_steps=1000)

tv_single_small_z0, sv_single_small_z0, _, _ = \
    prop2.propagate([0.0, 2 * t12_corr_small_z0], state0_corr_small_z0)
tv_multi_small_z0, sv_multi_small_z0, _, _ = \
    prop2.propagate([0.0, 2 * t_corr_small_z0[-1]], state_corr_small_z0[0, :])
tv_single_small_mn, sv_single_small_mn, _, _ = \
    prop2.propagate([0.0, 2 * t12_corr_small_mn], state0_corr_small_mn)
tv_multi_small_mn, sv_multi_small_mn, _, _ = \
    prop2.propagate([0.0, 2 * t_corr_small_mn[-1]], state_corr_small_mn[0, :])

tv_single_big_x0, sv_single_big_x0, _, _ = \
    prop2.propagate([0.0, 2 * t12_corr_big_x0], state0_corr_big_x0)
tv_multi_big_x0, sv_multi_big_x0, _, _ = \
    prop2.propagate([0.0, 2 * t_corr_big_x0[-1]], state_corr_big_x0[0, :])
tv_single_big_mn, sv_single_big_mn, _, _ = \
    prop2.propagate([0.0, 2 * t12_corr_big_mn], state0_corr_big_mn)
tv_multi_big_mn, sv_multi_big_mn, _, _ = \
    prop2.propagate([0.0, 2 * t_corr_big_mn[-1]], state_corr_big_mn[0, :])

# %% display of propagated states in the synodic frame
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot(halo_small.state_vec[:, 0], halo_small.state_vec[:, 1], halo_small.state_vec[:, 2],
        color='k', label=f"L2 Halo Az={halo_small.Azdim_estimate:.0f} km")
ax.plot(halo_big.state_vec[:, 0], halo_big.state_vec[:, 1], halo_big.state_vec[:, 2],
        color='b', label=f"L2 Halo Az={halo_big.Azdim_estimate:.0f} km")
ax.scatter(cr3bp.l2.position[0], 0., 0., color='g', label='L2')
ax.scatter(cr3bp.l1.position[0], 0., 0., color='r', label='L1')
ax.scatter(cr3bp.m2_pos[0], 0., 0., color='k', label='Moon')

ax.plot(sv_single_small_z0[:, 0], sv_single_small_z0[:, 1], sv_single_small_z0[:, 2],
        label=f"z0-fix single Az={halo_small.Azdim_estimate:.0f} km")
ax.plot(sv_multi_small_z0[:, 0], sv_multi_small_z0[:, 1], sv_multi_small_z0[:, 2], '--',
        label=f"z0-fix multiple Az={halo_small.Azdim_estimate:.0f} km")
ax.plot(sv_single_small_mn[:, 0], sv_single_small_mn[:, 1], sv_single_small_mn[:, 2],
        label=f"min norm single Az={halo_small.Azdim_estimate:.0f} km")
ax.plot(sv_multi_small_mn[:, 0], sv_multi_small_mn[:, 1], sv_multi_small_mn[:, 2], '--',
        label=f"min norm multiple Az={halo_small.Azdim_estimate:.0f} km")

ax.plot(sv_single_big_x0[:, 0], sv_single_big_x0[:, 1], sv_single_big_x0[:, 2],
        label=f"x0-fix single Az={halo_big.Azdim_estimate:.0f} km")
ax.plot(sv_multi_big_x0[:, 0], sv_multi_big_x0[:, 1], sv_multi_big_x0[:, 2], '--',
        label=f"x0-fix multiple Az={halo_big.Azdim_estimate:.0f} km")
ax.plot(sv_single_big_mn[:, 0], sv_single_big_mn[:, 1], sv_single_big_mn[:, 2],
        label=f"min norm single Az={halo_big.Azdim_estimate:.0f} km")
ax.plot(sv_multi_big_mn[:, 0], sv_multi_big_mn[:, 1], sv_multi_big_mn[:, 2], '--',
        label=f"min norm multiple Az={halo_big.Azdim_estimate:.0f} km")

set_axes_equal(ax)
ax.set_title('Corrected L1 and L2 Halo orbits')
ax.set_xlabel('x [-]')
ax.set_ylabel('y [-]')
ax.set_zlabel('z [-]')
ax.legend(loc=0)

plt.show()
