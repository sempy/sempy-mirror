#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

The aim of this scrip is to show the capabilities of the sempy plotting module:
    MASTERPLOTTER : CHANGING THE TITLES OF THE OUTPUT FIGURES. 
"""

# ___________imports_____________
from src.init.primary import Primary

from src.init.cr3bp import Cr3bp

from src.orbits.halo import Halo

"""Importing the classes needed for plotting..."""
import matplotlib.pyplot as plt

from src.plotting.plotting_module import PlottingOptions, MasterPlotter

# ___________computations____________
CRTBP = Cr3bp(Primary.EARTH, Primary.MOON)
orbit = Halo(CRTBP, CRTBP.l2, Halo.Family.southern, Azdim=12000)
orbit.computation()

# ___________plotting_________________
"""This line turns the interactive mode off. It is necessary to switch off the interactive
mode before using the classes of the sempy plotting module."""
plt.ioff()
"""It closes eventual opened figures."""
plt.close("all")
"""Initialization of the plotting options. """
plotting_options = PlottingOptions()
# a plotting option object has been generated, if nothing is passed. The defaut parameters are kept.
"""Creation of a MasterPlotter object"""
plot = MasterPlotter(plotting_options, orbit)
# the 2D views are displayes by running the following method
plot.scene_twoD()
# the 3D view is displayed by running the following method
plot.scene_TD()
# changing the Title
plot.modify_title("FORZA NAPOLI \n second line")

