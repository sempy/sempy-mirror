#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

The aim of this scrip is to show the capabilities of the sempy plotting module:
    USING DIFFERENT MASTERPLOTTERS REFERRING TO DIFFERENT CR3BP PROBLEMS WITHIN THE SAME SCRIPT.
"""
# ___________imports_____________
from src.init.primary import Primary

from src.init.cr3bp import Cr3bp

from src.orbits.halo import Halo

"""Importing the classes needed for plotting..."""
import matplotlib.pyplot as plt

from src.plotting.plotting_module import PlottingOptions, MasterPlotter

# ___________computations____________
"""EARTH - MOON CR3BP is initialised"""
CRTBP = Cr3bp(Primary.SUN, Primary.EARTH)
"""TWO Orbits associated to the EARTH - MOON CR3BP are computed"""
# 1st
orbit = Halo(CRTBP, CRTBP.l2, Halo.Family.southern, Azdim=125000)
orbit.computation()
# 2nd (Same libration point, different Azdim)
orbit1 = Halo(CRTBP, CRTBP.l2, Halo.Family.southern, Azdim=18000)
orbit1.computation()

"""SUN - EARTH CR3BP is initialised"""
CRTBP1 = Cr3bp(Primary.SUN, Primary.EARTH)
"""An orbit  associated to the SUN - EARTH CR3BP is computed"""
orbit3 = Halo(CRTBP1, CRTBP1.l2, Halo.Family.southern, Azdim=125000)
orbit3.computation()
# ___________plotting_________________
"""This line turns the interactive mode off. It is necessary to switch off the interactive
mode before using the classes of the sempy plotting module."""
plt.ioff()
"""It closes eventual opened figures."""
plt.close("all")
"""Initialization of the plotting options. """
plotting_options = PlottingOptions()
# a plotting option object has been generated keeping the default options.
plotting_options1 = PlottingOptions(XY=False)
# a different plotting object has been generated.
"""Creation of different MasterPlotter objects"""
# Within a script it is possible to call different MasterPlotter objects, each object will be completely
# indipendent from the others. This means that each object can produce its own figures accordign to the plotting
# options it is referring to.
# The indipendence is referred also to the CR3BP problems we would like to analyse
plot = MasterPlotter(plotting_options1, orbit)  # , orbit1 = orbit1)
plot.scene_twoD()
plot.scene_TD()
# the first MasterPlotter object produces its own figures referring to the object plotting_options.
# The input orbits of plot belong to CRTBP (EARTH - MOON)
plot1 = MasterPlotter(
    plotting_options1, orbit3
)  # only one orbit will be input in plot1
plot1.scene_twoD()
plot1.scene_TD()
# the second MasterPlotter object produces its own figures referring to the object plotting_options1.
# The input orbit of plot1 belongs to CRTBP1 (SUN -EARTH )
