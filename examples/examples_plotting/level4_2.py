#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
The aim of this scrip is to show the capabilities of the sempy plotting module:
    MASTERPLOTTER INNER CLASSES : COUPLING BETWEEN PLOTTERORBITMANIFOLDS.
"""
# ___________imports_____________
from src.init.primary import Primary

from src.init.cr3bp import Cr3bp

from src.orbits.halo import Halo

"""Importing the classes needed for plotting..."""
import matplotlib.pyplot as plt

from src.plotting.plotting_module import PlottingOptions, MasterPlotter

# ___________computations____________
"""EARTH - MOON CR3BP is initialised"""
CRTBP = Cr3bp(Primary.EARTH, Primary.MOON)
"""TWO Orbits associated to the EARTH - MOON CR3BP are computed"""
# 1st
orbit = Halo(CRTBP, CRTBP.l2, Halo.Family.southern, Azdim=12000)
orbit.computation()
# 2nd (Same libration point, different Azdim)
orbit1 = Halo(CRTBP, CRTBP.l2, Halo.Family.southern, Azdim=18000)
orbit1.computation()


# ___________plotting_________________
"""This line turns the interactive mode off. It is necessary to switch off the interactive
mode before using the classes of the sempy plotting module."""
plt.ioff()
"""It closes eventual opened figures."""
plt.close("all")
"""Initialization of the plotting options. """
plotting_options = PlottingOptions(legend=False)
# a plotting option object has been generated keeping the default options.

"""Creation of two MasterPlotter.PlotterOrbitManifolds objects"""
# after some computations lets suppose we want to analyse a trajectory
# it is possible to create an object of the MasterPlotter.PlotterOrbitManifolds inner class as following
plot = MasterPlotter.PlotterOrbitManifolds(plotting_options, orbit)
plot.plot_new()

plot1 = MasterPlotter.PlotterOrbitManifolds(plotting_options, orbit1)
plot1.plot_already_existing(plot)
