#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

The aim of this scrip is to show the capabilities of the sempy plotting module:
    PLOTTING OPTIONS : LEGEND.
"""
# ___________imports_____________
from src.init.primary import Primary

from src.init.cr3bp import Cr3bp

from src.orbits.halo import Halo

"""Importing the classes needed for plotting..."""
import matplotlib.pyplot as plt

from src.plotting.plotting_module import PlottingOptions, MasterPlotter

# ___________computations____________
CRTBP = Cr3bp(Primary.EARTH, Primary.MOON)

orbit = Halo(CRTBP, CRTBP.l2, Halo.Family.southern, Azdim=12000)
orbit.computation()


# ___________plotting_________________
"""This line turns the interactive mode off. It is necessary to switch off the interactive
mode before using the classes of the sempy plotting module."""
plt.ioff()
"""It closes eventual opened figures."""
plt.close("all")
"""Initialization of the plotting options. """
# In the legend only informations about the orbits are displayed.
# It is possible to swhich off the legend by typing the following keword argument:
# legend = True/False
# by default the Legend is shown.
# When set to True/False the legend appears/disappears in all the requested views automatically.

"""Example"""
plotting_options = PlottingOptions(legend=False)
# In that case the legend is not shown.
"""Creation of a MasterPlotter object"""

plot = MasterPlotter(plotting_options, orbit)

# the 2D views are displayes by running the following method
plot.scene_twoD()
# the 3D view is displayed by running the following method
plot.scene_TD()

