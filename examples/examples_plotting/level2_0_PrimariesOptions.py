#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

The aim of this scrip is to show the capabilities of the sempy plotting module:
    PLOTTING OPTIONS : PRIMARIES OPTIONS.
"""
# ___________imports_____________
from src.init.primary import Primary

from src.init.cr3bp import Cr3bp

from src.orbits.halo import Halo

"""Importing the classes needed for plotting..."""
import matplotlib.pyplot as plt

from src.plotting.plotting_module import PlottingOptions, MasterPlotter

# ___________computations____________
CRTBP = Cr3bp(Primary.EARTH, Primary.MOON)

orbit = Halo(CRTBP, CRTBP.l2, Halo.Family.southern, Azdim=12000)
orbit.computation()


# ___________plotting_________________
"""This line turns the interactive mode off. It is necessary to switch off the interactive
mode before using the classes of the sempy plotting module."""
plt.ioff()
"""It closes eventual opened figures."""
plt.close("all")
"""Initialization of the plotting options. """
# It is possible to visualize/hide the primaries within the scene by passing the following keyword arguments:
# disp_first_pr = True/False
# disp_second_pr = True/False
# Any combination of the two is allowed
# By default both primaries are shown.
"""Example"""
plotting_options = PlottingOptions(
    names=[True, "normal"], disp_second_pr=False, sci=True
)
# In that case the first Primary will not be shown.


"""Creation of a MasterPlotter object"""

plot = MasterPlotter(plotting_options, orbit)

# the 2D views are displayes by running the following method
plot.scene_twoD()
# the 3D view is displayed by running the following method
plot.scene_TD()

