#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

The aim of this scrip is to show the capabilities of the sempy plotting module:
    PLOTTING OPTIONS : SCALE FACTOR.
"""
# ___________imports_____________
from src.init.primary import Primary

from src.init.cr3bp import Cr3bp

from src.orbits.halo import Halo

"""Importing the classes needed for plotting..."""
import matplotlib.pyplot as plt

from src.plotting.plotting_module import PlottingOptions, MasterPlotter

# ___________computations____________
CRTBP = Cr3bp(Primary.EARTH, Primary.MOON)

orbit = Halo(CRTBP, CRTBP.l2, Halo.Family.southern, Azdim=12000)
orbit.computation()


# ___________plotting_________________
"""This line turns the interactive mode off. It is necessary to switch off the interactive
mode before using the classes of the sempy plotting module."""
plt.ioff()
"""It closes eventual opened figures."""
plt.close("all")
"""Initialization of the plotting options. """
# Since all the real distances between celestial bodies are kept in the figures, It may happen that
# planets and Libration points are too small to be seen.
# The scale factor parameter is used to solve this problem, and when active all Primaries
# have a radius which is equal to real_radius * scale_factor.
# Note: The scale factor doesn't affect directly the radius of the libration points, but indirectly since
# radius_generic_libration_point = radius_second_primary/2.
# The scale factor Option can be modified by typing the following keword argument:
# scale_factor = [a, b]
# where a is a bool and b is an int.
# a : is a global switch for the scale factor. When False the scale factor is not used at all.
# b : the value by which the primaries radius will be moltiplied.
# This means that when we want to keep the real dimensions of planets the following two formulation are
# equivalent:
# 1) scale_factor = [False, whatever_int]
# 2) scale_factor = [True, 1]
# By default:  scale_factor = [True, 3]

"""Example"""
plotting_options = PlottingOptions(scale_factor=[True, 10])
# In that case the primaries radius will be equal to real_radius * 10

"""Creation of a MasterPlotter object"""

plot = MasterPlotter(plotting_options, orbit)

# the 2D views are displayes by running the following method
plot.scene_twoD()
# the 3D view is displayed by running the following method
plot.scene_TD()

