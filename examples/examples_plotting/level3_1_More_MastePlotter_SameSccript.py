#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

The aim of this scrip is to show the capabilities of the sempy plotting module:
    USING MORE THAN ONE MASTERPLOTTER WITHIN THE SAME SCRIPT.
"""
# ___________imports_____________
from src.init.primary import Primary

from src.init.cr3bp import Cr3bp

from src.orbits.halo import Halo

"""Importing the classes needed for plotting..."""
import matplotlib.pyplot as plt

from src.plotting.plotting_module import PlottingOptions, MasterPlotter

# ___________computations____________
CRTBP = Cr3bp(Primary.EARTH, Primary.MOON)
"""Different Orbits associated to EARTH - MOON CR3BP are computed"""
# 1st
orbit = Halo(CRTBP, CRTBP.l2, Halo.Family.southern, Azdim=12000)
orbit.computation()
# 2nd (Same libration point, different Azdim)
orbit2 = Halo(CRTBP, CRTBP.l2, Halo.Family.southern, Azdim=18000)
orbit2.computation()
# 3rd (Different libration point, Same Azdim)
orbit3 = Halo(CRTBP, CRTBP.l1, Halo.Family.southern, Azdim=12000)
orbit3.computation()


# ___________plotting_________________
"""This line turns the interactive mode off. It is necessary to switch off the interactive
mode before using the classes of the sempy plotting module."""
plt.ioff()
"""It closes eventual opened figures."""
plt.close("all")
"""Initialization of the plotting options. """
plotting_options = PlottingOptions()
# a plotting option object has been generated keeping the default options.
plotting_options1 = PlottingOptions(XY=False, disp_second_pr=False)
# a different plotting object has been generated.
"""Creation of different MasterPlotter objects"""
# Within a script it is possible to call different MasterPlotter objects, each object will be completely
# indipendent from the others. This means that each object can produce its own figures accordign to the plotting
# options it is referring to.
plot = MasterPlotter(plotting_options, orbit, orbit2=orbit2, orbit3=orbit3)
plot.scene_twoD()
plot.scene_TD()
# the first MasterPlotter object produces its own figures referring to the object plotting_options.
plot1 = MasterPlotter(plotting_options1, orbit)  # only one orbit will be input in plot1
plot1.scene_twoD()
plot1.scene_TD()
# the second MasterPlotter object produces its own figures referring to the object plotting_options1.

# NOTE : The figures produced by each MasterPlotter will be labelled according to increasing numeration.
# Taking this scrit as example the first Masterplotter will produce 4 figures(default options are kept)
# that will be labelled as :
# Master Figure 0 - XY
# Master Figure 0 - XZ
# Master Figure 0 - YZ
# Master Figure 0 - 3D Visualization
# The second will produce only 3 figures (each of which without the first Primary)that will be labelled as:
# Master Figure 1 - XZ
# Master Figure 1 - YZ
# Master Figure 1 - 3D Visualization
# and so on for further MasterPlotter in the script.

