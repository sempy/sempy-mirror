#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

The aim of this scrip is to show the capabilities of the sempy plotting module:
    PLOTTING OPTIONS : NAMES.
"""
# ___________imports_____________
from src.init.primary import Primary

from src.init.cr3bp import Cr3bp

from src.orbits.halo import Halo

"""Importing the classes needed for plotting..."""
import matplotlib.pyplot as plt

from src.plotting.plotting_module import PlottingOptions, MasterPlotter

# ___________computations____________
CRTBP = Cr3bp(Primary.EARTH, Primary.MOON)

orbit = Halo(CRTBP, CRTBP.l2, Halo.Family.southern, Azdim=12000)
orbit.computation()


# ___________plotting_________________
"""This line turns the interactive mode off. It is necessary to switch off the interactive
mode before using the classes of the sempy plotting module."""
plt.ioff()
"""It closes eventual opened figures."""
plt.close("all")
"""Initialization of the plotting options. """
# It is possible to show within the scene also Primary names and libration point names by
# typing the following keword argument:
# names = True/False
# by default Primaries names and Libration point names are not displayed.

"""Example"""
plotting_options = PlottingOptions(names=[True, "fancy"])
# In that case names of primaries and libration points are displayed.

# IMPORTANT NOTE: when names = [True, 'fancy'/'normal'] all the names (of both Primaries and Libration points) are displayed
# at the same time.
# WHAT IF WE ONLY WANT TO SHOW PRIMARIES S NAMES (LIBRATION POINTS S NAMES)???
# Their corresponding plotting options have to be switched off.
# EXAMPLES:
# - plotting_options = PlottingOptions(disp_first_pr = False, disp_second_pr = False, names = [True, 'normal'])
# will display only the names of the libration points l1 and l2 (Remember that l3 l4, l5 are not shown
# by default). For further information about that see script level2_1_LibPointsOptions.py
# - plotting_options = PlottingOptions(l1 = False, l2 = False, names = [True,'normal'])
# will display only the names of the Primaries.

"""Creation of a MasterPlotter object"""

plot = MasterPlotter(plotting_options, orbit)

# the 2D views are displayes by running the following method
plot.scene_twoD()
# the 3D view is displayed by running the following method
plot.scene_TD()

