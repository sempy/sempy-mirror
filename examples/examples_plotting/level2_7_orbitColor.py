#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# ___________imports_____________
from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.orbits.halo import Halo
import matplotlib.pyplot as plt
from src.plotting.plotting_module import PlottingOptions, MasterPlotter

# _____________computation____________
CRTBP = Cr3bp(Primary.EARTH, Primary.MOON)

orbit = Halo(CRTBP, CRTBP.l1, Halo.Family.southern, Azdim=12000)
orbit.computation()

orbit1 = Halo(CRTBP, CRTBP.l1, Halo.Family.southern, Azdim=18000)
orbit1.computation()

orbit2 = Halo(CRTBP, CRTBP.l1, Halo.Family.southern, Azdim=20000)
orbit2.computation()

# ___________plotting_________________

plt.ioff()
plt.close("all")
plotting_options = PlottingOptions(
    l2=False, disp_first_pr=False, sci=True, orbit_color="b", legend=False
)
plot = MasterPlotter(plotting_options, orbit, orbit1=orbit1, orbit2=orbit2)
plot.show_all()
