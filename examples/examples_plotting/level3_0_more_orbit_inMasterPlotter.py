#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

The aim of this scrip is to show the capabilities of the sempy plotting module:
    PLOTTING MORE THAN ONE ORBIT(MANIFOLDS) USING THE MASTERPLOTTER CLASS.
"""
# ___________imports_____________
from src.init.primary import Primary

from src.init.cr3bp import Cr3bp

from src.orbits.halo import Halo

"""Importing the classes needed for plotting..."""
import matplotlib.pyplot as plt

from src.plotting.plotting_module import PlottingOptions, MasterPlotter

# ___________computations____________
CRTBP = Cr3bp(Primary.EARTH, Primary.MOON)
"""Different Orbits associated to EARTH - MOON CR3BP are computed"""
# 1st
orbit = Halo(CRTBP, CRTBP.l2, Halo.Family.southern, Azdim=12000)
orbit.computation()
# 2nd (Same libration point, different Azdim)
orbit2 = Halo(CRTBP, CRTBP.l2, Halo.Family.southern, Azdim=18000)
orbit2.computation()
# 3rd (Different libration point, Same Azdim)
orbit3 = Halo(CRTBP, CRTBP.l1, Halo.Family.southern, Azdim=12000)
orbit3.computation()


# ___________plotting_________________
"""This line turns the interactive mode off. It is necessary to switch off the interactive
mode before using the classes of the sempy plotting module."""
plt.ioff()
"""It closes eventual opened figures."""
plt.close("all")
"""Initialization of the plotting options. """
plotting_options = PlottingOptions()
# a plotting option object has been generated, if nothing is passed. The defaut parameters are kept.
"""Creation of a MasterPlotter object"""
# An arbitrary number of Orbits can be passed to Masterplotter as following:
plot = MasterPlotter(plotting_options, orbit, orbit2=orbit2, orbit3=orbit3)
"""The first two input parameters (in that case plotting_options and orbit) are MANDATORY, because if MasterPlotter
is called means that at least one Orbit has to be plotted.
The other optional orbits are passed with **kwargs.
It is not important how they are labelled, the important thing is that all the input orbits to MasterPlotter
belong to the same CR3BP.
Another correct call of MasterPlotter could have been :
plot = MasterPlotter(plotting_options, orbit, whatever = orbit2, whatever2 = orbit3)"""

# Obviously when legend = True, and when more than one orbit is passed to MasterPlotter the legend automatically updates.
# the 2D views are displayes by running the following method
plot.scene_twoD()
# the 3D view is displayed by running the following method
plot.scene_TD()

