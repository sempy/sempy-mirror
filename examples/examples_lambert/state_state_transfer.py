"""
State to state direct transfer in the CR3BP model.
==================================================

This example demonstrates the use of the CR3BP Lambert solver to compute a direct transfer
between two specified states in the Earth-Moon CR3BP model.

"""

# %%
# Import statements
#
# As usual, we will start importing all required modules and classes to generate the selected
# departure and arrival orbits and compute the Lambert arc.

import numpy as np
import matplotlib.pyplot as plt

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.init.constants import DAYS2SEC
from src.orbits.halo import Halo
from src.orbits.nrho import NRHO
from src.lambert.cr3bp_lambert_pbm import Cr3bpLambertPbm
from src.plotting.simple.utils import decorate_3d_axes

# %%
# Dynamical model
#
# At this point, we define the Earth-Moon CR3BP system as the dynamical model in which the
# transfer is computed.

cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
l_c, t_c = cr3bp.L, cr3bp.T / 2.0 / np.pi  # CR3BP characteristic length [km] and time [s]

# %%
# Departure and arrival states
#
# The departure and arrival states are taken from an L1 southern Halo with vertical extension
# ``Azdim=50000`` km and an L2 southern NRHO with ``Azdim=70000`` km respectively.
# The two orbits are firstly initialized, interpolated and corrected as follows:

halo_dep = Halo(cr3bp, cr3bp.l1, Halo.Family.southern, Azdim=50000)
nrho_arr = NRHO(cr3bp, cr3bp.l2, NRHO.Family.southern, Azdim=70000)

halo_dep.interpolation()
nrho_arr.interpolation()

# %%
# The departure and arrival states are then selected at the aposelene of the two defined orbits:

state1 = halo_dep.state0
state2 = nrho_arr.state_vec[nrho_arr.t_vec.size // 2, 0:6]

# %%
# Lambert problem object
#
# Once the endpoint states are defined, a `Cr3bpLambertPbm` object is instantiated specifying the
# desired force model in which the transfer is computed.
#
# Through its methods, this object allows the computation of multiple transfer trajectories
# with different endpoint states and transfer times.

lamb_pbm = Cr3bpLambertPbm(cr3bp, cr3bp.m2)

# %%
# Lambert problem solution
#
# In order to solve for a given transfer arc, the departure position, the arrival position and the
# time of flight must be chosen and passed as input parameters to the `solve` method of the above
# defined object. Initial and final positions are given by ``state1`` and ``state2`` defined above
# while a ``5`` days time of flight is chosen in this example.
#
# In addition to the above, other three important parameters must be specified: the number of
# complete revolutions about the main attractor, the number of patch point on which the
# differential correction procedure is performed and the type of initial guess. All of them are
# optional with their default values described in the method's docstring.
# Regarding the initial guess, two approximation are available: a solution to the Lambert Problem
# computed in the Restricted Two-Body Problem (R2BP) neglecting the influence of one of the two
# primaries or a series of patch points drawn from a number of CR3BP orbits belonging to the same
# family of the departure and arrival ones. A choice between the two is made with the input
# parameter `guess` which will assume one of the two allowed values ``r2bp`` or ``stack``
# respectively. In this example, an initial guess computed with the R2BP approximation will be
# used to generate the required patch points for the subsequent correction procedure.

sol = lamb_pbm.solve(tof=5 * DAYS2SEC / t_c, nb_revs=1, nb_pts=2, guess='r2bp',
                     state1=state1, state2=state2)

# %%
# After computing the specified initial guess and performing a differential correction of the
# approximated patch points, the following output values are returned by the aforementioned `solve`
# method: time of flight, corrected time and states at patch points, first and second maneuvers
# (dV vectors), maneuvers magnitude (first, second and total dV), departure and arrival states
# ``state1`` and ``state2``. More information on both input and output parameters might be found
# in the method's docstring.
#
# The impulsive dVs required to perform such transfer might be then retrieved from the fifth
# element of the ``sol`` tuple above.

dv_mag = sol[5] * l_c / t_c * 1e3  # dV1, dV2 and total dV magnitude [m/s]

print('\nImpulsive maneuvers:\n')
for i, m in enumerate(('dV1', 'dV2', 'dVt')):
    print(f"{m:5s}: {dv_mag[i]} [m/s]")

# %%
# Patch point propagation
#
# Once the Lambert Problem has been solved, a continuous transfer trajectory is obtained
# propagating the corrected patch points returned by the aforementioned `solve` method.
# The propagation is accomplished with the `Cr3bpLambertPbm` method `propagate` which ensure the
# same dynamical model and scaling parameters are used for the explicit integration of the
# corrected transfer arc.

t_vec, state_vec = lamb_pbm.propagate(sol[1], sol[2])

# %%
# Three-dimensional trajectory plot
#
# Finally, the departure orbit, the arrival orbit and the corresponding transfer trajectory are
# plotted on a three-dimensional figure for a visual inspection of the computed results.

fig = plt.figure()
ax = fig.add_subplot(projection='3d')
ax.plot(halo_dep.state_vec[:, 0], halo_dep.state_vec[:, 1], halo_dep.state_vec[:, 2],
        label='Departure L1 Halo')
ax.plot(nrho_arr.state_vec[:, 0], nrho_arr.state_vec[:, 1], nrho_arr.state_vec[:, 2],
        label='Arrival L2 NRHO')
ax.plot(state_vec[:, 0], state_vec[:, 1], state_vec[:, 2], label='Transfer arc')
ax.quiver(state_vec[0, 0], state_vec[0, 1], state_vec[0, 2],
          state_vec[0, 3], state_vec[0, 4], state_vec[0, 5], color='k', length=0.1)
ax.quiver(state_vec[-1, 0], state_vec[-1, 1], state_vec[-1, 2],
          state_vec[-1, 3], state_vec[-1, 4], state_vec[-1, 5], color='k', length=0.1)
decorate_3d_axes(ax, 'Direct transfer between L1/L2 southern Halo orbits', '-')
ax.view_init(azim=32.0, elev=22.0)
plt.show()
