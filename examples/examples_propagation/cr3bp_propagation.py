import numpy as np
import matplotlib.pyplot as plt
import time

from src.init.primary import Primary
from src.dynamics.cr3bp_environment import CR3BP

from src.propagation.propagator import Propagator, Events, PathParameters

# Create a CR3BP environment
crt = CR3BP(Primary.EARTH, Primary.MOON)

# Create initial states
x0 = np.array([1.129043956269,0 ,0.177774841545, 0,-0.225515065014,0])
x0_stm = np.concatenate((x0, np.eye(6).flatten()))

# Create propagator object
prop = Propagator()

# Propagate
tspan = [0, 10]

p1_impact = Events.collision("m1_imp", crt.m1_pos, crt.m1.Req/crt.adim_l)
p2_impact = Events.collision("m2_imp", crt.m2_pos, crt.m2.Req/crt.adim_l)

t = time.time()
sol = prop(crt, tspan, x0, n_eval=1000, events=[p1_impact, p2_impact])
t1 = time.time()
sol_stm = prop(crt, tspan, x0_stm, n_eval=1000, events=[p1_impact, p2_impact], with_stm=True)
t2 = time.time()

print('Propagation time', t1 - t)
print('Propagation time STM', t2 - t1)

# Plot
fig, ax = plt.subplots()
ax.plot(sol.state_vec[:, 0], sol.state_vec[:, 1])
plt.show()

state_end = sol.state_vec[-1]
state_end_dim = crt.dimensionalize_state(state_end)

# Propagation with additional path parameters
arclength = PathParameters.arclength
momentum_int = PathParameters.momentum_integral

# Create initial arrays, with correct number of additional path parameters
path_params = [{'function': arclength, 'initial_value': 0},
               {'function': momentum_int}]

sol_add = prop(crt, tspan, x0, n_eval=1000, events=[p1_impact, p2_impact],
                   path_param=path_params)
sol_add_stm = prop(crt, tspan, x0_stm, n_eval=1000, events=[p1_impact, p2_impact],
                       path_param=path_params, with_stm=True)

# Plot
f, axs = plt.subplots(2, 1)
axs[0].plot(sol_add.t_vec, sol_add.path_param_vec[:, 0])
axs[0].set_title('Arc length')
axs[1].plot(sol_add.t_vec, sol_add.path_param_vec[:, 1])
axs[1].set_title('Momentum integral')
plt.tight_layout()
plt.show()
