import numpy as np
import matplotlib.pyplot as plt
import time

from src.init.primary import Primary
from src.dynamics.r2bp_environment import R2BP

from src.propagation.propagator import Propagator, Events, PathParameters

# Create a CR3BP environment
r2bp = R2BP(Primary.EARTH)

# Create initial states
x0 = np.array([8000., 0, 0, 0, 7.5, 0.])
x0_stm = np.concatenate((x0, np.eye(6).flatten()))

# Create propagator object
prop = Propagator()

# Propagate
tspan = [0, 86400]

p1_impact = Events.collision("p1_imp", [0, 0, 0], r2bp.bodies[0].Req)

t = time.time()
sol = prop(r2bp, tspan, x0, n_eval=1000, events=[p1_impact])
t1 = time.time()
sol_stm = prop(r2bp, tspan, x0_stm, n_eval=1000, events=[p1_impact], with_stm=True)
t2 = time.time()

print('Propagation time', t1 - t)
print('Propagation time STM', t2 - t1)

# Plot
fig, ax = plt.subplots()
ax.plot(sol.state_vec[:, 0], sol.state_vec[:, 1])
plt.show()

# Propagation with additional path parameters
arclength = PathParameters.arclength
momentum_int = PathParameters.momentum_integral

# Create initial arrays, with correct number of additional path parameters
path_params = [{'function': arclength, 'initial_value': 0},
               {'function': momentum_int}]

sol_add = prop(r2bp, tspan, x0, n_eval=1000, events=[p1_impact],
                   path_param=path_params)
sol_add_stm = prop(r2bp, tspan, x0_stm, n_eval=1000, events=[p1_impact],
                       path_param=path_params, with_stm=True)

# Plot
f, axs = plt.subplots(2, 1)
axs[0].plot(sol_add.t_vec, sol_add.path_param_vec[:, 0])
axs[0].set_title('Arc length')
axs[1].plot(sol_add.t_vec, sol_add.path_param_vec[:, 1])
axs[1].set_title('Momentum integral')
plt.tight_layout()
plt.show()

#
