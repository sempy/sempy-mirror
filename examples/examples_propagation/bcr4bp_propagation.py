import numpy as np
import matplotlib.pyplot as plt
import time

from src.init.primary import Primary
from src.dynamics.bcr4bp_environment import BCR4BP

from src.propagation.propagator import Propagator, Events, PathParameters

# Create a CR3BP environment
bcr = BCR4BP(Primary.EARTH, Primary.MOON, start_sun_phase=0.1)

# Create initial states
x0 = np.array([0.98708576, 0., 0.01926937, 0., 1.08755146, 0.])
x0_stm = np.concatenate((x0, np.eye(6).flatten()))

# Create propagator object
prop = Propagator(method='RK45')

# Propagate
tspan = [0, 9.054899251910799]

p1_impact = Events.collision("m1_imp", bcr.m1_pos, bcr.m1.Req/bcr.adim_l)
p2_impact = Events.collision("m2_imp", bcr.m2_pos, bcr.m2.Req/bcr.adim_l)

t = time.time()
sol = prop(bcr, tspan, x0, n_eval=1000, events=[p1_impact, p2_impact])
t1 = time.time()
sol_stm = prop(bcr, tspan, x0_stm, n_eval=1000, events=[p1_impact, p2_impact], with_stm=True)
t2 = time.time()

print('Propagation time', t1 - t)
print('Propagation time STM', t2 - t1)

# Propagation with additional path parameters
arclength = PathParameters.arclength
momentum_int = PathParameters.momentum_integral

# Create initial arrays, with correct number of additional path parameters
path_params = [{'function': arclength, 'initial_value': 0},
               {'function': momentum_int}]
t3 = time.time()
sol_add = prop(bcr, tspan, x0, n_eval=1000, events=[p1_impact, p2_impact],
                   path_param=path_params)
t4 = time.time()
sol_add_stm = prop(bcr, tspan, x0_stm, n_eval=1000, events=[p1_impact, p2_impact],
                       path_param=path_params, with_stm=True)
t5 = time.time()

print('Propagation time', t4 - t3)
print('Propagation time STM', t5 - t4)

# %%
# Plot in 3D
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot(sol.state_vec[:, 0], sol.state_vec[:, 1], sol.state_vec[:, 2])
ax.plot(sol_stm.state_vec[:, 0], sol_stm.state_vec[:, 1], sol_stm.state_vec[:, 2])
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')

state_end = sol.state_vec[-1]
state_end_dim = bcr.dimensionalize_state(state_end)

# Plot
f, axs = plt.subplots(2, 1)
axs[0].plot(sol_add.t_vec, sol_add.path_param_vec[:, 0])
axs[0].set_title('Arc length')
axs[1].plot(sol_add.t_vec, sol_add.path_param_vec[:, 1])
axs[1].set_title('Momentum integral')
plt.tight_layout()
plt.show()
