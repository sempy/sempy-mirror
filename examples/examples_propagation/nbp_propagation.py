import numpy as np
import matplotlib.pyplot as plt
import spiceypy as spice
from time import time
from linetimer import CodeTimer

from src.init.primary import Primary
from src.dynamics.nbp_environment import NBP
from src.propagation.propagator import Propagator, Events, PathParameters
from src.init.constants import MOON_SMA, MOON_OMEGA_MEAN

from src.init.load_kernels import load_kernels

# Load kernel
load_kernels()

# %% Create the NBP environments
# First, two different NBP environments are created, one with the Earth-Moon system and one with the
# Earth-Moon-Sun system. Both system are centred in the Earth. The lists of Primary instances can be
# created in any order. They will be first ordered by mass, and then the central body will be
# automatically set as the first element of the list when the NBP environment is created.
center = 'MOON'
start_date = '2020 JUN 25 23:31:36.26752 UTC'
nbp_1 = NBP([Primary.EARTH, Primary.MOON, Primary.SUN], start_date=start_date)
nbp_2 = NBP([Primary.EARTH, Primary.MOON], start_date=start_date)

# Another possibility is to create an adimensionalized NBP environment. This can speed up the
# integration process, provided that the constants are chosen accordingly. In this case, the
# chosen constants are analoguous to the ones used in the CR3BP environment.
adim_t = 1/MOON_OMEGA_MEAN
adim_l = MOON_SMA
nbp_1_adim = NBP([Primary.EARTH, Primary.MOON, Primary.SUN],
                 adim_t=adim_t, adim_l=adim_l, start_date=start_date)

# %% Create initial states
# The initial propagation states have to be created according to two main factors:
#   - The optional integration of the STM variational equations
#   - The optional integration of additional path parameters
# Moreover, in the case of the adimensional NBP, the initial state has to be adimensionalized. There
# is a convenient method in all environment classes to do this, but it is currently limited to the
# 6-dimensional state. The adimensionalization of the STM and additional path parameters is not
# implemented yet.

x0_adim = np.array([-0.86483108,  0.35797308,  0.26037714, -0.99874246, -1.85421247,
       -0.73940532])

x0 = nbp_1_adim.dimensionalize_state(x0_adim)
x0_stm = np.concatenate((x0, np.eye(6).flatten()))
x0_adim_stm = np.concatenate((x0_adim, np.eye(6).flatten()))
x0_stm_ep = np.concatenate((x0_stm, np.zeros(6)))
x0_adim_stm_ep = np.concatenate((x0_adim_stm, np.zeros(6)))

# %% Create propagators
# The propagator objects are generic wrappers of the scipy.integrate.solve_ivp function. They can
# be used to propagate the state of the system, and optionally the STM and additional path
# parameters. Propagators are created and associated to a specific environment. This automatically
# creates the eom and variational equations methods of the propagator object. These are used to pass
# to the solve_ivp function.
prop = Propagator()

# Define time span
n_days = 5
tspan = np.linspace(0, n_days*86400, 1000)
tspan_adim = tspan/nbp_1_adim.adim_t

# Define propagation events
# The dataclass Events contains a list of convenient propagation events. They can be used to stop
# (if terminal=True) or to sample (if terminal=False) the propagation at specific events.
center_impact = Events.collision("center_impact", rad=nbp_1.bodies[0].Req)
center_impact_adim = Events.collision("center_impact_adim", rad=nbp_1_adim.bodies[0].Req/nbp_1_adim.adim_l)

# %% Propagate the state
# The propagation of the state is straightforward. The propagation of the STM and additional path
# parameters is also straightforward, but the initial state has to be extended with the initial STM
# and additional path parameters. The stm_flag argument is used to enable the integration of the
# STM, and the path_fun argument is used to pass the function (or list of functions) that compute
# the additional path parameters. The path_fun argument can be a list of functions, in which case
# the additional path parameters are stacked in the same order as the functions are passed. In this
# example, each propagation is timed. Propagations return Solution objects, similar to the
# scipy.integrate.OdeSolution class.

path_param = {'function': PathParameters.momentum_integral, 'initial_value': 0}

with CodeTimer("State 1 dim"):
    sol1_dim = prop(nbp_1, tspan, x0, events=[center_impact])
with CodeTimer("State 1 dim with STM"):
    sol1_dim_stm = prop(nbp_1, tspan, x0_stm, events=[center_impact], with_stm=True)
with CodeTimer("State 1 dim with STM and epoch partials"):
    sol1_dim_stm_epoch_part = prop(nbp_1, tspan, x0_stm_ep, events=[center_impact],
                                   with_stm=True, with_epoch_partials=True)
with CodeTimer("State 1 dim with path param"):
    sol1_dim_param = prop(nbp_1, tspan, x0, events=[center_impact],
                         path_param=path_param)
with CodeTimer("State 1 dim with STM and path param"):
    sol1_dim_param_stm = prop(nbp_1, tspan, x0_stm, events=[center_impact],
                            path_param=path_param, with_stm=True)
with CodeTimer("State 1 dim with STM, epoch partials and path param"):
    sol1_dim_stm_epoch_part_param = prop(nbp_1, tspan, x0_stm_ep, events=[center_impact],
                                            path_param=path_param, with_stm=True,
                                            with_epoch_partials=True)

with CodeTimer("State 1 adim"):
    sol1_adim = prop(nbp_1_adim, tspan_adim, x0_adim, events=[center_impact_adim])
with CodeTimer("State 1 adim with STM"):
    sol_1_adim_stm = prop(nbp_1_adim, tspan_adim, x0_adim_stm, events=[center_impact_adim],
                      with_stm=True)
with CodeTimer("State 1 adim with STM and epoch partials"):
    sol_1_adim_stm_epoch_part = prop(nbp_1_adim, tspan_adim, x0_adim_stm_ep,
                                     events=[center_impact_adim],
                                     with_stm=True, with_epoch_partials=True)
with CodeTimer("State 1 adim with path param"):
    sol_1_adim_param = prop(nbp_1_adim, tspan_adim, x0_adim, events=[center_impact_adim],
                        path_param=path_param)
with CodeTimer("State 1 adim with STM and path param"):
    sol_1_adim_param_stm = prop(nbp_1_adim, tspan_adim, x0_adim_stm, events=[center_impact_adim],
                            path_param=path_param, with_stm=True)
with CodeTimer("State 1 adim with STM, epoch partials and path param"):
    sol_1_adim_stm_epoch_part_param = prop(nbp_1_adim, tspan_adim, x0_adim_stm_ep, events=[center_impact_adim],
                                            path_param=path_param, with_stm=True,
                                            with_epoch_partials=True)

with CodeTimer("State 2 dim"):
    sol2_dim = prop(nbp_2, tspan, x0, events=[center_impact])

# %%  Plot results
# # The propagation results are stored in the Solution object.
plt.figure()
plt.plot(sol1_dim.state_vec[:, 0], sol1_dim.state_vec[:, 1], label='SEM')
plt.plot(sol2_dim.state_vec[:, 0], sol2_dim.state_vec[:, 1], label='EM')
plt.scatter(0, 0, label=nbp_1.bodies[0].name, color='k')
plt.legend()
plt.axis('equal')
plt.show()


# %% Unload kernels
spice.kclear()
