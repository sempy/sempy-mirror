# %% Import modules
from typing import Callable, List
import numpy as np

from src.init.primary import Primary
from src.dynamics.cr3bp_environment import CR3BP
from src.solve.initializer import Initializer

from src.propagation.propagator import Propagator

from src.solve.constraints_libraries.constraints_lib import PeriodicLib, PoincareLib, QuasiPeriodicLib
from src.solve.constraints import Constraints

from src.solve.correct.corrector import Corrector

from src.solve.generate_orbits.orbit_builder import BuildOrbit

from src.solve.predictor import PolynomialPredictor, Predictor
from src.solve.generate_orbits.continuation_lib import StopCriteriaList, ContinuationCriterion, InterpFunctions

import matplotlib.pyplot as plt
import plotly.graph_objects as go
import plotly.colors as co




# %% Initial setup of the problem

# Create environment
# env = CR3BP(Primary.EARTH, Primary.MOON,
#             CR3BP.ODEList.state,
#             CR3BP.FrameList.SYNODIC)

env = CR3BP(Primary.EARTH, Primary.MOON)



# Create Propagator
prop = Propagator()


# %% Generate Initial guess

# Extract lagrangian point
lag_pt = env.libration_points.L2

# Compute set of initial guessed
initial_guess_list = Initializer.lagrange_point_orbit(lag_pt, env.get_f_matrix, delta=-1e-4, direction='planar')
initial_guess = initial_guess_list[0]

# x0 = [1.02202420891772, 0, -0.182098030216387, 0, -0.103261833343593, 0]
# tau0 = 1.511146383966198
# initial_guess_list = Initializer.quasi_periodic_orbit(np.append(x0,tau0), prop, 15, delta=1e-4)
# initial_guess = initial_guess_list[0]

# Setup correction constraints
constr_1 = PeriodicLib.state_match(env, prop)
# constr_1 = QuasiPeriodicLib.strobo_map_match(initial_guess[0,:], env, prop)
constr_2 = PoincareLib.fix_elements([1], [initial_guess[0,1]])

corr_constr = [constr_1, constr_2]

# Setup continuation constraint
# cont_constr = ContinuationCriterion.fix_element(idx_fix=[0])
cont_constr = ContinuationCriterion.fix_distance([0, 1, 2])

# Setup stop conditions
max_count = 50
prim_pos = np.array([[-env.mu, 0, 0], [1-env.mu, 0, 0]])
min_dist_thresh = 1e-3
max_dist_thresh = 2

# stop_criteria_list = [StopCriteriaList.max_orbits_count(max_count),
#                       StopCriteriaList.min_primaries_distance(prop, prim_pos, dist_thresh)]

stop_criteria_list = [StopCriteriaList.max_orbits_count(max_count),
                      StopCriteriaList.min_reference_distance(prop, env, prim_pos[0], min_dist_thresh),
                      StopCriteriaList.min_reference_distance(prop, env, prim_pos[1], min_dist_thresh),
                      StopCriteriaList.max_reference_distance(prop, env, np.array([0, 0, 0]), max_dist_thresh)]

# Setup continuator
# cont = PolynomialPredictor(pred_tol=1e-6, max_degree=20)
cont = Predictor.polynomial(pred_tol=1e-4, max_degree=4)

# Build Family Generator
orb_gen = BuildOrbit(initial_guess, corr_constr, cont_constr, stop_criteria_list, cont, env, prop)

orb_gen.generate_family() # TODO: this can be also automatized in the initialization

# orb_gen.detect_bifurcations()

family = orb_gen.export_family(InterpFunctions.element(-1), [np.mean(orb_gen.states_stack[:,-1])])


# Interpolate family
# orb_gen.interpolate_family(InterpFunctions.distance(env.lagrange_points.L2), np.arange(0,1e-1,1e-2))
# orb_gen.states_stack = orb_gen.interp_states


# %% PLOT RESULTS

# %% MatplotLib Plots
fig = plt.figure()
ax_xy = fig.add_subplot(1,3,1)
ax_xz = fig.add_subplot(1,3,2)
ax_yz = fig.add_subplot(1,3,3)

ax_xy.grid()
ax_xz.grid()
ax_yz.grid()

ax_xy.axis('equal')
ax_xz.axis('equal')
ax_yz.axis('equal')

# plot earth
ax_xy.plot(-env.mu, 0, marker ='o', color='blue')
ax_xz.plot(-env.mu, 0, marker ='o', color='blue')
ax_yz.plot(0, 0, marker ='o', color='blue')

# plot moon
ax_xy.plot(1-env.mu, 0, marker ='o', color='gray')
ax_xz.plot(1-env.mu, 0, marker ='o', color='gray')
ax_yz.plot(0, 0, marker ='o', color='gray')

# Plot lagrangian point
ax_xy.plot(lag_pt[0], lag_pt[1], marker ='+', color='red')
ax_xz.plot(lag_pt[0], lag_pt[2], marker ='+', color='red')
ax_yz.plot(lag_pt[1], lag_pt[2], marker ='+', color='red')


for i in range(orb_gen.states_stack.shape[0]):
    
    s0 = orb_gen.states_stack[i,:]

    t_vec = np.linspace(0, s0[-1],101)
    x = prop(env, t_vec, s0[:6]).state_vec

    
    ax_xy.plot(x[:,0], x[:,1], color='black')
    ax_xz.plot(x[:,0], x[:,2], color='black')
    ax_yz.plot(x[:,1], x[:,2], color='black')



# # test bifurcations
# orb_gen.detect_bifurcations()

# for i in range(orb_gen.bi_state.shape[0]):
    
#     s0 = orb_gen.bi_state[i,:]

#     t_vec = np.linspace(0, s0[-1],100)
#     x = prop(t_vec, s0[:6]).state_vec

    
#     ax_xy.plot(x[:,0], x[:,1], color='red')
#     ax_xz.plot(x[:,0], x[:,2], color='red')
#     ax_yz.plot(x[:,1], x[:,2], color='red')



plt.show(block=True)


# %% Plotly Plots
fig = go.FigureWidget()

fig.add_scatter3d(x = [1-env.mu], y=[0], z=[0], mode='markers', marker={'color': 'gray', 'size': 10}, showlegend=False)

color_scale = co.get_colorscale("Plasma")
sampled_color_scale = co.sample_colorscale("Plasma",orb_gen.states_stack.shape[0])
# color_array = co.colorscale_to_colors(color_scale,)
for i in range(orb_gen.states_stack.shape[0]):
    s0 = orb_gen.states_stack[i,:]

    t_vec = np.linspace(0, s0[-1],101)
    x = prop(env, t_vec, s0[:6]).state_vec

    fig.add_scatter3d(x = x[:,0], y = x[:,1], z = x[:,2], mode='lines', line = {'width': 4, 'color': sampled_color_scale[i]},showlegend=False)

    # fig.add_scatter3d(x = x[:,0], y = x[:,1], z = x[:,2], mode='lines',showlegend=False)

    # fig.update_traces(line_colorscale='Electric', line_autocolorscale=False, line_cmin=0,line_cmax=1, line_color = list(range(orb_gen.states_stack.shape[0])))

fig.update_scenes(aspectmode = 'cube', #, aspectratio={'x' : 1,'y' : 1,'z' : 1})
                camera = {'projection' : {'type':'orthographic'}})
fig.show()