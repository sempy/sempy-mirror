"""
Computation of an entire family of Earth-Moon Halo orbits via continuation methods.

"""

import sys
sys.path.append('./')
sys.path.append('../')
sys.path.append('../../')


import numpy as np
import matplotlib.pyplot as plt

from time import perf_counter
from tqdm import tqdm, TqdmWarning
from copy import deepcopy
from warnings import simplefilter

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.orbits.halo import Halo
from src.solve.diff_corr_3d import DiffCorr3D
from src.plotting.util import set_axes_equal
from src.init.constants import DAYS2SEC

# save or not the results
save_abacus = False

# define environment (EM system)
cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

# init Halo by vertical extension
halo = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Azdim=10)

t0 = perf_counter()
# first differential correction with (x0, vy0) as free variables
halo.computation(fix_dim=Halo.DiffCorrFixDim.z0)

# second differential correction with (x0, z0, vy0, T12) as free variables
halo.computation(fix_dim=Halo.DiffCorrFixDim.min_norm)

# init continuation process
diff_corr = DiffCorr3D(cr3bp.mu, max_internal_steps=4_000_000)  # differential corrector instance
delta_s = 0.001  # pseudo-arclength step size
orbits = [deepcopy(halo)]  # list to be filled with all computed orbits

# format progress bar
simplefilter('ignore', TqdmWarning)
desc = halo.get_abacus_name(separator=' ')
pbar = tqdm(total=np.ceil(halo.m2_apsis['periapsis_altitude_dim']), desc=desc)

# continue the family until it crosses the Moon surface or the pseudo-arclength step size is zero
while delta_s > 1e-6 and halo.m2_apsis['periapsis_altitude'] > 0.0:

    # perturbation of the initial state in the direction of the null vector
    for j in range(3):
        halo.state0[2 * j] += delta_s * halo.continuation['null_vec'][j, 0]
    halo.T12 += delta_s * halo.continuation['null_vec'][3, 0]

    # differential correction with pseudo-arclength constraint
    halo.state0, halo.T12, halo.T, halo.continuation['g_vec'], halo.continuation['null_vec'] = \
        diff_corr.diff_corr_3d_cont(halo.state0, halo.T12, delta_s, halo.continuation['g_vec'],
                                    halo.continuation['null_vec'])

    # postprocessing (vertical extension, Jacobi constant, periapsis, apoapsis, state vector)
    halo.postprocess()

    # check on apoapsis radius, keep solution only if smaller than Earth-Moon distance
    if halo.m2_apsis['apoapsis_radius'] < 1.0:
        orbits.append(deepcopy(halo))
        pbar.update(np.ceil(orbits[-2].m2_apsis['periapsis_altitude_dim']) -
                    np.ceil(halo.m2_apsis['periapsis_altitude_dim']))
    else:
        halo = orbits[-1]  # discard the last solution, restart from the last good one
        delta_s *= 0.1  # reduce step size by a factor of 10
pbar.close()
tf = perf_counter()
print(f"Elapsed time for abacus generation: {(tf - t0)} s")

# %% extract data to be stored in the abacus
state0_vec = [o.state0 for o in orbits]  # initial state
az_vec = [o.Az for o in orbits]  # vertical extension
rp_vec = [o.m2_apsis['periapsis_radius'] for o in orbits]  # periselene radius
cjac_vec = [o.C for o in orbits]  # Jacobi constant
period_vec = [o.T for o in orbits]  # orbit period
stability_idx_vec = [o.stability_idx for o in orbits]  # stability indexes (not stored)
if save_abacus:
    orbit = Halo(cr3bp, halo.li, halo.family, Azdim=az_vec[0])
    orbit.save_abacus(state0_aba=state0_vec, az_aba=az_vec, rp_aba=rp_vec, cjac_aba=cjac_vec,
                      period_aba=period_vec)

# plot the family highlighting the NROs
fig = plt.figure()
fig.suptitle(desc + ' family')
axs = plt.axes(projection='3d')
for i in np.arange(0, len(orbits), 25):
    axs.plot3D(orbits[i].state_vec[:, 0], orbits[i].state_vec[:, 1], orbits[i].state_vec[:, 2], 'b')
    axs.scatter3D(orbits[i].m2_apsis['periapsis_position'][0],
                  orbits[i].m2_apsis['periapsis_position'][1],
                  orbits[i].m2_apsis['periapsis_position'][2], color='k')
set_axes_equal(axs)


# plot orbit's characteristics
def plot_params(x, y, y1=None, x_label='', y_label='', title='', stability=False):
    _, ax = plt.subplots()
    ax.plot(x, y, 'b')
    if y1 is not None:
        ax.plot(x, y1, 'r')
    if stability:
        y2 = np.ones(np.size(x))
        ax.plot(x, y2, 'k')
        ax.plot(x, -y2, 'k')
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.set_title(title)
    ax.grid()


rp_vec_dim = np.asarray(rp_vec) * cr3bp.L  # periapsis radius in km
az_vec_dim = np.asarray(az_vec) * cr3bp.L  # vertical extension in km
stb_idx_vec = np.asarray(stability_idx_vec)  # stability indexes
plot_params(rp_vec_dim, cjac_vec, x_label='Rp [km]', y_label='Cjac [-]',
            title='Jacobi constant vs periapsis radius')
plot_params(rp_vec_dim, np.asarray(period_vec) * (cr3bp.T / 2.0 / np.pi) / DAYS2SEC,
            x_label='Rp [km]', y_label='T [days]', title='Orbit period vs periapsis radius')
plot_params(az_vec_dim, np.asarray(period_vec) * (cr3bp.T / 2.0 / np.pi) / DAYS2SEC,
            x_label='Az [km]', y_label='T [days]', title='Orbit period vs vertical extension')
plot_params(az_vec_dim, rp_vec_dim, x_label='Az [km]', y_label='Rp [km]',
            title='Periapsis radius vs vertical extension')
plot_params(rp_vec_dim, stb_idx_vec[:, 0], y1=stb_idx_vec[:, 1], x_label='Rp [km]',
            y_label='stability index [-]', title='Stability indexes vs periapsis radius',
            stability=True)
plt.show()
