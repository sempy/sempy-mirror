import numpy as np

from src.init.primary import Primary
from src.dynamics.r2bp_environment import R2BP

# Create environment
r2bp = R2BP(Primary.EARTH)

# Create initial states
x0 = np.array([10000., 0., 0., 0., 8., 0.])
x0_stm = np.concatenate((x0, np.eye(6).flatten()))

# Test the equations of motion
xdot = r2bp.ode(0, x0)
xstmdot = r2bp.ode(0, x0_stm, with_stm=True)
