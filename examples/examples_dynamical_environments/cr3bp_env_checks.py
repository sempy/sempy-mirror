import numpy as np

from src.dynamics.cr3bp_environment import CR3BP
from src.init.primary import Primary

# Create a CR3BP environment
cr3bp = CR3BP(Primary.SUN, Primary.EARTH)

# Create initial state
x0 = np.array([1., 0., 0., 0., 1., 0.])
x0_stm = np.concatenate((x0, np.eye(6).flatten()))

# Check cr3bp methods
ubar = cr3bp.get_u_bar(x0)
ubar1 = cr3bp.get_u_bar_first_partials(x0)
ubar2 = cr3bp.get_u_bar_second_partials(x0)
jac = cr3bp.get_jacobi(x0)

# Check cr3bp eom and variational matrix
xdot = cr3bp.ode(0., x0)
xstmdot = cr3bp.ode(0., x0_stm, with_stm=True)

# %% Create an environment with the Sun as primary 1 and the Earth-Moon system as primary 2
cr3bp2 = CR3BP(Primary.SUN, Primary.EARTH_AND_MOON)

