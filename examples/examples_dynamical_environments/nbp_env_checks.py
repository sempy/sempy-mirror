import numpy as np
import spiceypy as sp

from src.init.primary import Primary
from src.init.load_kernels import load_kernels
from src.dynamics.nbp_environment import NBP

from src.init.constants import MOON_SMA, MOON_OMEGA_MEAN

# Load the SPICE kernels
load_kernels()

# %%
# Create a NBP environment
bodies = [Primary.SUN, Primary.MOON, Primary.EARTH]

start_date = sp.et2datetime(0)
start_date = start_date.strftime('%Y-%m-%d')

nbp = NBP(bodies, 'EARTH', start_date=start_date)

x0 = np.array([400000, 0, 0, 0, 1.5, 0])
x0_stm = np.concatenate((x0, np.eye(6).flatten()))
x0_stm_ep = np.concatenate((x0_stm, np.zeros(6)))

xdot = nbp.ode(0, x0)
xdot_stm = nbp.ode(0, x0_stm, with_stm=True)
xdot_stm_ep = nbp.ode(0, x0_stm_ep, with_stm=True, with_epoch_partials=True)

# Test adimensionalization
nbp_adim = NBP(bodies, 'EARTH', adim_t=1/MOON_OMEGA_MEAN, adim_l=MOON_SMA, start_date=start_date)
x0_adim = nbp_adim.adimensionalize_state(x0)
x0_stm_adim = np.concatenate((x0_adim, np.eye(6).flatten()))
x0_stm_ep_adim = np.concatenate((x0_stm_adim, np.zeros(6)))

xdot_adim = nbp_adim.ode(0, x0_adim)
xdot_stm_ep_adim = nbp_adim.ode(0, x0_stm_ep_adim, with_stm=True, with_epoch_partials=True)

# Test
xdot_adim_calc = xdot / nbp_adim.adim_v
xdot_adim_calc[3:6] = xdot_adim_calc[3:6] * nbp_adim.adim_t
np.testing.assert_allclose(xdot_adim, xdot_adim_calc, rtol=1e-12)

xdot_adim_redim = xdot_adim * nbp_adim.adim_v
xdot_adim_redim[3:6] = xdot_adim_redim[3:6] / nbp_adim.adim_t
np.testing.assert_allclose(xdot_adim_redim, xdot, rtol=1e-12)

# %% Test centre switching
nbp = NBP(bodies, 'EARTH', start_date=start_date)
xdot_earth = nbp.ode(0, x0)
print('Current centre: ', nbp.integration_center)
print('xdot_earth: ', xdot_earth)

# Test switching to MOON
nbp.integration_center = 'MOON'
xdot_moon = nbp.ode(0, x0)
print('Current centre: ', nbp.integration_center)
print('xdot_moon: ', xdot_moon)

# %% Test epoch switching
print()
print('xdot: ', xdot)

nbp.start_date = '2000-01-01 14:45:35.816069'
xdot_new = nbp.ode(0, x0)
print('Current epoch: ', nbp.start_date)
print('xdot_earth: ', xdot_new)

nbp.start_date = sp.et2datetime(10000)
print('Current epoch: ', nbp.start_date)
