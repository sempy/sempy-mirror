import numpy as np
import spiceypy as sp

from src.dynamics.bcr4bp_environment import BCR4BP
from src.init.primary import Primary
from src.init.load_kernels import load_kernels

load_kernels()


# Create a BCR4BP environment
bcr4bp = BCR4BP(Primary.EARTH, Primary.MOON, 0.2, continuation_param=1)

# Create initial state
x0 = np.array([1., 0., 0., 0., 1., 0.])
x0_stm = np.concatenate((x0, np.eye(6).flatten()))

# Check cr3bp methods
ubar = bcr4bp.get_u_bar(0, x0)
ubar1_b = bcr4bp.get_u_bar_first_partials(0, x0)
ubar2_b = bcr4bp.get_u_bar_second_partials(0, x0)
ubar2 = bcr4bp.cr3bp.get_u_bar_second_partials(x0)
ham = bcr4bp.get_hamiltonian(0, x0)

# Check cr3bp eom and variational matrix
xdot_b = bcr4bp.ode(0., x0)
xdot = bcr4bp.cr3bp.ode(0., x0)
xstmdot = bcr4bp.ode(0., x0_stm, with_stm=True)
xstmdot_b = bcr4bp.cr3bp.ode(0., x0_stm, with_stm=True)

# get the Sun's position
sun_pos = bcr4bp.get_sun_position(0.)
print(bcr4bp.start_sun_phase)
bcr4bp.set_sun_to_date('2020-01-01 00:00:00')
sun_pos_2 = bcr4bp.get_sun_position(0.)
print(bcr4bp.start_sun_phase)
print(sun_pos, sun_pos_2)

sp.kclear()
