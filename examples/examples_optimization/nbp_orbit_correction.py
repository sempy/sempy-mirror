import time
import numpy as np
import pandas as pd
import spiceypy as sp
import matplotlib.pyplot as plt

from src.init.primary import Primary
from src.init.load_kernels import load_kernels
from src.dynamics.cr3bp_environment import CR3BP
from src.dynamics.nbp_environment import NBP
from src.coc.synodic_j2000 import  synodic_to_j2000, j2000_to_synodic
from src.propagation.propagator import Propagator, PathParameters
import src.solve.constraints_libraries.constraints_lib as cl
from src.solve.optimize.objective_lib import EphemerisCorrectionLib
from src.solve.constraints import Constraints
from src.solve.correct.corrector import Corrector
from src.solve.optimize.optimizer import Optimizer

from examples.examples_correction.data import DATA_DIR

def interpolate_state(df, xval, xcol):
    df.sort_values(by=[xcol], inplace=True)
    ycols = df.columns[1:7]
    return np.array([np.interp(xval, df[xcol], df[ycol]) for ycol in ycols])

# %% Load SPICE kernels
load_kernels()

# %% Create the environment
# Select and load primaries
Earth = Primary.EARTH
Moon = Primary.MOON
Sun = Primary.SUN

# Build environment
cr3bp = CR3BP(Earth, Moon)

# %% USER DATA
# To have always the same time of trajectory, round up
n_rev = 10
n_patch_per_rev = 20

# Select first guess
haloL2_db = pd.read_csv(str(DATA_DIR) + f'/JPL_Halos_L2.csv', delimiter=',')
orb_dir = 'southern'
obj_epoch_UTC = "2036-AUG-7"
orb_period = 2.2
sampling = 'arclength'
pruning = True
damping = 1  # Only for min-norm solver
bound_tol = 1e-1  # Only for SLSQP and trust-constr solvers
solver = 'min-norm'
# solver = 'SLSQP'
# solver = 'trust-constr'

# %%

# Build a propagator
prop = Propagator()

# Generate the initial guess
astate0_guess = interpolate_state(haloL2_db, orb_period, 'Period (TU) ')

# Select the number of nodes, and identify them along the orbit
n_nodes_cr3bp = 1

astate0_guess = astate0_guess[np.newaxis, :]
variables_guess = np.append(astate0_guess.ravel(), orb_period)

# Extract constraints from the libraries
ctr_state_match = cl.PeriodicLib.state_match(environment=cr3bp, propagator=prop)
ctr_T_y_fix = cl.PoincareLib.fix_elements([1, -1],[0, orb_period])

# Build constraints object
constraints = Constraints([ctr_state_match, ctr_T_y_fix])

# Correct the orbit
# Build corrector with the constraints
corrector = Corrector(constraints)

# Run corrector with jacobian
t0 = time.time()
print('Correcting CR3BP orbit...')
sol_corr = corrector(variables_guess, jac_flag=True)
t1 = time.time()
print(f'Correction time elapsed: {t1-t0:.5f} s')

# Extract corrected states
astate_corr = sol_corr.x[:n_nodes_cr3bp*6].reshape(n_nodes_cr3bp, 6)

# Propagate half orbit to start from the periapsis
astate0 = prop(cr3bp, [0, orb_period/2], sol_corr.x[:-1]).state_vec[-1]
if orb_dir == 'southern':
    # This step is necessary to have the correct direction of the orbit
    astate0[2] = -astate0[2]

# %% Orbit sampling
if sampling == 'period':
    # Create array of patch points
    if n_patch_per_rev % 2 != 0:
        n_patch_per_rev += 1
    at_patch = np.linspace(0, orb_period, n_patch_per_rev+1)[:-1]
    orb_patch = prop(cr3bp, at_patch, astate0[:6])

    # Repeat arrays for each revolution
    astate0_cr3bp = np.tile(orb_patch.state_vec, (n_rev, 1))
    at0_cr3bp = np.tile(at_patch, n_rev) + orb_period * np.repeat(np.arange(n_rev), n_patch_per_rev)

elif sampling == 'arclength':
    # Sample CR3BP orbit in arclength over one revolution
    arclength = {'function': PathParameters.arclength}
    orb_rev = prop(cr3bp, [0.0, orb_period], astate0, path_param=arclength)

    # Get orbit lenght
    orb_length = orb_rev.path_param_vec[-1, 0]

    # arclength samples and events
    arclength_samp = np.linspace(0.0, orb_length * (1.0 - 1.0 / n_patch_per_rev), n_patch_per_rev)
    arclength_evt = [lambda _, state, al=al: state[-1] - al for al in arclength_samp]

    # time and states at patch points for the first revolution
    orb_patch_arc = prop(cr3bp, [0.0, orb_period], astate0, events=arclength_evt, path_param=arclength)

    # Repeat arrays for each revolution
    # time and states at patch points for all revolutions from orbit periodicity
    at0_cr3bp = np.tile(np.asarray([t[0] for t in orb_patch_arc.t_events]).squeeze(), n_rev) + \
                orb_period * np.repeat(np.arange(n_rev), n_patch_per_rev)
    astate0_cr3bp = np.tile(np.asarray([s[0] for s in orb_patch_arc.state_events]).squeeze()[:, 0:6]
                            ,(n_rev, 1))
else:
    raise ValueError('Unknown sampling method')

# %%
# Retrieve number of nodes
n_nodes_nbp_adim = at0_cr3bp.size

# Adimensionalize requested initial time
at_requested = sp.utc2et(obj_epoch_UTC)/cr3bp.adim_t

# Find approximate adjusted initial time (to take into account the theta_obj value)
if pruning:
    # If pruning, start the correction one whole revolution before the requested time
    et_adj = (at_requested - orb_period) * cr3bp.adim_t
    start_epoch_UTC = sp.et2utc(et_adj, 'C', 0)
else:
    start_epoch_UTC = obj_epoch_UTC

#  Create NBP environment (starting at the desired epoch)
nbp_adim = NBP([Earth, Moon, Sun], center=Moon,
               start_date=start_epoch_UTC, adim_t=cr3bp.adim_t,
               adim_l=cr3bp.adim_l, frame=NBP.AvailableFrames.J2000)

# Convert CR3BP states into inertial states and times (in Moon centred inertial frame)
_, astate0_lci = synodic_to_j2000(at0_cr3bp, astate0_cr3bp, nbp_adim.start_et, cr3bp, Moon,
                                  bary_from_spice=True, adim=True)

# Fixing the periapsis epoch
ctr_fix_epoch = cl.PoincareLib.fix_elements([-n_nodes_nbp_adim + pruning*n_patch_per_rev],
                                            [at_requested - nbp_adim.start_et/nbp_adim.adim_t])

# Assemble variables vector, create constraints and build constraints object
# avar_guess = np.concatenate([astate0_lci.ravel(), at0_cr3bp])
# ctr_timestate_match = cl.OptimizeLib.timestate_node_match(environment=nbp_adim, propagator=prop)
# constraints = Constraints([ctr_timestate_match, ctr_fix_epoch])

# Assemble variables vector, create constraints and build constraints object
avar_guess = np.concatenate([astate0_lci.ravel(), np.diff(at0_cr3bp), at0_cr3bp])
ctr_timestate_epoch_match = cl.OptimizeLib.timestateepochs_node_match(environment=nbp_adim, propagator=prop)
constraints = Constraints([ctr_timestate_epoch_match, ctr_fix_epoch])

# Build objective function
obj = EphemerisCorrectionLib.null_cost()

# Build bounds (only for SLSQP and trust-constr)
bounds_list = [[-np.inf, np.inf] for _ in range(avar_guess.size)]
for i in range(0, n_nodes_nbp_adim * 6, 6):
    for j in range(3):
        bounds_list[i + j] = [avar_guess[i + j] - bound_tol, avar_guess[i + j] + bound_tol]
bounds = tuple(bounds_list)

# pause()

# %%
# Run optimizer
print('Computing NBP corrections...')
opti = Optimizer(equality_constraints=constraints)
t0 = time.time()
if solver == 'SLSQP':
    sol_opti = opti.optimize_slsqp(avar_guess, obj, bounds=bounds, jac_flag=True)
elif solver == 'min-norm':
    sol_opti = opti.optimize_minimum_norm(avar_guess, maxiter=50, damping=damping,
                                          verbose=True)
elif solver == 'trust-constr':
    sol_opti = opti.optimize_trust_region(avar_guess, obj, bounds=bounds,
                                          jac_flag=True, )
else:
    raise ValueError('Unknown solver')

t1 = time.time()
print(f'Correction time elapsed: {t1-t0:.5f} s')

# %%
# Extract corrected states
astate_opti = sol_opti.x[pruning*n_patch_per_rev*6:n_nodes_nbp_adim*6].\
    reshape(n_nodes_nbp_adim-pruning*n_patch_per_rev, 6)

# Extract corrected times
at_vec_opti = sol_opti.x[-n_nodes_nbp_adim + pruning*n_patch_per_rev:]

# Propagate first state
sol_prop = prop(nbp_adim, [at_vec_opti[0], at_vec_opti[0] + orb_period*3], astate_opti[0, :],
                           n_eval=10000)

# Convert corrected states to synodic frame
t = np.ascontiguousarray(at_vec_opti) + nbp_adim.start_et/nbp_adim.adim_t
s = np.ascontiguousarray(astate_opti)
at_syn, astate_syn = j2000_to_synodic(t, s, nbp_adim.start_et, cr3bp, Moon, adim=True,
                                                  bary_from_spice=True,)

# Convert propagated states to synodic frame
t = np.ascontiguousarray(sol_prop.t_vec) + nbp_adim.start_et/nbp_adim.adim_t
s = np.ascontiguousarray(sol_prop.state_vec)
at_syn_prop, astate_syn_prop = j2000_to_synodic(t, s, nbp_adim.start_et, cr3bp, Moon, adim=True,
                                                bary_from_spice=True,)

# %% Plot results in synodic frame
orb_prop = prop(cr3bp, [0, orb_period], astate0, n_eval=1000)

_, axs = plt.subplots(1, 3, figsize=(12, 4))
INDEX = ((0, 1), (0, 2), (1, 2))
LABELS = (('x [LU]', 'y [LU]'), ('x [LU]', 'z [LU]'), ('y [LU]', 'z [LU]'))
for i, (x, y) in enumerate(INDEX):
    axs[i].plot(orb_prop.state_vec[:, x], orb_prop.state_vec[:, y],
                label='CR3BP', c='tab:orange', lw=2, zorder=0)
    axs[i].scatter(astate0_cr3bp[:, x], astate0_cr3bp[:, y],
                   label='Initial guess', c='k', s=30, edgecolors='w', lw=.5)
    axs[i].scatter(astate_syn[:, x], astate_syn[:, y],
                   label='Optimized', c='tab:blue', s=30, edgecolors='w', lw=.5)
    axs[i].plot(astate_syn_prop[:, x], astate_syn_prop[:, y],
                label='Propagated', c='tab:blue', lw=1)
    axs[i].set_xlabel(LABELS[i][0])
    axs[i].set_ylabel(LABELS[i][1])
    axs[i].grid()
    axs[i].set_aspect('equal')
axs[0].legend(bbox_to_anchor=(1.05, 1), loc='upper left')
plt.show()


# %% Clear SPICE kernels
sp.kclear()
