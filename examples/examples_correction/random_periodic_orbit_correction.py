import numpy as np
import time

from src.init.primary import Primary
from src.dynamics.cr3bp_environment import CR3BP
from src.propagation.propagator import Propagator

import src.solve.constraints_libraries.constraints_lib as cl
from src.solve.constraints import Constraints
from src.solve.correct.corrector import Corrector

# %%

# Select and load primaries
Earth = Primary.EARTH
Moon = Primary.MOON

# Build environment
dyn_env = CR3BP(Primary.EARTH, Primary.MOON)

# Build propagator
prop = Propagator()

# Extract constraints from the libraries
ctr_state_match = cl.PeriodicLib.state_match(environment=dyn_env, propagator=prop)
ctr_elems_fix = cl.PoincareLib.fix_elements([1,2],[0,0])

# Build constraints object
constraints = Constraints([ctr_state_match, ctr_elems_fix]) 
# COMMENTS: 
# - Review names (like "AssembleConstraints" or "BuildConstraintFunction")
# - maybe make it a callable instance like propagator

# test constraints
variables_guess = np.append(np.random.rand(6), 1.5)

# Build corrector
correct = Corrector(constraints)

# test corrector
t0 = time.time()
sol_corr = correct(variables_guess, jac_flag=False)
t1 = time.time()
sol_corr_with_jac = correct(variables_guess, jac_flag=True)
t2 = time.time()

print(sol_corr.x, '\nElapsed time: ', t1-t0)
print(sol_corr_with_jac.x, '\nElapsed time (with Jacobian): ', t2-t1)
