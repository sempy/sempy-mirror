import time
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from src.init.primary import Primary
from src.dynamics.cr3bp_environment import CR3BP
from src.propagation.propagator import Propagator

import src.solve.constraints_libraries.constraints_lib as cl
from src.solve.constraints import Constraints
from src.solve.correct.corrector import Corrector

from examples.examples_correction.data import DATA_DIR

def interpolate_state(df, xval, xcol):
    df.sort_values(by=[xcol], inplace=True)
    ycols = df.columns[1:7]
    return np.array([np.interp(xval, df[xcol], df[ycol]) for ycol in ycols])

# %% User inputs

# Orbit database
orbits_db = pd.read_csv(str(DATA_DIR) + '/JPL_Halos_L2.csv', delimiter=',')
# orbits_db = pd.read_csv(str(DATA_DIR) + '/JPL_Lyap_L1.csv', delimiter=',')
# orbits_db = pd.read_csv(str(DATA_DIR) + '/JPL_Long_L4.csv', delimiter=',')

# Fix coordinate
fix_coord = 1  # 1 for y, 2 for z

# Orbital period
orb_period = 3

# Number of nodes (1 for single shooting, N for multiple shooting)
N_nodes = 1

# Choose if to test asymmetric time distribution of nodes
asymm = False

# %% Create the environment
# Select and load primaries
Earth = Primary.EARTH
Moon = Primary.MOON

# Build environment
cr3bp = CR3BP(Earth, Moon)

# Build a propagator in the CR3BP
prop = Propagator()

# %% Build the initial guess
x0_guess_init = interpolate_state(orbits_db, orb_period, 'Period (TU) ')

if N_nodes == 1:
    x0_guess = x0_guess_init.reshape(N_nodes, 6)
    tau = orb_period
else:
    if asymm:
        time_int = np.sort(np.random.uniform(0, orb_period, N_nodes-2))
        time_int = np.insert(time_int, 0, 0)
        time_int = np.append(time_int, orb_period)
        # In the asymmetrical case, the final state ~~ initial state is included in the guess
        tau = time_int[:]
    else:
        time_int = np.linspace(0, orb_period, N_nodes+1)[:-1]  # Last node coincides with the first one (not used)...
        tau = orb_period
    x0_guess = prop(cr3bp, time_int, x0_guess_init).state_vec.reshape(N_nodes, 6)

variables_guess = np.append(x0_guess.ravel(), tau)

# %% Create Constraints
# Extract constraints from the libraries
if asymm:
    # Constraints:
    # - state match at each node
    # - fix y0=0 and all times (for Halo and lyapunov) or fix z0=0 and all times (for long period)
    # - set initial state and final state as equal
    cnstr_list = [
        cl.PoincareLib.fix_elements([fix_coord] + list(np.arange(-N_nodes, 0)),
                                 [0,] + variables_guess[np.arange(-N_nodes, 0)].tolist()),
        cl.OptimizeLib.timestate_node_match(cr3bp, prop),
        cl.PoincareLib.geom_state_match(0, -N_nodes-6)
    ]
else:
    cnstr_list = [cl.PoincareLib.fix_elements([fix_coord, -1], [0, orb_period]),
                  cl.PeriodicLib.state_match(cr3bp, prop)]

# Build constraints object
constraints = Constraints(cnstr_list)

# %% Correct the orbit
# Build corrector with the constraints
correct = Corrector(constraints, tol=1e-13)

# Run corrector with and without jacobian
t0 = time.time()
sol_corr = correct(variables_guess, jac_flag = False)
t1 = time.time()
sol_corr_with_jac = correct(variables_guess, jac_flag = True)
t2 = time.time()

print(f'Time elapsed: {t1-t0} s')
print(f'Time elapsed (with Jacobian): {t2-t1} s')

# %%
# Extract initial state
if asymm:
    ind = -N_nodes
else:
    ind = -1

x_corr = sol_corr.x[:ind].reshape(N_nodes, 6)
x_corr_with_jac = sol_corr_with_jac.x[:ind].reshape(N_nodes, 6)

x0 = x_corr[0, :]
x0_with_jac = x_corr_with_jac[0, :]

# Propagate the solution
tspan = np.linspace(0, orb_period, 1000)
guess_prop = prop(cr3bp, tspan, x0_guess[0])
orb_prop = prop(cr3bp, tspan, x0[:6])
orb_prop_with_jac = prop(cr3bp, tspan, x0_with_jac[:6])

# Check periodicity
print("\nPeriodicity:")
print(f'Initial state == final state: {np.allclose(orb_prop.state_vec[0], orb_prop.state_vec[-1])}')
print(f'Initial state == final state (with Jacobian): '
      f'{np.allclose(orb_prop_with_jac.state_vec[0], orb_prop_with_jac.state_vec[-1])}')

# %% Plot the results
fig, axs = plt.subplots(1, 3)
indexes = ((0, 1), (0, 2), (1, 2))
labels = ('x', 'y', 'z')
for i, (ax, ind) in enumerate(zip(axs, indexes)):
    ax.plot(guess_prop.state_vec[:, ind[0]], guess_prop.state_vec[:, ind[1]], label='Guess')
    ax.plot(orb_prop.state_vec[:, ind[0]], orb_prop.state_vec[:, ind[1]], label='Orbit')
    ax.plot(orb_prop_with_jac.state_vec[:, ind[0]], orb_prop_with_jac.state_vec[:, ind[1]], label='Orbit (with Jac)')
    ax.scatter(x0_guess[:, ind[0]], x0_guess[:, ind[1]], label='Guess nodes')
    ax.scatter(x_corr[:, ind[0]], x_corr[:, ind[1]], label='Orbit nodes')
    ax.scatter(x_corr_with_jac[:, ind[0]], x_corr_with_jac[:, ind[1]], label='Orbit nodes (Jac)')
    ax.set_xlabel(labels[ind[0]])
    ax.set_ylabel(labels[ind[1]])
    ax.axis('equal')
    ax.legend()
plt.legend()
plt.show()

# %% Plot relative percentage difference
labels = ['x', 'y', 'z', 'vx', 'vy', 'vz']
def abs_change(x, xref):
    return abs(x-xref)
fig2, axs = plt.subplots(1, 6, figsize=(15, 5))
for i, ax in enumerate(axs):
    ax.plot(tspan, abs_change(orb_prop.state_vec[:, i], guess_prop.state_vec[:, i]), label='Corrected')
    ax.plot(tspan, abs_change(orb_prop_with_jac.state_vec[:, i], guess_prop.state_vec[:, i]), label='Corrected (Jac)')
    ax.set_xlabel('t')
    ax.set_title(f'{labels[i]}')

axs[0].set_ylabel('rel_err')
axs[0].legend()
fig2.tight_layout()
plt.show()
