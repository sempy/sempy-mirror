Ephemeris Examples.
====================

This gallery contains different examples that demonstrate the transition from the Circular
Restricted Three-Body Problem formulation to the N-body ephemeris model.

Example cases cover the usage of `Ephemeris`, `EphemerisPropagator` and
`MultipleShooting` classes to define a N-body force model, integrate its dynamics and correct
a given trajectory transitioned from the CR3BP.
