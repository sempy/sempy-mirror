"""
Correction of an Earth-Moon L2 southern Halo orbit in the Sun-Earth-Moon ephemeris model.
=========================================================================================

This example demonstrate how to transition a periodic orbit generated in the CR3BP model into a
higher fidelity N-body ephemeris model using differential correction techniques based on multiple
shooting procedure.

The target orbit is an Earth-Moon L2 southern Halo orbit defined in the Earth-Moon CR3BP and
transitioned into the Sun-Earth-Moon ephemeris model. The following steps are required:

1. | Initialization of the CR3BP structure and the Halo orbit.
2. | Interpolation and sampling of the Halo orbit in the CR3BP force model.
3. | Coordinate transformation from CR3BP synodic to J2000 inertial frame centered
   | at the Earth's center.
4. | Multiple shooting procedure in J2000 to obtain a continuous trajectory in
   | the Sun-Earth-Moon ephemeris model.
5. | Coordinate transformation from J2000 inertial to instantaneous Earth-Moon barycenter
   | centered rotating frame.
6. | Display of the initial CR3BP orbit and corrected trajectory in a rotating frame.

"""

# %% Import statements and SPICE kernels loading:
#
# We will start importing all the modules and classes needed to complete the aforementioned steps
# and the SPICE kernels required by the change of coordinates and differential correction routines.

import numpy as np
import spiceypy as sp

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.init.ephemeris import Ephemeris
from src.orbits.halo import Halo
from src.init.load_kernels import load_kernels

from src.coc.synodic_j2000 import synodic_to_j2000, j2000_to_synodic
from src.solve.multiple_shooting import MultipleShooting

import matplotlib.pyplot as plt
from src.plotting.simple.utils import decorate_3d_axes

load_kernels()

# %% Definition of the environment:
#
# Once the import step is completed, we can move on defining the dynamical models of interest:
# Earth-Moon CR3BP and Sun-Earth-Moon ephemeris model.

cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
ephemeris = Ephemeris((Primary.EARTH, Primary.MOON, Primary.SUN))

# %% Halo orbit initialization and sampling:
#
# The third step consist in the initialization, interpolation and sampling of the target Halo
# orbit in the Earth-Moon CR3BP. Cr3bpOrbit class methods are available for all above
# mentioned steps. An Earth-Moon L2 southern Halo orbit with vertical extension of 30000 km
# is chosen in this case. After the initial interpolation, samples are collected over 10
# revolutions of the CR3BP orbit. A uniform period fraction sampling with 50 samples per orbit
# is selected in this example.

halo = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Azdim=30e3)
halo.interpolation()

nb_revs = 10
nb_patch_rev = 50
t_patch, state_patch = halo.sampling(nb_revs, nb_patch_rev)

# %% Change of coordinates from CR3BP synodic to J2000 inertial frame:
#
# After computing the required patch points in the CR3BP synodic frame, the last must be
# expressed in an Earth-centered J2000 inertial frame to perform the correction in the
# Sun-Earth-Moon ephemeris model.
#
# Transformation from synodic to inertial J2000 requires first the definition of an initial
# epoch, selected to be on June 18, 2020 at 12:00:00.000 UTC. The calendar date must be
# converted into the corresponding ephemeris time ET (ephemeris seconds past J2000) to be used
# as input parameter for the coordinate transformation functions. This is done invoking the
# SPICE routine `str2et`.
#
# Secondly, the output ephemeris time and patch points states expressed in the inertial frame
# are adimensionalized using the characteristic time and length of the initial CR3BP structure,
# defined as the reciprocal of the mean motion of the primary and their corresponding distance
# expressed in seconds and kilometers respectively. These quantities are defined below.

et0 = sp.str2et('2020 JUN 18 12:00:00.000')  # initial epoch in ephemeris seconds

t_c = cr3bp.T / 2.0 / np.pi  # characteristic time in seconds
l_c = cr3bp.L  # characteristic length in kilometers

t_patch_j2000, state_patch_j2000 = synodic_to_j2000(t_patch, state_patch, et0, cr3bp, cr3bp.m1,
                                                    adim=True, bary_from_spice=True)

# %% Initialization of the Multiple Shooting routine:
#
# The differential correction routines based on multiple shooting techniques and implemented
# in SEMPY are made available to the end user by the MultipleShooting class.
# To instantiate a new MultipleShooting object, it is required to specify the ephemeris model
# in which the correction will be performed and possibly several optional input parameters
# such as characteristic quantities for adimensionalization of the Equations of Motion,
# propagator settings and acceptable tolerance under which the iterative procedure is stopped.
# An additional flags determines if the constraint on epoch continuity must be added to the
# problem formulation to improve converge. If set to True, the correct method must be called
# with the variable time options set also to True (the default in this case).

multiple_shooting = MultipleShooting(ephemeris, epoch_constr=True, t_c=t_c, l_c=l_c)

# %% Correction of the trajectory in the Sun-Earth-Moon ephemeris model:
#
# Once a MultipleShooting object has been instantiated, the initial patch points expressed
# in the J2000 inertial frame can be corrected using the correct method of the former object.
# If only patch points time and states are passed as inputs, no additional constraints other
# than states continuity and (optionally) epoch continuity are enforced during the multiple
# shooting procedure. An example call is demonstrated below.

print('\nStart of the multiple shooting procedure\n')
t_corr_j2000, state_corr_j2000, _ = \
    multiple_shooting.correct(t_patch_j2000, state_patch_j2000)
print('\nEnd of the multiple shooting procedure')

# %% Change of coordinates from J2000 inertial to CR3BP synodic frame:
#
# After the differential correction procedure has ended, the resulting patch points time and
# states are transitioned back into an instantaneous Earth-Moon barycenter centered rotating
# frame to be more easily visualized together with the initial reference orbit. This is
# accomplished in a similar way as for the inverse transformation.
#
# Moreover, if a variable time correction procedure has been chosen, an updated initial
# epoch UTC and calendar format can be retrieved as well using the SPICE routine `et2utc`.

et0_corr = t_corr_j2000[0] * t_c  # corrected initial epoch in ephemeris seconds
utc0_corr = sp.et2utc(et0_corr, 'C', 5)  # corrected initial epoch in UTC and calendar format
print(f"\nCorrected initial epoch: {utc0_corr} UTC")

t_corr_syn, state_corr_syn = j2000_to_synodic(t_corr_j2000, state_corr_j2000, et0_corr, cr3bp,
                                              cr3bp.m1, adim=True, bary_from_spice=True)

# %% Display in a Earth-Moon barycenter centered rotating frame:
#
# Finally, the initial reference orbit defined in the CR3BP model and the corrected patch
# points can be displayed together in a rotating frame that approximates the CR3BP synodic one.

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot(halo.state_vec[:, 0], halo.state_vec[:, 1], halo.state_vec[:, 2], color='b',
        label='Halo orbit')
ax.plot(state_corr_syn[:, 0], state_corr_syn[:, 1], state_corr_syn[:, 2], color='r',
        label='Corrected trajectory')
ax.scatter(cr3bp.m2_pos[0], 0., 0., color='k', label='Moon')
ax.scatter(cr3bp.l2.position[0], 0., 0., color='m', label='L2')
decorate_3d_axes(ax, 'EM L2 southern Halo corrected in the Sun-Earth-Moon ephemeris model', '-')
plt.show()

# %% Kernel unloading:
#
# We will conclude unloading the kernel pool using the SPICE routine kclear.

sp.kclear()
