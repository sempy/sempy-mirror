"""
Correction of an Earth-Moon L2 southern NRHO orbit in the Sun-Earth-Moon ephemeris model.
=========================================================================================

This example demonstrate how to transition a periodic orbit generated in the CR3BP model into a
higher fidelity N-body ephemeris model using differential correction techniques based on multiple
shooting procedure.

The target orbit is an Earth-Moon L2 southern NRHO orbit defined in the Earth-Moon CR3BP and
transitioned into the Sun-Earth-Moon ephemeris model. The following steps are required:

1. | Initialization of the CR3BP structure and the NRHO orbit.
2. | Interpolation and sampling of the NRHO orbit in the CR3BP force model.
3. | Coordinate transformation from CR3BP synodic to J2000 inertial frame centered
   | at the Earth's center.
4. | Multiple shooting procedure in J2000 to obtain a continuous trajectory in
   | the Sun-Earth-Moon ephemeris model.
5. | Propagation of the previously corrected patch points.
6. | Coordinate transformation from J2000 inertial to instantaneous Earth-Moon barycenter
   | centered rotating frame.
7. | Display of the initial CR3BP orbit and corrected trajectory in a rotating frame.

"""

# %% Import statements and SPICE kernels loading:
#
# We will start importing all the modules and classes needed to complete the aforementioned steps
# and the SPICE kernels required by the change of coordinates and differential correction routines.

import numpy as np
import spiceypy as sp

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.init.ephemeris import Ephemeris
from src.init.load_kernels import load_kernels
from src.orbits.nrho import NRHO

from src.coc.synodic_j2000 import synodic_to_j2000, j2000_to_synodic
from src.solve.multiple_shooting import MultipleShooting
from src.propagation.patch_points_propagator import PatchPointsPropagator

import matplotlib.pyplot as plt
from src.plotting.simple.utils import decorate_3d_axes

load_kernels()

# %% Definition of the environment:
#
# Once the import step is completed, we can move on defining the dynamical models of interest:
# Earth-Moon CR3BP and Sun-Earth-Moon ephemeris model.

cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
ephemeris = Ephemeris((Primary.EARTH, Primary.MOON, Primary.SUN))

# %% NRHO orbit initialization and sampling:
#
# The third step consist in the initialization, interpolation and sampling of the target
# NRHO orbit in the Earth-Moon CR3BP.
# Cr3bpOrbit class methods are available for all above mentioned steps.
# An Earth-Moon L2 southern NRHO orbit with periselene radius of 5930 km is chosen in this case.
# After the initial interpolation, samples are collected over 10 revolutions of the CR3BP orbit.
# A uniform period fraction sampling with 50 samples per orbit is selected in this example.

nrho = NRHO(cr3bp, cr3bp.l2, NRHO.Family.southern, Rpdim=5930)
nrho.interpolation()

nb_revs = 10
nb_patch_rev = 50
t_patch, state_patch = nrho.sampling(nb_revs, nb_patch_rev)

# %% Change of coordinates from CR3BP synodic to J2000 inertial frame:
#
# After computing the required patch points in the CR3BP synodic frame, the last must be
# expressed in an Earth-centered J2000 inertial frame to perform the correction in the
# Sun-Earth-Moon ephemeris model.
#
# Transformation from synodic to inertial J2000 requires first the definition of an initial
# epoch, selected to be on June 18, 2020 at 12:00:00.000 UTC. The calendar date must be
# converted into the corresponding ephemeris time ET (ephemeris seconds past J2000) to be used
# as input parameter for the coordinate transformation functions. This is done invoking the
# SPICE routine `str2et`.
#
# Secondly, the output ephemeris time and patch points states expressed in the inertial
# frame are adimensionalized using the characteristic time and length of the initial CR3BP
# structure, defined as the reciprocal of the mean motion of the primary and their
# corresponding distance expressed in seconds and kilometers respectively.
# These quantities are defined below.

et0 = sp.str2et('2020 JUN 18 12:00:00.000')  # initial epoch in ephemeris seconds

t_c = cr3bp.T / 2.0 / np.pi  # characteristic time in seconds
l_c = cr3bp.L  # characteristic length in kilometers

t_patch_j2000, state_patch_j2000 = synodic_to_j2000(t_patch, state_patch, et0, cr3bp, cr3bp.m1,
                                                    adim=True, bary_from_spice=True)

# %% Initialization of the Multiple Shooting routine:
#
# The differential correction routines based on multiple shooting techniques and implemented
# in SEMPY are made available to the end user by the MultipleShooting class.
# To instantiate a new MultipleShooting object, it is required to specify the ephemeris model
# in which the correction will be performed and possibly several optional input parameters
# such as characteristic quantities for adimensionalization of the Equations of Motion,
# propagator settings and acceptable tolerance under which the iterative procedure is stopped.
# An additional flags determines if the constraint on epoch continuity must be added to the
# problem formulation to improve converge. If set to True, the correct method must be called
# with the variable time options set also to True (the default in this case).

multiple_shooting = MultipleShooting(ephemeris, epoch_constr=True, t_c=t_c, l_c=l_c,
                                     precision=1e-6, maxiter=20)

# %% Correction of the trajectory in the Sun-Earth-Moon ephemeris model:
#
# Once a MultipleShooting object has been instantiated, the initial patch points expressed
# in the J2000 inertial frame can be corrected using the correct method of the former object.
# If only patch points time and states are passed as inputs, no additional constraints other
# than states continuity and (optionally) epoch continuity are enforced during the multiple
# shooting procedure. An example call is demonstrated below.

print('\nStart of the multiple shooting procedure\n')
t_corr_j2000, state_corr_j2000, _ = multiple_shooting.correct(t_patch_j2000, state_patch_j2000)
print('\nEnd of the multiple shooting procedure')

# %% Propagation of the corrected patch points in the Sun-Earth-Moon ephemeris model:
#
# After the differential correction procedure has ended, a smooth trajectory is computed via
# explicit propagation of the corrected patch point states across the corresponding time
# intervals. This is accomplished instantiating a new PatchPointPropagator object with the
# dynamical model and characteristic quantities already defined for the multiple shooting
# procedure.
# Its propagate method takes as input parameters a series of patch point times and states and
# returns a smooth trajectory obtained via explicit integration of the associated Equations of
# Motion between subsequent nodes. Optional parameters expose to the end user the possibility
# to prune from the final solution a given number of initial and final patch points as well as
# determine the number of discrete points in which the solution is returned.
#
# In this example, the first and last revolutions are cut from the integrated trajectory and
# a sampling grid with 100 nodes between subsequents patch points is chosen.

patch_points_prop = PatchPointsPropagator(ephemeris, t_c=t_c, l_c=l_c, time_steps=100)
t_vec_j2000, state_vec_j2000 = \
    patch_points_prop.propagate(t_corr_j2000, state_corr_j2000, prune_first=nb_patch_rev,
                                prune_last=nb_patch_rev)

# %% Change of coordinates from J2000 inertial to CR3BP synodic frame:
#
# Once the corrected patch points have been propagated to obtain a smooth trajectory,
# the last is transitioned back into an instantaneous Earth-Moon barycenter centered rotating
# frame to be more easily visualized together with the initial reference orbit.
# This is accomplished in a similar way as for the inverse transformation.
#
# Moreover, if a variable time correction procedure has been chosen, an updated initial
# epoch UTC and calendar format can be retrieved as well using the SPICE routine `et2utc`.
# Note that its value will be different for the first corrected patch point and the first state
# in the propagated trajectory if a positive integer has been passed as input to the
# prune_first argument of the PatchPointsPropagator propagate method.

et0_corr = t_vec_j2000[0] * t_c  # corrected initial epoch in ephemeris seconds
utc0_corr = sp.et2utc(et0_corr, 'C', 5)  # corrected initial epoch in UTC and calendar format
print(f"\nCorrected initial epoch: {utc0_corr} UTC")

t_vec_syn, state_vec_syn = j2000_to_synodic(t_vec_j2000, state_vec_j2000, et0_corr, cr3bp,
                                            cr3bp.m1, adim=True, bary_from_spice=True)

# %% Display in a Earth-Moon barycenter centered rotating frame:
#
# Finally, the initial reference orbit defined in the CR3BP model and the corrected patch
# points can be displayed together in a rotating frame that approximates the CR3BP synodic one.

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot(nrho.state_vec[:, 0], nrho.state_vec[:, 1], nrho.state_vec[:, 2], color='b',
        label='NRHO orbit')
ax.plot(state_vec_syn[:, 0], state_vec_syn[:, 1], state_vec_syn[:, 2], color='r',
        label='Corrected trajectory')
ax.scatter(cr3bp.m2_pos[0], 0., 0., color='k', label='Moon')
ax.scatter(cr3bp.l2.position[0], 0., 0., color='m', label='L2')
decorate_3d_axes(ax, 'EM L2 southern NRHO corrected in the Sun-Earth-Moon ephemeris model', '-')
plt.show()

# %% Kernel unloading:
#
# We will conclude unloading the kernel pool using the SPICE routine kclear.

sp.kclear()
