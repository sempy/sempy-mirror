"""
Multiple Shooting procedure with velocity discontinuities.
==========================================================

This example demonstrates how to include velocity discontinuities at patch points while correcting
a given trajectory in the N-body ephemeris model.

For a more in-depth introduction on the initialization and exploitation of multiple shooting
procedures see also the other example scripts in this folder.

"""

# %%
#
# Import statements

import numpy as np
import spiceypy as sp
import matplotlib.pyplot as plt

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.init.ephemeris import Ephemeris
from src.init.load_kernels import load_kernels
from src.orbits.halo import Halo
from src.coc.synodic_j2000 import synodic_to_j2000, j2000_to_synodic
from src.solve.multiple_shooting import MultipleShooting
from src.propagation.patch_points_propagator import PatchPointsPropagator
from src.plotting.simple.utils import decorate_3d_axes

load_kernels()

# %%
#
# Definition of the dynamical models

cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)  # CR3BP structure for the Earth-Moon system
t_c = cr3bp.T / 2.0 / np.pi  # characteristic time [s]
l_c = cr3bp.L  # characteristic length [km]

# N-body ephemeris model for the Sun-Earth-Moon system

eph = Ephemeris((Primary.EARTH, Primary.MOON, Primary.SUN))

# %%
#
# Initialization and interpolation of an Earth-Moon L2 southern Halo orbit in the CR3BP

halo = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Azdim=30e3)  # Az of 30000 km
halo.interpolation()

# %%
#
# Orbit sampling

nb_revs = 10  # total number of revolutions
nb_patch_rev = 20  # number of patch points per revolution
t_patch, state_patch = halo.sampling(nb_revs, nb_patch_rev)

# %%
#
# Change of coordinates from synodic to inertial J2000

et0 = sp.str2et('01 JUL 2020 12:00:00.000')  # start epoch in ephemeris time [s]
t_patch_j2000, state_patch_j2000 = synodic_to_j2000(t_patch, state_patch, et0, cr3bp, cr3bp.m1,
                                                    adim=True, bary_from_spice=True)

# %%
#
# Definition of impulsive manoeuvres
#
# While correcting a series of patch points to obtain a continuous trajectory, impulsive
# manoeuvres can be taken into account as velocity discontinuities allowed at one or more
# patch points. The `correct` method of the `MultipleShooting` class accepts to different
# keywords to specify their location: `delta_v` and `constr_mask`.
#
# `delta_v` must be a single integer or a tuple of integers, each of them specifying the
# index or indexes of the patch points on which the continuity constraint on the three
# velocity components has to be removed. For example, passing `delta_v=(5, 10)` will result
# in two impulsive manoeuvres carried out at the 6th and 11th patch points respectively
# (remember that in Python indexes start from zero and not one).
#
# `constr_mask` must be a boolean mask (e.g. an array of zeros and ones) with size
# `6 * (number of patch points - 1)` which allows a more fine tuning of the enforced and
# released continuity constraints. In fact, the mask allows to control each component of the
# error vector independently, thus potentially preventing impulsive manoeuvres to be carried
# out in a given direction. Zeros correspond to imposed constraints and ones to released ones
# (impulsive manoeuvres). Be aware that in principle this second option allows to specify
# position discontinuities if ones are passed for the corresponding constraints.
#
# As a first example, impulsive manoeuvres carried out at the 21st and 41st patch points
# (second and fourth periselene passage) can be specified as follows:

delta_v = (20, 40)

# %%
#
# Then, using the second available keyword the same manoeuvres can be declared with the boolean
# mask defined below. Care must be taken since continuity constraints applied at the ith patch
# point correspond to rows `[6 * (i - 1), 6 * i - 1]` in the error vector `fx_vec`.
# As a consequence, indexes `(117, 118, 119)` and `(237, 238, 239)` must be fixed in this example.

constr_mask = np.zeros(6 * (nb_revs * nb_patch_rev - 1), dtype=np.bool)
constr_mask[6 * 19 + np.arange(3, 6)] = 1
constr_mask[6 * 39 + np.arange(3, 6)] = 1

# %%
#
# Differential correction
#
# Once the manoeuvres has been defined, the multiple shooting algorithm is initialized as usual
# and the two keywords has just to be passed to its `correct` method. Note that `delta_v` and
# `constr_mask` are mutually exclusive.

multi_shoot = MultipleShooting(eph, epoch_constr=True, t_c=t_c, l_c=l_c)

# %%
#
# A first solution is computed with the `delta_v` keyword as follows:

t_corr1_j2000, state_corr1_j2000, sol1 = \
    multi_shoot.correct(t_patch_j2000, state_patch_j2000, delta_v=delta_v)

# %%
#
# The second one is then computed with the `constr_mask` keyword as follows:

t_corr2_j2000, state_corr2_j2000, sol2 = \
    multi_shoot.correct(t_patch_j2000, state_patch_j2000, constr_mask=constr_mask)

# %%
#
# Maneuvers analysis
#
# Once the differential correction has ended, it is possible to retrieve the components and
# magnitude of the specified manoeuvres from the error vector included in the solution
# dictionaries `sol1` and `sol2`.
# In order to do so, it is sufficient to extract the array components corresponding to the
# continuity constraints on the velocities that have not been enforced during the correction
# procedure. The velocity components are then given by the opposite of the former values.
# The corresponding indexes are also stored in the same dictionaries with keywords
# `fx_free_idx`. It is then convenient to reshape the output array as a three column matrix
# such that each column correspond to a different velocity component (vx, vy, vz) and each
# row to a different manoeuvre.

dv_comps1_j2000 = - sol1['fx_vec'][sol1['fx_free_idx']].reshape(sol1['fx_free_idx'].size // 3, 3)
dv_comps2_j2000 = - sol2['fx_vec'][sol2['fx_free_idx']].reshape(sol2['fx_free_idx'].size // 3, 3)

# %%
#
# The dV magnitude applied at each patch point is then retrieved computing the norm of the
# previous vectors by rows.

dv_mag1 = np.linalg.norm(dv_comps1_j2000, axis=1)
dv_mag2 = np.linalg.norm(dv_comps2_j2000, axis=1)

# %%
#
# Finally, a visual check verifies that the two problem formulations described above are
# equivalent and lead to the same results

v_c = 1e3 * l_c / t_c  # characteristic speed [m/s]

print(f"\n{'Computed impulsive manoeuvres:':^50s}\n")
print(f"{'dV keyword':^20s}{'':5s}{'boolean mask':^20s}")
for i in range(dv_mag1.size):
    print(f"{dv_mag1[i] * v_c:20.15f}{'m/s':^5s}{dv_mag2[i] * v_c:20.15f}{'m/s':^5s}")

# %%
#
# Propagation of the corrected patch points

prop = PatchPointsPropagator(eph, t_c=t_c, l_c=l_c, time_steps=100)
t_prop1_j2000, state_prop1_j2000 = prop.propagate(t_corr1_j2000, state_corr1_j2000)
t_prop2_j2000, state_prop2_j2000 = prop.propagate(t_corr2_j2000, state_corr2_j2000)

# %%
#
# Change of coordinates from J2000 inertial to synodic

t_prop1, state_prop1 = j2000_to_synodic(t_prop1_j2000, state_prop1_j2000, t_prop1_j2000[0] * t_c,
                                        cr3bp, cr3bp.m1, True, True)
t_prop2, state_prop2 = j2000_to_synodic(t_prop2_j2000, state_prop2_j2000, t_prop2_j2000[0] * t_c,
                                        cr3bp, cr3bp.m1, True, True)

# %%
#
# Plots

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot(halo.state_vec[:, 0], halo.state_vec[:, 1], halo.state_vec[:, 2],
        color='k', label='Halo')
ax.plot(state_prop1[:, 0], state_prop1[:, 1], state_prop1[:, 2],
        color='b', label='corrected dV key')
ax.plot(state_prop2[:, 0], state_prop2[:, 1], state_prop2[:, 2], '--',
        color='r', label='corrected mask')
decorate_3d_axes(ax, 'Corrected Halo orbit with velocity discontinuities', '-')
plt.show()

sp.kclear()
