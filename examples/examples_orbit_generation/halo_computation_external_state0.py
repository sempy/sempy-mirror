"""
Halo Computation from External State.
=====================================
"""

# %%
# A halo orbit can be computed using an external initial state,
# we will see that by going throughout this script.
#
# 1) The imports:

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.orbits.halo import Halo

import numpy as np
import matplotlib.pyplot as plt
from src.plotting.plotting_module import PlottingOptions, MasterPlotter

# %%
# 2) CRTBP system of interest:

cr3bp = Cr3bp(Primary.SUN, Primary.EARTH)

# %%
# 3) The external state0, corresponds to a Halo in CRTBP Sun-Earth system about L2,
# Family.southern, Azdim bigger than 142000 Km.

state0_external = np.array([1.00831832,  0,  7.58154430e-04,  0,  9.96671251e-03, 0])

# %%
# 4) Orbit initialization and crtbp

orbit1 = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, state0=state0_external, Azdim=12000)
orbit1.computation()

# %%
# Does not matter what Azdim is passed in the constructor if it is passed an external Initial state,
# at the end it will take the Azdim computed with the Initial state that we are passing,
# in this case state0_external
#
# 5) Visualization:

plt.ioff()

# %%
# Note: The real scale for the primaries is set when we use:
# scale_factor=[True, 1] (default is: scale_factor=[True, 3])

plot_options = PlottingOptions(names=[True, 'normal'], scale_factor=[True, 10],
                               sci=True, l3=False, l4=False, l5=False)

plot1 = MasterPlotter(plot_options, orbit1)
plot1.scene_TD()
plot1.scene_twoD()
plt.show()
