"""
Halo Orbit Interpolation.
=========================
"""

# %%
# Imports:
#
# As in the script for halo_computation, to be able to run the script it is necessary to import
# modules and classes needed:

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.orbits.halo import Halo

import matplotlib.pyplot as plt
from src.plotting.plotting_module import PlottingOptions, MasterPlotter

# %%
# Initialization and Interpolation:
#
# This script is to interpolate an orbit from an abacus, which are .mat files that contain
# the required data to perform orbit interpolation, we will se here two cases.
#
# Initialization of the CRTBP system of interest:

cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

# %%
# In a first case, we will use the keyword argument Azdim to initialize the orbit with the
# nominal orbit vertical extension:

orbit = Halo(cr3bp, cr3bp.l1, Halo.Family.northern, Azdim=12000)

# %%
# Followed by the use of interpolation method, in order to interpolate the desired orbit:

orbit.interpolation()

# %%
# For a second case, we are going to initialize the orbit with another keyword argument,
# the nominal orbit Jacobi constant Cjac:

orbit1 = Halo(cr3bp, cr3bp.l2, Halo.Family.northern, Cjac=3.15)

# %%
# Then the instance method interpolation is used for orbit1:

orbit1.interpolation()

# %%
# Visualization:
#
# Ultimately to visualize orbit and orbit1:

plt.ioff()
plt.close('all')
plot_options = PlottingOptions(names=[True, 'normal'], sci=True)

plot1 = MasterPlotter(plot_options, orbit, orbit1=orbit1)
plot1.scene_TD()
plot1.scene_twoD()
plt.show()

# %%
# Note: The real scale for the primaries is set when we use:
# scale_factor=[True, 1] (default is: scale_factor=[True, 3])
#
# For more details on the visualization options, please refer to the plotting module documentation.
#
# If you want a quick look into all the attributes of the orbit instance just input the next line
# in the python console:

print(orbit.__dict__.keys())

# %%
# To see a given attribute, e.g. the initial state:

print(orbit.state0)

# %%
# For more details on Halo attributes please refer to the orbit module documentation
