"""
Interpolation of an Halo family.
================================
"""

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.orbits.halo import Halo

import matplotlib.pyplot as plt
from src.plotting.plotting_module import PlottingOptions, MasterPlotter

import time

start_time = time.perf_counter()

Az_f = []
orbits = []
orbit_labels = []
orbit_dict = {}
cr3bp_EM = Cr3bp(Primary.EARTH, Primary.MOON)

for i in range(1000, 70000, 4000):
    Az_f.append(i)
print(Az_f)  # Azdim of each orbit
print(len(Az_f))  # Total number of plotted orbits

for i in range(len(Az_f)):
    orbits.append(Halo(cr3bp_EM, cr3bp_EM.l2, Halo.Family.southern, Azdim=Az_f[i]))
    orbit_labels.append('orbit' + str(i))

for i in range(len(orbits)):

    orbits[i].interpolation()

for i in range(len(orbits)):
    orbit_dict[orbit_labels[i]] = orbits[i]

print('\n', '                   Computation time                    ', '\n')
print("--- %s seconds ---" % (time.perf_counter() - start_time))

plt.ioff()
plt.close('all')
plotting_options = PlottingOptions(names=[True, 'normal'], l1=False, disp_first_pr=False,
                                   legend=False, sci=True)
plot = MasterPlotter(plotting_options, orbits[0], **orbit_dict)
plot.show_all()
plot.modify_title('Earth-Moon CRTBP system, Halo family about L2.'
                  '\n Nominal Az_dim from 1000 km to 69000 km, step=4000km, 18 Halos')
plt.show()
