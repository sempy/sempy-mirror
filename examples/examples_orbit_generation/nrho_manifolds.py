"""
Near Rectilinear Halo Orbit manifolds.
======================================

This script demonstrate how to generate manifold tubes associated to a given Near Rectilinear Halo
Orbit (NRHO) in the Earth-Moon system.

Allowed tubes are all possible combinations of stability (stable/unstable) and way
(interior/exterior) and can be computed one by one or all together in a single function call.
For each tube, several positions along the orbit must be specified to compute a discrete set of
manifold branches that approximates the continuous tube.

Each branch is described by an object of the `CrtbpOrbit` inner class `ManifoldBranch` and
automatically appended to the orbit's attribute `manifolds` when one of the two methods
`CrtbpOrbit.all_manifolds_computation()` or `CrtbpOrbit.single_manifold_computation()` is called.

"""

# %%
# Firstly, all required modules and classes must be imported:

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.orbits.nrho import NRHO

import matplotlib.pyplot as plt
from src.plotting.util import set_axes_equal


def set_axes_lim_labels_title(ax, title):
    """Convenience function to set the axes limits, labels and the figure title. """
    ax.set_xlabel('x [-]')
    ax.set_ylabel('y [-]')
    ax.set_zlabel('z [-]')
    ax.set_title(title)
    set_axes_equal(ax)


if __name__ == '__main__':
    # %%
    # Then, the reference NRHO is instantiated and interpolated as described in
    # `Near Rectilinear Halo Orbit interpolation` example script:

    cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
    nrho1 = NRHO(cr3bp, cr3bp.l2, NRHO.Family.southern, Altpdim=6e3)
    nrho1.interpolation()

    # %%
    # To compute all manifolds tubes at once, we make use of the `CrtbpOrbit` class method
    # `all_manifolds_computation()`. Its usage is as simple as:

    nrho1.all_manifolds_computation(11, 20., max_internal_steps=2_000_000)

    # %%
    # Where we specified to compute 11 branches for each manifold equally spaced along the orbit.
    # Moreover, each branch will be propagated for an amount of time equal to 20 in non-dimensional
    # units.
    #
    # If a long propagation time is sought, numerical difficulties might be encountered by the
    # integration routines due to an imposed upper limit on the available integrator time steps.
    # To increase this limit, the `max_internal_steps` parameter should be set equal to an
    # higher value w.r.t. the default one specified in `default.ini`.
    #
    # If specific departure states ar sought for the branches, the first positional argument can be
    # replaced by a Numpy array containing those positions expressed as fractions of the orbit's
    # period between 0 and 1. For example, passing ``numpy.array([0.25, 0.75])`` will compute two
    # branches per tube that depart the orbit at 1/4 and 3/4 of its period.
    #
    # A single manifold tube can also be computed with a function call similar to the above where
    # two positional arguments must be prepended to specify the manifold stability
    # (stable/unstable) and way (interior/exterior):

    nrho2 = NRHO(cr3bp, cr3bp.l2, NRHO.Family.southern, Altpdim=6e3)
    nrho2.interpolation()

    nrho2.single_manifold_computation('unstable', 'interior', 11, 20.)

    # %%
    # Finally, if we are interested in a specific branch we can make use of the inner class
    # `ManifoldBranch` to compute it directly.
    #
    # Nota: differently as before, the computed branch is not automatically stored in the
    # orbit class attribute `manifolds`.

    manifold_branch = nrho2.ManifoldBranch(nrho2, 'stable', 'exterior')
    manifold_branch.computation(0.25, 20.)

    # %%
    # Where the two positional arguments in `ManifoldBranch.crtbp()` refer to the branch
    # position and propagation time respectively.
    #
    # The obtained trajectories are then plotted as follows:

    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111, projection='3d')
    ax1.plot(nrho1.state_vec[:, 0], nrho1.state_vec[:, 1], nrho1.state_vec[:, 2], color='b')
    for kind in nrho1.manifolds:
        for branch in nrho1.manifolds[kind]:
            color = 'r' if branch.stability == 'unstable' else 'g'
            ax1.plot(branch.state_vec[:, 0], branch.state_vec[:, 1], branch.state_vec[:, 2],
                     color=color)
    set_axes_lim_labels_title(ax1, 'Stable/unstable, interior/exterior manifold tubes')

    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111, projection='3d')
    ax2.plot(nrho2.state_vec[:, 0], nrho2.state_vec[:, 1], nrho2.state_vec[:, 2], color='b')
    for branch in nrho2.manifolds['unstable_interior']:
        ax2.plot(branch.state_vec[:, 0], branch.state_vec[:, 1], branch.state_vec[:, 2], color='r')
    ax2.plot(manifold_branch.state_vec[:, 0], manifold_branch.state_vec[:, 1],
             manifold_branch.state_vec[:, 2], color='g')
    set_axes_lim_labels_title(ax2, 'Unstable interior tube, stable exterior branch')
    plt.show()
