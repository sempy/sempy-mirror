"""
Halo Orbit Computation.
=======================

"""

# %%
# 1) Imports
#
# To run a script it is necessary to import modules and classes needed to perform the required task,
# in this script we will compute a halo orbit, therefore the following modules and classes are
# imported from the package.

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.orbits.halo import Halo

import matplotlib.pyplot as plt
from src.plotting.plotting_module import PlottingOptions, MasterPlotter

# %%
# 2) Initialization and Computation
#
# To compute an orbit from a first guess, the first step is to create the CRTBP system of interest,
# this task is done by using Cr3bp class.
# As arguments, the user needs to pass the primaries to the Cr3bp class constructor, m1 and m2,
# just by using two instances of Primary class, for example: Primary.SUN and Primary.JUPITER.
# The primaries that are available in the package can be found in primary module.
#
# The CRTBP system of interest is contained in an object (cr3bp), as follows:

cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

# %%
# Second step, initialize the orbit desired to be studied, here the user has to use the class
# corresponding to the desired orbit.
# Four required arguments must be passed to the class constructor:
# The first one is the cr3bp object create before;
# the second one is the libration point around which we want our spacecraft to orbit;
# the third one is the orbit's family;
# and the last one is the nominal Extension parameter or the nominal Jacobi constant,
# that the desired orbit has.
#
# For example, if a halo orbit is the focus of research the orbit initialization will be:

orbit = Halo(cr3bp, cr3bp.l2, Halo.Family.northern, Azdim=12000)

# %%
# Third step, the crtbp of the Halo orbit.
# In the previous step an instance (orbit) of the class Halo was created, it contains the required
# information to perform the orbit crtbp by just using the instance method crtbp().
#
# To perform this task, it is only necessary to state the following line of code:

orbit.computation()

# %%
# 3) Visualization
#
# Ultimately, the CRTBP system and the computed orbit can be visualized by using the next lines
# of code:
#
# The real scale for the primaries is set when we use:
# scale_factor=[True, 1] (default is: scale_factor=[True, 3])

plt.ioff()
plt.close('all')
plot_options = PlottingOptions(names=[True, 'normal'], scale_factor=[True, 3],
                               sci=True, l3=True, l4=False, l5=False)
plot1 = MasterPlotter(plot_options, orbit)
plot1.scene_TD()
plot1.scene_twoD()
plt.show()

# %%
# For more details on the visualization options, please refer to the plotting module documentation.
#
# 4) Accessing object attributes
#
# If you want a quick look into all the attributes of the orbit instance just input the next
# line in the python console:

print(orbit.__dict__.keys())

# %%
# To see a given attribute, e.g. the initial state:

print(orbit.state0)

# %%
# For more details on Halo attributes please refer to the orbit module documentation
#
# 5) Extra features for the crtbp method:
#
# crtbp() method has as argument: fix_dim
#
# When nothing is passed to the argument fix_dim, a suitable differential correction
# procedure is performed, relying on the definition of 'big orbits' (Az_estimate > 0.0520)
# that is loosely based on results from the Earth-Moon system.
#
# If fix_dim is equal to Halo.DiffCorrFixDim.z0, z0 dimension is fixed.
# Should be used with small Az

orbit.computation(Halo.DiffCorrFixDim.z0)

# %%
# If fix_dim is equal to Halo.DiffCorrFixDim.x0, x0 dimension is fixed.
# Should be used with big Az

orbit.computation(Halo.DiffCorrFixDim.x0)

# %%
# For more details on the differential correction procedure,
# please check Halo and DiffCorr3D classes docstring.
