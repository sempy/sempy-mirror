"""
Propagation of an Initial State.
================================

If there is available an initial state it can be propagated by following the next steps.

"""

# %%
# 1) Import all necessary modules and classes:

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.propagation.cr3bp_propagator import Cr3bpSynodicPropagator

# %%
# 2) Initialize the CRTBP system:

cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

# %%
# 3) Set the initial state to be propagated:

state0 = np.array([1.116756261235440, 0, 0.0222299747777274, 0, 0.186527612353890, 0])

# %%
# 4) Set the time for the propagation:

T = 3.407534811635316
t_span = [0, T]

# %%
# 5) Initialize the propagator:

prop = Cr3bpSynodicPropagator(cr3bp.mu)

# %%
# 5) Now the class Cr3bpSynodicPropagator can be used to propagate our initial state:

t_vec, state_vec, _, _ = prop.propagate(t_span, state0)

# %%
# 6) Plot the results:

plt.rcParams['legend.fontsize'] = 10
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot(state_vec[:, 0], state_vec[:, 1], state_vec[:, 2], 'y', label='Propagation state0')
ax.legend()
plt.show()
