"""
Near Rectilinear Orbit interpolation.
=====================================

This script demonstrates how to initialize, compute and display a Near Rectilinear Orbit (NRO).
Since a direct crtbp of these kind of orbits is not possible, the orbit's initial state is
firstly interpolated from previously stored abacus data and then corrected using a differential
corrector based on a single-shooting technique.

Currently available abacus data cover the northern and southern NRO families about the L1 and L2
libration points of the Earth-Moon system.

"""

# %%
# Imports:
#
# Firstly, the required modules and classes have to be imported for the script to be executed:

import sys
sys.path.append('./')
sys.path.append('../')
sys.path.append('../../')

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.orbits.nrho import NRHO

import matplotlib.pyplot as plt
from src.plotting.plotting_module import MasterPlotter, PlottingOptions

# %%
# Once all the elements have been imported, a `Cr3bp` class instance is instantiated:

cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)

# %%
# Then, an NRO can be instantiated specifying the libration point (L1 or L2), family
# (northern or southern) and one keyword arguments among periselene altitude ``Altpdim``,
# periselene radius ``Rpdim``, vertical extension ``Azdim`` or Jacobi constant ``Cjac``.
# Except for ``Cjac``, all other parameters must be expressed in kilometers.
#
# In this example an L2 southern NRO with periselene altitude of 8000 km is instantiated:

nrho = NRHO(cr3bp, cr3bp.l2, NRHO.Family.southern, Altpdim=1390)

# %%
# The instantiated orbit can now be interpolated, corrected and propagated to retrieve all its
# characteristics:

nrho.interpolation()

# %%
# Finally, the obtained NRO is plotted as follows:

opts = PlottingOptions(disp_first_pr=False, scale_factor=[True, 1],
                       legend=None, orbit_color='blue')
plot = MasterPlotter(opts, nrho)
plot.show_all()
plt.show()

