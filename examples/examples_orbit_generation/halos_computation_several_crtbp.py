"""
Halo Computation in several CRTBP systems.
==========================================

"""

# %%
# Imports:

from src.init.primary import Primary
from src.init.cr3bp import Cr3bp
from src.orbits.halo import Halo

import matplotlib.pyplot as plt
from src.plotting.plotting_module import PlottingOptions, MasterPlotter

# %%
# Initialization and Computation
#
# If needed, orbits in several CRTBP systems can be studied in the same script.
#
# First CRTBP system is Earth-Moon, here we are going to compute 4 orbits toward different
# Libration points or with different orbit families:

cr3bp = Cr3bp(Primary.EARTH, Primary.MOON)
orbit = Halo(cr3bp, cr3bp.l2, Halo.Family.northern, Azdim=12000)
orbit.computation()


orbit1 = Halo(cr3bp, cr3bp.l1, Halo.Family.northern, Azdim=12000)
orbit1.computation()

orbit2 = Halo(cr3bp, cr3bp.l3, Halo.Family.northern, Azdim=12000)
orbit2.computation()

orbit3 = Halo(cr3bp, cr3bp.l2, Halo.Family.southern, Azdim=12000)  # southern
orbit3.computation()

# %%
# Second system is Sun-Earth, here only one orbit is computed, is an orbit about L2:

cr3bp_SE = Cr3bp(Primary.SUN, Primary.EARTH)
orbit_SE = Halo(cr3bp_SE, cr3bp_SE.l2, Halo.Family.northern, Azdim=125000)
orbit_SE.computation()

# %%
# Third CRTBP system is Sun-Jupiter, with an orbit about L2:

cr3bp_SJ = Cr3bp(Primary.SUN, Primary.JUPITER)
orbit_SJ = Halo(cr3bp_SJ, cr3bp_SJ.l2, Halo.Family.northern, Azdim=15000000)
orbit_SJ.computation()

# %%
# To visualize all:

plt.ioff()
plt.close('all')

# %%
# Earth-Moon CRTBP system:

plot_options = PlottingOptions(names=[True, 'normal'], sci=True, l3=True, l4=False, l5=False)
plot1 = MasterPlotter(plot_options, orbit, orbit1=orbit1, orbit2=orbit2, orbit3=orbit3)

plot1.scene_TD()
plot1.scene_twoD()

# %%
# Sun-Earth CRTBP system:
plot_options_SE = PlottingOptions(names=[True, 'normal'], scale_factor=[True, 10], sci=True,
                                  l3=False, l4=False, l5=False)
plot2 = MasterPlotter(plot_options_SE, orbit_SE)
plot2.scene_twoD()
plot2.scene_TD()

# %%
# Sun-Jupiter CRTBP system:
plot_options_SJ = PlottingOptions(names=[True, 'normal'], scale_factor=[True, 20], sci=True,
                                  l3=False, l4=False, l5=False)
plot3 = MasterPlotter(plot_options_SJ, orbit_SJ)
plot3.scene_twoD()
plot3.scene_TD()
plt.show()
