# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
from setuptools.command.install import install
from setuptools.command.develop import develop
from setuptools.command.egg_info import egg_info

import atexit
import runpy

import os
from os.path import basename, splitext
from importlib.machinery import SourceFileLoader
from setuptools import setup, find_packages
from glob import glob
import io


############## In order to compile Cr3bp dynamics ##############
def _post_install():
    print("Compiling CR3BP dynamics with numba ...")
    """Post-installation script."""
    runpy.run_path("src/dynamics/srcs/cr3bp_dynamics.py", run_name="__main__")


class NewDevelop(develop):
    def __init__(self, *args, **kwargs):
        super(NewDevelop, self).__init__(*args, **kwargs)
        atexit.register(_post_install)


class NewEggInfo(egg_info):
    def __init__(self, *args, **kwargs):
        super(NewEggInfo, self).__init__(*args, **kwargs)
        atexit.register(_post_install)


class NewInstall(install):
    def __init__(self, *args, **kwargs):
        super(NewInstall, self).__init__(*args, **kwargs)
        atexit.register(_post_install)


here = os.path.abspath(os.path.dirname(__file__))

# get the dependencies and installs
with io.open(os.path.join(here, "requirements.txt"), encoding="utf-8") as f:
    # Remove flags like "--no-binary=rasterio"
    install_requires = [line.split(" ")[0] for line in f.read().split("\n")]

with open(os.path.join(here, "README.md")) as readme_file:
    readme = readme_file.read()


setup(
    name="SEMpy",
    version="0.1",
    description="SEMpy: A Research Tool for Mission Analysis in the Sun-Earth-Moon System",
    long_description=readme,
    long_description_content_type="text/markdown",
    author="Emmanuel BLAZQUEZ, Thibault GATEAU, Alberto FOSSA, Edgar Perez, Paolo Guardabasso, Andrea Zollo",
    author_email="emmanuel.blazquez@esa.int",
    url="https://gitlab.com/EBlazquez/sempy",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: GNU GPL V3 License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.8",
    cmdclass={"install": NewInstall, "develop": NewDevelop, "egg_info": NewEggInfo},
    packages=find_packages(),
    # zip_safe=False,
    include_package_data=True,
    install_requires=install_requires,
)

